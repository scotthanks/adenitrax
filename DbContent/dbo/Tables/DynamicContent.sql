﻿CREATE TABLE [dbo].[DynamicContent] (
    [Id]          BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]        UNIQUEIDENTIFIER NOT NULL,
    [IsLive]      BIT              NOT NULL,
    [Description] VARCHAR (100)    NOT NULL,
    [HTML]        VARCHAR (MAX)    NOT NULL,
    [Note]        TEXT             NOT NULL,
    [Type]        VARCHAR (20)     NOT NULL,
    CONSTRAINT [PK__DynamicContent_Guid] PRIMARY KEY CLUSTERED ([Guid] ASC)
);

