﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public class GetStaticContentFromPathProcedure : Procedure<GetStaticContentFromPathProcedure.GetStaticContentByStringParameters>
    {
        public GetStaticContentFromPathProcedure()
        {
            ProcedureName = "GetStaticContentFromPath";
        }

        public class GetStaticContentByStringParameters : ProcedureParameterBase
        {
            public string Path { get; set; }

            public override void AddTo(SqlCommand command)
            {
                var contentUrlParameter = new SqlParameter("@Path", SqlDbType.NVarChar, 255);
                contentUrlParameter.Value = Path;
                command.Parameters.Add(contentUrlParameter);
            }

            public override void GetOutputValuesFrom(SqlCommand command)
            {
            }
        };
    }
}
