﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;


namespace DataServiceLibrary
{
  

    public partial class OrderLineChangeStatusProcedure : Procedure<OrderLineChangeStatusProcedure.OrderLineChangeStatusParameters>
    {
        public OrderLineChangeStatusProcedure()
        {
            ProcedureName = "OrderLineChangeStatus";
        }

        public class OrderLineChangeStatusParameters : ProcedureParameterBase
        {
            public string UserCode { internal get; set; }
            public Guid? UserGuid { internal get; set; }
            public string ShipWeekCode { internal get; set; }
            public string ProgramTypeCode { internal get; set; }
            public string ProductFormCategoryCode { internal get; set; }
            public Guid? GrowerOrderGuid { internal get; set; }
            public Guid? SupplierOrderGuid { internal get; set; }
            public Guid? OrderLineGuid { internal get; set; }
            public string FromOrderLineStatusCode { internal get; set; }
            public string FromOrderLineStatusCodeList { internal get; set; }
            public string ToOrderLineStatusCode { internal get; set; }
            public string DeleteOrderLinesWithOrderLineStatusCode { internal get; set; }
            public int? OrderLinesChanged { get; set; }
            public int? OrderLinesDeleted { get; set; }

            public override void AddTo(SqlCommand command)
            {
                CheckForParameterOverflow("@UserCode", UserCode, 56, "OrderLineChangeStatus");
                var userCodeParameter = new SqlParameter("@UserCode", SqlDbType.NChar, 56);
                userCodeParameter.Value = UserCode;
                command.Parameters.Add(userCodeParameter);

                var userGuidParameter = new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier, 16);
                userGuidParameter.Value = UserGuid;
                command.Parameters.Add(userGuidParameter);

                CheckForParameterOverflow("@ShipWeekCode", ShipWeekCode, 6, "OrderLineChangeStatus");
                var shipWeekCodeParameter = new SqlParameter("@ShipWeekCode", SqlDbType.NChar, 6);
                shipWeekCodeParameter.Value = ShipWeekCode;
                command.Parameters.Add(shipWeekCodeParameter);

                CheckForParameterOverflow("@ProgramTypeCode", ProgramTypeCode, 20, "OrderLineChangeStatus");
                var programTypeCodeParameter = new SqlParameter("@ProgramTypeCode", SqlDbType.NChar, 20);
                programTypeCodeParameter.Value = ProgramTypeCode;
                command.Parameters.Add(programTypeCodeParameter);

                CheckForParameterOverflow("@ProductFormCategoryCode", ProductFormCategoryCode, 20, "OrderLineChangeStatus");
                var productFormCategoryCodeParameter = new SqlParameter("@ProductFormCategoryCode", SqlDbType.NChar, 20);
                productFormCategoryCodeParameter.Value = ProductFormCategoryCode;
                command.Parameters.Add(productFormCategoryCodeParameter);

                var growerOrderGuidParameter = new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier, 16);
                growerOrderGuidParameter.Value = GrowerOrderGuid;
                command.Parameters.Add(growerOrderGuidParameter);

                var supplierOrderGuidParameter = new SqlParameter("@SupplierOrderGuid", SqlDbType.UniqueIdentifier, 16);
                supplierOrderGuidParameter.Value = SupplierOrderGuid;
                command.Parameters.Add(supplierOrderGuidParameter);

                var orderLineGuidParameter = new SqlParameter("@OrderLineGuid", SqlDbType.UniqueIdentifier, 16);
                orderLineGuidParameter.Value = OrderLineGuid;
                command.Parameters.Add(orderLineGuidParameter);

                CheckForParameterOverflow("@FromOrderLineStatusCode", FromOrderLineStatusCode, 50, "OrderLineChangeStatus");
                var fromOrderLineStatusCodeParameter = new SqlParameter("@FromOrderLineStatusCode", SqlDbType.NChar, 50);
                fromOrderLineStatusCodeParameter.Value = FromOrderLineStatusCode;
                command.Parameters.Add(fromOrderLineStatusCodeParameter);

                CheckForParameterOverflow("@FromOrderLineStatusCodeList", FromOrderLineStatusCodeList, 500, "OrderLineChangeStatus");
                var fromOrderLineStatusCodeListParameter = new SqlParameter("@FromOrderLineStatusCodeList", SqlDbType.NVarChar, 500);
                fromOrderLineStatusCodeListParameter.Value = FromOrderLineStatusCodeList;
                command.Parameters.Add(fromOrderLineStatusCodeListParameter);

                CheckForParameterOverflow("@ToOrderLineStatusCode", ToOrderLineStatusCode, 50, "OrderLineChangeStatus");
                var toOrderLineStatusCodeParameter = new SqlParameter("@ToOrderLineStatusCode", SqlDbType.NChar, 50);
                toOrderLineStatusCodeParameter.Value = ToOrderLineStatusCode;
                command.Parameters.Add(toOrderLineStatusCodeParameter);

                CheckForParameterOverflow("@DeleteOrderLinesWithOrderLineStatusCode", DeleteOrderLinesWithOrderLineStatusCode, 30, "OrderLineChangeStatus");
                var deleteOrderLinesWithOrderLineStatusCodeParameter = new SqlParameter("@DeleteOrderLinesWithOrderLineStatusCode", SqlDbType.NChar, 30);
                deleteOrderLinesWithOrderLineStatusCodeParameter.Value = DeleteOrderLinesWithOrderLineStatusCode;
                command.Parameters.Add(deleteOrderLinesWithOrderLineStatusCodeParameter);

                var orderLinesChangedParameter = new SqlParameter("@OrderLinesChanged", SqlDbType.Int, 4);
                orderLinesChangedParameter.Value = OrderLinesChanged;
                orderLinesChangedParameter.Direction = ParameterDirection.Output;
                command.Parameters.Add(orderLinesChangedParameter);

                var orderLinesDeletedParameter = new SqlParameter("@OrderLinesDeleted", SqlDbType.Int, 4);
                orderLinesDeletedParameter.Value = OrderLinesDeleted;
                orderLinesDeletedParameter.Direction = ParameterDirection.Output;
                command.Parameters.Add(orderLinesDeletedParameter);

            }

            public override void GetOutputValuesFrom(SqlCommand command)
            {
                OrderLinesChanged = (int?)command.Parameters["@OrderLinesChanged"].Value ?? null;
                OrderLinesDeleted = (int?)command.Parameters["@OrderLinesDeleted"].Value ?? null;
            }
        };
    }
   
    

}
