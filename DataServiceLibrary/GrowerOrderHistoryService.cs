﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using Utility;
using DataObjectLibrary;
using System.Data.SqlClient;



namespace DataServiceLibrary
{
    public  class GrowerOrderHistoryService
    {
        public class RequestStatus
        {
            public bool Success { get; set; }
            public string Message { get; set; }
        }

        public RequestStatus DeleteOrderHistroy(Guid UserGuid,Guid OrderHistoryGuid )
        {
            RequestStatus response = new RequestStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("GrowerHistoryTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = "EXEC GrowerOrderHistoryDelete '" + UserGuid + "','" + OrderHistoryGuid + "'";
                    

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }

            return response;
        }
        public RequestStatus EditOrderHistroy(Guid UserGuid, Guid OrderHistoryGuid, string theComment, bool IsInternal)
        {
            RequestStatus response = new RequestStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("GrowerHistoryTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    int theBool = 1;
                    if (IsInternal == false)
                    { 
                        theBool = 0;
                    }
                    command.CommandText = "Update GrowerOrderHistory set IsInternal  = " + theBool + " ,Comment = '" + theComment + "' WHERE Guid = '" + OrderHistoryGuid + "'";
                     

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }

            return response;
        }

    }
}
