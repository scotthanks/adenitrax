﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class VarietyTableService : TableService<Variety>
    {
        public VarietyTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override Variety DefaultInsert(Variety variety, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [Variety]([Guid], [DateDeactivated], [Code], [SpeciesGuid], [GeneticOwnerGuid], [Name], [ActiveDate], [DeactiveDate], [ColorDescription], [ImageSetGuid], [ColorLookupGuid], [HabitLookupGuid], [VigorLookupGuid], [TimingLookupGuid], [SeriesGuid], [DescriptionHTMLGUID], [ResearchStatus], [CultureLibraryLink], [LastUpdate], [NameStripped], [SpeciesAndNameStripped], [ZoneLookupGuid], [BrandLookupGuid], ) VALUES (@Guid, @DateDeactivated, @Code, @SpeciesGuid, @GeneticOwnerGuid, @Name, @ActiveDate, @DeactiveDate, @ColorDescription, @ImageSetGuid, @ColorLookupGuid, @HabitLookupGuid, @VigorLookupGuid, @TimingLookupGuid, @SeriesGuid, @DescriptionHTMLGUID, @ResearchStatus, @CultureLibraryLink, @LastUpdate, @NameStripped, @SpeciesAndNameStripped, @ZoneLookupGuid, @BrandLookupGuid, );SELECT * FROM [Variety] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, variety, transaction: transaction);
        }

        protected override Variety DefaultUpdate(Variety variety, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [Variety] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [SpeciesGuid]=@SpeciesGuid, [GeneticOwnerGuid]=@GeneticOwnerGuid, [Name]=@Name, [ActiveDate]=@ActiveDate, [DeactiveDate]=@DeactiveDate, [ColorDescription]=@ColorDescription, [ImageSetGuid]=@ImageSetGuid, [ColorLookupGuid]=@ColorLookupGuid, [HabitLookupGuid]=@HabitLookupGuid, [VigorLookupGuid]=@VigorLookupGuid, [TimingLookupGuid]=@TimingLookupGuid, [SeriesGuid]=@SeriesGuid, [DescriptionHTMLGUID]=@DescriptionHTMLGUID, [ResearchStatus]=@ResearchStatus, [CultureLibraryLink]=@CultureLibraryLink, [LastUpdate]=@LastUpdate, [NameStripped]=@NameStripped, [SpeciesAndNameStripped]=@SpeciesAndNameStripped, [ZoneLookupGuid]=@ZoneLookupGuid, [BrandLookupGuid]=@BrandLookupGuid, WHERE [Guid]=@Guid;SELECT * FROM [Variety] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, variety, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, Variety variety)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = variety.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = variety.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = variety.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NChar, 30);
            codeParameter.IsNullable = false;
            variety.Code = variety.Code ?? "";
            variety.Code = variety.Code.Trim();
            codeParameter.Value = variety.Code;
            command.Parameters.Add(codeParameter);

            var speciesGuidParameter = new SqlParameter("@SpeciesGuid", SqlDbType.UniqueIdentifier, 16);
            speciesGuidParameter.IsNullable = false;
            speciesGuidParameter.Value = variety.SpeciesGuid;
            command.Parameters.Add(speciesGuidParameter);

            var geneticOwnerGuidParameter = new SqlParameter("@GeneticOwnerGuid", SqlDbType.UniqueIdentifier, 16);
            geneticOwnerGuidParameter.IsNullable = false;
            geneticOwnerGuidParameter.Value = variety.GeneticOwnerGuid;
            command.Parameters.Add(geneticOwnerGuidParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
            nameParameter.IsNullable = false;
            variety.Name = variety.Name ?? "";
            variety.Name = variety.Name.Trim();
            nameParameter.Value = variety.Name;
            command.Parameters.Add(nameParameter);

            var activeDateParameter = new SqlParameter("@ActiveDate", SqlDbType.DateTime, 8);
            activeDateParameter.IsNullable = false;
            activeDateParameter.Value = variety.ActiveDate;
            command.Parameters.Add(activeDateParameter);

            var deactiveDateParameter = new SqlParameter("@DeactiveDate", SqlDbType.DateTime, 8);
            deactiveDateParameter.IsNullable = true;
            deactiveDateParameter.Value = variety.DeactiveDate ?? (object)DBNull.Value;
            command.Parameters.Add(deactiveDateParameter);

            var colorDescriptionParameter = new SqlParameter("@ColorDescription", SqlDbType.NChar, 30);
            colorDescriptionParameter.IsNullable = false;
            variety.ColorDescription = variety.ColorDescription ?? "";
            variety.ColorDescription = variety.ColorDescription.Trim();
            colorDescriptionParameter.Value = variety.ColorDescription;
            command.Parameters.Add(colorDescriptionParameter);

            var imageSetGuidParameter = new SqlParameter("@ImageSetGuid", SqlDbType.UniqueIdentifier, 16);
            imageSetGuidParameter.IsNullable = true;
            imageSetGuidParameter.Value = variety.ImageSetGuid ?? (object)DBNull.Value;
            command.Parameters.Add(imageSetGuidParameter);

            var colorLookupGuidParameter = new SqlParameter("@ColorLookupGuid", SqlDbType.UniqueIdentifier, 16);
            colorLookupGuidParameter.IsNullable = true;
            colorLookupGuidParameter.Value = variety.ColorLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(colorLookupGuidParameter);

            var habitLookupGuidParameter = new SqlParameter("@HabitLookupGuid", SqlDbType.UniqueIdentifier, 16);
            habitLookupGuidParameter.IsNullable = true;
            habitLookupGuidParameter.Value = variety.HabitLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(habitLookupGuidParameter);

            var vigorLookupGuidParameter = new SqlParameter("@VigorLookupGuid", SqlDbType.UniqueIdentifier, 16);
            vigorLookupGuidParameter.IsNullable = true;
            vigorLookupGuidParameter.Value = variety.VigorLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(vigorLookupGuidParameter);

            var timingLookupGuidParameter = new SqlParameter("@TimingLookupGuid", SqlDbType.UniqueIdentifier, 16);
            timingLookupGuidParameter.IsNullable = false;
            timingLookupGuidParameter.Value = variety.TimingLookupGuid;
            command.Parameters.Add(timingLookupGuidParameter);

            var seriesGuidParameter = new SqlParameter("@SeriesGuid", SqlDbType.UniqueIdentifier, 16);
            seriesGuidParameter.IsNullable = true;
            seriesGuidParameter.Value = variety.SeriesGuid ?? (object)DBNull.Value;
            command.Parameters.Add(seriesGuidParameter);

            var descriptionHTMLGUIDParameter = new SqlParameter("@DescriptionHTMLGUID", SqlDbType.UniqueIdentifier, 16);
            descriptionHTMLGUIDParameter.IsNullable = true;
            descriptionHTMLGUIDParameter.Value = variety.DescriptionHTMLGUID ?? (object)DBNull.Value;
            command.Parameters.Add(descriptionHTMLGUIDParameter);

            var researchStatusParameter = new SqlParameter("@ResearchStatus", SqlDbType.VarChar, 15);
            researchStatusParameter.IsNullable = true;
            variety.ResearchStatus = variety.ResearchStatus ?? "";
            variety.ResearchStatus = variety.ResearchStatus.Trim();
            researchStatusParameter.Value = variety.ResearchStatus ?? (object)DBNull.Value;
            command.Parameters.Add(researchStatusParameter);

            var cultureLibraryLinkParameter = new SqlParameter("@CultureLibraryLink", SqlDbType.NVarChar, 200);
            cultureLibraryLinkParameter.IsNullable = false;
            variety.CultureLibraryLink = variety.CultureLibraryLink ?? "";
            variety.CultureLibraryLink = variety.CultureLibraryLink.Trim();
            cultureLibraryLinkParameter.Value = variety.CultureLibraryLink;
            command.Parameters.Add(cultureLibraryLinkParameter);

            var lastUpdateParameter = new SqlParameter("@LastUpdate", SqlDbType.DateTime, 8);
            lastUpdateParameter.IsNullable = false;
            lastUpdateParameter.Value = variety.LastUpdate;
            command.Parameters.Add(lastUpdateParameter);

            var nameStrippedParameter = new SqlParameter("@NameStripped", SqlDbType.NVarChar, 100);
            nameStrippedParameter.IsNullable = false;
            variety.NameStripped = variety.NameStripped ?? "";
            variety.NameStripped = variety.NameStripped.Trim();
            nameStrippedParameter.Value = variety.NameStripped;
            command.Parameters.Add(nameStrippedParameter);

            var speciesAndNameStrippedParameter = new SqlParameter("@SpeciesAndNameStripped", SqlDbType.NVarChar, 150);
            speciesAndNameStrippedParameter.IsNullable = true;
            variety.SpeciesAndNameStripped = variety.SpeciesAndNameStripped ?? "";
            variety.SpeciesAndNameStripped = variety.SpeciesAndNameStripped.Trim();
            speciesAndNameStrippedParameter.Value = variety.SpeciesAndNameStripped ?? (object)DBNull.Value;
            command.Parameters.Add(speciesAndNameStrippedParameter);

            var zoneLookupGuidParameter = new SqlParameter("@ZoneLookupGuid", SqlDbType.UniqueIdentifier, 16);
            zoneLookupGuidParameter.IsNullable = true;
            zoneLookupGuidParameter.Value = variety.ZoneLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(zoneLookupGuidParameter);

            var brandLookupGuidParameter = new SqlParameter("@BrandLookupGuid", SqlDbType.UniqueIdentifier, 16);
            brandLookupGuidParameter.IsNullable = true;
            brandLookupGuidParameter.Value = variety.BrandLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(brandLookupGuidParameter);

        }

        protected override Variety DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var variety = new Variety();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)Variety.ColumnEnum.Id] ?? long.MinValue;
                variety.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)Variety.ColumnEnum.Guid] ?? Guid.Empty;
                variety.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)Variety.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                variety.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "Code";
                var code = reader[(int)Variety.ColumnEnum.Code] ?? string.Empty;
                code = TrimString(code);
                variety.Code = (string)code;

                columnName = "SpeciesGuid";
                var speciesGuid = reader[(int)Variety.ColumnEnum.SpeciesGuid] ?? Guid.Empty;
                variety.SpeciesGuid = (Guid)speciesGuid;

                columnName = "GeneticOwnerGuid";
                var geneticOwnerGuid = reader[(int)Variety.ColumnEnum.GeneticOwnerGuid] ?? Guid.Empty;
                variety.GeneticOwnerGuid = (Guid)geneticOwnerGuid;

                columnName = "Name";
                var name = reader[(int)Variety.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                variety.Name = (string)name;

                columnName = "ActiveDate";
                var activeDate = reader[(int)Variety.ColumnEnum.ActiveDate] ?? DateTime.MinValue;
                variety.ActiveDate = (DateTime)activeDate;

                columnName = "DeactiveDate";
                var deactiveDate = reader[(int)Variety.ColumnEnum.DeactiveDate];
                if (deactiveDate == DBNull.Value) deactiveDate = null;
                variety.DeactiveDate = (DateTime?)deactiveDate;

                columnName = "ColorDescription";
                var colorDescription = reader[(int)Variety.ColumnEnum.ColorDescription] ?? string.Empty;
                colorDescription = TrimString(colorDescription);
                variety.ColorDescription = (string)colorDescription;

                columnName = "ImageSetGuid";
                var imageSetGuid = reader[(int)Variety.ColumnEnum.ImageSetGuid];
                if (imageSetGuid == DBNull.Value) imageSetGuid = null;
                variety.ImageSetGuid = (Guid?)imageSetGuid;

                columnName = "ColorLookupGuid";
                var colorLookupGuid = reader[(int)Variety.ColumnEnum.ColorLookupGuid];
                if (colorLookupGuid == DBNull.Value) colorLookupGuid = null;
                variety.ColorLookupGuid = (Guid?)colorLookupGuid;

                columnName = "HabitLookupGuid";
                var habitLookupGuid = reader[(int)Variety.ColumnEnum.HabitLookupGuid];
                if (habitLookupGuid == DBNull.Value) habitLookupGuid = null;
                variety.HabitLookupGuid = (Guid?)habitLookupGuid;

                columnName = "VigorLookupGuid";
                var vigorLookupGuid = reader[(int)Variety.ColumnEnum.VigorLookupGuid];
                if (vigorLookupGuid == DBNull.Value) vigorLookupGuid = null;
                variety.VigorLookupGuid = (Guid?)vigorLookupGuid;

                columnName = "TimingLookupGuid";
                var timingLookupGuid = reader[(int)Variety.ColumnEnum.TimingLookupGuid] ?? Guid.Empty;
                variety.TimingLookupGuid = (Guid)timingLookupGuid;

                columnName = "SeriesGuid";
                var seriesGuid = reader[(int)Variety.ColumnEnum.SeriesGuid];
                if (seriesGuid == DBNull.Value) seriesGuid = null;
                variety.SeriesGuid = (Guid?)seriesGuid;

                columnName = "DescriptionHTMLGUID";
                var descriptionHTMLGUID = reader[(int)Variety.ColumnEnum.DescriptionHTMLGUID];
                if (descriptionHTMLGUID == DBNull.Value) descriptionHTMLGUID = null;
                variety.DescriptionHTMLGUID = (Guid?)descriptionHTMLGUID;

                columnName = "ResearchStatus";
                var researchStatus = reader[(int)Variety.ColumnEnum.ResearchStatus];
                if (researchStatus == DBNull.Value) researchStatus = null;
                researchStatus = TrimString(researchStatus);
                variety.ResearchStatus = (string)researchStatus;

                columnName = "CultureLibraryLink";
                var cultureLibraryLink = reader[(int)Variety.ColumnEnum.CultureLibraryLink] ?? string.Empty;
                cultureLibraryLink = TrimString(cultureLibraryLink);
                variety.CultureLibraryLink = (string)cultureLibraryLink;

                columnName = "LastUpdate";
                var lastUpdate = reader[(int)Variety.ColumnEnum.LastUpdate] ?? DateTime.MinValue;
                variety.LastUpdate = (DateTime)lastUpdate;

                columnName = "NameStripped";
                var nameStripped = reader[(int)Variety.ColumnEnum.NameStripped] ?? string.Empty;
                nameStripped = TrimString(nameStripped);
                variety.NameStripped = (string)nameStripped;

                columnName = "SpeciesAndNameStripped";
                var speciesAndNameStripped = reader[(int)Variety.ColumnEnum.SpeciesAndNameStripped];
                if (speciesAndNameStripped == DBNull.Value) speciesAndNameStripped = null;
                speciesAndNameStripped = TrimString(speciesAndNameStripped);
                variety.SpeciesAndNameStripped = (string)speciesAndNameStripped;

                columnName = "ZoneLookupGuid";
                var zoneLookupGuid = reader[(int)Variety.ColumnEnum.ZoneLookupGuid];
                if (zoneLookupGuid == DBNull.Value) zoneLookupGuid = null;
                variety.ZoneLookupGuid = (Guid?)zoneLookupGuid;

                columnName = "BrandLookupGuid";
                var brandLookupGuid = reader[(int)Variety.ColumnEnum.BrandLookupGuid];
                if (brandLookupGuid == DBNull.Value) brandLookupGuid = null;
                variety.BrandLookupGuid = (Guid?)brandLookupGuid;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return variety;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Variety variety)
        {
            SetUpSpeciesLazyLoad(variety);
            SetUpGeneticOwnerLazyLoad(variety);
            SetUpImageSetLazyLoad(variety);
            SetUpColorLookupLazyLoad(variety);
            SetUpHabitLookupLazyLoad(variety);
            SetUpVigorLookupLazyLoad(variety);
            SetUpTimingLookupLazyLoad(variety);
            SetUpSeriesLazyLoad(variety);
            SetUpZoneLookupLazyLoad(variety);
            SetUpBrandLookupLazyLoad(variety);
            SetUpProductListLazyLoad(variety);
            //SetUpSearchVarietyHelperListLazyLoad( variety);
        }

        protected override void FixAnyLazyLoadedLists(Variety variety, ListActionEnum listAction)
        {
            FixSpeciesList(variety, listAction);
            FixGeneticOwnerList(variety, listAction);
            FixImageSetList(variety, listAction);
            FixColorLookupList(variety, listAction);
            FixHabitLookupList(variety, listAction);
            FixVigorLookupList(variety, listAction);
            FixTimingLookupList(variety, listAction);
            FixSeriesList(variety, listAction);
            FixZoneLookupList(variety, listAction);
            FixBrandLookupList(variety, listAction);
        }

        private void SetUpSpeciesLazyLoad(Variety variety)
        {
            var speciesTableService = Context.Get<SpeciesTableService>();
            variety.SetLazySpecies(new Lazy<Species>(() => speciesTableService.GetByGuid(variety.SpeciesGuid), false));
        }

        private void FixSpeciesList(Variety variety, ListActionEnum listAction)
        {
            if (variety.SpeciesIsLoaded)
            {
                FixLazyLoadedList(variety.Species.VarietyList, variety, listAction);
            }
        }

        private void SetUpGeneticOwnerLazyLoad(Variety variety)
        {
            var geneticOwnerTableService = Context.Get<GeneticOwnerTableService>();
            variety.SetLazyGeneticOwner(new Lazy<GeneticOwner>(() => geneticOwnerTableService.GetByGuid(variety.GeneticOwnerGuid), false));
        }

        private void FixGeneticOwnerList(Variety variety, ListActionEnum listAction)
        {
            if (variety.GeneticOwnerIsLoaded)
            {
                FixLazyLoadedList(variety.GeneticOwner.VarietyList, variety, listAction);
            }
        }

        private void SetUpImageSetLazyLoad(Variety variety)
        {
            var imageSetTableService = Context.Get<ImageSetTableService>();
            variety.SetLazyImageSet(new Lazy<ImageSet>(() => imageSetTableService.GetByGuid(variety.ImageSetGuid), false));
        }

        private void FixImageSetList(Variety variety, ListActionEnum listAction)
        {
            if (variety.ImageSetIsLoaded)
            {
                FixLazyLoadedList(variety.ImageSet.VarietyList, variety, listAction);
            }
        }

        private void SetUpColorLookupLazyLoad(Variety variety)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            variety.SetLazyColorLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(variety.ColorLookupGuid), false));
        }

        private void FixColorLookupList(Variety variety, ListActionEnum listAction)
        {
            if (variety.ColorLookupIsLoaded)
            {
                FixLazyLoadedList(variety.ColorLookup.ColorVarietyList, variety, listAction);
            }
        }

        private void SetUpHabitLookupLazyLoad(Variety variety)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            variety.SetLazyHabitLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(variety.HabitLookupGuid), false));
        }

        private void FixHabitLookupList(Variety variety, ListActionEnum listAction)
        {
            if (variety.HabitLookupIsLoaded)
            {
                FixLazyLoadedList(variety.HabitLookup.HabitVarietyList, variety, listAction);
            }
        }

        private void SetUpVigorLookupLazyLoad(Variety variety)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            variety.SetLazyVigorLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(variety.VigorLookupGuid), false));
        }

        private void FixVigorLookupList(Variety variety, ListActionEnum listAction)
        {
            if (variety.VigorLookupIsLoaded)
            {
                FixLazyLoadedList(variety.VigorLookup.VigorVarietyList, variety, listAction);
            }
        }

        private void SetUpTimingLookupLazyLoad(Variety variety)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            variety.SetLazyTimingLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(variety.TimingLookupGuid), false));
        }

        private void FixTimingLookupList(Variety variety, ListActionEnum listAction)
        {
            if (variety.TimingLookupIsLoaded)
            {
                FixLazyLoadedList(variety.TimingLookup.TimingVarietyList, variety, listAction);
            }
        }

        private void SetUpSeriesLazyLoad(Variety variety)
        {
            var seriesTableService = Context.Get<SeriesTableService>();
            variety.SetLazySeries(new Lazy<Series>(() => seriesTableService.GetByGuid(variety.SeriesGuid), false));
        }

        private void FixSeriesList(Variety variety, ListActionEnum listAction)
        {
            if (variety.SeriesIsLoaded)
            {
                FixLazyLoadedList(variety.Series.VarietyList, variety, listAction);
            }
        }

        private void SetUpZoneLookupLazyLoad(Variety variety)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            variety.SetLazyZoneLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(variety.ZoneLookupGuid), false));
        }

        private void FixZoneLookupList(Variety variety, ListActionEnum listAction)
        {
            if (variety.ZoneLookupIsLoaded)
            {
                FixLazyLoadedList(variety.ZoneLookup.ZoneVarietyList, variety, listAction);
            }
        }

        private void SetUpBrandLookupLazyLoad(Variety variety)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            variety.SetLazyBrandLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(variety.BrandLookupGuid), false));
        }

        private void FixBrandLookupList(Variety variety, ListActionEnum listAction)
        {
            if (variety.BrandLookupIsLoaded)
            {
                FixLazyLoadedList(variety.BrandLookup.BrandVarietyList, variety, listAction);
            }
        }

        private void SetUpProductListLazyLoad(Variety variety)
        {
            var productTableService = Context.Get<ProductTableService>();
            variety.SetLazyProductList(new Lazy<List<Product>>(() => productTableService.GetAllActiveByGuid(variety.Guid, "VarietyGuid"), false));
        }

        //private void SetUpSearchVarietyHelperListLazyLoad(Variety variety)
        //{
        //    var searchVarietyHelperTableService = Context.Get<SearchVarietyHelperTableService>();
        //    variety.SetLazySearchVarietyHelperList(new Lazy<List<SearchVarietyHelper>>(() => searchVarietyHelperTableService.GetAllActiveByGuid(variety.Guid, "VarietyGuid"), false));
        //}

        #endregion
    }
}
