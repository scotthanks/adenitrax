﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using DataAccessLibrary;


namespace DataServiceLibrary
{
    
    
    public partial class OrderLineProcessTransactionProcedure : Procedure<OrderLineProcessTransactionProcedure.OrderLineProcessTransactionParameters>
    {
        public OrderLineProcessTransactionProcedure()
        {
            ProcedureName = "OrderLineProcessTransaction";
        }

        public class OrderLineProcessTransactionParameters : ProcedureParameterBase
        {
            public Guid? UserGuid { internal get; set; }
            public string UserCode { internal get; set; }
            public Guid? SupplierOrderGuid { internal get; set; }
            public Guid? ProductGuid { internal get; set; }
            public Guid? OrderLineToUpdateGuid { internal get; set; }
            public int? QtyOrdered { internal get; set; }
            public Decimal? Price { internal get; set; }
            public string OrderLineTrxStatusLookupCode { get; set; }
            public int? ActualQuantity { get; set; }
            public Guid? OrderLineGuid { get; set; }
            public string SupplierOrGrower { get; set; }

            public override void AddTo(SqlCommand command)
            {
                var userGuidParameter = new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier, 16);
                userGuidParameter.Value = UserGuid;
                command.Parameters.Add(userGuidParameter);

                CheckForParameterOverflow("@UserCode", UserCode, 56, "OrderLineProcessTransaction");
                var userCodeParameter = new SqlParameter("@UserCode", SqlDbType.NChar, 56);
                userCodeParameter.Value = UserCode;
                command.Parameters.Add(userCodeParameter);

                var supplierOrderGuidParameter = new SqlParameter("@SupplierOrderGuid", SqlDbType.UniqueIdentifier, 16);
                supplierOrderGuidParameter.Value = SupplierOrderGuid;
                command.Parameters.Add(supplierOrderGuidParameter);

                var productGuidParameter = new SqlParameter("@ProductGuid", SqlDbType.UniqueIdentifier, 16);
                productGuidParameter.Value = ProductGuid;
                command.Parameters.Add(productGuidParameter);

                var orderLineToUpdateGuidParameter = new SqlParameter("@OrderLineToUpdateGuid", SqlDbType.UniqueIdentifier, 16);
                orderLineToUpdateGuidParameter.Value = OrderLineToUpdateGuid;
                command.Parameters.Add(orderLineToUpdateGuidParameter);

                var qtyOrderedParameter = new SqlParameter("@QtyOrdered", SqlDbType.Int, 4);
                qtyOrderedParameter.Value = QtyOrdered;
                command.Parameters.Add(qtyOrderedParameter);

                var priceParameter = new SqlParameter("@Price", SqlDbType.Decimal, 5);
                priceParameter.Value = Price;
                command.Parameters.Add(priceParameter);

                var supplierOrGrowerParameter = new SqlParameter("@SupplierOrGrower", SqlDbType.NVarChar, 20);
                supplierOrGrowerParameter.Value = SupplierOrGrower;
                command.Parameters.Add(supplierOrGrowerParameter);

                CheckForParameterOverflow("@OrderLineTrxStatusLookupCode", OrderLineTrxStatusLookupCode, 20, "OrderLineProcessTransaction");
                var orderLineTrxStatusLookupCodeParameter = new SqlParameter("@OrderLineTrxStatusLookupCode", SqlDbType.NChar, 20);
                orderLineTrxStatusLookupCodeParameter.Value = OrderLineTrxStatusLookupCode;
                orderLineTrxStatusLookupCodeParameter.Direction = ParameterDirection.Output;
                command.Parameters.Add(orderLineTrxStatusLookupCodeParameter);

                var actualQuantityParameter = new SqlParameter("@ActualQuantity", SqlDbType.Int, 4);
                actualQuantityParameter.Value = ActualQuantity;
                actualQuantityParameter.Direction = ParameterDirection.Output;
                command.Parameters.Add(actualQuantityParameter);

                var orderLineGuidParameter = new SqlParameter("@OrderLineGuid", SqlDbType.UniqueIdentifier, 16);
                orderLineGuidParameter.Value = OrderLineGuid;
                orderLineGuidParameter.Direction = ParameterDirection.Output;
                command.Parameters.Add(orderLineGuidParameter);

            }

            public override void GetOutputValuesFrom(SqlCommand command)
            {
                OrderLineTrxStatusLookupCode = (string)command.Parameters["@OrderLineTrxStatusLookupCode"].Value ?? null;
                ActualQuantity = (int?)command.Parameters["@ActualQuantity"].Value ?? null;
                OrderLineGuid = (Guid?)command.Parameters["@OrderLineGuid"].Value ?? null;
            }
        };
    }


    
}
