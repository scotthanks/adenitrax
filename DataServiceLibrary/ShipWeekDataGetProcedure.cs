﻿using System;
using System.Collections.Generic;
using DataAccessLibrary;
using DataObjectLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class ShipWeekDataGetProcedure : DataServiceBaseNew
    {
        public ShipWeekDataGetProcedure(StatusObject status)
            : base(status)
        {

        }
    
        public List<DataObjectLibrary.ShipWeek> GetShipWeekData(Guid userGuid, string userCode, string beginShipWeekCode, int numberOfWeeks)
        {
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            List<DataObjectLibrary.ShipWeek> shipWeekList = null;

            try
            {

                var command = new SqlCommand("[dbo].[ShipWeekDataGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = userGuid;
                command.Parameters
                    .Add(new SqlParameter("@UserCode", SqlDbType.NVarChar, 56))
                    .Value = userCode;
                command.Parameters
                    .Add(new SqlParameter("@BeginShipWeekCode", SqlDbType.NChar, 6))
                    .Value = beginShipWeekCode;
                command.Parameters
                    .Add(new SqlParameter("@NumberOfWeeks", SqlDbType.Int, 4))
                    .Value = numberOfWeeks;

                

               var shipWeekTableService = new ShipWeekTableService();
                var reader = (SqlDataReader)command.ExecuteReader();
                shipWeekList = shipWeekTableService.GetAllFromReader(reader);


            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "CartService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {
                connection.Close();

            }
            return shipWeekList;


           
        }
    }
}
