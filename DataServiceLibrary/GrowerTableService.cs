﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public partial class GrowerTableService
    {
        public bool ExistsSimilarName(string growerName, bool excludeInactive)
        {
            return TryGetBySimilarName(growerName, excludeInactive) != null;
        }

        public Grower TryGetBySimilarName(string growerName, bool excludeInactive)
        {
            return TryGetOneFromReader(connection => GetByNameLikeReader(connection, CreateSearchString(growerName), excludeInactive));
        }

        private SqlDataReader GetByNameLikeReader(SqlConnection connection, string nameSearchString, bool excludeInactive)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.GetByFieldLikeReader(TableName, connection, "Name", nameSearchString, excludeInactive);
        }

        //TODO: Make this algorithm better by storing a copy of the grower name in the table with no spaces.
        private string CreateSearchString(string name)
        {
            var stringBuilder = new StringBuilder();

            bool suppressWildCard = true;
            foreach (char c in name)
            {
                if (char.IsLetter(c) || char.IsDigit(c))
                {
                    stringBuilder.Append(c);
                    suppressWildCard = false;
                }
                else
                {
                    if (!suppressWildCard)
                    {
                        stringBuilder.Append('%');
                        suppressWildCard = true;
                    }
                }
            }

            return stringBuilder.ToString();
        }
    }
}
