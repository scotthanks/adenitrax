﻿//using System;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using DataObjectLibrary;

//namespace DataServiceLibrary
//{
//    public partial class TripleTableService
//    {
//        public List<Triple> GetMatches(Guid? subjectGuid = null, Guid? predicateGuid = null, Guid? objectGuid = null, SqlTransaction transaction = null)
//        {
//            var sqlQueryServices = new DataAccessLibrary.SqlQueryServices();

//            var reader = sqlQueryServices.ReaderFromSqlQuery
//                (
//                    this.GetConnection(),
//                    GetMatchesQuery(subjectGuid, predicateGuid, objectGuid),
//                    transaction: transaction
//                );

//            return GetAllFromReader(reader);
//        }

//        public Triple TryGetMatch(Guid? subjectGuid = null, Guid? predicateGuid = null, Guid? objectGuid = null, SqlTransaction transaction = null)
//        {
//            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
//            {
//                connection.Open();

//                var sqlQueryServices = new DataAccessLibrary.SqlQueryServices();

//                var reader = sqlQueryServices.ReaderFromSqlQuery
//                    (
//                    //this.GetConnection(),
//                        connection,
//                        GetMatchesQuery(subjectGuid, predicateGuid, objectGuid),
//                        transaction: transaction
//                    );


//                return TryGetOneFromReader(reader);
//            }
//        }

//        private string GetMatchesQuery(Guid? subjectGuid = null, Guid? predicateGuid = null, Guid? objectGuid = null)
//        {
//            var sqlBuilder = new StringBuilder();
//            sqlBuilder.AppendFormat("SELECT {0}.* FROM {0}", this.TableName);

//            var whereClauseBuilder = new StringBuilder();
//            AddCondition(whereClauseBuilder, "SubjectGuid", subjectGuid);
//            AddCondition(whereClauseBuilder, "PredicateLookupGuid", predicateGuid);
//            AddCondition(whereClauseBuilder, "ObjectGuid", objectGuid);

//            sqlBuilder.Append(whereClauseBuilder);

//            return sqlBuilder.ToString();
//        }

//        private void AddCondition(StringBuilder whereClauseBuilder, string fieldName, Guid? guid)
//        {
//            if (guid != null && guid != Guid.Empty)
//            {
//                whereClauseBuilder.Append(whereClauseBuilder.Length == 0 ? " WHERE " : " AND ");

//                whereClauseBuilder.AppendFormat("{0}.{1} = '{2}'", this.TableName, fieldName, guid);
//            }
//        }
//    }
//}
