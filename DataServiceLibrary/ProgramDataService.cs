﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;
using System.Collections.Generic;

using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class ProgramDataService: DataServiceBaseNew
    {
        public ProgramDataService(StatusObject status)
            : base(status)
        {

        }
        public void GetLists(out List<DataObjectLibrary.ProgramType> programTypeList, out List<DataObjectLibrary.ProductFormCategory> productFormCategoryList, out List<ProductFormCategoryProgramTypeCombinationView> combinationList)
        {

            programTypeList = new List<DataObjectLibrary.ProgramType>();
            productFormCategoryList = new List<DataObjectLibrary.ProductFormCategory>();
            combinationList = new List<DataObjectLibrary.ProductFormCategoryProgramTypeCombinationView>();

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[ProgramTypeAndProductFormCategoryDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();

                var context = new Context();

                var programTypeTableService = context.Get<ProgramTypeTableService>();
                programTypeList = programTypeTableService.GetAllFromReader(reader);

                reader.NextResult();

                var productFormCategoryTableService = context.Get<ProductFormCategoryTableService>();
                productFormCategoryList = productFormCategoryTableService.GetAllFromReader(reader);

                reader.NextResult();

                var productFormCategoryProgramTypeCombinationViewService = context.Get<ProductFormCategoryProgramTypeCombinationViewService>();
                combinationList = productFormCategoryProgramTypeCombinationViewService.GetAllFromReader(reader);


                Status.Success = true;
                Status.StatusMessage = "Lists Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ProgramService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
    
            
        }
    }
}
