﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public abstract class DataService<T, TB> : DataServiceBase where T : TB where TB : class
    {
        protected delegate T TryGetFromCacheDelegate(T dataObject);

        protected delegate void AddToCacheDelegate(T dataObject);

        protected delegate void SetUpLazyLoadsDelegate(T dataObject);

        private TryGetFromCacheDelegate _tryGetFromCache = NullTryGetFromCacheDelegate;
        protected AddToCacheDelegate _addToCache = NullAddToCacheDelegate;
        private SetUpLazyLoadsDelegate _setUpLazyLoads = NullSetUpLazyLoadsDelegate;

        protected void SetUpCacheDelegates(AddToCacheDelegate addToCacheDelegate,
                                           TryGetFromCacheDelegate tryGetFromCacheDelegate)
        {
            _addToCache = addToCacheDelegate;
            _tryGetFromCache = tryGetFromCacheDelegate;
        }

        protected void SetUpLazyLoadDelegates(SetUpLazyLoadsDelegate setUpLazyLoadsDelegate)
        {
            _setUpLazyLoads = setUpLazyLoadsDelegate;
        }

        public Type DataObjectType
        {
            get { return typeof (T); }
        }

        public string TableName
        {
            get { return DataObjectType.Name; }
        }

        protected void VerifyThatTableNameHasBeenSet()
        {
            if (string.IsNullOrWhiteSpace(TableName))
            {
                throw new ApplicationException(string.Format("The TableName for {0} has not been set.",
                                                             typeof (T).ToString()));
            }
        }

        public delegate SqlDataReader GetReaderDelegate(SqlConnection connection);

        public List<T> GetAllFromReader(GetReaderDelegate getReaderDelegate)
        {
            return GetNFromReader(getReaderDelegate, long.MaxValue);
        }

        public List<T> GetAllFromReader(SqlDataReader reader)
        {
            return GetNFromReader(reader, long.MaxValue);
        }

        protected T TryGetOneFromReader(GetReaderDelegate getReaderDelegate)
        {
            var list = GetNFromReader(getReaderDelegate, 1);
            return list.FirstOrDefault();
        }

        protected T GetOneFromReader(GetReaderDelegate getReaderDelegate)
        {
            var item = TryGetOneFromReader(getReaderDelegate);
            if (item == null)
            {
                throw new ApplicationException(string.Format("No {0} found.", typeof (T).Name));
            }

            return item;
        }

        public List<T> GetNFromReader(GetReaderDelegate getReaderDelegate, long maxToRead)
        {
            List<T> results = null;
            using (var connection = ConnectionServices.ConnectionToData)
            {
                connection.Open();
                using (var reader = getReaderDelegate(connection))
                {
                    results = GetNFromReader(reader, maxToRead);
                }
                connection.Close();
            }

            return results;
        }

        public T TryGetOneFromReader(SqlDataReader reader)
        {
            var list = GetNFromReader(reader, 1);

            return list.FirstOrDefault();
        }

        public List<T> GetNFromReader(SqlDataReader reader, long maxToRead)
        {
            var results = new List<T>();
            long n = 0;
            if (reader != null && reader.HasRows)
            {
                
                while (reader.Read())
                {
                    

                    T dataObject = LoadFromSqlDataReader(reader);
                    if (!dataObject.Equals(null))
                    {
                        n++;

                        T cachedDataObject = _tryGetFromCache(dataObject);
                        if (!Equals(cachedDataObject, null))
                        {
                            //TODO: Should really compare timestamps of cached and new object. If different, copy result into cached object.
                            dataObject = cachedDataObject;
                        }
                        else
                        {
                            _addToCache(dataObject);
                            _setUpLazyLoads(dataObject);
                        }

                        results.Add(dataObject);
                        if (n >= maxToRead)
                            break;
                    }
                }
            }

            return results;
        }

        public IEnumerable<T> GetAll(int maxRowsToGet = int.MaxValue)
        {
            if (maxRowsToGet == int.MaxValue)
                return GetAllFromReader(GetAllReader);
            else
                return GetNFromReader(GetAllReader, maxRowsToGet);
        }

        private delegate void GetDataObjectDelegate();

        private delegate string GetNotFoundMessageDelegate(object searchTerm);

        public SqlDataReader GetAllReader(SqlConnection connection)
        {
            VerifyThatTableNameHasBeenSet();
            var sqlQueryServices = new SqlQueryServices();
            return sqlQueryServices.GetAllReader(TableName, connection);
        }

        public IEnumerable<T> GetEnumerableFromProcedure(string procedureName,
                                                         DataAccessLibrary.ProcedureParameterBase parameters)
        {
            VerifyThatTableNameHasBeenSet();
            var storedProcedureServices = new StoredProcedureServices();
            return
                GetAllFromReader(
                    connection => storedProcedureServices.ReaderFromSproc(connection, procedureName, parameters));
        }

        public virtual T LoadFromSqlDataReader(SqlDataReader reader)
        {
            return DefaultLoadFromSqlDataReader(reader);
        }

        protected abstract void AddParameters(SqlCommand command, T parameters);

        protected abstract T DefaultLoadFromSqlDataReader(SqlDataReader reader);

        private static void NullAddToCacheDelegate(T dataObject)
        {
        }

        private static T NullTryGetFromCacheDelegate(T dataObject)
        {
            return default(T);
        }

        private static void NullSetUpLazyLoadsDelegate(T dataObject)
        {
        }

        protected void ThrowReaderException(Exception e, string fieldName)
        {
            throw new ApplicationException(string.Format("An exception has occurred while processing the reader on field \"{0}\" ({1}).", fieldName, e.Message), e);
        }
    }
}