﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataServiceLibrary.Common
{
    public class ProcessResult
    {
        public Object ObjectProcessed { get; set; }
        public Exception Exception { get; set; }
        public string Result { get; set; }
    }
}
