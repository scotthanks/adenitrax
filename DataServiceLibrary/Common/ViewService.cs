﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public abstract class ViewService<T> : DataService<T,DataObjectBase> where T : DataObjectBase
    {
        protected override T DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            throw new NotImplementedException();
        }

        override protected void AddParameters(SqlCommand command, T parameters)
        {
            throw new NotImplementedException();
        }
    }
}