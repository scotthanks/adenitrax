﻿using System.Data.SqlClient;
using DataAccessLibrary;

namespace DataServiceLibrary
{
    public abstract class ServiceBase
    {
        private Context _context;

        internal Context Context
        {
            get { return _context ?? (_context = new Context()); }
            set { _context = value; }
        }

        public SqlConnection GetConnection()
        {
            return ConnectionServices.GetConnection();
        }
    }
}