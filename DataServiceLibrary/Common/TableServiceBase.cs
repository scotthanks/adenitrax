﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public abstract class TableServiceBase : ServiceBase
    {
        public TableObjectBase GetByForeignKeyBase(string foreignKeyName, string value)
        {
            var dataObject = TryGetByForeignKeyBase(foreignKeyName, value);

            if (dataObject == null)
            {
                throw new DataException(string.Format("An object was not found with a {0} value of \"{1}\".", foreignKeyName, value));
            }

            return dataObject;
        }

        public TableObjectBase TryGetByForeignKeyBase(string foreignKeyName, string value)
        {
            TableObjectBase dataObject;

            //TODO: Move this logic into a foreign key parser and check for table name etc.
            if (foreignKeyName.EndsWith("Code") && !foreignKeyName.EndsWith("Guid"))
            {
                throw new ArgumentException(string.Format("The foreign key name \"{0}\" does not end with \"Guid\" nor \"Code\".", foreignKeyName), "foreignKeyName");
            }

            if (foreignKeyName.EndsWith("Guid"))
            {
                Guid guid;

                bool success = Guid.TryParse(value, out guid);

                if (!success)
                {
                    throw new ArgumentException(string.Format("The value \"{0}\" is not a valid Guid.", value));
                }

                dataObject = GetByGuidBase(guid);
            }
            else
            {
                dataObject = GetByCodeBase(value);
            }

            return dataObject;
        }

        public abstract TableObjectBase GetByCodeBase(string code);
        public abstract TableObjectBase GetByGuidBase(Guid? guid);
    }
}
