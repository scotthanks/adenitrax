﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataServiceLibrary
{
    public class Context
    {
        private new readonly Dictionary<Type, ServiceBase> _serviceDictionary;

        public Context()
        {
            _serviceDictionary = new Dictionary<Type, ServiceBase>();
        }

        public void Add(ServiceBase service)
        {
            Type type = service.GetType();

            if (_serviceDictionary.ContainsKey(type))
            {
                throw new ApplicationException
                (
                    string.Format
                    (
                        "A service of type {0} has already been added to the context. Only one of each type is allowed.",
                        type
                    )
                );
            }

            _serviceDictionary.Add(type, service);
            service.Context = this;
        }

        public T TryGet<T>() where T : ServiceBase
        {
            ServiceBase service;

            if (typeof (T) == typeof (LookupTableService))
            {
                service = LookupTableService.SingletonInstance;
            }
            else
            {
                _serviceDictionary.TryGetValue(typeof(T), out service);                
            }

            return service as T;
        }

        public T Get<T>() where T : ServiceBase, new()
        {
            T service = TryGet<T>();

            if (service == null)
            {
                service = new T();
                Add(service);
            }

            return service;
        }
    }
}
