﻿using System;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public static partial class TableServices
    {
        public static DataObjectBase TryGetForeignTableObjectFrom(Type type, string fieldName, string value)
        {
            DataObjectBase foreignTableObject = null;

            if (TableObjects.IsForeignKeyOf(type, fieldName))
            {
                Type foreignKeyType = TableObjects.GetForeignKeyTypeOf(type, fieldName);
                TableServiceBase dataService = GetDataServiceFor(foreignKeyType);

                foreignTableObject = dataService.GetByCodeBase(value);
            }

            return foreignTableObject;
        }

        public static TableServiceBase GetDataServiceFor(Type type)
        {
            if (!type.IsSubclassOf(typeof (TableObjectBase)))
            {
                throw new ArgumentException(string.Format("The type {0} is not a subclass of {1}.", typeof (Type).Name,
                                                          typeof (TableObjectBase).Name));
            }

            int index = TableObjects.GetIndexOf(type);

            return (TableServiceBase) Activator.CreateInstance(TypeList[index]);
        }

        public static TableServiceBase GetDataServiceFor(TableObjects.TypeEnum dataObject)
        {
            //TODO: Some sort of check should be done to ensure that DataServiceEnum and DataObjectClassEnum are in sync.
            // Integer values of DataServiceEnum and DataObjectClassEnum correspond to each other.
            var dataService = (TypeEnum) (int) dataObject;
            return GetDataServiceFor(dataService);
        }

        public static TableServiceBase GetDataServiceFor(TypeEnum dataService)
        {
            Type dataServiceType = TypeList[(int) dataService];
            return (TableServiceBase) Activator.CreateInstance(dataServiceType);
        }

        private static TypeEnum DetermineDataServiceOfForeignKey(string foreignKeyFieldName)
        {
            int lowestIndex = int.MaxValue;
            TypeEnum? foreignKeyDataService = null;

            foreach (string tableName in TableObjects.TypeNameList)
            {
                string codeName = tableName + "Code";

                if (foreignKeyFieldName.EndsWith(codeName))
                {
                    int index = foreignKeyFieldName.IndexOf(codeName, StringComparison.Ordinal);
                    if (index < lowestIndex)
                    {
                        lowestIndex = Math.Min(lowestIndex, index);
                        string dataServiceName = tableName + "DataService";
                        TypeEnum dataService;
                        Enum.TryParse(dataServiceName, false, out dataService);
                        foreignKeyDataService = dataService;
                    }
                }
            }

            if (foreignKeyDataService == null)
            {
                throw new ApplicationException(string.Format("No foreign table found for the field \"{0}\".",
                                                             foreignKeyFieldName));
            }

            return (TypeEnum) foreignKeyDataService;
        }
    }
}