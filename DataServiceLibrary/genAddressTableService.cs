﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class AddressTableService : TableService<Address>
    {
        public AddressTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override Address DefaultInsert(Address address, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [Address]([Guid], [DateDeactivated], [Name], [StreetAddress1], [StreetAddress2], [City], [StateGuid], [ZipCode], ) VALUES (@Guid, @DateDeactivated, @Name, @StreetAddress1, @StreetAddress2, @City, @StateGuid, @ZipCode, );SELECT * FROM [Address] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, address, transaction: transaction);
        }

        protected override Address DefaultUpdate(Address address, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [Address] SET [DateDeactivated]=@DateDeactivated, [Name]=@Name, [StreetAddress1]=@StreetAddress1, [StreetAddress2]=@StreetAddress2, [City]=@City, [StateGuid]=@StateGuid, [ZipCode]=@ZipCode, WHERE [Guid]=@Guid;SELECT * FROM [Address] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, address, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, Address address)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = address.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = address.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = address.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            nameParameter.IsNullable = false;
            address.Name = address.Name ?? "";
            address.Name = address.Name.Trim();
            nameParameter.Value = address.Name;
            command.Parameters.Add(nameParameter);

            var streetAddress1Parameter = new SqlParameter("@StreetAddress1", SqlDbType.NVarChar, 50);
            streetAddress1Parameter.IsNullable = false;
            address.StreetAddress1 = address.StreetAddress1 ?? "";
            address.StreetAddress1 = address.StreetAddress1.Trim();
            streetAddress1Parameter.Value = address.StreetAddress1;
            command.Parameters.Add(streetAddress1Parameter);

            var streetAddress2Parameter = new SqlParameter("@StreetAddress2", SqlDbType.NVarChar, 50);
            streetAddress2Parameter.IsNullable = false;
            address.StreetAddress2 = address.StreetAddress2 ?? "";
            address.StreetAddress2 = address.StreetAddress2.Trim();
            streetAddress2Parameter.Value = address.StreetAddress2;
            command.Parameters.Add(streetAddress2Parameter);

            var cityParameter = new SqlParameter("@City", SqlDbType.NVarChar, 50);
            cityParameter.IsNullable = false;
            address.City = address.City ?? "";
            address.City = address.City.Trim();
            cityParameter.Value = address.City;
            command.Parameters.Add(cityParameter);

            var stateGuidParameter = new SqlParameter("@StateGuid", SqlDbType.UniqueIdentifier, 16);
            stateGuidParameter.IsNullable = false;
            stateGuidParameter.Value = address.StateGuid;
            command.Parameters.Add(stateGuidParameter);

            var zipCodeParameter = new SqlParameter("@ZipCode", SqlDbType.NChar, 12);
            zipCodeParameter.IsNullable = false;
            address.ZipCode = address.ZipCode ?? "";
            address.ZipCode = address.ZipCode.Trim();
            zipCodeParameter.Value = address.ZipCode;
            command.Parameters.Add(zipCodeParameter);

        }

        protected override Address DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var address = new Address();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)Address.ColumnEnum.Id] ?? long.MinValue;
                address.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)Address.ColumnEnum.Guid] ?? Guid.Empty;
                address.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)Address.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                address.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "Name";
                var name = reader[(int)Address.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                address.Name = (string)name;

                columnName = "StreetAddress1";
                var streetAddress1 = reader[(int)Address.ColumnEnum.StreetAddress1] ?? string.Empty;
                streetAddress1 = TrimString(streetAddress1);
                address.StreetAddress1 = (string)streetAddress1;

                columnName = "StreetAddress2";
                var streetAddress2 = reader[(int)Address.ColumnEnum.StreetAddress2] ?? string.Empty;
                streetAddress2 = TrimString(streetAddress2);
                address.StreetAddress2 = (string)streetAddress2;

                columnName = "City";
                var city = reader[(int)Address.ColumnEnum.City] ?? string.Empty;
                city = TrimString(city);
                address.City = (string)city;

                columnName = "StateGuid";
                var stateGuid = reader[(int)Address.ColumnEnum.StateGuid] ?? Guid.Empty;
                address.StateGuid = (Guid)stateGuid;

                columnName = "ZipCode";
                var zipCode = reader[(int)Address.ColumnEnum.ZipCode] ?? string.Empty;
                zipCode = TrimString(zipCode);
                address.ZipCode = (string)zipCode;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return address;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Address address)
        {
            SetUpStateLazyLoad(address);
            SetUpGrowerListLazyLoad(address);
            SetUpGrowerAddressListLazyLoad(address);
            SetUpShipToGrowerOrderListLazyLoad(address);
            SetUpSupplierListLazyLoad(address);
        }

        protected override void FixAnyLazyLoadedLists(Address address, ListActionEnum listAction)
        {
            FixStateList(address, listAction);
        }

        private void SetUpStateLazyLoad(Address address)
        {
            var stateTableService = Context.Get<StateTableService>();
            address.SetLazyState(new Lazy<State>(() => stateTableService.GetByGuid(address.StateGuid), false));
        }

        private void FixStateList(Address address, ListActionEnum listAction)
        {
            if (address.StateIsLoaded)
            {
                FixLazyLoadedList(address.State.AddressList, address, listAction);
            }
        }

        private void SetUpGrowerListLazyLoad(Address address)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            address.SetLazyGrowerList(new Lazy<List<Grower>>(() => growerTableService.GetAllActiveByGuid(address.Guid, "AddressGuid"), false));
        }

        private void SetUpGrowerAddressListLazyLoad(Address address)
        {
            var growerAddressTableService = Context.Get<GrowerAddressTableService>();
            address.SetLazyGrowerAddressList(new Lazy<List<GrowerAddress>>(() => growerAddressTableService.GetAllActiveByGuid(address.Guid, "AddressGuid"), false));
        }

        private void SetUpShipToGrowerOrderListLazyLoad(Address address)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            address.SetLazyShipToGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(address.Guid, "ShipToAddressGuid"), false));
        }

        private void SetUpSupplierListLazyLoad(Address address)
        {
            var supplierTableService = Context.Get<SupplierTableService>();
            address.SetLazySupplierList(new Lazy<List<Supplier>>(() => supplierTableService.GetAllActiveByGuid(address.Guid, "AddressGuid"), false));
        }

        #endregion
    }
}
