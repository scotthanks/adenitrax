﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class ImageFileTableService : TableService<ImageFile>
    {
        public ImageFileTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override ImageFile DefaultInsert(ImageFile imageFile, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [ImageFile]([Guid], [Code], [Name], [FileExtensionLookupGuid], [ImageFileTypeLookupGuid], [Height], [Width], [Size], [Notes], [ImageSetGuid], [DateDeactivated], ) VALUES (@Guid, @Code, @Name, @FileExtensionLookupGuid, @ImageFileTypeLookupGuid, @Height, @Width, @Size, @Notes, @ImageSetGuid, @DateDeactivated, );SELECT * FROM [ImageFile] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, imageFile, transaction: transaction);
        }

        protected override ImageFile DefaultUpdate(ImageFile imageFile, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [ImageFile] SET [Code]=@Code, [Name]=@Name, [FileExtensionLookupGuid]=@FileExtensionLookupGuid, [ImageFileTypeLookupGuid]=@ImageFileTypeLookupGuid, [Height]=@Height, [Width]=@Width, [Size]=@Size, [Notes]=@Notes, [ImageSetGuid]=@ImageSetGuid, [DateDeactivated]=@DateDeactivated, WHERE [Guid]=@Guid;SELECT * FROM [ImageFile] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, imageFile, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, ImageFile imageFile)
        {
            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = imageFile.Guid;
            command.Parameters.Add(guidParameter);

            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = imageFile.Id;
            command.Parameters.Add(idParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NChar, 50);
            codeParameter.IsNullable = false;
            imageFile.Code = imageFile.Code ?? "";
            imageFile.Code = imageFile.Code.Trim();
            codeParameter.Value = imageFile.Code;
            command.Parameters.Add(codeParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
            nameParameter.IsNullable = false;
            imageFile.Name = imageFile.Name ?? "";
            imageFile.Name = imageFile.Name.Trim();
            nameParameter.Value = imageFile.Name;
            command.Parameters.Add(nameParameter);

            var fileExtensionLookupGuidParameter = new SqlParameter("@FileExtensionLookupGuid", SqlDbType.UniqueIdentifier, 16);
            fileExtensionLookupGuidParameter.IsNullable = false;
            fileExtensionLookupGuidParameter.Value = imageFile.FileExtensionLookupGuid;
            command.Parameters.Add(fileExtensionLookupGuidParameter);

            var imageFileTypeLookupGuidParameter = new SqlParameter("@ImageFileTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            imageFileTypeLookupGuidParameter.IsNullable = false;
            imageFileTypeLookupGuidParameter.Value = imageFile.ImageFileTypeLookupGuid;
            command.Parameters.Add(imageFileTypeLookupGuidParameter);

            var heightParameter = new SqlParameter("@Height", SqlDbType.Int, 4);
            heightParameter.IsNullable = false;
            heightParameter.Value = imageFile.Height;
            command.Parameters.Add(heightParameter);

            var widthParameter = new SqlParameter("@Width", SqlDbType.Int, 4);
            widthParameter.IsNullable = false;
            widthParameter.Value = imageFile.Width;
            command.Parameters.Add(widthParameter);

            var sizeParameter = new SqlParameter("@Size", SqlDbType.BigInt, 8);
            sizeParameter.IsNullable = false;
            sizeParameter.Value = imageFile.Size;
            command.Parameters.Add(sizeParameter);

            var notesParameter = new SqlParameter("@Notes", SqlDbType.NVarChar, 500);
            notesParameter.IsNullable = false;
            imageFile.Notes = imageFile.Notes ?? "";
            imageFile.Notes = imageFile.Notes.Trim();
            notesParameter.Value = imageFile.Notes;
            command.Parameters.Add(notesParameter);

            var imageSetGuidParameter = new SqlParameter("@ImageSetGuid", SqlDbType.UniqueIdentifier, 16);
            imageSetGuidParameter.IsNullable = false;
            imageSetGuidParameter.Value = imageFile.ImageSetGuid;
            command.Parameters.Add(imageSetGuidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = imageFile.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

        }

        protected override ImageFile DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var imageFile = new ImageFile();

            string columnName = "";
            try
            {
                columnName = "Guid";
                var guid = reader[(int)ImageFile.ColumnEnum.Guid] ?? Guid.Empty;
                imageFile.Guid = (Guid)guid;

                columnName = "Id";
                var id = reader[(int)ImageFile.ColumnEnum.Id] ?? long.MinValue;
                imageFile.Id = (long)id;

                columnName = "Code";
                var code = reader[(int)ImageFile.ColumnEnum.Code] ?? string.Empty;
                code = TrimString(code);
                imageFile.Code = (string)code;

                columnName = "Name";
                var name = reader[(int)ImageFile.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                imageFile.Name = (string)name;

                columnName = "FileExtensionLookupGuid";
                var fileExtensionLookupGuid = reader[(int)ImageFile.ColumnEnum.FileExtensionLookupGuid] ?? Guid.Empty;
                imageFile.FileExtensionLookupGuid = (Guid)fileExtensionLookupGuid;

                columnName = "ImageFileTypeLookupGuid";
                var imageFileTypeLookupGuid = reader[(int)ImageFile.ColumnEnum.ImageFileTypeLookupGuid] ?? Guid.Empty;
                imageFile.ImageFileTypeLookupGuid = (Guid)imageFileTypeLookupGuid;

                columnName = "Height";
                var height = reader[(int)ImageFile.ColumnEnum.Height] ?? int.MinValue;
                imageFile.Height = (int)height;

                columnName = "Width";
                var width = reader[(int)ImageFile.ColumnEnum.Width] ?? int.MinValue;
                imageFile.Width = (int)width;

                columnName = "Size";
                var size = reader[(int)ImageFile.ColumnEnum.Size] ?? long.MinValue;
                imageFile.Size = (long)size;

                columnName = "Notes";
                var notes = reader[(int)ImageFile.ColumnEnum.Notes] ?? string.Empty;
                notes = TrimString(notes);
                imageFile.Notes = (string)notes;

                columnName = "ImageSetGuid";
                var imageSetGuid = reader[(int)ImageFile.ColumnEnum.ImageSetGuid] ?? Guid.Empty;
                imageFile.ImageSetGuid = (Guid)imageSetGuid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)ImageFile.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                imageFile.DateDeactivated = (DateTime?)dateDeactivated;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return imageFile;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ImageFile imageFile)
        {
            SetUpFileExtensionLookupLazyLoad(imageFile);
            SetUpImageFileTypeLookupLazyLoad(imageFile);
            SetUpImageSetLazyLoad(imageFile);
            SetUpOriginalImageSetListLazyLoad(imageFile);
        }

        protected override void FixAnyLazyLoadedLists(ImageFile imageFile, ListActionEnum listAction)
        {
            FixFileExtensionLookupList(imageFile, listAction);
            FixImageFileTypeLookupList(imageFile, listAction);
            FixImageSetList(imageFile, listAction);
        }

        private void SetUpFileExtensionLookupLazyLoad(ImageFile imageFile)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            imageFile.SetLazyFileExtensionLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(imageFile.FileExtensionLookupGuid), false));
        }

        private void FixFileExtensionLookupList(ImageFile imageFile, ListActionEnum listAction)
        {
            if (imageFile.FileExtensionLookupIsLoaded)
            {
                FixLazyLoadedList(imageFile.FileExtensionLookup.FileExtensionImageFileList, imageFile, listAction);
            }
        }

        private void SetUpImageFileTypeLookupLazyLoad(ImageFile imageFile)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            imageFile.SetLazyImageFileTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(imageFile.ImageFileTypeLookupGuid), false));
        }

        private void FixImageFileTypeLookupList(ImageFile imageFile, ListActionEnum listAction)
        {
            if (imageFile.ImageFileTypeLookupIsLoaded)
            {
                FixLazyLoadedList(imageFile.ImageFileTypeLookup.ImageFileTypeImageFileList, imageFile, listAction);
            }
        }

        private void SetUpImageSetLazyLoad(ImageFile imageFile)
        {
            var imageSetTableService = Context.Get<ImageSetTableService>();
            imageFile.SetLazyImageSet(new Lazy<ImageSet>(() => imageSetTableService.GetByGuid(imageFile.ImageSetGuid), false));
        }

        private void FixImageSetList(ImageFile imageFile, ListActionEnum listAction)
        {
            if (imageFile.ImageSetIsLoaded)
            {
                FixLazyLoadedList(imageFile.ImageSet.ImageFileList, imageFile, listAction);
            }
        }

        private void SetUpOriginalImageSetListLazyLoad(ImageFile imageFile)
        {
            var imageSetTableService = Context.Get<ImageSetTableService>();
            imageFile.SetLazyOriginalImageSetList(new Lazy<List<ImageSet>>(() => imageSetTableService.GetAllActiveByGuid(imageFile.Guid, "OriginalImageFileGuid"), false));
        }

        #endregion
    }
}
