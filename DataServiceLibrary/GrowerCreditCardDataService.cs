﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using DataAccessLibrary;
using DataObjectLibrary;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public class GrowerCreditCardDataService : DataServiceBaseNew
    {
        public GrowerCreditCardDataService(StatusObject status)
            : base(status)
        {

        }
             

        public bool UpdateCard(string userCode, Guid creditCardGuid,string cardDescription, DateTime dateDeactivated)
        {
           
            bool bRet = false;
            try
            {
                var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
                var command = new SqlCommand("[dbo].[GrowerCreditCardUpdate]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerCreditCardGuid",  SqlDbType.UniqueIdentifier))
                    .Value = creditCardGuid;
                command.Parameters
                    .Add(new SqlParameter("@CardDescription", SqlDbType.NVarChar, 100))
                    .Value = cardDescription;
                command.Parameters
                    .Add(new SqlParameter("@DateDeactivated", SqlDbType.DateTime))
                    .Value = dateDeactivated;
               

                connection.Open();
                var response = (int)command.ExecuteNonQuery();
                bRet = true;
            }
            catch (Exception ex) 
            {
                var errormsg = ex.Message;
               
            }
            return bRet;
        }


    }
}
