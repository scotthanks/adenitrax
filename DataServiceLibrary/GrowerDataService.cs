﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using Utility;
using DataObjectLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class GrowerDataService: DataServiceBaseNew
    {
        public GrowerDataService(StatusObject status)
            : base(status)
        {

        }
        public class ReturnData
        {
            //public List<GrowerVolumeMax> GrowerVolumeList { get; set; }
            public List<BusinessObjectsLibrary.GrowerVolume> GrowerVolumeList { get; set; }
        }

        public ReturnData GetVolumesForGrower(String supplierCodeList)
        {
            return GetVolumes(supplierCodeList);
        }

        private ReturnData GetVolumes(String supplierCodeList)
        {
            var returnData = new ReturnData();
            var growerVolumeList = new List<BusinessObjectsLibrary.GrowerVolume>();


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerVolumeDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = Guid.Empty;
                command.Parameters
                    .Add(new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier))
                    .Value = Guid.Empty;
                command.Parameters
                    .Add(new SqlParameter("@SupplierCodeList", SqlDbType.NVarChar, 3000))
                    .Value = supplierCodeList;


                var reader = command.ExecuteReader();
                var context = new Context();
                       
                while (reader.Read())
                {

                    var growerVolume = new BusinessObjectsLibrary.GrowerVolume();
                    growerVolume.SupplierGuid = reader.GetGuid(0);
                    growerVolume.SupplierCode = reader.GetString(1);
                    growerVolume.SupplierName = reader.GetString(2);
                    growerVolume.LevelNumber = reader.GetInt32(3);
                    growerVolume.RangeLow = reader.GetInt32(4);
                    growerVolume.RangeHigh = reader.GetInt32(5);
                    growerVolume.RangeType = reader.GetString(6);
                    growerVolume.CurrentLevelType = reader.GetString(7);

                    growerVolumeList.Add(growerVolume);
                            
                }
                      
                reader.Close();

                Status.Success = true;
                Status.StatusMessage = "Volumes Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            returnData.GrowerVolumeList = growerVolumeList;
            return returnData;
        }
        public List<Seller> GetSellers(string email)
        {

            var list = new List<Seller>();


            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerSellersGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@Email", SqlDbType.NVarChar,70))
                    .Value = email;
                


                var reader = command.ExecuteReader();
                var context = new Context();

                while (reader.Read())
                {

                    var item = new Seller();
                    item.SellerGuid = reader.GetGuid(0);
                    item.Code = reader.GetString(1);
                    item.Name = reader.GetString(2);
                    item.IsDefault = reader.GetBoolean(3);

                    list.Add(item);

                }

                reader.Close();

                Status.Success = true;
                Status.StatusMessage = "Sellers Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }


            
            return list;
        }
        public bool SetDefaultCreditCard(Guid growerGuid, Guid creditCardGuid)
        {
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerSetDefaultCreditCard]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerCreditCardGuid", SqlDbType.UniqueIdentifier))
                    .Value = creditCardGuid;
               


                var reader = command.ExecuteReader();
                var context = new Context();

                reader.Close();

                Status.Success = true;
                Status.StatusMessage = "Default Credit Card Updated";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }




            return Status.Success;
        }

        public bool Exists(string growerName, string zipCode, decimal phoneNumberAreaCode, decimal phoneNumber)
        {
            var growerCount = 0;
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerSearch]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerNameSearchString", SqlDbType.NVarChar, 50))
                    .Value = ConvertToSearchString(growerName);
                command.Parameters
                    .Add(new SqlParameter("@ZipCode", SqlDbType.NChar, 5))
                    .Value = zipCode;
                command.Parameters
                    .Add(new SqlParameter("@PhoneNumberAreaCode", SqlDbType.Decimal, 5))
                    .Value = phoneNumberAreaCode;
                command.Parameters
                    .Add(new SqlParameter("@PhoneNumber", SqlDbType.Decimal, 5))
                    .Value = phoneNumber;
                command.Parameters
                    .Add(new SqlParameter("@IncludeOnlyActivated", SqlDbType.Bit, 1))
                    .Value = true;
                command.Parameters
                    .Add(new SqlParameter("@ReturnRecordsets", SqlDbType.Bit, 1))
                    .Value = false;
                command.Parameters
                    .Add(new SqlParameter("@GrowerCount", SqlDbType.Int, 4))
                    .Direction = ParameterDirection.Output;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Direction = ParameterDirection.Output;
                
                

                var reader = command.ExecuteReader();
                var context = new Context();
                reader.Close();

                growerCount = (int)command.Parameters["@GrowerCount"].Value;
                

                Status.Success = true;
                Status.StatusMessage = "Search Complete";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }



            return growerCount > 0;
        }

        
        public DataObjectLibrary.Grower GetGrowerDataFromUserGuid(Guid userGuid)
        {
            if (userGuid == Guid.Empty)
                throw new ArgumentNullException("userGuid");

            return GetGrowerData(growerGuid: Guid.Empty, personGuid: Guid.Empty);
        }

        public DataObjectLibrary.Grower GetGrowerDataFromPersonGuid(Guid personGuid)
        {
            if (personGuid == Guid.Empty)
                throw new ArgumentNullException("personGuid");

            return GetGrowerData(growerGuid: Guid.Empty, personGuid: personGuid);
        }

        public DataObjectLibrary.Grower GetGrowerDataFromGrowerGuid(Guid growerGuid)
        {
            if (growerGuid == Guid.Empty)
                throw new ArgumentNullException("growerGuid");

            return GetGrowerData(growerGuid: growerGuid, personGuid: Guid.Empty);
        }

        private DataObjectLibrary.Grower GetGrowerData(Guid growerGuid, Guid personGuid)
        {
            DataObjectLibrary.Grower grower = null;


            
            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                if (personGuid != Guid.Empty)
                {  
                    command.Parameters
                        .Add(new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier))
                        .Value = personGuid;
                }
                if (growerGuid != Guid.Empty)
                {
                    command.Parameters
                       .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                       .Value = growerGuid;
                }

                var reader = command.ExecuteReader();
                var context = new Context();

                var growerTableService = context.Get<GrowerTableService>();
                var growerList = growerTableService.GetAllFromReader(reader);
                if (growerList.Count == 1)
                {
                    grower = growerList[0];

                    reader.NextResult();

                    var growerAddressTableService = context.Get<GrowerAddressTableService>();
                    var growerAddressList = growerAddressTableService.GetAllFromReader(reader);
                    grower.SetLazyGrowerAddressList(new Lazy<List<GrowerAddress>>(() => growerAddressList));

                    reader.NextResult();

                    var addressTableService = context.Get<AddressTableService>();
                    addressTableService.GetAllFromReader(reader);

                    reader.NextResult();

                    var growerCreditCardTableService = context.Get<GrowerCreditCardTableService>();
                    var growerCreditCardList = growerCreditCardTableService.GetAllFromReader(reader);
                    grower.SetLazyGrowerCreditCardList(new Lazy<List<GrowerCreditCard>>(() => growerCreditCardList));
                }

                reader.Close();

                Status.Success = true;
                Status.StatusMessage = "Grower Data Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            
      

            return grower;
        }

        private string ConvertToSearchString(string s)
        {
            var stringBuilder = new StringBuilder();

            foreach (var c in s.Trim())
            {
                if (char.IsDigit(c) || char.IsLetter(c))
                {
                    stringBuilder.Append(c);
                }
                else
                {
                    stringBuilder.Append('%');
                }
            }

            return stringBuilder.ToString();
        }
    }
}
