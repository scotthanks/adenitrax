﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class GeneticOwnerTableService : TableService<GeneticOwner>
    {
        public GeneticOwnerTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override GeneticOwner DefaultInsert(GeneticOwner geneticOwner, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [GeneticOwner]([Guid], [DateDeactivated], [Name], [Code], ) VALUES (@Guid, @DateDeactivated, @Name, @Code, );SELECT * FROM [GeneticOwner] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, geneticOwner, transaction: transaction);
        }

        protected override GeneticOwner DefaultUpdate(GeneticOwner geneticOwner, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [GeneticOwner] SET [DateDeactivated]=@DateDeactivated, [Name]=@Name, [Code]=@Code, WHERE [Guid]=@Guid;SELECT * FROM [GeneticOwner] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, geneticOwner, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, GeneticOwner geneticOwner)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = geneticOwner.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = geneticOwner.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = geneticOwner.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            nameParameter.IsNullable = false;
            geneticOwner.Name = geneticOwner.Name ?? "";
            geneticOwner.Name = geneticOwner.Name.Trim();
            nameParameter.Value = geneticOwner.Name;
            command.Parameters.Add(nameParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NChar, 10);
            codeParameter.IsNullable = false;
            geneticOwner.Code = geneticOwner.Code ?? "";
            geneticOwner.Code = geneticOwner.Code.Trim();
            codeParameter.Value = geneticOwner.Code;
            command.Parameters.Add(codeParameter);

        }

        protected override GeneticOwner DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var geneticOwner = new GeneticOwner();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)GeneticOwner.ColumnEnum.Id] ?? long.MinValue;
                geneticOwner.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)GeneticOwner.ColumnEnum.Guid] ?? Guid.Empty;
                geneticOwner.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)GeneticOwner.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                geneticOwner.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "Name";
                var name = reader[(int)GeneticOwner.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                geneticOwner.Name = (string)name;

                columnName = "Code";
                var code = reader[(int)GeneticOwner.ColumnEnum.Code] ?? string.Empty;
                code = TrimString(code);
                geneticOwner.Code = (string)code;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return geneticOwner;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(GeneticOwner geneticOwner)
        {
            SetUpVarietyListLazyLoad(geneticOwner);
        }

        protected override void FixAnyLazyLoadedLists(GeneticOwner geneticOwner, ListActionEnum listAction)
        {
        }

        private void SetUpVarietyListLazyLoad(GeneticOwner geneticOwner)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            geneticOwner.SetLazyVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(geneticOwner.Guid, "GeneticOwnerGuid"), false));
        }

        #endregion
    }
}
