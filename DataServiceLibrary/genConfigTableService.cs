﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class ConfigTableService : TableService<Config>
    {
        public ConfigTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override Config DefaultInsert(Config config, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [Config]([Guid], [Code], [Value], [DateDeactivated], ) VALUES (@Guid, @Code, @Value, @DateDeactivated, );SELECT * FROM [Config] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, config, transaction: transaction);
        }

        protected override Config DefaultUpdate(Config config, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [Config] SET [Code]=@Code, [Value]=@Value, [DateDeactivated]=@DateDeactivated, WHERE [Guid]=@Guid;SELECT * FROM [Config] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, config, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, Config config)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = config.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = config.Guid;
            command.Parameters.Add(guidParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NVarChar, 30);
            codeParameter.IsNullable = false;
            config.Code = config.Code ?? "";
            config.Code = config.Code.Trim();
            codeParameter.Value = config.Code;
            command.Parameters.Add(codeParameter);

            var valueParameter = new SqlParameter("@Value", SqlDbType.NVarChar, 200);
            valueParameter.IsNullable = false;
            config.Value = config.Value ?? "";
            config.Value = config.Value.Trim();
            valueParameter.Value = config.Value;
            command.Parameters.Add(valueParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = config.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

        }

        protected override Config DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var config = new Config();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)Config.ColumnEnum.Id] ?? long.MinValue;
                config.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)Config.ColumnEnum.Guid] ?? Guid.Empty;
                config.Guid = (Guid)guid;

                columnName = "Code";
                var code = reader[(int)Config.ColumnEnum.Code] ?? string.Empty;
                code = TrimString(code);
                config.Code = (string)code;

                columnName = "Value";
                var value = reader[(int)Config.ColumnEnum.Value] ?? string.Empty;
                value = TrimString(value);
                config.Value = (string)value;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)Config.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                config.DateDeactivated = (DateTime?)dateDeactivated;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return config;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Config config)
        {
        }

        protected override void FixAnyLazyLoadedLists(Config config, ListActionEnum listAction)
        {
        }

        #endregion
    }
}
