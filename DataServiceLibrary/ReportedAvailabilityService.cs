﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using DataObjectLibrary;

namespace DataServiceLibrary
{
    public partial class ReportedAvailabilityTableService
    {
        public ReportedAvailability TryGetByProductAndShipWeek(Guid productGuid, Guid shipWeekGuid)
        {
            var sqlQueryServices = new SqlQueryServices();
            string getAllQuery = sqlQueryServices.GetAllQuery(this.TableName);

            string query = string.Format("{0} WHERE {1}=@{1} AND {2}=@{2}", getAllQuery, ReportedAvailability.ColumnEnum.ProductGuid.ToString(), ReportedAvailability.ColumnEnum.ShipWeekGuid.ToString());

            var reportedAvailability = new ReportedAvailability()
                {
                    ProductGuid = productGuid,
                    ShipWeekGuid = shipWeekGuid,
                    //TODO: This is necessary to prevent an error when parameters are loaded. Fix it some other way.
                    DateReported = DateTime.Now
                };

            return TryGetOneFromCommand(query, AddParameters, reportedAvailability);
        }
    }
}
