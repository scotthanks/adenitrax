﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using DataAccessLibrary;
using DataObjectLibrary;
using BusinessObjectsLibrary;
using BusinessObjectsLibrary.Catalog;


namespace DataServiceLibrary
{
    public class SellerDataService : DataServiceBaseNew
    {

        public SellerDataService(StatusObject status)
            : base(status)
        {

        }
        public string GetSellerForUser()
        {
            var seller = "EPS";
            try
            {
                using (var connection = ConnectionServices.ConnectionToData)
                {
                    using (var command = new SqlCommand("[dbo].[SellerGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                            .Value = Status.User.UserGuid;
                        
                        connection.Open();
                       
                        var reader = command.ExecuteReader();
                        var context = new Context();

                        while (reader.Read())
                        {

                            seller = reader.GetString(0);
                            

                        }

                        reader.Close();




                    }
                }
               
                Status.StatusMessage = "got seller";
                Status.Success = true;

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "SellerDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {


            }

            return seller;
            
            
            
            
            
            
           
 
            
        }
    }
}
