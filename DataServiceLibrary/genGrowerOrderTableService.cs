﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class GrowerOrderTableService : TableService<GrowerOrder>
    {
        public GrowerOrderTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override GrowerOrder DefaultInsert(GrowerOrder growerOrder, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [GrowerOrder]([Guid], [DateDeactivated], [GrowerGuid], [ShipWeekGuid], [OrderNo], [Description], [CustomerPoNo], [OrderTypeLookupGuid], [ProductFormCategoryGuid], [ProgramTypeGuid], [ShipToAddressGuid], [PaymentTypeLookupGuid], [GrowerOrderStatusLookupGuid], [DateEntered], [GrowerCreditCardGuid], [PersonGuid], [PromotionalCode], ) VALUES (@Guid, @DateDeactivated, @GrowerGuid, @ShipWeekGuid, @OrderNo, @Description, @CustomerPoNo, @OrderTypeLookupGuid, @ProductFormCategoryGuid, @ProgramTypeGuid, @ShipToAddressGuid, @PaymentTypeLookupGuid, @GrowerOrderStatusLookupGuid, @DateEntered, @GrowerCreditCardGuid, @PersonGuid, @PromotionalCode, );SELECT * FROM [GrowerOrder] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, growerOrder, transaction: transaction);
        }

        protected override GrowerOrder DefaultUpdate(GrowerOrder growerOrder, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [GrowerOrder] SET [DateDeactivated]=@DateDeactivated, [GrowerGuid]=@GrowerGuid, [ShipWeekGuid]=@ShipWeekGuid, [OrderNo]=@OrderNo, [Description]=@Description, [CustomerPoNo]=@CustomerPoNo, [OrderTypeLookupGuid]=@OrderTypeLookupGuid, [ProductFormCategoryGuid]=@ProductFormCategoryGuid, [ProgramTypeGuid]=@ProgramTypeGuid, [ShipToAddressGuid]=@ShipToAddressGuid, [PaymentTypeLookupGuid]=@PaymentTypeLookupGuid, [GrowerOrderStatusLookupGuid]=@GrowerOrderStatusLookupGuid, [DateEntered]=@DateEntered, [GrowerCreditCardGuid]=@GrowerCreditCardGuid, [PersonGuid]=@PersonGuid, [PromotionalCode]=@PromotionalCode, WHERE [Guid]=@Guid;SELECT * FROM [GrowerOrder] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, growerOrder, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, GrowerOrder growerOrder)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = growerOrder.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = growerOrder.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = growerOrder.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var growerGuidParameter = new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier, 16);
            growerGuidParameter.IsNullable = false;
            growerGuidParameter.Value = growerOrder.GrowerGuid;
            command.Parameters.Add(growerGuidParameter);

            var shipWeekGuidParameter = new SqlParameter("@ShipWeekGuid", SqlDbType.UniqueIdentifier, 16);
            shipWeekGuidParameter.IsNullable = false;
            shipWeekGuidParameter.Value = growerOrder.ShipWeekGuid;
            command.Parameters.Add(shipWeekGuidParameter);

            var orderNoParameter = new SqlParameter("@OrderNo", SqlDbType.NChar, 10);
            orderNoParameter.IsNullable = false;
            growerOrder.OrderNo = growerOrder.OrderNo ?? "";
            growerOrder.OrderNo = growerOrder.OrderNo.Trim();
            orderNoParameter.Value = growerOrder.OrderNo;
            command.Parameters.Add(orderNoParameter);

            var descriptionParameter = new SqlParameter("@Description", SqlDbType.NVarChar, 50);
            descriptionParameter.IsNullable = false;
            growerOrder.Description = growerOrder.Description ?? "";
            growerOrder.Description = growerOrder.Description.Trim();
            descriptionParameter.Value = growerOrder.Description;
            command.Parameters.Add(descriptionParameter);

            var customerPoNoParameter = new SqlParameter("@CustomerPoNo", SqlDbType.NChar, 20);
            customerPoNoParameter.IsNullable = false;
            growerOrder.CustomerPoNo = growerOrder.CustomerPoNo ?? "";
            growerOrder.CustomerPoNo = growerOrder.CustomerPoNo.Trim();
            customerPoNoParameter.Value = growerOrder.CustomerPoNo;
            command.Parameters.Add(customerPoNoParameter);

            var orderTypeLookupGuidParameter = new SqlParameter("@OrderTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            orderTypeLookupGuidParameter.IsNullable = false;
            orderTypeLookupGuidParameter.Value = growerOrder.OrderTypeLookupGuid;
            command.Parameters.Add(orderTypeLookupGuidParameter);

            var productFormCategoryGuidParameter = new SqlParameter("@ProductFormCategoryGuid", SqlDbType.UniqueIdentifier, 16);
            productFormCategoryGuidParameter.IsNullable = false;
            productFormCategoryGuidParameter.Value = growerOrder.ProductFormCategoryGuid;
            command.Parameters.Add(productFormCategoryGuidParameter);

            var programTypeGuidParameter = new SqlParameter("@ProgramTypeGuid", SqlDbType.UniqueIdentifier, 16);
            programTypeGuidParameter.IsNullable = false;
            programTypeGuidParameter.Value = growerOrder.ProgramTypeGuid;
            command.Parameters.Add(programTypeGuidParameter);

            var shipToAddressGuidParameter = new SqlParameter("@ShipToAddressGuid", SqlDbType.UniqueIdentifier, 16);
            shipToAddressGuidParameter.IsNullable = true;
            shipToAddressGuidParameter.Value = growerOrder.ShipToAddressGuid ?? (object)DBNull.Value;
            command.Parameters.Add(shipToAddressGuidParameter);

            var paymentTypeLookupGuidParameter = new SqlParameter("@PaymentTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            paymentTypeLookupGuidParameter.IsNullable = false;
            paymentTypeLookupGuidParameter.Value = growerOrder.PaymentTypeLookupGuid;
            command.Parameters.Add(paymentTypeLookupGuidParameter);

            var growerOrderStatusLookupGuidParameter = new SqlParameter("@GrowerOrderStatusLookupGuid", SqlDbType.UniqueIdentifier, 16);
            growerOrderStatusLookupGuidParameter.IsNullable = false;
            growerOrderStatusLookupGuidParameter.Value = growerOrder.GrowerOrderStatusLookupGuid;
            command.Parameters.Add(growerOrderStatusLookupGuidParameter);

            var dateEnteredParameter = new SqlParameter("@DateEntered", SqlDbType.DateTime, 8);
            dateEnteredParameter.IsNullable = false;
            dateEnteredParameter.Value = growerOrder.DateEntered;
            command.Parameters.Add(dateEnteredParameter);

            var growerCreditCardGuidParameter = new SqlParameter("@GrowerCreditCardGuid", SqlDbType.UniqueIdentifier, 16);
            growerCreditCardGuidParameter.IsNullable = true;
            growerCreditCardGuidParameter.Value = growerOrder.GrowerCreditCardGuid ?? (object)DBNull.Value;
            command.Parameters.Add(growerCreditCardGuidParameter);

            var personGuidParameter = new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier, 16);
            personGuidParameter.IsNullable = false;
            personGuidParameter.Value = growerOrder.PersonGuid;
            command.Parameters.Add(personGuidParameter);

            var promotionalCodeParameter = new SqlParameter("@PromotionalCode", SqlDbType.NVarChar, 20);
            promotionalCodeParameter.IsNullable = false;
            growerOrder.PromotionalCode = growerOrder.PromotionalCode ?? "";
            growerOrder.PromotionalCode = growerOrder.PromotionalCode.Trim();
            promotionalCodeParameter.Value = growerOrder.PromotionalCode;
            command.Parameters.Add(promotionalCodeParameter);

        }

        protected override GrowerOrder DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var growerOrder = new GrowerOrder();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)GrowerOrder.ColumnEnum.Id] ?? long.MinValue;
                growerOrder.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)GrowerOrder.ColumnEnum.Guid] ?? Guid.Empty;
                growerOrder.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)GrowerOrder.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                growerOrder.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "GrowerGuid";
                var growerGuid = reader[(int)GrowerOrder.ColumnEnum.GrowerGuid] ?? Guid.Empty;
                growerOrder.GrowerGuid = (Guid)growerGuid;

                columnName = "ShipWeekGuid";
                var shipWeekGuid = reader[(int)GrowerOrder.ColumnEnum.ShipWeekGuid] ?? Guid.Empty;
                growerOrder.ShipWeekGuid = (Guid)shipWeekGuid;

                columnName = "OrderNo";
                var orderNo = reader[(int)GrowerOrder.ColumnEnum.OrderNo] ?? string.Empty;
                orderNo = TrimString(orderNo);
                growerOrder.OrderNo = (string)orderNo;

                columnName = "Description";
                var description = reader[(int)GrowerOrder.ColumnEnum.Description] ?? string.Empty;
                description = TrimString(description);
                growerOrder.Description = (string)description;

                columnName = "CustomerPoNo";
                var customerPoNo = reader[(int)GrowerOrder.ColumnEnum.CustomerPoNo] ?? string.Empty;
                customerPoNo = TrimString(customerPoNo);
                growerOrder.CustomerPoNo = (string)customerPoNo;

                columnName = "OrderTypeLookupGuid";
                var orderTypeLookupGuid = reader[(int)GrowerOrder.ColumnEnum.OrderTypeLookupGuid] ?? Guid.Empty;
                growerOrder.OrderTypeLookupGuid = (Guid)orderTypeLookupGuid;

                columnName = "ProductFormCategoryGuid";
                var productFormCategoryGuid = reader[(int)GrowerOrder.ColumnEnum.ProductFormCategoryGuid] ?? Guid.Empty;
                growerOrder.ProductFormCategoryGuid = (Guid)productFormCategoryGuid;

                columnName = "ProgramTypeGuid";
                var programTypeGuid = reader[(int)GrowerOrder.ColumnEnum.ProgramTypeGuid] ?? Guid.Empty;
                growerOrder.ProgramTypeGuid = (Guid)programTypeGuid;

                columnName = "ShipToAddressGuid";
                var shipToAddressGuid = reader[(int)GrowerOrder.ColumnEnum.ShipToAddressGuid];
                if (shipToAddressGuid == DBNull.Value) shipToAddressGuid = null;
                growerOrder.ShipToAddressGuid = (Guid?)shipToAddressGuid;

                columnName = "PaymentTypeLookupGuid";
                var paymentTypeLookupGuid = reader[(int)GrowerOrder.ColumnEnum.PaymentTypeLookupGuid] ?? Guid.Empty;
                growerOrder.PaymentTypeLookupGuid = (Guid)paymentTypeLookupGuid;

                columnName = "GrowerOrderStatusLookupGuid";
                var growerOrderStatusLookupGuid = reader[(int)GrowerOrder.ColumnEnum.GrowerOrderStatusLookupGuid] ?? Guid.Empty;
                growerOrder.GrowerOrderStatusLookupGuid = (Guid)growerOrderStatusLookupGuid;

                columnName = "DateEntered";
                var dateEntered = reader[(int)GrowerOrder.ColumnEnum.DateEntered] ?? DateTime.MinValue;
                growerOrder.DateEntered = (DateTime)dateEntered;

                columnName = "GrowerCreditCardGuid";
                var growerCreditCardGuid = reader[(int)GrowerOrder.ColumnEnum.GrowerCreditCardGuid];
                if (growerCreditCardGuid == DBNull.Value) growerCreditCardGuid = null;
                growerOrder.GrowerCreditCardGuid = (Guid?)growerCreditCardGuid;

                columnName = "PersonGuid";
                var personGuid = reader[(int)GrowerOrder.ColumnEnum.PersonGuid] ?? Guid.Empty;
                growerOrder.PersonGuid = (Guid)personGuid;

                columnName = "PromotionalCode";
                var promotionalCode = reader[(int)GrowerOrder.ColumnEnum.PromotionalCode] ?? string.Empty;
                promotionalCode = TrimString(promotionalCode);
                growerOrder.PromotionalCode = (string)promotionalCode;

                columnName = "GrowerShipMethodCode";
                var growerShipMethodCode = reader[(int)GrowerOrder.ColumnEnum.GrowerShipMethodCode] ?? string.Empty;
                growerShipMethodCode = TrimString(growerShipMethodCode);
                //growerOrder.growerShipMethodCode = (string)growerShipMethodCode;

                columnName = "SellerGuid";
                var sellerGuid = reader[(int)GrowerOrder.ColumnEnum.SellerGuid] ?? string.Empty;
                //growerOrder.sellerGuid = (string)sellerGuid;

                columnName = "NavigationProgramTypeCode";
                var navigationProgramTypeCode = reader[(int)GrowerOrder.ColumnEnum.NavigationProgramTypeCode] ?? string.Empty;
                navigationProgramTypeCode = TrimString(navigationProgramTypeCode);
                growerOrder.NavigationProgramTypeCode = (string)navigationProgramTypeCode;

                columnName = "NavigationProductFormCategoryCode";
                var navigationProductFormCategoryCode = reader[(int)GrowerOrder.ColumnEnum.NavigationProductFormCategoryCode] ?? string.Empty;
                navigationProductFormCategoryCode = TrimString(navigationProductFormCategoryCode);
                growerOrder.NavigationProductFormCategoryCode = (string)navigationProductFormCategoryCode;


            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return growerOrder;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(GrowerOrder growerOrder)
        {
            SetUpGrowerLazyLoad(growerOrder);
            SetUpShipWeekLazyLoad(growerOrder);
            SetUpOrderTypeLookupLazyLoad(growerOrder);
            SetUpProductFormCategoryLazyLoad(growerOrder);
            SetUpProgramTypeLazyLoad(growerOrder);
            SetUpShipToAddressLazyLoad(growerOrder);
            SetUpPaymentTypeLookupLazyLoad(growerOrder);
            SetUpGrowerOrderStatusLookupLazyLoad(growerOrder);
            SetUpGrowerCreditCardLazyLoad(growerOrder);
            SetUpPersonLazyLoad(growerOrder);
            //SetUpClaimListLazyLoad( growerOrder);
            SetUpGrowerOrderFeeListLazyLoad(growerOrder);
            SetUpGrowerOrderHistoryListLazyLoad(growerOrder);
            //SetUpGrowerOrderReminderListLazyLoad( growerOrder);
            SetUpSupplierOrderListLazyLoad(growerOrder);
        }

        protected override void FixAnyLazyLoadedLists(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            FixGrowerList(growerOrder, listAction);
            FixShipWeekList(growerOrder, listAction);
            FixOrderTypeLookupList(growerOrder, listAction);
            FixProductFormCategoryList(growerOrder, listAction);
            FixProgramTypeList(growerOrder, listAction);
            FixShipToAddressList(growerOrder, listAction);
            FixPaymentTypeLookupList(growerOrder, listAction);
            FixGrowerOrderStatusLookupList(growerOrder, listAction);
            FixGrowerCreditCardList(growerOrder, listAction);
            FixPersonList(growerOrder, listAction);
        }

        private void SetUpGrowerLazyLoad(GrowerOrder growerOrder)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            growerOrder.SetLazyGrower(new Lazy<Grower>(() => growerTableService.GetByGuid(growerOrder.GrowerGuid), false));
        }

        private void FixGrowerList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.GrowerIsLoaded)
            {
                FixLazyLoadedList(growerOrder.Grower.GrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpShipWeekLazyLoad(GrowerOrder growerOrder)
        {
            var shipWeekTableService = Context.Get<ShipWeekTableService>();
            growerOrder.SetLazyShipWeek(new Lazy<ShipWeek>(() => shipWeekTableService.GetByGuid(growerOrder.ShipWeekGuid), false));
        }

        private void FixShipWeekList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.ShipWeekIsLoaded)
            {
                FixLazyLoadedList(growerOrder.ShipWeek.GrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpOrderTypeLookupLazyLoad(GrowerOrder growerOrder)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            growerOrder.SetLazyOrderTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrder.OrderTypeLookupGuid), false));
        }

        private void FixOrderTypeLookupList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.OrderTypeLookupIsLoaded)
            {
                FixLazyLoadedList(growerOrder.OrderTypeLookup.OrderTypeGrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpProductFormCategoryLazyLoad(GrowerOrder growerOrder)
        {
            var productFormCategoryTableService = Context.Get<ProductFormCategoryTableService>();
            growerOrder.SetLazyProductFormCategory(new Lazy<ProductFormCategory>(() => productFormCategoryTableService.GetByGuid(growerOrder.ProductFormCategoryGuid), false));
        }

        private void FixProductFormCategoryList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.ProductFormCategoryIsLoaded)
            {
                FixLazyLoadedList(growerOrder.ProductFormCategory.GrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpProgramTypeLazyLoad(GrowerOrder growerOrder)
        {
            var programTypeTableService = Context.Get<ProgramTypeTableService>();
            growerOrder.SetLazyProgramType(new Lazy<ProgramType>(() => programTypeTableService.GetByGuid(growerOrder.ProgramTypeGuid), false));
        }

        private void FixProgramTypeList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.ProgramTypeIsLoaded)
            {
                FixLazyLoadedList(growerOrder.ProgramType.GrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpShipToAddressLazyLoad(GrowerOrder growerOrder)
        {
            var addressTableService = Context.Get<AddressTableService>();
            growerOrder.SetLazyShipToAddress(new Lazy<Address>(() => addressTableService.GetByGuid(growerOrder.ShipToAddressGuid), false));
        }

        private void FixShipToAddressList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.ShipToAddressIsLoaded)
            {
                FixLazyLoadedList(growerOrder.ShipToAddress.ShipToGrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpPaymentTypeLookupLazyLoad(GrowerOrder growerOrder)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            growerOrder.SetLazyPaymentTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrder.PaymentTypeLookupGuid), false));
        }

        private void FixPaymentTypeLookupList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.PaymentTypeLookupIsLoaded)
            {
                FixLazyLoadedList(growerOrder.PaymentTypeLookup.PaymentTypeGrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpGrowerOrderStatusLookupLazyLoad(GrowerOrder growerOrder)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            growerOrder.SetLazyGrowerOrderStatusLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrder.GrowerOrderStatusLookupGuid), false));
        }

        private void FixGrowerOrderStatusLookupList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.GrowerOrderStatusLookupIsLoaded)
            {
                FixLazyLoadedList(growerOrder.GrowerOrderStatusLookup.GrowerOrderStatusGrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpGrowerCreditCardLazyLoad(GrowerOrder growerOrder)
        {
            var growerCreditCardTableService = Context.Get<GrowerCreditCardTableService>();
            growerOrder.SetLazyGrowerCreditCard(new Lazy<GrowerCreditCard>(() => growerCreditCardTableService.GetByGuid(growerOrder.GrowerCreditCardGuid), false));
        }

        private void FixGrowerCreditCardList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.GrowerCreditCardIsLoaded)
            {
                FixLazyLoadedList(growerOrder.GrowerCreditCard.GrowerOrderList, growerOrder, listAction);
            }
        }

        private void SetUpPersonLazyLoad(GrowerOrder growerOrder)
        {
            var personTableService = Context.Get<PersonTableService>();
            growerOrder.SetLazyPerson(new Lazy<Person>(() => personTableService.GetByGuid(growerOrder.PersonGuid), false));
        }

        private void FixPersonList(GrowerOrder growerOrder, ListActionEnum listAction)
        {
            if (growerOrder.PersonIsLoaded)
            {
                FixLazyLoadedList(growerOrder.Person.GrowerOrderList, growerOrder, listAction);
            }
        }

        //private void SetUpClaimListLazyLoad(GrowerOrder growerOrder)
        //{
        //    var claimTableService = Context.Get<ClaimTableService>();
        //    growerOrder.SetLazyClaimList(new Lazy<List<Claim>>(() => claimTableService.GetAllActiveByGuid(growerOrder.Guid, "GrowerOrderGuid"), false));
        //}

        private void SetUpGrowerOrderFeeListLazyLoad(GrowerOrder growerOrder)
        {
            var growerOrderFeeTableService = Context.Get<GrowerOrderFeeTableService>();
            growerOrder.SetLazyGrowerOrderFeeList(new Lazy<List<GrowerOrderFee>>(() => growerOrderFeeTableService.GetAllActiveByGuid(growerOrder.Guid, "GrowerOrderGuid"), false));
        }

        private void SetUpGrowerOrderHistoryListLazyLoad(GrowerOrder growerOrder)
        {
            var growerOrderHistoryTableService = Context.Get<GrowerOrderHistoryTableService>();
            growerOrder.SetLazyGrowerOrderHistoryList(new Lazy<List<GrowerOrderHistory>>(() => growerOrderHistoryTableService.GetAllActiveByGuid(growerOrder.Guid, "GrowerOrderGuid"), false));
        }

        //private void SetUpGrowerOrderReminderListLazyLoad(GrowerOrder growerOrder)
        //{
        //    var growerOrderReminderTableService = Context.Get<GrowerOrderReminderTableService>();
        //    growerOrder.SetLazyGrowerOrderReminderList(new Lazy<List<GrowerOrderReminder>>(() => growerOrderReminderTableService.GetAllActiveByGuid(growerOrder.Guid, "GrowerOrderGuid"), false));
        //}

        private void SetUpSupplierOrderListLazyLoad(GrowerOrder growerOrder)
        {
            var supplierOrderTableService = Context.Get<SupplierOrderTableService>();
            growerOrder.SetLazySupplierOrderList(new Lazy<List<SupplierOrder>>(() => supplierOrderTableService.GetAllActiveByGuid(growerOrder.Guid, "GrowerOrderGuid"), false));
        }

        #endregion
    }
}
