﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
namespace DataServiceLibrary
{
    public class RequestStatus
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class GrowerOrderFeeDataService: DataServiceBaseNew
    {
        public GrowerOrderFeeDataService(StatusObject status)
            : base(status)
        {

        }
        public RequestStatus CreateOrderFee(Guid OrderFeeGuid, Guid GrowerOrderGuid,  decimal Amount, Guid FeeTypeLookupGuid)
        {
            RequestStatus response = new RequestStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("GrowerFeeTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = string.Format(@"
                    Insert into GrowerOrderFee 
                    (Guid, GrowerOrderGuid, FeeTypeLookupGuid, Amount, IsManual)
                    VALUES
                    ('{0}','{1}', '{2}', '{3}', 1)",
                    OrderFeeGuid,
                    GrowerOrderGuid,
                    FeeTypeLookupGuid,
                    Amount);

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    //LogOrderLineUpdate(UserGuid, UserCode, OrderLineGuid, transaction);

                    // Attempt to commit the transaction.
                    transaction.Commit();

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                { connection.Close(); }
            }

            return response;
        }

        public RequestStatus UpdateOrderFee(Guid OrderFeeGuid, decimal Amount, Guid FeeTypeLookupGuid)
        {
            RequestStatus response = new RequestStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("GrowerFeeTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = string.Format(@"
                    Update GrowerOrderFee Set 
                    FeeTypeLookupGuid = '{0}', Amount = '{1}',IsManual = 1
                    Where Guid = '{2}'",
                    FeeTypeLookupGuid,
                    Amount,
                    OrderFeeGuid);

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    //LogOrderLineUpdate(UserGuid, UserCode, OrderLineGuid, transaction);

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }

            return response;
        }

        public RequestStatus DeleteOrderFee(Guid OrderFeeGuid)
        {
            RequestStatus response = new RequestStatus();

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("GrowerFeeTransaction");

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = string.Format(@"
                    Delete GrowerOrderFee
                    Where Guid = '{0}'",
                    OrderFeeGuid);

                    if (command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception(string.Format("Sql command: {0} failed.", command.CommandText));
                    }

                    //LogOrderLineUpdate(UserGuid, UserCode, OrderLineGuid, transaction);

                    // Attempt to commit the transaction.
                    transaction.Commit();
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    response.Success = false;
                    response.Message = ex.Message;
                }
                finally
                { 
                    connection.Close();
                }
            }

            return response;
        }



        public List<DataObjectLibrary.GrowerOrderFee> GrowerOrderGetFees(Guid growerOrderGuid)
        {
            List<DataObjectLibrary.GrowerOrderFee> growerOrderFeeList = null;

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[GrowerOrderGetFees]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;




                
                var reader = command.ExecuteReader();

                var context = new Context();

                var growerOrderFeeTableService = context.Get<GrowerOrderFeeTableService>();
                growerOrderFeeList = growerOrderFeeTableService.GetAllFromReader(reader);

                reader.Close();
               

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerOrderGetFees",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }

            finally
            {
                connection.Close();
            }

            return growerOrderFeeList;
        }

    }
}
