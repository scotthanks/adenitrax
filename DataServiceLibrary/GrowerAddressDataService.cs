﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;
using System.Data.SqlClient;
using System.Data;
using DataObjectLibrary;
using DataAccessLibrary;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public partial class GrowerAddressDataService : DataServiceBaseNew
    {
        public GrowerAddressDataService(StatusObject status)
            : base(status)
        {

        }
    
        public bool DeleteAddress(Guid growerAddressGuid)
        {
            
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[GrowerAddressDelete]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerAddressGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerAddressGuid;
                command.Parameters
                    .Add(new SqlParameter("@Success", SqlDbType.Bit, 1))
                    .Direction = ParameterDirection.Output;



               
                var response = (int)command.ExecuteNonQuery();
                Status.Success = (bool)command.Parameters["@Success"].Value;

                
                Status.StatusMessage = "Address Deleted";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerAddressDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return Status.Success;
            }
            finally
            {

                connection.Close();
            }
            return Status.Success;
        }

        public bool UpdateAddress(Guid userGuid, Guid growerAddressGuid, string name, string streetAddress1, string streetAddress2, string city, string stateCode, string zipCode, PhoneNumber phoneNumber, string specialInstructions,string sellerCustomerID, bool isDefault)
        {
            streetAddress2 = streetAddress2 ?? "";
            streetAddress2 = streetAddress2.Trim();

            specialInstructions = specialInstructions ?? "";
            specialInstructions = specialInstructions.Trim();
            
            sellerCustomerID = sellerCustomerID ?? "";
            sellerCustomerID = sellerCustomerID.Trim();



            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[GrowerAddressUpdate]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerAddressGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerAddressGuid;
                command.Parameters
                    .Add(new SqlParameter("@Name", SqlDbType.NVarChar, 50))
                    .Value = name;
                command.Parameters
                    .Add(new SqlParameter("@StreetAddress1", SqlDbType.NVarChar, 50))
                    .Value = streetAddress1;
                command.Parameters
                    .Add(new SqlParameter("@streetAddress2", SqlDbType.NVarChar, 50))
                    .Value = streetAddress2;
                command.Parameters
                    .Add(new SqlParameter("@City", SqlDbType.NVarChar, 50))
                    .Value = city;
                command.Parameters
                    .Add(new SqlParameter("@StateCode", SqlDbType.NVarChar, 2))
                    .Value = stateCode;
                command.Parameters
                    .Add(new SqlParameter("@ZipCode", SqlDbType.NChar, 12))
                    .Value = zipCode;
                command.Parameters
                    .Add(new SqlParameter("@PhoneAreaCode", SqlDbType.Decimal, 5))
                    .Value = phoneNumber.AreaCode;
                command.Parameters
                    .Add(new SqlParameter("@Phone", SqlDbType.Decimal, 5))
                    .Value = phoneNumber.LocalPhoneNumber;
                command.Parameters
                    .Add(new SqlParameter("@SpecialInstructions", SqlDbType.NVarChar, 200))
                    .Value = specialInstructions;
                command.Parameters
                    .Add(new SqlParameter("@IsDefault", SqlDbType.Bit, 1))
                    .Value = isDefault;
                
                
                command.Parameters
                    .Add(new SqlParameter("@Success", SqlDbType.Bit, 1))
                    .Direction = ParameterDirection.Output;




                var response = (int)command.ExecuteNonQuery();
               
                Status.Success = (bool)command.Parameters["@Success"].Value;


                Status.StatusMessage = "Address Updated";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerAddressDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return Status.Success;
            }
            finally
            {

                connection.Close();
            }
            return Status.Success;



        }
        public class ReturnData
        {
            public List<GrowerAddress> GrowerAddressList { get; set; }
            public DataObjectLibrary.GrowerOrder GrowerOrder { get; set; }
        }

        public ReturnData GetAddressesForGrower(Guid userGuid, Guid growerGuid)
        {
            var emptyGuid = new Guid();
            emptyGuid = Guid.Empty;

            return GetAddresses(growerGuid,emptyGuid, emptyGuid);
        }

        public ReturnData GetAddress(Guid userGuid, Guid growerAddressGuid)
        {
            var emptyGuid = new Guid();
            emptyGuid = Guid.Empty;

            return GetAddresses(emptyGuid, emptyGuid, growerAddressGuid);
        }

        public ReturnData GetAddressesForGrowerOrder(Guid userGuid, Guid growerGuid, Guid growerOrderGuid)
        {
            var emptyGuid = new Guid();
            emptyGuid = Guid.Empty;

            return GetAddresses(growerGuid, growerOrderGuid, emptyGuid);
        }

       

        private ReturnData GetAddresses( Guid growerGuid, Guid growerOrderGuid, Guid growerAddressGuid)
        {
            var returnData = new ReturnData();

            var connection = ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {
                var command = new SqlCommand("[dbo].[GrowerAddressDataGet]", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                if (growerGuid != Guid.Empty)
                {
                    command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                }
                if (growerOrderGuid != Guid.Empty)
                { 
                    command.Parameters
                    .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerOrderGuid;
                 } 
                 if (growerAddressGuid != Guid.Empty)
                { 
                    command.Parameters
                    .Add(new SqlParameter("@GrowerAddressGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerAddressGuid;
                 }



                var reader = command.ExecuteReader();
                var context = new Context();

                var growerAddressTableService = context.Get<GrowerAddressTableService>();
                returnData.GrowerAddressList = growerAddressTableService.GetAllFromReader(reader);

                reader.NextResult();

                var addressTableService = context.Get<AddressTableService>();
                addressTableService.GetAllFromReader(reader);

                reader.NextResult();

                var growerOrderTableService = context.Get<GrowerOrderTableService>();
                var growerOrderList = growerOrderTableService.GetAllFromReader(reader);

                if (growerOrderList != null && growerOrderList.Count == 1)
                {
                    returnData.GrowerOrder = growerOrderList.FirstOrDefault();
                }

                reader.Close();

                Status.Success = true;
                Status.StatusMessage = "Addresses Loaded";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerAddressDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }

            
            return returnData;
        }


        public class ReturnAddressGuids
        {
            public Guid GrowerAddressGuid { get; set; }
            public Guid AddressGuid { get; set; }
        }

        public ReturnAddressGuids AddAddress(Guid growerGuid, string name, string streetAddress1, string streetAddress2, string city, string stateCode, string zipCode, PhoneNumber phoneNumber, string specialInstructions, string sellerCustomerID,bool isDefault)
        {
            var returnData = new ReturnAddressGuids();






            streetAddress2 = streetAddress2 ?? "";
            streetAddress2 = streetAddress2.Trim();

            specialInstructions = specialInstructions ?? "";
            specialInstructions = specialInstructions.Trim();


            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[GrowerAddressAdd]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@userGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                    .Value = growerGuid;
                command.Parameters
                    .Add(new SqlParameter("@Name", SqlDbType.NVarChar, 50))
                    .Value = name;
                command.Parameters
                    .Add(new SqlParameter("@StreetAddress1", SqlDbType.NVarChar, 50))
                    .Value = streetAddress1;
                command.Parameters
                    .Add(new SqlParameter("@streetAddress2", SqlDbType.NVarChar, 50))
                    .Value = streetAddress2;
                command.Parameters
                    .Add(new SqlParameter("@City", SqlDbType.NVarChar, 50))
                    .Value = city;
                command.Parameters
                    .Add(new SqlParameter("@StateCode", SqlDbType.NVarChar, 2))
                    .Value = stateCode;
                command.Parameters
                    .Add(new SqlParameter("@ZipCode", SqlDbType.NChar, 12))
                    .Value = zipCode;
                command.Parameters
                    .Add(new SqlParameter("@PhoneAreaCode", SqlDbType.Decimal, 5))
                    .Value = phoneNumber.AreaCode;
                command.Parameters
                    .Add(new SqlParameter("@Phone", SqlDbType.Decimal, 5))
                    .Value = phoneNumber.LocalPhoneNumber;
                command.Parameters
                    .Add(new SqlParameter("@SpecialInstructions", SqlDbType.NVarChar, 200))
                    .Value = specialInstructions;
                command.Parameters
                    .Add(new SqlParameter("@IsDefault", SqlDbType.Bit, 1))
                    .Value = isDefault;
                command.Parameters
                    .Add(new SqlParameter("@AddressGuid", SqlDbType.UniqueIdentifier))
                    .Direction = ParameterDirection.Output;
                command.Parameters
                    .Add(new SqlParameter("@GrowerAddressGuid", SqlDbType.UniqueIdentifier))
                    .Direction = ParameterDirection.Output;





                var response = (int)command.ExecuteNonQuery();
                returnData.GrowerAddressGuid = (Guid)command.Parameters["@GrowerAddressGuid"].Value;
                returnData.AddressGuid = (Guid)command.Parameters["@AddressGuid"].Value;
                
                Status.Success = true;
                Status.StatusMessage = "Address Added";

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "GrowerAddressDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return returnData;
            }
            finally
            {

                connection.Close();
            }
            return returnData;
        }
    }
}
