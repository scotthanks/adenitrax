﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class GrowerCreditCardTableService : TableService<GrowerCreditCard>
    {
        public GrowerCreditCardTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override GrowerCreditCard DefaultInsert(GrowerCreditCard growerCreditCard, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [GrowerCreditCard]([Guid], [GrowerGuid], [AuthorizeNetSerialNumber], [CardDescription], [DateDeactivated], [DefaultCard], [LastFourDigits], [CardType], ) VALUES (@Guid, @GrowerGuid, @AuthorizeNetSerialNumber, @CardDescription, @DateDeactivated, @DefaultCard, @LastFourDigits, @CardType, );SELECT * FROM [GrowerCreditCard] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, growerCreditCard, transaction: transaction);
        }

        protected override GrowerCreditCard DefaultUpdate(GrowerCreditCard growerCreditCard, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [GrowerCreditCard] SET [GrowerGuid]=@GrowerGuid, [AuthorizeNetSerialNumber]=@AuthorizeNetSerialNumber, [CardDescription]=@CardDescription, [DateDeactivated]=@DateDeactivated, [DefaultCard]=@DefaultCard, [LastFourDigits]=@LastFourDigits, [CardType]=@CardType, WHERE [Guid]=@Guid;SELECT * FROM [GrowerCreditCard] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, growerCreditCard, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, GrowerCreditCard growerCreditCard)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = growerCreditCard.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = growerCreditCard.Guid;
            command.Parameters.Add(guidParameter);

            var growerGuidParameter = new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier, 16);
            growerGuidParameter.IsNullable = false;
            growerGuidParameter.Value = growerCreditCard.GrowerGuid;
            command.Parameters.Add(growerGuidParameter);

            var authorizeNetSerialNumberParameter = new SqlParameter("@AuthorizeNetSerialNumber", SqlDbType.NVarChar, 100);
            authorizeNetSerialNumberParameter.IsNullable = true;
            growerCreditCard.AuthorizeNetSerialNumber = growerCreditCard.AuthorizeNetSerialNumber ?? "";
            growerCreditCard.AuthorizeNetSerialNumber = growerCreditCard.AuthorizeNetSerialNumber.Trim();
            authorizeNetSerialNumberParameter.Value = growerCreditCard.AuthorizeNetSerialNumber ?? (object)DBNull.Value;
            command.Parameters.Add(authorizeNetSerialNumberParameter);

            var cardDescriptionParameter = new SqlParameter("@CardDescription", SqlDbType.NVarChar, 100);
            cardDescriptionParameter.IsNullable = true;
            growerCreditCard.CardDescription = growerCreditCard.CardDescription ?? "";
            growerCreditCard.CardDescription = growerCreditCard.CardDescription.Trim();
            cardDescriptionParameter.Value = growerCreditCard.CardDescription ?? (object)DBNull.Value;
            command.Parameters.Add(cardDescriptionParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = growerCreditCard.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var defaultCardParameter = new SqlParameter("@DefaultCard", SqlDbType.Bit, 1);
            defaultCardParameter.IsNullable = false;
            defaultCardParameter.Value = growerCreditCard.DefaultCard;
            command.Parameters.Add(defaultCardParameter);

            var lastFourDigitsParameter = new SqlParameter("@LastFourDigits", SqlDbType.NVarChar, 8);
            lastFourDigitsParameter.IsNullable = true;
            growerCreditCard.LastFourDigits = growerCreditCard.LastFourDigits ?? "";
            growerCreditCard.LastFourDigits = growerCreditCard.LastFourDigits.Trim();
            lastFourDigitsParameter.Value = growerCreditCard.LastFourDigits ?? (object)DBNull.Value;
            command.Parameters.Add(lastFourDigitsParameter);

            var cardTypeParameter = new SqlParameter("@CardType", SqlDbType.NVarChar, 20);
            cardTypeParameter.IsNullable = true;
            growerCreditCard.CardType = growerCreditCard.CardType ?? "";
            growerCreditCard.CardType = growerCreditCard.CardType.Trim();
            cardTypeParameter.Value = growerCreditCard.CardType ?? (object)DBNull.Value;
            command.Parameters.Add(cardTypeParameter);

        }

        protected override GrowerCreditCard DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var growerCreditCard = new GrowerCreditCard();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)GrowerCreditCard.ColumnEnum.Id] ?? long.MinValue;
                growerCreditCard.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)GrowerCreditCard.ColumnEnum.Guid] ?? Guid.Empty;
                growerCreditCard.Guid = (Guid)guid;

                columnName = "GrowerGuid";
                var growerGuid = reader[(int)GrowerCreditCard.ColumnEnum.GrowerGuid] ?? Guid.Empty;
                growerCreditCard.GrowerGuid = (Guid)growerGuid;

                columnName = "AuthorizeNetSerialNumber";
                var authorizeNetSerialNumber = reader[(int)GrowerCreditCard.ColumnEnum.AuthorizeNetSerialNumber];
                if (authorizeNetSerialNumber == DBNull.Value) authorizeNetSerialNumber = null;
                authorizeNetSerialNumber = TrimString(authorizeNetSerialNumber);
                growerCreditCard.AuthorizeNetSerialNumber = (string)authorizeNetSerialNumber;

                columnName = "CardDescription";
                var cardDescription = reader[(int)GrowerCreditCard.ColumnEnum.CardDescription];
                if (cardDescription == DBNull.Value) cardDescription = null;
                cardDescription = TrimString(cardDescription);
                growerCreditCard.CardDescription = (string)cardDescription;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)GrowerCreditCard.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                growerCreditCard.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "DefaultCard";
                var defaultCard = reader[(int)GrowerCreditCard.ColumnEnum.DefaultCard] ?? false;
                growerCreditCard.DefaultCard = (bool)defaultCard;

                columnName = "LastFourDigits";
                var lastFourDigits = reader[(int)GrowerCreditCard.ColumnEnum.LastFourDigits];
                if (lastFourDigits == DBNull.Value) lastFourDigits = null;
                lastFourDigits = TrimString(lastFourDigits);
                growerCreditCard.LastFourDigits = (string)lastFourDigits;

                columnName = "CardType";
                var cardType = reader[(int)GrowerCreditCard.ColumnEnum.CardType];
                if (cardType == DBNull.Value) cardType = null;
                cardType = TrimString(cardType);
                growerCreditCard.CardType = (string)cardType;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return growerCreditCard;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(GrowerCreditCard growerCreditCard)
        {
            SetUpGrowerLazyLoad(growerCreditCard);
            SetUpGrowerOrderListLazyLoad(growerCreditCard);
        }

        protected override void FixAnyLazyLoadedLists(GrowerCreditCard growerCreditCard, ListActionEnum listAction)
        {
            FixGrowerList(growerCreditCard, listAction);
        }

        private void SetUpGrowerLazyLoad(GrowerCreditCard growerCreditCard)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            growerCreditCard.SetLazyGrower(new Lazy<Grower>(() => growerTableService.GetByGuid(growerCreditCard.GrowerGuid), false));
        }

        private void FixGrowerList(GrowerCreditCard growerCreditCard, ListActionEnum listAction)
        {
            if (growerCreditCard.GrowerIsLoaded)
            {
                FixLazyLoadedList(growerCreditCard.Grower.GrowerCreditCardList, growerCreditCard, listAction);
            }
        }

        private void SetUpGrowerOrderListLazyLoad(GrowerCreditCard growerCreditCard)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            growerCreditCard.SetLazyGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(growerCreditCard.Guid, "GrowerCreditCardGuid"), false));
        }

        #endregion
    }
}
