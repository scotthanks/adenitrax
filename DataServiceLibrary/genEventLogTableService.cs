﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class EventLogTableService : TableService<EventLog>
    {
        public EventLogTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override EventLog DefaultInsert(EventLog eventLog, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [EventLog]([Guid], [LogTypeLookupGuid], [ObjectTypeLookupGuid], [ObjectGuid], [ProcessLookupGuid], [ProcessId], [PersonGuid], [EventTime], [SyncTime], [IntValue], [FloatValue], [Comment], [XmlData], [GuidValue], [UserCode], [VerbosityLookupGuid], [PriorityLookupGuid], [SeverityLookupGuid], ) VALUES (@Guid, @LogTypeLookupGuid, @ObjectTypeLookupGuid, @ObjectGuid, @ProcessLookupGuid, @ProcessId, @PersonGuid, @EventTime, @SyncTime, @IntValue, @FloatValue, @Comment, @XmlData, @GuidValue, @UserCode, @VerbosityLookupGuid, @PriorityLookupGuid, @SeverityLookupGuid, );SELECT * FROM [EventLog] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, eventLog, transaction: transaction);
        }

        protected override EventLog DefaultUpdate(EventLog eventLog, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [EventLog] SET [LogTypeLookupGuid]=@LogTypeLookupGuid, [ObjectTypeLookupGuid]=@ObjectTypeLookupGuid, [ObjectGuid]=@ObjectGuid, [ProcessLookupGuid]=@ProcessLookupGuid, [ProcessId]=@ProcessId, [PersonGuid]=@PersonGuid, [EventTime]=@EventTime, [SyncTime]=@SyncTime, [IntValue]=@IntValue, [FloatValue]=@FloatValue, [Comment]=@Comment, [XmlData]=@XmlData, [GuidValue]=@GuidValue, [UserCode]=@UserCode, [VerbosityLookupGuid]=@VerbosityLookupGuid, [PriorityLookupGuid]=@PriorityLookupGuid, [SeverityLookupGuid]=@SeverityLookupGuid, WHERE [Guid]=@Guid;SELECT * FROM [EventLog] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, eventLog, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, EventLog eventLog)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = eventLog.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = eventLog.Guid;
            command.Parameters.Add(guidParameter);

            var logTypeLookupGuidParameter = new SqlParameter("@LogTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            logTypeLookupGuidParameter.IsNullable = false;
            logTypeLookupGuidParameter.Value = eventLog.LogTypeLookupGuid;
            command.Parameters.Add(logTypeLookupGuidParameter);

            var objectTypeLookupGuidParameter = new SqlParameter("@ObjectTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            objectTypeLookupGuidParameter.IsNullable = false;
            objectTypeLookupGuidParameter.Value = eventLog.ObjectTypeLookupGuid;
            command.Parameters.Add(objectTypeLookupGuidParameter);

            var objectGuidParameter = new SqlParameter("@ObjectGuid", SqlDbType.UniqueIdentifier, 16);
            objectGuidParameter.IsNullable = true;
            objectGuidParameter.Value = eventLog.ObjectGuid ?? (object)DBNull.Value;
            command.Parameters.Add(objectGuidParameter);

            var processLookupGuidParameter = new SqlParameter("@ProcessLookupGuid", SqlDbType.UniqueIdentifier, 16);
            processLookupGuidParameter.IsNullable = false;
            processLookupGuidParameter.Value = eventLog.ProcessLookupGuid;
            command.Parameters.Add(processLookupGuidParameter);

            var processIdParameter = new SqlParameter("@ProcessId", SqlDbType.BigInt, 8);
            processIdParameter.IsNullable = false;
            processIdParameter.Value = eventLog.ProcessId;
            command.Parameters.Add(processIdParameter);

            var personGuidParameter = new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier, 16);
            personGuidParameter.IsNullable = true;
            personGuidParameter.Value = eventLog.PersonGuid ?? (object)DBNull.Value;
            command.Parameters.Add(personGuidParameter);

            var eventTimeParameter = new SqlParameter("@EventTime", SqlDbType.DateTime, 8);
            eventTimeParameter.IsNullable = false;
            eventTimeParameter.Value = eventLog.EventTime;
            command.Parameters.Add(eventTimeParameter);

            var syncTimeParameter = new SqlParameter("@SyncTime", SqlDbType.DateTime, 8);
            syncTimeParameter.IsNullable = true;
            syncTimeParameter.Value = eventLog.SyncTime ?? (object)DBNull.Value;
            command.Parameters.Add(syncTimeParameter);

            var intValueParameter = new SqlParameter("@IntValue", SqlDbType.BigInt, 8);
            intValueParameter.IsNullable = true;
            intValueParameter.Value = eventLog.IntValue ?? (object)DBNull.Value;
            command.Parameters.Add(intValueParameter);

            var floatValueParameter = new SqlParameter("@FloatValue", SqlDbType.Float, 8);
            floatValueParameter.IsNullable = true;
            floatValueParameter.Value = eventLog.FloatValue ?? (object)DBNull.Value;
            command.Parameters.Add(floatValueParameter);

            var commentParameter = new SqlParameter("@Comment", SqlDbType.NVarChar, 2000);
            commentParameter.IsNullable = true;
            eventLog.Comment = eventLog.Comment ?? "";
            eventLog.Comment = eventLog.Comment.Trim();
            commentParameter.Value = eventLog.Comment ?? (object)DBNull.Value;
            command.Parameters.Add(commentParameter);

            var xmlDataParameter = new SqlParameter("@XmlData", SqlDbType.Xml, -1);
            xmlDataParameter.IsNullable = true;
            eventLog.XmlData = eventLog.XmlData ?? "";
            eventLog.XmlData = eventLog.XmlData.Trim();
            xmlDataParameter.Value = eventLog.XmlData ?? (object)DBNull.Value;
            command.Parameters.Add(xmlDataParameter);

            var guidValueParameter = new SqlParameter("@GuidValue", SqlDbType.UniqueIdentifier, 16);
            guidValueParameter.IsNullable = true;
            guidValueParameter.Value = eventLog.GuidValue ?? (object)DBNull.Value;
            command.Parameters.Add(guidValueParameter);

            var userCodeParameter = new SqlParameter("@UserCode", SqlDbType.NChar, 56);
            userCodeParameter.IsNullable = true;
            eventLog.UserCode = eventLog.UserCode ?? "";
            eventLog.UserCode = eventLog.UserCode.Trim();
            userCodeParameter.Value = eventLog.UserCode ?? (object)DBNull.Value;
            command.Parameters.Add(userCodeParameter);

            var verbosityLookupGuidParameter = new SqlParameter("@VerbosityLookupGuid", SqlDbType.UniqueIdentifier, 16);
            verbosityLookupGuidParameter.IsNullable = true;
            verbosityLookupGuidParameter.Value = eventLog.VerbosityLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(verbosityLookupGuidParameter);

            var priorityLookupGuidParameter = new SqlParameter("@PriorityLookupGuid", SqlDbType.UniqueIdentifier, 16);
            priorityLookupGuidParameter.IsNullable = true;
            priorityLookupGuidParameter.Value = eventLog.PriorityLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(priorityLookupGuidParameter);

            var severityLookupGuidParameter = new SqlParameter("@SeverityLookupGuid", SqlDbType.UniqueIdentifier, 16);
            severityLookupGuidParameter.IsNullable = true;
            severityLookupGuidParameter.Value = eventLog.SeverityLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(severityLookupGuidParameter);

        }

        protected override EventLog DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var eventLog = new EventLog();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)EventLog.ColumnEnum.Id] ?? long.MinValue;
                eventLog.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)EventLog.ColumnEnum.Guid] ?? Guid.Empty;
                eventLog.Guid = (Guid)guid;

                columnName = "LogTypeLookupGuid";
                var logTypeLookupGuid = reader[(int)EventLog.ColumnEnum.LogTypeLookupGuid] ?? Guid.Empty;
                eventLog.LogTypeLookupGuid = (Guid)logTypeLookupGuid;

                columnName = "ObjectTypeLookupGuid";
                var objectTypeLookupGuid = reader[(int)EventLog.ColumnEnum.ObjectTypeLookupGuid] ?? Guid.Empty;
                eventLog.ObjectTypeLookupGuid = (Guid)objectTypeLookupGuid;

                columnName = "ObjectGuid";
                var objectGuid = reader[(int)EventLog.ColumnEnum.ObjectGuid];
                if (objectGuid == DBNull.Value) objectGuid = null;
                eventLog.ObjectGuid = (Guid?)objectGuid;

                columnName = "ProcessLookupGuid";
                var processLookupGuid = reader[(int)EventLog.ColumnEnum.ProcessLookupGuid] ?? Guid.Empty;
                eventLog.ProcessLookupGuid = (Guid)processLookupGuid;

                columnName = "ProcessId";
                var processId = reader[(int)EventLog.ColumnEnum.ProcessId] ?? long.MinValue;
                eventLog.ProcessId = (long)processId;

                columnName = "PersonGuid";
                var personGuid = reader[(int)EventLog.ColumnEnum.PersonGuid];
                if (personGuid == DBNull.Value) personGuid = null;
                eventLog.PersonGuid = (Guid?)personGuid;

                columnName = "EventTime";
                var eventTime = reader[(int)EventLog.ColumnEnum.EventTime] ?? DateTime.MinValue;
                eventLog.EventTime = (DateTime)eventTime;

                columnName = "SyncTime";
                var syncTime = reader[(int)EventLog.ColumnEnum.SyncTime];
                if (syncTime == DBNull.Value) syncTime = null;
                eventLog.SyncTime = (DateTime?)syncTime;

                columnName = "IntValue";
                var intValue = reader[(int)EventLog.ColumnEnum.IntValue];
                if (intValue == DBNull.Value) intValue = null;
                eventLog.IntValue = (long?)intValue;

                columnName = "FloatValue";
                var floatValue = reader[(int)EventLog.ColumnEnum.FloatValue];
                if (floatValue == DBNull.Value) floatValue = null;
                eventLog.FloatValue = (Double?)floatValue;

                columnName = "Comment";
                var comment = reader[(int)EventLog.ColumnEnum.Comment];
                if (comment == DBNull.Value) comment = null;
                comment = TrimString(comment);
                eventLog.Comment = (string)comment;

                columnName = "XmlData";
                var xmlData = reader[(int)EventLog.ColumnEnum.XmlData];
                if (xmlData == DBNull.Value) xmlData = null;
                xmlData = TrimString(xmlData);
                eventLog.XmlData = (string)xmlData;

                columnName = "GuidValue";
                var guidValue = reader[(int)EventLog.ColumnEnum.GuidValue];
                if (guidValue == DBNull.Value) guidValue = null;
                eventLog.GuidValue = (Guid?)guidValue;

                columnName = "UserCode";
                var userCode = reader[(int)EventLog.ColumnEnum.UserCode];
                if (userCode == DBNull.Value) userCode = null;
                userCode = TrimString(userCode);
                eventLog.UserCode = (string)userCode;

                columnName = "VerbosityLookupGuid";
                var verbosityLookupGuid = reader[(int)EventLog.ColumnEnum.VerbosityLookupGuid];
                if (verbosityLookupGuid == DBNull.Value) verbosityLookupGuid = null;
                eventLog.VerbosityLookupGuid = (Guid?)verbosityLookupGuid;

                columnName = "PriorityLookupGuid";
                var priorityLookupGuid = reader[(int)EventLog.ColumnEnum.PriorityLookupGuid];
                if (priorityLookupGuid == DBNull.Value) priorityLookupGuid = null;
                eventLog.PriorityLookupGuid = (Guid?)priorityLookupGuid;

                columnName = "SeverityLookupGuid";
                var severityLookupGuid = reader[(int)EventLog.ColumnEnum.SeverityLookupGuid];
                if (severityLookupGuid == DBNull.Value) severityLookupGuid = null;
                eventLog.SeverityLookupGuid = (Guid?)severityLookupGuid;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return eventLog;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(EventLog eventLog)
        {
            SetUpLogTypeLookupLazyLoad(eventLog);
            SetUpObjectTypeLookupLazyLoad(eventLog);
            SetUpProcessLookupLazyLoad(eventLog);
            SetUpPersonLazyLoad(eventLog);
            SetUpVerbosityLookupLazyLoad(eventLog);
            SetUpPriorityLookupLazyLoad(eventLog);
            SetUpSeverityLookupLazyLoad(eventLog);
        }

        protected override void FixAnyLazyLoadedLists(EventLog eventLog, ListActionEnum listAction)
        {
            FixLogTypeLookupList(eventLog, listAction);
            FixObjectTypeLookupList(eventLog, listAction);
            FixProcessLookupList(eventLog, listAction);
            //FixPersonList( eventLog, listAction);
            FixVerbosityLookupList(eventLog, listAction);
            FixPriorityLookupList(eventLog, listAction);
            FixSeverityLookupList(eventLog, listAction);
        }

        private void SetUpLogTypeLookupLazyLoad(EventLog eventLog)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            eventLog.SetLazyLogTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(eventLog.LogTypeLookupGuid), false));
        }

        private void FixLogTypeLookupList(EventLog eventLog, ListActionEnum listAction)
        {
            if (eventLog.LogTypeLookupIsLoaded)
            {
                FixLazyLoadedList(eventLog.LogTypeLookup.LogTypeEventLogList, eventLog, listAction);
            }
        }

        private void SetUpObjectTypeLookupLazyLoad(EventLog eventLog)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            eventLog.SetLazyObjectTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(eventLog.ObjectTypeLookupGuid), false));
        }

        private void FixObjectTypeLookupList(EventLog eventLog, ListActionEnum listAction)
        {
            if (eventLog.ObjectTypeLookupIsLoaded)
            {
                FixLazyLoadedList(eventLog.ObjectTypeLookup.ObjectTypeEventLogList, eventLog, listAction);
            }
        }

        private void SetUpProcessLookupLazyLoad(EventLog eventLog)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            eventLog.SetLazyProcessLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(eventLog.ProcessLookupGuid), false));
        }

        private void FixProcessLookupList(EventLog eventLog, ListActionEnum listAction)
        {
            if (eventLog.ProcessLookupIsLoaded)
            {
                FixLazyLoadedList(eventLog.ProcessLookup.ProcessEventLogList, eventLog, listAction);
            }
        }

        private void SetUpPersonLazyLoad(EventLog eventLog)
        {
            var personTableService = Context.Get<PersonTableService>();
            eventLog.SetLazyPerson(new Lazy<Person>(() => personTableService.GetByGuid(eventLog.PersonGuid), false));
        }

        //private void FixPersonList( EventLog eventLog, ListActionEnum listAction)
        //{
        //    if (eventLog.PersonIsLoaded)
        //    {
        //        FixLazyLoadedList(eventLog.Person.EventLogList, eventLog, listAction);
        //    }
        //}

        private void SetUpVerbosityLookupLazyLoad(EventLog eventLog)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            eventLog.SetLazyVerbosityLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(eventLog.VerbosityLookupGuid), false));
        }

        private void FixVerbosityLookupList(EventLog eventLog, ListActionEnum listAction)
        {
            if (eventLog.VerbosityLookupIsLoaded)
            {
                FixLazyLoadedList(eventLog.VerbosityLookup.VerbosityEventLogList, eventLog, listAction);
            }
        }

        private void SetUpPriorityLookupLazyLoad(EventLog eventLog)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            eventLog.SetLazyPriorityLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(eventLog.PriorityLookupGuid), false));
        }

        private void FixPriorityLookupList(EventLog eventLog, ListActionEnum listAction)
        {
            if (eventLog.PriorityLookupIsLoaded)
            {
                FixLazyLoadedList(eventLog.PriorityLookup.PriorityEventLogList, eventLog, listAction);
            }
        }

        private void SetUpSeverityLookupLazyLoad(EventLog eventLog)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            eventLog.SetLazySeverityLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(eventLog.SeverityLookupGuid), false));
        }

        private void FixSeverityLookupList(EventLog eventLog, ListActionEnum listAction)
        {
            if (eventLog.SeverityLookupIsLoaded)
            {
                FixLazyLoadedList(eventLog.SeverityLookup.SeverityEventLogList, eventLog, listAction);
            }
        }

        #endregion
    }
}
