﻿using System;
using System.Collections.Generic;

using System.Data.SqlClient;
using System.Data;

using BusinessObjectsLibrary;


namespace DataServiceLibrary
{
    
     public class ClinicPatientDataService : DataServiceBaseNew
    {
        public ClinicPatientDataService(StatusObject status)
            : base(status)
        {

        }
    
       

      
        public List<ClinicPatientVisit> GetClinicPatientVisits(Guid clinicPatientGuid,DateTime dateStart, DateTime dateEnd )
        {
            var list = new List<ClinicPatientVisit>();
            var connection = DataAccessLibrary.ConnectionServices.ConnectionToData;
            connection.Open();
            
            if (clinicPatientGuid ==null)
            {
                clinicPatientGuid = Guid.Empty;
            }
            try
            {

                var command = new SqlCommand("[dbo].[ClinicPatientVisitsGet]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;
                command.Parameters
                    .Add(new SqlParameter("@ClinicPatientGuid", SqlDbType.UniqueIdentifier))
                    .Value = clinicPatientGuid;
                command.Parameters
                    .Add(new SqlParameter("@DateStart", SqlDbType.DateTime))
                    .Value = dateStart;
                command.Parameters
                    .Add(new SqlParameter("@DateEnd", SqlDbType.DateTime))
                    .Value = dateEnd;


                var reader = command.ExecuteReader();

                while (reader.Read())
                {



                    var item = new ClinicPatientVisit();
                    item.Guid = (Guid)reader["Guid"];        
                    item.ClinicPatientGuid = (Guid)reader["ClinicPatientGuid"];        
                    item.DaysSinceStart = (int)reader["DaysSinceStart"];
                    item.ImageLink = (string)reader["ImageLink"];
                    item.Note = (string)reader["Note"];
                    item.PercentReductionSinceStart = (decimal)reader["PercentReductionSinceStart"];
                    item.ProductAppliedDescription = (string)reader["ProductAppliedDescription"];
                    item.ProductAppliedGuid = (Guid)reader["ProductAppliedGuid"];
                    item.VisitDate = (DateTime)reader["VisitDate"];
                    item.ClinicPatientName = (string)reader["ClinicPatientName"];

                    item.InitialWoundSizeCM = (decimal)reader["InitialWoundSizeCM"];
                    item.WoundSizeCM = (decimal)reader["WoundSizeCM"];



                    list.Add(item);
                }

                Status.Success = true;
                Status.StatusMessage = "Clinic Patient Visits Retrieved";
            
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "ClinicPatientDataService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
            }
            finally
            {
                connection.Close();
            }
            return list;
        }
      
    }
}
