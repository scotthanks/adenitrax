﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class GrowerOrderHistoryTableService : TableService<GrowerOrderHistory>
    {
        public GrowerOrderHistoryTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override GrowerOrderHistory DefaultInsert(GrowerOrderHistory growerOrderHistory, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [GrowerOrderHistory]([Guid], [DateDeactivated], [GrowerOrderGuid], [Comment], [IsInternal], [PersonGuid], [EventDate], [LogTypeLookupGuid], ) VALUES (@Guid, @DateDeactivated, @GrowerOrderGuid, @Comment, @IsInternal, @PersonGuid, @EventDate, @LogTypeLookupGuid, );SELECT * FROM [GrowerOrderHistory] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, growerOrderHistory, transaction: transaction);
        }

        protected override GrowerOrderHistory DefaultUpdate(GrowerOrderHistory growerOrderHistory, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [GrowerOrderHistory] SET [DateDeactivated]=@DateDeactivated, [GrowerOrderGuid]=@GrowerOrderGuid, [Comment]=@Comment, [IsInternal]=@IsInternal, [PersonGuid]=@PersonGuid, [EventDate]=@EventDate, [LogTypeLookupGuid]=@LogTypeLookupGuid, WHERE [Guid]=@Guid;SELECT * FROM [GrowerOrderHistory] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, growerOrderHistory, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, GrowerOrderHistory growerOrderHistory)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = growerOrderHistory.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = growerOrderHistory.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = growerOrderHistory.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var growerOrderGuidParameter = new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier, 16);
            growerOrderGuidParameter.IsNullable = false;
            growerOrderGuidParameter.Value = growerOrderHistory.GrowerOrderGuid;
            command.Parameters.Add(growerOrderGuidParameter);

            var commentParameter = new SqlParameter("@Comment", SqlDbType.NVarChar, 2000);
            commentParameter.IsNullable = true;
            growerOrderHistory.Comment = growerOrderHistory.Comment ?? "";
            growerOrderHistory.Comment = growerOrderHistory.Comment.Trim();
            commentParameter.Value = growerOrderHistory.Comment ?? (object)DBNull.Value;
            command.Parameters.Add(commentParameter);

            var isInternalParameter = new SqlParameter("@IsInternal", SqlDbType.Bit, 1);
            isInternalParameter.IsNullable = false;
            isInternalParameter.Value = growerOrderHistory.IsInternal;
            command.Parameters.Add(isInternalParameter);

            var personGuidParameter = new SqlParameter("@PersonGuid", SqlDbType.UniqueIdentifier, 16);
            personGuidParameter.IsNullable = false;
            personGuidParameter.Value = growerOrderHistory.PersonGuid;
            command.Parameters.Add(personGuidParameter);

            var eventDateParameter = new SqlParameter("@EventDate", SqlDbType.DateTime, 8);
            eventDateParameter.IsNullable = false;
            eventDateParameter.Value = growerOrderHistory.EventDate;
            command.Parameters.Add(eventDateParameter);

            var logTypeLookupGuidParameter = new SqlParameter("@LogTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            logTypeLookupGuidParameter.IsNullable = false;
            logTypeLookupGuidParameter.Value = growerOrderHistory.LogTypeLookupGuid;
            command.Parameters.Add(logTypeLookupGuidParameter);

        }

        protected override GrowerOrderHistory DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var growerOrderHistory = new GrowerOrderHistory();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)GrowerOrderHistory.ColumnEnum.Id] ?? long.MinValue;
                growerOrderHistory.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)GrowerOrderHistory.ColumnEnum.Guid] ?? Guid.Empty;
                growerOrderHistory.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)GrowerOrderHistory.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                growerOrderHistory.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "GrowerOrderGuid";
                var growerOrderGuid = reader[(int)GrowerOrderHistory.ColumnEnum.GrowerOrderGuid] ?? Guid.Empty;
                growerOrderHistory.GrowerOrderGuid = (Guid)growerOrderGuid;

                columnName = "Comment";
                var comment = reader[(int)GrowerOrderHistory.ColumnEnum.Comment];
                if (comment == DBNull.Value) comment = null;
                comment = TrimString(comment);
                growerOrderHistory.Comment = (string)comment;

                columnName = "IsInternal";
                var isInternal = reader[(int)GrowerOrderHistory.ColumnEnum.IsInternal] ?? false;
                growerOrderHistory.IsInternal = (bool)isInternal;

                columnName = "PersonGuid";
                var personGuid = reader[(int)GrowerOrderHistory.ColumnEnum.PersonGuid] ?? Guid.Empty;
                growerOrderHistory.PersonGuid = (Guid)personGuid;

                columnName = "EventDate";
                var eventDate = reader[(int)GrowerOrderHistory.ColumnEnum.EventDate] ?? DateTime.MinValue;
                growerOrderHistory.EventDate = (DateTime)eventDate;

                columnName = "LogTypeLookupGuid";
                var logTypeLookupGuid = reader[(int)GrowerOrderHistory.ColumnEnum.LogTypeLookupGuid] ?? Guid.Empty;
                growerOrderHistory.LogTypeLookupGuid = (Guid)logTypeLookupGuid;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return growerOrderHistory;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(GrowerOrderHistory growerOrderHistory)
        {
            SetUpGrowerOrderLazyLoad(growerOrderHistory);
            SetUpPersonLazyLoad(growerOrderHistory);
            SetUpLogTypeLookupLazyLoad(growerOrderHistory);
        }

        protected override void FixAnyLazyLoadedLists(GrowerOrderHistory growerOrderHistory, ListActionEnum listAction)
        {
            FixGrowerOrderList(growerOrderHistory, listAction);
            FixPersonList(growerOrderHistory, listAction);
            FixLogTypeLookupList(growerOrderHistory, listAction);
        }

        private void SetUpGrowerOrderLazyLoad(GrowerOrderHistory growerOrderHistory)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            growerOrderHistory.SetLazyGrowerOrder(new Lazy<GrowerOrder>(() => growerOrderTableService.GetByGuid(growerOrderHistory.GrowerOrderGuid), false));
        }

        private void FixGrowerOrderList(GrowerOrderHistory growerOrderHistory, ListActionEnum listAction)
        {
            if (growerOrderHistory.GrowerOrderIsLoaded)
            {
                FixLazyLoadedList(growerOrderHistory.GrowerOrder.GrowerOrderHistoryList, growerOrderHistory, listAction);
            }
        }

        private void SetUpPersonLazyLoad(GrowerOrderHistory growerOrderHistory)
        {
            var personTableService = Context.Get<PersonTableService>();
            growerOrderHistory.SetLazyPerson(new Lazy<Person>(() => personTableService.GetByGuid(growerOrderHistory.PersonGuid), false));
        }

        private void FixPersonList(GrowerOrderHistory growerOrderHistory, ListActionEnum listAction)
        {
            if (growerOrderHistory.PersonIsLoaded)
            {
                FixLazyLoadedList(growerOrderHistory.Person.GrowerOrderHistoryList, growerOrderHistory, listAction);
            }
        }

        private void SetUpLogTypeLookupLazyLoad(GrowerOrderHistory growerOrderHistory)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            growerOrderHistory.SetLazyLogTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(growerOrderHistory.LogTypeLookupGuid), false));
        }

        private void FixLogTypeLookupList(GrowerOrderHistory growerOrderHistory, ListActionEnum listAction)
        {
            if (growerOrderHistory.LogTypeLookupIsLoaded)
            {
                FixLazyLoadedList(growerOrderHistory.LogTypeLookup.LogTypeGrowerOrderHistoryList, growerOrderHistory, listAction);
            }
        }

        #endregion
    }
}
