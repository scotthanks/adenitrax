﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class GrowerTableService : TableService<Grower>
    {
        public GrowerTableService()
        {
            SetUpCacheDelegates();
            SetUpLazyLoadDelegates();
        }

        protected override Grower DefaultInsert(Grower grower, SqlTransaction transaction = null)
        {
            string sqlInsertCommand = "INSERT INTO [Grower]([Guid], [DateDeactivated], [Name], [Code], [IsActivated], [AddressGuid], [AllowSubstitutions], [CreditLimit], [CreditLimitTemporaryIncrease], [CreditLimitTemporaryIncreaseExpiration], [EMail], [PhoneAreaCode], [Phone], [GrowerTypeLookupGuid], [DefaultPaymentTypeLookupGuid], [SalesTaxNumber], [CreditAppOnFile], [RequestedLineOfCredit], [RequestedLineOfCreditStatusLookupGuid], [GrowerSizeLookupGuid], [AdditionalAccountingEmail], [AdditionalOrderEmail], ) VALUES (@Guid, @DateDeactivated, @Name, @Code, @IsActivated, @AddressGuid, @AllowSubstitutions, @CreditLimit, @CreditLimitTemporaryIncrease, @CreditLimitTemporaryIncreaseExpiration, @EMail, @PhoneAreaCode, @Phone, @GrowerTypeLookupGuid, @DefaultPaymentTypeLookupGuid, @SalesTaxNumber, @CreditAppOnFile, @RequestedLineOfCredit, @RequestedLineOfCreditStatusLookupGuid, @GrowerSizeLookupGuid, @AdditionalAccountingEmail, @AdditionalOrderEmail, );SELECT * FROM [Grower] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlInsertCommand, AddParameters, grower, transaction: transaction);
        }

        protected override Grower DefaultUpdate(Grower grower, SqlTransaction transaction = null)
        {
            string sqlUpdateCommand = "UPDATE [Grower] SET [DateDeactivated]=@DateDeactivated, [Name]=@Name, [Code]=@Code, [IsActivated]=@IsActivated, [AddressGuid]=@AddressGuid, [AllowSubstitutions]=@AllowSubstitutions, [CreditLimit]=@CreditLimit, [CreditLimitTemporaryIncrease]=@CreditLimitTemporaryIncrease, [CreditLimitTemporaryIncreaseExpiration]=@CreditLimitTemporaryIncreaseExpiration, [EMail]=@EMail, [PhoneAreaCode]=@PhoneAreaCode, [Phone]=@Phone, [GrowerTypeLookupGuid]=@GrowerTypeLookupGuid, [DefaultPaymentTypeLookupGuid]=@DefaultPaymentTypeLookupGuid, [SalesTaxNumber]=@SalesTaxNumber, [CreditAppOnFile]=@CreditAppOnFile, [RequestedLineOfCredit]=@RequestedLineOfCredit, [RequestedLineOfCreditStatusLookupGuid]=@RequestedLineOfCreditStatusLookupGuid, [GrowerSizeLookupGuid]=@GrowerSizeLookupGuid, [AdditionalAccountingEmail]=@AdditionalAccountingEmail, [AdditionalOrderEmail]=@AdditionalOrderEmail, WHERE [Guid]=@Guid;SELECT * FROM [Grower] WHERE [Guid]=@Guid";

            return GetOneFromCommand(sqlUpdateCommand, AddParameters, grower, transaction: transaction);
        }

        protected override void AddParameters(SqlCommand command, Grower grower)
        {
            var idParameter = new SqlParameter("@Id", SqlDbType.BigInt, 8);
            idParameter.IsNullable = false;
            idParameter.Value = grower.Id;
            command.Parameters.Add(idParameter);

            var guidParameter = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier, 16);
            guidParameter.IsNullable = false;
            guidParameter.Value = grower.Guid;
            command.Parameters.Add(guidParameter);

            var dateDeactivatedParameter = new SqlParameter("@DateDeactivated", SqlDbType.DateTime, 8);
            dateDeactivatedParameter.IsNullable = true;
            dateDeactivatedParameter.Value = grower.DateDeactivated ?? (object)DBNull.Value;
            command.Parameters.Add(dateDeactivatedParameter);

            var nameParameter = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            nameParameter.IsNullable = false;
            grower.Name = grower.Name ?? "";
            grower.Name = grower.Name.Trim();
            nameParameter.Value = grower.Name;
            command.Parameters.Add(nameParameter);

            var codeParameter = new SqlParameter("@Code", SqlDbType.NChar, 20);
            codeParameter.IsNullable = true;
            grower.Code = grower.Code ?? "";
            grower.Code = grower.Code.Trim();
            codeParameter.Value = grower.Code ?? (object)DBNull.Value;
            command.Parameters.Add(codeParameter);

            var isActivatedParameter = new SqlParameter("@IsActivated", SqlDbType.Bit, 1);
            isActivatedParameter.IsNullable = false;
            isActivatedParameter.Value = grower.IsActivated;
            command.Parameters.Add(isActivatedParameter);

            var addressGuidParameter = new SqlParameter("@AddressGuid", SqlDbType.UniqueIdentifier, 16);
            addressGuidParameter.IsNullable = false;
            addressGuidParameter.Value = grower.AddressGuid;
            command.Parameters.Add(addressGuidParameter);

            var allowSubstitutionsParameter = new SqlParameter("@AllowSubstitutions", SqlDbType.Bit, 1);
            allowSubstitutionsParameter.IsNullable = false;
            allowSubstitutionsParameter.Value = grower.AllowSubstitutions;
            command.Parameters.Add(allowSubstitutionsParameter);

            var creditLimitParameter = new SqlParameter("@CreditLimit", SqlDbType.Int, 4);
            creditLimitParameter.IsNullable = false;
            creditLimitParameter.Value = grower.CreditLimit;
            command.Parameters.Add(creditLimitParameter);

            var creditLimitTemporaryIncreaseParameter = new SqlParameter("@CreditLimitTemporaryIncrease", SqlDbType.Int, 4);
            creditLimitTemporaryIncreaseParameter.IsNullable = false;
            creditLimitTemporaryIncreaseParameter.Value = grower.CreditLimitTemporaryIncrease;
            command.Parameters.Add(creditLimitTemporaryIncreaseParameter);

            var creditLimitTemporaryIncreaseExpirationParameter = new SqlParameter("@CreditLimitTemporaryIncreaseExpiration", SqlDbType.DateTime, 8);
            creditLimitTemporaryIncreaseExpirationParameter.IsNullable = true;
            creditLimitTemporaryIncreaseExpirationParameter.Value = grower.CreditLimitTemporaryIncreaseExpiration ?? (object)DBNull.Value;
            command.Parameters.Add(creditLimitTemporaryIncreaseExpirationParameter);

            var eMailParameter = new SqlParameter("@EMail", SqlDbType.VarChar, 70);
            eMailParameter.IsNullable = false;
            grower.EMail = grower.EMail ?? "";
            grower.EMail = grower.EMail.Trim();
            eMailParameter.Value = grower.EMail;
            command.Parameters.Add(eMailParameter);

            var phoneAreaCodeParameter = new SqlParameter("@PhoneAreaCode", SqlDbType.Decimal, 5);
            phoneAreaCodeParameter.IsNullable = false;
            phoneAreaCodeParameter.Value = grower.PhoneAreaCode;
            command.Parameters.Add(phoneAreaCodeParameter);

            var phoneParameter = new SqlParameter("@Phone", SqlDbType.Decimal, 5);
            phoneParameter.IsNullable = false;
            phoneParameter.Value = grower.Phone;
            command.Parameters.Add(phoneParameter);

            var growerTypeLookupGuidParameter = new SqlParameter("@GrowerTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            growerTypeLookupGuidParameter.IsNullable = false;
            growerTypeLookupGuidParameter.Value = grower.GrowerTypeLookupGuid;
            command.Parameters.Add(growerTypeLookupGuidParameter);

            var defaultPaymentTypeLookupGuidParameter = new SqlParameter("@DefaultPaymentTypeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            defaultPaymentTypeLookupGuidParameter.IsNullable = false;
            defaultPaymentTypeLookupGuidParameter.Value = grower.DefaultPaymentTypeLookupGuid;
            command.Parameters.Add(defaultPaymentTypeLookupGuidParameter);

            var salesTaxNumberParameter = new SqlParameter("@SalesTaxNumber", SqlDbType.NVarChar, 20);
            salesTaxNumberParameter.IsNullable = false;
            grower.SalesTaxNumber = grower.SalesTaxNumber ?? "";
            grower.SalesTaxNumber = grower.SalesTaxNumber.Trim();
            salesTaxNumberParameter.Value = grower.SalesTaxNumber;
            command.Parameters.Add(salesTaxNumberParameter);

            var creditAppOnFileParameter = new SqlParameter("@CreditAppOnFile", SqlDbType.Bit, 1);
            creditAppOnFileParameter.IsNullable = false;
            creditAppOnFileParameter.Value = grower.CreditAppOnFile;
            command.Parameters.Add(creditAppOnFileParameter);

            var requestedLineOfCreditParameter = new SqlParameter("@RequestedLineOfCredit", SqlDbType.Int, 4);
            requestedLineOfCreditParameter.IsNullable = false;
            requestedLineOfCreditParameter.Value = grower.RequestedLineOfCredit;
            command.Parameters.Add(requestedLineOfCreditParameter);

            var requestedLineOfCreditStatusLookupGuidParameter = new SqlParameter("@RequestedLineOfCreditStatusLookupGuid", SqlDbType.UniqueIdentifier, 16);
            requestedLineOfCreditStatusLookupGuidParameter.IsNullable = true;
            requestedLineOfCreditStatusLookupGuidParameter.Value = grower.RequestedLineOfCreditStatusLookupGuid ?? (object)DBNull.Value;
            command.Parameters.Add(requestedLineOfCreditStatusLookupGuidParameter);

            var growerSizeLookupGuidParameter = new SqlParameter("@GrowerSizeLookupGuid", SqlDbType.UniqueIdentifier, 16);
            growerSizeLookupGuidParameter.IsNullable = false;
            growerSizeLookupGuidParameter.Value = grower.GrowerSizeLookupGuid;
            command.Parameters.Add(growerSizeLookupGuidParameter);

            var additionalAccountingEmailParameter = new SqlParameter("@AdditionalAccountingEmail", SqlDbType.NVarChar, 500);
            additionalAccountingEmailParameter.IsNullable = false;
            grower.AdditionalAccountingEmail = grower.AdditionalAccountingEmail ?? "";
            grower.AdditionalAccountingEmail = grower.AdditionalAccountingEmail.Trim();
            additionalAccountingEmailParameter.Value = grower.AdditionalAccountingEmail;
            command.Parameters.Add(additionalAccountingEmailParameter);

            var additionalOrderEmailParameter = new SqlParameter("@AdditionalOrderEmail", SqlDbType.NVarChar, 500);
            additionalOrderEmailParameter.IsNullable = false;
            grower.AdditionalOrderEmail = grower.AdditionalOrderEmail ?? "";
            grower.AdditionalOrderEmail = grower.AdditionalOrderEmail.Trim();
            additionalOrderEmailParameter.Value = grower.AdditionalOrderEmail;
            command.Parameters.Add(additionalOrderEmailParameter);

        }

        protected override Grower DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var grower = new Grower();

            string columnName = "";
            try
            {
                columnName = "Id";
                var id = reader[(int)Grower.ColumnEnum.Id] ?? long.MinValue;
                grower.Id = (long)id;

                columnName = "Guid";
                var guid = reader[(int)Grower.ColumnEnum.Guid] ?? Guid.Empty;
                grower.Guid = (Guid)guid;

                columnName = "DateDeactivated";
                var dateDeactivated = reader[(int)Grower.ColumnEnum.DateDeactivated];
                if (dateDeactivated == DBNull.Value) dateDeactivated = null;
                grower.DateDeactivated = (DateTime?)dateDeactivated;

                columnName = "Name";
                var name = reader[(int)Grower.ColumnEnum.Name] ?? string.Empty;
                name = TrimString(name);
                grower.Name = (string)name;

                columnName = "Code";
                var code = reader[(int)Grower.ColumnEnum.Code];
                if (code == DBNull.Value) code = null;
                code = TrimString(code);
                grower.Code = (string)code;

                columnName = "IsActivated";
                var isActivated = reader[(int)Grower.ColumnEnum.IsActivated] ?? false;
                grower.IsActivated = (bool)isActivated;

                columnName = "AddressGuid";
                var addressGuid = reader[(int)Grower.ColumnEnum.AddressGuid] ?? Guid.Empty;
                grower.AddressGuid = (Guid)addressGuid;

                columnName = "AllowSubstitutions";
                var allowSubstitutions = reader[(int)Grower.ColumnEnum.AllowSubstitutions] ?? false;
                grower.AllowSubstitutions = (bool)allowSubstitutions;

                columnName = "CreditLimit";
                var creditLimit = reader[(int)Grower.ColumnEnum.CreditLimit] ?? int.MinValue;
                grower.CreditLimit = (int)creditLimit;

                columnName = "CreditLimitTemporaryIncrease";
                var creditLimitTemporaryIncrease = reader[(int)Grower.ColumnEnum.CreditLimitTemporaryIncrease] ?? int.MinValue;
                grower.CreditLimitTemporaryIncrease = (int)creditLimitTemporaryIncrease;

                columnName = "CreditLimitTemporaryIncreaseExpiration";
                var creditLimitTemporaryIncreaseExpiration = reader[(int)Grower.ColumnEnum.CreditLimitTemporaryIncreaseExpiration];
                if (creditLimitTemporaryIncreaseExpiration == DBNull.Value) creditLimitTemporaryIncreaseExpiration = null;
                grower.CreditLimitTemporaryIncreaseExpiration = (DateTime?)creditLimitTemporaryIncreaseExpiration;

                columnName = "EMail";
                var eMail = reader[(int)Grower.ColumnEnum.EMail] ?? string.Empty;
                eMail = TrimString(eMail);
                grower.EMail = (string)eMail;

                columnName = "PhoneAreaCode";
                var phoneAreaCode = reader[(int)Grower.ColumnEnum.PhoneAreaCode] ?? Decimal.MinValue;
                grower.PhoneAreaCode = (Decimal)phoneAreaCode;

                columnName = "Phone";
                var phone = reader[(int)Grower.ColumnEnum.Phone] ?? Decimal.MinValue;
                grower.Phone = (Decimal)phone;

                columnName = "GrowerTypeLookupGuid";
                var growerTypeLookupGuid = reader[(int)Grower.ColumnEnum.GrowerTypeLookupGuid] ?? Guid.Empty;
                grower.GrowerTypeLookupGuid = (Guid)growerTypeLookupGuid;

                columnName = "DefaultPaymentTypeLookupGuid";
                var defaultPaymentTypeLookupGuid = reader[(int)Grower.ColumnEnum.DefaultPaymentTypeLookupGuid] ?? Guid.Empty;
                grower.DefaultPaymentTypeLookupGuid = (Guid)defaultPaymentTypeLookupGuid;

                columnName = "SalesTaxNumber";
                var salesTaxNumber = reader[(int)Grower.ColumnEnum.SalesTaxNumber] ?? string.Empty;
                salesTaxNumber = TrimString(salesTaxNumber);
                grower.SalesTaxNumber = (string)salesTaxNumber;

                columnName = "CreditAppOnFile";
                var creditAppOnFile = reader[(int)Grower.ColumnEnum.CreditAppOnFile] ?? false;
                grower.CreditAppOnFile = (bool)creditAppOnFile;

                columnName = "RequestedLineOfCredit";
                var requestedLineOfCredit = reader[(int)Grower.ColumnEnum.RequestedLineOfCredit] ?? int.MinValue;
                grower.RequestedLineOfCredit = (int)requestedLineOfCredit;

                columnName = "RequestedLineOfCreditStatusLookupGuid";
                var requestedLineOfCreditStatusLookupGuid = reader[(int)Grower.ColumnEnum.RequestedLineOfCreditStatusLookupGuid];
                if (requestedLineOfCreditStatusLookupGuid == DBNull.Value) requestedLineOfCreditStatusLookupGuid = null;
                grower.RequestedLineOfCreditStatusLookupGuid = (Guid?)requestedLineOfCreditStatusLookupGuid;

                columnName = "GrowerSizeLookupGuid";
                var growerSizeLookupGuid = reader[(int)Grower.ColumnEnum.GrowerSizeLookupGuid] ?? Guid.Empty;
                grower.GrowerSizeLookupGuid = (Guid)growerSizeLookupGuid;

                columnName = "AdditionalAccountingEmail";
                var additionalAccountingEmail = reader[(int)Grower.ColumnEnum.AdditionalAccountingEmail] ?? string.Empty;
                additionalAccountingEmail = TrimString(additionalAccountingEmail);
                grower.AdditionalAccountingEmail = (string)additionalAccountingEmail;

                columnName = "AdditionalOrderEmail";
                var additionalOrderEmail = reader[(int)Grower.ColumnEnum.AdditionalOrderEmail] ?? string.Empty;
                additionalOrderEmail = TrimString(additionalOrderEmail);
                grower.AdditionalOrderEmail = (string)additionalOrderEmail;

                columnName = "UseDummenPriceMethod";
                var useDummenPriceMethod = reader[(int)Grower.ColumnEnum.UseDummenPriceMethod] ?? false;
                grower.UseDummenPriceMethod = (bool)useDummenPriceMethod;
            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return grower;
        }

        #region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Grower grower)
        {
            SetUpAddressLazyLoad(grower);
            SetUpGrowerTypeLookupLazyLoad(grower);
            SetUpDefaultPaymentTypeLookupLazyLoad(grower);
            SetUpRequestedLineOfCreditStatusLookupLazyLoad(grower);
            SetUpGrowerSizeLookupLazyLoad(grower);
            SetUpGrowerAddressListLazyLoad(grower);
            SetUpGrowerCreditCardListLazyLoad(grower);
            SetUpGrowerOrderListLazyLoad(grower);
            //SetUpGrowerVolumeListLazyLoad( grower);
            //SetUpLedgerListLazyLoad( grower);
            SetUpPersonListLazyLoad(grower);
        }

        protected override void FixAnyLazyLoadedLists(Grower grower, ListActionEnum listAction)
        {
            FixAddressList(grower, listAction);
            FixGrowerTypeLookupList(grower, listAction);
            FixDefaultPaymentTypeLookupList(grower, listAction);
            FixRequestedLineOfCreditStatusLookupList(grower, listAction);
            FixGrowerSizeLookupList(grower, listAction);
        }

        private void SetUpAddressLazyLoad(Grower grower)
        {
            var addressTableService = Context.Get<AddressTableService>();
            grower.SetLazyAddress(new Lazy<Address>(() => addressTableService.GetByGuid(grower.AddressGuid), false));
        }

        private void FixAddressList(Grower grower, ListActionEnum listAction)
        {
            if (grower.AddressIsLoaded)
            {
                FixLazyLoadedList(grower.Address.GrowerList, grower, listAction);
            }
        }

        private void SetUpGrowerTypeLookupLazyLoad(Grower grower)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            grower.SetLazyGrowerTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(grower.GrowerTypeLookupGuid), false));
        }

        private void FixGrowerTypeLookupList(Grower grower, ListActionEnum listAction)
        {
            if (grower.GrowerTypeLookupIsLoaded)
            {
                FixLazyLoadedList(grower.GrowerTypeLookup.GrowerTypeGrowerList, grower, listAction);
            }
        }

        private void SetUpDefaultPaymentTypeLookupLazyLoad(Grower grower)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            grower.SetLazyDefaultPaymentTypeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(grower.DefaultPaymentTypeLookupGuid), false));
        }

        private void FixDefaultPaymentTypeLookupList(Grower grower, ListActionEnum listAction)
        {
            if (grower.DefaultPaymentTypeLookupIsLoaded)
            {
                FixLazyLoadedList(grower.DefaultPaymentTypeLookup.DefaultPaymentTypeGrowerList, grower, listAction);
            }
        }

        private void SetUpRequestedLineOfCreditStatusLookupLazyLoad(Grower grower)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            grower.SetLazyRequestedLineOfCreditStatusLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(grower.RequestedLineOfCreditStatusLookupGuid), false));
        }

        private void FixRequestedLineOfCreditStatusLookupList(Grower grower, ListActionEnum listAction)
        {
            if (grower.RequestedLineOfCreditStatusLookupIsLoaded)
            {
                FixLazyLoadedList(grower.RequestedLineOfCreditStatusLookup.RequestedLineOfCreditStatusGrowerList, grower, listAction);
            }
        }

        private void SetUpGrowerSizeLookupLazyLoad(Grower grower)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            grower.SetLazyGrowerSizeLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(grower.GrowerSizeLookupGuid), false));
        }

        private void FixGrowerSizeLookupList(Grower grower, ListActionEnum listAction)
        {
            if (grower.GrowerSizeLookupIsLoaded)
            {
                FixLazyLoadedList(grower.GrowerSizeLookup.GrowerSizeGrowerList, grower, listAction);
            }
        }

        private void SetUpGrowerAddressListLazyLoad(Grower grower)
        {
            var growerAddressTableService = Context.Get<GrowerAddressTableService>();
            grower.SetLazyGrowerAddressList(new Lazy<List<GrowerAddress>>(() => growerAddressTableService.GetAllActiveByGuid(grower.Guid, "GrowerGuid"), false));
        }

        private void SetUpGrowerCreditCardListLazyLoad(Grower grower)
        {
            var growerCreditCardTableService = Context.Get<GrowerCreditCardTableService>();
            grower.SetLazyGrowerCreditCardList(new Lazy<List<GrowerCreditCard>>(() => growerCreditCardTableService.GetAllActiveByGuid(grower.Guid, "GrowerGuid"), false));
        }

        private void SetUpGrowerOrderListLazyLoad(Grower grower)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            grower.SetLazyGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(grower.Guid, "GrowerGuid"), false));
        }

        //private void SetUpGrowerVolumeListLazyLoad(Grower grower)
        //{
        //    //var growerVolumeTableService = Context.Get<GrowerVolumeTableService>();
        //    //grower.SetLazyGrowerVolumeList(new Lazy<List<GrowerVolume>>(() => growerVolumeTableService.GetAllActiveByGuid(grower.Guid, "GrowerGuid"), false));
        //}

        //private void SetUpLedgerListLazyLoad(Grower grower)
        //{
        //    var ledgerTableService = Context.Get<LedgerTableService>();
        //    grower.SetLazyLedgerList(new Lazy<List<Ledger>>(() => ledgerTableService.GetAllActiveByGuid(grower.Guid, "GrowerGuid"), false));
        //}

        private void SetUpPersonListLazyLoad(Grower grower)
        {
            var personTableService = Context.Get<PersonTableService>();
            grower.SetLazyPersonList(new Lazy<List<Person>>(() => personTableService.GetAllActiveByGuid(grower.Guid, "GrowerGuid"), false));
        }

        #endregion
    }
}
