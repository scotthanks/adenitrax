﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataServiceLibrary
{
    public class InvoicedOrder
    {
        public Guid GrowerGuid { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public decimal BillTotal { get; set; }
        public decimal PayTotal { get; set; }
        public decimal BalDue { get; set; }
        public string OrderNo { get; set; }
        public string CustomerPoNo { get; set; }
        public string PromotionalCode { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string Name { get; set; }
    }

    public class InvoiceSummaryService
    {
        public List<InvoicedOrder> GetOrders(bool balanceDueOrders, Guid GrowerGuid = new Guid())
        {
            List<InvoicedOrder> orderList = new List<InvoicedOrder>();

            LookupTableValues.CodeValues.GrowerOrderStatusValues orderStatusLookupService = new LookupTableValues.CodeValues.GrowerOrderStatusValues();
            Guid InvoicedLookupStatus = orderStatusLookupService.Invoiced.Guid;

            using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = string.Format(@"
                        select 
                            GrowerGuid,
                            GrowerOrderGuid,
                            BillTotal,
                            PayTotal,
                            BalDue,
                            OrderNo,
                            CustomerPoNo,
                            PromotionalCode,
                            InvoiceDate,
                            InvoiceNumber,
                            Name
                        from LedgerAccountView
                            join GrowerOrder on GrowerOrder.Guid = LedgerAccountView.GrowerOrderGuid
                            join Grower on Grower.Guid = GrowerOrder.GrowerGuid
                        where
                            GrowerOrderStatusLookupGuid = '{0}'",
                            InvoicedLookupStatus);

                if (GrowerGuid != Guid.Empty)
                {
                    command.CommandText += string.Format(" AND GrowerGuid = '{0}'", GrowerGuid);
                }

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    InvoicedOrder order = new InvoicedOrder();

                    order.GrowerGuid = (Guid)reader["GrowerGuid"];
                    order.GrowerOrderGuid = (Guid)reader["GrowerOrderGuid"];
                    order.BillTotal = (decimal)reader["BillTotal"];
                    if (reader["PayTotal"] != DBNull.Value)
                        order.PayTotal = (decimal)reader["PayTotal"];
                    else order.PayTotal = 0;
                    if (reader["BalDue"] != DBNull.Value)
                        order.BalDue = (decimal)reader["BalDue"];
                    else
                        order.BalDue = order.BillTotal;
                    order.OrderNo = reader["OrderNo"] as string;
                    order.CustomerPoNo = reader["CustomerPoNo"] as string;
                    order.PromotionalCode = reader["PromotionalCode"] as string;
                    order.InvoiceDate = ((DateTime)(reader["InvoiceDate"])).ToString("g", CultureInfo.CreateSpecificCulture("en-us"));
                    order.InvoiceNumber = reader["InvoiceNumber"] as string;
                    order.Name = reader["Name"] as string;
                    if (balanceDueOrders && order.BalDue <= 0)
                        continue;

                    orderList.Add(order);
                }
                                                                                        
            }
            return orderList;
        }
    }
}
