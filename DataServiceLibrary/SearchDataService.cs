﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using BusinessObjectsLibrary;

namespace DataServiceLibrary
{
    public class SearchDataService : DataServiceBaseNew
    {
        public SearchDataService(StatusObject status)
            : base(status)
        {

        }
    

        public string[] GetAutoFillResult(string SearchTerm)
        {
            string theRealStem = SearchTerm + "%";
            string[] searchResult = null;
           // System.Collections.ArrayList theArrayList = new System.Collections.ArrayList();
           
           

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToSearch;
            connection.Open(); 
            try
            {

                var command = new SqlCommand("[dbo].[SearchGetAutoFillValues]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };


                command.Parameters
                    .Add(new SqlParameter("@theSearchTerm", SqlDbType.NVarChar, 200))
                    .Value = theRealStem;




               
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    
                    List<String> sl = new List<String>();
                    while (reader.Read())
                    {
                        object[] values = new object[reader.FieldCount];
                        reader.GetValues(values);
                        sl.Add(values[0].ToString());
                    }
                    reader.Close();
                    searchResult = sl.ToArray();
                }
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "SearchService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {
                connection.Close();

            }
            

            return searchResult;
        }
        
        public SearchResultData GetSearchResult( string SearchTerm, bool GetOnlyPage1)
        {

            SearchResultData searchResult = new SearchResultData();

            var connection = DataAccessLibrary.ConnectionServices.ConnectionToSearch;
            connection.Open();
            try
            {

                var command = new SqlCommand("[dbo].[SearchEventRun]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };


                command.Parameters
                    .Add(new SqlParameter("@theSearchTerm", SqlDbType.NVarChar, 200))
                    .Value = SearchTerm;
                command.Parameters
                    .Add(new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier))
                    .Value = Status.User.UserGuid;

                command.Parameters
                    .Add(new SqlParameter("@GetOnlyPage1", SqlDbType.Bit,1))
                    .Value = GetOnlyPage1;


                var reader = command.ExecuteReader();
                searchResult = GetSearchResultFromReader(reader);
                
            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "SearchService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {
                connection.Close();

            }
            
            return searchResult;
        }


        protected SearchResultData GetSearchResultFromReader(SqlDataReader reader)
        {
            var searchResultData = new SearchResultData();

            string columnName = "(undefined)";
            try
            {
                reader.Read();



                //columnName = "Guid";
                //var theguid = reader[columnName];
                //searchResultData.Guid = (Guid)theguid;
                
                columnName = "SearchString";
                var searchString = reader[columnName];
                if (searchString == DBNull.Value) searchString = null;
                searchResultData.SearchString = (string)searchString;
                
                columnName = "ResultCount";
                var resultCount = reader[columnName];
                searchResultData.ResultCount = (int)resultCount;

                // columnName = "Page";
                //var page = reader[columnName];
                //searchResult.Page = (int)page;

                columnName = "RelatedSearches";
                var relatedSearches = reader[columnName];
                if (relatedSearches == DBNull.Value) relatedSearches = null;
                searchResultData.RelatedSearches = (string)relatedSearches;

                reader.NextResult();

                var theResults = new List<SearchResultRow2>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var oResultRow = new SearchResultRow2();  
                        
                        columnName = "SortOrder";
                        int SortOrder = Convert.ToInt16(reader[columnName]);
                        oResultRow.SortOrder = SortOrder;

                        //columnName = "RelevanceCategory";
                        //int RelevanceCategory = Convert.ToInt16(reader[columnName]);
                        //oResultRow.RelevanceCategory = RelevanceCategory;

                        columnName = "ResultType";
                        var ResultType = reader[columnName];
                        oResultRow.ResultType = ResultType.ToString();
                        
                        columnName = "ResultCode";
                        var ResultCode = reader[columnName];
                        oResultRow.ResultCode = ResultCode.ToString();

                        columnName = "ResultName";
                        var ResultName = reader[columnName];
                        oResultRow.ResultName = ResultName.ToString();

                        columnName = "HeaderText";
                        var HeaderText = reader[columnName];
                        oResultRow.HeaderText = HeaderText.ToString();

                        columnName = "SubHeaderText";
                        var SubHeaderText = reader[columnName];
                        oResultRow.SubHeaderText = SubHeaderText.ToString();

                        columnName = "ImageURI";
                        var ImageURI = reader[columnName];
                        oResultRow.ImageURI = ImageURI.ToString();

                        columnName = "CatalogProductFormCategoryCode";
                        var CatalogProductFormCategoryCode = reader[columnName];
                        oResultRow.CatalogProductFormCategoryCode = CatalogProductFormCategoryCode.ToString();

                        columnName = "CatalogProgramTypeCode";
                        var CatalogProgramTypeCode = reader[columnName];
                        oResultRow.CatalogProgramTypeCode = CatalogProgramTypeCode.ToString();

                        columnName = "Program1Name";
                        var Program1Name = reader[columnName];
                        oResultRow.Program1Name = Program1Name.ToString();

                        columnName = "Program1ProductFormCategoryCode";
                        var Program1ProductFormCategoryCode = reader[columnName];
                        oResultRow.Program1ProductFormCategoryCode = Program1ProductFormCategoryCode.ToString();

                        columnName = "Program1ProgramTypeCode";
                        var Program1ProgramTypeCode = reader[columnName];
                        oResultRow.Program1ProgramTypeCode = Program1ProgramTypeCode.ToString();

                        columnName = "Program1ProductCount";
                        int Program1ProductCount = Convert.ToInt16(reader[columnName]);
                        oResultRow.Program1ProductCount = Program1ProductCount;

                        columnName = "Program2Name";
                        var Program2Name = reader[columnName];
                        oResultRow.Program2Name = Program2Name.ToString();

                        columnName = "Program2ProductFormCategoryCode";
                        var Program2ProductFormCategoryCode = reader[columnName];
                        oResultRow.Program2ProductFormCategoryCode = Program2ProductFormCategoryCode.ToString();

                        columnName = "Program2ProgramTypeCode";
                        var Program2ProgramTypeCode = reader[columnName];
                        oResultRow.Program2ProgramTypeCode = Program2ProgramTypeCode.ToString();

                        columnName = "Program2ProductCount";
                        int Program2ProductCount = Convert.ToInt16(reader[columnName]);
                        oResultRow.Program2ProductCount = Program2ProductCount;

                        columnName = "Program3Name";
                        var Program3Name = reader[columnName];
                        oResultRow.Program3Name = Program3Name.ToString();

                        columnName = "Program3ProductFormCategoryCode";
                        var Program3ProductFormCategoryCode = reader[columnName];
                        oResultRow.Program3ProductFormCategoryCode = Program3ProductFormCategoryCode.ToString();

                        columnName = "Program3ProgramTypeCode";
                        var Program3ProgramTypeCode = reader[columnName];
                        oResultRow.Program3ProgramTypeCode = Program3ProgramTypeCode.ToString();

                        columnName = "Program3ProductCount";
                        int Program3ProductCount = Convert.ToInt16(reader[columnName]);
                        oResultRow.Program3ProductCount = Program3ProductCount;


                        columnName = "Species1Code";
                        var Species1Code = reader[columnName];
                        oResultRow.Species1Code = Species1Code.ToString();

                        columnName = "Species1Name";
                        var Species1Name = reader[columnName];
                        oResultRow.Species1Name = Species1Name.ToString();

                        columnName = "Species1ProgramTypeCode";
                        var Species1ProgramTypeCode = reader[columnName];
                        oResultRow.Species1ProgramTypeCode = Species1ProgramTypeCode.ToString();

                        columnName = "Species1ProductFormCategoryCode";
                        var Species1ProductFormCategoryCode = reader[columnName];
                        oResultRow.Species1ProductFormCategoryCode = Species1ProductFormCategoryCode.ToString();
                        
                        columnName = "Species2Code";
                        var Species2Code = reader[columnName];
                        oResultRow.Species2Code = Species2Code.ToString();

                        columnName = "Species2Name";
                        var Species2Name = reader[columnName];
                        oResultRow.Species2Name = Species2Name.ToString();

                        columnName = "Species2ProgramTypeCode";
                        var Species2ProgramTypeCode = reader[columnName];
                        oResultRow.Species2ProgramTypeCode = Species2ProgramTypeCode.ToString();

                        columnName = "Species2ProductFormCategoryCode";
                        var Species2ProductFormCategoryCode = reader[columnName];
                        oResultRow.Species2ProductFormCategoryCode = Species2ProductFormCategoryCode.ToString();

                        columnName = "Species3Code";
                        var Species3Code = reader[columnName];
                        oResultRow.Species3Code = Species3Code.ToString();
                        
                        columnName = "Species3Name";
                        var Species3Name = reader[columnName];
                        oResultRow.Species3Name = Species3Name.ToString();

                        columnName = "Species3ProgramTypeCode";
                        var Species3ProgramTypeCode = reader[columnName];
                        oResultRow.Species3ProgramTypeCode = Species3ProgramTypeCode.ToString();

                        columnName = "Species3ProductFormCategoryCode";
                        var Species3ProductFormCategoryCode = reader[columnName];
                        oResultRow.Species3ProductFormCategoryCode = Species3ProductFormCategoryCode.ToString();

                        columnName = "Program1AvailabilityWeek";
                        var Program1AvailabilityWeek = reader[columnName];
                        oResultRow.Program1AvailabilityWeek = Program1AvailabilityWeek.ToString();

                        columnName = "Program2AvailabilityWeek";
                        var Program2AvailabilityWeek = reader[columnName];
                        oResultRow.Program2AvailabilityWeek = Program2AvailabilityWeek.ToString();
                       
                        columnName = "Program3AvailabilityWeek";
                        var Program3AvailabilityWeek = reader[columnName];
                        oResultRow.Program3AvailabilityWeek = Program3AvailabilityWeek.ToString();



                        theResults.Add(oResultRow);
                    }
                   
                   
                    
                    reader.Close();
                    
                }
          
               
                
                searchResultData.ResultRows = theResults;

            }
            catch (Exception ex)
            {
                var error = new ErrorObject()
                {
                    ErrorNumber = ex.HResult,
                    ErrorLocation = "SearchService",
                    ErrorMessageUser = ex.Message,
                    ErrorMessageSystem = ex.Message,
                    Error = ex.Source
                };
                AddError(error);
                Status.Success = false;
                return null;
            }
            finally
            {


            }
            return searchResultData;
        }
    }
}
