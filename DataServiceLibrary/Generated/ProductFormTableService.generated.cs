using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProductFormTableService : TableService<ProductForm>
	{
		public ProductFormTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override ProductForm DefaultInsert( ProductForm productForm, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [ProductForm]([Guid], [DateDeactivated], [Code], [Name], [ProductFormCategoryGuid], [SalesUnitQty], [TagBundleQty], [LineItemMinimumQty], [SalesUnitsPerPackingUnit], [SupplierGuid], [IncludesDelivery], [TagBundlesPerSalesUnit], ) VALUES (@Guid, @DateDeactivated, @Code, @Name, @ProductFormCategoryGuid, @SalesUnitQty, @TagBundleQty, @LineItemMinimumQty, @SalesUnitsPerPackingUnit, @SupplierGuid, @IncludesDelivery, @TagBundlesPerSalesUnit, );SELECT * FROM [ProductForm] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, productForm, transaction: transaction);
		}

		protected override ProductForm DefaultUpdate( ProductForm productForm, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [ProductForm] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, [ProductFormCategoryGuid]=@ProductFormCategoryGuid, [SalesUnitQty]=@SalesUnitQty, [TagBundleQty]=@TagBundleQty, [LineItemMinimumQty]=@LineItemMinimumQty, [SalesUnitsPerPackingUnit]=@SalesUnitsPerPackingUnit, [SupplierGuid]=@SupplierGuid, [IncludesDelivery]=@IncludesDelivery, [TagBundlesPerSalesUnit]=@TagBundlesPerSalesUnit, WHERE [Guid]=@Guid;SELECT * FROM [ProductForm] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, productForm, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, ProductForm productForm)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = productForm.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = productForm.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = productForm.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 30);
			codeParameter.IsNullable = false;
			productForm.Code = productForm.Code ?? "";
			productForm.Code = productForm.Code.Trim();
			codeParameter.Value = productForm.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			productForm.Name = productForm.Name ?? "";
			productForm.Name = productForm.Name.Trim();
			nameParameter.Value = productForm.Name;
			command.Parameters.Add(nameParameter);

			var productFormCategoryGuidParameter = new SqlParameter( "@ProductFormCategoryGuid", SqlDbType.UniqueIdentifier, 16);
			productFormCategoryGuidParameter.IsNullable = false;
			productFormCategoryGuidParameter.Value = productForm.ProductFormCategoryGuid;
			command.Parameters.Add(productFormCategoryGuidParameter);

			var salesUnitQtyParameter = new SqlParameter( "@SalesUnitQty", SqlDbType.Int, 4);
			salesUnitQtyParameter.IsNullable = false;
			salesUnitQtyParameter.Value = productForm.SalesUnitQty;
			command.Parameters.Add(salesUnitQtyParameter);

			var tagBundleQtyParameter = new SqlParameter( "@TagBundleQty", SqlDbType.Int, 4);
			tagBundleQtyParameter.IsNullable = false;
			tagBundleQtyParameter.Value = productForm.TagBundleQty;
			command.Parameters.Add(tagBundleQtyParameter);

			var lineItemMinimumQtyParameter = new SqlParameter( "@LineItemMinimumQty", SqlDbType.Int, 4);
			lineItemMinimumQtyParameter.IsNullable = false;
			lineItemMinimumQtyParameter.Value = productForm.LineItemMinimumQty;
			command.Parameters.Add(lineItemMinimumQtyParameter);

			var salesUnitsPerPackingUnitParameter = new SqlParameter( "@SalesUnitsPerPackingUnit", SqlDbType.Int, 4);
			salesUnitsPerPackingUnitParameter.IsNullable = false;
			salesUnitsPerPackingUnitParameter.Value = productForm.SalesUnitsPerPackingUnit;
			command.Parameters.Add(salesUnitsPerPackingUnitParameter);

			var supplierGuidParameter = new SqlParameter( "@SupplierGuid", SqlDbType.UniqueIdentifier, 16);
			supplierGuidParameter.IsNullable = true;
			supplierGuidParameter.Value = productForm.SupplierGuid ?? (object)DBNull.Value;
			command.Parameters.Add(supplierGuidParameter);

			var includesDeliveryParameter = new SqlParameter( "@IncludesDelivery", SqlDbType.Bit, 1);
			includesDeliveryParameter.IsNullable = false;
			includesDeliveryParameter.Value = productForm.IncludesDelivery;
			command.Parameters.Add(includesDeliveryParameter);

			var tagBundlesPerSalesUnitParameter = new SqlParameter( "@TagBundlesPerSalesUnit", SqlDbType.Int, 4);
			tagBundlesPerSalesUnitParameter.IsNullable = false;
			tagBundlesPerSalesUnitParameter.Value = productForm.TagBundlesPerSalesUnit;
			command.Parameters.Add(tagBundlesPerSalesUnitParameter);

		}

        protected override ProductForm DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var productForm = new ProductForm();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)ProductForm.ColumnEnum.Id] ?? long.MinValue;
				productForm.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)ProductForm.ColumnEnum.Guid] ?? Guid.Empty;
				productForm.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)ProductForm.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				productForm.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Code";
				var code = reader[(int)ProductForm.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				productForm.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)ProductForm.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				productForm.Name = (string)name;

				columnName = "ProductFormCategoryGuid";
				var productFormCategoryGuid = reader[(int)ProductForm.ColumnEnum.ProductFormCategoryGuid] ?? Guid.Empty;
				productForm.ProductFormCategoryGuid = (Guid)productFormCategoryGuid;

				columnName = "SalesUnitQty";
				var salesUnitQty = reader[(int)ProductForm.ColumnEnum.SalesUnitQty] ?? int.MinValue;
				productForm.SalesUnitQty = (int)salesUnitQty;

				columnName = "TagBundleQty";
				var tagBundleQty = reader[(int)ProductForm.ColumnEnum.TagBundleQty] ?? int.MinValue;
				productForm.TagBundleQty = (int)tagBundleQty;

				columnName = "LineItemMinimumQty";
				var lineItemMinimumQty = reader[(int)ProductForm.ColumnEnum.LineItemMinimumQty] ?? int.MinValue;
				productForm.LineItemMinimumQty = (int)lineItemMinimumQty;

				columnName = "SalesUnitsPerPackingUnit";
				var salesUnitsPerPackingUnit = reader[(int)ProductForm.ColumnEnum.SalesUnitsPerPackingUnit] ?? int.MinValue;
				productForm.SalesUnitsPerPackingUnit = (int)salesUnitsPerPackingUnit;

				columnName = "SupplierGuid";
				var supplierGuid = reader[(int)ProductForm.ColumnEnum.SupplierGuid];
				if (supplierGuid == DBNull.Value) supplierGuid = null;
				productForm.SupplierGuid = (Guid?)supplierGuid;

				columnName = "IncludesDelivery";
				var includesDelivery = reader[(int)ProductForm.ColumnEnum.IncludesDelivery] ?? false;
				productForm.IncludesDelivery = (bool)includesDelivery;

				columnName = "TagBundlesPerSalesUnit";
				var tagBundlesPerSalesUnit = reader[(int)ProductForm.ColumnEnum.TagBundlesPerSalesUnit] ?? int.MinValue;
				productForm.TagBundlesPerSalesUnit = (int)tagBundlesPerSalesUnit;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return productForm;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ProductForm productForm)
        {
			SetUpProductFormCategoryLazyLoad( productForm);
			SetUpSupplierLazyLoad( productForm);
			SetUpProductListLazyLoad( productForm);
            //SetUpFirstProductFormAvailabilityConversionListLazyLoad( productForm);
            //SetUpSecondProductFormAvailabilityConversionListLazyLoad( productForm);
            //SetUpProductFormAvailabilityLinkedWeeksListLazyLoad( productForm);
            //SetUpSupplierBoxProductFormListLazyLoad( productForm);
		}

        protected override void FixAnyLazyLoadedLists(ProductForm productForm, ListActionEnum listAction)
        {
			FixProductFormCategoryList( productForm, listAction);
			FixSupplierList( productForm, listAction);
		}

        private void SetUpProductFormCategoryLazyLoad( ProductForm productForm)
        {
            var productFormCategoryTableService = Context.Get<ProductFormCategoryTableService>();
            productForm.SetLazyProductFormCategory(new Lazy<ProductFormCategory>(() => productFormCategoryTableService.GetByGuid(productForm.ProductFormCategoryGuid), false));
        }

        private void FixProductFormCategoryList( ProductForm productForm, ListActionEnum listAction)
        {
            if (productForm.ProductFormCategoryIsLoaded)
            {
                FixLazyLoadedList(productForm.ProductFormCategory.ProductFormList, productForm, listAction);
            }
        }

        private void SetUpSupplierLazyLoad( ProductForm productForm)
        {
            var supplierTableService = Context.Get<SupplierTableService>();
            productForm.SetLazySupplier(new Lazy<Supplier>(() => supplierTableService.GetByGuid(productForm.SupplierGuid), false));
        }

        private void FixSupplierList( ProductForm productForm, ListActionEnum listAction)
        {
            if (productForm.SupplierIsLoaded)
            {
                FixLazyLoadedList(productForm.Supplier.ProductFormList, productForm, listAction);
            }
        }

        private void SetUpProductListLazyLoad(ProductForm productForm)
        {
            var productTableService = Context.Get<ProductTableService>();
            productForm.SetLazyProductList(new Lazy<List<Product>>(() => productTableService.GetAllActiveByGuid(productForm.Guid, "ProductFormGuid"), false));
        }

        //private void SetUpFirstProductFormAvailabilityConversionListLazyLoad(ProductForm productForm)
        //{
        //    var productFormAvailabilityConversionTableService = Context.Get<ProductFormAvailabilityConversionTableService>();
        //    productForm.SetLazyFirstProductFormAvailabilityConversionList(new Lazy<List<ProductFormAvailabilityConversion>>(() => productFormAvailabilityConversionTableService.GetAllActiveByGuid(productForm.Guid, "FirstProductFormGuid"), false));
        //}

        //private void SetUpSecondProductFormAvailabilityConversionListLazyLoad(ProductForm productForm)
        //{
        //    var productFormAvailabilityConversionTableService = Context.Get<ProductFormAvailabilityConversionTableService>();
        //    productForm.SetLazySecondProductFormAvailabilityConversionList(new Lazy<List<ProductFormAvailabilityConversion>>(() => productFormAvailabilityConversionTableService.GetAllActiveByGuid(productForm.Guid, "SecondProductFormGuid"), false));
        //}

        //private void SetUpProductFormAvailabilityLinkedWeeksListLazyLoad(ProductForm productForm)
        //{
        //    var productFormAvailabilityLinkedWeeksTableService = Context.Get<ProductFormAvailabilityLinkedWeeksTableService>();
        //    productForm.SetLazyProductFormAvailabilityLinkedWeeksList(new Lazy<List<ProductFormAvailabilityLinkedWeeks>>(() => productFormAvailabilityLinkedWeeksTableService.GetAllActiveByGuid(productForm.Guid, "ProductFormGuid"), false));
        //}

        //private void SetUpSupplierBoxProductFormListLazyLoad(ProductForm productForm)
        //{
        //    var supplierBoxProductFormTableService = Context.Get<SupplierBoxProductFormTableService>();
        //    productForm.SetLazySupplierBoxProductFormList(new Lazy<List<SupplierBoxProductForm>>(() => supplierBoxProductFormTableService.GetAllActiveByGuid(productForm.Guid, "ProductFormGuid"), false));
        //}

		#endregion
	}
}
