using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProgramTableService : TableService<Program>
	{
		public ProgramTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override Program DefaultInsert( Program program, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [Program]([Guid], [DateDeactivated], [Code], [Name], [ProgramTypeGuid], [SupplierGuid], [ProductFormCategoryGuid], [MinimumOrderPackingUnitQty], [OrderMultiplePackingUnitQty], [ShippingUnitWarningMinimumQty], [ShippingUnitValidMinimumQty], [AvailabilityWeekCalcMethodLookupGuid], ) VALUES (@Guid, @DateDeactivated, @Code, @Name, @ProgramTypeGuid, @SupplierGuid, @ProductFormCategoryGuid, @MinimumOrderPackingUnitQty, @OrderMultiplePackingUnitQty, @ShippingUnitWarningMinimumQty, @ShippingUnitValidMinimumQty, @AvailabilityWeekCalcMethodLookupGuid, );SELECT * FROM [Program] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, program, transaction: transaction);
		}

		protected override Program DefaultUpdate( Program program, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [Program] SET [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, [ProgramTypeGuid]=@ProgramTypeGuid, [SupplierGuid]=@SupplierGuid, [ProductFormCategoryGuid]=@ProductFormCategoryGuid, [MinimumOrderPackingUnitQty]=@MinimumOrderPackingUnitQty, [OrderMultiplePackingUnitQty]=@OrderMultiplePackingUnitQty, [ShippingUnitWarningMinimumQty]=@ShippingUnitWarningMinimumQty, [ShippingUnitValidMinimumQty]=@ShippingUnitValidMinimumQty, [AvailabilityWeekCalcMethodLookupGuid]=@AvailabilityWeekCalcMethodLookupGuid, WHERE [Guid]=@Guid;SELECT * FROM [Program] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, program, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, Program program)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = program.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = program.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = program.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 10);
			codeParameter.IsNullable = false;
			program.Code = program.Code ?? "";
			program.Code = program.Code.Trim();
			codeParameter.Value = program.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			program.Name = program.Name ?? "";
			program.Name = program.Name.Trim();
			nameParameter.Value = program.Name;
			command.Parameters.Add(nameParameter);

			var programTypeGuidParameter = new SqlParameter( "@ProgramTypeGuid", SqlDbType.UniqueIdentifier, 16);
			programTypeGuidParameter.IsNullable = false;
			programTypeGuidParameter.Value = program.ProgramTypeGuid;
			command.Parameters.Add(programTypeGuidParameter);

			var supplierGuidParameter = new SqlParameter( "@SupplierGuid", SqlDbType.UniqueIdentifier, 16);
			supplierGuidParameter.IsNullable = false;
			supplierGuidParameter.Value = program.SupplierGuid;
			command.Parameters.Add(supplierGuidParameter);

			var productFormCategoryGuidParameter = new SqlParameter( "@ProductFormCategoryGuid", SqlDbType.UniqueIdentifier, 16);
			productFormCategoryGuidParameter.IsNullable = false;
			productFormCategoryGuidParameter.Value = program.ProductFormCategoryGuid;
			command.Parameters.Add(productFormCategoryGuidParameter);

			var minimumOrderPackingUnitQtyParameter = new SqlParameter( "@MinimumOrderPackingUnitQty", SqlDbType.Int, 4);
			minimumOrderPackingUnitQtyParameter.IsNullable = false;
			minimumOrderPackingUnitQtyParameter.Value = program.MinimumOrderPackingUnitQty;
			command.Parameters.Add(minimumOrderPackingUnitQtyParameter);

			var orderMultiplePackingUnitQtyParameter = new SqlParameter( "@OrderMultiplePackingUnitQty", SqlDbType.Int, 4);
			orderMultiplePackingUnitQtyParameter.IsNullable = false;
			orderMultiplePackingUnitQtyParameter.Value = program.OrderMultiplePackingUnitQty;
			command.Parameters.Add(orderMultiplePackingUnitQtyParameter);

			var shippingUnitWarningMinimumQtyParameter = new SqlParameter( "@ShippingUnitWarningMinimumQty", SqlDbType.Int, 4);
			shippingUnitWarningMinimumQtyParameter.IsNullable = false;
			shippingUnitWarningMinimumQtyParameter.Value = program.ShippingUnitWarningMinimumQty;
			command.Parameters.Add(shippingUnitWarningMinimumQtyParameter);

			var shippingUnitValidMinimumQtyParameter = new SqlParameter( "@ShippingUnitValidMinimumQty", SqlDbType.Int, 4);
			shippingUnitValidMinimumQtyParameter.IsNullable = false;
			shippingUnitValidMinimumQtyParameter.Value = program.ShippingUnitValidMinimumQty;
			command.Parameters.Add(shippingUnitValidMinimumQtyParameter);

			var availabilityWeekCalcMethodLookupGuidParameter = new SqlParameter( "@AvailabilityWeekCalcMethodLookupGuid", SqlDbType.UniqueIdentifier, 16);
			availabilityWeekCalcMethodLookupGuidParameter.IsNullable = true;
			availabilityWeekCalcMethodLookupGuidParameter.Value = program.AvailabilityWeekCalcMethodLookupGuid ?? (object)DBNull.Value;
			command.Parameters.Add(availabilityWeekCalcMethodLookupGuidParameter);

		}

        protected override Program DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var program = new Program();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)Program.ColumnEnum.Id] ?? long.MinValue;
				program.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)Program.ColumnEnum.Guid] ?? Guid.Empty;
				program.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)Program.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				program.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Code";
				var code = reader[(int)Program.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				program.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)Program.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				program.Name = (string)name;

				columnName = "ProgramTypeGuid";
				var programTypeGuid = reader[(int)Program.ColumnEnum.ProgramTypeGuid] ?? Guid.Empty;
				program.ProgramTypeGuid = (Guid)programTypeGuid;

				columnName = "SupplierGuid";
				var supplierGuid = reader[(int)Program.ColumnEnum.SupplierGuid] ?? Guid.Empty;
				program.SupplierGuid = (Guid)supplierGuid;

				columnName = "ProductFormCategoryGuid";
				var productFormCategoryGuid = reader[(int)Program.ColumnEnum.ProductFormCategoryGuid] ?? Guid.Empty;
				program.ProductFormCategoryGuid = (Guid)productFormCategoryGuid;

				columnName = "MinimumOrderPackingUnitQty";
				var minimumOrderPackingUnitQty = reader[(int)Program.ColumnEnum.MinimumOrderPackingUnitQty] ?? int.MinValue;
				program.MinimumOrderPackingUnitQty = (int)minimumOrderPackingUnitQty;

				columnName = "OrderMultiplePackingUnitQty";
				var orderMultiplePackingUnitQty = reader[(int)Program.ColumnEnum.OrderMultiplePackingUnitQty] ?? int.MinValue;
				program.OrderMultiplePackingUnitQty = (int)orderMultiplePackingUnitQty;

				columnName = "ShippingUnitWarningMinimumQty";
				var shippingUnitWarningMinimumQty = reader[(int)Program.ColumnEnum.ShippingUnitWarningMinimumQty] ?? int.MinValue;
				program.ShippingUnitWarningMinimumQty = (int)shippingUnitWarningMinimumQty;

				columnName = "ShippingUnitValidMinimumQty";
				var shippingUnitValidMinimumQty = reader[(int)Program.ColumnEnum.ShippingUnitValidMinimumQty] ?? int.MinValue;
				program.ShippingUnitValidMinimumQty = (int)shippingUnitValidMinimumQty;

				columnName = "AvailabilityWeekCalcMethodLookupGuid";
				var availabilityWeekCalcMethodLookupGuid = reader[(int)Program.ColumnEnum.AvailabilityWeekCalcMethodLookupGuid];
				if (availabilityWeekCalcMethodLookupGuid == DBNull.Value) availabilityWeekCalcMethodLookupGuid = null;
				program.AvailabilityWeekCalcMethodLookupGuid = (Guid?)availabilityWeekCalcMethodLookupGuid;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return program;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Program program)
        {
			SetUpProgramTypeLazyLoad( program);
			SetUpSupplierLazyLoad( program);
			SetUpProductFormCategoryLazyLoad( program);
			SetUpAvailabilityWeekCalcMethodLookupLazyLoad( program);
            //SetUpFreightProgramZoneBoxCostListLazyLoad( program);
			SetUpProductListLazyLoad( program);
            //SetUpProgramCountryListLazyLoad( program);
			SetUpProgramSeasonListLazyLoad( program);
		}

        protected override void FixAnyLazyLoadedLists(Program program, ListActionEnum listAction)
        {
			FixProgramTypeList( program, listAction);
			FixSupplierList( program, listAction);
			FixProductFormCategoryList( program, listAction);
			FixAvailabilityWeekCalcMethodLookupList( program, listAction);
		}

        private void SetUpProgramTypeLazyLoad( Program program)
        {
            var programTypeTableService = Context.Get<ProgramTypeTableService>();
            program.SetLazyProgramType(new Lazy<ProgramType>(() => programTypeTableService.GetByGuid(program.ProgramTypeGuid), false));
        }

        private void FixProgramTypeList( Program program, ListActionEnum listAction)
        {
            if (program.ProgramTypeIsLoaded)
            {
                FixLazyLoadedList(program.ProgramType.ProgramList, program, listAction);
            }
        }

        private void SetUpSupplierLazyLoad( Program program)
        {
            var supplierTableService = Context.Get<SupplierTableService>();
            program.SetLazySupplier(new Lazy<Supplier>(() => supplierTableService.GetByGuid(program.SupplierGuid), false));
        }

        private void FixSupplierList( Program program, ListActionEnum listAction)
        {
            if (program.SupplierIsLoaded)
            {
                FixLazyLoadedList(program.Supplier.ProgramList, program, listAction);
            }
        }

        private void SetUpProductFormCategoryLazyLoad( Program program)
        {
            var productFormCategoryTableService = Context.Get<ProductFormCategoryTableService>();
            program.SetLazyProductFormCategory(new Lazy<ProductFormCategory>(() => productFormCategoryTableService.GetByGuid(program.ProductFormCategoryGuid), false));
        }

        private void FixProductFormCategoryList( Program program, ListActionEnum listAction)
        {
            if (program.ProductFormCategoryIsLoaded)
            {
                FixLazyLoadedList(program.ProductFormCategory.ProgramList, program, listAction);
            }
        }

        private void SetUpAvailabilityWeekCalcMethodLookupLazyLoad( Program program)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            program.SetLazyAvailabilityWeekCalcMethodLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(program.AvailabilityWeekCalcMethodLookupGuid), false));
        }

        private void FixAvailabilityWeekCalcMethodLookupList( Program program, ListActionEnum listAction)
        {
            if (program.AvailabilityWeekCalcMethodLookupIsLoaded)
            {
                FixLazyLoadedList(program.AvailabilityWeekCalcMethodLookup.AvailabilityWeekCalcMethodProgramList, program, listAction);
            }
        }

        //private void SetUpFreightProgramZoneBoxCostListLazyLoad(Program program)
        //{
        //    var freightProgramZoneBoxCostTableService = Context.Get<FreightProgramZoneBoxCostTableService>();
        //    program.SetLazyFreightProgramZoneBoxCostList(new Lazy<List<FreightProgramZoneBoxCost>>(() => freightProgramZoneBoxCostTableService.GetAllActiveByGuid(program.Guid, "ProgramGuid"), false));
        //}

        private void SetUpProductListLazyLoad(Program program)
        {
            var productTableService = Context.Get<ProductTableService>();
            program.SetLazyProductList(new Lazy<List<Product>>(() => productTableService.GetAllActiveByGuid(program.Guid, "ProgramGuid"), false));
        }

        //private void SetUpProgramCountryListLazyLoad(Program program)
        //{
        //    var programCountryTableService = Context.Get<ProgramCountryTableService>();
        //    program.SetLazyProgramCountryList(new Lazy<List<ProgramCountry>>(() => programCountryTableService.GetAllActiveByGuid(program.Guid, "ProgramGuid"), false));
        //}

        private void SetUpProgramSeasonListLazyLoad(Program program)
        {
            var programSeasonTableService = Context.Get<ProgramSeasonTableService>();
            program.SetLazyProgramSeasonList(new Lazy<List<ProgramSeason>>(() => programSeasonTableService.GetAllActiveByGuid(program.Guid, "ProgramGuid"), false));
        }

		#endregion
	}
}
