using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ShipWeekTableService : TableService<ShipWeek>
	{
		public ShipWeekTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override ShipWeek DefaultInsert( ShipWeek shipWeek, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [ShipWeek]([Guid], [DateDeactivated], [Week], [Year], [MondayDate], [ContinuousWeekNumber], [FiscalYear], ) VALUES (@Guid, @DateDeactivated, @Week, @Year, @MondayDate, @ContinuousWeekNumber, @FiscalYear, );SELECT * FROM [ShipWeek] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, shipWeek, transaction: transaction);
		}

		protected override ShipWeek DefaultUpdate( ShipWeek shipWeek, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [ShipWeek] SET [DateDeactivated]=@DateDeactivated, [Week]=@Week, [Year]=@Year, [MondayDate]=@MondayDate, [ContinuousWeekNumber]=@ContinuousWeekNumber, [FiscalYear]=@FiscalYear, WHERE [Guid]=@Guid;SELECT * FROM [ShipWeek] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, shipWeek, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, ShipWeek shipWeek)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = shipWeek.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = shipWeek.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = shipWeek.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var weekParameter = new SqlParameter( "@Week", SqlDbType.Int, 4);
			weekParameter.IsNullable = false;
			weekParameter.Value = shipWeek.Week;
			command.Parameters.Add(weekParameter);

			var yearParameter = new SqlParameter( "@Year", SqlDbType.Int, 4);
			yearParameter.IsNullable = false;
			yearParameter.Value = shipWeek.Year;
			command.Parameters.Add(yearParameter);

			var mondayDateParameter = new SqlParameter( "@MondayDate", SqlDbType.DateTime, 8);
			mondayDateParameter.IsNullable = false;
			mondayDateParameter.Value = shipWeek.MondayDate;
			command.Parameters.Add(mondayDateParameter);

			var continuousWeekNumberParameter = new SqlParameter( "@ContinuousWeekNumber", SqlDbType.Int, 4);
			continuousWeekNumberParameter.IsNullable = false;
			continuousWeekNumberParameter.Value = shipWeek.ContinuousWeekNumber;
			command.Parameters.Add(continuousWeekNumberParameter);

			var fiscalYearParameter = new SqlParameter( "@FiscalYear", SqlDbType.NVarChar, 10);
			fiscalYearParameter.IsNullable = true;
			shipWeek.FiscalYear = shipWeek.FiscalYear ?? "";
			shipWeek.FiscalYear = shipWeek.FiscalYear.Trim();
			fiscalYearParameter.Value = shipWeek.FiscalYear ?? (object)DBNull.Value;
			command.Parameters.Add(fiscalYearParameter);

		}

        protected override ShipWeek DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var shipWeek = new ShipWeek();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)ShipWeek.ColumnEnum.Id] ?? long.MinValue;
				shipWeek.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)ShipWeek.ColumnEnum.Guid] ?? Guid.Empty;
				shipWeek.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)ShipWeek.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				shipWeek.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Week";
				var week = reader[(int)ShipWeek.ColumnEnum.Week] ?? int.MinValue;
				shipWeek.Week = (int)week;

				columnName = "Year";
				var year = reader[(int)ShipWeek.ColumnEnum.Year] ?? int.MinValue;
				shipWeek.Year = (int)year;

				columnName = "MondayDate";
				var mondayDate = reader[(int)ShipWeek.ColumnEnum.MondayDate] ?? DateTime.MinValue;
				shipWeek.MondayDate = (DateTime)mondayDate;

				columnName = "ContinuousWeekNumber";
				var continuousWeekNumber = reader[(int)ShipWeek.ColumnEnum.ContinuousWeekNumber] ?? int.MinValue;
				shipWeek.ContinuousWeekNumber = (int)continuousWeekNumber;

				columnName = "FiscalYear";
				var fiscalYear = reader[(int)ShipWeek.ColumnEnum.FiscalYear];
				if (fiscalYear == DBNull.Value) fiscalYear = null;
				fiscalYear = TrimString(fiscalYear);
				shipWeek.FiscalYear = (string)fiscalYear;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return shipWeek;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ShipWeek shipWeek)
        {
			SetUpGrowerOrderListLazyLoad( shipWeek);
            //SetUpProductFormAvailabilityLinkedWeeksListLazyLoad( shipWeek);
			SetUpReportedAvailabilityListLazyLoad( shipWeek);
            //SetUpReportedAvailabilityUpdateListLazyLoad( shipWeek);
		}

        protected override void FixAnyLazyLoadedLists(ShipWeek shipWeek, ListActionEnum listAction)
        {
		}

        private void SetUpGrowerOrderListLazyLoad(ShipWeek shipWeek)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            shipWeek.SetLazyGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(shipWeek.Guid, "ShipWeekGuid"), false));
        }

        //private void SetUpProductFormAvailabilityLinkedWeeksListLazyLoad(ShipWeek shipWeek)
        //{
        //    var productFormAvailabilityLinkedWeeksTableService = Context.Get<ProductFormAvailabilityLinkedWeeksTableService>();
        //    shipWeek.SetLazyProductFormAvailabilityLinkedWeeksList(new Lazy<List<ProductFormAvailabilityLinkedWeeks>>(() => productFormAvailabilityLinkedWeeksTableService.GetAllActiveByGuid(shipWeek.Guid, "ShipWeekGuid"), false));
        //}

        private void SetUpReportedAvailabilityListLazyLoad(ShipWeek shipWeek)
        {
            var reportedAvailabilityTableService = Context.Get<ReportedAvailabilityTableService>();
            shipWeek.SetLazyReportedAvailabilityList(new Lazy<List<ReportedAvailability>>(() => reportedAvailabilityTableService.GetAllActiveByGuid(shipWeek.Guid, "ShipWeekGuid"), false));
        }

        //private void SetUpReportedAvailabilityUpdateListLazyLoad(ShipWeek shipWeek)
        //{
        //    var reportedAvailabilityUpdateTableService = Context.Get<ReportedAvailabilityUpdateTableService>();
        //    shipWeek.SetLazyReportedAvailabilityUpdateList(new Lazy<List<ReportedAvailabilityUpdate>>(() => reportedAvailabilityUpdateTableService.GetAllActiveByGuid(shipWeek.Guid, "ShipWeekGuid"), false));
        //}

		#endregion
	}
}
