using System;
using System.Collections.Generic;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public static partial class TableServices
	{
		new public enum TypeEnum
		{
			AddressTableService,
			//ChallengeTableService,
			//ChallengeEventTableService,
            //ClaimTableService,
            //ClaimOrderLineTableService,
            ConfigTableService,
			CountryTableService,
            //EmailQueueTableService,
			EventLogTableService,
			//FreightProgramZoneBoxCostTableService,
            //FreightSupplierStateZoneTableService,
			GeneticOwnerTableService,
			GrowerTableService,
			GrowerAddressTableService,
			GrowerCreditCardTableService,
			GrowerOrderTableService,
			GrowerOrderFeeTableService,
			GrowerOrderHistoryTableService,
            //GrowerOrderReminderTableService,
            //GrowerVolumeTableService,
            ImageFileTableService,
            ImageSetTableService,
            //LedgerTableService,
			LookupTableService,
			OrderLineTableService,
			PersonTableService,
			PriceTableService,
			PriceGroupTableService,
			ProductTableService,
            //ProductExclusionTableService,
			ProductFormTableService,
            //ProductFormAvailabilityConversionTableService,
            ProductFormAvailabilityLinkedWeeksTableService,
			ProductFormCategoryTableService,
            //ProductPriceGroupProgramSeasonTableService,
			ProgramTableService,
            //ProgramCountryTableService,
			ProgramSeasonTableService,
			ProgramTypeTableService,
            //PromoCodeTableService,
			ReportedAvailabilityTableService,
            //ReportedAvailabilityUpdateTableService,
            //SearchEventTableService,
            //SearchResultTableService,
            //SearchTermTableService,
            //SearchTermSearchResultTableService,
            //SearchVarietyHelperTableService,
            //SequenceTableService,
			SeriesTableService,
			ShipWeekTableService,
            SpeciesTableService,
			StateTableService,
			SupplierTableService,
            //SupplierBoxTableService,
            //SupplierBoxProductFormTableService,
			SupplierOrderTableService,
			SupplierOrderFeeTableService,
            //SynchControlTableService,
            //TripleTableService,
			VarietyTableService,
			VolumeLevelTableService,
		}

		private static List<Type> _typeList;

		new public static List<Type> TypeList
		{
			get
			{
				if (_typeList == null)
				{
					_typeList = new List<Type>
					{
						typeof( AddressTableService),
						//typeof( ChallengeTableService),
						//typeof( ChallengeEventTableService),
                        //typeof( ClaimTableService),
                        //typeof( ClaimOrderLineTableService),
                        typeof( ConfigTableService),
						typeof( CountryTableService),
                        //typeof( EmailQueueTableService),
						typeof( EventLogTableService),
						//typeof( FreightProgramZoneBoxCostTableService),
                        //typeof( FreightSupplierStateZoneTableService),
						typeof( GeneticOwnerTableService),
						typeof( GrowerTableService),
						typeof( GrowerAddressTableService),
						typeof( GrowerCreditCardTableService),
						typeof( GrowerOrderTableService),
						typeof( GrowerOrderFeeTableService),
						typeof( GrowerOrderHistoryTableService),
                        //typeof( GrowerOrderReminderTableService),
                        //typeof( GrowerVolumeTableService),
                        typeof( ImageFileTableService),
                        typeof( ImageSetTableService),
                        //typeof( LedgerTableService),
						typeof( LookupTableService),
						typeof( OrderLineTableService),
						typeof( PersonTableService),
						typeof( PriceTableService),
						typeof( PriceGroupTableService),
						typeof( ProductTableService),
                        //typeof( ProductExclusionTableService),
						typeof( ProductFormTableService),
                        //typeof( ProductFormAvailabilityConversionTableService),
                        typeof( ProductFormAvailabilityLinkedWeeksTableService),
						typeof( ProductFormCategoryTableService),
                        //typeof( ProductPriceGroupProgramSeasonTableService),
						typeof( ProgramTableService),
                        //typeof( ProgramCountryTableService),
						typeof( ProgramSeasonTableService),
						typeof( ProgramTypeTableService),
                        //typeof( PromoCodeTableService),
						typeof( ReportedAvailabilityTableService),
                        //typeof( ReportedAvailabilityUpdateTableService),
                        //typeof( SearchEventTableService),
                        //typeof( SearchResultTableService),
                        //typeof( SearchTermTableService),
                        //typeof( SearchTermSearchResultTableService),
                        //typeof( SearchVarietyHelperTableService),
                        //typeof( SequenceTableService),
						typeof( SeriesTableService),
						typeof( ShipWeekTableService),
                        typeof( SpeciesTableService),
						typeof( StateTableService),
						typeof( SupplierTableService),
                        //typeof( SupplierBoxTableService),
                        //typeof( SupplierBoxProductFormTableService),
						typeof( SupplierOrderTableService),
						typeof( SupplierOrderFeeTableService),
                        //typeof( SynchControlTableService),
                        //typeof( TripleTableService),
						typeof( VarietyTableService),
                        //typeof( VolumeLevelTableService),
					};
				}

				return _typeList;
			}
		}
	}
}
