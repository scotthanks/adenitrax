using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
    public partial class ClaimViewService : ViewService<ClaimView>
    {
        protected override ClaimView DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var claimView = new ClaimView();

            string columnName = "";
            try
            {
                columnName = "ClaimId";
                var claimId = reader[(int)ClaimView.ColumnEnum.ClaimId] ?? long.MinValue;
                claimView.ClaimId = (long)claimId;

                columnName = "ClaimGuid";
                var claimGuid = reader[(int)ClaimView.ColumnEnum.ClaimGuid] ?? Guid.Empty;
                claimView.ClaimGuid = (Guid)claimGuid;

                columnName = "GrowerOrderGuid";
                var growerOrderGuid = reader[(int)ClaimView.ColumnEnum.GrowerOrderGuid] ?? Guid.Empty;
                claimView.GrowerOrderGuid = (Guid)growerOrderGuid;

                columnName = "ClaimReasonLookupGuid";
                var claimReasonLookupGuid = reader[(int)ClaimView.ColumnEnum.ClaimReasonLookupGuid];
                if (claimReasonLookupGuid == DBNull.Value) claimReasonLookupGuid = null;
                claimView.ClaimReasonLookupGuid = (Guid?)claimReasonLookupGuid;

                columnName = "Comment";
                var comment = reader[(int)ClaimView.ColumnEnum.Comment] ?? string.Empty;
                comment = TrimString(comment);
                claimView.Comment = (string)comment;

                columnName = "ClaimDate";
                var claimDate = reader[(int)ClaimView.ColumnEnum.ClaimDate] ?? DateTime.MinValue;
                claimView.ClaimDate = (DateTime)claimDate;

                columnName = "CreditMemoLedgerGuid";
                var creditMemoLedgerGuid = reader[(int)ClaimView.ColumnEnum.CreditMemoLedgerGuid];
                if (creditMemoLedgerGuid == DBNull.Value) creditMemoLedgerGuid = null;
                claimView.CreditMemoLedgerGuid = (Guid?)creditMemoLedgerGuid;

                columnName = "ClaimStatusLookupGuid";
                var claimStatusLookupGuid = reader[(int)ClaimView.ColumnEnum.ClaimStatusLookupGuid] ?? Guid.Empty;
                claimView.ClaimStatusLookupGuid = (Guid)claimStatusLookupGuid;

                columnName = "ClaimNo";
                var claimNo = reader[(int)ClaimView.ColumnEnum.ClaimNo] ?? string.Empty;
                claimNo = TrimString(claimNo);
                claimView.ClaimNo = (string)claimNo;

                columnName = "GrowerGuid";
                var growerGuid = reader[(int)ClaimView.ColumnEnum.GrowerGuid] ?? Guid.Empty;
                claimView.GrowerGuid = (Guid)growerGuid;

                columnName = "OrderNo";
                var orderNo = reader[(int)ClaimView.ColumnEnum.OrderNo] ?? string.Empty;
                orderNo = TrimString(orderNo);
                claimView.OrderNo = (string)orderNo;

                columnName = "Description";
                var description = reader[(int)ClaimView.ColumnEnum.Description] ?? string.Empty;
                description = TrimString(description);
                claimView.Description = (string)description;

                columnName = "CustomerPoNo";
                var customerPoNo = reader[(int)ClaimView.ColumnEnum.CustomerPoNo] ?? string.Empty;
                customerPoNo = TrimString(customerPoNo);
                claimView.CustomerPoNo = (string)customerPoNo;

                columnName = "ShipWeekGuid";
                var shipWeekGuid = reader[(int)ClaimView.ColumnEnum.ShipWeekGuid] ?? Guid.Empty;
                claimView.ShipWeekGuid = (Guid)shipWeekGuid;

                columnName = "ShipWeekId";
                var shipWeekId = reader[(int)ClaimView.ColumnEnum.ShipWeekId] ?? long.MinValue;
                claimView.ShipWeekId = (long)shipWeekId;

                columnName = "ShipWeekCode";
                var shipWeekCode = reader[(int)ClaimView.ColumnEnum.ShipWeekCode];
                if (shipWeekCode == DBNull.Value) shipWeekCode = null;
                shipWeekCode = TrimString(shipWeekCode);
                claimView.ShipWeekCode = (string)shipWeekCode;

                columnName = "ProgramTypeName";
                var programTypeName = reader[(int)ClaimView.ColumnEnum.ProgramTypeName] ?? string.Empty;
                programTypeName = TrimString(programTypeName);
                claimView.ProgramTypeName = (string)programTypeName;

                columnName = "ProductFormCategoryName";
                var productFormCategoryName = reader[(int)ClaimView.ColumnEnum.ProductFormCategoryName] ?? string.Empty;
                productFormCategoryName = TrimString(productFormCategoryName);
                claimView.ProductFormCategoryName = (string)productFormCategoryName;

                columnName = "TotalQtyClaimed";
                var totalQtyClaimed = reader[(int)ClaimView.ColumnEnum.TotalQtyClaimed];
                if (totalQtyClaimed == DBNull.Value) totalQtyClaimed = null;
                claimView.TotalQtyClaimed = (int?)totalQtyClaimed;

                columnName = "ClaimAmount";
                var claimAmount = reader[(int)ClaimView.ColumnEnum.ClaimAmount];
                if (claimAmount == DBNull.Value) claimAmount = null;
                claimView.ClaimAmount = (Decimal?)claimAmount;

            }
            catch (Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return claimView;
        }
    }
}
