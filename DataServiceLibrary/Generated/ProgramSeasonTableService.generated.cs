using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class ProgramSeasonTableService : TableService<ProgramSeason>
	{
		public ProgramSeasonTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override ProgramSeason DefaultInsert( ProgramSeason programSeason, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [ProgramSeason]([Guid], [DateDeactivated], [ProgramGuid], [StartDate], [EndDate], [EODDate], [Code], [Name], [RollingWeeksEOD], [OpenWeeksDesc], [NAWeeksDesc], [SpecialRulesByForm], [SpecialRulesBySpecies], [SpecialRulesByProduct], [RunAvailabilityUpdate], [InternalStatus], ) VALUES (@Guid, @DateDeactivated, @ProgramGuid, @StartDate, @EndDate, @EODDate, @Code, @Name, @RollingWeeksEOD, @OpenWeeksDesc, @NAWeeksDesc, @SpecialRulesByForm, @SpecialRulesBySpecies, @SpecialRulesByProduct, @RunAvailabilityUpdate, @InternalStatus, );SELECT * FROM [ProgramSeason] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, programSeason, transaction: transaction);
		}

		protected override ProgramSeason DefaultUpdate( ProgramSeason programSeason, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [ProgramSeason] SET [DateDeactivated]=@DateDeactivated, [ProgramGuid]=@ProgramGuid, [StartDate]=@StartDate, [EndDate]=@EndDate, [EODDate]=@EODDate, [Code]=@Code, [Name]=@Name, [RollingWeeksEOD]=@RollingWeeksEOD, [OpenWeeksDesc]=@OpenWeeksDesc, [NAWeeksDesc]=@NAWeeksDesc, [SpecialRulesByForm]=@SpecialRulesByForm, [SpecialRulesBySpecies]=@SpecialRulesBySpecies, [SpecialRulesByProduct]=@SpecialRulesByProduct, [RunAvailabilityUpdate]=@RunAvailabilityUpdate, [InternalStatus]=@InternalStatus, WHERE [Guid]=@Guid;SELECT * FROM [ProgramSeason] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, programSeason, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, ProgramSeason programSeason)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = programSeason.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = programSeason.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = programSeason.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var programGuidParameter = new SqlParameter( "@ProgramGuid", SqlDbType.UniqueIdentifier, 16);
			programGuidParameter.IsNullable = false;
			programGuidParameter.Value = programSeason.ProgramGuid;
			command.Parameters.Add(programGuidParameter);

			var startDateParameter = new SqlParameter( "@StartDate", SqlDbType.DateTime, 8);
			startDateParameter.IsNullable = false;
			startDateParameter.Value = programSeason.StartDate;
			command.Parameters.Add(startDateParameter);

			var endDateParameter = new SqlParameter( "@EndDate", SqlDbType.DateTime, 8);
			endDateParameter.IsNullable = false;
			endDateParameter.Value = programSeason.EndDate;
			command.Parameters.Add(endDateParameter);

			var eODDateParameter = new SqlParameter( "@EODDate", SqlDbType.DateTime, 8);
			eODDateParameter.IsNullable = false;
			eODDateParameter.Value = programSeason.EODDate;
			command.Parameters.Add(eODDateParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 20);
			codeParameter.IsNullable = false;
			programSeason.Code = programSeason.Code ?? "";
			programSeason.Code = programSeason.Code.Trim();
			codeParameter.Value = programSeason.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 50);
			nameParameter.IsNullable = false;
			programSeason.Name = programSeason.Name ?? "";
			programSeason.Name = programSeason.Name.Trim();
			nameParameter.Value = programSeason.Name;
			command.Parameters.Add(nameParameter);

			var rollingWeeksEODParameter = new SqlParameter( "@RollingWeeksEOD", SqlDbType.Int, 4);
			rollingWeeksEODParameter.IsNullable = false;
			rollingWeeksEODParameter.Value = programSeason.RollingWeeksEOD;
			command.Parameters.Add(rollingWeeksEODParameter);

			var openWeeksDescParameter = new SqlParameter( "@OpenWeeksDesc", SqlDbType.NVarChar, 30);
			openWeeksDescParameter.IsNullable = false;
			programSeason.OpenWeeksDesc = programSeason.OpenWeeksDesc ?? "";
			programSeason.OpenWeeksDesc = programSeason.OpenWeeksDesc.Trim();
			openWeeksDescParameter.Value = programSeason.OpenWeeksDesc;
			command.Parameters.Add(openWeeksDescParameter);

			var nAWeeksDescParameter = new SqlParameter( "@NAWeeksDesc", SqlDbType.NVarChar, 30);
			nAWeeksDescParameter.IsNullable = false;
			programSeason.NAWeeksDesc = programSeason.NAWeeksDesc ?? "";
			programSeason.NAWeeksDesc = programSeason.NAWeeksDesc.Trim();
			nAWeeksDescParameter.Value = programSeason.NAWeeksDesc;
			command.Parameters.Add(nAWeeksDescParameter);

			var specialRulesByFormParameter = new SqlParameter( "@SpecialRulesByForm", SqlDbType.NVarChar, 500);
			specialRulesByFormParameter.IsNullable = false;
			programSeason.SpecialRulesByForm = programSeason.SpecialRulesByForm ?? "";
			programSeason.SpecialRulesByForm = programSeason.SpecialRulesByForm.Trim();
			specialRulesByFormParameter.Value = programSeason.SpecialRulesByForm;
			command.Parameters.Add(specialRulesByFormParameter);

			var specialRulesBySpeciesParameter = new SqlParameter( "@SpecialRulesBySpecies", SqlDbType.NVarChar, 500);
			specialRulesBySpeciesParameter.IsNullable = false;
			programSeason.SpecialRulesBySpecies = programSeason.SpecialRulesBySpecies ?? "";
			programSeason.SpecialRulesBySpecies = programSeason.SpecialRulesBySpecies.Trim();
			specialRulesBySpeciesParameter.Value = programSeason.SpecialRulesBySpecies;
			command.Parameters.Add(specialRulesBySpeciesParameter);

			var specialRulesByProductParameter = new SqlParameter( "@SpecialRulesByProduct", SqlDbType.NVarChar, -1);
			specialRulesByProductParameter.IsNullable = false;
			programSeason.SpecialRulesByProduct = programSeason.SpecialRulesByProduct ?? "";
			programSeason.SpecialRulesByProduct = programSeason.SpecialRulesByProduct.Trim();
			specialRulesByProductParameter.Value = programSeason.SpecialRulesByProduct;
			command.Parameters.Add(specialRulesByProductParameter);

			var runAvailabilityUpdateParameter = new SqlParameter( "@RunAvailabilityUpdate", SqlDbType.Bit, 1);
			runAvailabilityUpdateParameter.IsNullable = false;
			runAvailabilityUpdateParameter.Value = programSeason.RunAvailabilityUpdate;
			command.Parameters.Add(runAvailabilityUpdateParameter);

			var internalStatusParameter = new SqlParameter( "@InternalStatus", SqlDbType.NVarChar, 20);
			internalStatusParameter.IsNullable = false;
			programSeason.InternalStatus = programSeason.InternalStatus ?? "";
			programSeason.InternalStatus = programSeason.InternalStatus.Trim();
			internalStatusParameter.Value = programSeason.InternalStatus;
			command.Parameters.Add(internalStatusParameter);

		}

        protected override ProgramSeason DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var programSeason = new ProgramSeason();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)ProgramSeason.ColumnEnum.Id] ?? long.MinValue;
				programSeason.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)ProgramSeason.ColumnEnum.Guid] ?? Guid.Empty;
				programSeason.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)ProgramSeason.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				programSeason.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "ProgramGuid";
				var programGuid = reader[(int)ProgramSeason.ColumnEnum.ProgramGuid] ?? Guid.Empty;
				programSeason.ProgramGuid = (Guid)programGuid;

				columnName = "StartDate";
				var startDate = reader[(int)ProgramSeason.ColumnEnum.StartDate] ?? DateTime.MinValue;
				programSeason.StartDate = (DateTime)startDate;

				columnName = "EndDate";
				var endDate = reader[(int)ProgramSeason.ColumnEnum.EndDate] ?? DateTime.MinValue;
				programSeason.EndDate = (DateTime)endDate;

				columnName = "EODDate";
				var eODDate = reader[(int)ProgramSeason.ColumnEnum.EODDate] ?? DateTime.MinValue;
				programSeason.EODDate = (DateTime)eODDate;

				columnName = "Code";
				var code = reader[(int)ProgramSeason.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				programSeason.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)ProgramSeason.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				programSeason.Name = (string)name;

				columnName = "RollingWeeksEOD";
				var rollingWeeksEOD = reader[(int)ProgramSeason.ColumnEnum.RollingWeeksEOD] ?? int.MinValue;
				programSeason.RollingWeeksEOD = (int)rollingWeeksEOD;

				columnName = "OpenWeeksDesc";
				var openWeeksDesc = reader[(int)ProgramSeason.ColumnEnum.OpenWeeksDesc] ?? string.Empty;
				openWeeksDesc = TrimString(openWeeksDesc);
				programSeason.OpenWeeksDesc = (string)openWeeksDesc;

				columnName = "NAWeeksDesc";
				var nAWeeksDesc = reader[(int)ProgramSeason.ColumnEnum.NAWeeksDesc] ?? string.Empty;
				nAWeeksDesc = TrimString(nAWeeksDesc);
				programSeason.NAWeeksDesc = (string)nAWeeksDesc;

				columnName = "SpecialRulesByForm";
				var specialRulesByForm = reader[(int)ProgramSeason.ColumnEnum.SpecialRulesByForm] ?? string.Empty;
				specialRulesByForm = TrimString(specialRulesByForm);
				programSeason.SpecialRulesByForm = (string)specialRulesByForm;

				columnName = "SpecialRulesBySpecies";
				var specialRulesBySpecies = reader[(int)ProgramSeason.ColumnEnum.SpecialRulesBySpecies] ?? string.Empty;
				specialRulesBySpecies = TrimString(specialRulesBySpecies);
				programSeason.SpecialRulesBySpecies = (string)specialRulesBySpecies;

				columnName = "SpecialRulesByProduct";
				var specialRulesByProduct = reader[(int)ProgramSeason.ColumnEnum.SpecialRulesByProduct] ?? string.Empty;
				specialRulesByProduct = TrimString(specialRulesByProduct);
				programSeason.SpecialRulesByProduct = (string)specialRulesByProduct;

				columnName = "RunAvailabilityUpdate";
				var runAvailabilityUpdate = reader[(int)ProgramSeason.ColumnEnum.RunAvailabilityUpdate] ?? false;
				programSeason.RunAvailabilityUpdate = (bool)runAvailabilityUpdate;

				columnName = "InternalStatus";
				var internalStatus = reader[(int)ProgramSeason.ColumnEnum.InternalStatus] ?? string.Empty;
				internalStatus = TrimString(internalStatus);
				programSeason.InternalStatus = (string)internalStatus;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return programSeason;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(ProgramSeason programSeason)
        {
			SetUpProgramLazyLoad( programSeason);
            //SetUpGrowerVolumeListLazyLoad( programSeason);
            //SetUpProductPriceGroupProgramSeasonListLazyLoad( programSeason);
            //SetUpVolumeLevelListLazyLoad( programSeason);
		}

        protected override void FixAnyLazyLoadedLists(ProgramSeason programSeason, ListActionEnum listAction)
        {
			FixProgramList( programSeason, listAction);
		}

        private void SetUpProgramLazyLoad( ProgramSeason programSeason)
        {
            var programTableService = Context.Get<ProgramTableService>();
            programSeason.SetLazyProgram(new Lazy<Program>(() => programTableService.GetByGuid(programSeason.ProgramGuid), false));
        }

        private void FixProgramList( ProgramSeason programSeason, ListActionEnum listAction)
        {
            if (programSeason.ProgramIsLoaded)
            {
                FixLazyLoadedList(programSeason.Program.ProgramSeasonList, programSeason, listAction);
            }
        }

        //private void SetUpGrowerVolumeListLazyLoad(ProgramSeason programSeason)
        //{
        //    var growerVolumeTableService = Context.Get<GrowerVolumeTableService>();
        //    programSeason.SetLazyGrowerVolumeList(new Lazy<List<GrowerVolume>>(() => growerVolumeTableService.GetAllActiveByGuid(programSeason.Guid, "ProgramSeasonGuid"), false));
        //}

        //private void SetUpProductPriceGroupProgramSeasonListLazyLoad(ProgramSeason programSeason)
        //{
        //    var productPriceGroupProgramSeasonTableService = Context.Get<ProductPriceGroupProgramSeasonTableService>();
        //    programSeason.SetLazyProductPriceGroupProgramSeasonList(new Lazy<List<ProductPriceGroupProgramSeason>>(() => productPriceGroupProgramSeasonTableService.GetAllActiveByGuid(programSeason.Guid, "ProgramSeasonGuid"), false));
        //}

        //private void SetUpVolumeLevelListLazyLoad(ProgramSeason programSeason)
        //{
        //    var volumeLevelTableService = Context.Get<VolumeLevelTableService>();
        //    programSeason.SetLazyVolumeLevelList(new Lazy<List<VolumeLevel>>(() => volumeLevelTableService.GetAllActiveByGuid(programSeason.Guid, "ProgramSeasonGuid"), false));
        //}

		#endregion
	}
}
