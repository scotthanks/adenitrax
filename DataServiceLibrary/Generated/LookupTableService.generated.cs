using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class LookupTableService : TableService<Lookup>
	{
		public LookupTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override Lookup DefaultInsert( Lookup lookup, SqlTransaction transaction = null)
		{
			string sqlInsertCommand = "INSERT INTO [Lookup]([Guid], [ParentLookupGuid], [DateDeactivated], [Code], [Name], [SortSequence], [Path], [SequenceChildren], [CategoryLookupGuid], ) VALUES (@Guid, @ParentLookupGuid, @DateDeactivated, @Code, @Name, @SortSequence, @Path, @SequenceChildren, @CategoryLookupGuid, );SELECT * FROM [Lookup] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, lookup, transaction: transaction);
		}

		protected override Lookup DefaultUpdate( Lookup lookup, SqlTransaction transaction = null)
		{
			string sqlUpdateCommand = "UPDATE [Lookup] SET [ParentLookupGuid]=@ParentLookupGuid, [DateDeactivated]=@DateDeactivated, [Code]=@Code, [Name]=@Name, [SortSequence]=@SortSequence, [Path]=@Path, [SequenceChildren]=@SequenceChildren, [CategoryLookupGuid]=@CategoryLookupGuid, WHERE [Guid]=@Guid;SELECT * FROM [Lookup] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, lookup, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, Lookup lookup)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = lookup.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = lookup.Guid;
			command.Parameters.Add(guidParameter);

			var parentLookupGuidParameter = new SqlParameter( "@ParentLookupGuid", SqlDbType.UniqueIdentifier, 16);
			parentLookupGuidParameter.IsNullable = true;
			parentLookupGuidParameter.Value = lookup.ParentLookupGuid ?? (object)DBNull.Value;
			command.Parameters.Add(parentLookupGuidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = lookup.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var codeParameter = new SqlParameter( "@Code", SqlDbType.NChar, 50);
			codeParameter.IsNullable = false;
			lookup.Code = lookup.Code ?? "";
			lookup.Code = lookup.Code.Trim();
			codeParameter.Value = lookup.Code;
			command.Parameters.Add(codeParameter);

			var nameParameter = new SqlParameter( "@Name", SqlDbType.NVarChar, 100);
			nameParameter.IsNullable = false;
			lookup.Name = lookup.Name ?? "";
			lookup.Name = lookup.Name.Trim();
			nameParameter.Value = lookup.Name;
			command.Parameters.Add(nameParameter);

			var sortSequenceParameter = new SqlParameter( "@SortSequence", SqlDbType.Float, 8);
			sortSequenceParameter.IsNullable = false;
			sortSequenceParameter.Value = lookup.SortSequence;
			command.Parameters.Add(sortSequenceParameter);

			var pathParameter = new SqlParameter( "@Path", SqlDbType.NVarChar, 200);
			pathParameter.IsNullable = true;
			lookup.Path = lookup.Path ?? "";
			lookup.Path = lookup.Path.Trim();
			pathParameter.Value = lookup.Path ?? (object)DBNull.Value;
			command.Parameters.Add(pathParameter);

			var sequenceChildrenParameter = new SqlParameter( "@SequenceChildren", SqlDbType.Bit, 1);
			sequenceChildrenParameter.IsNullable = false;
			sequenceChildrenParameter.Value = lookup.SequenceChildren;
			command.Parameters.Add(sequenceChildrenParameter);

			var categoryLookupGuidParameter = new SqlParameter( "@CategoryLookupGuid", SqlDbType.UniqueIdentifier, 16);
			categoryLookupGuidParameter.IsNullable = true;
			categoryLookupGuidParameter.Value = lookup.CategoryLookupGuid ?? (object)DBNull.Value;
			command.Parameters.Add(categoryLookupGuidParameter);

		}

        protected override Lookup DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var lookup = new Lookup();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)Lookup.ColumnEnum.Id] ?? long.MinValue;
				lookup.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)Lookup.ColumnEnum.Guid] ?? Guid.Empty;
				lookup.Guid = (Guid)guid;

				columnName = "ParentLookupGuid";
				var parentLookupGuid = reader[(int)Lookup.ColumnEnum.ParentLookupGuid];
				if (parentLookupGuid == DBNull.Value) parentLookupGuid = null;
				lookup.ParentLookupGuid = (Guid?)parentLookupGuid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)Lookup.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				lookup.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "Code";
				var code = reader[(int)Lookup.ColumnEnum.Code] ?? string.Empty;
				code = TrimString(code);
				lookup.Code = (string)code;

				columnName = "Name";
				var name = reader[(int)Lookup.ColumnEnum.Name] ?? string.Empty;
				name = TrimString(name);
				lookup.Name = (string)name;

				columnName = "SortSequence";
				var sortSequence = reader[(int)Lookup.ColumnEnum.SortSequence] ?? Double.MinValue;
				lookup.SortSequence = (Double)sortSequence;

				columnName = "Path";
				var path = reader[(int)Lookup.ColumnEnum.Path];
				if (path == DBNull.Value) path = null;
				path = TrimString(path);
				lookup.Path = (string)path;

				columnName = "SequenceChildren";
				var sequenceChildren = reader[(int)Lookup.ColumnEnum.SequenceChildren] ?? false;
				lookup.SequenceChildren = (bool)sequenceChildren;

				columnName = "CategoryLookupGuid";
				var categoryLookupGuid = reader[(int)Lookup.ColumnEnum.CategoryLookupGuid];
				if (categoryLookupGuid == DBNull.Value) categoryLookupGuid = null;
				lookup.CategoryLookupGuid = (Guid?)categoryLookupGuid;

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return lookup;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(Lookup lookup)
        {
			SetUpParentLookupLazyLoad( lookup);
			SetUpCategoryLookupLazyLoad( lookup);
            //SetUpClaimReasonClaimListLazyLoad( lookup);
            //SetUpClaimStatusClaimListLazyLoad( lookup);
			SetUpLogTypeEventLogListLazyLoad( lookup);
			SetUpObjectTypeEventLogListLazyLoad( lookup);
			SetUpProcessEventLogListLazyLoad( lookup);
			SetUpVerbosityEventLogListLazyLoad( lookup);
			SetUpPriorityEventLogListLazyLoad( lookup);
			SetUpSeverityEventLogListLazyLoad( lookup);
            //SetUpZoneFreightProgramZoneBoxCostListLazyLoad( lookup);
            //SetUpZoneFreightSupplierStateZoneListLazyLoad( lookup);
			SetUpGrowerTypeGrowerListLazyLoad( lookup);
			SetUpDefaultPaymentTypeGrowerListLazyLoad( lookup);
			SetUpRequestedLineOfCreditStatusGrowerListLazyLoad( lookup);
			SetUpGrowerSizeGrowerListLazyLoad( lookup);
			SetUpOrderTypeGrowerOrderListLazyLoad( lookup);
			SetUpPaymentTypeGrowerOrderListLazyLoad( lookup);
			SetUpGrowerOrderStatusGrowerOrderListLazyLoad( lookup);
			SetUpFeeTypeGrowerOrderFeeListLazyLoad( lookup);
			SetUpFeeStatXGrowerOrderFeeListLazyLoad( lookup);
			SetUpLogTypeGrowerOrderHistoryListLazyLoad( lookup);
			SetUpFileExtensionImageFileListLazyLoad( lookup);
			SetUpImageFileTypeImageFileListLazyLoad( lookup);
			SetUpImageSetTypeImageSetListLazyLoad( lookup);
            //SetUpEntryTypeLedgerListLazyLoad( lookup);
            //SetUpGLAccountLedgerListLazyLoad( lookup);
			SetUpOrderLineStatusOrderLineListLazyLoad( lookup);
			SetUpChangeTypeOrderLineListLazyLoad( lookup);
			SetUpPersonTypePersonListLazyLoad( lookup);
			SetUpAvailabilityWeekCalcMethodProductFormCategoryListLazyLoad( lookup);
			SetUpAvailabilityWeekCalcMethodProgramListLazyLoad( lookup);
			SetUpAvailabilityTypeReportedAvailabilityListLazyLoad( lookup);
            //SetUpAvailabilityTypeReportedAvailabilityUpdateListLazyLoad( lookup);
			SetUpSupplierOrderStatusSupplierOrderListLazyLoad( lookup);
			SetUpTagRatioSupplierOrderListLazyLoad( lookup);
			SetUpShipMethodSupplierOrderListLazyLoad( lookup);
			SetUpFeeTypeSupplierOrderFeeListLazyLoad( lookup);
			SetUpFeeStatXSupplierOrderFeeListLazyLoad( lookup);
            //SetUpPredicateTripleListLazyLoad( lookup);
			SetUpColorVarietyListLazyLoad( lookup);
			SetUpHabitVarietyListLazyLoad( lookup);
			SetUpVigorVarietyListLazyLoad( lookup);
			SetUpTimingVarietyListLazyLoad( lookup);
			SetUpZoneVarietyListLazyLoad( lookup);
			SetUpBrandVarietyListLazyLoad( lookup);
            //SetUpVolumeLevelTypeVolumeLevelListLazyLoad( lookup);
		}

        protected override void FixAnyLazyLoadedLists(Lookup lookup, ListActionEnum listAction)
        {
			FixParentLookupList( lookup, listAction);
			FixCategoryLookupList( lookup, listAction);
		}

        private void SetUpParentLookupLazyLoad( Lookup lookup)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            lookup.SetLazyParentLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(lookup.ParentLookupGuid), false));
        }

        private void FixParentLookupList( Lookup lookup, ListActionEnum listAction)
        {
            if (lookup.ParentLookupIsLoaded)
            {
                FixLazyLoadedList(lookup.ParentLookup.ParentLookupList, lookup, listAction);
            }
        }

        private void SetUpCategoryLookupLazyLoad( Lookup lookup)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            lookup.SetLazyCategoryLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(lookup.CategoryLookupGuid), false));
        }

        private void FixCategoryLookupList( Lookup lookup, ListActionEnum listAction)
        {
            if (lookup.CategoryLookupIsLoaded)
            {
                FixLazyLoadedList(lookup.CategoryLookup.CategoryLookupList, lookup, listAction);
            }
        }

        //private void SetUpClaimReasonClaimListLazyLoad(Lookup lookup)
        //{
        //    var claimTableService = Context.Get<ClaimTableService>();
        //    lookup.SetLazyClaimReasonClaimList(new Lazy<List<Claim>>(() => claimTableService.GetAllActiveByGuid(lookup.Guid, "ClaimReasonLookupGuid"), false));
        //}

        //private void SetUpClaimStatusClaimListLazyLoad(Lookup lookup)
        //{
        //    var claimTableService = Context.Get<ClaimTableService>();
        //    lookup.SetLazyClaimStatusClaimList(new Lazy<List<Claim>>(() => claimTableService.GetAllActiveByGuid(lookup.Guid, "ClaimStatusLookupGuid"), false));
        //}

        private void SetUpLogTypeEventLogListLazyLoad(Lookup lookup)
        {
            var eventLogTableService = Context.Get<EventLogTableService>();
            lookup.SetLazyLogTypeEventLogList(new Lazy<List<EventLog>>(() => eventLogTableService.GetAllActiveByGuid(lookup.Guid, "LogTypeLookupGuid"), false));
        }

        private void SetUpObjectTypeEventLogListLazyLoad(Lookup lookup)
        {
            var eventLogTableService = Context.Get<EventLogTableService>();
            lookup.SetLazyObjectTypeEventLogList(new Lazy<List<EventLog>>(() => eventLogTableService.GetAllActiveByGuid(lookup.Guid, "ObjectTypeLookupGuid"), false));
        }

        private void SetUpProcessEventLogListLazyLoad(Lookup lookup)
        {
            var eventLogTableService = Context.Get<EventLogTableService>();
            lookup.SetLazyProcessEventLogList(new Lazy<List<EventLog>>(() => eventLogTableService.GetAllActiveByGuid(lookup.Guid, "ProcessLookupGuid"), false));
        }

        private void SetUpVerbosityEventLogListLazyLoad(Lookup lookup)
        {
            var eventLogTableService = Context.Get<EventLogTableService>();
            lookup.SetLazyVerbosityEventLogList(new Lazy<List<EventLog>>(() => eventLogTableService.GetAllActiveByGuid(lookup.Guid, "VerbosityLookupGuid"), false));
        }

        private void SetUpPriorityEventLogListLazyLoad(Lookup lookup)
        {
            var eventLogTableService = Context.Get<EventLogTableService>();
            lookup.SetLazyPriorityEventLogList(new Lazy<List<EventLog>>(() => eventLogTableService.GetAllActiveByGuid(lookup.Guid, "PriorityLookupGuid"), false));
        }

        private void SetUpSeverityEventLogListLazyLoad(Lookup lookup)
        {
            var eventLogTableService = Context.Get<EventLogTableService>();
            lookup.SetLazySeverityEventLogList(new Lazy<List<EventLog>>(() => eventLogTableService.GetAllActiveByGuid(lookup.Guid, "SeverityLookupGuid"), false));
        }

        //private void SetUpZoneFreightProgramZoneBoxCostListLazyLoad(Lookup lookup)
        //{
        //    var freightProgramZoneBoxCostTableService = Context.Get<FreightProgramZoneBoxCostTableService>();
        //    lookup.SetLazyZoneFreightProgramZoneBoxCostList(new Lazy<List<FreightProgramZoneBoxCost>>(() => freightProgramZoneBoxCostTableService.GetAllActiveByGuid(lookup.Guid, "ZoneLookupGuid"), false));
        //}

        //private void SetUpZoneFreightSupplierStateZoneListLazyLoad(Lookup lookup)
        //{
        //    var freightSupplierStateZoneTableService = Context.Get<FreightSupplierStateZoneTableService>();
        //    lookup.SetLazyZoneFreightSupplierStateZoneList(new Lazy<List<FreightSupplierStateZone>>(() => freightSupplierStateZoneTableService.GetAllActiveByGuid(lookup.Guid, "ZoneLookupGuid"), false));
        //}

        private void SetUpGrowerTypeGrowerListLazyLoad(Lookup lookup)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            lookup.SetLazyGrowerTypeGrowerList(new Lazy<List<Grower>>(() => growerTableService.GetAllActiveByGuid(lookup.Guid, "GrowerTypeLookupGuid"), false));
        }

        private void SetUpDefaultPaymentTypeGrowerListLazyLoad(Lookup lookup)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            lookup.SetLazyDefaultPaymentTypeGrowerList(new Lazy<List<Grower>>(() => growerTableService.GetAllActiveByGuid(lookup.Guid, "DefaultPaymentTypeLookupGuid"), false));
        }

        private void SetUpRequestedLineOfCreditStatusGrowerListLazyLoad(Lookup lookup)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            lookup.SetLazyRequestedLineOfCreditStatusGrowerList(new Lazy<List<Grower>>(() => growerTableService.GetAllActiveByGuid(lookup.Guid, "RequestedLineOfCreditStatusLookupGuid"), false));
        }

        private void SetUpGrowerSizeGrowerListLazyLoad(Lookup lookup)
        {
            var growerTableService = Context.Get<GrowerTableService>();
            lookup.SetLazyGrowerSizeGrowerList(new Lazy<List<Grower>>(() => growerTableService.GetAllActiveByGuid(lookup.Guid, "GrowerSizeLookupGuid"), false));
        }

        private void SetUpOrderTypeGrowerOrderListLazyLoad(Lookup lookup)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            lookup.SetLazyOrderTypeGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(lookup.Guid, "OrderTypeLookupGuid"), false));
        }

        private void SetUpPaymentTypeGrowerOrderListLazyLoad(Lookup lookup)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            lookup.SetLazyPaymentTypeGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(lookup.Guid, "PaymentTypeLookupGuid"), false));
        }

        private void SetUpGrowerOrderStatusGrowerOrderListLazyLoad(Lookup lookup)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            lookup.SetLazyGrowerOrderStatusGrowerOrderList(new Lazy<List<GrowerOrder>>(() => growerOrderTableService.GetAllActiveByGuid(lookup.Guid, "GrowerOrderStatusLookupGuid"), false));
        }

        private void SetUpFeeTypeGrowerOrderFeeListLazyLoad(Lookup lookup)
        {
            var growerOrderFeeTableService = Context.Get<GrowerOrderFeeTableService>();
            lookup.SetLazyFeeTypeGrowerOrderFeeList(new Lazy<List<GrowerOrderFee>>(() => growerOrderFeeTableService.GetAllActiveByGuid(lookup.Guid, "FeeTypeLookupGuid"), false));
        }

        private void SetUpFeeStatXGrowerOrderFeeListLazyLoad(Lookup lookup)
        {
            var growerOrderFeeTableService = Context.Get<GrowerOrderFeeTableService>();
            lookup.SetLazyFeeStatXGrowerOrderFeeList(new Lazy<List<GrowerOrderFee>>(() => growerOrderFeeTableService.GetAllActiveByGuid(lookup.Guid, "FeeStatXLookupGuid"), false));
        }

        private void SetUpLogTypeGrowerOrderHistoryListLazyLoad(Lookup lookup)
        {
            var growerOrderHistoryTableService = Context.Get<GrowerOrderHistoryTableService>();
            lookup.SetLazyLogTypeGrowerOrderHistoryList(new Lazy<List<GrowerOrderHistory>>(() => growerOrderHistoryTableService.GetAllActiveByGuid(lookup.Guid, "LogTypeLookupGuid"), false));
        }

        private void SetUpFileExtensionImageFileListLazyLoad(Lookup lookup)
        {
            var imageFileTableService = Context.Get<ImageFileTableService>();
            lookup.SetLazyFileExtensionImageFileList(new Lazy<List<ImageFile>>(() => imageFileTableService.GetAllActiveByGuid(lookup.Guid, "FileExtensionLookupGuid"), false));
        }

        private void SetUpImageFileTypeImageFileListLazyLoad(Lookup lookup)
        {
            var imageFileTableService = Context.Get<ImageFileTableService>();
            lookup.SetLazyImageFileTypeImageFileList(new Lazy<List<ImageFile>>(() => imageFileTableService.GetAllActiveByGuid(lookup.Guid, "ImageFileTypeLookupGuid"), false));
        }

        private void SetUpImageSetTypeImageSetListLazyLoad(Lookup lookup)
        {
            var imageSetTableService = Context.Get<ImageSetTableService>();
            lookup.SetLazyImageSetTypeImageSetList(new Lazy<List<ImageSet>>(() => imageSetTableService.GetAllActiveByGuid(lookup.Guid, "ImageSetTypeLookupGuid"), false));
        }

        //private void SetUpEntryTypeLedgerListLazyLoad(Lookup lookup)
        //{
        //    var ledgerTableService = Context.Get<LedgerTableService>();
        //    lookup.SetLazyEntryTypeLedgerList(new Lazy<List<Ledger>>(() => ledgerTableService.GetAllActiveByGuid(lookup.Guid, "EntryTypeLookupGuid"), false));
        //}

        //private void SetUpGLAccountLedgerListLazyLoad(Lookup lookup)
        //{
        //    var ledgerTableService = Context.Get<LedgerTableService>();
        //    lookup.SetLazyGLAccountLedgerList(new Lazy<List<Ledger>>(() => ledgerTableService.GetAllActiveByGuid(lookup.Guid, "GLAccountLookupGuid"), false));
        //}

        private void SetUpParentLookupListLazyLoad(Lookup lookup)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            lookup.SetLazyParentLookupList(new Lazy<List<Lookup>>(() => lookupTableService.GetAllActiveByGuid(lookup.Guid, "ParentLookupGuid"), false));
        }

        private void SetUpCategoryLookupListLazyLoad(Lookup lookup)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            lookup.SetLazyCategoryLookupList(new Lazy<List<Lookup>>(() => lookupTableService.GetAllActiveByGuid(lookup.Guid, "CategoryLookupGuid"), false));
        }

        private void SetUpOrderLineStatusOrderLineListLazyLoad(Lookup lookup)
        {
            var orderLineTableService = Context.Get<OrderLineTableService>();
            lookup.SetLazyOrderLineStatusOrderLineList(new Lazy<List<OrderLine>>(() => orderLineTableService.GetAllActiveByGuid(lookup.Guid, "OrderLineStatusLookupGuid"), false));
        }

        private void SetUpChangeTypeOrderLineListLazyLoad(Lookup lookup)
        {
            var orderLineTableService = Context.Get<OrderLineTableService>();
            lookup.SetLazyChangeTypeOrderLineList(new Lazy<List<OrderLine>>(() => orderLineTableService.GetAllActiveByGuid(lookup.Guid, "ChangeTypeLookupGuid"), false));
        }

        private void SetUpPersonTypePersonListLazyLoad(Lookup lookup)
        {
            var personTableService = Context.Get<PersonTableService>();
            lookup.SetLazyPersonTypePersonList(new Lazy<List<Person>>(() => personTableService.GetAllActiveByGuid(lookup.Guid, "PersonTypeLookupGuid"), false));
        }

        private void SetUpAvailabilityWeekCalcMethodProductFormCategoryListLazyLoad(Lookup lookup)
        {
            var productFormCategoryTableService = Context.Get<ProductFormCategoryTableService>();
            lookup.SetLazyAvailabilityWeekCalcMethodProductFormCategoryList(new Lazy<List<ProductFormCategory>>(() => productFormCategoryTableService.GetAllActiveByGuid(lookup.Guid, "AvailabilityWeekCalcMethodLookupGuid"), false));
        }

        private void SetUpAvailabilityWeekCalcMethodProgramListLazyLoad(Lookup lookup)
        {
            var programTableService = Context.Get<ProgramTableService>();
            lookup.SetLazyAvailabilityWeekCalcMethodProgramList(new Lazy<List<Program>>(() => programTableService.GetAllActiveByGuid(lookup.Guid, "AvailabilityWeekCalcMethodLookupGuid"), false));
        }

        private void SetUpAvailabilityTypeReportedAvailabilityListLazyLoad(Lookup lookup)
        {
            var reportedAvailabilityTableService = Context.Get<ReportedAvailabilityTableService>();
            lookup.SetLazyAvailabilityTypeReportedAvailabilityList(new Lazy<List<ReportedAvailability>>(() => reportedAvailabilityTableService.GetAllActiveByGuid(lookup.Guid, "AvailabilityTypeLookupGuid"), false));
        }

        //private void SetUpAvailabilityTypeReportedAvailabilityUpdateListLazyLoad(Lookup lookup)
        //{
        //    var reportedAvailabilityUpdateTableService = Context.Get<ReportedAvailabilityUpdateTableService>();
        //    lookup.SetLazyAvailabilityTypeReportedAvailabilityUpdateList(new Lazy<List<ReportedAvailabilityUpdate>>(() => reportedAvailabilityUpdateTableService.GetAllActiveByGuid(lookup.Guid, "AvailabilityTypeLookupGuid"), false));
        //}

        private void SetUpSupplierOrderStatusSupplierOrderListLazyLoad(Lookup lookup)
        {
            var supplierOrderTableService = Context.Get<SupplierOrderTableService>();
            lookup.SetLazySupplierOrderStatusSupplierOrderList(new Lazy<List<SupplierOrder>>(() => supplierOrderTableService.GetAllActiveByGuid(lookup.Guid, "SupplierOrderStatusLookupGuid"), false));
        }

        private void SetUpTagRatioSupplierOrderListLazyLoad(Lookup lookup)
        {
            var supplierOrderTableService = Context.Get<SupplierOrderTableService>();
            lookup.SetLazyTagRatioSupplierOrderList(new Lazy<List<SupplierOrder>>(() => supplierOrderTableService.GetAllActiveByGuid(lookup.Guid, "TagRatioLookupGuid"), false));
        }

        private void SetUpShipMethodSupplierOrderListLazyLoad(Lookup lookup)
        {
            var supplierOrderTableService = Context.Get<SupplierOrderTableService>();
            lookup.SetLazyShipMethodSupplierOrderList(new Lazy<List<SupplierOrder>>(() => supplierOrderTableService.GetAllActiveByGuid(lookup.Guid, "ShipMethodLookupGuid"), false));
        }

        private void SetUpFeeTypeSupplierOrderFeeListLazyLoad(Lookup lookup)
        {
            var supplierOrderFeeTableService = Context.Get<SupplierOrderFeeTableService>();
            lookup.SetLazyFeeTypeSupplierOrderFeeList(new Lazy<List<SupplierOrderFee>>(() => supplierOrderFeeTableService.GetAllActiveByGuid(lookup.Guid, "FeeTypeLookupGuid"), false));
        }

        private void SetUpFeeStatXSupplierOrderFeeListLazyLoad(Lookup lookup)
        {
            var supplierOrderFeeTableService = Context.Get<SupplierOrderFeeTableService>();
            lookup.SetLazyFeeStatXSupplierOrderFeeList(new Lazy<List<SupplierOrderFee>>(() => supplierOrderFeeTableService.GetAllActiveByGuid(lookup.Guid, "FeeStatXLookupGuid"), false));
        }

        //private void SetUpPredicateTripleListLazyLoad(Lookup lookup)
        //{
        //    var tripleTableService = Context.Get<TripleTableService>();
        //    lookup.SetLazyPredicateTripleList(new Lazy<List<Triple>>(() => tripleTableService.GetAllActiveByGuid(lookup.Guid, "PredicateLookupGuid"), false));
        //}

        private void SetUpColorVarietyListLazyLoad(Lookup lookup)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            lookup.SetLazyColorVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(lookup.Guid, "ColorLookupGuid"), false));
        }

        private void SetUpHabitVarietyListLazyLoad(Lookup lookup)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            lookup.SetLazyHabitVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(lookup.Guid, "HabitLookupGuid"), false));
        }

        private void SetUpVigorVarietyListLazyLoad(Lookup lookup)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            lookup.SetLazyVigorVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(lookup.Guid, "VigorLookupGuid"), false));
        }

        private void SetUpTimingVarietyListLazyLoad(Lookup lookup)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            lookup.SetLazyTimingVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(lookup.Guid, "TimingLookupGuid"), false));
        }

        private void SetUpZoneVarietyListLazyLoad(Lookup lookup)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            lookup.SetLazyZoneVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(lookup.Guid, "ZoneLookupGuid"), false));
        }

        private void SetUpBrandVarietyListLazyLoad(Lookup lookup)
        {
            var varietyTableService = Context.Get<VarietyTableService>();
            lookup.SetLazyBrandVarietyList(new Lazy<List<Variety>>(() => varietyTableService.GetAllActiveByGuid(lookup.Guid, "BrandLookupGuid"), false));
        }

        //private void SetUpVolumeLevelTypeVolumeLevelListLazyLoad(Lookup lookup)
        //{
        //    var volumeLevelTableService = Context.Get<VolumeLevelTableService>();
        //    lookup.SetLazyVolumeLevelTypeVolumeLevelList(new Lazy<List<VolumeLevel>>(() => volumeLevelTableService.GetAllActiveByGuid(lookup.Guid, "VolumeLevelTypeLookupGuid"), false));
        //}

		#endregion
	}
}
