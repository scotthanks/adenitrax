//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.SqlClient;
//using DataAccessLibrary;
//using DataObjectLibrary;

//// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

//namespace DataServiceLibrary
//{
//    public partial class EventLogViewService : ViewService<EventLogView>
//    {
//        protected override EventLogView DefaultLoadFromSqlDataReader(SqlDataReader reader)
//        {
//            var eventLogView = new EventLogView();

//            string columnName = "";
//            try
//            {
//                columnName = "EventTime";
//                var eventTime = reader[(int)EventLogView.ColumnEnum.EventTime];
//                if (eventTime == DBNull.Value) eventTime = null;
//                eventLogView.EventTime = (DateTime?)eventTime;

//                columnName = "LogTypeLookupCode";
//                var logTypeLookupCode = reader[(int)EventLogView.ColumnEnum.LogTypeLookupCode] ?? string.Empty;
//                logTypeLookupCode = TrimString(logTypeLookupCode);
//                eventLogView.LogTypeLookupCode = (string)logTypeLookupCode;

//                columnName = "ObjectTypeLookupCode";
//                var objectTypeLookupCode = reader[(int)EventLogView.ColumnEnum.ObjectTypeLookupCode] ?? string.Empty;
//                objectTypeLookupCode = TrimString(objectTypeLookupCode);
//                eventLogView.ObjectTypeLookupCode = (string)objectTypeLookupCode;

//                columnName = "ProcessLookupCode";
//                var processLookupCode = reader[(int)EventLogView.ColumnEnum.ProcessLookupCode] ?? string.Empty;
//                processLookupCode = TrimString(processLookupCode);
//                eventLogView.ProcessLookupCode = (string)processLookupCode;

//                columnName = "Comment";
//                var comment = reader[(int)EventLogView.ColumnEnum.Comment];
//                if (comment == DBNull.Value) comment = null;
//                comment = TrimString(comment);
//                eventLogView.Comment = (string)comment;

//                columnName = "UserCode";
//                var userCode = reader[(int)EventLogView.ColumnEnum.UserCode];
//                if (userCode == DBNull.Value) userCode = null;
//                userCode = TrimString(userCode);
//                eventLogView.UserCode = (string)userCode;

//                columnName = "PersonGuid";
//                var personGuid = reader[(int)EventLogView.ColumnEnum.PersonGuid];
//                if (personGuid == DBNull.Value) personGuid = null;
//                eventLogView.PersonGuid = (Guid?)personGuid;

//                columnName = "PersonFirstName";
//                var personFirstName = reader[(int)EventLogView.ColumnEnum.PersonFirstName];
//                if (personFirstName == DBNull.Value) personFirstName = null;
//                personFirstName = TrimString(personFirstName);
//                eventLogView.PersonFirstName = (string)personFirstName;

//                columnName = "PersonLastName";
//                var personLastName = reader[(int)EventLogView.ColumnEnum.PersonLastName];
//                if (personLastName == DBNull.Value) personLastName = null;
//                personLastName = TrimString(personLastName);
//                eventLogView.PersonLastName = (string)personLastName;

//                columnName = "IntValue";
//                var intValue = reader[(int)EventLogView.ColumnEnum.IntValue];
//                if (intValue == DBNull.Value) intValue = null;
//                eventLogView.IntValue = (long?)intValue;

//                columnName = "FloatValue";
//                var floatValue = reader[(int)EventLogView.ColumnEnum.FloatValue];
//                if (floatValue == DBNull.Value) floatValue = null;
//                eventLogView.FloatValue = (Double?)floatValue;

//                columnName = "GuidValue";
//                var guidValue = reader[(int)EventLogView.ColumnEnum.GuidValue];
//                if (guidValue == DBNull.Value) guidValue = null;
//                eventLogView.GuidValue = (Guid?)guidValue;

//                columnName = "ObjectGuid";
//                var objectGuid = reader[(int)EventLogView.ColumnEnum.ObjectGuid];
//                if (objectGuid == DBNull.Value) objectGuid = null;
//                eventLogView.ObjectGuid = (Guid?)objectGuid;

//                columnName = "EventLogId";
//                var eventLogId = reader[(int)EventLogView.ColumnEnum.EventLogId] ?? long.MinValue;
//                eventLogView.EventLogId = (long)eventLogId;

//                columnName = "EventLogGuid";
//                var eventLogGuid = reader[(int)EventLogView.ColumnEnum.EventLogGuid] ?? Guid.Empty;
//                eventLogView.EventLogGuid = (Guid)eventLogGuid;

//                columnName = "ProcessId";
//                var processId = reader[(int)EventLogView.ColumnEnum.ProcessId] ?? long.MinValue;
//                eventLogView.ProcessId = (long)processId;

//                columnName = "SyncTime";
//                var syncTime = reader[(int)EventLogView.ColumnEnum.SyncTime];
//                if (syncTime == DBNull.Value) syncTime = null;
//                eventLogView.SyncTime = (DateTime?)syncTime;

//                columnName = "XmlData";
//                var xmlData = reader[(int)EventLogView.ColumnEnum.XmlData];
//                if (xmlData == DBNull.Value) xmlData = null;
//                xmlData = TrimString(xmlData);
//                eventLogView.XmlData = (string)xmlData;

//                columnName = "LogTypeLookupGuid";
//                var logTypeLookupGuid = reader[(int)EventLogView.ColumnEnum.LogTypeLookupGuid] ?? Guid.Empty;
//                eventLogView.LogTypeLookupGuid = (Guid)logTypeLookupGuid;

//                columnName = "LogTypeLookupId";
//                var logTypeLookupId = reader[(int)EventLogView.ColumnEnum.LogTypeLookupId] ?? long.MinValue;
//                eventLogView.LogTypeLookupId = (long)logTypeLookupId;

//                columnName = "ObjectTypeLookupId";
//                var objectTypeLookupId = reader[(int)EventLogView.ColumnEnum.ObjectTypeLookupId] ?? long.MinValue;
//                eventLogView.ObjectTypeLookupId = (long)objectTypeLookupId;

//                columnName = "ObjectTypeLookupGuid";
//                var objectTypeLookupGuid = reader[(int)EventLogView.ColumnEnum.ObjectTypeLookupGuid] ?? Guid.Empty;
//                eventLogView.ObjectTypeLookupGuid = (Guid)objectTypeLookupGuid;

//                columnName = "ProcessLookupGuid";
//                var processLookupGuid = reader[(int)EventLogView.ColumnEnum.ProcessLookupGuid] ?? Guid.Empty;
//                eventLogView.ProcessLookupGuid = (Guid)processLookupGuid;

//                columnName = "ProcessLookupId";
//                var processLookupId = reader[(int)EventLogView.ColumnEnum.ProcessLookupId] ?? long.MinValue;
//                eventLogView.ProcessLookupId = (long)processLookupId;

//                columnName = "PersonId";
//                var personId = reader[(int)EventLogView.ColumnEnum.PersonId];
//                if (personId == DBNull.Value) personId = null;
//                eventLogView.PersonId = (long?)personId;

//                columnName = "OrderLineGuid";
//                var orderLineGuid = reader[(int)EventLogView.ColumnEnum.OrderLineGuid];
//                if (orderLineGuid == DBNull.Value) orderLineGuid = null;
//                eventLogView.OrderLineGuid = (Guid?)orderLineGuid;

//                columnName = "SupplierOrderGuid";
//                var supplierOrderGuid = reader[(int)EventLogView.ColumnEnum.SupplierOrderGuid];
//                if (supplierOrderGuid == DBNull.Value) supplierOrderGuid = null;
//                eventLogView.SupplierOrderGuid = (Guid?)supplierOrderGuid;

//                columnName = "GrowerOrderGuid";
//                var growerOrderGuid = reader[(int)EventLogView.ColumnEnum.GrowerOrderGuid];
//                if (growerOrderGuid == DBNull.Value) growerOrderGuid = null;
//                eventLogView.GrowerOrderGuid = (Guid?)growerOrderGuid;

//            }
//            catch( Exception e)
//            {
//                ThrowReaderException(e, columnName);
//            }

//            return eventLogView;
//        }
//    }
//}
