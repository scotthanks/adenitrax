using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataAccessLibrary;
using DataObjectLibrary;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataServiceLibrary
{
	public partial class SupplierOrderTableService : TableService<SupplierOrder>
	{
		public SupplierOrderTableService()
		{
			SetUpCacheDelegates();
			SetUpLazyLoadDelegates();
		}

		protected override SupplierOrder DefaultInsert( SupplierOrder supplierOrder, SqlTransaction transaction = null)
		{
            string sqlInsertCommand = "INSERT INTO [SupplierOrder]([Guid], [DateDeactivated], [GrowerOrderGuid], [SupplierGuid], [PoNo], [SupplierOrderNo], [TotalCost], [SupplierOrderStatusLookupGuid], [TagRatioLookupGuid], [ShipMethodLookupGuid], [TrackingNumber], [TrackingComment], [ExpectedDeliveredDate], [DateLastSenttoSupplier], ) VALUES (@Guid, @DateDeactivated, @GrowerOrderGuid, @SupplierGuid, @PoNo, @SupplierOrderNo, @TotalCost, @SupplierOrderStatusLookupGuid, @TagRatioLookupGuid, @ShipMethodLookupGuid, @TrackingNumber,  @TrackingComment,@ExpectedDeliveredDate, @DateLastSenttoSupplier, );SELECT * FROM [SupplierOrder] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlInsertCommand, AddParameters, supplierOrder, transaction: transaction);
		}

		protected override SupplierOrder DefaultUpdate( SupplierOrder supplierOrder, SqlTransaction transaction = null)
		{
            string sqlUpdateCommand = "UPDATE [SupplierOrder] SET [DateDeactivated]=@DateDeactivated, [GrowerOrderGuid]=@GrowerOrderGuid, [SupplierGuid]=@SupplierGuid, [PoNo]=@PoNo, [SupplierOrderNo]=@SupplierOrderNo, [TotalCost]=@TotalCost, [SupplierOrderStatusLookupGuid]=@SupplierOrderStatusLookupGuid, [TagRatioLookupGuid]=@TagRatioLookupGuid, [ShipMethodLookupGuid]=@ShipMethodLookupGuid, [TrackingNumber]=@TrackingNumber, [TrackingComment]=@TrackingComment, [ExpectedDeliveredDate]=@ExpectedDeliveredDate, [DateLastSenttoSupplier]=@DateLastSenttoSupplier, WHERE [Guid]=@Guid;SELECT * FROM [SupplierOrder] WHERE [Guid]=@Guid";

	        return GetOneFromCommand(sqlUpdateCommand, AddParameters, supplierOrder, transaction: transaction);
	    }

		protected override void AddParameters( SqlCommand command, SupplierOrder supplierOrder)
		{		
			var idParameter = new SqlParameter( "@Id", SqlDbType.BigInt, 8);
			idParameter.IsNullable = false;
			idParameter.Value = supplierOrder.Id;
			command.Parameters.Add(idParameter);

			var guidParameter = new SqlParameter( "@Guid", SqlDbType.UniqueIdentifier, 16);
			guidParameter.IsNullable = false;
			guidParameter.Value = supplierOrder.Guid;
			command.Parameters.Add(guidParameter);

			var dateDeactivatedParameter = new SqlParameter( "@DateDeactivated", SqlDbType.DateTime, 8);
			dateDeactivatedParameter.IsNullable = true;
			dateDeactivatedParameter.Value = supplierOrder.DateDeactivated ?? (object)DBNull.Value;
			command.Parameters.Add(dateDeactivatedParameter);

			var growerOrderGuidParameter = new SqlParameter( "@GrowerOrderGuid", SqlDbType.UniqueIdentifier, 16);
			growerOrderGuidParameter.IsNullable = false;
			growerOrderGuidParameter.Value = supplierOrder.GrowerOrderGuid;
			command.Parameters.Add(growerOrderGuidParameter);

			var supplierGuidParameter = new SqlParameter( "@SupplierGuid", SqlDbType.UniqueIdentifier, 16);
			supplierGuidParameter.IsNullable = false;
			supplierGuidParameter.Value = supplierOrder.SupplierGuid;
			command.Parameters.Add(supplierGuidParameter);

			var poNoParameter = new SqlParameter( "@PoNo", SqlDbType.NChar, 30);
			poNoParameter.IsNullable = false;
			supplierOrder.PoNo = supplierOrder.PoNo ?? "";
			supplierOrder.PoNo = supplierOrder.PoNo.Trim();
			poNoParameter.Value = supplierOrder.PoNo;
			command.Parameters.Add(poNoParameter);

			var supplierOrderNoParameter = new SqlParameter( "@SupplierOrderNo", SqlDbType.NChar, 30);
			supplierOrderNoParameter.IsNullable = false;
			supplierOrder.SupplierOrderNo = supplierOrder.SupplierOrderNo ?? "";
			supplierOrder.SupplierOrderNo = supplierOrder.SupplierOrderNo.Trim();
			supplierOrderNoParameter.Value = supplierOrder.SupplierOrderNo;
			command.Parameters.Add(supplierOrderNoParameter);

			var totalCostParameter = new SqlParameter( "@TotalCost", SqlDbType.Decimal, 5);
			totalCostParameter.IsNullable = false;
			totalCostParameter.Value = supplierOrder.TotalCost;
			command.Parameters.Add(totalCostParameter);

			var supplierOrderStatusLookupGuidParameter = new SqlParameter( "@SupplierOrderStatusLookupGuid", SqlDbType.UniqueIdentifier, 16);
			supplierOrderStatusLookupGuidParameter.IsNullable = false;
			supplierOrderStatusLookupGuidParameter.Value = supplierOrder.SupplierOrderStatusLookupGuid;
			command.Parameters.Add(supplierOrderStatusLookupGuidParameter);

			var tagRatioLookupGuidParameter = new SqlParameter( "@TagRatioLookupGuid", SqlDbType.UniqueIdentifier, 16);
			tagRatioLookupGuidParameter.IsNullable = true;
			tagRatioLookupGuidParameter.Value = supplierOrder.TagRatioLookupGuid ?? (object)DBNull.Value;
			command.Parameters.Add(tagRatioLookupGuidParameter);

			var shipMethodLookupGuidParameter = new SqlParameter( "@ShipMethodLookupGuid", SqlDbType.UniqueIdentifier, 16);
			shipMethodLookupGuidParameter.IsNullable = true;
			shipMethodLookupGuidParameter.Value = supplierOrder.ShipMethodLookupGuid ?? (object)DBNull.Value;
			command.Parameters.Add(shipMethodLookupGuidParameter);

			var trackingNumberParameter = new SqlParameter( "@TrackingNumber", SqlDbType.NVarChar, 200);
			trackingNumberParameter.IsNullable = false;
			supplierOrder.TrackingNumber = supplierOrder.TrackingNumber ?? "";
			supplierOrder.TrackingNumber = supplierOrder.TrackingNumber.Trim();
			trackingNumberParameter.Value = supplierOrder.TrackingNumber;
			command.Parameters.Add(trackingNumberParameter);

           
            
            var expectedDeliveredDateParameter = new SqlParameter("@ExpectedDeliveredDate", SqlDbType.DateTime, 8);
			expectedDeliveredDateParameter.IsNullable = true;
			expectedDeliveredDateParameter.Value = supplierOrder.ExpectedDeliveredDate ?? (object)DBNull.Value;
			command.Parameters.Add(expectedDeliveredDateParameter);

			var dateLastSenttoSupplierParameter = new SqlParameter( "@DateLastSenttoSupplier", SqlDbType.DateTime, 8);
			dateLastSenttoSupplierParameter.IsNullable = true;
			dateLastSenttoSupplierParameter.Value = supplierOrder.DateLastSenttoSupplier ?? (object)DBNull.Value;
			command.Parameters.Add(dateLastSenttoSupplierParameter);

            var trackingCommentParameter = new SqlParameter("@TrackingComment", SqlDbType.NVarChar, 200);
            trackingCommentParameter.IsNullable = false;
            supplierOrder.TrackingComment = supplierOrder.TrackingComment ?? "";
            supplierOrder.TrackingComment = supplierOrder.TrackingComment.Trim();
            trackingCommentParameter.Value = supplierOrder.TrackingComment;
            command.Parameters.Add(trackingCommentParameter);

		}

        protected override SupplierOrder DefaultLoadFromSqlDataReader(SqlDataReader reader)
        {
            var supplierOrder = new SupplierOrder();

			string columnName = "";
            try
            {
				columnName = "Id";
				var id = reader[(int)SupplierOrder.ColumnEnum.Id] ?? long.MinValue;
				supplierOrder.Id = (long)id;

				columnName = "Guid";
				var guid = reader[(int)SupplierOrder.ColumnEnum.Guid] ?? Guid.Empty;
				supplierOrder.Guid = (Guid)guid;

				columnName = "DateDeactivated";
				var dateDeactivated = reader[(int)SupplierOrder.ColumnEnum.DateDeactivated];
				if (dateDeactivated == DBNull.Value) dateDeactivated = null;
				supplierOrder.DateDeactivated = (DateTime?)dateDeactivated;

				columnName = "GrowerOrderGuid";
				var growerOrderGuid = reader[(int)SupplierOrder.ColumnEnum.GrowerOrderGuid] ?? Guid.Empty;
				supplierOrder.GrowerOrderGuid = (Guid)growerOrderGuid;

				columnName = "SupplierGuid";
				var supplierGuid = reader[(int)SupplierOrder.ColumnEnum.SupplierGuid] ?? Guid.Empty;
				supplierOrder.SupplierGuid = (Guid)supplierGuid;

				columnName = "PoNo";
				var poNo = reader[(int)SupplierOrder.ColumnEnum.PoNo] ?? string.Empty;
				poNo = TrimString(poNo);
				supplierOrder.PoNo = (string)poNo;

				columnName = "SupplierOrderNo";
				var supplierOrderNo = reader[(int)SupplierOrder.ColumnEnum.SupplierOrderNo] ?? string.Empty;
				supplierOrderNo = TrimString(supplierOrderNo);
				supplierOrder.SupplierOrderNo = (string)supplierOrderNo;

				columnName = "TotalCost";
				var totalCost = reader[(int)SupplierOrder.ColumnEnum.TotalCost] ?? Decimal.MinValue;
				supplierOrder.TotalCost = (Decimal)totalCost;

				columnName = "SupplierOrderStatusLookupGuid";
				var supplierOrderStatusLookupGuid = reader[(int)SupplierOrder.ColumnEnum.SupplierOrderStatusLookupGuid] ?? Guid.Empty;
				supplierOrder.SupplierOrderStatusLookupGuid = (Guid)supplierOrderStatusLookupGuid;

				columnName = "TagRatioLookupGuid";
				var tagRatioLookupGuid = reader[(int)SupplierOrder.ColumnEnum.TagRatioLookupGuid];
				if (tagRatioLookupGuid == DBNull.Value) tagRatioLookupGuid = null;
				supplierOrder.TagRatioLookupGuid = (Guid?)tagRatioLookupGuid;

				columnName = "ShipMethodLookupGuid";
				var shipMethodLookupGuid = reader[(int)SupplierOrder.ColumnEnum.ShipMethodLookupGuid];
				if (shipMethodLookupGuid == DBNull.Value) shipMethodLookupGuid = null;
				supplierOrder.ShipMethodLookupGuid = (Guid?)shipMethodLookupGuid;

				columnName = "TrackingNumber";
				var trackingNumber = reader[(int)SupplierOrder.ColumnEnum.TrackingNumber] ?? string.Empty;
				trackingNumber = TrimString(trackingNumber);
				supplierOrder.TrackingNumber = (string)trackingNumber;


                columnName = "TrackingComment";
                var trackingComment = reader[(int)SupplierOrder.ColumnEnum.TrackingComment] ?? string.Empty;
                trackingComment = TrimString(trackingComment);
                supplierOrder.TrackingComment = (string)trackingComment;
                
                columnName = "ExpectedDeliveredDate";
				var expectedDeliveredDate = reader[(int)SupplierOrder.ColumnEnum.ExpectedDeliveredDate];
				if (expectedDeliveredDate == DBNull.Value) expectedDeliveredDate = null;
				supplierOrder.ExpectedDeliveredDate = (DateTime?)expectedDeliveredDate;

				columnName = "DateLastSenttoSupplier";
				var dateLastSenttoSupplier = reader[(int)SupplierOrder.ColumnEnum.DateLastSenttoSupplier];
				if (dateLastSenttoSupplier == DBNull.Value) dateLastSenttoSupplier = null;
				supplierOrder.DateLastSenttoSupplier = (DateTime?)dateLastSenttoSupplier;

                

            }
            catch( Exception e)
            {
                ThrowReaderException(e, columnName);
            }

            return supplierOrder;
        }

		#region Lazy Loading Properties and Methods

        public override void SetUpLazyLoads(SupplierOrder supplierOrder)
        {
			SetUpGrowerOrderLazyLoad( supplierOrder);
			SetUpSupplierLazyLoad( supplierOrder);
			SetUpSupplierOrderStatusLookupLazyLoad( supplierOrder);
			SetUpTagRatioLookupLazyLoad( supplierOrder);
			SetUpShipMethodLookupLazyLoad( supplierOrder);
			SetUpOrderLineListLazyLoad( supplierOrder);
			SetUpSupplierOrderFeeListLazyLoad( supplierOrder);
		}

        protected override void FixAnyLazyLoadedLists(SupplierOrder supplierOrder, ListActionEnum listAction)
        {
			FixGrowerOrderList( supplierOrder, listAction);
			FixSupplierList( supplierOrder, listAction);
			FixSupplierOrderStatusLookupList( supplierOrder, listAction);
			FixTagRatioLookupList( supplierOrder, listAction);
			FixShipMethodLookupList( supplierOrder, listAction);
		}

        private void SetUpGrowerOrderLazyLoad( SupplierOrder supplierOrder)
        {
            var growerOrderTableService = Context.Get<GrowerOrderTableService>();
            supplierOrder.SetLazyGrowerOrder(new Lazy<GrowerOrder>(() => growerOrderTableService.GetByGuid(supplierOrder.GrowerOrderGuid), false));
        }

        private void FixGrowerOrderList( SupplierOrder supplierOrder, ListActionEnum listAction)
        {
            if (supplierOrder.GrowerOrderIsLoaded)
            {
                FixLazyLoadedList(supplierOrder.GrowerOrder.SupplierOrderList, supplierOrder, listAction);
            }
        }

        private void SetUpSupplierLazyLoad( SupplierOrder supplierOrder)
        {
            var supplierTableService = Context.Get<SupplierTableService>();
            supplierOrder.SetLazySupplier(new Lazy<Supplier>(() => supplierTableService.GetByGuid(supplierOrder.SupplierGuid), false));
        }

        private void FixSupplierList( SupplierOrder supplierOrder, ListActionEnum listAction)
        {
            if (supplierOrder.SupplierIsLoaded)
            {
                FixLazyLoadedList(supplierOrder.Supplier.SupplierOrderList, supplierOrder, listAction);
            }
        }

        private void SetUpSupplierOrderStatusLookupLazyLoad( SupplierOrder supplierOrder)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            supplierOrder.SetLazySupplierOrderStatusLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(supplierOrder.SupplierOrderStatusLookupGuid), false));
        }

        private void FixSupplierOrderStatusLookupList( SupplierOrder supplierOrder, ListActionEnum listAction)
        {
            if (supplierOrder.SupplierOrderStatusLookupIsLoaded)
            {
                FixLazyLoadedList(supplierOrder.SupplierOrderStatusLookup.SupplierOrderStatusSupplierOrderList, supplierOrder, listAction);
            }
        }

        private void SetUpTagRatioLookupLazyLoad( SupplierOrder supplierOrder)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            supplierOrder.SetLazyTagRatioLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(supplierOrder.TagRatioLookupGuid), false));
        }

        private void FixTagRatioLookupList( SupplierOrder supplierOrder, ListActionEnum listAction)
        {
            if (supplierOrder.TagRatioLookupIsLoaded)
            {
                FixLazyLoadedList(supplierOrder.TagRatioLookup.TagRatioSupplierOrderList, supplierOrder, listAction);
            }
        }

        private void SetUpShipMethodLookupLazyLoad( SupplierOrder supplierOrder)
        {
            var lookupTableService = Context.Get<LookupTableService>();
            supplierOrder.SetLazyShipMethodLookup(new Lazy<Lookup>(() => lookupTableService.GetByGuid(supplierOrder.ShipMethodLookupGuid), false));
        }

        private void FixShipMethodLookupList( SupplierOrder supplierOrder, ListActionEnum listAction)
        {
            if (supplierOrder.ShipMethodLookupIsLoaded)
            {
                FixLazyLoadedList(supplierOrder.ShipMethodLookup.ShipMethodSupplierOrderList, supplierOrder, listAction);
            }
        }

        private void SetUpOrderLineListLazyLoad(SupplierOrder supplierOrder)
        {
            var orderLineTableService = Context.Get<OrderLineTableService>();
            supplierOrder.SetLazyOrderLineList(new Lazy<List<OrderLine>>(() => orderLineTableService.GetAllActiveByGuid(supplierOrder.Guid, "SupplierOrderGuid"), false));
        }

        private void SetUpSupplierOrderFeeListLazyLoad(SupplierOrder supplierOrder)
        {
            var supplierOrderFeeTableService = Context.Get<SupplierOrderFeeTableService>();
            supplierOrder.SetLazySupplierOrderFeeList(new Lazy<List<SupplierOrderFee>>(() => supplierOrderFeeTableService.GetAllActiveByGuid(supplierOrder.Guid, "SupplierOrderGuid"), false));
        }

		#endregion
	}
}
