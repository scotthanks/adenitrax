﻿using System;
using System.Web;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BusinessObjectServices
{
    //This Service is a toolbox for Http processing, thngs like cookie management
    
    public class HttpService
    {
        //Important Info: Browser only sends cookie name and values, not Expires !!!!

        public const string COOKIE_NAME_SESSION = "epsSession";
        public const double SESSION_COOKIE_DURATION_MINUTES = 60d;
        public const string COOKIE_NAME_STATE = "epsState";
        public const double STATE_COOKIE_DURATION_HOURS = 2d;

        /// <summary>
        /// Create a new Session Cookie
        /// </summary>
        /// <returns></returns>
        public HttpCookie NewSessionCookie()
        {
            var cookie = new HttpCookie(COOKIE_NAME_SESSION, Guid.NewGuid().ToString().ToLower());
            cookie.Expires = DateTime.Now.AddMinutes(SESSION_COOKIE_DURATION_MINUTES);
            cookie.Path = "/";
            return cookie;
        }
        /// <summary>
        /// Returns Current Session Cookie or Creates New One
        /// </summary>
        /// <param name="HttpRequest"></param>
        /// <returns></returns>
        public HttpCookie ReturnOrCreateSessionCookie(HttpRequestBase HttpRequest)
        {
            //This version accepts HttpRequestBase
            HttpCookie cookie = this.ReturnCookie(HttpRequest, COOKIE_NAME_SESSION);
            if (cookie != null) {
                cookie.Expires = DateTime.Now.AddMinutes(SESSION_COOKIE_DURATION_MINUTES);
            }else {
                cookie = this.NewSessionCookie(); };
            return cookie;
        }
        /// <summary>
        /// Returns Current Session Cookie or Creates New One
        /// </summary>
        /// <param name="HttpRequest"></param>
        /// <returns></returns>
        public HttpCookie ReturnOrCreateSessionCookie(HttpRequest HttpRequest)
        {
            //This version accepts HttpRequest
            HttpCookie cookie = this.ReturnCookie(HttpRequest, COOKIE_NAME_SESSION);
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddMinutes(SESSION_COOKIE_DURATION_MINUTES);
            }
            else
            {
                cookie = this.NewSessionCookie();
            };
            return cookie;
        }


        /// <summary>
        /// Creates a new State Cookie
        /// </summary>
        /// <returns></returns>
        public HttpCookie NewStateCookie() {
            var cookie = new HttpCookie(COOKIE_NAME_STATE, Guid.NewGuid().ToString().ToLower());
            cookie.Expires = DateTime.Now.AddHours(STATE_COOKIE_DURATION_HOURS);
            cookie.Path = "/";
            return cookie;
        }

        /// <summary>
        /// Returns Current State Cookie or Creates New One
        /// </summary>
        /// <param name="HttpRequest"></param>
        /// <returns></returns>
        public HttpCookie ReturnOrCreateStateCookie(HttpRequestBase HttpRequest)
        {
            //This version accepts HttpRequestBase
            HttpCookie cookie = this.ReturnCookie(HttpRequest, COOKIE_NAME_STATE);
            if (cookie != null) {
                cookie.Expires = DateTime.Now.AddHours(STATE_COOKIE_DURATION_HOURS);
            }else { 
                cookie = this.NewStateCookie(); };
            return cookie;
        }
        /// <summary>
        /// Returns Current State Cookie or Creates New One
        /// </summary>
        /// <param name="HttpRequest"></param>
        /// <returns></returns>
        public HttpCookie ReturnOrCreateStateCookie(HttpRequest HttpRequest)
        {
            //This version accepts HttpRequest
            HttpCookie cookie = this.ReturnCookie(HttpRequest, COOKIE_NAME_STATE);
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddHours(STATE_COOKIE_DURATION_HOURS);
            }
            else
            {
                cookie = this.NewStateCookie();
            };
            return cookie;
        }

        public HttpCookie ReturnStateCookie(HttpResponse HttpResponse)
        {
            //This version accepts HttpResponse
            HttpCookieCollection responseCookiesCollection = HttpResponse.Cookies;
            HttpCookie cookie = responseCookiesCollection[COOKIE_NAME_STATE] ?? null;
            return cookie;

        }
        public HttpCookie ReturnStateCookie(HttpResponseBase HttpResponse)
        {
            //This version accepts HttpResponseBase
            HttpCookieCollection responseCookiesCollection = HttpResponse.Cookies;
            HttpCookie cookie = responseCookiesCollection[COOKIE_NAME_STATE] ?? null;
            return cookie;

        }

        public HttpCookie ReturnStateCookie(HttpRequestBase HttpRequest)
        {
            //Returns existing or new State Cookie
            HttpCookie cookie = this.ReturnCookie(HttpRequest, COOKIE_NAME_STATE);
           return cookie;
        }
        public HttpCookie ReturnStateCookie(HttpRequest HttpRequest)
        {
            //This version accepts HttpRequest
            HttpCookie cookie = this.ReturnCookie(HttpRequest, COOKIE_NAME_STATE);
            return cookie;
        }




        public HttpCookie ReturnCookie(HttpRequest HttpRequest, string CookieName)
        {
            //This version accepts HttpRequest
            HttpCookieCollection requestCookiesCollection = HttpRequest.Cookies;
            HttpCookie cookie = requestCookiesCollection[CookieName] ?? null;
            return cookie;
        }

        public HttpCookie ReturnCookie(HttpRequestBase HttpRequest, string CookieName)
        {
            //This version uses HttpRequestBase 
            HttpCookieCollection requestCookiesCollection = HttpRequest.Cookies;
            HttpCookie cookie = requestCookiesCollection[CookieName] ?? null;
            return cookie;
        }

        public HttpCookie SetCookieExpires(HttpCookie Cookie, DateTime Expires) {
            if(Cookie != null && Expires != null){

                Cookie.Expires = Expires;
            }

            return Cookie;
        }

        public HttpCookie SetCookieStringValue(HttpCookie Cookie, string Key, string Value) {
            Key = Key ?? string.Empty; Key = Key.Trim();
            Value = Value ?? string.Empty; Value = Value.Trim();
            if(Cookie != null && Key !=string.Empty && Value != string.Empty) {
                Cookie[Key] = Value;                
            };
            return Cookie;
        }

        public void AddCookie(HttpResponse HttpResponse, HttpCookie Cookie)
        {
            //This version uses HttpResponse
            if (HttpResponse != null && Cookie != null)
            {
                HttpResponse.Cookies.Add(Cookie);
            };
        }
        public void AddCookie(HttpResponseBase HttpResponse, HttpCookie Cookie)
        {
            //This version is for HttpResponseBase
            if (HttpResponse != null && Cookie != null)
            {
                HttpResponse.Cookies.Add(Cookie);
            };
        }

    }
}
