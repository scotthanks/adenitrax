﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataServiceLibrary;

using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    public class ShipWeekService
    {
        private int[] _shipWeekSequences = new int[] { 1, 2012, 2012, 1, 2, 52, 52, 53, 52, 52, 52, 52, 53, 52, 52, 52, 52 };
        private const int SEED_SHIP_WEEK_INDEX = 0;
        private const int SEED_SHIP_YEAR_INDEX = 1;
        private const int SEED_DATE_YEAR_INDEX = 2;
        private const int SEED_DATE_MONTH_INDEX = 3;
        private const int SEED_DATE_DAY_INDEX = 4;
        private const int SERIES_START_INDEX = 5;
        private string _composeShipWeekString(int week, int year) {
            return string.Format("{0:D2}{1}{2}", week, SHIP_WEEK_DELIMITER_CHAR, year);

        }   
        
        public const char SHIP_WEEK_DELIMITER_CHAR = '|';

        private int _seedWeek { get { return _shipWeekSequences[SEED_SHIP_WEEK_INDEX]; } }
        private int _seedYear { get { return _shipWeekSequences[SEED_SHIP_YEAR_INDEX]; } }
        private DateTime _seedDate { get { return new DateTime(_shipWeekSequences[SEED_DATE_YEAR_INDEX], _shipWeekSequences[SEED_DATE_MONTH_INDEX], _shipWeekSequences[SEED_DATE_DAY_INDEX]); } }
        private int _firstYearKnown { get { return _seedWeek; } }
        private int _lastYearKnown { get { return _seedYear + _sequences.Length - 1; } }
        private DateTime _firstDateKnown { get { return _seedDate; } }
        private DateTime _lastDateKnown { get {
            int totalDays = -(_seedWeek * 7);
            foreach(int sequence in _sequences){
                totalDays += sequence * 7;
            }
            return _seedDate.AddDays(totalDays);
        
        } }
        private int[] _sequences
        {
            get
            {
                int[] arr = new int[_shipWeekSequences.Length - SERIES_START_INDEX];
                Array.Copy(_shipWeekSequences, SERIES_START_INDEX, arr, 0, arr.Length);
                return arr;
            }
        }

        public int CurrentShipWeekNumber
        {
            get
            {
                var now = DateTime.Today;
                ///////////////now = new DateTime(2021, 1, 4);  //Just Testing
                var returnValue = _seedWeek + (((now - _seedDate).Days) / 7);  //The seed week plus all the weeks since
                foreach (int sequence in _sequences)
                {
                    if (returnValue > sequence)
                    {
                        returnValue = returnValue - sequence;
                    }
                    else
                    {
                        break;
                    };
                }
                return returnValue;
            }
        }
        
        public DateTime ReturnMondayDate(DateTime date){
            while(1 != (int)date.DayOfWeek){
                date = date.AddDays(-1);
            }
            return date;
        }
        public DateTime CalculateMondayDate(int shipWeek, int shipYear)
        {
            int[] sequences = _sequences;
            if (shipWeek < 1 || shipWeek > 53 || shipYear < _seedYear || shipYear > _lastYearKnown) { return new DateTime(); };
            if (shipWeek == 53 && sequences[shipYear - _seedYear] != 53) { return new DateTime(); };
            int seedWeek = _shipWeekSequences[SEED_SHIP_WEEK_INDEX];
            DateTime mondayDate = _seedDate;  //begin with seed
            int offsetDays = 0;
            for (int i = _seedYear; i < shipYear; i++) {
                //adjust for years
                offsetDays += (sequences[i - _seedYear] * 7);
            };
            offsetDays += (shipWeek - seedWeek) * 7;     //adjust for weeks
            mondayDate = mondayDate.AddDays(offsetDays);
            return mondayDate;
        }

        public ShipWeek ShipWeek(int week, int year){
            var shipWeek = new ShipWeek();
            // Use CalculateMondayData to determine if this is valid week and year combination, if not return empty
            shipWeek.MondayDate = this.CalculateMondayDate(week, year);
            if (shipWeek.MondayDate.Year < 2000) { return new ShipWeek(); };
            shipWeek.Week = week;
            shipWeek.Year = year;
            shipWeek.ShipWeekString = _composeShipWeekString(shipWeek.Week, shipWeek.Year);
            return shipWeek;
        }


        public ShipWeek GetShipWeek(int week, int year)
        {
            var shipWeek = new ShipWeek();
            // Use CalculateMondayData to determine if this is valid week and year combination, if not return empty
            shipWeek.MondayDate = this.CalculateMondayDate(week, year);
            if (shipWeek.MondayDate.Year < 2000) { return new ShipWeek(); };

            var shipWeekService = new DataServiceLibrary.ShipWeekTableService();
            var dbShipWeek = shipWeekService.TryGetByField("MondayDate",shipWeek.MondayDate.ToShortDateString(),"=",false);
            
            shipWeek.Guid = dbShipWeek.Guid;
            shipWeek.Week = week;
            shipWeek.Year = year;
            shipWeek.ShipWeekString = _composeShipWeekString(shipWeek.Week, shipWeek.Year);
            return shipWeek;
        }

        public ShipWeek ReturnShipWeekTypeFromDate(DateTime date){
            date = ReturnMondayDate(date);

            if (date < _firstDateKnown || date > _lastDateKnown) {return new ShipWeek(); }
            
            int daysDiff = (date - _seedDate).Days;
            int shipWeek = (daysDiff / (int)7) + _seedWeek;
            int shipYear = _seedYear;

            foreach (int sequence in _sequences) {
                if (shipWeek <= sequence) { break; };
                shipWeek -= sequence;
                shipYear++;
            }
            var ShipWeek = this.ParseShipWeekString(this._composeShipWeekString(shipWeek, shipYear));
            return ShipWeek;    
        }
        public ShipWeek OffsetByWeeks(ShipWeek shipWeek, int offsetWeeks) {
          
            var newShipWeek = new ShipWeek();
            // Use CalculateMondayData to determine if this is valid week and year combination, if not return empty
            DateTime mondayDate = this.CalculateMondayDate(shipWeek.Week, shipWeek.Year);
            if (mondayDate.Year < 2000) { return newShipWeek; };

            mondayDate = mondayDate.AddDays(offsetWeeks * 7);
            newShipWeek = ReturnShipWeekTypeFromDate(mondayDate);
            return newShipWeek;
        }
        public ShipWeek ParseShipWeekString(string shipWeekString) {
            //Note: ShipWeek String format = "##|####";
            //Note: This handles any single-character separator, eg "11/2012"

            const int EXPECTED_LENGTH = 7;

            if (shipWeekString == null) return new ShipWeek();

            if (shipWeekString.Length == (EXPECTED_LENGTH - 1))
            {
                //prepend a zero
                shipWeekString = string.Format("0{0}", shipWeekString);
            };
            if (shipWeekString.Length != EXPECTED_LENGTH) return new ShipWeek();
            
            var shipWeek = new ShipWeek();
            try
            {
                //this approach actually ignors current value of delimiter
                shipWeek.Week = Convert.ToInt32(shipWeekString.Substring(0,2));
                shipWeek.Year = Convert.ToInt32(shipWeekString.Substring(3));
            }
            catch (FormatException fe) { return new ShipWeek(); }
            catch (OverflowException oe) { return new ShipWeek(); }

            shipWeek.MondayDate = this.CalculateMondayDate(shipWeek.Week, shipWeek.Year);
            if (shipWeek.MondayDate.Year < 2000) { return new ShipWeek(); }

            shipWeek.ShipWeekString = _composeShipWeekString(shipWeek.Week, shipWeek.Year);
            
            return shipWeek;
        
        }

        public List<ShipWeek> GetShipWeeks(string userCode, string programTypeCode, string productFormCategoryCode, out ShipWeek selectedShipWeek)
        {
            var shipWeekDataGetProcedure = new DataServiceLibrary.ShipWeekDataGetProcedure();

            DataObjectLibrary.ShipWeek selectedShipWeekTableObject;

            var shipWeekTableObjectList = shipWeekDataGetProcedure.GetShipWeekData(userCode, programTypeCode, productFormCategoryCode, out selectedShipWeekTableObject);

            selectedShipWeek = null;
            var shipWeekList = new List<ShipWeek>();
            foreach (var shipWeekTableRow in shipWeekTableObjectList)
            {
                var shipWeek = new ShipWeek(shipWeekTableRow);

                if (shipWeekTableRow.Guid == selectedShipWeekTableObject.Guid)
                    selectedShipWeek = shipWeek;

                shipWeekList.Add(shipWeek);
            }

            if (selectedShipWeek == null)
            {
                throw new ApplicationException("The selected ship week is null.");
            }

            return shipWeekList;
        }


        public List<BusinessObjectsLibrary.ShipWeek> GetAllShipWeeks(int? shipWeekBegin, int? shipYearBegin, int shipWeekEnd, int shipYearEnd)
        {

            var shipWeekService = new DataServiceLibrary.ShipWeekTableService();
            var shipWeeks = shipWeekService.GetAll();
          
            var shipWeekList = new List<BusinessObjectsLibrary.ShipWeek>();

            foreach (var shipWeek in shipWeeks)
            {
                if ((shipWeek.Year == shipYearBegin && shipWeek.Week >= shipWeekBegin) || shipWeek.Year > shipYearBegin)
                {

                    if ((shipWeek.Year == shipYearEnd && shipWeek.Week <= shipWeekEnd) || shipWeek.Year < shipYearEnd)
                    {
                    var oShipWeek = new BusinessObjectsLibrary.ShipWeek(shipWeek);
                    shipWeekList.Add(oShipWeek);
                    }
                }
                
            }

            return shipWeekList;

        }
    }
}
