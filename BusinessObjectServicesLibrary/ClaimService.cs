﻿using System.Data.SqlClient;
using System.Web;
using BusinessObjectsLibrary;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Threading;
using GrowerOrderHistoryEvent = BusinessObjectsLibrary.GrowerOrderHistoryEvent;
//using GrowerOrder = BusinessObjectsLibrary.GrowerOrder;
using Lookup = DataObjectLibrary.Lookup;
//using OrderLine = DataObjectLibrary.OrderLine;
//using SupplierOrder = DataObjectLibrary.SupplierOrder;

namespace BusinessObjectServices
{
    public class ClaimService
    {
        public static bool RoundTripToDatabaseHasOccurredFORDEBUGGINGPURPOSESONLY;

       

        public List<ClaimSummary> GetMyClaims(Guid userGuid)
        {
            return GetClaims
                (
                    userGuid,
                    includeJustMyClaims: true
                );
        }
        private List<ClaimSummary> GetClaims
           (
               Guid userGuid,
               bool includeJustMyClaims
           )
        {
            var status = new StatusObject(userGuid, true);
            var procedureService = new DataServiceLibrary.ClaimDataGetProcedure(status);

            var ClaimSummaryList = new List<ClaimSummary>();

           

            var ClaimDataObjectList = procedureService.GetClaims
                (
                    userCode: userGuid.ToString(),
                    returnJustMyClaims: includeJustMyClaims
                );

            foreach (var ClaimDataObject in ClaimDataObjectList)
            {
                {
                    ClaimSummaryList.Add(new ClaimSummary(ClaimDataObject));
                }
            }

            return ClaimSummaryList;
        }
    }
}