﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    public class LookupService
    {
        public List<Lookup> GetAll()
        {
            var lookupTableService = new DataServiceLibrary.LookupTableService();

            return lookupTableService.GetAll().Select(lookupDataRow => new Lookup(lookupDataRow, getParentPath: true)).ToList();
        }

        public Lookup GetByGuid(Guid lookupGuid)
        {
            var lookupTableService = new DataServiceLibrary.LookupTableService();

            return new Lookup(lookupTableService.GetByGuid(lookupGuid));
        }

        public bool IsValidOrderTransitionCode(string orderTransitionTypeLookupCode)
        {
            var lookupTableService = new DataServiceLibrary.LookupTableService();

            var orderTransitionLookup = DataServiceLibrary.LookupTableValues.Code.OrderTransitionType.LookupValue;

            var orderTransitionTypeLookup = lookupTableService.TryGetByCode(orderTransitionLookup.Path, orderTransitionTypeLookupCode);

            return orderTransitionTypeLookup != null;
        }
        public string GetConfigValue(string configCode)
        {
            var oConfigService = new DataServiceLibrary.ConfigTableService ();

            var  oConfig = oConfigService.GetByCode(configCode);

            return oConfig.Value;
        }
    }
}
