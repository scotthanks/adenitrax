﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DataServiceLibrary;
using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    public class OrderLinePriceUpdateService
    {
        private readonly Guid _growerOrderGuid;
        private readonly Guid _orderLineGuid;
        private readonly SqlTransaction _transaction = null;

        public OrderLinePriceUpdateService(Guid growerOrderGuid,Guid orderLineGuid, SqlTransaction transaction = null)
        {
            _growerOrderGuid = growerOrderGuid;
            _orderLineGuid = orderLineGuid;
            _transaction = transaction;
        }

        public void UpdatePricesVoid()
        {
            var userGuid = new Guid();
            UpdatePrices(userGuid);
        }
        public int UpdatePrice(Guid userGuid)
        {
            // 
            //  

           
            int priceUpdated = 0;
            priceUpdated = UpdatePrices(userGuid);
            return priceUpdated;
        }
        public int UpdatePrices(Guid userGuid)
        {
            int pricesUpdated = 0;

            var service = new GrowerOrderService();
            
            var growerOrder = service.GetGrowerOrderDetail(_growerOrderGuid, includePriceData: true);

            var orderLineTableService = new OrderLineTableService();
            var status = new StatusObject(userGuid, true);
            foreach (var supplierOrder in growerOrder.SupplierOrders)
            {
                
                
                var repriceProc = new SupplierOrderService(status);
                repriceProc.Reprice(supplierOrder.SupplierOrderGuid,false);
                pricesUpdated = supplierOrder.OrderLines.Count();
            
                
            }

            return pricesUpdated;

            
        }

        public static void StartOrderLinePriceUpdateThread(Guid userGuid,Guid growerOrderGuid, SqlTransaction transaction = null)
        {
            var orderLinePriceUpdateService = new OrderLinePriceUpdateService(growerOrderGuid,Guid.Empty, transaction);

            Thread thread = new Thread(orderLinePriceUpdateService.UpdatePricesVoid);

            thread.Start();

            while (!thread.IsAlive)
            {
            }

            Thread.Sleep(1);
        }

        public static int UpdatePrices(Guid userGuid,Guid growerOrderGuid, bool runOnSeparateThread = false, SqlTransaction transaction = null)
        {
            int pricesUpdated = -1;

            if (runOnSeparateThread)
            {
                StartOrderLinePriceUpdateThread(userGuid,growerOrderGuid, transaction);
            }
            else
            {
                var orderLinePriceUpdateService = new OrderLinePriceUpdateService(growerOrderGuid, Guid.Empty,transaction);
                pricesUpdated = orderLinePriceUpdateService.UpdatePrices(userGuid);
            }

            return pricesUpdated;
        }
        public static int UpdatePrice(Guid userGuid, Guid growerOrderGuid, Guid orderLineGuid, SqlTransaction transaction = null)
        {
            int priceUpdated = -1;


            var orderLinePriceUpdateService = new OrderLinePriceUpdateService(growerOrderGuid, orderLineGuid, transaction);
            priceUpdated = orderLinePriceUpdateService.UpdatePrice(userGuid);
            

            return priceUpdated;
        }


    }
}
