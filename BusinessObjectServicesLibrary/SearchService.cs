﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using DataObjectLibrary;
using Utility;

namespace BusinessObjectServices
{
    public class SearchService : BusinessServiceBase
    {

        public SearchService(StatusObject status)
            : base(status)
        { }
    

           

        public SearchResult2 RunSearch( string searchValue, bool runFirstPage)
        {
            var returnData = new SearchResult2();
           
           
            var service = new SearchDataService(Status);
            var theData = service.GetSearchResult(searchValue, runFirstPage);

            returnData.Guid = theData.Guid;
            returnData.SearchString = theData.SearchString;
            returnData.ResultCount = theData.ResultCount;
            returnData.Page = theData.Page;
            returnData.RelatedSearches = theData.RelatedSearches;
            returnData.ResultRows = theData.ResultRows;

           return returnData;
        }
        public string[] GetAutoFillValues( string searchStem)
        {

            var service = new SearchDataService(Status);
            var theData = service.GetAutoFillResult(searchStem);
            
            return theData;
        }
     
   }
}
