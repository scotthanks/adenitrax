﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    //TODO: Refactor this service. The code should be common to Availability and Catalog.
    public class CatalogueService
    {
        public ProductFormCategory SelectedProductFormCategory { get; set; }

        public int Count { get; set; }
        public List<Catalogue> CategoryCollection { get; set; }
        public List<ProductFormCategory> ProductFormCollection { get; set; }  //Holds the ProductForms for Selected Catalogue

        public CatalogueService()
        {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, false);
            var service = new MenuService(status);
            var menuData = service.GetMenuData();

            this.Count = menuData.ProgramTypes.Count;

            var catalogList = new List<Catalogue>();

            foreach (var programType in menuData.ProgramTypes)
            {
                var catalog = new Catalogue()
                {
                    Guid = programType.Guid,
                    Key = programType.Code,
                    Name = programType.Name
                };

                var activeProductForms = new List<Guid>();
                foreach (var productForm in programType.ActiveProductForms)
                {
                    activeProductForms.Add(productForm.Guid);
                }

                catalog.FormsAvailable = activeProductForms.ToArray();

                catalogList.Add(catalog);
            }

            this.CategoryCollection = catalogList;//.ToArray();

            this.ProductFormCollection = menuData.ProductForms;            
        }

        private DateTime FirstMondayOfYear(int year)
        {
            DateTime date = new DateTime(year, 1, 1);  //get new years that year
            int offset = 8 - (int)date.DayOfWeek; offset = offset % 7;
            return date.AddDays(offset);
        }

        public int CurrentShipWeek
        {
            get
            {
                //Todo: Deprecated here, now is in ShipWeek Services
                //Note: this is good till End of 2014
                //Note: week 8 = 2/18/2013
                int returnValue = 8;
                var seed = new DateTime(2013, 2, 18);
                var now = DateTime.Today;
                //now = new DateTime(2013, 12, 30);  //Just Testing
                returnValue += ((now - seed).Days) / 7;
                if (returnValue > 52) { returnValue = returnValue - 52; };  //restarts at 52 weels, 12/30/2013 = ship week one of 2014
                return returnValue;
            }
        }

        public ShipWeek ReturnShipWeek(int week, int year)
        {
            var shipWeek = new ShipWeek();
            //Code to translate week and year into ShipWeek
            //ToDo: Replace this with something that returns accurate week
            //Current Algorithm assumes week one = first Monday of year so week 52 might be next year.
            shipWeek.MondayDate = this.FirstMondayOfYear(year).AddDays((week - 1) * 7);
            shipWeek.Week = shipWeek.MondayDate.DayOfYear / week;
            shipWeek.Year = shipWeek.MondayDate.Year;

            return shipWeek;
        }

        public List<ProductFormCategory> AllProductForms()
        {
            return this.ProductFormCollection;
        }

        public ProductFormCategory ProductForm(string productFormKey)
        {
            var productFormList = this.ProductFormCollection;
            foreach (var productForm in productFormList)
            {
                if (productForm.Code == productFormKey) { return productForm; }
            }
            return new ProductFormCategory();
        }

        public List<ProductFormCategory> CatalogueProductForms(string catalogueKey)
        {
            var catalog = Catalogue(catalogueKey);
            var formsAvailable = new HashSet<Guid>();

            foreach (var catalogFormGuid in catalog.FormsAvailable)
            {
                formsAvailable.Add(catalogFormGuid);
            }

            var list = new List<ProductFormCategory>();

            foreach (var productFormCategory in this.AllProductForms())
            {
                if (formsAvailable.Contains(productFormCategory.Guid))
                    list.Add(productFormCategory);
            }

            return list;
        }

        public List<Catalogue> Catalogues()
        {
            return this.CategoryCollection;
        }

        public Catalogue Catalogue(string key)
        {
            var catalogueList = this.CategoryCollection;
            foreach (var catalogue in catalogueList) {
                if (catalogue.Key == key) {return catalogue;}
            }
            return new Catalogue();
        }
    }
}
