﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectServices
{
    class ProductFormAvailabilityLinkedWeeksService
    {

        public List<BusinessObjectsLibrary.ProductFormAvailabilityLinkedWeeks> GetLinkedWeeks(Guid productFormGuid, Guid shipWeekGuid)
        {

            var linkedWeeks = new List<BusinessObjectsLibrary.ProductFormAvailabilityLinkedWeeks>();

            var linkWeeksService = new DataServiceLibrary.ProductFormAvailabilityLinkedWeeksTableService();

            var linkedProductFormsWeeks = linkWeeksService.GetAllByField("ProductFormGuid", productFormGuid.ToString(), "=", false);

            if (linkedProductFormsWeeks.Count > 0)
            {

                DataObjectLibrary.ProductFormAvailabilityLinkedWeeks
                    firstLinkedWeek = linkedProductFormsWeeks.First<DataObjectLibrary.ProductFormAvailabilityLinkedWeeks>();

                var weeks = linkWeeksService.GetAllByField("GroupingCode", firstLinkedWeek.GroupingCode.ToString(), "=", false);

                foreach (var week in weeks)
                {
                    var linkedWeek = new BusinessObjectsLibrary.ProductFormAvailabilityLinkedWeeks();
                    linkedWeek.Guid = week.Guid;
                    linkedWeek.GroupingCode = week.GroupingCode;
                    linkedWeek.ProductFormGuid = week.ProductFormGuid;
                    linkedWeek.ShipWeekGuid = week.ShipWeekGuid;
                    linkedWeeks.Add(linkedWeek);
                }
            }
            


            return linkedWeeks;
        }

        //public List<BusinessObjectsLibrary.Product> GetAllProgramProducts(Guid programGuid)
        //{

        //    var productService = new DataServiceLibrary.ProductTableService();
        //    //var products = productService.GetAll();

        //    var products = productService.GetAllByField("ProgramGuid", programGuid.ToString(), "=", false);

        //    var productList = new List<BusinessObjectsLibrary.Product>();

        //    foreach (var product in products)
        //    {

        //        var newProduct = new BusinessObjectsLibrary.Product();
        //        newProduct.Guid = product.Guid;
        //        newProduct.SupplierProductIdentifier = product.SupplierIdentifier;
        //        newProduct.SupplierProductDescription = product.SupplierDescription;
        //        newProduct.ProgramGuid = product.ProgramGuid;
        //        productList.Add(newProduct);


        //    }

        //    return productList;
        //}
    }
}
