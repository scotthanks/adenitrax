﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class StateService
    {
        public bool IsValidStateCode(string code)
        {
            var service = new DataServiceLibrary.StateTableService();

            //TODO: If true is entered here, it tries to use DateDeactivated, which is not valid.
            var state = service.TryGetByCode(code, false);

            return state != null;
        }
    }
}
