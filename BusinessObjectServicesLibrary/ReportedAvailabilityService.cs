﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class ReportedAvailabilityService
    {
        public DataObjectLibrary.ReportedAvailability UpdateReportedAvailability(Guid productGuid, Guid shipWeekGuid, int qtyAvailable)
        {

            ReportedAvailabilityTableService dbservice = new ReportedAvailabilityTableService();
            DataObjectLibrary.ReportedAvailability rptAvailability = dbservice.TryGetByProductAndShipWeek(productGuid, shipWeekGuid);

            DataObjectLibrary.ReportedAvailability updatedRptAvailability = null;

            if (rptAvailability != null)
            {

                updatedRptAvailability = dbservice.UpdateField(rptAvailability.Guid, "Qty", qtyAvailable);
            }
            else
            {
                DataObjectLibrary.ReportedAvailability newReportedAvailability = new DataObjectLibrary.ReportedAvailability();
                newReportedAvailability.ProductGuid = productGuid;
                newReportedAvailability.ShipWeekGuid = shipWeekGuid;
                newReportedAvailability.Qty = qtyAvailable;
                updatedRptAvailability = dbservice.Insert(newReportedAvailability);

                //updatedRptAvailabiity = dbservice.Insert(

            }


            return updatedRptAvailability;
        }


        public DataObjectLibrary.ReportedAvailability UpdateSalesSinceDateReported(Guid productGuid, Guid productFormGuid, Guid shipWeekGuid, int updateQty)
        {

            DataObjectLibrary.ReportedAvailability updatedReportedAvailability = null;

            ProductFormAvailabilityLinkedWeeksService lwService = new ProductFormAvailabilityLinkedWeeksService();
            List<BusinessObjectsLibrary.ProductFormAvailabilityLinkedWeeks> linkedWeeks = lwService.GetLinkedWeeks(productFormGuid, shipWeekGuid);

            if (linkedWeeks.Count > 0)
            {
                foreach (var week in linkedWeeks)
                {
                    updatedReportedAvailability = UpdateOneSalesSinceDateReported(productGuid, week.ShipWeekGuid, updateQty);
                }
            }
            else
            {
                updatedReportedAvailability = UpdateOneSalesSinceDateReported(productGuid, shipWeekGuid, updateQty);
            }


            return updatedReportedAvailability;
        }

        private DataObjectLibrary.ReportedAvailability UpdateOneSalesSinceDateReported(Guid productGuid, Guid shipWeekGuid, int updateQty)
        {

            DataObjectLibrary.ReportedAvailability reportedAvailability = null;

            ReportedAvailabilityTableService dbservice = new ReportedAvailabilityTableService();
            DataObjectLibrary.ReportedAvailability rptAvailability = dbservice.TryGetByProductAndShipWeek(productGuid, shipWeekGuid);

            if (rptAvailability.AvailabilityTypeLookup.Code == LookupTableValues.Code.AvailabilityType.Avail.Code)
            {

                if (rptAvailability != null)
                {

                    int currentQty = 0;
                    if (rptAvailability.SalesSinceDateReported.HasValue)
                    {
                        currentQty = (int)rptAvailability.SalesSinceDateReported;
                    }

                    int newQty = currentQty + updateQty;
                    reportedAvailability = dbservice.UpdateField(rptAvailability.Guid, "SalesSinceDateReported", newQty);
                }
            }
            return reportedAvailability;
        }

    }
}
