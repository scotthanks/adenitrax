﻿using System.Collections.Generic;
using System.Linq;
using BusinessObjectsLibrary;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class ProgramTypeService
    {
        public List<ProgramType> GetAll()
        {
            var programTypeTableService = new ProgramTypeTableService();

            return
                programTypeTableService.GetAll()
                    .Select(
                        programTypeDataRow =>
                        new ProgramType(programTypeDataRow))
                    .ToList();
        }
        public ProgramType GetByCode(string programTypeCode)
        {
            //TODO: This requires a round trip to the database. This could be avoided by caching.
            var programTypeTableService = new ProgramTypeTableService();
            var programTypeTableObject = programTypeTableService.GetByCode(programTypeCode, excludeInactive: true);
            return new ProgramType(programTypeTableObject);
        }
    }
}