﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AuthorizeNet;

namespace BusinessObjectServices
{
    
    
    public class CardChargeService
    {

        private const string API_LOGIN_ID = "2HAk9yxjT6p";         //ToDo: Move this value to WebConfig, Table Storage, or Database
        private const string TRANSACTION_KEY = "97ar45699KUcZCRh";  //ToDo: Move this value to WebConfig, Table Storage, or Database

        private const int CARD_NUMBER_LENGTH_MAX = 16;
        private const int CARD_NUMBER_LENGTH_MIN = 13;
        private const int EXPIRES_LENGTH = 4;

        public CardChargeService(CardCharge charge)
        {
            if (this.IsValid(charge)) {

                //step 1 - create the request
                var request = new AuthorizationRequest(charge.CardNumber, charge.Expires, charge.Amount, charge.Description );
                
                //step 2 - create the gateway, sending in your credentials
                var gate = new Gateway(API_LOGIN_ID, TRANSACTION_KEY, true);

                //step 3 - make some money
                var response = gate.Send(request);
                
                //step 4 - marshal response
                if (response != null)
                {
                    this.Response = new CardChargeResponse()
                    {
                        Approved = response.Approved,
                        Code = response.ResponseCode,
                        Message = response.Message
                    };
                }

            };
        }

        public CardChargeResponse Response { get; set; }

        private bool IsValid(CardCharge charge) {
            if (charge.CardNumber == null || charge.CardNumber.Length < CARD_NUMBER_LENGTH_MIN || charge.CardNumber.Length > CARD_NUMBER_LENGTH_MAX) { return false; };
            if (charge.Expires == null || charge.Expires.Length != EXPIRES_LENGTH) { return false; };
            if (charge.Amount < 0) { return false; };
            return true;
        }
            //ToDo: add private function to parse and validate Expires
       
    }

    public class CardCharge {
        public string CardNumber { get; set; }
        public string Expires { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }

    public class CardChargeResponse {
        public bool Approved {get; set;}
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
