﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;
using System.Data.SqlClient;
using System.Data;
using BusinessObjectsLibrary;
namespace BusinessObjectServices
{
    public class BreederService
    {
        public IEnumerable<GeneticOwner> SelectBreeders(string plantCategoryCode, string productFormCode)
        {   
            //PlantCategoryCode maps to ProgramType[code] (e.g. VA)
            //ProductFormCode maps ProductFormCategory[code] (e.g. RC)
            //Note: Table named Species has code = cal for Calibrachoa

            //plantCategoryCode = "VA";
            //productFormCode = "RC";
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var procedure = new DataServiceLibrary.VarietyDataService(status);

            var geneticOwnerList = 
                procedure.GetGeneticOwners
                (
                    programTypeCode: plantCategoryCode,
                    productFormCategoryCode: productFormCode
                );

            var breederList = geneticOwnerList.Select
                (
                    geneticOwner => new GeneticOwner() 
                    {
                        Code = geneticOwner.Code, 
                        Name = geneticOwner.Name
                    }
                ).ToList();

            breederList = breederList.OrderBy(o => o.Name).ToList();

            return breederList;
        }
    }
}
