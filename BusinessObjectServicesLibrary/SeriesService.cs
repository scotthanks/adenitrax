﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using DataServiceLibrary;

namespace BusinessObjectServices
{
    public class SeriesService
    {

        public Series GetSeriesFromGuid(Guid? seriesGuid)
        {

            SeriesTableService dbService = new SeriesTableService();
            DataObjectLibrary.Series dbSeries = dbService.TryGetByGuid(seriesGuid);
          
            Series series = new Series();

            series.SeriesGuid = dbSeries.Guid;
            series.Code = dbSeries.Code;
            series.Name = dbSeries.Name;
            series.SpeciesGuid = dbSeries.SpeciesGuid;
            series.DescriptionHTMLGuid = dbSeries.DescriptionHTMLGUID;
            series.CultureLibraryLink = dbSeries.CultureLibraryLink;
            return series;

        }

    }
}
