﻿using System;
using System.Collections.Generic;
using BusinessObjectsLibrary;
using DataServiceLibrary;


namespace BusinessObjectServices
{
    public class ClinicPatientService : BusinessServiceBase
    {
        public ClinicPatientService(StatusObject status)
            : base(status)
        { }


        public List<ClinicPatientVisit> GetClinicPatientVisits(Guid clinicPatientGuid, DateTime dateStart, DateTime dateEnd)
        {
            var clinicDataService = new ClinicPatientDataService(Status);
            var list = clinicDataService.GetClinicPatientVisits(clinicPatientGuid, dateStart, dateEnd);
            return list;
        }
       
    }
}
