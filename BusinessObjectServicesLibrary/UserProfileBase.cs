﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using DataAccessLibrary;
//using Helpers;


namespace BusinessServiceLibrary
//Intentionally not using the Base Service class becuase this has the Role and permission stuff that is 
//set in the contrcutor of the base so is used by the base.  Would be circular....
{
    public class UserProfileBase
    {

        public UserProfileBase(StatusObject status)
        {

            this.Status = status;



        }

        public StatusObject Status { get; set; }

        //public List<RoleObject> GetRolesForUserInAuthoirtyRegulatoryProgramCompany(Guid userGuid,Guid companyGuid)
        //{

        //    var service = new LinkoUserAccess(Status);
        //    var connection = service.SqlConnectionGet("DataDb");
        //    var gGuid = new Guid();
        //    var roles = new List<RoleObject>();
        //    SqlDataReader reader = service.GetRolesForUserInAuthoirtyRegulatoryProgramCompany(connection, userGuid, companyGuid);
        //    if (reader != null && reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            var helper = new ReaderHelper();
        //            var role = new RoleObject(); 
        //            role.Guid = helper.GetGuid(reader["Guid"]);
        //            role.Code = helper.GetString(reader["Code"]);
        //            role.Name = helper.GetString(reader["Name"]);
        //            role.RoleTypeLookupGuid = helper.GetGuid(reader["RoleTypeLookupGuid"]);
        //            role.RoleType = helper.GetString(reader["RoleType"]);
        //            roles.Add(role);
        //        }
        //        reader.Close();
        //    }

        //    connection.Close();

        //    return roles;
        //}
        //public List<RoleObject> GetRolesForUserInAuthoirtyRegulatoryProgram(Guid userGuid)
        //{

        //    var service = new LinkoUserAccess(Status);
        //    var connection = service.SqlConnectionGet("DataDb");
        //    var gGuid = new Guid();
        //    var roles = new List<RoleObject>();
        //    SqlDataReader reader = service.GetRolesForUserInAuthoirtyRegulatoryProgram(connection);
        //    if (reader != null && reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            var helper = new ReaderHelper();
        //            var role = new RoleObject();
        //            role.Guid = helper.GetGuid(reader["Guid"]);
        //            role.Code = helper.GetString(reader["Code"]);
        //            role.Name = helper.GetString(reader["Name"]);
        //            role.RoleTypeLookupGuid = helper.GetGuid(reader["RoleTypeLookupGuid"]);
        //            role.RoleType = helper.GetString(reader["RoleType"]);
        //            roles.Add(role);
        //        }
        //        reader.Close();
        //    }

        //    connection.Close();

        //    return roles;
        //}
        //public List<string> GetPermissionsForUserInAuthoirtyRegulatoryProgram()
        //{

        //    var service = new LinkoUserAccess(Status);
        //    var connection = service.SqlConnectionGet("DataDb");
        //    var gGuid = new Guid();
        //    var permissions = new List<string>();
        //    SqlDataReader reader = service.GetPermissionsForUserInAuthoirtyRegulatoryProgram(connection);
        //    if (reader != null && reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            var helper = new ReaderHelper();
        //            string permission = helper.GetString(reader["Code"]);

        //            permissions.Add(permission);
        //        }
        //        reader.Close();
        //    }

        //    connection.Close();

        //    return permissions;
        //}
        //public List<string> GetPermissionsForUserInAuthoirtyRegulatoryProgramCompany()
        //{

        //    var service = new LinkoUserAccess(Status);
        //    var connection = service.SqlConnectionGet("DataDb");
        //    var gGuid = new Guid();
        //    var permissions = new List<string>();
        //    SqlDataReader reader = service.GetPermissionsForUserInAuthoirtyRegulatoryProgramCompany(connection);
        //    if (reader != null && reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            var helper = new ReaderHelper();
        //            string permission = helper.GetString(reader["Code"]);

        //            permissions.Add(permission);
        //        }
        //        reader.Close();
        //    }

        //    connection.Close();

        //    return permissions;
        //}



        //public IEnumerable<RoleObject> GetAuthorityRoles()
        //{
        //    return GetRolesByUXType("AuthorityUX");
        //}
        //public IEnumerable<RoleObject> GetIndusutrialUserRoles()
        //{
        //    return GetRolesByUXType("IndustrialUserUX");
        //}
        //private IEnumerable<RoleObject> GetRolesByUXType(string sType)
        //{

        //    var list = new List<RoleObject>();


        //    var service = new LinkoUserAccess(Status);
        //    var connection = service.SqlConnectionGet("DataDb");
        //    SqlDataReader reader = service.Role_GetByUXType(connection, sType);
        //    if (reader != null && reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            var helper = new ReaderHelper();
        //            var role = new RoleObject();
        //            role.Guid = helper.GetGuid(reader["Guid"]);
        //            role.Name = helper.GetString(reader["Name"]);
        //            role.RoleTypeLookupGuid = helper.GetGuid(reader["RoleTypeLookupGuid"]);
        //            role.RoleType = helper.GetString(reader["RoleType"]);



        //            list.Add(role);
        //        }
        //        reader.Close();
        //    }
        //    connection.Close();
        //    return list;

        //}


        //public bool UserProfileLoad()
        //{

        //    var service = new LinkoUserAccess(Status);
        //    var connection = service.SqlConnectionGet("DataDb");
        //    try
        //    {
        //        SqlDataReader reader = service.UserProfileDataReader(connection);
        //        if (reader != null && reader.HasRows)
        //        {
        //            reader.Read();

        //            var helper = new ReaderHelper();


        //            Status.User.LinkoExchangeUserGuid = helper.GetGuid(reader["LinkoExchangeUserGuid"]);
        //            Status.IsUserProfileInfoLoaded = true;
        //            Status.User.UserStatus = helper.GetString(reader["UserStatus"]);
        //            Status.User.KnowledgeBasedQuestionsNeeded = 5;
        //            Status.User.KnowledgeBasedQuestionsCount = helper.GetInt32(reader["KnowledgeBasedQuestionCount"]);
        //            Status.User.SecurityQuestionsCount = 2;
        //            Status.User.SecurityQuestionsCount = helper.GetInt32(reader["SecurityQuestionCount"]) ;
        //            Status.User.NeedsPasswordReset  = helper.GetBool(reader["NeedsPasswordReset"]) ;

        //            if (Status.User.UserGuid == null)
        //            {
        //                Status.User.NeedsCredentials = true;
        //            }
        //            else {
        //                Status.User.NeedsCredentials = false;
        //            }
        //            reader.Close();

        //        }
        //        Status.Success = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        var error = new ErrorObject()
        //        {
        //            ErrorNumber = ex.HResult,
        //            ErrorLocation = "UserProfileService",
        //            ErrorMessageUser = ex.Message,
        //            ErrorMessageSystem = ex.Message,
        //            Error = ex.Source
        //        };
        //        AddError(error);
        //        Status.Success = false;
        //    }

        //    finally
        //    {
        //        connection.Close();
        //    }
        //    return Status.Success;

        //}

        public bool AddError(ErrorObject error)
        {
            List<ErrorObject> errorList = null;
            if (Status.Errors == null)
            { errorList = new List<ErrorObject>(); }
            else
            { errorList = Status.Errors.ToList(); }
            errorList.Add(error);
            Status.Errors = errorList;
            return true;

        }




    }
}
