﻿//using ePS.Types;
//using System;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BusinessObjectServices
//{
//    public class BillingSummaryService
//    {
//        public List<BillingSummary> GetBillingSummaries(BillingSummaryRequest request)
//        {
//            request.OrderStatusCode = request.OrderStatusCode + "%";
//            List<BillingSummary> summaryList = new List<BillingSummary>();
//            try
//            {
//                using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
//                {
//                    connection.Open();

//                    string query = string.Format(
//                    @"select 
//	                Grower.Name, 
//	                Grower.CreditLimit, 
//	                Grower.CreditLimitTemporaryIncrease,
//	                Grower.CreditLimitTemporaryIncreaseExpiration,
//	                Grower.SalesTaxNumber,
//	                Grower.EMail,
//	                Grower.PhoneAreaCode,
//	                Grower.Phone,
//                    OrderBillingView.OrderGuid,
//	                OrderBillingView.FeeTotal,
//	                OrderBillingView.ProductTotal,
//	                OrderBillingView.OrderTotal,	 
//	                OrderBillingView.CustomerPoNo,
//	                OrderBillingView.OrderNumber,
//	                OrderBillingView.Description,
//                    OrderBillingView.GrowerGuid,
//	                ShipWeek.Week, 
//	                ShipWeek.Year,
//	                GrowerOrderLowestOrderLineStatusView.OrderLineStatusLookupGuid,
//	                orderLineStatus.Code as OrderStatus,
//	                payType.Code as AccountType
//                from OrderBillingView
//	                left join Grower on Grower.Guid = OrderBillingView.GrowerGuid
//	                left join ShipWeek on ShipWeek.Guid = OrderBillingView.ShipWeekGuid
//	                left join Lookup as payType on OrderBillingView.PaymentTypeLookupGuid = payType.Guid
//	                left join GrowerOrderLowestOrderLineStatusView on GrowerOrderLowestOrderLineStatusView.GrowerOrderGuid = OrderBillingView.OrderGuid
//	                left join Lookup as orderLineStatus on GrowerOrderLowestOrderLineStatusView.OrderLineStatusLookupGuid = orderLineStatus.Guid
//                where payType.Code like '{0}'
//	                  AND orderLineStatus.Code like '{1}'
//	                  AND GrowerOrderStatusLookupGuid != '{2}' 
//                      AND GrowerOrderStatusLookupGuid != '{3}'",
//                      request.PaymentTypeCode, request.OrderStatusCode, 
//                      "A00FF27A-798C-41F3-B930-A4FD4F82725C", 
//                      "E5D18A23-076A-4936-99AC-EB533691671B");

//                    SqlCommand sqlCmd = new SqlCommand(query, connection);

//                    SqlDataReader reader = sqlCmd.ExecuteReader();

//                    while (reader.Read())
//                    {
//                        BillingSummary summary = new BillingSummary();

//                        summary.OrderGuid = (Guid)reader["OrderGuid"];
//                        summary.Name = reader["Name"] as string;
//                        summary.CreditLimit = Convert.ToDecimal(reader["CreditLimit"]);
//                        summary.CreditLimitTemporaryIncrease = Convert.ToDecimal(reader["CreditLimitTemporaryIncrease"]);
//                        summary.CreditLimitTemporaryIncreaseExpiration = reader["CreditLimitTemporaryIncreaseExpiration"] as string;
//                        summary.SalesTaxNumber = reader["SalesTaxNumber"] as string;
//                        summary.EMail = reader["EMail"] as string;
//                        summary.PhoneAreaCode = reader["PhoneAreaCode"] as string;
//                        if (reader["FeeTotal"] != DBNull.Value)
//                            summary.FeeTotal = Convert.ToDecimal(reader["FeeTotal"]);
//                        else
//                            summary.FeeTotal = 0;
//                        if (reader["ProductTotal"] != DBNull.Value)
//                            summary.ProductTotal = Convert.ToDecimal(reader["ProductTotal"]);
//                        else
//                            summary.ProductTotal = 0;
//                        if (reader["OrderTotal"] != DBNull.Value)
//                            summary.OrderTotal = Convert.ToDecimal(reader["OrderTotal"]);
//                        else
//                            summary.OrderTotal = summary.FeeTotal + summary.ProductTotal;
//                        summary.CustomerPoNo = reader["CustomerPoNo"] as string;
//                        summary.OrderNumber = reader["OrderNumber"] as string;
//                        summary.Description = reader["Description"] as string;
//                        summary.Week = Convert.ToString(reader["Week"]);
//                        summary.Year = Convert.ToString(reader["Year"]);
//                        summary.OrderStatus = reader["OrderStatus"] as string;
//                        summary.AccountType = reader["AccountType"] as string;
//                        summary.GrowerGuid = (Guid)reader["GrowerGuid"];
 
//                        summaryList.Add(summary);
//                    }
//                    var sortedResponseList = summaryList.OrderBy(x => x.Week).ToList();

//                    return sortedResponseList;

//                }
//            }
//            catch (Exception Exc)
//            {
//                return summaryList;
//            }
//        }

//    }
//}
