﻿using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectServices
{
    public class LedgerEntry
    {
        public DateTime TransactionDate { get; set; }
        public string InteralIdNumber { get; set; }
        public string ExternalIdNumber { get; set; }
        public decimal Amount { get; set; }
        public Guid TransactionGuid { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class OrderAccountService
    {
        public LedgerEntry GetInvoiceEntry(Guid orderGuid)
        {
            LookupTableValues.CodeValues.GeneralLedgerAccountValues accountLookupService = new LookupTableValues.CodeValues.GeneralLedgerAccountValues();
            Guid GLAccountGuid = accountLookupService.AccountsRecievable.Guid;

            LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues entryTypeLookupService = new LookupTableValues.CodeValues.GeneralLedgerEntryTypeValues();
            Guid entryTypeLookupGuid = entryTypeLookupService.Debit.Guid;

            return GetLedgerEntry(GLAccountGuid, orderGuid, entryTypeLookupGuid);
        }

        public LedgerEntry GetLedgerEntry(Guid GLAccountGuidLookupGuid, Guid parentGuid, Guid entryTypeLookupGuid)
        {
            LedgerEntry ledgerEntry = new LedgerEntry();
            try
            {
                using (var connection = DataAccessLibrary.ConnectionServices.ConnectionToData)
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText =
                        string.Format(
                            @"select Amount, TransactionDate, InternalIdNumber, ExternalIdNumber, TransactionGuid from Ledger 
                            where ParentGuid = '{0}' AND EntryTypeLookupGuid = '{1}' AND GLAccountLookupGuid = '{2}'",
                        parentGuid, entryTypeLookupGuid, GLAccountGuidLookupGuid);

                    SqlDataReader reader = command.ExecuteReader();
                    if (!reader.HasRows)
                        throw new Exception("Ledger entry not found");

                    reader.Read();
                    ledgerEntry.Amount = (decimal)reader["Amount"];
                    if (reader["InternalIdNumber"] != DBNull.Value)
                        ledgerEntry.InteralIdNumber = reader["InternalIdNumber"] as string;
                    if (reader["ExternalIdNumber"] != DBNull.Value)
                        ledgerEntry.ExternalIdNumber = reader["ExternalIdNumber"] as string;
                    ledgerEntry.TransactionDate = (DateTime)reader["TransactionDate"];
                    ledgerEntry.TransactionGuid = (Guid)reader["TransactionGuid"];
                    ledgerEntry.Success = true;                   
                }
            }
            catch (Exception ex)
            {
                ledgerEntry.Success = false;
                ledgerEntry.Message = ex.Message;
            }
            return ledgerEntry;
        }
    }
    
}
