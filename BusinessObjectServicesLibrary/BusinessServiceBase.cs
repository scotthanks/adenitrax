﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using BusinessObjectsLibrary;
using BusinessServiceLibrary;




namespace BusinessObjectServices
{
    public abstract class BusinessServiceBase
    {
        public BusinessServiceBase(StatusObject status)
        {

            this.Status = status;
            var service = new UserProfileBase(Status);




        }
        public StatusObject Status { get; set; }

        public bool AddError(ErrorObject error)
        {
            List<ErrorObject> errorList = null;
            if (Status.Errors == null)
            { errorList = new List<ErrorObject>(); }
            else
            { errorList = Status.Errors.ToList(); }
            errorList.Add(error);
            Status.Errors = errorList;
            return true;

        }

    }
}





