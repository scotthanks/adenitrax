﻿using System;
using System.Collections.Generic;
using DataServiceLibrary;
using BusinessObjectsLibrary;

namespace BusinessObjectServices
{
    public class OrderSummaryService
    {
        //public List<OrderSummary> GetMyShoppingCartOrders(Guid userGuid)
        //{
        //    return GetOrders
        //        (
        //            userGuid,
        //            includeJustMyOrders: true,
        //            includePendingOrders: true,
        //            includePlacedOrders: false,
        //            includeShippedOrders: false,
        //            includeCancelledOrders: false
        //        );
        //}

        //public List<OrderSummary> GetMyOpenOrders(Guid userGuid)
        //{
        //    return GetOrders
        //        (
        //            userGuid,
        //            includeJustMyOrders: true,
        //            includePendingOrders: false,
        //            includePlacedOrders: true,
        //            includeShippedOrders: false,
        //            includeCancelledOrders: false
        //        );
        //}

        public List<OrderSummary> GetMyShippedOrders(Guid userGuid)
        {
            return GetOrders
                (
                    userGuid,
                    includeJustMyOrders: true,
                    includePendingOrders: false,
                    includePlacedOrders: false,
                    includeShippedOrders: true,
                    includeCancelledOrders: false
                );
        }

        //public List<OrderSummary> GetMyOrders(Guid userGuid)
        //{
        //    return GetOrders
        //        (
        //            userGuid,
        //            includeJustMyOrders: true,
        //            includePendingOrders: false,
        //            includePlacedOrders: true,
        //            includeShippedOrders: true,
        //            includeCancelledOrders: true
        //        );
        //}

        //public List<OrderSummary> GetMyCancelledOrders(Guid userGuid)
        //{
        //    return GetOrders
        //        (
        //            userGuid,
        //            includeJustMyOrders: true,
        //            includePendingOrders: false,
        //            includePlacedOrders: false,
        //            includeShippedOrders: false,
        //            includeCancelledOrders: true
        //        );
        //}

        public List<OrderSummary> GetAllShoppingCartOrders(Guid userGuid)
        {
            return GetOrders
                (
                    userGuid,
                    includeJustMyOrders: false,
                    includePendingOrders: true,
                    includePlacedOrders: false,
                    includeShippedOrders: false,
                    includeCancelledOrders: false
                );
        }

        public List<OrderSummary> GetAllOpenOrders(Guid userGuid)
        {
            return GetOrders
                (
                    userGuid,
                    includeJustMyOrders: false,
                    includePendingOrders: false,
                    includePlacedOrders: true,
                    //includeShippedOrders: false,
                    //includeCancelledOrders: false
                    includeShippedOrders: true,
                    includeCancelledOrders: true
                );
        }

        public List<OrderSummary> GetAllShippedOrders(Guid userGuid)
        {
            return GetOrders
                (
                    userGuid,
                    includeJustMyOrders: false,
                    includePendingOrders: false,
                    includePlacedOrders: false,
                    includeShippedOrders: true,
                    includeCancelledOrders: false
                );
        }

        public List<OrderSummary> GetAllOrders(Guid userGuid)
        {
            return GetOrders
                (
                    userGuid,
                    includeJustMyOrders: false,
                    includePendingOrders: false,
                    includePlacedOrders: true,
                    includeShippedOrders: true,
                    includeCancelledOrders: true
                );
        }

        public List<OrderSummary> GetAllCancelledOrders(Guid userGuid)
        {
            return GetOrders
                (
                    userGuid,
                    includeJustMyOrders: false,
                    includePendingOrders: false,
                    includePlacedOrders: false,
                    includeShippedOrders: false,
                    includeCancelledOrders: true
                );
        }

        private List<OrderSummary> GetOrders
            (
                Guid userGuid,
                bool includeJustMyOrders,
                bool includePendingOrders,
                bool includePlacedOrders,
                bool includeShippedOrders,
                bool includeCancelledOrders
            )
        {
            var status = new StatusObject(userGuid, true);
            var procedureService = new DataServiceLibrary.GrowerOrderSummaryDataService(status);

            var orderSummaryList = new List<OrderSummary>();

            string growerOrderStatusLookupCode = null;
            string supplierOrderStatuslookupCode = null;
            string orderLineStatusLookupCode = null;

            if (includePendingOrders && !includePlacedOrders && !includeShippedOrders && !includeCancelledOrders)
            {
                orderLineStatusLookupCode = LookupTableValues.Code.OrderLineStatus.Pending.Code;
            }

            var orderSummaryDataObjectList = procedureService.GetGrowerOrderSummaries
                (
                    //TODO: Replace this with a Guid parameter to the procedure.
                    userCode: userGuid.ToString(),
                    orderTypeLookupCode: "Order",
                    growerOrderStatusLookupCode: growerOrderStatusLookupCode,
                    supplierOrderStatusLookupCode: supplierOrderStatuslookupCode,
                    orderLineStatusLookupCode: orderLineStatusLookupCode,
                    returnJustMyOrders: includeJustMyOrders
                );

            foreach (var orderSummaryDataObject in orderSummaryDataObjectList)
            {
                if
                    (
                        (
                            includePendingOrders &&
                            orderSummaryDataObject.LowestOrderLineStatusLookup.SortSequence < LookupTableValues.Code.OrderLineStatus.Ordered.SortSequence
                        )
                        ||
                        (
                            includePlacedOrders &&
                            orderSummaryDataObject.GrowerOrderStatusLookup.SortSequence < LookupTableValues.Code.GrowerOrderStatus.Invoiced.SortSequence &&
                            orderSummaryDataObject.LowestOrderLineStatusLookup.SortSequence >= LookupTableValues.Code.OrderLineStatus.Ordered.SortSequence &&
                            orderSummaryDataObject.LowestOrderLineStatusLookup.SortSequence < LookupTableValues.Code.OrderLineStatus.Cancelled.SortSequence
                        )
                        ||
                        (
                            includeShippedOrders &&
                            (
                                (
                                    orderSummaryDataObject.GrowerOrderStatusLookup.SortSequence >= LookupTableValues.Code.GrowerOrderStatus.Invoiced.SortSequence &&
                                    orderSummaryDataObject.GrowerOrderStatusLookup.SortSequence <= LookupTableValues.Code.GrowerOrderStatus.Paid.SortSequence
                                ) ||
                                orderSummaryDataObject.LowestOrderLineStatusLookup.Guid == LookupTableValues.Code.OrderLineStatus.Shipped.Guid
                            )
                        )
                        ||
                        (
                            includeCancelledOrders 
                            &&
                            //(
                            //
                              //  orderSummaryDataObject.GrowerOrderStatusLookup.Guid == LookupTableValues.Code.GrowerOrderStatus.Cancelled.Guid 
                        //||
                               orderSummaryDataObject.LowestOrderLineStatusLookup.Guid == LookupTableValues.Code.OrderLineStatus.Cancelled.Guid
                            //)
                        )
                    )
                {
                    orderSummaryList.Add(new OrderSummary(orderSummaryDataObject));
                }
            }

            return orderSummaryList;
        }
    }
}
