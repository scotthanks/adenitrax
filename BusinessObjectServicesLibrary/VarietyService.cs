﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class VarietyService
    {
        public IEnumerable<Species> SelectVarietiesGroupedBySpecies(Guid userGuid,string plantCategoryCode = null, string productFormCode = null, string[] supplierCodeList = null, string[] breederCodeList = null, string[] speciesCodeList = null)
        {
            var status = new StatusObject(userGuid, true);
            var procedure = new DataServiceLibrary.VarietyDataService(status);

            //TODO: Add logic to the stored procedure "proxy" so that if the stuff passed in is longer than the allowed parameter, it takes an exception.
            var varietyTableObjectList = 
                procedure.GetVarieties
                (                   
                    programTypeCode: plantCategoryCode,
                    productFormCategoryCode: productFormCode,
                    geneticOwnerCodes: breederCodeList,
                    supplierCodes: supplierCodeList,
                    speciesCodes: speciesCodeList
                );

            // Group by Species into the Species.SelectedVarieties property.

            var speciesDictionary = new Dictionary<Guid, Species>();
            foreach (var varietyTableObject in varietyTableObjectList)
            {
                string thumbnailImageUrl = GetImageUrl(varietyTableObject, "Thumbnail");
                string fullImageUrl = GetImageUrl(varietyTableObject, "Full");

                var variety = new Variety(varietyTableObject, thumbnailImageUrl, fullImageUrl);

                Guid speciesGuid = varietyTableObject.SpeciesGuid;
                Species species;
                if (!speciesDictionary.ContainsKey(speciesGuid))
                {
                    species = new Species(varietyTableObject.Species);
                    speciesDictionary.Add(speciesGuid, species);
                }
                else
                {
                    bool found = speciesDictionary.TryGetValue(speciesGuid, out species);

                    if (!found || species == null)
                    {
                        throw new ApplicationException(string.Format( "The species guid {0} was not found in the speciesDictionary.", speciesGuid));
                    }
                }

                species.SelectedVarieties.Add(variety);
            }

            var speciesList = speciesDictionary.Values.ToList();

            speciesList = speciesList.OrderBy(o => o.Name).ToList();

            foreach (var species in speciesList)
            {
                species.SelectedVarieties = species.SelectedVarieties.OrderBy(o => o.Name).ToList();
            }

            return speciesList;
        }

        private string GetImageUrl(DataObjectLibrary.Variety varietyTableObject, string imageType)
        {
            //TODO: This is not the way to handle the default!!!
            //string defaultImageName = "Unknown" + (imageType == "Full" ? "Large" : "Thumb");
            string defaultImageName = "Unknown_" + (imageType == "Full" ? "FUILL" : "Thumbnail");

            string imageUrl = DataObjectLibrary.ImageFile.GetImageUrl(ImageFile.ImageContainerNameEnum.CatalogImages, defaultImageName, "JPG");

            //TODO: Make it so you don't have to check for guid = null
            if (varietyTableObject.ImageSetGuid != null && varietyTableObject.ImageSet != null)
            {
                var lookupService = DataServiceLibrary.LookupTableService.SingletonInstance;
                //TODO: Do something better with this (maybe bury it in the lookup service).
                var thumnailImageFileTypeLookup = lookupService.GetByPath("Code/ImageFileType/" + imageType, excludeInactive: true);
                foreach (var imageFile in varietyTableObject.ImageSet.ImageFileList)
                {
                    if (imageFile.ImageFileTypeLookupGuid == thumnailImageFileTypeLookup.Guid)
                    {
                        imageUrl = imageFile.GetImageUrl(ImageFile.ImageContainerNameEnum.CatalogImages);
                    }
                }
            }

            return imageUrl;
        }

        public Variety GetVarietyByCode(string varietyCode)
        {
            if (string.IsNullOrEmpty(varietyCode))
            {
                throw new ArgumentNullException("varietyCode");
            }
            var userGuid = new Guid();
            var status = new StatusObject(userGuid, true);
            var procedure = new DataServiceLibrary.VarietyDataService(status);
            
            var varietyList = procedure.GetVarieties (varietyCode: varietyCode);

            if (varietyList == null || varietyList.Count > 1)
            {
                throw new DataException("Unexpected result from GetVarieties procedure.");
            }
            else if (varietyList.Count == 0)
            {
                throw new ArgumentException(string.Format( "No Variety was found with the code {0}.", varietyCode));
            }

            var varietyTableRow = varietyList.First();

            //TODO: Using strings here is bad.
            string thumbnailImageUrl = GetImageUrl(varietyTableRow, "Thumbnail");
            string fullImageUrl = GetImageUrl(varietyTableRow, "Full");

            return new Variety(varietyTableRow, thumbnailImageUrl, fullImageUrl);
        }
    }
}
