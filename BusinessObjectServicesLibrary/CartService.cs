﻿using System.Data.SqlClient;
using System.Web;
using BusinessObjectsLibrary;
using DataAccessLibrary;
using DataObjectLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Threading;
using ePS.Types;

using Lookup = DataObjectLibrary.Lookup;


namespace BusinessObjectServices
{   
    public class CartService : BusinessServiceBase
    {
        public CartService(StatusObject status)
            : base(status)
        {

        }


        public List<CartOrderSummary> GetCartOrderSummaries()
        {

            var service = new CartDataService(Status);

            var list = service.GetCartSummaries();


            return list;
        }
    }
}