﻿
CREATE view [dbo].[PermissionsView]
AS
	SELECT
		UserProfile.UserId,
		UserProfile.UserGuid,
		UserProfile.UserName,
		Permissions.PermissionName,
		Permissions.PermissionId
	FROM UserProfile
		INNER JOIN webpages_UsersInRoles
			ON UserProfile.UserId = webPages_UsersInRoles.UserId
		INNER JOIN webPages_Roles
			ON webPages_UsersInRoles.RoleId = webpages_Roles.RoleId
		INNER JOIN PermissionsInRoles
			ON webpages_Roles.RoleId = PermissionsInRoles.RoleId
		INNER JOIN Permissions
			ON PermissionsInRoles.PermissionId = Permissions.PermissionId
	GROUP BY
		UserProfile.UserId,
		UserProfile.UserGuid,
		UserProfile.UserName,
		Permissions.PermissionName,
		Permissions.PermissionId


