﻿CREATE TABLE [dbo].[UserProfile] (
    [UserId]       INT              IDENTITY (1, 1) NOT NULL,
    [UserName]     NVARCHAR (56)    NOT NULL,
    [EmailAddress] NVARCHAR (1024)  CONSTRAINT [DF__tmp_ms_xx__Email__03F0984C] DEFAULT ('') NOT NULL,
    [UserGuid]     UNIQUEIDENTIFIER CONSTRAINT [DF__UserProfile] DEFAULT (newid()) NOT NULL,
    [GrowerGuid]   UNIQUEIDENTIFIER CONSTRAINT [DF__UserProfi__Growe__14270015] DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    CONSTRAINT [PK__UserProfile] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [UQ__UserProfile] UNIQUE NONCLUSTERED ([UserName] ASC)
);



