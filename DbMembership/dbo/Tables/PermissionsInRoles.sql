﻿CREATE TABLE [dbo].[PermissionsInRoles] (
    [PermissionId] INT NOT NULL,
    [RoleId]       INT NOT NULL,
    CONSTRAINT [PK__PermissionInrolesPermissionID] PRIMARY KEY CLUSTERED ([PermissionId] ASC, [RoleId] ASC),
    CONSTRAINT [fk_PermissionsInRolesRoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[webpages_Roles] ([RoleId])
);

