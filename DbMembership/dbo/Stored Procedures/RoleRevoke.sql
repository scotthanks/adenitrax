﻿create procedure dbo.RoleRevoke
	@UserName AS NVARCHAR(56),
	@RoleName AS NVARCHAR(256),
	@Success AS BIT = NULL OUT
AS
	DELETE webpages_UsersInRoles
	FROM webPages_UsersInRoles
		INNER JOIN UserProfile
			ON UserProfile.UserId = webpages_UsersInRoles.UserId
		INNER JOIN webpages_Roles
			ON webpages_UsersInRoles.RoleId = webpages_Roles.RoleId
	WHERE
		UserProfile.UserName = @UserName AND
		webpages_Roles.RoleName = @RoleName

	IF @@ROWCOUNT = 1
		SET @Success = 1
	ELSE
		SET @Success = 0
