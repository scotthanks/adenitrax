﻿
"use strict";

angular.module('catalogApp').controller('catalogController', [
    '$scope', '$location', '$filter', '$modal', '$log', '$timeout', 'catalogService', function ($scope, $location, $filter, $modal, $log, $timeout, catalogService) {


        //$scope.call = function(a) {
        //    console.log(a);
        //    if (a.SpeciesCode == 'IRE')
        //        return true;
        //    else
        //        return false;
        //}


        $scope.catalog = {};
        $scope.processUrl = function(callbacks) {
            //console.log('processUrl', callbacks);
            locationGet();
            var path = $scope.location.path;
            var split = path.split('/');
            //console.log('split', split);
            //clear catalog object and set mode
            $scope.catalog.mode = split[1] || null;
            if ($scope.catalog.mode == 'catalog') {
                var categoryCodePreviously = $scope.catalog.categoryCode;
                var productFormCodePreviously = $scope.catalog.productFormCode;
                $scope.catalog.categoryCode = split[2] || '';
                $scope.catalog.productFormCode = split[3] || '';
                if ($scope.catalog.categoryCode == '' || $scope.catalog.productFormCode == '') {
                    $scope.catalog.categoryCode = null;
                    $scope.catalog.productFormCode = null;
                    $scope.catalog.supplierCodes = null;
                    $scope.catalog.specieCodes = null;
                    $scope.catalog.productCode = null;
                    $scope.catalog.shipWeekString = null;
                } else {
                    $scope.catalog.supplierCodes = angular.lowercase(split[4] || '');
                    $scope.catalog.specieCodes = split[5] || '';
                    $scope.catalog.productCode = split[6] || '';
                    $scope.catalog.shipWeekString = split[7] || '';
                };
                if (categoryCodePreviously !== $scope.catalog.categoryCode || productFormCodePreviously !== $scope.catalog.productFormCode) {
                    $scope.dataGet(callbacks);
                } else {
                    $scope.processCallbacks(callbacks);
                };
            } else if ($scope.catalog.mode == 'product') {
                var productCodePreviously = $scope.catalog.productCode;
                $scope.catalog.productCode = split[2] || 'null';
                if (productCodePreviously !== $scope.catalog.productCode) {
                    $scope.dataGet(callbacks);
                } else {
                    $scope.processCallbacks(callbacks);
                };
            } else {
            };


        };
        var locationGet = function() {
            var path = $location.path().toLowerCase();
            //$location.path(path).replace();
            $location.path(path);
            //capture current URI
            $scope.location = {
                url: $location.absUrl(),
                protocol: $location.protocol(),
                host: $location.host(),
                port: $location.port(),
                path: $location.path(),
                hash: $location.hash(),
                values: $location.path().split('/')
            };
            console.log('$scope.location', $scope.location);
        };
        $scope.dataGet = function(callbacks) {
            //console.log("dataGet");
            $scope.catalog.searching = true;
            $scope.catalog.data = null;
            //$scope.dataSetsClear();
            catalogService.dataGet($scope.catalog.categoryCode, $scope.catalog.productFormCode).then(
                function(data) {
                    $scope.catalog.data = data;
                    console.log('$scope.catalog.data =', data);
                    $scope.processCallbacks(callbacks);
                },
                function(err) {
                    //console.log('err', err);
                    alert('Error retreiving data.\n' + err.data.Message + '\n' + err.data.MessageDetail);
                }
            );
        };
        $scope.selectionsSet = function (callbacks) {
            //uses scope.catalog values to set supplier and species selections
            if ($scope.catalog.data == null || $scope.catalog.data == {}) {
                return;
            };
            var selectedSupplierCodes = $scope.catalog.supplierCodes.split(',');
            console.log('selectionsSet', callbacks);
            var selectedSuppliersIndex = [];
            for (var i = 0; i < $scope.catalog.data.Suppliers.length; i++) {
                $scope.catalog.data.Suppliers[i].Code = angular.lowercase($scope.catalog.data.Suppliers[i].Code).trim();
                if ($scope.catalog.supplierCodes.trim() == '$' || $scope.catalog.supplierCodes.trim() == '') {
                    selectedSuppliersIndex[i] = true;
                } else {
                    selectedSuppliersIndex[i] = false;
                    for (var ii = 0; ii < selectedSupplierCodes.length; ii++) {
                        console.log('|' + $scope.catalog.data.Suppliers[i].Code + '|' + selectedSupplierCodes[ii] + '|', $scope.catalog.data.Suppliers[i].Code == selectedSupplierCodes[ii]);
                        if ($scope.catalog.data.Suppliers[i].Code == selectedSupplierCodes[ii]) {
                            selectedSuppliersIndex[i] = true;
                            break;
                        };
                    };
                };
            };

            console.log('selectedSupplierCodes =', selectedSupplierCodes, 'selectedSuppliersIndex =', selectedSuppliersIndex);

            $scope.catalog.searching = false;
            $scope.processCallbacks(callbacks);
        };
        $scope.selectionClear = function() {
            $scope.catalog.categoryCode = null;
            $scope.catalog.productFormCode = null;
            $scope.catalog.supplierCodes = null;
            $scope.catalog.specieCodes = null;
            $scope.catalog.productCode = null;
            $scope.catalog.shipWeekString = null;
        };


        $scope.slider = {
            value: 0,
            selectedVariety: null,
            init: function() {
                console.log('sider.init');
                $scope.slider.options.to = (($scope.catalog.data.Varieties.length || 0) - 1).toString();
            },
            update: function (value) {
                var currentValue = Number($scope.slider.value);
                //console.log('currentValue=', currentValue, 'value =',value);
                //if (typeof $scope.slider.value !== 'number') { $scope.slider.value = 0; };
                var performUpdate = false;
                if (typeof value === 'undefined' || value == null) {
                    performUpdate = true;
                } else if (typeof value === 'number' && currentValue != value) {
                    currentValue = value;
                    performUpdate = true;
                };
                if (performUpdate) {
                    $scope.slider.value = currentValue.toString();
                    $scope.slider.selectedVariety = $scope.catalog.data.Varieties[currentValue];
                    //console.log($scope.slider.value, value, $scope.slider.selectedVariety);
                };
                
            },
            options: {
                from: 0,
                to: 125,
                step: 1,
                //scale: [0, '|', 10, '|', 20, '|', 30, '|', 40],
                //dimension: " km",
                realtime: true,
                skin: 'round',
                //css: {
                //    background: { "background-color": "silver" },
                //    //before: { "background-color": "purple" },
                //    //default: { "background-color": "white" },
                //    //after: { "background-color": "green" },
                //    pointer: { "background-color": "blue" }

                //},
                watchOptions:true,
                vertical: true,
                callback: function (value, released) {
                    //console.log('value =', value, 'released =', released);
                    $scope.slider.update(null);
                }

            }
        };

        $scope.varietiesInSpecies = function(speciesCode) {
            var varieties = [];

            speciesCode = $filter('lowercase')(speciesCode.trim());

            for (var i = 0; i < $scope.catalog.data.Varieties.length || 0; i++) {
                var variety = $scope.catalog.data.Varieties[i];
                var varietySpeciesCode = $filter('lowercase')(variety.SpeciesCode.trim());
                if (speciesCode == varietySpeciesCode) {
                    varieties.push(variety);
                };
            };
            return varieties;
        };


        $scope.checkBoxes = [
            { text: "Developing Page", value: 1, selected: true, onClick: "" },
            { text: "Functional Test Buttons", value: 2, selected: true, onClick: "" },
            { text: "State Display", value: 3, selected: true, onClick: "" },
            { text: "Data Splats", value: 4, selected: false, onClick: "" },
            { text: "New Selections Data", value: 5, selected: false, onClick: "" }
        ];


        $scope.setPath = function(mode, path) {
            $location.path(mode + path);
        };

        var varietyDetailGet = function() {
            $scope.product = null;
            var productCode = ($scope.catalog.productCode || '').trim();
            if (productCode != '') {
                catalogService.varietyDetailGet(productCode).then(
                    function(data) {
                        //console.log('varietyDetailGet', data);
                        $scope.product = data;
                    },
                    function(err) {
                        //console.log('err', err);
                        alert('Error retreiving product.\n' + err.data.Message + '\n' + err.data.MessageDetail);
                    });
            };
        };
        $scope.varietyDetailPopup = {
            left: 100,
            top: 200,
        };
        $scope.varietyDetail = {
            data: {}
        };
        $scope.varietyDetailDisplay = function(varietyCode) {

            catalogService.varietyDetailGet(varietyCode).then(
                function(response) {
                    //console.log('varietyDetailGet =>', response);
                    $scope.varietyDetail.data = response;

                    var modalInstance = $modal.open({
                        animation: true,
                        templateUrl: '/app/html/epsVarietyDetailPopup.html',
                        controller: 'VarietyDetailController',
                        //size: 'sm',
                        resolve: {
                            varietyDetailData: function() {
                                return $scope.varietyDetail.data;
                            }
                        }
                    });
                    modalInstance.result.then(function(selectedItem) {
                        //$scope.selected = selectedItem;
                    }, function() {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                },
                function(err) {
                    //console.log('err', err);
                    alert('Error retreiving variety detail.\n' + err.data.Message + '\n' + err.data.MessageDetail);
                });


        };





//////////////////////////////////////////  HELPERS  //////////////////////////////////////////
        $scope.processCallbacks = function(callbacks) {
            if (callbacks !== 'undefined' && callbacks != null && callbacks.length > 0 && typeof callbacks[0] === "function") {
                var callback = callbacks.shift();
                //console.log(callback);
                callback(callbacks);
            };
        };
        var arrayOfStringsToDelimitedString = function(arr, delimiterStr, trimEach, noEmpty, toLower) {
            if (!angular.isArray(arr)
                || !angular.isString(delimiterStr)
                || delimiterStr.length != 1) {
                return '';
            };
            var str = '';
            for (var i = 0; i < arr.length; i++) {
                var s = arr[i];
                if (trimEach) {
                    s = s.trim();
                };
                if (noEmpty) {
                    if (s.trim() != '') {
                        str += (delimiterStr + s);
                    }
                } else {
                    str += (delimiterStr + s);
                };
            };
            if (str.length > 0) {
                str += ",";
            };
            if (toLower) {
                str = angular.lowercase(str);
            };
            return str;

        };
        var delimitedStringToArray = function(str, delimiterStr, trimEach, noEmpty) {
            if (!angular.isString(str)
                || !angular.isString(delimiterStr
                    || str.length == 0
                    || delimiterStr.length != 1)) {
                return [];
            };
            var splits = str.split(delimiterStr);
            //console.log(splits);
            if (trimEach) {
                var arr1 = [];
                for (var i = 0; i < splits.length; i++) {
                    arr1.push(splits[i].trim());
                };
                splits = arr1;
            };
            if (noEmpty) {
                var arr2 = [];
                for (i = 0; i < splits.length; i++) {
                    if (splits[i].trim() > 0) {
                        arr2.push(splits[i]);
                    };
                };
                splits = arr2;
            };
            return splits;
        };

        $scope.value = "0";

        


        ////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.processUrl([$scope.selectionsSet, $scope.slider.init, varietyDetailGet]);


        //$scope.processUrl([function() {
        //    switch ($scope.catalog.mode) {
        //    case 'catalog':
        //        $scope.dataGet([$scope.selectionsSet]);
        //        break;
        //    case 'product':
        //        varietyDetailGet();
        //        break;
        //    default:
        //    }
        //}]);


    }
]);
