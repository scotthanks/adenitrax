﻿
///////////////////////////////// Bread Crumbs /////////////////////////////////////
var breadCrumbModel = new Object;
//breadCrumbModel.IsInitialized = false;
breadCrumbModel.IsDisplayed = false;
breadCrumbModel.Config = {
    DisplayElement: $("#bread_crumbs"),
    ItemSeparator: "<span>&nbsp;&gt;&nbsp;</span>",
    SeparatorCSS: "transparent url('/Images/icons/bread_crumb_separator_2_16.png') no-repeat 0px -2px",
    ItemClassName: "main_nav_items"
};

breadCrumbModel.data = [];  //Array of objects
breadCrumbModel.ClearDisplay = function () {
    $(breadCrumbModel.Config.DisplayElement).empty();
    breadCrumbModel.IsDisplayed = false;
};

breadCrumbModel.Display = function (data) {
    if (data == null) { return false; };
    if (data.length == 0) { return false; };
    var list = $("<ul style=\"display:inline; list-style: none; list-style-type: none; margin:0; padding:0; \"></ul>");
    $.each(data, function (ndx, item) {
        var listItem = $("<li style=\"display:inline; margin:0; padding:0 5px 0 20px; background:transparent; \"></li>");
        if (ndx > 0) { $(listItem).css("background", breadCrumbModel.Config.SeparatorCSS); }

        if (item.Type.toLowerCase() == "anchor" && item.State.toLowerCase() == "active") {
            var anchor = $("<a></a>");
            //alert('bingo');
            $(anchor).text(item.Text);
            var properties = new Object;
            if (item.id != null && item.id != "") { properties.id = item.id; } // Optionally include the id

            anchor.prop("class", breadCrumbModel.Config.ItemClassName);

            //properties.class = breadCrumbModel.Config.ItemClassName;  //Include the class name
            properties.title = item.Title;  //Include Title Text
            properties.href = item.Href;    //Include href value
            $(anchor).prop(properties);


            $(listItem).append(anchor);
        };

        if (item.Type.toLowerCase() == "span" || item.State.toLowerCase() == "inactive") {
            var span = $("<span></span>");
            //alert('bingo');
            $(span).text(item.Text);
            var properties = { };
            //var properties = { 'class': className, 'id': id, 'title': title };
            if (item.id != null && item.id != "") { properties.id = item.id; }// Optionally include the id

            span.prop("class", breadCrumbModel.Config.ItemClassName);
            //properties.class = breadCrumbModel.Config.ItemClassName;  //Include the class name

            properties.title = item.Title;  //Include Title Text
            $(span).prop(properties);
            $(listItem).append(span);

        };
        $(list).append(listItem);
    });
    $(breadCrumbModel.Config.DisplayElement).append(list);
    breadCrumbModel.IsDisplayed = true;
   
};

//////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////  Selection Menu  /////////////////////////////////////////////
var PageState2 = new Object;
PageState2.State = {
    NavigationLink: "",
    NavigationTarget: "",
};

PageState2.NavigationLinkAvailaibility = $("#main_navigation_link_availability");
PageState2.NavigationLinkCatalog = $("#main_navigation_link_catalogue");

PageState2.CategorySelectionDisplay = $("#selection_menu_category_container");
PageState2.CategorySelectionListClass = "select_category_list";
PageState2.CategorySelectionItemsClass = "category_selection_links";
PageState2.CategorySelectionItemsSelectedClass = "select_category_list_selected"

PageState2.FormSelectionDisplay = $("#selection_menu_form_container");
PageState2.FormSelectionListClass = "select_form_list";
PageState2.FormSelectionItemsClass = "form_selection_links";
PageState2.FormSelectionItemsUnavailableClass = "form_selection_links_unavailable";

PageState2.LocateCategorySelectionDisplay = function () {
    //var el = PageState2.NavigationLinkAvailaibility;
    //var el = PageState2.NavigationLinkCatalog;

    var el = PageState2.State.NavigationLink;
    var offset = el.offset();
  //  PageState2.CategorySelectionDisplay.css("left", offset.left - 45).css("top", offset.top + el.height() - 1);
    PageState2.CategorySelectionDisplay.css("left", offset.left).css("top", offset.top + el.height() + 30);
};
PageState2.LocateFormSelectionDisplay = function (selectedCategoryId) {
    var el = $("#" + selectedCategoryId);
    var offset = el.offset();
    // PageState2.FormSelectionDisplay.css("top", offset.top - 25).css("left", offset.left + el.width()+12);
    PageState2.FormSelectionDisplay.css("top", offset.top).css("left", offset.left + el.width() + 20);
};
PageState2.Init = function (navigationLink, navigationTarget) {
    PageState2.State.NavigationLink = navigationLink;
    PageState2.State.NavigationTarget = navigationTarget;
    PageState2.LocateCategorySelectionDisplay();
    $("." + PageState2.CategorySelectionItemsClass).hoverIntent(
        function () {
            //hovering
            selectedCategoryId = $(this).attr("id");
            PageState2.LocateFormSelectionDisplay(selectedCategoryId);
            $.data(PageState2.FormSelectionDisplay, 'SelectedCategory', selectedCategoryId);

            var activeFormCodesString = "";
            $.each(
                selectionMenuModel.data.CategoriesMenu, function (index, item) {
                    if (item.Code == selectedCategoryId) {
                        activeFormCodesString = item.ActiveFormCodesString;
                        return;
                };
            });

            $("." + PageState2.FormSelectionItemsClass).addClass(PageState2.FormSelectionItemsUnavailableClass);
            $(activeFormCodesString).removeClass(PageState2.FormSelectionItemsUnavailableClass);

            PageState2.FormSelectionDisplay.show();
            $("." + PageState2.CategorySelectionItemsClass).removeClass(PageState2.CategorySelectionItemsSelectedClass);


            //$("#selected_category_test").html("Test Info: " + $.data(PageState2.FormSelectionDisplay, 'SelectedCategory'));

        },
        function () {
            //leaving
            //PageState2.FormSelectionDisplay.hide();
        }
    );

    $("." + PageState2.FormSelectionItemsClass).on("click", function () {


        //if (!eps_EarlyIeBrowser) {
        //    //alert(eps_EarlyIeBrowser);
        //    alert('It appears you are using an older version of Internet Explorer.\n\nIf so, some features of this page will not work or might perform very slowly.\n\nIf you have Google Chrome or Mozilla Firefox, , we recommend using one of these free browsers or updating to Internet Explorer 10 for the best site experience.');
        //    //eps_tools.OkDialog({ 'title': '<span>Alert!</span>', 'content': '<p>It appears you may be using an older version of Internet Explorer.</p><p>If so, some features of this page will not work or might perform very slowly.</p><p>If you have Mozilla Firefox or Google Chrome installed, you may prefer using one of them instead.<p>Your experience will be much better with one of these more modern Internet browsers.</p>', 'modal': true });
        //    window.location.href = "/";
        //} else {





            var url = PageState2.State.NavigationTarget;

            url += "?Category=";


            url += $.data(PageState2.FormSelectionDisplay, 'SelectedCategory', selectedCategoryId);
            url += "&Form=";
            url += $(this).attr("id");
            //alert(url);
            window.location.href = url;
        //};



        PageState2.FormSelectionDisplay.hide();
        PageState2.CategorySelectionDisplay.hide()
    });

    //alert("Init");
    
};

PageState2.SetCategoryFocus = function (id) {
    $("." + PageState2.CategorySelectionItemsClass).css({ "border-bottom": "2px solid #fff", "background": "url('')" });
    $("#" + id).css({ "border-bottom": "2px solid #287d87", "color": "#737474", "background": "url('/Images/layout/menu_subchild_pointer.png') right 1px no-repeat" });
    //$("#" + id).addClass(PageState2.CategorySelectionItemsSelectedClass);


   //$("#" + id).removeClass(PageState2.CategorySelectionItemsClass).
};

var selectionMenuModel = new Object;
selectionMenuModel.Config = {
    IsComposed: false,
    urlSelectionMenuApi: "/api/SelectionMenu",
    urlAvailabilitySearch: "/Availability/Search",
    urlCatalogSearch: "/Catalog/Search",

};
selectionMenuModel.data = [];
selectionMenuModel.Init = function () {
   // alert("start");
    selectionMenuModel.LoadThenCompose();
    PageState2.NavigationLinkAvailaibility.hover(
        function () {
            PageState2.Init(PageState2.NavigationLinkAvailaibility, selectionMenuModel.Config.urlAvailabilitySearch);
            PageState2.CategorySelectionDisplay.css("display", "block");
            //PageState2.CategorySelectionDisplay.fadeIn(300);
        },
        function () {
            
            PageState2.CategorySelectionDisplay.css("display", "none");
            //PageState2.CategorySelectionDisplay.fadeOut(100);
        }
    );
    PageState2.NavigationLinkCatalog.hover(
        function () {
            PageState2.Init(PageState2.NavigationLinkCatalog, selectionMenuModel.Config.urlCatalogSearch);
            PageState2.CategorySelectionDisplay.css("display", "block");
            //PageState2.CategorySelectionDisplay.fadeIn(300);
        },
        function () {
            PageState2.CategorySelectionDisplay.css("display", "none");
            //PageState2.CategorySelectionDisplay.fadeOut(100);
        }
    );
    PageState2.CategorySelectionDisplay.hover(
        function () {
            PageState2.CategorySelectionDisplay.css("display", "block");
            //$(this).doTimeout('hover', 250, 'addClass', 'hover');

        },
        function () {
            PageState2.CategorySelectionDisplay.css("display", "none");
            //$(this).doTimeout('hover', 250, 'removeClass', 'hover');
            PageState2.FormSelectionDisplay.hide();

        }
    );
    PageState2.FormSelectionDisplay.hover(
        function () {
            PageState2.FormSelectionDisplay.css("display", "block");
            PageState2.CategorySelectionDisplay.css("display", "block");
            $("#" + $.data(this, 'SelectedCategory', selectedCategoryId)).removeClass("select_category_list").addClass("select_category_list_selected");
            //PageState2.SetCategoryFocus();

        },
        function () {
            PageState2.FormSelectionDisplay.css("display", "none");
            PageState2.CategorySelectionDisplay.css("display", "none");
        }
    );

};

selectionMenuModel.LoadThenCompose = function () {
    $.ajax({
        datatype: "json",
        url: selectionMenuModel.Config.urlSelectionMenuApi,
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            selectionMenuModel.data = [];
        },
        success: function (response, textStatus, jqXHR) {
            selectionMenuModel.data = response;
            //alert(JSON.stringify(selectionMenuModel.data));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            selectionMenuModel.ComposeMenus();
            //viewModel.ProcessEventChain(callbacks);
        }
    });
};
selectionMenuModel.ComposeMenus = function () {
    if (!selectionMenuModel.Config.IsComposed) {

        //Compose Category Selection Menu
        var container = $("<div></div>");
       // var header = $("<h2><span>Category</span></h2>");
       // container.append(header);
        var list = $("<ul></ul>");
        list.prop("id", PageState2.CategorySelectionListClass);

        for (var i = 0; i < selectionMenuModel.data.CategoriesMenu.length ; i++) {
            //alert(selectionMenuModel.data[i].ProgramType.Name);
            var item = $("<li></li>");
          item.addClass(PageState2.CategorySelectionItemsClass);
            //    item.addClass("selected_menu_item");
            
            item.text(selectionMenuModel.data.CategoriesMenu[i].Name);
            item.attr("id", selectionMenuModel.data.CategoriesMenu[i].Code);
            list.append(item);

        };
        container.append(list);
       
        PageState2.CategorySelectionDisplay.hide().empty().append(container);
        selectionMenuModel.Config.IsComposed = true;

        //Compose Form Selection Menu
        container = $("<div></div>");
       // var header = $("<h3><span>Plant Form</span></h3>");
      //  container.append(header);
        var list = $("<ul></ul>");
        list.prop("id", PageState2.FormSelectionListClass);
                
        for (var i = 0; i < selectionMenuModel.data.FormsMenu.length; i++) {
            //alert(selectionMenuModel.data[i].ProgramType.Name);
            var item = $("<li></li>");
            item.addClass(PageState2.FormSelectionItemsClass);
            item.text(selectionMenuModel.data.FormsMenu[i].Name);
            item.attr("id", selectionMenuModel.data.FormsMenu[i].Code);

            list.append(item);
        };
        container.append(list);
        PageState2.FormSelectionDisplay.hide().empty().append(container);

        for (var i = 0; i < selectionMenuModel.data.CategoriesMenu.length; i++) {
            //add array of availableFormSelections
            var s = "";
            for (ii = 0; ii < selectionMenuModel.data.CategoriesMenu[i].ActiveFormCodes.length; ii++) {
                if (ii != 0) { s += ","; };
                s += "#" + selectionMenuModel.data.CategoriesMenu[i].ActiveFormCodes[ii];
            };
            selectionMenuModel.data.CategoriesMenu[i].ActiveFormCodesString = s;
        };


        //PageState2.FormSelectionDisplay.append($("<div id=\"selected_category_test\"></div>"));

    };
};
selectionMenuModel.Display = function () {
    PageState2.CategorySelectionDisplay.show();
}
selectionMenuModel.Remove = function () {
    PageState2.CategorySelectionDisplay.hide();

};
//alert("bingo");


var loginModel = {};
loginModel.forgottenPassword = function () {
    //close login popup
    logInDialog.dialog("close");
    //get request form
    $.get('/account/ResetPasswordRequestForm', function(form) {
         //show request form
        eps_tools.FormSubmitDialog({
            "title": "Forgotten Password:",
            "content": form,
            "modal": true,
        });
        //popUpLogIn.close();
        $("#reset_password_request_form #email_address_input").focus();
    });

};



