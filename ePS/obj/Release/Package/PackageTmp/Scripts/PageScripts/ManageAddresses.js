﻿var model = {};

model.stateData = [];
model.provinceData = [];
model.ShipToAddresses = [];
model.config = {
    urlShipToAddressApi: '/api/V3/Grower/ShipToAddress',
    urlShipToAddressUpdateDefaultApi: '/api/V3/Grower/ShipToAddressDefault',
    urlShipToAddressDeleteApi: '/api/shiptoaddress',
    urlShipToAddressOrigApi: '/api/shiptoaddress',
};
model.state = {
    selectedAddressGuid: null,

};

//Generic Ajax Call - overides global error handler
model.ajaxCall = function (element, uri, httpVerb, data, onSuccess, callbacks) {
    //alert("ajaxCall");
    $.ajax({
        url: uri,
        dataType: "json",
        data: data,
        type: httpVerb,
    })
    .done(function (data, textStatus, jqXHR) {
        alert("hi");
        alert(traverseObj(data, false));
         alert(textStatus);
        if (data.Success == true)
        { onSuccess(data); }
        else
        {
            viewModel.floatMessage(element, "Error: " + data.Message, 2500);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        if (typeof element !== "undefined" & element != null) {
            viewModel.floatMessage(element, errorMsg, 4000);
        };
    })
    .always(function (jqXHR, textStatus) {
        if (textStatus == "success") {
            viewModel.ProcessEventChain(callbacks);
        } else {
        };
    });
};

model.getStateProvinceData = function (callback) {
    var url = '/Scripts/Json/StateData.js';
    var jqxhr = $.getJSON(url, function (data) {
        model.stateData = data;
        //  alert(traverseObj(model.stateData, false));
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
    })
    .done(
    function () {
        var url = '/Scripts/Json/ProvinceData.js';
        var jqxhr = $.getJSON(url, function (data) {
            model.provinceData = data;
            // alert(traverseObj(model.provinceData, false));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown);
        })
        .done(callback);

    });

};

model.UpdateShipToAddressData = function ( callbacks) {

    var url = model.config.urlShipToAddressApi;
    $.ajax({
        datatype: "json",
        url: url,
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
        },
        success: function (response, textStatus, jqXHR) {
            //write new data into Detail Data
            model.ShipToAddresses = response.ShipToAddressList;
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.ProcessEventChain(callbacks);
        }
    });

};

model.WriteShipToAddressNew = function (data, element, callbacks) {

    //element (optional) is message element to receive messages.
   
    var resultString = "Write New ShipTo Address:\n";
    $.ajax({
        type: "POST",
        url: model.config.urlShipToAddressOrigApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            resultString += "url: " + this.url + "\n";
            resultString += "data: " + JSON.stringify(this.data) + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    //update element
                    element.text("Data updated.");
                    //close dialog
                    eps_tools.Dialog.close();
                    //refresh ship-to addresses - selecting new one
                    viewModel.RefreshShipToDisplay()
                } else {
                    element.text(response.Message);
                }
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.WriteShipToAddressUpdate = function (data, element) {

    //element (optional) is message element to receive messages.
    //alert(element.length);
    var resultString = "Write New ShipTo Address:\n";
    $.ajax({
        type: "PUT",
        url: model.config.urlShipToAddressOrigApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            resultString += "url: " + this.url + "\n";
            resultString += "data: " + JSON.stringify(this.data) + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    //update element
                    element.text("Data updated.");
                    //close dialog
                    eps_tools.Dialog.close();
                    //refresh ship-to addresses - selecting new one
                    viewModel.RefreshShipToDisplay();
                } else {
                    element.text(response.Message);
                }
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.WriteShipToAddressUpdateDefault = function (data, element) {

    //element (optional) is message element to receive messages.
    //  alert("writing: " + model.config.urlShipToAddressUpdateDefaultApi);
    var resultString = "Write New ShipTo Address:\n";
    $.ajax({
        type: "PUT",
        url: model.config.urlShipToAddressUpdateDefaultApi,
        data: data,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            resultString += "url: " + this.url + "\n";
            resultString += "data: " + JSON.stringify(this.data) + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    viewModel.floatMessage(element, "Default Updated", 2500);
                } else {
                    element.text(response.Message);
                }
            };
        },
        complete: function (jqXHR, textStatus) {
            //alert(resultString);
            viewModel.ProcessEventChain(callbacks);
        }
    });
};
model.WriteShipToAddressDelete = function (addressGuid, element, callbacks) {
    //element (optional) is message element to receive messages.
    var resultString = "Write New ShipTo Address:\n";
    $.ajax({
        type: "DELETE",
        url: model.config.urlShipToAddressDeleteApi,
        datatype: "json",
        beforeSend: function (jqXHR, settings) {
            //alert("AJAX url: " + this.url);
            this.url += "/" + addressGuid;
            resultString += "url: " + this.url + "\n";
        },
        success: function (response, textStatus, jqXHR) {
            if (typeof response !== 'undefined' && response != null) {
                //alert(JSON.stringify(response));
                resultString += "RESPONSE: " + JSON.stringify(response) + "\n";
                if (!response.IsAuthenticated) {
                    element.text("You must be logged-in to use this page.");
                } else if (response.Success) {
                    //update element
                    element.text("Data updated.");
                    //close dialog
                    eps_tools.Dialog.close();
                    //refresh ship-to addresses - selecting new one
                    viewModel.RefreshShipToDisplay()
                } else {
                    element.text(response.Message);
                }
            };
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
        }
    });

};

var viewModel = {};
viewModel.config = {
   
    actionButtonEnabledClass: "action_buttons",
    actionButtonDisabledClass: "action_buttons_disabled", 

    Forms: {
        ClassName: "address_page_forms",
        //NewAddressDisplayId: "new_address_form_display",
        AddressFormTemplateId: "address_form_template",
        AddressFormId: "new_address_form",
        AddressFormMessageClass: "ship_to_message",
        AddressFormInputBoxesClass: "inputTextBox",

        AddressSaveButtonId: "ship_to_button_save",
        AddressDeleteButtonId: "ship_to_button_delete",
        AddressCancelButtonId: "ship_to_button_cancel",
        AddressFormTemplate: null,
        AddressForm: null,

        ShipToGrowerShipToAddressGuidId: "hidden_ship_to_address_guid",
        ShipToGrowerGuidId: "hidden_grower_guid",
        ShipToNameId: "ship_to_name",
        ShipToStreetAddress1: "ship_to_street_address_1",
        ShipToStreetAddress2: "ship_to_street_address_2",
        ShipToCity: "ship_to_city",
        ShipToZipCode: "ship_to_zip_code",
        ShipToPhoneNumber: "ship_to_phone_number",
        ShipToCountryCodeName: "ship_to_country",
        ShipToStateCode: "ship_to_state_code",
        ShipToSpecialInstructions: "ship_to_special_instructions",

    },
    DetailDisplay: {

        headerBarClass: "header_bars",
        TextInputDisplaysClass: "text_input_displays",

        ShipToTableId: "ship_to_table",
        ShipToEditLinkClass: "ship_to_edit_links",
        ShipToRadioButtonsName: "ship_to_radio_buttons_name",
        ShipToRadioButtonsClass: "ship_to_radio_buttons",

        ButtonDisplayId: "button_display",
      
        ButtonAddAddressId: "add_new_address_button",
        ButtonAddCardId: "add_new_card_button",
       

    },
};
viewModel.state = {
    pageMode: "all",
    ShipToAddressGuid: function () { return $('input:radio[name=' + viewModel.config.DetailDisplay.ShipToRadioButtonsName + ']:checked').val(); },
};
viewModel.init = function () {
   // alert("bingo");
    $("#" + viewModel.config.DetailDisplay.ShipToTableId).css({ "display": "none" });
    //State and Province Data
    model.getStateProvinceData();
   
    viewModel.RefreshShipToDisplay()

    ///////////////// Store and Delete Form Templates //////////////////
    viewModel.config.Forms.AddressFormTemplate = $("#" + viewModel.config.Forms.AddressFormTemplateId).clone(true); //store the Address Form HTML Template
    $("#" + viewModel.config.Forms.AddressFormTemplateId).remove();


    /////////////////// Test Buttons //////////////////////////

    //////////////// Action Buttons ///////////////////////
    $("#" + viewModel.config.DetailDisplay.ButtonAddAddressId).on("click", function () {
        viewModel.DisplayAddShipToAddressForm();
    });

    //stop return key from closing form when pushed in textarea
    $("#ship_to_special_instructions").off().on('keydown', function (evt) {
        evt.stopPropagation();
        //return false;         //false would stop browser from processing key
        return true;
    });

};

viewModel.DisplayAddShipToAddressForm = function () {
    //clone address form template and set id
    viewModel.config.Forms.AddressForm = viewModel.config.Forms.AddressFormTemplate.clone();                    //Copy the template and 
    viewModel.config.Forms.AddressForm.prop({
        "id": viewModel.config.Forms.AddressFormId,                                                             //change id to working id
    });
    
    //create and show address form
    eps_tools.FormSubmitDialog({
        "title": "Ship-To Address:",
        "content": viewModel.config.Forms.AddressForm,
        "modal": true,
    });

    //remove delete button
    $("#" + viewModel.config.Forms.AddressDeleteButtonId).remove();

    //focus on first field
    $("#" + viewModel.config.Forms.AddressFormId + " input:first").select();

    //set country change event
    $("#" + viewModel.config.Forms.AddressFormId + " input[type='radio'][name='" + viewModel.config.Forms.ShipToCountryCodeName + "']").off().on("change", function () {
        var country = $("#" + viewModel.config.Forms.AddressFormId + " input[type='radio'][name='ship_to_country']:checked").val() || "";
        //alert(country);

        var select = $("#ship_to_state_code");
        select.empty();
        var arr = [];
        var lbl = $("#" + viewModel.config.Forms.AddressFormId + ".state_province_selection_label");
        if (country == "ca") {
            arr = model.provinceData;
            lbl.text("Province");
        } else {
            arr = model.stateData;
            lbl.text("State");
        };
        $.each(arr, function (i, item) {
            var option = $("<option></option>");
            option.text(item.Abbr);
            option.prop("value", item.Abbr);
            select.append(option);
        });
    }).change();

    //get reference to message box
    var messageBox = $("#" + viewModel.config.Forms.AddressFormId + " ." + viewModel.config.Forms.AddressFormMessageClass);



    // Set send button event
    var sendButton = $("#" + viewModel.config.Forms.AddressSaveButtonId);
    sendButton.off().on("click", function () {
        viewModel.CreateAddress(messageBox);
    });


    // Set Cancel button event
    $("#" + viewModel.config.Forms.AddressCancelButtonId).off().on("click", function () {
        eps_tools.Dialog.close();
    });

    //Stop return key from closing form when pushed in text box
    var inputBoxes = $("#" + viewModel.config.Forms.AddressFormId + " ." + viewModel.config.Forms.AddressFormInputBoxesClass);

    //clear and reset input box keydown event
    inputBoxes.off().on('keydown', function (evt) {
        evt.stopPropagation();
        messageBox.text('');
        return true;
    });

    //stop return key from closing form when pushed in textarea
    $("#ship_to_special_instructions").off().on('keydown', function (evt) {
        evt.stopPropagation();
        //return false;         //false would stop browser from processing key
        return true;
    });

};
viewModel.UpdateAddressDefault = function (addressGuid, element) {
    var data = {};
    data.AddressGuid = addressGuid;
   // alert(data.AddressGuid);
    model.WriteShipToAddressUpdateDefault(data, element);
};
viewModel.UpdateAddress = function (addressGuid, element) {
    var data = viewModel.ComposeShipToAddressDataFromForm(addressGuid);
    
    model.WriteShipToAddressUpdate(data, element);
};
viewModel.CreateAddress = function (element) {
    var data = viewModel.ComposeShipToAddressDataFromForm();
    model.WriteShipToAddressNew(data, element);
};


viewModel.ComposeShipToAddressDataFromForm = function (addressGuid) {
    var data = {};

    if (typeof addressGuid !== 'undefined' && addressGuid != null && addressGuid.length == 36) {
        data.GrowerShipToAddressGuid = addressGuid;
    };
    data.Name = $("#ship_to_name").val();
    data.StreetAddress1 = $("#ship_to_street_address_1").val();
    data.StreetAddress2 = $("#ship_to_street_address_2").val();
    data.City = $("#ship_to_city").val();
    data.StateCode = $("#ship_to_state_code").val();
    data.Country = $("#" + viewModel.config.Forms.AddressFormId + " input[type='radio'][name='ship_to_country']:checked").val() || "";
    data.ZipCode = $("#ship_to_zip_code").val();
    data.PhoneNumber = $("#ship_to_phone_number").val();
    data.SpecialInstructions = $("#ship_to_special_instructions").val();
   
    return data;
};
viewModel.DeleteAddress = function (addressGuid, element) {
    model.WriteShipToAddressDelete(addressGuid, element);
};




viewModel.DisplayEditShipToAddressForm = function (addressGuid) {
    
    //get current values
    var addressData = $.grep(model.ShipToAddresses, function (n, i) {
        return n.Guid == addressGuid;
    });
    if (typeof addressData === 'undefined' || addressData == null || addressData <= 0) { return false; };
    addressData = addressData[0];

    
    //clone address form template and set id
    viewModel.config.Forms.AddressForm = viewModel.config.Forms.AddressFormTemplate.clone();                    //Copy the template and 
    
    viewModel.config.Forms.AddressForm.prop({
        "id": viewModel.config.Forms.AddressFormId,                                                             //change id to working id
    });
   
    //create and show address form
    eps_tools.FormSubmitDialog({
        "title": "Ship-To Address:",
        "content": viewModel.config.Forms.AddressForm,
        "modal": true,
    });

    
    //set country change event and fire it
    $("#" + viewModel.config.Forms.AddressFormId + " input[type='radio'][name='" + viewModel.config.Forms.ShipToCountryCodeName + "']").off().on("change", function () {
        var country = $("#" + viewModel.config.Forms.AddressFormId + " input[type='radio'][name='ship_to_country']:checked").val() || "";
        
        var select = $("#ship_to_state_code");
        select.empty();
        var arr = [];
        var lbl = $("#" + viewModel.config.Forms.AddressFormId + ".state_province_selection_label");
        if (country == "ca") {
            arr = model.provinceData;
            lbl.text("Province");
        } else {
            arr = model.stateData;
            lbl.text("State");
        };
        $.each(arr, function (i, item) {
            var option = $("<option></option>");
            option.text(item.Abbr);
            option.prop("value", item.Abbr);
            select.append(option);
        });
    }).change();

    //load currentData
    var countryCode = "usa";
    if (addressData.Country == 'Canada') { countryCode = "ca"; };
    $("#" + viewModel.config.Forms.AddressFormId + " input[type='radio'][name='" + viewModel.config.Forms.ShipToCountryCodeName + "']").value = countryCode;
    $("#" + viewModel.config.Forms.ShipToStateCode).val(addressData.StateCode || '');
    $.each($("#" + viewModel.config.Forms.AddressFormId + " .inputTextBox"), function (ndx, textBox) {
        if (textBox.id == viewModel.config.Forms.ShipToNameId) {
            textBox.value = addressData.Name || '';
        } else if (textBox.id == viewModel.config.Forms.ShipToStreetAddress1) {
            textBox.value = addressData.StreetAddress1 || '';
        } else if (textBox.id == viewModel.config.Forms.ShipToStreetAddress2) {
            textBox.value = addressData.StreetAddress2 || '';
        } else if (textBox.id == viewModel.config.Forms.ShipToCity) {
            textBox.value = addressData.City || '';
        } else if (textBox.id == viewModel.config.Forms.ShipToZipCode) {
            textBox.value = addressData.ZipCode || '';
        } else if (textBox.id == viewModel.config.Forms.ShipToPhoneNumber) {
            textBox.value = addressData.PhoneNumber || '';
        } else {
            //alert(textBox.id);
        };
    });
    $("#" + viewModel.config.Forms.ShipToSpecialInstructions).val(addressData.SpecialInstructions || '');
   
    
    //get reference to message box
    var messageBox = $("#" + viewModel.config.Forms.AddressFormId + " ." + viewModel.config.Forms.AddressFormMessageClass);

    //alert(messageBox.length);

    // Set send button event
    var sendButton = $("#" + viewModel.config.Forms.AddressSaveButtonId);
    sendButton.off().on("click", function () {
        viewModel.UpdateAddress(addressGuid, messageBox);
    });
    
    //alert(addressData.IsDefault);

    // Set delete button event
    if (addressData.IsDefault) {
        $("#" + viewModel.config.Forms.AddressDeleteButtonId).off().hide();
    } else {
        $("#" + viewModel.config.Forms.AddressDeleteButtonId).off().on("click", function () {
            viewModel.DeleteAddress(addressGuid, messageBox);
        });
    };
    
    // Set Cancel button event
    $("#" + viewModel.config.Forms.AddressCancelButtonId).off().on("click", function () {
        eps_tools.Dialog.close();
    });
   
    //Stop return key from closing form when pushed in text box
    var inputBoxes = $("#" + viewModel.config.Forms.AddressFormId + " ." + viewModel.config.Forms.AddressFormInputBoxesClass);

    //clear and reset input box keydown event
    inputBoxes.off().on('keydown', function (evt) {
        evt.stopPropagation();
        messageBox.text('');
        return true;
    });

    //stop return key from closing form when pushed in textarea
    $("#ship_to_special_instructions").off().on('keydown', function (evt) {
        evt.stopPropagation();
        //return false;         //false would stop browser from processing key
        return true;
    });

    //focus on first field
    $("#" + viewModel.config.Forms.AddressFormId + " input:first").select();

    
};


viewModel.RefreshShipToDisplay = function () {
   // alert("refresh");
    model.UpdateShipToAddressData([function () {
        //redisplay addresseses
        var tbody = $("#" + viewModel.config.DetailDisplay.ShipToTableId + " tbody");
       // alert("fill");
        viewModel.FillAddressTable(tbody);
        $("#" + viewModel.config.DetailDisplay.ShipToTableId).css({ "display": "block" });
        //Define events
        viewModel.defineShipToAddressRadioEvents();
    }]);
    
};


viewModel.CompileData_ShipToAddress = function () {
    var d = {};
    d.FieldName = "ShipToAddressGuid";
    d.ShipToAddressGuid = viewModel.state.ShipToAddressGuid();
    return d;
};

viewModel.defineShipToAddressRadioEvents = function () {
   
    $("input[name='" + viewModel.config.DetailDisplay.ShipToRadioButtonsName + "']")
    .off()      //first remove all events
    .on("change", function (e) {
        model.state.selectedAddressGuid = $(this).val();
        var guid = model.state.selectedAddressGuid;
       // alert('change' + guid);
        viewModel.UpdateAddressDefault(guid, $(this));
      
    })
    
    .on("blur", function (e) {

    });

}

viewModel.FillAddressTable = function (tbody) {
    tbody.empty();


    //alert(model.DetailData.GrowerOrder.OrderGuid);

    model.state.selectedAddressGuid = null;
    var row = $("<tr />");

    row.append('<th style="width:20%;vertical-align:top">Default:</th>"');
    row.append('<th style="width:60%;">Address:</th>');
    row.append('<th style="width:20%;">Action:</th>');
    tbody.append(row);

    $.each(model.ShipToAddresses, function (i, item) {

       
        if (i == 0 || item.IsDefault) {
                //alert("set selected by default" + item.Guid);
            model.state.selectedAddressGuid = item.Guid;
        };
       


        var row = $("<tr />");

        var cell1 = $("<td />");
        var radio = $("<input type=\"radio\" />");

        var radioProperties = {
            //"id": viewModel.Config.DetailDisplay.RadioSelectionShipToIdStem + i,
            "id": item.Guid,
            "name": viewModel.config.DetailDisplay.ShipToRadioButtonsName,
            "class": viewModel.config.DetailDisplay.ShipToRadioButtonsClass,
            "value": item.Guid,
            //"selected": "selected"
        };

        radio.prop(radioProperties);
        cell1.append(radio);
        cell1.append("&nbsp;" + item.Name);
        row.append(cell1);

        var cell2 = $('<li>' + item.StreetAddress1 + '</li><li>' + item.StreetAddress2 + '</li><li>' + item.City + ", " + item.StateCode + " " + item.ZipCode + '</li><li>' + item.Country + '</li>');

        row.append($("<td />").append($('<ul style="list-style:none;" />').append(cell2)));

        var cell3 = $("<td> <img src=\"/Images/layout/pg_pointer.png\" alt=\"\" /> </td>");

        var editLink = $("<a />");
        editLinkProperties = { "class": viewModel.config.DetailDisplay.ShipToEditLinkClass };
        editLink.prop(editLinkProperties);
        editLink.text("EDIT");
        editLink.on("click", function () {
            //alert("Edit Address Guid: " + item.Guid);
            viewModel.DisplayEditShipToAddressForm(item.Guid);
        });
        cell3.append(editLink);
        row.append(cell3);
        tbody.append(row);

    });

    //alert(model.state.selectedAddressGuid);
    var buttonToBeSelected = $("#" + model.state.selectedAddressGuid)
    buttonToBeSelected.prop("checked", true);


};





/////////////// Utilities //////////////////////////////
viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };

    var top, left;
    if (element == null) {
        //Leave values null
        top = null;
        left = null;
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};
viewModel.ProcessEventChain = function (callbacks) {
    //alert("ProcessEventChain");
    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};
