﻿

var model = {};
model.data = {
    userIsAccountAdministrator: null,
    accountSettings: {
        Guid:null,
        IsAccountAdministrator: false,
        CompanyName: null,
        Country: null,
        StreetAddress: null,
        City: null,
        State: null,
        PostalCode: null,
        CompanyEmail: null,
        CompanyPhone: null,
        CompanyType: null,
        CompanyRole: null,
        SubscribeToNewsLetter: null,
        SubscribeToPromotions: null,
        FirstName: null,
        LastName: null,
        PersonalPhone: null,
        PersonalEmail: null
    },
    emailPreferences: [],
    emailPreference: {
        Guid: '',
        IsAccountAdministrator: true,
        Name: null,
        Email: null,
    },
};

model.config = {
    urlGetUserIsAccountAdministrator: "/api/AccountSettings/GetUserIsAccountAdministrator/",
    urlGetAccountSettings: "/api/accountsettings/GetUserAccountSettings",
    urlGetCurrentAccountSettings: "/api/accountsettings/GetCurrentAccountSettings",

    urlCreateAccountSettings: "/api/accountsettings/Create",
    urlUpdateAccountSettings: "/api/accountsettings/Update/",
    urlDeleteAccountSettings: "/api/accountsettings/Delete/",


    urlGetEmailPreferences: "/api/emailpreferences/GetEmailPreferences",
    urlCreateEmailPreference: "/api/EmailPreferences/Create",
    urlUpdateEmailPreference: "/api/EmailPreferences/Update",
    urlDeleteEmailPreference: "/api/EmailPreferences/Delete/",
};

//model.asyncCall = function (element, url, verb, data, dataType, onSuccess, callbacks) {

//    //console.log(data);

//    var testDisplay = $("#test_area_results_display").empty();
//    var p1 = $("<p />")
//        .text("[" + verb + "] url : " + url + " | dataType: " + dataType);
//    var p2 = $("<p />")
//        .text(JSON.stringify(data));
//    //console.log(traverseObj(data));
//    testDisplay.append(p1, p2, $("<hr />"));
//    $.ajax({
//        url: url,
//        type: verb,
//        data: data,
//        dataType: dataType,
//    })
//    .done(function (response, textStatus, jqXHR) {
//        testDisplay.append(traverseObj(response));
//        onSuccess(response);
//    })
//    .fail(function (jqXHR, textStatus, errorThrown) {
//        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
//        testDisplay.append(errorMsg);
//        if (typeof element !== "undefined" & element != null) {
//            viewModel.floatMessage(element, errorMsg, 4000);
//        };
//    })
//    .always(callbacks);
//};

var viewModel = {};
viewModel.config = {
    pageMode: "all",
    accordions: {
        accountSettingsContainerId: "account_settings_container",
        emailPreferencesContainerId: "email_preferences_container",
        rateOfDisplay: function (accordionId) {
            var et = 500;
            var height = viewModel.config.accordions.display$(accordionId).height();
            et += Math.floor(height / 25 * 50);
            return et;
        },

        returnAccordion: function (headerBar$) {
            return headerBar$.parent("div");
        },
        container$: function (accordionId) { return $("#" + accordionId); },
        headerBar$: function (accordionId) { return $("#" + accordionId + " .accordion_header_bars"); },
        display$: function (accordionId) { return $("#" + accordionId + " .accordion_displays"); },
        displayDisabled$: function (accordionId) { return $("#" + accordionId + " .accordion_disabled_displays"); },
    },
    icons: {
        headerBarWorking: "../../Images/icons/Loading_Availability.gif",
        headerBarExpanded: "/images/layout/MyAccountArrowDown.png",
        headerBarCollapsed: "/images/layout/cart_header_pointer.png",
    },
};
viewModel.state = {
    accordions: {
        isExpanded: function (accordionId) {
            //alert(accordionId);
            var accordion$ = viewModel.config.accordions.container$(accordionId);
            //alert(viewModel.config.accordions.container$(accordionId).prop("class"));
            return (viewModel.config.accordions.container$(accordionId).hasClass("accordions_expanded"));
        },
    },
};
viewModel.init = function () {
    alert("bingo");
    alert(viewModel.config.accordions.accountSettingsContainerId);
    /////////////////////// Initialize Expanses /////////////////////////////
    eps_expanse.init(viewModel.config.accordions.accountSettingsContainerId, viewModel.accountSettingsExpand, viewModel.accountSettingsCollapse, "Edit Settings");
    alert("here");
    if (viewModel.config.pageMode == "all" || viewModel.config.pageMode == "settings") {
        alert("account expand");
        viewModel.accountSettingsExpand(viewModel.config.accordions.accountSettingsContainerId);
    };

    eps_expanse.init(viewModel.config.accordions.emailPreferencesContainerId, viewModel.emailPreferencesExpand, viewModel.emailPreferencesCollapse, "Edit Preferences");
    if (viewModel.config.pageMode == "all" || viewModel.config.pageMode == "email") {
        alert("email expand");
        viewModel.emailPreferencesExpand(viewModel.config.accordions.emailPreferencesContainerId);
    } 


    //////////////////// Test Buttons ///////////////////////////////
    //$("#test_data_show_all").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();
    //    var div = $("<div />")
    //        .html("client-side data object:<hr />" + traverseObj(model.data));
    //    testDisplay.append(div);
    //});

    //$("#test_api_user_is_account_administrator").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();
    //    model.data.userIsAccountAdministrator = null;
    //    var onSuccess = function (response) {
    //        //Authentication and Success
    //        if (response.Success && response.IsAuthenticated) {
    //            model.data.userIsAccountAdministrator = response.IsAccountAdministrator;
    //        };
    //        var div = $("<div />")
    //            .text("User is account administrator: " + model.data.userIsAccountAdministrator);
    //        testDisplay.append(div);
    //    };
    //    model.asyncCall($(this), model.config.urlGetUserIsAccountAdministrator, "get", { "id": '00000000-0000-0000-0000-000000000006' }, "json", onSuccess);
    //});

    
    //$("#test_api_current_account_settings").button().on("click", function () {
    //    var onSuccess = function (response) {
    //        model.data.accountSettings = response.AccountSettings;
    //    };
    //    model.asyncCall($(this), model.config.urlGetCurrentAccountSettings, "get", { }, "json", onSuccess);
    //});


    //$("#test_api_put_account_settings").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();

    //    var url = model.config.urlCreateAccountSettings;
        
    //    var data = {
    //        AccountSettings: model.data.accountSettings
    //    }
    //    data.AccountSettings.companyName = "Lava Rock Quarry";
    //    var onSuccess = function (response) {

    //        if (response.Success && response.IsAuthenticated) {
    //            //ToDo: Float a message confirming creation

    //        };

    //    };
    //    model.asyncCall($(this), url, "put", data, "json", onSuccess);
    //});

    //$("#test_api_post_account_settings").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();

    //    var url = model.config.urlUpdateAccountSettings;

    //    var data = {
    //        AccountSettings: model.data.accountSettings
    //    }
    //    data.AccountSettings.Guid = '00000000-0000-0000-0000-000000000006';

    //    var onSuccess = function (response) {

    //        if (response.Success && response.IsAuthenticated) {
    //            //ToDo: Float a message confirming deletion
    //        };

    //    };
    //    model.asyncCall($(this), url, "post", data, "json", onSuccess);
    //});




    //$("#test_api_delete_account_settings").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();

    //    var url = model.config.urlDeleteAccountSettings + '00000000-0000-0000-0000-000000000006';
    //    var onSuccess = function (response) {

    //        if (response.Success && response.IsAuthenticated) {
    //            //ToDo: Float a message confirming deletion
    //        };

    //    };
    //    model.asyncCall($(this), url, "delete", { }, "json", onSuccess);
    //});


    //$("#test_api_account_settings").button().on("click", function () {
    //    var onSuccess = function (response) {
    //        //ToDo: Authentication
    //        //ToDo: Authorizaion as Account Administrator
    //        model.data.accountSettings = response.AccountSettings;
    //    };
    //    model.asyncCall($(this), model.config.urlGetAccountSettings, "get", { "AccountGuid": '00000000-0000-0000-0000-000000000006' }, "json", onSuccess);
    //});
    ////$("#test_data_account_settings").button().on("click", function () {
    ////    var testDisplay = $("#test_area_results_display").empty();
    ////    var div = $("<div />")
    ////        .html("account settings data:<hr />" + traverseObj(model.data.accountSettings));
    ////    testDisplay.append(div);
    ////});
    //$("#test_api_email_preferences").button().on("click", function () {
    //    var onSuccess = function (data) {
    //        //ToDo: Authentication
    //        //ToDo: Authorizaion as Account Administrator
    //        model.data.emailPreferences = data;
    //    };
    //    model.asyncCall($(this), model.config.urlGetEmailPreferences, "get", {}, "json", onSuccess);
    //});

    ////
    //$("#test_api_put_email_preferences").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();

    //    var url = model.config.urlCreateEmailPreference;

    //    var data = {
    //        EmailPreference: model.data.emailPreference
    //    }
    //    data.EmailPreference.Email = "Fred@LavaRock.com";
    //    var onSuccess = function (response) {

    //        if (response.Success && response.IsAuthenticated) {
    //            //ToDo: Float a message confirming creation

    //        };

    //    };
    //    model.asyncCall($(this), url, "put", data, "json", onSuccess);
    //});

    //$("#test_api_post_email_preferences").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();

    //    var url = model.config.urlUpdateEmailPreference;

    //    var data = {
    //        EmailPreference: model.data.emailPreference
    //    }
    //    data.EmailPreference.Guid = '00000000-0000-0000-0000-000000000006';

    //    var onSuccess = function (response) {

    //        if (response.Success && response.IsAuthenticated) {
    //            //ToDo: Float a message confirming deletion
    //        };

    //    };
    //    model.asyncCall($(this), url, "post", data, "json", onSuccess);
    //});


    //$("#test_api_delete_email_preferences").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();

    //    var url = model.config.urlDeleteEmailPreference + '00000000-0000-0000-0000-000000000006';
    //    var onSuccess = function (response) {

    //        if (response.Success && response.IsAuthenticated) {
    //            //ToDo: Float a message confirming deletion
    //        };

    //    };
    //    model.asyncCall($(this), url, "delete", {}, "json", onSuccess);
    //});



    //$("#test_data_email_preferences").button().on("click", function () {
    //    var testDisplay = $("#test_area_results_display").empty();
    //    var div = $("<div />")
    //        .html("email preferences data:<hr />" + traverseObj(model.data.emailPreferences));
    //    testDisplay.append(div);
    //});
};

////////////////// Account Settings //////////////////////////////////
viewModel.accountSettingsExpand = function (containerId) {
    alert("in expand account");
    eps_expanse.state.setWorking(containerId);
    var display = eps_expanse.state.getDisplayElement(containerId);

    eps_expanse.setStatus(viewModel.config.accordions.accountSettingsContainerId, "Edit Settings");
    eps_expanse.expand(containerId, null);
  
};
viewModel.accountSettingsCollapse = function (containerId) {
    alert("in collaspe account");
    eps_expanse.setStatus(viewModel.config.accordions.accountSettingsContainerId, "Edit Settings");
    eps_expanse.collapse(containerId, null, false);
};

////////////////// email Preferences //////////////////////////////////
viewModel.emailPreferencesExpand = function (containerId) {
    alert("in expand email");
    eps_expanse.state.setWorking(containerId);
    var display = eps_expanse.state.getDisplayElement(containerId);

    alert("in collaspe email");

    eps_expanse.setStatus(viewModel.config.accordions.emailPreferencesContainerId, "Edit Email Preferences");
    eps_expanse.expand(containerId, null);

};
viewModel.accountSeetingsCollapse = function (containerId) {
    alert("in collapse email");
    eps_expanse.setStatus(viewModel.config.emailPreferencesContainerId, "Edit Email Preferences");
    eps_expanse.collapse(containerId, null, false);
};

//////////////// Accordions //////////////////////////////

viewModel.accordionSetHeaderBarEvents = function (headerBar$, onLoad) {
    // alert("hi");
    if (typeof headerBar$ === 'undefined' || headerBar$ == null) { return false; };
    headerBar$
        .off()
        .on("click", function () {

            var accordionId = viewModel.config.accordions.returnAccordion($(this)).prop("id");

            //alert("expanded: " + viewModel.state.accordions.isExpanded(accordionId));
            if (viewModel.state.accordions.isExpanded(accordionId)) {
                //alert(accordionId);
                viewModel.accordionCollapse(accordionId);
            } else {

                //var content = onLoad(accordionId);
                //viewModel.accordionCollapse(accordionId);
                //viewModel.accordionLoad(accordionId, viewModel.loadUserForm(accordionId));

                viewModel.accordionExpand(accordionId, onLoad);
            };
        });
};

viewModel.setHeaderBarIcon = function (accordionId, iconSrc, callbacks) {
    var headerBar$ = viewModel.config.accordions.headerBar$(accordionId)
    headerBar$.css({ "background-image": "url('" + iconSrc + "')" });
    viewModel.processEventChain(callbacks);
};
viewModel.accordionEnable = function (accordionId, callbacks) {
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    accordion$.removeClass().addClass("accordions_collapsed");
};
viewModel.accordionDisable = function (accordionId, message, callbacks) {
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    var display$ = viewModel.config.accordions.displayDisabled$(accordionId);
    display$.html(message);
    accordion$.removeClass().addClass("accordions_disabled");
};
viewModel.accordionExpand = function (accordionId, onLoad, callbacks) {
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarWorking);
    viewModel.accordionEnable(accordionId);
    var display$ = viewModel.config.accordions.display$(accordionId);
    display$.hide();
    onLoad(accordionId);
    display$.show("blind", {}, viewModel.config.accordions.rateOfDisplay(accordionId), function () {
        viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarExpanded);
        accordion$.removeClass().addClass("accordions_expanded");
        viewModel.processEventChain(callbacks);
    });
};
viewModel.accordionCollapse = function (accordionId, callbacks) {
    //alert("collapse");
    var accordion$ = viewModel.config.accordions.container$(accordionId);
    viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarWorking);
    var display$ = viewModel.config.accordions.display$(accordionId);
    display$
        .hide("blind", {}, viewModel.config.accordions.rateOfDisplay(accordionId), function () {
            //alert(accordionId);
            viewModel.setHeaderBarIcon(accordionId, viewModel.config.icons.headerBarCollapsed);
            accordion$.removeClass().addClass("accordions_collapsed");
            display$.empty();
            viewModel.processEventChain(callbacks);
        })


};
viewModel.accordionClear = function (accordionId) {
    viewModel.config.accordions.displayDisabled$(accordionId).empty();
    viewModel.config.accordions.display$(accordionId).empty();
};
viewModel.accordionLoad = function (accordionId, content) {
    viewModel.config.accordions.display$(accordionId)
        .empty()
        .html(content);
};


////////////////////// Utilities /////////////////////////
viewModel.processEventChain = function (callbacks) {
    // alert("in callbacks");
    if (typeof callbacks !== 'undefined' && callbacks != null && callbacks.length > 0) {
        var callback = callbacks.shift();
        //   alert(callback);
        callback(callbacks);
    }
};
viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };

    var top, left;
    if (element == null) {
        //Leave values null
        top = null;
        left = null;
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};

//Generic Ajax Call - overides global error handler
model.ajaxCall = function (element, uri, httpVerb, data, onSuccess, callbacks) {
    //alert("ajaxCall");
    $.ajax({
        url: uri,
        dataType: "json",
        data: data,
        type: httpVerb,
    })
    .done(function (data, textStatus, jqXHR) {
        // alert(traverseObj(data, false));
        // alert(textStatus);
        if (data.Success == true)
        { onSuccess(data); }
        else
        {
            viewModel.floatMessage(element, "Error: " + data.Message, 2500);
            onSuccess(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
        if (typeof element !== "undefined" & element != null) {
            viewModel.floatMessage(element, errorMsg, 4000);
        };
    })
    .always(function (jqXHR, textStatus) {
        if (textStatus == "success") {
            //alert("hi there");
            viewModel.processEventChain(callbacks);
        } else {
        };
    });
};