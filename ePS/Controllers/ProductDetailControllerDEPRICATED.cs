﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessObjectsLibrary;

using ePS.Models;
using UserProfile = ePS.Models.UserProfile;

namespace ePS.Controllers
{
    [Obsolete("Use AvailabilityController instead.")]
    public class ProductDetailControllerDEPRICATED : ApiController
    {
        private Guid UserGuid(string userHandle)
        {
            var userGuid = new Guid();

            using (UsersContext db = new UsersContext())
            {
                UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userHandle.ToLower());
                userGuid = user.UserGuid;
            }

            return userGuid;
        }

        // GET api/ProductDetail
        //public string Get()
        public IEnumerable<ProductDetailType> Get()
        {
            var list = new List<ProductDetailType>();
            var breeders = new string[] { "Syngenta" };
            //var model = new ProductDetailApiResponse(breeders, "Achillea", "unrooted", "6/2014", "7/2014");
            //return model.ProductDetailTypeCollection;
            return list;
        }

        // GET api/ProductDetail/ [with ProductDetailApiRequest]
        public IEnumerable<ProductDetailType> Get(string id, [FromUri] ProductDetailApiRequest requestData)
        {
            bool v = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            if (v)
            {
                string n = System.Web.HttpContext.Current.User.Identity.Name;
                requestData.UserHandle = n;
                requestData.UserGuid = UserGuid(requestData.UserHandle);
            }

            //ToDo: Validate Request Values before proceeding

            //ToDo: Might want to revise this to acept the ApiRequestObject instead of parameters
            //var model = new ProductDetailApiResponse(requestData.UserGuid, requestData);
            var model = new List<ProductDetailType>();
            return model;
        }

        // POST api/ProductDetail
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/ProductDetail/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE api/ProductDetail/5
        //public void Delete(int id)
        //{
        //}
    }
}
