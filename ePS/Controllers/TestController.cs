﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataServiceLibrary;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using ePS.Filters;
using ePS.Models;
using BusinessObjectServices;

namespace ePS.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            //var userName = "MichaelR";

            //using (UsersContext db = new UsersContext())
            //{
            //    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userName.ToLower());
            //    var s = user.UserGuid;
            //}




            return View();
        }

        //[Authorize]
        //public ActionResult ANetCardCharge()
        //{
        //    return View();
        //}
        //[Authorize]
        //public ActionResult ANetCardOnFile()
        //{
        //    return View();
        //}

        //[RequireHttps]
        public ActionResult secureLogin()
        {

            return View();
        }


        public ActionResult Dialogs()
        {

            return View();
        }

        public ActionResult SimpleMembership() {
            ViewBag.Title = "Simple Memebership Test Page";
            
            
            return View();
        }
        
        public ActionResult Login()
        {
            return PartialView("_login");
        }

        public ActionResult Authenticate()
        {
            return PartialView("_login");
        }

        public ActionResult ValidateRole(string role)
        {
            role = role ?? string.Empty;

            var isLoggedIn = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            var isInRole = System.Web.HttpContext.Current.User.IsInRole(role);

            return Json(
                    new
                    {
                        WasLoggedIn = isInRole,
                        IsLoggedIn = isLoggedIn
                    },
                        JsonRequestBehavior.AllowGet
                    );

        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult TestMe()
        {
            var isAdministrator = false;
            string sFirstName = string.Empty;
            string userName = string.Empty;
            var loggedIn = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            if (loggedIn)
            {
                isAdministrator = System.Web.HttpContext.Current.User.IsInRole("Administrator");

                userName = System.Web.HttpContext.Current.User.Identity.Name;

                //Todo:Scott 
                //The information gathered below comes from the Membership Db UserProfile table
                //The userGuid is the link between the membershipDb and the dataDb
                //We need add a method - probably added to GrowerService.cs - that takes userGuid and returns more information about the user, including First Name
                //Then, lower down FirstName can be populated with live data where it says "BettyBoop" now.  That will do it.
                //I do not recommend adding FirstName to UserProfile and having multiple versions of the truth.
                //The next five lines of code call on some services that return user profile information but dont use it.  Just here to illustrate.
                var service = new UserProfileTableService();
                var userProfile = service.Get(userName);
               // var bolUserProfile = new BusinessObjectsLibrary.UserProfile(userProfile);
                var userGuid = userProfile.UserGuid;
              //  var bolUserGuid = bolUserProfile.UserGuid;
                var personService = new BusinessObjectServices.PersonService();
                var oPerson =  personService.GetPersonFromUser(userGuid);
                sFirstName = oPerson.FirstName;

            }

            return Json(
                    new
                    {
                        LoggedIn = loggedIn,
                        UserName = userName,
                        FirstName = sFirstName,
                        IsAdministrator = isAdministrator
                    }//,
                        //JsonRequestBehavior.AllowGet
                    );
            }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            var isLoggedIn = false;
            var wasLoggedIn = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            try
            {
                WebSecurity.Logout();
                isLoggedIn = false;
            }
            catch {
                isLoggedIn = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            }

            return Json(
                    new
                    {
                        WasLoggedIn = wasLoggedIn,
                        IsLoggedIn = isLoggedIn
                    }//,
                        //JsonRequestBehavior.AllowGet
                    );
            }
    
    }

}
