﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models.ProductApi;
using AttributeRouting.Web.Http;
using ePS.Types;

namespace ePS.Controllers.api
{
    public class AdminAppProductController : ApiControllerBase
    {

        // GET api/adminappproduct
        [GET("api/GetProgramProducts", RouteName = "GetProgramProducts")]
        public GetResponseModel GetProgramProducts([FromUri] GetRequestModel request)
        {
            var model = new GetResponseModel(GetCurrentUserGuid(), request);
            return model;
        }

        [GET("api/GetProductFromCode", RouteName = "GetProductFromCode")]
        public GetProductFromCodeResponseModel GetProductFromCode([FromUri] GetRequestModel request)
        {
            var model = new GetProductFromCodeResponseModel(GetCurrentUserGuid(), request);
            return model;
        }




        // POST api/adminappproduct
        public void Post([FromBody]string value)
        {
        }

        // PUT api/adminappproduct/5
        public void Put(int id, [FromBody]string value)
        {
        }

        //// DELETE api/adminappproduct/5
        //public void Delete(int id)
        //{
        //}
    }
}
