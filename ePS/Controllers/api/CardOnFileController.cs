﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ePS.Models;
using ePS.Models.CardOnFile;

using ePS.Filters;

namespace ePS.Controllers.api
{
    public class CardOnFileController : ApiController
    {
        private Guid UserGuid(string userHandle)
        {
            var userGuid = Guid.Empty;

            using (UsersContext db = new UsersContext())
            {
                UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userHandle.ToLower());
                if (user != null) userGuid = user.UserGuid;
            }

            return userGuid;
        }


        [ApiRequiresHttps]
        public PutResponseModel Put([FromBody] PutRequestModel request)
        {

            var userGuid = Guid.Empty;
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var userHandle = System.Web.HttpContext.Current.User.Identity.Name; //set to users Login
                userGuid = this.UserGuid(userHandle);
            }

            var response = new PutResponseModel(userGuid, request);

            return response;
        }

        // Get api/cardonfile//< a valid guid >?type ="GrowerOrderGuid"
        public GetResponseModel Get(Guid id, string mode)
        {
            if (id != Guid.Empty) { };

            var request = new GetRequestModel();
            if (mode == "GrowerOrderGuid")
            {
                request.GrowerOrderGuid = id;
            };

            var userGuid = Guid.Empty;

            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string userHandle = System.Web.HttpContext.Current.User.Identity.Name;
                userGuid = this.UserGuid(userHandle);
            }

            var responseModel = new GetResponseModel(userGuid, request);

            return responseModel;
        }
        
        // DELETE api/cardonfile/5
        public bool Delete(string id)
        {
            id = id ?? string.Empty;
            var service = new BusinessObjectServices.CardOnFileService();
            var success = service.DeleteCustomerProfile(id);

            return success;
        }

       

    }
}
