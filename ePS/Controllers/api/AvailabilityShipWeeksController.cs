﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using ePS.Models.AvailabilityShipWeeksApi;

namespace ePS.Controllers.api
{
    public class AvailabilityShipWeeksController : ApiControllerBase
    {
        [GET("api/AvailabilityShipWeeks")]
        [GET("api/V1/AvailabilityShipWeeks")]
        [GET("api/V2/AvailabilityShipWeeks")]
        public GetResponseModel Get([FromUri] GetRequestModel request)
        {
            var model = new GetResponseModel(GetCurrentUserGuid(), request);
            return model;
        }
    }
}
