﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models.ProgramApi;
using AttributeRouting.Web.Http;
using ePS.Types;

namespace ePS.Controllers.api
{
    public class AdminAppProgramController : ApiControllerBase
    {
        // GET api/adminappprogram
        [GET("api/GetAllPrograms", RouteName = "GetAllPrograms")]
        public GetResponseModel GetAllPrograms([FromUri] GetRequestModel request)
        {
            var model = new GetResponseModel(GetCurrentUserGuid(), request);
            return model;
        }

        [GET("api/GetSupplierPrograms", RouteName = "GetSupplierPrograms")]
        public GetSupplierProgramsResponseModel GetSupplierPrograms([FromUri] GetRequestModel request)
        {
            var model = new GetSupplierProgramsResponseModel(GetCurrentUserGuid(), request);
            return model;
        }
        
        // GET api/adminappprogram/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/adminappprogram
        public void Post([FromBody]string value)
        {
        }

        // PUT api/adminappprogram/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/adminappprogram/5
        public void Delete(int id)
        {
        }
    }
}
