﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models;

using ePS.Models.AdminAuthApi;


namespace ePS.Controllers.api
{
    [Authorize]
    public class AdminAuthController : ApiController
    {
        //// GET api/adminauth
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/adminauth/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        [AllowAnonymous]
        public PostResponseModel Post(PostRequestModel request)
        {
            var response = new PostResponseModel(request);
            return response;
        }

        //// PUT api/adminauth/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/adminauth/5
        //public void Delete(int id)
        //{
        //}
    }
}
