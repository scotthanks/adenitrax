﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;


//using ePS.Filters;
using ePS.Models;

using ePS.Models.AvailabilityApi;
using AttributeRouting.Web.Http;
using AdminAppApiLibrary;


namespace ePS.Controllers.api
{
    public class AvailabilityController : ApiControllerBase
    {
        [GET("api/Availability/GetGrowerVolumeLevels")]
        public GetGrowerVolumeLevelsResponseModel GetVolumeLevels([FromUri] GetGrowerVolumeLevelsRequestModel request)
        {
            return new GetGrowerVolumeLevelsResponseModel(GetCurrentUserGuid(), request);
        }

        
        [GET("api/AvailabilityCart")]
        [GET("api/V1/Availability")]
        [GET("api/V2/Availability")]
        public GetAvailabilityResponseModel Get([FromUri] GetAvailabilityRequestModel availabilityRequest)
        {
            return new GetAvailabilityResponseModel(GetCurrentUserGuid(), availabilityRequest);
        }
        [GET("api/AvailabilityCount")]
        public GetAvailabilityCountResponseModel GetAvailabilityCount([FromUri] GetAvailabilityRequestModel availabilityRequest)
        {
            return new GetAvailabilityCountResponseModel(GetCurrentUserGuid(), availabilityRequest);
        }
        [GET("api/AvailabilityByPage")]
        public GetAvailabilityByPageResponseModel GetAvailabilityByPage([FromUri] GetAvailabilityByPageRequestModel availabilityRequest)
        {
            var x = new GetAvailabilityByPageResponseModel(GetCurrentUserGuid(), availabilityRequest);
            return x;
        }

        [GET("api/V2/Availability/SubstituteList", RouteName = "AvailabilityV1SubstituteList")]
        [GET("api/V2/Availability/SubstituteList"), System.Web.Http.HttpGet]
        public GetSubstitutesResponseModel GetSubstituteList([FromUri] GetSubstitutesRequestModel request)
        {
            return new GetSubstitutesResponseModel(GetCurrentUserGuid(), request);
        }

        [GET("api/Availability/SupplierProducts"), System.Web.Http.HttpGet]
        public SupplierProductsResponse SupplierProducts([FromUri] Guid supplierOrderGuid)
        {
            var model = new SupplierProductsModel(GetCurrentUserGuid(), supplierOrderGuid);
            return model.Response;
        }

        [PUT("api/AvailabilityCart")]
        [PUT("api/V1/Availability")]
        [PUT("api/V2/Availability/AddOrUpdateOrderLineQuantity")]
        public PutResponseModel Put([FromBody]PutRequestModel request)
        {
            var putmodel = new PutResponseModel(GetCurrentUserGuid(), request,false);
            return putmodel;
        }

        [PUT("api/AvailabilityCart2")]
        public PutResponseModel Put2([FromBody]PutRequestModel request)
        {
            var putmodel = new PutResponseModel(GetCurrentUserGuid(), request,true);
            return putmodel;
        }

        [POST("api/AvailabilityCart")]
        [POST("api/V1/Availability")]
        [POST("api/V2/Availability")]
        public PostResponseModel Post([FromBody]PostRequestModel request)
        {
            var postmodel =  new PostResponseModel(GetCurrentUserGuid(), request,false);
            return postmodel;
        }
        [POST("api/AvailabilityCart2")]
        public PostResponseModel Post2([FromBody]PostRequestModel request)
        {
            var postmodel = new PostResponseModel(GetCurrentUserGuid(), request,true);
            return postmodel;
        }
        [POST("api/AvailabilityOrder")]
        public PostResponseModel PostOrder([FromBody]PostRequestModel request)
        {
            request.AddToOrder = true;
            return new PostResponseModel(GetCurrentUserGuid(), request,false);
        }
        [GET("api/Availability/GetCategoryAndFormNames")]
        public GetCategoryAndFormNamesResponseModel GetCategoryAndFormNames([FromUri] GetCategoryAndFormNamesRequestModel request)
        {
            return new GetCategoryAndFormNamesResponseModel(GetCurrentUserGuid(), request);
        }
    }
}
