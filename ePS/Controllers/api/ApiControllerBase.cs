﻿using System;
using System.Linq;
using System.Web.Http;
using ePS.Models;
using System.Web;

namespace ePS.Controllers.api
{
    public class ApiControllerBase : ApiController
    {
        internal Guid GetCurrentUserGuid()
        {
            var userGuid = Guid.Empty;
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string userHandle = System.Web.HttpContext.Current.User.Identity.Name;
                userGuid = GetUserGuid(userHandle);
            }
            return userGuid;
        }

        private Guid GetUserGuid(string userHandle)
        {
            Guid userGuid = Guid.Empty;

            using (UsersContext db = new UsersContext())
            {
                UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userHandle.ToLower());
                if (user != null)
                {
                    userGuid = user.UserGuid;                    
                }
            }
            return userGuid;
        }

        public bool IsSecureRequest()
        {
            bool isSecureRequest = (Request.RequestUri.Scheme == "https");
            return isSecureRequest;
        }

       // protected HttpContextWrapper GetHttpContextWrapper()
       public HttpContextWrapper GetHttpContextWrapper()
        {
            HttpContextWrapper httpContextWrapper = null;
            if (HttpContext.Current != null)
            {
                httpContextWrapper = new HttpContextWrapper(HttpContext.Current);
            }
            else if (Request.Properties.ContainsKey("MS_HttpContext"))
            {
                httpContextWrapper = (HttpContextWrapper)Request.Properties["MS_HttpContext"];
            }
            return httpContextWrapper;
        }

    }
}
