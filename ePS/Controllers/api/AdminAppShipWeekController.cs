﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ePS.Models.ShipWeekApi;
using AttributeRouting.Web.Http;
using ePS.Types;


namespace ePS.Controllers.api
{
    public class AdminAppShipWeekController : ApiControllerBase
    {
        // GET api/adminappshipweek
       [GET("api/GetShipWeekList", RouteName = "GetShipWeekList")]
        public GetResponseModel GetShipWeekList([FromUri] GetRequestModel request)
        {
            var model = new GetResponseModel(GetCurrentUserGuid(), request);
            return model;
        }

       [GET("api/GetCurrentMonday", RouteName = "GetCurrentMonday")]
       public GetCurrentMondayResponseModel GetCurrentMonday([FromUri] GetRequestModel request)
       {
           var model = new GetCurrentMondayResponseModel(GetCurrentUserGuid());
           return model;
       }

       [GET("api/GetCurrentShipWeek", RouteName = "GetCurrentShipWeek")]
       public GetCurrentShipWeekResponseModel GetCurrentShipWeek([FromUri] GetRequestModel request)
       {
           var model = new GetCurrentShipWeekResponseModel(GetCurrentUserGuid());
           return model;
       }

       [GET("api/GetShipWeek", RouteName = "GetShipWeek")]
       public GetShipWeekResponseModel GetShipWeek([FromUri] GetShipWeekRequestModel request)
       {
           var model = new GetShipWeekResponseModel(GetCurrentUserGuid(), request);
           return model;
       }


    //        public class OrderController : ApiControllerBase
    //{
    //    [GET("api/Order", RouteName = "OrderGetSummaryV1a")]
    //    [GET("api/V1/Order/{RequestType:alpha}", RouteName = "OrderGetSummaryV1b")]
    //    [GET("api/V2/Order/Summary/{RequestType:alpha}", RouteName = "OrderGetSummaryV2")]
    //    public GetResponseSummaryModel Get([FromUri] GetSummaryRequestModel summaryRequest, string requestType)
    //    {
    //        return new GetResponseSummaryModel(GetCurrentUserGuid(), summaryRequest);
    //    }


        // GET api/adminappshipweek/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/adminappshipweek
        public void Post([FromBody]string value)
        {
        }

        // PUT api/adminappshipweek/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/adminappshipweek/5
        public void Delete(int id)
        {
        }
    }
}
