﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessObjectsLibrary;

using BusinessObjectServices;

namespace ePS.Controllers.api
{
    public class VarietiesController : ApiControllerBase
    {

        // GET api/varieties/5
        public IEnumerable<Species> Get(int id, string PlantCategoryCode, string ProductFormCode, string SupplierCodes, string SpeciesCodes)
        {
            var list = new List<Species>();
            if (SupplierCodes == null || SupplierCodes == string.Empty) { return list; }
            if (SpeciesCodes == null)
            {
                SpeciesCodes = ",";
            }
            if (SpeciesCodes == null || SpeciesCodes == string.Empty) { return list; }

            string[] supplierCodesArray = SupplierCodes.Split(',');
            string[] speciesCodesArray = SpeciesCodes.Split(',');

            var userGuid = GetCurrentUserGuid();
          //  var status = new StatusObject(new Guid(), false);
          //  var service = new SpeciesService(status);
          //  return service.SelectSpecies("EPS", PlantCategoryCode, ProductFormCode, supplierCodesArray);


            var service = new VarietyService();
            return service.SelectVarietiesGroupedBySpecies(userGuid, plantCategoryCode: PlantCategoryCode, productFormCode: ProductFormCode, supplierCodeList: supplierCodesArray, speciesCodeList: speciesCodesArray);


        }

    }
}
