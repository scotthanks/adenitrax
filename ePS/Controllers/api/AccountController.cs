﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ePS.Filters;
using ePS.Models.AccountApi;

namespace ePS.Controllers.api
{
    //[Authorize]
    //[InitializeSimpleMembership]
    public class AccountController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/registration/5
        public PutResponseModel Put([FromBody]PutRequestModel request)
        {
            return new PutResponseModel(request);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}