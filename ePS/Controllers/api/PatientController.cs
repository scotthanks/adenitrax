﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using ePS.Models.PatientApi;
using ePS.Types;
using AttributeRouting.Web.Http;
using BusinessObjectsLibrary;
using System.IO;

namespace ePS.Controllers.api
{
    public class PatientController : ApiControllerBase
    {

        [GET("api/patient", RouteName = "GetClinicPatients")]
        public ePS.Models.PatientApi.GetClinicPatients GetClinicPatients()
        {
        

             return new ePS.Models.PatientApi.GetClinicPatients(GetCurrentUserGuid());

        }

    }
}