﻿using ePS.Models.SupplierOrderFeeApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ePS.Models;
using AttributeRouting.Web.Http;
using ePS.Types;
using AdminAppApiLibrary;

namespace ePS.Controllers.api
{
    public class SupplierOrderFeeController : ApiControllerBase
    {
        [POST("api/SupplierOrderFee", RouteName = "SupplierOrderFeePostV1a")]
        [POST("api/V1/SupplierOrderFee", RouteName = "SupplierOrderFeePostV1b")]
        public PostResponseModel PostV1([FromBody]PostRequestModel request)
        {
            return new PostResponseModel(GetCurrentUserGuid(), request);
        }

        [POST("api/V2/Order/SupplierOrder/{SupplierOrderGuid:guid}", RouteName = "SupplierOrderFeePostV2")]
        public PostResponseModel PostV2(Guid supplierOrderGuid, [FromBody]PostRequestModel request)
        {
            request.SupplierOrderGuid = supplierOrderGuid;
            return new PostResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/SupplierOrderFee", RouteName = "SupplierOrderFeePutV1a")]
        [PUT("api/V1/SupplierOrderFee", RouteName = "SupplierOrderFeePutV1b")]
        public PutResponseModel PutV1([FromBody]PutRequestModel request)
        {
            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/SupplierOrderFee/Update", RouteName = "SupplierOrderFee/Update"), System.Web.Http.HttpPut]
        public AdminAppResponseBase Update([FromBody]SupplierOrderFeeType request)
        {
            var model = new SupplierFeeUpdateModel(GetCurrentUserGuid(), request);
            return model.Response;
        }


        [PUT("api/V2/Order/SupplierOrderFee/{SupplierOrderFeeGuid:guid}", RouteName = "SupplierOrderFeePutV2")]
        public PutResponseModel PutV2(Guid supplierOrderFeeGuid, [FromBody]PutRequestModel request)
        {
            request.SupplierOrderFeeGuid = supplierOrderFeeGuid;
            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [DELETE("api/SupplierOrderFee/Delete", RouteName = "SupplierOrderFeeDeleteV1a")]
        public DeleteResponseModel Delete([FromBody] DeleteRequestModel request)
        {
            return new DeleteResponseModel(GetCurrentUserGuid(), request.supplierOrderFeeGuid);
        }

        //[DELETE("api/SupplierOrderFee/{SupplierOrderFeeGuid:guid}", RouteName = "SupplierOrderFeeDeleteV1a")]
        //[DELETE("api/V1/SupplierOrderFee/{SupplierOrderFeeGuid:guid}", RouteName = "SupplierOrderFeeDeleteV1b")]
        //[DELETE("api/V2/Order/SupplierOrderFee/{SupplierOrderFeeGuid:guid}", RouteName = "SupplierOrderFeeDeleteV2")]
        //public DeleteResponseModel Delete(Guid supplierOrderFeeGuid)
        //{
        //    return new DeleteResponseModel(GetCurrentUserGuid(), supplierOrderFeeGuid);
        //}
    }
}
