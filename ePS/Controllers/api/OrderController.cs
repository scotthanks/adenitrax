﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using ePS.Models.OrderApi;
using ePS.Types;
using AttributeRouting.Web.Http;
using BusinessObjectsLibrary;
using System.IO;

namespace ePS.Controllers.api
{
    public class OrderController : ApiControllerBase
    {

        [GET("api/Order/SupplierOrderFees/{supplierOrderGuid:guid}", RouteName = "GetSupplierOrderFees")]
        public ePS.Models.OrderApi.GetResponseSupplierFeeModel GetSupplierOrderFees(Guid supplierOrderGuid)
        {
            //var id = Guid.NewGuid();
            var request = new GetRequestSupplierFeeModel() { SupplierOrderGuid = supplierOrderGuid };

             return new ePS.Models.OrderApi.GetResponseSupplierFeeModel(GetCurrentUserGuid(), request);

        }

        [GET("api/Order/GrowerOrderFees/{growerOrderGuid:guid}", RouteName = "GetGrowerOrderFees")]
        public ePS.Models.OrderApi.GetResponseGrowerFeeModel GetGrowerOrderFees(Guid growerOrderGuid)
        {
            //var id = Guid.NewGuid();
            var request = new GetRequestGrowerFeeModel() { GrowerOrderGuid = growerOrderGuid };
            return new ePS.Models.OrderApi.GetResponseGrowerFeeModel(GetCurrentUserGuid(), request);

        }
        

        
        //[GET("api/Order", RouteName = "OrderGetSummaryV1a")]
        //[GET("api/V1/Order/{RequestType:alpha}", RouteName = "OrderGetSummaryV1b")]
        //public GetResponseSummaryModel Get([FromUri] GetSummaryRequestModel summaryRequest, string requestType)
        //{
        //    if (summaryRequest.RequestType == "MyPlacedOrders")
        //    {
        //        summaryRequest.RequestType = GetSummaryRequestModel.RequestTypeEnum.MyOpenOrders.ToString();
        //    }
        //    else if (summaryRequest.RequestType == "AllPlacedOrders")
        //    {
        //        summaryRequest.RequestType = GetSummaryRequestModel.RequestTypeEnum.AllOpenOrders.ToString();
        //    }

            
        //    //return new GetResponseSummaryModel(GetCurrentUserGuid(), summaryRequest);
        //        var model = new GetResponseSummaryModel(GetCurrentUserGuid(), summaryRequest);
        //        return model;
           
        //}
       
        [GET("api/Order/DetailHistory", RouteName = "OrderGetDetailHistory")]
        public GetOrderHistoryResponseModel GetDetailHistory([FromUri] string OrderGuid)
        {
            var theGuid = new Guid(OrderGuid);
            //ToDo: Scott, the line of code creating the var theGuid will convert any faulty guid passed in to an empty guid
            //an empty guid breaks the GetOrderHistoryResponseModel.  It should be able to handle an empty guid (a faulty guid coming in)
            //Moreover, it is questionable to create a new GetOrderHistoryRequestModel with no inputs as you create a new GetOrderHistoryResponseModel, why not create it inside rather than as a parameter here
            return new GetOrderHistoryResponseModel(GetCurrentUserGuid(), theGuid, new GetOrderHistoryRequestModel());
           
        }

        [GET("api/V2/Order/Summary/MyShoppingCartOrders")]
        public GetCartResponseModel GetMyShoppingCartOrders()
        {
            return new GetCartResponseModel(GetCurrentUserGuid());
        }

        [GET("api/V2/Order/Summary/MyOpenOrders")]
        public GetOpenOrderSummary GetMyOpenOrders()
        {
       
            var getOpenOrderSummary =  new GetOpenOrderSummary(GetCurrentUserGuid());
            return getOpenOrderSummary;
        }

        [GET("api/V2/Order/Summary/MyShippedOrders")]
        public GetShippedOrderSummary GetMyShippedOrders()
        {
            var getShippedOrderSummary = new GetShippedOrderSummary(GetCurrentUserGuid());
            return getShippedOrderSummary;
        }

        [GET("api/V2/Order/Summary/MyCancelledOrders")]
        public GetCancelledOrderSummary GetMyCancelledOrders()
        {
            var getCancelledOrderSummary = new GetCancelledOrderSummary(GetCurrentUserGuid());
            return getCancelledOrderSummary;
        }


        //Old way
        //[GET("api/Order/Totals", RouteName = "OrderGetTotals")]
        //public object Get([FromUri] string category, string form, string shipWeeks)
        //{
        //    //ToDo: Rick you may want to revise the name I gave this to fit your convention
        //    //ToDo: Rick please populate these values and add any other values that are relevent to an order over its entire life
        //    int carted = 555;
        //    int uncarted = 999;

        //    //ToDo: Rick, you may want to use a Request Object, Response Object and Defined Type 
        //    var response = new {
        //        Carted = carted,
        //        Uncarted = uncarted,
        //        Total = carted + uncarted
        //    };
            
        //    return response;
        //}

        //new way
        [GET("api/Order/Totals", RouteName = "OrderGetTotals")]
        public GetResponseGrowerOrderTotalsModel GetGrowerOrderTotals([FromUri] GetRequestGrowerOrderTotalsModel request)
        {
        //public GetResponseGrowerOrderTotalsModel GetGrowerOrderTotals([FromUri] Guid GrowerOrderGuid,string Category, string Form, string ShipWeekString)
            //var id = Guid.NewGuid();
            //var request = new GetRequestGrowerOrderTotalsModel() {GrowerOrderGuid = GrowerOrderGuid, Category = Category, Form = Form, ShipWeekString = ShipWeekString };
            return new GetResponseGrowerOrderTotalsModel(GetCurrentUserGuid(), request);

        }
        [GET("api/Grower/Totals", RouteName = "GrowerGetTotals")]
        public GetGrowerTotalsResponseModel GetGrowerTotals([FromUri] GetGrowerTotalsRequestModel request)
        {
            return new GetGrowerTotalsResponseModel(GetCurrentUserGuid(), request);

        }
        [GET("api/Grower/TotalsPerWeek", RouteName = "GrowerGetTotalsPerWeek")]
        public GetGrowerTotalsPerWeekResponseModel GetGrowerTotalsPerWeek([FromUri] GetGrowerTotalsRequestModel request)
        {
            return new GetGrowerTotalsPerWeekResponseModel(GetCurrentUserGuid(), request);

        }


        //[GET("api/Order/DetailData", RouteName = "OrderGetDetailV1")]
        //public GetResponseDetailModel Get([FromUri] GetRequestDetailModel request, Guid orderGuid)
        //{
        //    var model = new GetResponseDetailModel(GetCurrentUserGuid(), request);
        //    return model;
        //}
        [GET("api/Order/OrderPDF")] //AttributeRouting
        //public HttpResponseMessage GetPdfRecordData([FromUri] Guid OrderGuid)
        //{
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");
        //    var x = new GetOrderPDFResponseModel(GetCurrentUserGuid(), OrderGuid);

        //    MemoryStream ms = x.OrderPDF;
        //    response.Content = new ByteArrayContent(ms.ToArray());
        //    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
        //    ms.Close();
        //    return response;
        //    //MemoryStream ms2 = x.OrderPDF;

        //    //using (FileStream file = new FileStream("c:\\temp\\confirmation.pdf", FileMode.Create, System.IO.FileAccess.Write))
        //    //{
        //    //    byte[] bytes = new byte[ms2.Length];
        //    //    ms2.Read(bytes, 0, (int)ms2.Length);
        //    //    file.Write(bytes, 0, bytes.Length);
        //    //    ms.Close();
        //    //}

        //}
        [GET("api/Order/OrderPDF")] //AttributeRouting
        public GetOrderPDFResponseModel GetPdfRecordData([FromUri] Guid OrderGuid)
        {
           
            var x = new GetOrderPDFResponseModel(GetCurrentUserGuid(), OrderGuid);   
            return x;
        } 
      

        //TODO: Depricated
        [GET("api/Order/DetailData", RouteName = "OrderGetDetailV1a")]
        [GET("api/V1/Order/DetailData", RouteName = "OrderGetDetailV1b")]
        public GrowerOrderType GetV1(Guid orderGuid, [FromUri] GetDetailRequestModel detailRequest)
        {
            var model = new GetDetailResponseModel(GetCurrentUserGuid(), detailRequest);
            return model.GrowerOrder;
        }

        [GET("api/V2/Order/Detail", RouteName = "OrderGetDetailV2Request")]
        [GET("api/V2/Order/Detail/{orderGuid:guid}", RouteName = "OrderGetDetailV2OrderGuid")]
        public GrowerOrderType GetV2(Guid orderGuid, [FromUri] GetRequestDetailModelV2 request)
        {
            return new GetResponseDetailModelV2(GetCurrentUserGuid(), request).GrowerOrder;
        }

        [PUT("api/Order", RouteName = "OrderPutV1a")]
        [PUT("api/V1/Order", RouteName = "OrderPutV1b")]
        public PutResponseModel PutV1([FromBody] PutRequestModel request)
        {
            
            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order/{GrowerOrderGuid:guid}/PlaceOrder", RouteName = "OrderPutV2GrowerOrderPlaceOrder")]
        public PutResponseModel PutV2PlaceOrder(Guid growerOrderGuid)
        {
            var request = new PutRequestModel()
                {
                    GrowerOrderGuid = growerOrderGuid,
                    OrderTransitionTypeLookupCode = "PlaceOrder"
                };

            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order", RouteName = "OrderPutV2Request")]
        public PutResponseModel PutV2Request([FromBody] PutRequestModel request)
        {
            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order/GrowerOrder/{GrowerOrderGuid:guid}", RouteName = "OrderPutV2bGrowerOrder")]
        public PutResponseModel PutV2GrowerOrder(Guid growerOrderGuid, [FromBody] PutRequestModel request)
        {
            request.GrowerOrderGuid = growerOrderGuid;

            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order/SupplierOrder/{SupplierOrderGuid:guid}", RouteName = "OrderPutV2SupplierOrderGuid")]
        public PutResponseModel PutV2SupplierOrder(Guid supplierOrderGuid, [FromBody] PutRequestModel request)
        {
            request.SupplierOrderGuid = supplierOrderGuid;

            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order/OrderLine/{OrderLineGuid:guid}", RouteName = "OrderPutV2OrderLine")]
        public PutResponseModel PutV2OrderLine(Guid orderLineGuid, [FromBody] PutRequestModel request)
        {
            request.OrderLineGuid = orderLineGuid;

            return new PutResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order/UpdatePrices", RouteName = "OrderPutUpdatePricesV2")]
        public PutPriceUpdateResponseModel Put([FromBody] PutPriceUpdateRequestModel request)
        {
            return new PutPriceUpdateResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order/Substitute", RouteName = "OrderSubstituteV2"), System.Web.Http.HttpPut]
        public SubstituteResponseModel Substitute([FromBody] SubstituteRequestModel request)
        {
            return new SubstituteResponseModel(GetCurrentUserGuid(), request);
        }

        [PUT("api/V2/Order/UpdateOrderLineQuantity", RouteName = "UpdateOrderLineQuantity"), System.Web.Http.HttpPut]
        public UpdateOrderLineQuantityResponseModel UpdateOrderLineQuantity(UpdateOrderLineQuantityRequestModel request)
        {
            //CSingleTone.Instance.theCounter += 1;
            var x = new UpdateOrderLineQuantityResponseModel(GetCurrentUserGuid(), request);
            //request.Messages.
            //request.Messages = request.Messages + " " + CSingleTone.Instance.theCounter.ToString();
            return x;
        }



        [POST("api/Order/AddOrUpdateOrderLine", RouteName = "AddOrUpdateOrderLine"), System.Web.Http.HttpPost]
        public AddOrUpdateOrderLineResponseModel AddOrUpdateOrderLine(AddOrUpdateOrderLineRequestModel request)
        {
            return new AddOrUpdateOrderLineResponseModel(GetCurrentUserGuid(), request);
        }


        [POST("api/V2/Order/GrowerOrderLog", RouteName = "OrderPostGrowerOrderLogV2")]
        public PostGrowerOrderLogResponseModel Post([FromBody] PostGrowerOrderLogRequestModel growerOrderLogRequest)
        {
            return new PostGrowerOrderLogResponseModel(GetCurrentUserGuid(), growerOrderLogRequest,"Add");
        }
  

        [POST("api/V2/Order/GrowerOrderLogDelete", RouteName = "OrderDeleteGrowerOrderLogV2"), System.Web.Http.HttpPost]
        public DeleteGrowerOrderLogResponseModel DeleteLog([FromBody]  DeleteGrowerOrderLogRequestModel growerOrderHistoryLogRequest)
        {
           // var growerOrderHistoryLogRequest = new DeleteGrowerOrderLogRequestModel();
           // growerOrderHistoryLogRequest.GrowerOrderHistoryGuid = growerOrderLogRequest.GrowerOrderGuid;
            return new DeleteGrowerOrderLogResponseModel(GetCurrentUserGuid(), growerOrderHistoryLogRequest);
        }

        [POST("api/V2/Order/GrowerOrderLogEdit", RouteName = "OrderEditGrowerOrderLogV2"), System.Web.Http.HttpPost]
        public EditGrowerOrderLogResponseModel EditLog([FromBody]  EditGrowerOrderLogRequestModel growerOrderLogRequest)
        {
            // var growerOrderHistoryLogRequest = new DeleteGrowerOrderLogRequestModel();
            // growerOrderHistoryLogRequest.GrowerOrderHistoryGuid = growerOrderLogRequest.GrowerOrderGuid;
            return new EditGrowerOrderLogResponseModel(GetCurrentUserGuid(), growerOrderLogRequest);
        }

    }
}