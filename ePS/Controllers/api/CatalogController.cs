﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using BusinessObjectsLibrary.Catalog;
using ePS.Models;

namespace ePS.Controllers.api
{
    public class CatalogController : ApiControllerBase
    {
        [GET("api/Catalog/Categories", RouteName = "GetCatalogCategories")]
        public IEnumerable<CategoryType> GetCatalogCategories()
        {
            var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userguid, IsSecureRequest());
            var service = new CatalogService(status);
            var result = service.CategoriesGet();
            return result;
        }

        [GET("api/Catalog/ProductForms", RouteName = "GetCatalogProductForms")]
        public IEnumerable<ProductFormType> GetCatalogForms(string categoryCode)
        {
            var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userguid, IsSecureRequest());
            var service = new CatalogService(status);
            var result = service.ProductFormsGet(categoryCode);
            return result;
        }


        [GET("api/Catalog/Suppliers", RouteName = "GetCatalogSuppliers")]
        public IEnumerable<SupplierType> GetCatalogSuppliers(string categoryCode, string productFormCode)
        {
            var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userguid, IsSecureRequest());
            var service = new CatalogService(status);

            var suppliers = service.SuppliersGet(userguid, categoryCode, productFormCode);
            return suppliers;
        }


        //Returns complex SelectionsType containing all suppliers, all species, all varieties plus a relationship type showing species to suppliers relationships
        [GET("api/Catalog/Selections", RouteName = "GetCatalogSelections")]
        public SelectionsType GetCatalogSelections(string categoryCode, string productFormCode)
        {
            var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userguid, IsSecureRequest());
            var service = new CatalogService(status);

            var selections = service.SelectionsGet( categoryCode, productFormCode);
            return selections;
        }


        [GET("api/Catalog/Species", RouteName = "GetCatalogSpecies")]
        public IEnumerable<SpeciesType> GetCatalogSpecies(string categoryCode, string productFormCode, string supplierCodes)
        {

            if (string.IsNullOrEmpty(supplierCodes)) { return new List<SpeciesType>(); }

            var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userguid, IsSecureRequest());
            var service = new CatalogService(status);
            var species = service.SpeciesGet(categoryCode, productFormCode, supplierCodes);
            
            return species;
        }


        [GET("api/Catalog/Varieties", RouteName = "GetCatalogVarieties")]
        public IEnumerable<VarietyType> GetCatalogVarieties(string categoryCode, string productFormCode, string supplierCodes, string speciesCodes)
        {
            if (string.IsNullOrEmpty(supplierCodes) || string.IsNullOrEmpty(speciesCodes))
            {
                return new List<VarietyType>();
            }
            var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userguid, IsSecureRequest());

            var service = new CatalogService(status);
            var varietiesInSpecies = service.VarietiesInSpeciesGetV2(categoryCode, productFormCode, supplierCodes,
                speciesCodes);
            status = service.Status;


            return varietiesInSpecies;
        }

        [GET("api/Catalog/Variety/Detail", RouteName = "GetCatalogVarietyDetail")]
        public VarietyDetailType GetCatalogVarietyDetail(string varietyCode)
        {
            if (string.IsNullOrEmpty(varietyCode))
            {
                return new VarietyDetailType();
            }
            var userguid = GetCurrentUserGuid();
            var status = new StatusObject(userguid, IsSecureRequest());

            var service = new CatalogService(status);
            var product = service.VarietyDetailGet(varietyCode);

            return product;
        }
    }
}
