﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models.ProductFormApi;

namespace ePS.Controllers.api
{
    public class ProductFormController : ApiController
    {
        // GET api/productform
        public GetResponseModel Get([FromUri] GetRequestModel request)
        {
            var model = new GetResponseModel(request);
            return model;
        }

        // GET api/productform/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/productform
        public void Post([FromBody]string value)
        {
        }

        // PUT api/productform/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productform/5
        public void Delete(int id)
        {
        }
    }
}
