﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;

using ePS.Models.SelectionMenu;

namespace ePS.Controllers.api
{
    public class SelectionMenuController : ApiController
    {
        [GET("api/SelectionMenu", RouteName = "SelectionMenuGet")]
        public SelectionMenuModel Get()
        {
            var model = new Models.SelectionMenu.SelectionMenuModel();
            return model;
        }
    }
}
