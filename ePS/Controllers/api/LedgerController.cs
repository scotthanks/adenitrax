﻿using AttributeRouting.Web.Http;
using ePS.Models.LedgerApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ePS.Controllers.api
{
    public class LedgerController :  ApiControllerBase
    {
        [GET("api/Ledger/Invoices", RouteName = "Invoices"), System.Web.Http.HttpGet]
        public InvoicesResponse Invoices()
        {
            var model = new InvoicesResponseModel(GetCurrentUserGuid());
            return model.Response;
        }

        [GET("api/Ledger/Payments", RouteName = "Payments"), System.Web.Http.HttpGet]
        public PaymentsResponse Payments()
        {
            var model = new PaymentsResponseModel(GetCurrentUserGuid());
            return model.Response;
        }

    }
}
