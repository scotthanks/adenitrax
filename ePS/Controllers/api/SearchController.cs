﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;

using ePS.Models;
using ePS.Models.SearchApi;
using ePS.Types;

namespace ePS.Controllers.api
{
    public class SearchController : ApiControllerBase
    {


        //[GET("api/Search/AutoFill/{SearchPage}/{SearchStem}", RouteName = "GetAutoFillList")]
        [GET("api/Search/AutoFill/{Scope}/", RouteName = "GetAutoFillList")]
        public GetResponseAutoFill GetAutoFill([FromUri] GetRequestAutoFill request)
        {
            var model = new GetResponseAutoFill(GetCurrentUserGuid(), request);
            return model;
        }

        [GET("api/Search/Result/{Scope}/", RouteName = "GetSearchResult")]
        public GetResponseResult GetResult([FromUri] GetRequestResult request)
        {
            var model = new GetResponseResult(GetCurrentUserGuid(), request);
            return model;
        }


        [GET("api/Search/Template/{TemplateGuid:guid}/", RouteName = "GetSearchResultTemplate")]
        public GetResponseTemplate GetTemplate([FromUri] GetRequestTemplate request)
        {
            var model = new GetResponseTemplate(GetCurrentUserGuid(), request);
            return model;
        }


        [GET("api/Search/ResultTemplates/", RouteName = "GetSearchResultTemplates")]
        public GetResponseSearchTemplates GetResultTemplates()
        {
            var model = new GetResponseSearchTemplates();
            return model;
        }



    }
}
