﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models.CardChargeApi;

namespace ePS.Controllers.api
{
    [Authorize]
    public class CardChargeController : ApiController
    {

        public PutResponseModel Put([FromBody] PutRequestModel request)
        {

            //ToDo Authenticate user and validate right to use company credit card

            var response = new PutResponseModel(request);


            return response;

        }

    }
}
