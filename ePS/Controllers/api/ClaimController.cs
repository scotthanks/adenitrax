﻿using System;
using System.Web;
using System.Web.Http;
using ePS.Models.ClaimApi;
using ePS.Types;
using AttributeRouting.Web.Http;

namespace ePS.Controllers.api
{
    public class ClaimController : ApiControllerBase
    {
         [GET("api/claims")]
        public GetResponseSummaryModel GetMyClaims()
        {
            return new GetResponseSummaryModel(GetCurrentUserGuid(), new GetSummaryRequestModel() { RequestType = GetSummaryRequestModel.RequestTypeEnum.MyClaims.ToString() });
        }

        

    
        


    }
}