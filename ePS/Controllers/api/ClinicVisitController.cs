﻿using System;
using ePS.Models.ClinicVisitApi;
using AttributeRouting.Web.Http;
namespace ePS.Controllers.api
{
    public class ClinicVisitController : ApiControllerBase
    {
        
        
      
        [GET("api/ClinicVisit/GetClinicVisits")]
        public GetClinicVisits ClinicVisitsGet(string ClinicPatientGuid, string DateStart, string DateEnd)
        {
            try 
            {
                Guid clinicPatientGuid = Guid.Empty;
                if (ClinicPatientGuid != null && ClinicPatientGuid != "" )
                { 
                    clinicPatientGuid = new Guid(ClinicPatientGuid);
                }

                DateTime dateStart = new DateTime(2018, 1, 1);
                DateTime dateEnd = new DateTime(2018, 12, 31);
                dateStart = DateTime.ParseExact(DateStart, "yyyy-MM-dd HH:mm:ss,fff",
                                       System.Globalization.CultureInfo.InvariantCulture);

                dateEnd = DateTime.ParseExact(DateEnd, "yyyy-MM-dd HH:mm:ss,fff",
                                       System.Globalization.CultureInfo.InvariantCulture);

                var response = new GetClinicVisits(GetCurrentUserGuid(), clinicPatientGuid, dateStart, dateEnd);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }

            
        }
        
    }
}
