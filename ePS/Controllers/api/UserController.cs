﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ePS.Models;
using ePS.Models.UserApi;
using AttributeRouting.Web.Http;

namespace ePS.Controllers.api
{
    public class UserController : ApiControllerBase
    {

        [GET("api/user/growerusers")]
        public GetGrowerUserResponseModel GetGrowerUsers()
        {

            var response = new GetGrowerUserResponseModel(GetCurrentUserGuid());
            return response;
            //return new GetGrowerUserResponseModel(GetCurrentUserGuid());

        }

        [POST("/api/user/post")]
        public PostGrowerUserResponseModel Post (PostGrowerUserRequestModel request)
        {
            var response = new PostGrowerUserResponseModel(GetCurrentUserGuid(), request);
            return response;
        }

        [PUT("api/userrole/put")]
        public PutGrowerUserRoleResponseModel PutRole([FromBody]PutGrowerUserRoleRequestModel request)
        {
            return new PutGrowerUserRoleResponseModel(GetCurrentUserGuid(), request);
        }
        [PUT("api/userdelete/put")]
        public PutGrowerUserDeleteResponseModel PutRole2([FromBody]PutGrowerUserDeleteRequestModel request)
        {
            return new PutGrowerUserDeleteResponseModel(GetCurrentUserGuid(), request);
        }
        
        //public PutGrowerUserDeleteResponseModel DeleteUser([FromBody]PutGrowerUserDeleteRequestModel request)
        //{
        //    var response = new PutGrowerUserDeleteResponseModel(GetCurrentUserGuid(), request);
        //    return response;
        //}
      
        //[POST("/api/userrole/post")]
        //public PostGrowerUserRoleResponseModel PostUpdate(PutGrowerUserRoleRequestModel request)
        //{
        //    var response = new PostGrowerUserRoleResponseModel(GetCurrentUserGuid(), request);
        //    return response;
        //}

       


    }
}
