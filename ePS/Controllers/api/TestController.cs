﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models.TestApi;
using ePS.Types;
using AttributeRouting.Web.Http;


namespace ePS.Controllers.api
{
    public class TestController : ApiControllerBase
    {

        [PUT("api/test/Order/UpdateOrderedQuantity", RouteName = "UpdateOrderedQuantityTest")]
        public Object Put(TestRequestModel request)
        {
            //var response = new {
            //    Success = true,
            //    Message = "Here is a message"
            //};

            //return response;

            var response = new TestResponseModel(GetCurrentUserGuid(), request);

            return response;
        }

    }

}
