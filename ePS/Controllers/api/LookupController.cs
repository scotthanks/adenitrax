﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using ePS.Models;
using ePS.Models.LookupApi;

namespace ePS.Controllers.api
{
    public class LookupController : ApiController
    {
        private Guid _UserGuid(string userHandle)
        {
            Guid userGuid = Guid.Empty;

            using (var db = new UsersContext())
            {
                UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userHandle.ToLower());
                if (user != null) userGuid = user.UserGuid;
            }

            return userGuid;
        }

        public GetResponseModel Get([FromUri] GetRequestModel request)
        {
            Guid userGuid = Guid.Empty;
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string userHandle = HttpContext.Current.User.Identity.Name; //set to users Login
                userGuid = _UserGuid(userHandle);
            }

            var model = new GetResponseModel(userGuid, request);

            return model;
        }
    }
}