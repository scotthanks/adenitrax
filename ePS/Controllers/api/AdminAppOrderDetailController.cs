﻿using AdminAppApiLibrary;
using AttributeRouting.Web.Http;
using ePS.Models;
using ePS.Models.AdminAppOrderDetailApi;
using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ePS.Controllers.api
{
    public class AdminAppOrderDetailController : ApiControllerBase
    {
        [POST("api/AdminAppOrderDetail/UpdateOrderLine", RouteName = "UpdateOrderLine"), System.Web.Http.HttpPost]
        public UpdateOrderLineResponse UpdateOrderLine(UpdateOrderLineRequest request)
        {
            var model = new UpdateOrderLineResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }

        [POST("api/AdminAppOrderDetail/UpdateOrderLine/QantityShipped", RouteName = "QantityShipped"), System.Web.Http.HttpPost]
        public UpdateOrderLineResponse QantityShipped(UpdateOrderLineRequest request)
        {
            var model = new QantityShippedResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }


        [POST("api/AdminAppOrderDetail/UpdateSupplierOrder", RouteName = "UpdateSupplierOrder"), System.Web.Http.HttpPost]
        public UpdateSupplierOrderResponse UpdateSupplierOrder(UpdateSupplierOrderRequest request)
        {
            var model = new UpdateSupplierOrderResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }

        [POST("api/AdminAppOrderDetail/SendSupplierEmail", RouteName = "SendSupplierEmail"), System.Web.Http.HttpPost]
        public AdminAppResponseBase SendSupplierEmail(SendEmailRequest request)
        {
            var model = new SupplierEmailResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }


        [POST("api/AdminAppOrderDetail/CreateOrderFee", RouteName = "CreateOrderFee"), System.Web.Http.HttpPost]
        public CreateOrderFeeResponse CreateOrderFee(CreateOrderFeeRequest request)
        {
            var model = new CreateOrderFeeResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }

        //[POST("api/AdminAppOrderDetail/CancelGrowerOrder", RouteName = "CancelGrowerOrder"), System.Web.Http.HttpPost]
        //public CancelGrowerOrderResponse CancelGrowerOrder(CancelGrowerOrderRequest request)
        //{
        //    var model = new CancelGrowerOrderResponseModel(GetCurrentUserGuid(), request);
        //    return model.Response;
        //}

        [POST("api/AdminAppOrderDetail/AddOrderLine", RouteName = "AddOrderLine"), System.Web.Http.HttpPost]
        public AddOrderLineResponse AddOrderLine(AddOrderLineRequest request)
        {
            var model = new AddOrderLineResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }

        [PUT("api/AdminAppOrderDetail/UpdateOrderFee", RouteName = "UpdateOrderFee"), System.Web.Http.HttpPut]
        public CreateOrderFeeResponse UpdateOrderFee(CreateOrderFeeRequest request)
        {
            var model = new UpdateOrderFeeResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }

        [DELETE("api/AdminAppOrderDetail/DeleteOrderFee", RouteName = "DeleteOrderFee"), System.Web.Http.HttpDelete]
        public CreateOrderFeeResponse DeleteOrderFee(CreateOrderFeeRequest request)
        {
            var model = new DeleteOrderFeeResponseModel(GetCurrentUserGuid(), request);
            return model.Response;
        }

        [GET("api/AdminAppOrderDetail/GetSpeciesListForSupplierOrder", RouteName = "GetSpeciesListForSupplierOrder"), System.Web.Http.HttpGet]
        public GetSpeciesListForSupplierOrderResponse GetSpeciesListForSupplierOrder(GetSpeciesListForSupplierOrderRequest request)
        {
            var model = new GetSpeciesListResponseModel(GetCurrentUserGuid(), request);
            
            return model.Response;
        }
       

    }
}
