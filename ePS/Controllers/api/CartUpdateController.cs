﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ePS.Models;
using ePS.Models.CartUpdateApi;
using ePS.Types;

namespace ePS.Controllers.api
{
    public class CartUpdateController : ApiController
    {
        private Guid _UserGuid(string userHandle)
        {
            var userGuid = new Guid();

            using (UsersContext db = new UsersContext())
            {
                UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userHandle.ToLower());
                userGuid = user.UserGuid;
            }

            return userGuid;
        }

        public object Post(PostRequestModel request)
        {
            var userHandle = string.Empty;
            var userGuid = Guid.Empty;
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                userHandle = System.Web.HttpContext.Current.User.Identity.Name;  //set to users Login
                userGuid = this._UserGuid(userHandle);
            }
            
            var responseModel = new PostResponseModel(userGuid, request);

            return responseModel;
        }
    }
}
