﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessObjectsLibrary;
using ePS.Models;

namespace ePS.Controllers.CatalogueAPI
{
    public class ShipWeekController : ApiController
    {
        // GET api/shipweek
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/shipweek/5
        public ShipWeekApiResponseModel Get(string id, [FromUri] ShipWeekApiRequest requestData)
        {
            var model = new ShipWeekApiResponseModel(requestData);
            return model;
        }

        // POST api/shipweek
        public void Post([FromBody]string value)
        {
        }

        // PUT api/shipweek/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/shipweek/5
        public void Delete(int id)
        {
        }
    }
}
