﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Controllers
{
    public class BreederController : ApiController
    {
        // GET api/breeder
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/breeder/5
        //public string Get(int id)
        public IEnumerable<GeneticOwner> Get(int id, string PlantCategoryCode, string ProductFormCode)
        {
            //Note: for now this ignores value of id but some value there is necessary to call this api

            var service = new BreederService();

            var breeders = service.SelectBreeders(PlantCategoryCode, ProductFormCode);

            return breeders;
        }

        // POST api/breeder
        public void Post([FromBody]string value)
        {
        }

        // PUT api/breeder/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/breeder/5
        public void Delete(int id)
        {
        }
    }
}
