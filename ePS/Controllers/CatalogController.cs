﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using BusinessObjectServices;

using ePS.Models;
using Microsoft.Ajax.Utilities;

namespace ePS.Controllers
{
    public class CatalogController : Controller
    {
        const string SUB_DOMAIN_DEVELOPMENT = "dev";

        //public ActionResult Index()
        //{
        //    var service = new ContentService();
        //    var content = service.GetStaticContentType("/Catalog/");
        //    ViewBag.ContentType = content;


        //    return View();

            
        //}

        ///////////////////////////////  OLD CATALOG ///////////////////////////////////////////////////////
        public ActionResult Search(string Category, string Form)
        {

            Category = (Category ?? string.Empty).Trim();
            Form = (Form ?? string.Empty).Trim();

            if (Category == string.Empty || Form == string.Empty)
            {
                return Redirect("/Catalog");
            }
            else
            {
                var model = new CatalogSearchPageModel(Category, Form);
                ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());


                //return Redirect(string.Format("../Cat.html/{0}/{1}", Category, Form));

                return View("CatalogSearch", model);


            };

        }
        [HttpGet]
        [Route("catalog/ProductDetail/varietyDetailAvailability")]
        public ActionResult ProductDetail(string id, string plantCategoryCode, string productFormCode, string supplierCodes, string varietyCode, string shipWeek)
        {

            ViewBag.Id = id;
            ViewBag.Title = "Product Detail Page";
            ViewBag.Message = "ProductDetail Application";

            var model = new CatalogDetailPartialPageModel(plantCategoryCode, productFormCode, supplierCodes, varietyCode, shipWeek);



            return PartialView("ProductDetail", model);

        }








        ///////////////////////////////  BEGIN VERSION 2 OF CATALOG ///////////////////////////////////////////////////////
        ////////////////////// REM THIS OUT to disable for production /////////////////////////////////////////////////////

        [HttpGet]
        [Route("catalog/{category}/{form}")]
        //[Route("catalog/{category}/{form}/{suppliers}")]
        //[Route("catalog/{category}/{form}/{suppliers}/{species}")]
        //[Route("catalog/{category}/{form}/{suppliers}/{species}/{product}")]
        //[Route("catalog/{category}/{form}/{suppliers}/{species}/{product}/{shipWeek}")]
        //public ActionResult CatalogV2(string category, string form, string suppliers, string species, string product, string shipWeek)
        public ActionResult CatalogV2(string category, string form)

        {
            if (category.IsNullOrWhiteSpace() || form.IsNullOrWhiteSpace() || category == string.Empty || form == string.Empty)
            {
                return Redirect("/Catalog");
            }
            else
            {
                var result = new CatalogSearchPageModel(category.ToUpper(), form.ToUpper());
                ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
                if (result.CategoryKey == null || result.FormKey == null)
                {
                    return Redirect("/");
                }

                ViewData["Mode"] = "catalog";
                ViewData["CategoryKey"] = result.CategoryKey.ToLower();
                ViewData["CategoryName"] = result.CategoryName;
                ViewData["FormKey"] = result.FormKey.ToLower();
                ViewData["FormName"] = result.FormName;
                
                return View("CatalogV2");
            }

        }

        [HttpGet]
        [Route("product/{productCode}")]
        public ActionResult ProductV2(string productCode)
        {

            if (string.IsNullOrEmpty(productCode))
            {
                return Redirect("/Catalog");
            }
            else
            {
                ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
                //return Redirect(string.Format("../Cat.html/{0}/{1}", Category, Form));

                ViewData["Mode"] = "product";
                ViewData["ProductCode"] = productCode;
                return View("CatalogV2");


            }
        }

        ///////////////////////////////  END VERSION 2 OF CATALOG ///////////////////////////////////////////////////////

















        ///////////////////////////////  BEGIN NEW CATALOG ///////////////////////////////////////////////////////
        ////////// REM THIS OUT to disable for production ///////////////////////////////////////////////////////

        //////[HttpGet]
        //////[Route("catalog/{category}/{form}")]
        //////[Route("catalog/{category}/{form}/{suppliers}")]
        //////[Route("catalog/{category}/{form}/{suppliers}/{species}")]
        //////[Route("catalog/{category}/{form}/{suppliers}/{species}/{product}")]
        //////[Route("catalog/{category}/{form}/{suppliers}/{species}/{product}/{shipWeek}")]
        //////public ActionResult CatalogNg(string category, string form, string suppliers, string species, string product, string shipWeek)
        //////{


        //////    if (category == null || form ==null|| category == string.Empty || form == string.Empty)
        //////    {
        //////        return Redirect("/Catalog");
        //////    } else {
        //////        var model = new CatalogSearchPageModel(category, category);
        //////        ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

        //////        //return Redirect(string.Format("../Cat.html/{0}/{1}", Category, Form));

        //////        return View("CatalogNg");


        //////    }

        //////}

        //////[HttpGet]
        //////[Route("product/{productCode}")]
        //////public ActionResult ProductNg(string productCode)
        //////{

        //////    if (string.IsNullOrEmpty(productCode) )
        //////    {
        //////        return Redirect("/Catalog");
        //////    }
        //////    else
        //////    {
        //////        //var model = new CatalogSearchPageModel(category, category);
        //////        ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

        //////        //return Redirect(string.Format("../Cat.html/{0}/{1}", Category, Form));

        //////        return View("CatalogNg");


        //////    }
        //////}
        ///////////////////////////////  END NEW CATALOG ///////////////////////////////////////////////////////
    }
}
