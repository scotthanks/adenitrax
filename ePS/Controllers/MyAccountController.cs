﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ePS.Controllers
{

    [Authorize]
    public class MyAccountController : Controller
    {

        const string SUB_DOMAIN_IS_DEVELOPMENT = "dev";

        [AllowAnonymous]
        public ActionResult ResetPassword(string id) {

            var passwordResetToken = (id ?? string.Empty).Trim();

            if (passwordResetToken == string.Empty) {
                return HttpNotFound();
            };

            ViewBag.PasswordResetToken = passwordResetToken;

            return View();
        }
        
        //[AllowAnonymous]    -- Commented out by Scott to fix #406  
        public ActionResult Index()
        {
            ViewBag.Title = "MyAccount | eCommerce for the Wound Care Professional | Adenitrax ";
            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            return View("MyAccount");
        }

        public ActionResult OrderPreferences()
        {
            ViewBag.Title = "Order Preferences | eCommerce for the professional grower | ePlantSource";
            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
            return View();
        }
        public ActionResult PaymentOptions(string id)
        {
            const string PAGE_MODE_ALL = "all";
            const string PAGE_MODE_METHODS = "paymentmethods";
            const string PAGE_MODE_CARDS = "creditcards";
            const string PAGE_MODE_LINE = "lineofcredit";

            //clarify value of id
            id = id ?? string.Empty;
            id = id.Trim().ToLower();
            //if id != open OR shipped then id = all
            if (id != PAGE_MODE_METHODS && id != PAGE_MODE_CARDS && id != PAGE_MODE_LINE) { id = PAGE_MODE_ALL; };
            ViewBag.PageMode = id;

            ViewBag.Title = "Payment Options | eCommerce for the Wound Care Professional | Adenitrax";
            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
            return View();
        }

        public ActionResult ManageAddresses(string id)
        {
            const string PAGE_MODE_ALL = "all";
            const string PAGE_MODE_METHODS = "paymentmethods";
            const string PAGE_MODE_CARDS = "creditcards";
            const string PAGE_MODE_LINE = "lineofcredit";

            //clarify value of id
            id = id ?? string.Empty;
            id = id.Trim().ToLower();
            //if id != open OR shipped then id = all
            if (id != PAGE_MODE_METHODS && id != PAGE_MODE_CARDS && id != PAGE_MODE_LINE) { id = PAGE_MODE_ALL; };
            ViewBag.PageMode = id;

            ViewBag.Title = "Payment Options | eCommerce for the Wound Care Professional | Adenitrax";
            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
            return View();
        }



        public ActionResult Claims()
        {

            ViewBag.Title = "Track Claims | eCommerce for the Wound Care Professional | Adenitrax";

            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());


            return View();
        }

        public ActionResult SubmitClaim()
        {

            ViewBag.Title = "Submit Claim | eCommerce for the Wound Care Professional | Adenitraxe";

            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());


            return View();
        }

        public ActionResult TransactionSummary(string id)
        {

            ViewBag.Title = "Transactions | eCommerce for the Wound Care Professional | Adenitrax";
            const string PAGE_MODE_ALL = "all";
            const string PAGE_MODE_STATEMENT = "statement";
            const string PAGE_MODE_HISTORY = "history";
            //clarify value of id
            id = id ?? string.Empty;
            id = id.Trim().ToLower();
            if (id != PAGE_MODE_STATEMENT && id != PAGE_MODE_HISTORY) { id = PAGE_MODE_ALL; };
            ViewBag.PageMode = id;


            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());


            return View();
        }

        public ActionResult OrdersPDF()
        {

            ViewBag.Title = "Order Confirmation | eCommerce for the Wound Care Professional | Adenitraxe";



            return View();
        }
        public ActionResult Orders(string id)
        {
            ViewBag.Title = "Orders | eCommerce for the Wound Care Professional | Adenitrax";

            const string PAGE_MODE_ALL = "all";
            const string PAGE_MODE_OPEN = "open";
            const string PAGE_MODE_SHIPPED = "shipped";
            const string PAGE_MODE_CANCELLED = "cancelled";

            //clarify value of id
            id = id ?? string.Empty;
            id = id.Trim().ToLower();
            //if id != open OR shipped then id = all
            if (id != PAGE_MODE_OPEN && id != PAGE_MODE_SHIPPED && id != PAGE_MODE_CANCELLED) { id = PAGE_MODE_ALL; };
            ViewBag.PageMode = id;

            //show/hide TestInfo based on AppSetting
            ViewBag.ShowTestInfo = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            return View();
        }

        public ActionResult AccountSettings(string id)
        {
            ViewBag.Title = "Settings | eCommerce for the Wound Care Professional | Adenitraxe";
            const string PAGE_MODE_ALL = "all";
            const string PAGE_MODE_ACCOUNT_SETTINGS = "settings";
            const string PAGE_MODE_EMAIL_PREFERENCES = "email";

            //clarify value of id
            id = id ?? string.Empty;
            id = id.Trim().ToLower();
            //if id != open OR shipped then id = all
            if (id != PAGE_MODE_ACCOUNT_SETTINGS && id != PAGE_MODE_EMAIL_PREFERENCES) { id = PAGE_MODE_ALL; };
            ViewBag.PageMode = id;

            //show/hide TestInfo based on AppSetting
            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            return View();
        }


        public ActionResult Users(string id)
        {
            ViewBag.Title = "Users | eCommerce for the Wound Care Professional | Adenitrax";
            const string PAGE_MODE_ALL = "all";
            const string PAGE_MODE_ADD_USER = "add";
            const string PAGE_MODE_MANAGE_USERS = "manage";

            //clarify value of id
            id = id ?? string.Empty;
            id = id.Trim().ToLower();
            //if id != open OR shipped then id = all
            if (id != PAGE_MODE_ADD_USER && id != PAGE_MODE_MANAGE_USERS) { id = PAGE_MODE_ALL; };
            ViewBag.PageMode = id;

            //show/hide TestInfo based on AppSetting
            ViewBag.RunningInDevMode = (SUB_DOMAIN_IS_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());

            return View();
        }

        public ActionResult UnderConstruction(string title)
        {
            ViewBag.Title = title;
            ViewBag.Message = "This page is under construction.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult TermsAndConditions(string id)
        {
            ViewBag.Title = "Terms-Conditions | eCommerce for the Wound Care Professional | Adenitrax";


            return View();
        }

    }
}
