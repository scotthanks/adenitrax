﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using ePS.Filters;

using BusinessObjectsLibrary;
using BusinessObjectServices;

using ePS.Models;

namespace ePS.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        const string SUB_DOMAIN_DEVELOPMENT = "dev";

        public ActionResult EarlyIE()
        {
            return View();
        }



        public ActionResult Index()
        {
            ViewBag.Title = "eCommerce for the Wound Care Professional | Adenitrax";

         

            return View();
        }


        [Authorize]
        public ActionResult ProductDetail(string id, string plantCategoryCode, string productFormCode, string supplierCodes, string varietyCode, string shipWeek)
        {
            //Note: shipWeek by convention is ##|#### = 30|2013
            
            
            //string productCategoryCode = id ?? string.Empty;
            //Note: id is ProductCategory
            //ToDo: This may not be the right call - breeder vs supplier
            //ToDo: currently defaulting to these values
            //productCategoryCode = "VA";
            //productFormCode = "RC";
            //suppier = "Breeder";
            //species = "Calibrachoa";
            /////////////////////////////////////////

            //Note productForm matches the submitted data and must be kept the same, it contains the Product Form Code per Database
            //id = id ?? string.Empty;
            ViewBag.Id = id;
            ViewBag.Title = "Product Detail Page";
            ViewBag.Message = "ProductDetail Application";

            var model = new ProductDetailViewModel(plantCategoryCode, productFormCode, supplierCodes, varietyCode, shipWeek);

            return View("ProductDetailView", model);
        }

        public ActionResult Catalogue(string id, string productForm)
        {
            //If id and productForm are defined then go to CatalogueSearch_2, else CatalogueSearch_1
            id = id ?? string.Empty;

            if (productForm == null)
            { //This is Catalogue Level 1
                ViewBag.Id = id;
                ViewBag.Title = "Catalogue Page 1";
                ViewBag.Message = "Catalogue Application Page";
                var model = new CatalogueSearch_1PageModel(id);
                return View("CatalogueSearch_1", model);
            }
            else
            { //This is Catalogue Level 2
                var model = new CatalogueSearch_2PageModel(id, productForm);

                ViewBag.Title = "Catalogue Page 2";
                ViewBag.Message = "Catalogue Application Page";
                return View("CatalogueSearch_2", model);
            }
        }
        
        [RequireHttps]
        [Authorize]
        public ActionResult Cart()
        {
            ViewBag.Title = "Shopping Cart";
            ViewBag.Message = "Your shopping cart";
            ViewBag.ShowTestInfo = (SUB_DOMAIN_DEVELOPMENT == System.Configuration.ConfigurationManager.AppSettings["SubDomain"].ToLower());
            return View();
        }


        public ActionResult About()
        {
         

            return View();
        }
        public ActionResult Contact()
        {
          

            return View();
        }
    
     

    }
}
