﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ProductApi
{
    public class GetResponseModel : ResponseBase
    {


        public GetResponseModel(Guid userGuid, GetRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {

            var productService = new BusinessObjectServices.ProductService();
            var productList = productService.GetAllProgramProducts(request.ProgramGuid);
            var newProductList = new List<ePS.Types.ProductType>();
            foreach (var product in productList)
            {

                var newItem = new ePS.Types.ProductType
                {
                    ProductGuid = product.Guid,
                    SupplierProductIdentifier = product.SupplierProductIdentifier,
                    SupplierProductDescription = product.SupplierProductDescription,
                    ProgramGuid = product.ProgramGuid

                };

                newProductList.Add(newItem);
            }

            this.Products = newProductList;


        }

        public List<ePS.Types.ProductType> Products { get; set; }
    }
}