﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;



namespace ePS.Models.ClinicVisitApi
{
    public class GetClinicVisits : ResponseBase
    {

        public List<ClinicPatientVisit> ClientPatientVisits;
        public GetClinicVisits(Guid userGuid, Guid ClinicPatientGuid, DateTime DateStart, DateTime DateEnd)
            : base(userGuid, requiresAuthentication: true)
        {

           
            
            StatusObject status = new StatusObject(userGuid,true);
            var service = new ClinicPatientService(status);

            var visits = service.GetClinicPatientVisits(ClinicPatientGuid, DateStart, DateEnd);
            ClientPatientVisits = visits;

            Success = true;
        }
    
    }
}