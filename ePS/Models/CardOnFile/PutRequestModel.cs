﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.CardOnFile
{
    public class PutRequestModel
    {
        public Guid GrowerGuid { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }

        // These are not needed, they can be gotten from the Grower record.
        public string EmailAddress { get; set; }

        internal bool IsValid
        {
            get { return string.IsNullOrEmpty(IsValidMessage); }
        }

        internal string IsValidMessage
        {
            get
            {
                string message = "";

                if (string.IsNullOrEmpty(FirstName))
                {
                    message = "The first name is required.";
                }
                else if (string.IsNullOrEmpty(LastName))
                {
                    message = "The last name is required.";
                }
                else if (string.IsNullOrEmpty(CardNumber))
                {
                    message = "The credit card number is required.";
                }
                //else if (!Utility.CreditCard.IsValidCardNumber(CardNumber))
                //{
                //    message = "The credit card number is not valid.";
                //    message = "";
                //}
                else if (string.IsNullOrEmpty(ExpirationDate))
                {
                    message = "The expiration date is required.";
                }
                else if (!Utility.CreditCard.IsValidExpirationDate(ExpirationDate))
                {
                    message = "The expiration date is not valid.";
                }
                else if (string.IsNullOrWhiteSpace(StreetAddress1))
                {
                    message = "The Street Address is required.";
                }
                else if (string.IsNullOrWhiteSpace(City))
                {
                    message = "The City Name is required";
                }
                else if (string.IsNullOrWhiteSpace(StateCode))
                {
                    message = "The State is required.";
                }
                else if (string.IsNullOrWhiteSpace(ZipCode))
                {
                    message = "The Zip Code is required.";
                }
                //else if (!Utility.ZipCode.IsValidUsOrCanadaZipCode(ZipCode))
                //{
                //    message = "The Zip Code is invalid.";
                //}
                else if (string.IsNullOrWhiteSpace(PhoneNumber))
                {
                    message = "The Phone Number is required.";
                }
                else if (!Utility.PhoneNumber.IsValidPhoneNumber(PhoneNumber, allowEmpty: false))
                {
                    message = "The Phone Number is invalid.";
                }
                else if (!string.IsNullOrWhiteSpace(FaxNumber) && !Utility.PhoneNumber.IsValidPhoneNumber(FaxNumber, allowEmpty: true))
                {
                    message = "The Fax Number is invalid.";
                }
                else
                {
                    message = "";
                }

                return message;
            }
        }
    }
}