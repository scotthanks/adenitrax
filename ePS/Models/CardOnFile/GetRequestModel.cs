﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.CardOnFile
{
    public class GetRequestModel
    {
        public Guid GrowerGuid { get; set; }
        public Guid GrowerCreditCardGuid { get; set; }
        public Guid GrowerOrderGuid { get; set; }
    }
}