﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;
using BusinessObjectServices;

namespace ePS.Models
{
    public class ProductDetailApiResponse
    {
        //public ProductDetailApiResponse(string PlantCategoryCode, string ProductFormCode, string SupplierCodes, string VarietyCodes, string startWeek, string endWeek)
        //public ProductDetailApiResponse(ProductDetailApiRequest requestData, string PlantCategoryCode, string ProductFormCode, string SupplierCodes, string VarietyCodes, string ShipWeekString)
        public ProductDetailApiResponse(Guid userGuid, ProductDetailApiRequest requestData)
        {
            var plantCategoryCode = requestData.PlantCategoryCode;
            var productFormCode = requestData.ProductFormCode;
            var supplierCodeArray = requestData.SupplierCodes.Split(new char[] { ',' });
            var varietyCodeArray = requestData.VarietyCode.Split(new char[] { ',' });
            var selectedShipWeekString = requestData.ShipWeekString;

            var shipWeekService = new ShipWeekService();
            var selectedShipWeek = shipWeekService.ParseShipWeekString(selectedShipWeekString);

            var userProfileService = new UserProfileService();

            var status = new StatusObject(userGuid, true);
            var service = new AvailabilityService(status);
            var availability = 
                service.GetAvailability
                (
                    userCode:requestData.UserHandle,
                    shipWeek:selectedShipWeek,
                    weeksBefore:1,
                    weeksAfter:3,
                    programTypeCode: plantCategoryCode, 
                    productFormCategoryCode:productFormCode,
                    supplierCodeList:supplierCodeArray,
                    geneticOwnerCodeList:null,
                    speciesCodeList:null,
                    varietyCodeList:varietyCodeArray,
                    varietyMatchOptional: true,
                    includePrices: userProfileService.UserHasPermissionToSeePrices(requestData.UserGuid),
                    includeCartData: userProfileService.UserHasPermissionToPlaceOrders(requestData.UserGuid)
                );

            var list = new List<ProductDetailType>();

            foreach (var productData in availability.ProductList)
            {
                var availabilityList = new int[productData.SelectedAvailability.Count];
                int i = 0;
                foreach (var quantityData in productData.SelectedAvailability)
                {
                    availabilityList[i] = quantityData.Quantity;
                    i++;
                }

                var type = new ProductDetailType()
                    {
                        Guid = productData.Guid,
                        SupplierName = productData.SupplierName,
                        ProductForm = productData.ProductFormName,
                        Product = productData.ProductDescription,
                        OrderQuantity = productData.OrderLine == null ? 0 : productData.OrderLine.QuantityOrdered,
                        Availability = availabilityList
                    };

                list.Add(type);
            }
            
            this.ProductDetailTypeCollection = list;
        }
        public IEnumerable<ProductDetailType> ProductDetailTypeCollection { get; set; }

    }
}