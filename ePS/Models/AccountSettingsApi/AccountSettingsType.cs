﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AccountSettingsApi
{
    public class AccountSettingsType
    {
        public Guid Guid { get; set; }
        public bool IsAccountAdministrator { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyType { get; set; }
        public string CompanyRole { get; set; }
        public bool SubscribeToNewsLetter { get; set; }
        public bool SubscribeToPromotions { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalPhone { get; set; }
        public string PersonalEmail { get; set; }

        public bool IsValid{
            get
            {
                if (this.Guid == Guid.Empty){return false;}
                this.CompanyName = this.CompanyName ?? string.Empty;
                if (string.IsNullOrEmpty(this.CompanyName)){ return false; }
                return true;
            }
        }
    }
}