﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AccountSettingsApi
{
    public class PostRequestModel : RequestBase
    {
        public override void Validate()
        {
            //ToDo: Validate the Guid Passed in - check to see that record exists
            if (this.AccountSettings.Guid == Guid.Empty)
            {
                this.AddMessage("Guid Supplied is not Valid");
                
            }


            //ToDo: validate the data submitted 
            if (string.IsNullOrEmpty(this.AccountSettings.CompanyName))
            {
                this.AddMessage("Company Name is required.");

            }
        }

        public AccountSettingsType AccountSettings { get; set; }

    }
}