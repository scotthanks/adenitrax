﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AccountSettingsApi
{
    public class GetRequestModel : RequestBase
    {
        public override void Validate()
        {
            if (this.AccountGuid == Guid.Empty)
            {
                this.AddMessage("Account Guid is empty or was not specified.");
            }
        }

        public Guid AccountGuid { get; set; }

    }
}