﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AvailabilityShipWeeksApi
{
    public class GetRequestModel : RequestBase
    {
        public string Category { get; set; }
        public string Form { get; set; }
        public string SupplierCodes { get; set; }
        public string SpeciesCodes { get; set; }
        public string ShipWeek { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(Category))
            {
                AddMessage("Category is required.");
            }

            if (string.IsNullOrEmpty(Form))
            {
                AddMessage("Form is required.");
            }
        }
    }
}