﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;
using BusinessObjectServices;


namespace ePS.Models
{
    public class CatalogueSearch_2PageModel
    {
        public CatalogueSearch_2PageModel(string categoryKey, string productFormKey)
        {
            
            var service = new CatalogueService();
            categoryKey = categoryKey ?? string.Empty;
            categoryKey = categoryKey.Trim();
            if (categoryKey.Length > 0)
            {
                this.SelectedCategory = service.Catalogue(categoryKey);
            }
            else
            {
                this.SelectedCategory = new Catalogue();
            }
        
            //ToDo: Code to populate this.SelectedProductForm
            productFormKey = productFormKey ?? string.Empty;
            productFormKey = productFormKey.Trim();
            if (productFormKey.Length > 0)
            {
                //this.SelectedCategory = service.Catalogue(catalogueKey);
                this.SelectedProductFormCategory = service.ProductForm(productFormKey);
            }
            else
            {
                this.SelectedProductFormCategory = new ProductFormCategory();
            }

        }
        public Catalogue SelectedCategory { get; set; }
        public ProductFormCategory SelectedProductFormCategory { get; set; }

    }
}