﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.TestApi
{
    public class TestRequestModel : RequestBase
    {
        public Guid OrderLineGuid { get; set; }
        public int Quantity { get; set; }
        public int OriginalQuantity { get; set; }
        public string ProductDescription { get; set; }


        public override void Validate()
        {
            if (OrderLineGuid == Guid.Empty) {
                AddMessage("Guid is empty.");
            };
        }




    }
}