﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.TestApi
{
    public class TestResponseModel : ResponseBase
    {
        public TestResponseModel(Guid userGuid, TestRequestModel request) : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                Success = true;
            }

            if (request.Quantity == 0) { QuantityOnOrder = 0; };  //cancelled
            QuantityOnOrder = 1000;     //return value to trigger update on client
            


            //Setup for Prod 2013-10-21
            Success = false;
            QuantityOnOrder = request.OriginalQuantity;
            //unable to set Message, it is read only at this point



        }
        public int QuantityOnOrder { get; set; }
        public int LineItemStatus { get; set; }
        public int Price { get; set; }
    }
}