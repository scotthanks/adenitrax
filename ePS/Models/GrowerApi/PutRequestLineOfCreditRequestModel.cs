﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class PutRequestLineOfCreditRequestModel : RequestBase
    {
        public Guid UserGuid { get; set; }
        public int RequestedLineOfCredit { get; set; }       

        public override void Validate()
        {
            if (UserGuid == Guid.Empty)
            {
                AddMessage("User Guid is required.");
            }
            if (RequestedLineOfCredit == 0)
            {
                AddMessage("Requested Line Of Credit must be greater than 0.");
            }
           
        }
    }
}