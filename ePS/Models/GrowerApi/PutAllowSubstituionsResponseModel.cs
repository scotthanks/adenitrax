﻿using System;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PutAllowSubstitutionsResponseModel : ResponseBase
    {
                
     
        public PutAllowSubstitutionsResponseModel(Guid userGuid, bool AllowSubstitutions,PutAllowSubstitutionsRequestModel AllowSubstitutionRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            AllowSubstitutionRequest.UserGuid = userGuid;
            AllowSubstitutionRequest.AllowSubstitutions = AllowSubstitutions;

            if (ValidateAuthenticationAndRequest(AllowSubstitutionRequest))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                Success = growerBusinessService.UpdateGrowerField
                (
                    userGuid: userGuid,
                    theField: "AllowSubstitutions",
                    theValue: AllowSubstitutions.ToString()
                );


                if (!Success)
                {
                    AddMessage("The grower was not updated.");
                }
            }
        }
    }
}