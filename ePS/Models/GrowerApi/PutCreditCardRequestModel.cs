﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class PutCreditCardRequestModel : RequestBase
    {
        public Guid UserGuid { get; set; }
        public Guid CreditCardGuid { get; set; }
        public string CardDescription { get; set; }
        public string Action { get; set; }
       

        public override void Validate()
        {
            if (UserGuid == Guid.Empty)
            {
                AddMessage("User Guid is required.");
            }
            if (CreditCardGuid == Guid.Empty)
            {
                AddMessage("Credit Card Guid is required.");
            }
            if (CardDescription == string.Empty)
            {
                AddMessage("Credit Card description is required.");
            }
            if (Action == string.Empty)
            {
                AddMessage("Action is required.");
            }
        
        }
    }
}