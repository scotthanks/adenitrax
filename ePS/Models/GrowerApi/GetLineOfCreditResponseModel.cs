﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;



namespace ePS.Models.GrowerApi
{
    public class GetLineOfCreditResponseModel : ResponseBase
    {
        public Guid userGuid { get; set; }
        public List<SelectListItemType> CreditAppStatusTypeList { get; set; }
        public int RequestedLineOfCredit { get; set; }
        public int CreditLimit { get; set; }
        public bool CreditAppOnFile { get; set; }
        public string CreditAppStatusName { get; set; }
        public string GrowerName { get; set; }

      

        public SelectListItemType SelectListItem { get; private set; }


        public GetLineOfCreditResponseModel(Guid userGuid)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(userGuid, "User Guid"))
            {
               
                var growerService = new GrowerService();
                Grower oGrower = growerService.TryGetGrowerForUser(userGuid);
                CreditLimit = oGrower.CreditLimit;
                CreditAppOnFile = oGrower.CreditAppOnFile;
                RequestedLineOfCredit = oGrower.RequestedLineOfCredit;
                CreditAppStatusName = oGrower.CreditAppStatus.DisplayName;
                GrowerName = oGrower.GrowerName;       
                var creditAppStatusTypeList = new List<SelectListItemType>();

                foreach (var creditAppStatusTypeBusinessObject in oGrower.CreditAppStatus.SiblingList)
                {
                    var creditAppStatusType = new SelectListItemType
                    {
                        Guid = creditAppStatusTypeBusinessObject.Guid,
                        Code = creditAppStatusTypeBusinessObject.Code,
                        Name = creditAppStatusTypeBusinessObject.Name,
                        DisplayName = creditAppStatusTypeBusinessObject.DisplayName,
                    };
                    if (creditAppStatusTypeBusinessObject.Guid == oGrower.CreditAppStatus.Guid)
                        creditAppStatusType.IsSelected = true;



                    creditAppStatusTypeList.Add(creditAppStatusType);
                }
                CreditAppStatusTypeList = creditAppStatusTypeList;
                Success = true;
            }
        }

   
    }
}
