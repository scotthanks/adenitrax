﻿using System;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PutCreditCardResponseModel : ResponseBase
    {


        public PutCreditCardResponseModel(Guid userGuid,  PutCreditCardRequestModel UpdateCreditCardRequest)
            : base(userGuid, requiresAuthentication: true)
        {

            UpdateCreditCardRequest.UserGuid = userGuid;
            if (ValidateAuthenticationAndRequest(UpdateCreditCardRequest))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();
                DateTime DateDeactivated = Convert.ToDateTime ("1/1/2000");
                if (UpdateCreditCardRequest.Action  == "Delete")
                { 
                    DateDeactivated = DateTime.Now;
                }
                Success = growerBusinessService.UpdateCreditCard
                (
                    userGuid: userGuid,
                    userCode: "",
                    creditCardGuid: UpdateCreditCardRequest.CreditCardGuid,
                    cardDescription: UpdateCreditCardRequest.CardDescription,
                    dateDeactivated: DateDeactivated 
                );


                if (!Success)
                {
                    AddMessage("The card was not updated.");
                }
            }
        }
    }
}