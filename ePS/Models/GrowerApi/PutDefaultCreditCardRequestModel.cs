﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class PutDefaultCreditCardRequestModel : RequestBase
    {
        public Guid UserGuid { get; set; }
        public Guid DefaultCreditCardGuid { get; set; }
       

        public override void Validate()
        {
            if (UserGuid == Guid.Empty)
            {
                AddMessage("User Guid is required.");
            }
            if (DefaultCreditCardGuid == Guid.Empty)
            {
                AddMessage("Default Credit Card Guid is required.");
            }
           
        }
    }
}