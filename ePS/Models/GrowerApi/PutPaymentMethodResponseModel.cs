﻿using System;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PutPaymentMethodResponseModel : ResponseBase
    {


        public PutPaymentMethodResponseModel(Guid userGuid, Guid DefaultPaymentTypeLookupGuid, PutPaymentMethodRequestModel PaymentMethodRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            PaymentMethodRequest.UserGuid = userGuid;
            PaymentMethodRequest.DefaultPaymentTypeLookupGuid = DefaultPaymentTypeLookupGuid;

            if (ValidateAuthenticationAndRequest(PaymentMethodRequest))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                Success = growerBusinessService.UpdateDefaultPaymentType
                (
                    userGuid: userGuid,
                    DefaultPaymentTypeLookupGuid: DefaultPaymentTypeLookupGuid
                );


                if (!Success)
                {
                    AddMessage("The grower was not updated.");
                }
            }
        }
    }
}