﻿using System;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using Utility;

namespace ePS.Models.GrowerApi
{
    public class PostShipToAddressResponseModel : ResponseBase
    {
        public PostShipToAddressResponseModel(Guid userGuid, PostShipToAddressRequestModel shipToAddressRequest)
            : this(userGuid, shipToAddressRequest.GrowerGuid, shipToAddressRequest)
        {
        }

        public PostShipToAddressResponseModel(Guid userGuid, Guid growerGuid, PostShipToAddressRequestModel shipToAddressRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            shipToAddressRequest.GrowerGuid = growerGuid;

            if (ValidateAuthenticationAndRequest(shipToAddressRequest))
            {
                var growerService = new GrowerService();
                Grower grower = growerService.TryGetGrowerForUser(userGuid);

                if (grower == null)
                {
                    AddMessage("A grower could not be found for the current user.");
                }
                else
                {
                    var shipToAddress = new GrowerShipToAddress
                        {
                            AddressName = shipToAddressRequest.Name,
                            StreetAddress1 = shipToAddressRequest.StreetAddress1,
                            StreetAddress2 = shipToAddressRequest.StreetAddress2,
                            City = shipToAddressRequest.City,
                            State = new State(shipToAddressRequest.StateCode),
                            ZipCode = shipToAddressRequest.ZipCode,
                            Phone = new PhoneNumber(shipToAddressRequest.PhoneNumber),
                            SpecialInstructions = shipToAddressRequest.SpecialInstructions,
                            SellerCustomerID = shipToAddressRequest.SellerCustomerID,
                            IsDefault = true
                        };

                    GrowerService.AddShipToAddressReturnData returnData =
                        growerService.AddShipToAddress
                            (
                                userGuid,
                                grower.Guid,
                                shipToAddress
                            );

                    GrowerAddressGuid = returnData.GrowerAddressGuid;
                    AddressGuid = returnData.AddressGuid;

                    if (GrowerAddressGuid == Guid.Empty || AddressGuid == Guid.Empty)
                    {
                        AddMessage("The address was not added.");
                    }
                    else
                    {
                        Success = true;
                    }
                }
            }
        }

        public Guid GrowerAddressGuid { get; set; }
        public Guid AddressGuid { get; set; }
    }
}