﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class GetAllowSubstitutionsResponseModel : ResponseBase
    {
        
       
        public GetAllowSubstitutionsResponseModel(Guid userGuid)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid( userGuid,"User Guid"))
            {
                var growerService = new GrowerService();
                Grower oGrower = growerService.TryGetGrowerForUser(userGuid);
                AllowSubstitutions = oGrower.AllowSubstitutions;
            
            }
        }

        public bool AllowSubstitutions { get; set; }
    }
}
