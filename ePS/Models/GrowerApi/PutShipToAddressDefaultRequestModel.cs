﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class PutShipToAddressDefaultRequestModel : RequestBase
    {
        public Guid AddressGuid { get; set; }
        

        public override void Validate()
        {
            if (AddressGuid == Guid.Empty)
            {
                AddMessage("AddressGuid is required.");
            }

            
        }
    }
}