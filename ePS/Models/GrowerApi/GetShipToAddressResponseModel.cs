﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.GrowerApi
{
    public class GetShipToAddressResponseModel : ResponseBase
    {
        public enum GuidTypeEnum { GrowerGuid, GrowerAddressGuid, GrowerOrderGuid,UserGuid}

        public GetShipToAddressResponseModel(Guid userGuid, Guid guid, GuidTypeEnum guidType)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(guid, guidType.ToString()))
            {
                List<GrowerShipToAddress> growerAddressList = null;

                var growerService = new GrowerService();

                string searchDescription;
                switch(guidType)
                {
                    case GuidTypeEnum.GrowerGuid:
                        growerAddressList = growerService.GetShipToAddressesForGrower(userGuid, guid);
                        searchDescription = "Grower addresses";
                        break;
                    case GuidTypeEnum.GrowerAddressGuid:
                        growerAddressList = growerService.GetShipToAddress(userGuid, guid);
                        searchDescription = "Grower address";
                        break;
                    case GuidTypeEnum.GrowerOrderGuid:
                        Grower grower = growerService.TryGetGrowerForUser(userGuid);
                        if (grower == null)
                        {
                            AddMessage("Grower not found for current user.");
                        }
                        else
                        {
                            growerAddressList = growerService.GetShipToAddressesForGrowerOrder(userGuid, grower.Guid, guid);                            
                        }
                        searchDescription = "Grower order";
                        break;
                    case GuidTypeEnum.UserGuid:
                        Grower grower2 = growerService.TryGetGrowerForUser(userGuid);
                        if (grower2 == null)
                        {
                            AddMessage("Grower not found for current user.");
                        }
                        else
                        {
                            growerAddressList = growerService.GetShipToAddressesForGrowerOrder(userGuid, grower2.Guid, guid);
                        }
                        searchDescription = "Grower order";
                        break;
                    default:
                        throw new NotImplementedException(string.Format( "Guid type {0} is not implemented.", guidType.ToString()));
                       
                }

                if (growerAddressList != null)
                {
                    var addressList = new List<ShipToAddressType>();

                    if (growerAddressList.Count == 0)
                    {
                        AddMessage(string.Format( "{0} not found.", searchDescription));
                    }
                    else
                    {
                        foreach (GrowerShipToAddress address in growerAddressList)
                        {
                            string phoneNumber = "";
                            if (address.Phone != null)
                            {
                                phoneNumber = address.Phone.UnformattedPhoneNumber;
                            }

                            addressList.Add
                                (
                                    new ShipToAddressType
                                    {
                                        GrowerAddressGuid = address.GrowerAddressGuid,
                                        AddressGuid = address.AddressGuid,
                                        Name = address.AddressName,
                                        StreetAddress1 = address.StreetAddress1,
                                        StreetAddress2 = address.StreetAddress2,
                                        City = address.City,
                                        StateCode = address.State.StateCode,
                                        Country = address.State.Country.CountryName,
                                        CountryCode = address.State.Country.CountryCode,
                                        ZipCode = address.ZipCode,
                                        PhoneNumber = phoneNumber,
                                        SpecialInstructions = address.SpecialInstructions,
                                        SellerCustomerID = address.SellerCustomerID,
                                        IsDefault = address.IsDefault,
                                        IsSelected = address.IsSelected
                                    }
                                );

                            ShipToAddressList = addressList.ToArray();

                            Success = true;
                        }
                    }
                }
            }
        }

        public ShipToAddressType[] ShipToAddressList { get; set; }
    }
}
