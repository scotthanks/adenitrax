﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SelectionMenu
{
    public class SelectionCategoryType
    {

        public string Code {get; set;}
        public string Name {get; set;}
        public string[] ActiveFormCodes { get; set; }
    }
}