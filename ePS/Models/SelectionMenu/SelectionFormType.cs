﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SelectionMenu
{
    public class SelectionFormType
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}