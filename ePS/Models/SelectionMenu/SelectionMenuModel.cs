﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BusinessObjectsLibrary;

namespace ePS.Models.SelectionMenu
{
    public class SelectionMenuModel
    {
        public SelectionMenuModel() {
            var userGuid = new Guid();
            var status = new StatusObject(userGuid,false);
            var service = new BusinessObjectServices.MenuService(status);
            var menuData = service.GetMenuData();

            var categoryList = menuData.ActiveProgramTypes;
            var formsList = menuData.ProductForms;
            
            var categoryMenuList = new List<SelectionCategoryType>();

            foreach (var category in categoryList) {
                var s = new List<string>();

                foreach (var form in category.ActiveProductForms) {
                    s.Add(form.Code);
                }

                var item = new SelectionCategoryType() {
                    Code = category.Code,
                    Name = category.Name,
                    ActiveFormCodes = s.ToArray()
                };
                categoryMenuList.Add(item);
            };
            this.CategoriesMenu = categoryMenuList;

            var formMenuList = new List<SelectionFormType>();
            foreach (var form in formsList)
            {
                var item = new SelectionFormType() {
                    Code = form.Code,
                    Name = form.Name
                };
                formMenuList.Add(item);
            };

            this.FormsMenu = formMenuList;
        }

        public IEnumerable<SelectionCategoryType> CategoriesMenu { get; set; }
        public IEnumerable<SelectionFormType> FormsMenu { get; set; }
    }
}