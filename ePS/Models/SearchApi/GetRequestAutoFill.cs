﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SearchApi
{
    public class GetRequestAutoFill : RequestBase
    {

        public enum ScopeEnum
        {
            OpenSearch,
            CatalogPage,
            AvailabilityPage
        }


        public string Scope { get; set; }
        public string Stem { get; set; }

        public override void Validate()
        {
            ScopeEnum requestType;

            bool pageIsValid = Enum.TryParse(Scope, ignoreCase: true, result: out requestType);
            if (!pageIsValid)
            {
                AddMessage(string.Format("Scope: {0} - not valid.", Scope));
            }

            Stem = Stem ?? string.Empty;
            Stem = Stem.Trim();
            bool stemIsValid = Stem.Length > 0;
            if (!stemIsValid)
            {
                AddMessage(string.Format("Stem: {0} - not valid.", Scope));
            }
        }


    }
}