﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;

namespace ePS.Models.SearchApi
{
    public class GetResponseAutoFill : ResponseBase
    {
        public GetResponseAutoFill(Guid userGuid, GetRequestAutoFill request)
            : base(userGuid, requiresAuthentication: false)
        {
            if (ValidateAuthenticationAndRequest(request))
            {

                var status = new StatusObject(userGuid, true);
                var service = new SearchService(status);

                
                //This is the search autofill code
                
                //ToDo: Scott I have rivised this with the AutoFillType
                string[] searchValues = service.GetAutoFillValues(request.Stem);

                var autoFillList = new List<AutoFillType>();
                if (searchValues != null)
                { 
                    foreach (string value in searchValues)
                    {
                        autoFillList.Add(new AutoFillType() 
                        {
                            DisplayValue = value,
                            //DisplayValue = string.Format("{0} - some text - {0}", value),
                            SearchValue = value
                        });
                    } 
  
                };

                AutoFillList = autoFillList;


                
                Success = true;
               

            }
        }
       
        public IEnumerable<AutoFillType> AutoFillList { get; set; }
    }
}