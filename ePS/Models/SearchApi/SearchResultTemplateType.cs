﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SearchApi
{
    public class SearchResultTemplateType
    {

        public Guid TemplateGuid { get; set; }
        public string Css { get; set; }
        public string Html { get; set; }
        public string Javascript { get; set; }

    }
}