﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace ePS.Models.SearchApi
{
    public class SearchResultRow
    {
        public Guid RowTypeTemplateGuid { get; set; }
        public string ResultType { get; set; }
        public string ResultCode { get; set; }
        public string ResultName { get; set; }
        public int SortOrder { get; set; }
        public int PageNumber { get; set; }
        public string ImageURI { get; set; }
        public string HeaderText { get; set; }
        public string SubHeaderText { get; set; }
        public string CatalogProgramTypeCode { get; set; }
        public string CatalogProductFormCategoryCode { get; set; }
        public string Program1Name { get; set; }
        public string Program1ProgramTypeCode { get; set; }
        public string Program1ProductFormCategoryCode { get; set; }
        public int Program1ProductCount { get; set; }
        public string Program2Name { get; set; }
        public string Program2ProgramTypeCode { get; set; }
        public string Program2ProductFormCategoryCode { get; set; }
        public int Program2ProductCount { get; set; }
        public string Program3Name { get; set; }
        public string Program3ProgramTypeCode { get; set; }
        public string Program3ProductFormCategoryCode { get; set; }
        public int Program3ProductCount { get; set; }
        public string Species1Code { get; set; }
        public string Species1Name { get; set; }
        public string Species1ProgramTypeCode { get; set; }
        public string Species1ProductFormCategoryCode { get; set; }
        public string Species2Code { get; set; }
        public string Species2Name { get; set; }
        public string Species2ProgramTypeCode { get; set; }
        public string Species2ProductFormCategoryCode { get; set; }
        public string Species3Code { get; set; }
        public string Species3Name { get; set; }
        public string Species3ProgramTypeCode { get; set; }
        public string Species3ProductFormCategoryCode { get; set; }
        public string Program1AvailabilityWeek { get; set; }
        public string Program2AvailabilityWeek { get; set; }
        public string Program3AvailabilityWeek { get; set; }

    }
}