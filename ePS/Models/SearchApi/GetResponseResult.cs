﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;


namespace ePS.Models.SearchApi
{
    public class GetResponseResult : ResponseBase
    {
        public GetResponseResult(Guid userGuid, GetRequestResult request) : base(userGuid, requiresAuthentication: false)
        {
            if (ValidateAuthenticationAndRequest(request))
            {


                //-----------------------------------
                //This is the actual Search Code, you can change the expected result type if you want.

                var status = new StatusObject(userGuid, true);
                var service = new SearchService(status);

           
                var returnData = service.RunSearch(request.Term, true);       
                 // string[] theArray = returnData.HTMLResult.Select(i => i.ToString()).ToArray();
                var oSearchResult = new SearchResultType();
                oSearchResult.Guid = returnData.Guid;

                oSearchResult.Page = returnData.Page;
                oSearchResult.RelatedSearches = returnData.RelatedSearches;
                if (returnData.ResultCount == 0)
                {
                    oSearchResult.ResultCount = returnData.ResultRows.Count();
                }
                else
                { 
                    oSearchResult.ResultCount = returnData.ResultCount; 
                }
                oSearchResult.SearchString = returnData.SearchString;
                oSearchResult.ResultRows = new List<SearchResultRow>();
                foreach (var oRow in returnData.ResultRows)
                {
                    var oResultRow = new SearchResultRow();
                    oResultRow.RowTypeTemplateGuid = oRow.RowTypeTemplateGuid;
                    oResultRow.ResultType = oRow.ResultType;
                    oResultRow.PageNumber = oRow.PageNumber;
                    oResultRow.SortOrder = oRow.SortOrder;
                    oResultRow.ResultCode = oRow.ResultCode;
                    oResultRow.CatalogProductFormCategoryCode = oRow.CatalogProductFormCategoryCode;
                    oResultRow.CatalogProgramTypeCode = oRow.CatalogProgramTypeCode;
                    oResultRow.HeaderText = oRow.HeaderText;
                    oResultRow.SubHeaderText = oRow.SubHeaderText;
                    oResultRow.ImageURI = oRow.ImageURI;
                    oResultRow.Program1Name = oRow.Program1Name;
                    oResultRow.Program1ProductFormCategoryCode = oRow.Program1ProductFormCategoryCode;
                    oResultRow.Program1ProgramTypeCode = oRow.Program1ProgramTypeCode;
                    oResultRow.Program1ProductCount = oRow.Program1ProductCount;
                    oResultRow.Program2Name = oRow.Program2Name;
                    oResultRow.Program2ProductFormCategoryCode = oRow.Program2ProductFormCategoryCode;
                    oResultRow.Program2ProgramTypeCode = oRow.Program2ProgramTypeCode;
                    oResultRow.Program2ProductCount = oRow.Program2ProductCount;
                    oResultRow.Program3Name = oRow.Program3Name;
                    oResultRow.Program3ProductFormCategoryCode = oRow.Program3ProductFormCategoryCode;
                    oResultRow.Program3ProgramTypeCode = oRow.Program3ProgramTypeCode;
                    oResultRow.Program3ProductCount = oRow.Program3ProductCount;
                    oResultRow.ResultName = oRow.ResultName;
                    oResultRow.Species1Code = oRow.Species1Code;
                    oResultRow.Species1Name = oRow.Species1Name;
                    oResultRow.Species1ProductFormCategoryCode = oRow.Species1ProductFormCategoryCode;
                    oResultRow.Species1ProgramTypeCode = oRow.Species1ProgramTypeCode;
                    oResultRow.Species2Code = oRow.Species2Code;
                    oResultRow.Species2Name = oRow.Species2Name;
                    oResultRow.Species2ProductFormCategoryCode = oRow.Species2ProductFormCategoryCode;
                    oResultRow.Species2ProgramTypeCode = oRow.Species2ProgramTypeCode;
                    oResultRow.Species3Code = oRow.Species3Code;
                    oResultRow.Species3Name = oRow.Species3Name;
                    oResultRow.Species3ProductFormCategoryCode = oRow.Species3ProductFormCategoryCode;
                    oResultRow.Species3ProgramTypeCode = oRow.Species3ProgramTypeCode;
                    oResultRow.Program1AvailabilityWeek = oRow.Program1AvailabilityWeek;
                    oResultRow.Program2AvailabilityWeek = oRow.Program2AvailabilityWeek;
                    oResultRow.Program3AvailabilityWeek = oRow.Program3AvailabilityWeek;
                            



                    oSearchResult.ResultRows.Add(oResultRow);
                }
                
                SearchResult = oSearchResult;
                Success = true;
            }
        }

        public SearchResultType SearchResult { get; set; }

        
    }
}