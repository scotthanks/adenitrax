﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SearchApi
{
    public class AutoFillType
    {
        public string SearchValue { get; set; }
        public string DisplayValue { get; set; }

    }
}