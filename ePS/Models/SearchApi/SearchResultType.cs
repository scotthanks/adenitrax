﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace ePS.Models.SearchApi
{
    public class SearchResultType
    {
        public Guid Guid { get; set; }
        public string SearchString { get; set; }
        public int ResultCount { get; set; }
        public int Page { get; set; }
        public string RelatedSearches { get; set; }
        public List<SearchResultRow> ResultRows { get; set; }
    }
}