﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ReportedAvailability
{
    public class PostReportedAvailabilityRequestModel : RequestBase
    {

        public Guid ProductGuid { get; set; }
        public Guid ProductFormGuid { get; set; }
        public Guid ShipWeekGuid { get; set; }
        public int Quantity { get; set; }
        public int Sales { get; set; }
        public Guid AvailabilityTypeLookupGuid { get; set; }

        public override void Validate()
        {
        }

    }
}