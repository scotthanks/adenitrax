﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ReportedAvailability
{
    public class PostReportedAvailabilitySalesResponseModel : ResponseBase
    {

        public PostReportedAvailabilitySalesResponseModel(Guid userGuid, PostReportedAvailabilityRequestModel reportedAvailability)
            : base(userGuid, requiresAuthentication: true)
        {

            BusinessObjectServices.ReportedAvailabilityService service = new BusinessObjectServices.ReportedAvailabilityService();
            DataObjectLibrary.ReportedAvailability availabilityUpdated = service.UpdateSalesSinceDateReported(
                reportedAvailability.ProductGuid, reportedAvailability.ProductFormGuid, reportedAvailability.ShipWeekGuid, reportedAvailability.Sales );

        }

    }
}