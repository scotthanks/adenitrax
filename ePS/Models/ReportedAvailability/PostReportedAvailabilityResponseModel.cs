﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ReportedAvailability
{
    public class PostReportedAvailabilityResponseModel : ResponseBase
    {

        public PostReportedAvailabilityResponseModel(Guid userGuid, PostReportedAvailabilityRequestModel reportedAvailability)
            : base(userGuid, requiresAuthentication: true)
        {

            BusinessObjectServices.ReportedAvailabilityService service = new BusinessObjectServices.ReportedAvailabilityService();
            DataObjectLibrary.ReportedAvailability availabilityUpdated = service.UpdateReportedAvailability(reportedAvailability.ProductGuid, reportedAvailability.ShipWeekGuid, reportedAvailability.Quantity);



        }



    }
}