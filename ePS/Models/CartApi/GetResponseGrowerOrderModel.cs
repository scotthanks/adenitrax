﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BusinessObjectsLibrary;
using ePS.Types;
using AdminAppApiLibrary;
using DataServiceLibrary;

namespace ePS.Models.CartApi
{
    public class GetResponseGrowerOrderModel
    {
        public GetResponseGrowerOrderModel(GrowerOrder growerOrder)
        {
            GrowerOrder = new GrowerOrderType
                {
                    GrowerCode = growerOrder.Grower.GrowerCode,
                    GrowerName = growerOrder.Grower.GrowerName,
                    ShipWeekCode = growerOrder.ShipWeek.Code,
                    GrowerPhone = growerOrder.Grower.PhoneNumber.FormattedPhoneNumber,
                    GrowerEmail = growerOrder.Grower.EMail,
                    GrowerAdditionalAccountingEmail = growerOrder.Grower.AdditionalAccountingEmail,
                    GrowerAdditionalOrderEmail = growerOrder.Grower.AdditionalOrderEmail,
                    GrowerAllowsSubstitutions = growerOrder.Grower.AllowSubstitutions,

                    OrderGuid = growerOrder.OrderGuid,
                    OrderNo = growerOrder.OrderNo,
                    CustomerPoNo = growerOrder.CustomerPoNo,
                    OrderDescription = growerOrder.OrderDescription,
                    PromotionalCode = growerOrder.PromotionalCode,
                    //DateEntered = growerOrder.DateEntered,
                    DateEnteredString = growerOrder.DateEntered.ToString(CultureInfo.InvariantCulture),

                    OrderType = growerOrder.OrderType,
                    ProgramTypeCode = growerOrder.ProgramTypeCode,
                    ProgramTypeName = growerOrder.ProgramTypeName,
                    ProductFormCategoryCode = growerOrder.ProductFormCategoryCode,
                    ProductFormCategoryName = growerOrder.ProductFormCategoryName,
                    NavigationProgramTypeCode = growerOrder.NavigationProgramTypeCode,
                    NavigationProductFormCategoryCode = growerOrder.NavigationProductFormCategoryCode,

                    GrowerOrderStatusCode = growerOrder.GrowerOrderStatus.Code,

                    PersonWhoPlacedOrder = new GetResponsePersonModel(growerOrder.PersonWhoPlacedOrder).PersonType,

                    SupplierOrders =
                        growerOrder.SupplierOrders.Select(
                            supplierOrder => new GetResponseSupplierOrderModel(supplierOrder).SupplierOrder).ToArray(),
                };

            var paymentTypeList = new List<SelectListItemType>();
            foreach (var paymentTypeBusinessObject in growerOrder.Grower.DefaultPaymentType.SiblingList)
            {
                var paymentType = new GetResponseSelectListItemModel(paymentTypeBusinessObject);

                if (paymentTypeBusinessObject.Guid == growerOrder.Grower.DefaultPaymentType.Guid)
                    paymentType.IsDefault = true;

                if (paymentTypeBusinessObject.Guid == growerOrder.PaymentType.Guid)
                    paymentType.IsSelected = true;

                paymentTypeList.Add(paymentType.SelectListItem);
            }
            GrowerOrder.PaymentTypeList = paymentTypeList.ToArray();

            var creditCardList = new List<CreditCardType>();
            foreach (var creditCardBusinessObject in growerOrder.Grower.CreditCardList)
            {
                var creditCard = new GetResponseCreditCardModel(creditCardBusinessObject).CreditCard;
                creditCardList.Add(creditCard);

                //TODO: There is no default credit card stored in the system.
                creditCard.IsDefault = false;

                if (creditCard.Guid == growerOrder.PaymentCreditCard.Guid)
                {
                    creditCard.IsSelected = true;
                }
            }
            GrowerOrder.CreditCards = creditCardList.ToArray();

            List<GrowerOrderFeeType> feeList = new List<GrowerOrderFeeType>();
            foreach (var orderFee in growerOrder.GrowerOrderFeeList)
            {
                GrowerOrderFeeType fee = new GrowerOrderFeeType();
                fee.Amount = orderFee.Amount;
                fee.GrowerOrderFeeTypeCode = orderFee.GrowerOrderFeeType.Code;
                fee.GrowerOrderGuid = growerOrder.OrderGuid;
                fee.Guid = orderFee.Guid;
                DataObjectLibrary.Lookup LookupValue = LookupTableService.SingletonInstance.GetByGuid(orderFee.GrowerOrderFeeType.Guid);
                fee.GrowerOrderFeeTypeDescription = LookupValue.Name;
                fee.GrowerOrderFeeTypeCode = LookupValue.Code;

                feeList.Add(fee);
            }
            GrowerOrder.GrowerOrderFeeList = feeList.ToArray();

            var shipToAddressList = new List<ShipToAddressType>();
            bool defaultShipToSelected = false;
            ShipToAddressType alternateDefaultShipTo = null;
            foreach (var shipToAddressBusinessObject in growerOrder.Grower.ShipToAddressList)
            {
                var shipToAddress = new GetResponseShipToAddressModel(shipToAddressBusinessObject).ShipToAddress;
                shipToAddressList.Add(shipToAddress);

                if (shipToAddress.IsDefault)
                {
                    defaultShipToSelected = true;                    
                }

                if (shipToAddress.AddressGuid == growerOrder.Grower.Address.AddressGuid)
                {
                    alternateDefaultShipTo = shipToAddress;
                }

                if (shipToAddress.GrowerAddressGuid == growerOrder.ShipToAddressGuid)
                {
                    shipToAddress.IsSelected = true;
                }
            }

            if (!defaultShipToSelected && alternateDefaultShipTo != null)
            {
                alternateDefaultShipTo.IsDefault = true;
            }

            GrowerOrder.ShipToAddresses = shipToAddressList.ToArray();

            var logList = new List<LogType>();
            foreach (var log in growerOrder.GrowerOrderHistoryEvents)
            {
                logList.Add(new LogType()
                {
                    Guid = log.Guid,
                    //LogTime = log.EventTime,
                    LogTimeString = log.EventTime.ToString("g", CultureInfo.CreateSpecificCulture("en-us")),
                    Text = log.Comment,
                    LogTypeLookupCode = log.LogTypeLookupCode,
                    IsInternal = log.IsInternal,
                    PersonName = log.PersonName,
                    UserCode = log.UserCode
                });
            }
            GrowerOrder.Logs = logList.ToArray();
        }

        public GrowerOrderType GrowerOrder { get; private set; }
    }
}