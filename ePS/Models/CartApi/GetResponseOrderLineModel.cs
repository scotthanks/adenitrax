﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

using BusinessObjectsLibrary;
using System.Globalization;

namespace ePS.Models.CartApi
{
    public class GetResponseOrderLineModel
    {
        public GetResponseOrderLineModel(BusinessObjectsLibrary.OrderLine orderLine)
        {
            ////TODO: Implement these for real. (Dan).
            //bool isLockedForReduction = Utility.Random.RandomBool;
            //bool isLockedForAllChanges = Utility.Random.RandomBool;

            ////If it's locked for all changes, reduction is not valid either.
            //if (isLockedForAllChanges)
            //{
            //    isLockedForReduction = true;
            //}

            OrderLine = new OrderLineType()
                {
                    Guid = orderLine.Guid,
                    LineNumber = orderLine.LineNumber,
                    LineComment = orderLine.LineComment,
                    OrderLineID= orderLine.OrderLineID,
                    ProductID = orderLine.ProductID,
                    SpeciesCode = orderLine.Product.SpeciesCode,
                    SpeciesName = orderLine.Product.SpeciesName,
                    VarietyCode = orderLine.Product.VarietyCode,
                    VarietyName = orderLine.Product.VarietyName,

                    ProductCode = orderLine.Product.Code,
                    ProductDescription = orderLine.Product.ProductDescription,
                    SupplierProductIdentifier = orderLine.Product.SupplierProductIdentifier,
                    SupplierProductDescription = orderLine.Product.SupplierProductDescription,
                    ProductFormName = orderLine.Product.ProductFormName,

                    Multiple = orderLine.Product.SalesUnitQty,
                    Minumum = orderLine.Product.LineItemMinumumQty,
                    Price = orderLine.Price,
                    Cost = orderLine.Cost,

                    QuantityOrdered = orderLine.QuantityOrdered,
                    QuantityShipped = orderLine.QuantityShipped,
                    IsCarted = orderLine.IsCarted,

                    AvailableQty = orderLine.AvailableQty,
                    AvailabilityTypeCode = orderLine.AvailabilityTypeCode,

                    OrderLineStatusCode = orderLine.OrderLineStatus.Code,
                    
                    IsLockedForReduction = orderLine.IsLockedForReduction,
                    IsLockedForAllChanges = orderLine.IsLockedForAllChanges,
                    DateEnteredString = orderLine.DateEnteredString,
                    DateChangedString = orderLine.DateChangedString,
                    DateQtyChangedString = orderLine.DateQtyChangedString,
                };

            var logList = new List<LogType>();
            foreach (var log in orderLine.GrowerOrderHistoryEvents)
            {
                logList.Add(new LogType()
                    {
                        Guid = log.Guid,
                        //LogTime = log.EventTime,
                        LogTimeString = log.EventTime.ToString("g", CultureInfo.CreateSpecificCulture("en-us")),
                        Text = log.Comment,
                        LogTypeLookupCode = log.LogTypeLookupCode,
                        IsInternal = log.IsInternal,
                        PersonName = log.PersonName,
                        UserCode = log.UserCode
                    });
            }
            OrderLine.Logs = logList.ToArray();
        }

        public OrderLineType OrderLine { get; set; }
    }
}