﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.CartApi
{
    public class PostRequestModel : RequestBase
    {
        public IEnumerable<CartOrderLineUpdate> CartOrderLineUpdates { get; set; }

        public override void Validate()
        {
            if (!CartOrderLineUpdates.Any())
            {
                AddMessage("There must be at least one update specified.");
            }
            else
            {
                int row = 0;
                foreach (var update in CartOrderLineUpdates)
                {
                    if (update.Guid == Guid.Empty)
                    {
                        AddMessage(string.Format("The Guid (OrderLineGuid) is required on each update. Row {0} has an empty guid.", row));
                    }

                    if (update.Quantity < 0)
                    {
                        AddMessage(string.Format("The quantity ({0}) on row {1} is less than 0.", update.Quantity, row));
                    }

                    row++;
                }
            }
        }
    }

    public class CartOrderLineUpdate
    {
        public Guid GrowerOrderGuid { get; set; } // Optional (improves performance).
        //TODO: This is the OrderLineGuid; change the name to make it clearer!
        public Guid Guid { get; set; }
        public int Quantity { get; set; }
        public int OriginalQuantity { get; set; } // Optional - improves performance.
        public string ProductDescription { get; set; } // Optional - improves performance.
        public decimal Price { get; set; } // Optional - improves performance.
    }


}