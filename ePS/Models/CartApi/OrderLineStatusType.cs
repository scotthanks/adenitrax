﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.CartApi
{
    public class OrderLineStatusType
    {
        public Guid OrderLineGuid { get; set; }
        public bool Success { get; set; }
        public int Quantity { get; set; }
        public string Message { get; set; }
    }
}