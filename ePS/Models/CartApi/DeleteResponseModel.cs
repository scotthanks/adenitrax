﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;


namespace ePS.Models.CartApi
{
    public class DeleteResponseModel : ResponseBase
    {
        public DeleteResponseModel(Guid userGuid, string userCode, Guid growerOrderGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(growerOrderGuid, "GrowerOrderGuid"))
            {
                this.OrderGuid = growerOrderGuid;

                var service = new GrowerOrderService();

                var returnData = service.DeleteCartOrder(userGuid, userCode, growerOrderGuid);

                OrderLinesDeleted = returnData.OrderLinesDeleted;

                this.Success = OrderLinesDeleted > 0;

                this.Success = true;                
            }
        }

        public Guid OrderGuid { get; set; }
        public int OrderLinesDeleted { get; set; }
    }
}