﻿using System;
using System.Collections.Generic;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;



namespace ePS.Models.CartApi
{
    public class GetPromoCodeResponseModel : ResponseBase
    {

        public string PromoName { get; set; }
        public string PromoCode { get; set; }


        public GetPromoCodeResponseModel(Guid userGuid, GetPromoCodeRequestModel request)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(userGuid, "User Guid"))
            {
               
                

                var service = new GrowerService();
                var promoName = service.GetPromoName(userGuid, request.PromoCode);

                PromoCode = request.PromoCode;
                PromoName = promoName;
                Success = true;
            }
        }

   
    }
}
