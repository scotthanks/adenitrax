﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.CartApi
{
    public class GetResponseDetailModel : ResponseBase
    {
        public GetResponseDetailModel(Guid userGuid, GetRequestDetailModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                if (request.RequestType != "DetailData")
                {
                    AddMessage("Expected \"DetailData\" request type.");
                }
                else
                {
                    var service = new BusinessObjectServices.GrowerOrderService();
                    var growerOrder = service.GetGrowerOrderDetail(request.OrderGuid, includePriceData: false, includePreCart: false);

                    var responseModel = new GetResponseGrowerOrderModel(growerOrder);

                    this.GrowerOrder = responseModel.GrowerOrder;

                    if (GrowerOrder == null)
                    {
                        AddMessage("The GrowerOrder was not found.");
                        Success = true;
                    }                    
                }
            }
        }

        public GrowerOrderType GrowerOrder { get; private set; }
    }

}