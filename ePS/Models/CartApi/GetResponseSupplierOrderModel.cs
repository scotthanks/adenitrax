﻿using System.Collections.Generic;
using System.Linq;
using BusinessObjectsLibrary;
using ePS.Types;
using System.Globalization;
using System;
using AdminAppApiLibrary;

namespace ePS.Models.CartApi
{
    public class GetResponseSupplierOrderModel
    {
        public GetResponseSupplierOrderModel(SupplierOrder supplierOrder)
        {
            supplierOrder.TagRatioList.Sort();
            supplierOrder.ShipMethodList.Sort();

            string expectedDeliveryDateStr;
            if (supplierOrder.ExpectedDeliveryDate == DateTime.MinValue)
            {
                expectedDeliveryDateStr = "not defined";
            }
            else
            {
                expectedDeliveryDateStr = supplierOrder.ExpectedDeliveryDate.ToShortDateString();
            }

            var orderLineTypeList = supplierOrder.OrderLines.Select(orderLine => new GetResponseOrderLineModel(orderLine).OrderLine).ToList();
            orderLineTypeList.Sort();

            SupplierOrder = new SupplierOrderType
                {
                    SupplierOrderGuid = supplierOrder.SupplierOrderGuid,

                    SupplierOrderNo = supplierOrder.SupplierOrderNo,
                    SupplierOrderStatusCode = supplierOrder.SupplierOrderStatus.Code,

                    SupplierCode = supplierOrder.Supplier.Code,
                    SupplierName = supplierOrder.Supplier.Name,
                    SupplierPhone = supplierOrder.Supplier.Phone,
                    SupplierEmail = supplierOrder.Supplier.Email,
                    LastSentToSupplierDate = supplierOrder.LastSentToSupplierDate.ToString(),

                    TrackingNumber = supplierOrder.TrackingNumber,
                    TrackingComment = supplierOrder.TrackingComment,
                    ExpectedDeliveryDate = expectedDeliveryDateStr,

                    OrderLines = orderLineTypeList.ToArray(),
                };

            List<SupplierOrderFeeType> feeList = new List<SupplierOrderFeeType>();
            foreach(var fee in supplierOrder.SupplierOrderFeeList)
            {
                SupplierOrderFeeType orderFee = new SupplierOrderFeeType();
                orderFee.Amount = fee.Amount;
                orderFee.Guid = fee.Guid;
                orderFee.SupplierOrderFeeTypeCode = fee.SupplierOrderFeeType.Code;
                orderFee.SupplierOrderFeeTypeDescription = fee.SupplierOrderFeeType.Description;
                orderFee.SupplierOrderGuid = fee.SupplierOrderGuid;
                orderFee.SupplierOrderFeeStatusCode = fee.SupplierOrderFeeStatus.Code;
                orderFee.SupplierOrderFeeStatusDescription = fee.SupplierOrderFeeStatus.Description;
                orderFee.FeeDescription = fee.FeeDescription; 

                feeList.Add(orderFee);
            }
            SupplierOrder.SupplierOrderFeeList = feeList.ToArray();

            var shipMethodList = new List<SelectListItemType>();
            foreach (var shipMethodBusinessObject in supplierOrder.ShipMethodList)
            {
                var shipMethod = new GetResponseSelectListItemModel(shipMethodBusinessObject).SelectListItem;
                shipMethodList.Add(shipMethod);

                if (shipMethod.Code == supplierOrder.ShipMethodDefault.Code)
                {
                    shipMethod.IsDefault = true;
                }

                if (shipMethod.Code == supplierOrder.ShipMethod.Code)
                {
                    shipMethod.IsSelected = true;
                }
            }
            SupplierOrder.ShipMethodList = shipMethodList.ToArray();

            var tagRatioList = new List<SelectListItemType>();
            foreach (var tagRatioBusinessObject in supplierOrder.TagRatioList)
            {
                var tagRatio = new GetResponseSelectListItemModel(tagRatioBusinessObject).SelectListItem;
                tagRatioList.Add(tagRatio);

                if (tagRatio.Code == supplierOrder.TagRatioDefault.Code)
                {
                    tagRatio.IsDefault = true;
                }

                if (tagRatio.Code == supplierOrder.TagRatio.Code)
                {
                    tagRatio.IsSelected = true;
                }
            }
            SupplierOrder.TagRatioList = tagRatioList.ToArray();

            var logList = new List<LogType>();
            foreach (var log in supplierOrder.GrowerOrderHistoryEvents)
            {
                logList.Add(new LogType()
                {
                    Guid = log.Guid,
                    //LogTime = log.EventTime,
                    LogTimeString = log.EventTime.ToString("g", CultureInfo.CreateSpecificCulture("en-us")),
                    Text = log.Comment,
                    LogTypeLookupCode = log.LogTypeLookupCode,
                    IsInternal = log.IsInternal,
                    PersonName = log.PersonName,
                    UserCode  = log.UserCode
                });
            }
            SupplierOrder.Logs = logList.ToArray();
        }

        public SupplierOrderType SupplierOrder { get; set; }
    }
}