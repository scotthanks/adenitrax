﻿using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SupplierOrderFeeApi
{
    public class DeleteResponseModel : ResponseBase
    {
        public DeleteResponseModel(Guid userGuid, Guid supplierOrderFeeGuid)
            : base( userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndGuid(supplierOrderFeeGuid, "SupplierOrderFeeGuid"))
            {
                BusinessObjectServices.GrowerOrderService growerOrderService = new BusinessObjectServices.GrowerOrderService();
                Success = growerOrderService.DeleteSupplierFee(supplierOrderFeeGuid);
            }
        }        
    }
}