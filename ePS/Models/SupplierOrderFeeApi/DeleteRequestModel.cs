﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SupplierOrderFeeApi
{
    public class DeleteRequestModel
    {
        public Guid supplierOrderFeeGuid { get; set; }
    }
}