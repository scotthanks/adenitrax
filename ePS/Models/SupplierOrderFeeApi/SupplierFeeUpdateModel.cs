﻿using AdminAppApiLibrary;
using ePS.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.SupplierOrderFeeApi
{
    public class SupplierFeeUpdateModel : ResponseBase
    {
        public SupplierFeeUpdateModel(Guid userGuid, SupplierOrderFeeType request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new AdminAppResponseBase();
            try
            {
                BusinessObjectServices.GrowerOrderService growerOrderService = new BusinessObjectServices.GrowerOrderService();
                growerOrderService.FullUpdateSupplierFee(request.Guid, request.Amount, request.FeeDescription, 
                    request.SupplierOrderFeeStatusCode, request.SupplierOrderFeeTypeCode);
                Response.Success = true;

            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
        }

        public AdminAppResponseBase Response;
    }
}