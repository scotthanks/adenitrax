﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using BusinessObjectServices;
using AdminAppApiLibrary;

namespace ePS.Models.SupplierOrderFeeApi
{
    public class PostResponseModel : ResponseBase
    {
        public SupplierOrderFeeType SupplierOrderFeeType { get; set; }

        public PostResponseModel(Guid userGuid, PostRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                BusinessObjectServices.GrowerOrderService growerOrderService = new BusinessObjectServices.GrowerOrderService();

                var supplierFee = growerOrderService.AddSupplierFee(request.SupplierOrderGuid, request.SupplierOrderFeeTypeCode, request.Amount, request.FeeDescription, request.SupplierOrderFeeStatusCode);

                this.SupplierOrderFeeType = new SupplierOrderFeeType();
                SupplierOrderFeeType.Guid = supplierFee.Guid;
                SupplierOrderFeeType.SupplierOrderGuid = supplierFee.SupplierOrderGuid;
                SupplierOrderFeeType.SupplierOrderFeeTypeCode = supplierFee.SupplierOrderFeeType.Code;
                SupplierOrderFeeType.SupplierOrderFeeTypeDescription = supplierFee.SupplierOrderFeeType.Description;
                SupplierOrderFeeType.Amount = supplierFee.Amount;
                SupplierOrderFeeType.FeeDescription = supplierFee.FeeDescription;

                Success = true;                
            }
        }
    }
}