﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePS.Models
{
    public class ProductDetailType
    {
            public Guid Guid { get; set; }
            public string SupplierName { get; set; }
            public string Product { get; set; }
            public string ProductForm { get; set; }
            public int OrderQuantity { get; set; }
            public int[] Availability { get; set; }
    }
}
