﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ShipWeekApi
{
    public class GetShipWeekRequestModel
    {

        public int ShipWeek { get; set; }
        public int ShipYear { get; set; }
    }
}