﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ShipWeekApi
{
    public class GetRequestModel
    {
        public int ShipWeekBegin { get; set; }
        public int ShipYearBegin { get; set; }
        public int ShipWeekEnd { get; set; }
        public int ShipYearEnd { get; set; }
    }
}