﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ShipWeekApi
{
    public class GetCurrentShipWeekResponseModel : ResponseBase
    {

        public GetCurrentShipWeekResponseModel(Guid userGuid)
         :base(userGuid, requiresAuthentication: true)
        {
            var shipWeekService = new BusinessObjectServices.ShipWeekService();
            DateTime monday = shipWeekService.ReturnMondayDate(DateTime.Today);

            var currentShipWeek = shipWeekService.ReturnShipWeekTypeFromDate(monday);
            
            var list = new List<ePS.Types.ShipWeekType>();
            ePS.Types.ShipWeekType shipWeek = new ePS.Types.ShipWeekType();

            shipWeek.Monday = currentShipWeek.MondayDate.ToShortDateString();
            shipWeek.Week = currentShipWeek.Week;
            shipWeek.Year = currentShipWeek.Year;

            list.Add(shipWeek);

            this.ShipWeeks = list;

        }

        public List<ePS.Types.ShipWeekType> ShipWeeks { get; set; }
    }
}