﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace ePS.Models.AvailabilityApi
{
    public class GetCategoryAndFormNamesRequestModel : RequestBase
    {
        public string CategoryCode { get; set; }
        public string FormCode { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(CategoryCode))
            {
                AddMessage("No CategoryCode submitted.");
            }
            if (string.IsNullOrEmpty(FormCode))
            {
                AddMessage("No FormCode submitted.");
            }
        }

    }
}