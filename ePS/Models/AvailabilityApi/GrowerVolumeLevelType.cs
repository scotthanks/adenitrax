﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AvailabilityApi
{
    public class GrowerVolumeLevelType
    {
        public Guid SupplierGuid { get; set; }
        public string SuppierCode { get; set; }

        public string SupplierName { get; set; }
        public int VolumeLevel { get; set; }
        public int RangeLow { get; set; }
        public int RangeHigh { get; set; }
        public string RangeType { get; set; }
        public string CurrentLevelType { get; set; }

    }
}