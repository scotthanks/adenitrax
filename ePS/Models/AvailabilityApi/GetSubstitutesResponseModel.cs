﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using ePS.Types;
using BusinessObjectsLibrary;

namespace ePS.Models.AvailabilityApi
{
    public class GetSubstitutesResponseModel : ResponseBase
    {
        public GetSubstitutesResponseModel(Guid userGuid, GetSubstitutesRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                var shipWeekService = new ShipWeekService();
                var selectedShipWeek = shipWeekService.ParseShipWeekString(request.ShipWeek);

                
                var status = new StatusObject(userGuid, true);
                var service = new AvailabilityService(status);
                var availabilityOfSubstitutes =
                    service.GetAvailabilityOfSubstitutes
                    (
                        userCode: request.UserCode,
                        orderLineGuid: request.OrderLineGuid,
                        speciesCode: request.SpeciesCode
                    );

                var list = new List<ProductAvailabilityDataType>();

                if (availabilityOfSubstitutes.ProductList.Count == 0)
                {
                    AddMessage("There are no substitutes available.");
                }
                else
                {
                    foreach (var productData in availabilityOfSubstitutes.ProductList)
                    {
                        var availabilityList = new int[productData.SelectedAvailability.Count];
                        var displayCodes = new string[productData.SelectedAvailability.Count];
                        int i = 0;
                        foreach (var quantityData in productData.SelectedAvailability)
                        {
                            availabilityList[i] = quantityData.Quantity;

                            string availabilityType = quantityData.AvailabilityType;
                            displayCodes[i] = availabilityType == "AVAIL" ? "" : availabilityType;

                            i++;
                        }

                        var type = new ProductAvailabilityDataType()
                        {
                            ProductGuid = productData.Guid,
                            Supplier = productData.SupplierName,
                            SpeciesCode = productData.SpeciesCode,
                            Species = productData.SpeciesName,
                            Product = productData.ProductDescription,
                            ProductCode = productData.Code,
                            VarietyCode = productData.VarietyCode,
                            Form = productData.ProductFormName,
                            Min = productData.LineItemMinumumQty,
                            Mult = productData.SalesUnitQty,
                            IsOrganic = productData.IsOrganic,
                            Price = productData.Price,
                            IncludesDelivery = productData.IncludesDelivery,
                            OrderQuantity = productData.OrderLine == null ? 0 : productData.OrderLine.QuantityOrdered,
                            IsCarted = productData.OrderLine != null && (productData.OrderLine.IsCarted != null && (bool)productData.OrderLine.IsCarted),
                            Availabilities = availabilityList,
                            DisplayCodes = displayCodes
                        };

                        if (type.OrderQuantity != 0)
                        {
                            type.OrderQuantity = type.OrderQuantity;
                        }

                        list.Add(type);
                    }                    
                }

                this.RowData = list;

                Success = true;
            }
        }

        public IEnumerable<ProductAvailabilityDataType> RowData  { get; set; }
    }
}