﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AvailabilityApi
{
    public class PostRequestModel : RequestBase
    {
        [Obsolete]
        public string UserHandle { get; set; }
        [Obsolete]
        public Guid UserGuid { get; set; }

        public String GrowerOrderGuid { get; set; }
        public String Category { get; set; }
        public String Form { get; set; }
        public String ShipWeekString { get; set; }
        public bool AddToOrder { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(Category))
            {
                AddMessage("Category is required.");
            }

            if (string.IsNullOrEmpty(Form))
            {
                AddMessage("Form is required.");
            }

            if (string.IsNullOrEmpty(ShipWeekString))
            {
                AddMessage("ShipWeekString is required.");
            }
        }
    }

}