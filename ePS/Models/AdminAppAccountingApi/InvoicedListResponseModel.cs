﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class InvoicedListResponseModel : ResponseBase
    {
        public InvoicedListResponseModel(Guid userGuid, InvoicedSummaryRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new InvoicedSummaryResponse();
            Response.SummaryList = new List<InvoicedOrderSummary>();
            InvoiceSummaryService invoiceService = new InvoiceSummaryService();
            List<InvoicedOrder> orderList = invoiceService.GetOrders(request.BalanceDueOrders, request.GrowerGuid);
            foreach (var order in orderList)
            {
                InvoicedOrderSummary orderSummary = new InvoicedOrderSummary();

                orderSummary.GrowerGuid = order.GrowerGuid;
                orderSummary.GrowerOrderGuid = order.GrowerOrderGuid;
                orderSummary.BillTotal = order.BillTotal;
                orderSummary.PayTotal = order.PayTotal;
                orderSummary.BalDue = order.BalDue;
                orderSummary.OrderNo = order.OrderNo;
                orderSummary.CustomerPoNo = order.CustomerPoNo;
                orderSummary.PromotionalCode = order.PromotionalCode;
                orderSummary.InvoiceDate = order.InvoiceDate;
                orderSummary.InvoiceNumber = order.InvoiceNumber;
                orderSummary.Name = order.Name;

                Response.SummaryList.Add(orderSummary);
            }
        }

        public InvoicedSummaryResponse Response;
    }
}