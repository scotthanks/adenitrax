﻿using AdminAppApiLibrary;
using BusinessObjectServices;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class CreditCardPaymentResponseModel : ResponseBase
    {
        public CreditCardPaymentResponseModel(Guid userGuid, CreditCardPaymentRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            // Special case for Credit Card: create payment and apply payment from one user action
            Response = new CreditCardPaymentResponse();

            // indentifier for the CC payment
            Guid paymentId = Guid.NewGuid(); 

            // create the ledger service with payment ID as liablity parent guid
            LedgerTransactionService ledgerService = new LedgerTransactionService(request.GrowerGuid, Guid.Empty, request.OrderGuid, paymentId);

            // create the CC Payment (creates a sequence number for CC payment)
            Response.TransactionGuid = ledgerService.CreditCardPayment(request.Amount, request.UserCode, request.ReferenceNumber);

            // pay the invoice
            ledgerService.PayInvoiceFromUnappliedCreditCardPayments(request.Amount, request.UserCode, request.InvoiceNumber, request.ReferenceNumber);

            Response.Success = Response.TransactionGuid != Guid.Empty;
        }

        public CreditCardPaymentResponse Response { get; set; }
    }
}