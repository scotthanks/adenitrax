﻿using AdminAppApiLibrary;
using BusinessObjectServices;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class CheckPaymentResponseResponseModel : ResponseBase
    {
        public CheckPaymentResponseResponseModel(Guid userGuid, CheckPaymentRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new CheckPaymentResponse();
            LedgerTransactionService ledgerService = new LedgerTransactionService(request.GrowerGuid, Guid.Empty, request.OrderGuid, Guid.Empty);
            Response.TransactionGuid = ledgerService.CheckPayment(request.Amount, request.UserCode, request.CheckNumber);
            Response.Success = Response.TransactionGuid != Guid.Empty;
        }

        public CheckPaymentResponse Response { get; set; }
    }
}