﻿using AdminAppApiLibrary;
using BusinessObjectServices;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class BillOrderResponseModel : ResponseBase
    {
        public BillOrderResponseModel(Guid userGuid, BillOrderRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            // TBD:
            // set order status to billed
            // if PayType is 'Invoice' 
                //save InvoicePDF bytes as invoice file in blob storage
                // create invoice transaction
            //else
                // create CC transaction
            // log billing trnsaction

            Response = new BillOrderResponse();

            string invoiceNumber = "";
            LedgerTransactionService ledgerService = new LedgerTransactionService(request.GrowerGuid, Guid.Empty, request.OrderGuid, Guid.Empty);
            bool success = ledgerService.InvoiceTransaction(request.Amount, request.UserCode, request.CreditCardReference);
            Response.InvoiceNumber = invoiceNumber;
            Response.Success = success;

        }
        public BillOrderResponse Response { get; set; }
    }
}