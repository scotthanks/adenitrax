﻿using AdminAppApiLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class SupplierInvoicesResponseModel : ResponseBase
    {
        public SupplierInvoicesResponseModel(Guid userGuid, Guid SupplierGuid)
            : base(userGuid, requiresAuthentication: true)
        {
        }

        public SupplierInvoicesResponse Response;
    }
}