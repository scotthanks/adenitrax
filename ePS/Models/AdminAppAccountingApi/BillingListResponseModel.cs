﻿//using BusinessObjectServices;
//using ePS.Types;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace ePS.Models.AdminAppAccountingApi
//{
//    public class BillingListResponseModel : ResponseBase
//    {
//        public BillingListResponseModel(Guid userGuid, BillingSummaryRequest request)
//            : base(userGuid, requiresAuthentication: true)
//        {
//            BillingSummaryService summaryService = new BillingSummaryService();
//            Response = new BillingSummaryResponse();
//            Response.SummaryList = summaryService.GetBillingSummaries(request);
//        }

//        public BillingSummaryResponse Response { get; set; }
//    }
//}