﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class LedgerTransactionResponseModel : ResponseBase
    {
        public LedgerTransactionResponseModel(Guid userGuid, LedgerTransactionRequest request)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new LedgerTransactionResponse();
            try
            {
                LedgerTransactionType transactionType = (LedgerTransactionType)System.Enum.Parse(typeof(LedgerTransactionType), request.LedgerTransactionType);
                LedgerTransactionService ledgerService = new LedgerTransactionService(request.GrowerGuid, request.SupplerGuid, request.AssetParentGuid, request.LiabilityParentGuid);
                if (transactionType == LedgerTransactionType.PayInvoiceFromUnappliedPayments)
                {
                    Response.TransactionGuid = ledgerService.PayInvoiceFromUnappliedPayments(request.Amount, request.UserCode, request.InternalIdNumber, request.ExternalIdNumber);
                }
                else if (transactionType == LedgerTransactionType.PayInvoiceFromUnappliedCredits)
                {
                    Response.TransactionGuid = ledgerService.PayInvoiceFromUnappliedCredits(request.Amount, request.UserCode, request.InternalIdNumber, request.ExternalIdNumber);
                }
                else if (transactionType == LedgerTransactionType.UnappliedPaymentFromGrowerCheck)
                {
                    Response.TransactionGuid = ledgerService.CheckPayment(request.Amount, request.UserCode, request.ExternalIdNumber);
                }
                else if (transactionType == LedgerTransactionType.UnappliedCreditFromCreditMemo)
                {
                    Response.TransactionGuid = ledgerService.CreditPayment(request.Amount, request.UserCode, null);
                    Response.InternalIdNumber = ledgerService.InternalIdNumber;
                }
                else if (transactionType == LedgerTransactionType.RecordSupplierInvoice)
                {
                    Response.TransactionGuid = ledgerService.RecordSupplierInvoice(request.Amount, request.UserCode, request.ExternalIdNumber);
                    Response.InternalIdNumber = ledgerService.InternalIdNumber;
                }
                else if (transactionType == LedgerTransactionType.UnappliedPaymentFromEpsCheck)
                {
                    Response.TransactionGuid = ledgerService.UnappliedPaymentFromEpsCheck(request.Amount, request.UserCode, request.ExternalIdNumber);
                    Response.InternalIdNumber = ledgerService.InternalIdNumber;
                }
                else if (transactionType == LedgerTransactionType.PaySupplierInvoiceFromEpsCheck)
                {
                    Response.TransactionGuid = ledgerService.PaySupplierInvoiceFromEpsCheck(request.Amount, request.UserCode, request.ExternalIdNumber);
                    Response.InternalIdNumber = ledgerService.InternalIdNumber;
                    
                }
                    

                else
                    throw new Exception("LedgerTransactionType not supported");

                Response.TransactionDate = ledgerService.TransactionDateString;
                Response.Success = Response.TransactionGuid != Guid.Empty;
            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.Message = ex.Message;
            }
        }

        public LedgerTransactionResponse Response;
    }
}