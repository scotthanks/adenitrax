﻿using AdminAppApiLibrary;
using BusinessObjectServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppAccountingApi
{
    public class InvoiceLedgerEntryResponseModel : ResponseBase
    {
        public InvoiceLedgerEntryResponseModel(Guid userGuid, Guid orderGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            OrderAccountService accountService = new OrderAccountService();
            LedgerEntry ledgerEntry = accountService.GetInvoiceEntry(orderGuid);
            Response = new InvoiceLedgerEntryResponse();
            Response.Success = ledgerEntry.Success;
            Response.Message = ledgerEntry.Message;
            Response.Amount = ledgerEntry.Amount;
            Response.InvoiceDate = ledgerEntry.TransactionDate.ToString("g", CultureInfo.CreateSpecificCulture("en-us"));
            Response.InvoiceNumber = ledgerEntry.InteralIdNumber;
        }

        public InvoiceLedgerEntryResponse Response { get; set; }

    }
}