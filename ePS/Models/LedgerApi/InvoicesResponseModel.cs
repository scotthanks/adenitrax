﻿using BusinessObjectServices;
using BusinessObjectsLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.LedgerApi
{
    public class InvoicesResponseModel : ResponseBase
    {
        public InvoicesResponseModel(Guid userGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            Response = new InvoicesResponse(userGuid);
            Response.TransactionList = new List<OpenTransaction>();
            Response.AccountBalance = 0;
            try
            {
                GrowerService growerService = new GrowerService();
                Grower grower = growerService.TryGetGrowerForUser(userGuid);

                LedgerTransactionService ledgerService = new LedgerTransactionService(grower.Guid, Guid.Empty, Guid.Empty, Guid.Empty);
                LedgerEntityResponse enityResponse = ledgerService.LedgerEntityRunningBalances(LookupTableValues.Code.GeneralLedgerAccount.AccountsRecievable.Guid);
                if (!enityResponse.Success)
                    throw new Exception("LedgerService.LedgerEntityBalances for Invoices failed.");

                List<string> invoiceList = new List<string>();

                foreach (var entity in enityResponse.EntityList)
                {
                    if (entity.FinalBalance <= 0)
                        continue;

                    if (!invoiceList.Contains(entity.InternalIdNumber))
                    {
                        invoiceList.Add(entity.InternalIdNumber);
                        Response.AccountBalance += entity.FinalBalance;
                    }

                    OpenTransaction trans = new OpenTransaction();
                    trans.Amount = entity.IncreaseTotal;
                    trans.AmountPaid = entity.DecreaseTotal;
                    trans.Balance = entity.Balance;
                    //Response.AccountBalance += entity.Balance;
                    trans.TransactionId = entity.InternalIdNumber;
                    trans.TransactionType = entity.TransactionType;
                    trans.Date = entity.TransactionDate;
                    trans.OrderNumber = GetOrderNumber(entity.ParentGuid);
                    Response.TransactionList.Add(trans);
                }
                enityResponse = ledgerService.LedgerEntityRunningBalances(LookupTableValues.Code.GeneralLedgerAccount.UnappliedPayments.Guid);
                if (!enityResponse.Success)
                    throw new Exception("LedgerService.LedgerEntityBalances for checks failed.");

                foreach (var entity in enityResponse.EntityList)
                {
                    if (entity.FinalBalance <= 0)
                        continue;

                    OpenTransaction trans = new OpenTransaction();
                    trans.Amount = entity.IncreaseTotal;
                    trans.AmountPaid = entity.DecreaseTotal;
                    trans.Balance = entity.Balance;
                    Response.AccountBalance -= entity.Balance;
                    trans.TransactionId = entity.ExternalIdNumber;
                    trans.TransactionType = entity.TransactionType;
                    trans.Date = entity.TransactionDate;
                    Response.TransactionList.Add(trans);
                }

                enityResponse = ledgerService.LedgerEntityRunningBalances(LookupTableValues.Code.GeneralLedgerAccount.UnappliedCredits.Guid);
                if (!enityResponse.Success)
                    throw new Exception("LedgerService.LedgerEntityBalances for credits failed.");

                foreach (var entity in enityResponse.EntityList)
                {
                    if (entity.FinalBalance <= 0)
                        continue;

                    OpenTransaction trans = new OpenTransaction();
                    trans.Amount = entity.IncreaseTotal;
                    trans.AmountPaid = entity.DecreaseTotal;
                    trans.Balance = entity.Balance;
                    Response.AccountBalance -= entity.Balance;
                    trans.TransactionId = entity.ExternalIdNumber;
                    trans.TransactionType = entity.TransactionType;
                    trans.Date = entity.TransactionDate;
                    Response.TransactionList.Add(trans);
                }

                Response.TransactionList = Response.TransactionList.OrderBy(x => x.Date).ToList();

                Response.Success = true;
            }
            catch (Exception ex)
            {
                Response.Success = false;
                Response.AddMessage(ex.Message);
            }
        }

        public InvoicesResponse Response;

        private string GetOrderNumber(Guid OrderGuid)
        {

            var status = new StatusObject(new Guid(), false);
            var x = new GrowerOrderDataService(status);
            var order = x.GetSummaryByGuid(OrderGuid);
            return order.OrderNo;
            //GrowerOrderTableService orderService = new GrowerOrderTableService();
            //List<DataObjectLibrary.GrowerOrder> orderList = (List<DataObjectLibrary.GrowerOrder>)orderService.GetAllActiveByGuid(OrderGuid);
            //if (orderList.Count > 0)
            //{
            //    return orderList[0].OrderNo;
            //}
            //return "";
        }
    }

}