﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectsLibrary;

namespace ePS.Models.AdminAppOrderDetailApi
{
    public class UpdateSupplierOrderResponseModel
    {
        public UpdateSupplierOrderResponseModel(Guid userGuid, UpdateSupplierOrderRequest request)
        {
            Response = new UpdateSupplierOrderResponse();
            if (userGuid == Guid.Empty)
            {
                Response.Success = false;
                Response.Message = "Not authorized";
                return;
            }
       
            var status = new StatusObject(userGuid, true);
            SupplierOrderService updateService = new SupplierOrderService(status);
            UpdateStatus result = updateService.UpdateTrackingData(userGuid, request.SupplierOrderGuid, 
                                        request.UserCode, request.TrackingNumber,request.TrackingComment, DateTime.Parse(request.ExpectedDeliveryDate));
            Response.Success = result.Success;
            Response.Message = result.Message;
        }

        public UpdateSupplierOrderResponse Response;
    }
}