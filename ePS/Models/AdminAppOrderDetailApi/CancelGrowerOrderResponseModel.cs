﻿//using AdminAppApiLibrary;
//using BusinessObjectServices;
//using BusinessObjectsLibrary;
//using DataServiceLibrary;
//using ePS.Models.OrderApi;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace ePS.Models.AdminAppOrderDetailApi
//{
//    public class CancelGrowerOrderResponseModel
//    {
//        public CancelGrowerOrderResponseModel(Guid userGuid, CancelGrowerOrderRequest request)
//        {
//            Response = new CancelGrowerOrderResponse();
//            if (userGuid == Guid.Empty)
//            {
//                Response.Success = false;
//                Response.Message = "Not authorized";
//                return;
//            }

//            try
//            {
//                GetDetailRequestModel detailRequest = new GetDetailRequestModel();
//                detailRequest.OrderGuid = request.GrowerOrderGuid;
//                GetDetailResponseModel orderDetail = new GetDetailResponseModel(userGuid, detailRequest);
//                if (orderDetail.Success)
//                {
//                    foreach (var supplierOrder in orderDetail.GrowerOrder.SupplierOrders)
//                    {
//                        AddOrUpdateOrderLineRequestModel orderLineRequest = new AddOrUpdateOrderLineRequestModel();
//                        List<OrderLineUpdate> orderLineList = new List<OrderLineUpdate>();
//                        orderLineRequest.UserCode = request.UserCode;
//                        foreach (var order in supplierOrder.OrderLines)
//                        {
//                            //TBD: cancel each order line
//                            OrderLineUpdate orderLine = new OrderLineUpdate();
//                            orderLine.GrowerOrderGuid = request.GrowerOrderGuid;
//                            orderLine.Guid = order.Guid;
//                            orderLine.Quantity = 0;
//                            orderLine.OriginalQuantity = order.QuantityOrdered;
//                            orderLineList.Add(orderLine);
//                        }
//                        orderLineRequest.OrderLineUpdates = orderLineList;
//                        AddOrUpdateOrderLineResponseModel orderLineService = new AddOrUpdateOrderLineResponseModel(userGuid, orderLineRequest);
//                        if (!orderLineService.Success)
//                        {
//                            throw new Exception(orderLineService.Messages[0]);
//                        }
                    
//                        // set supplierorder.status = cancelled
//                        SupplierOrderTableService supplierOrderService = new SupplierOrderTableService();
//                        List<DataObjectLibrary.SupplierOrder> supplierOrderList = supplierOrderService.GetAllActiveByGuid(supplierOrder.SupplierOrderGuid);
//                        DataObjectLibrary.Lookup logTypeLookup = LookupTableValues.Logging.LogType.Grower.SupplierOrderCancel;
                       
//                        if(supplierOrderList.Count > 0)
//                        {
//                            LookupTableValues.CodeValues.SupplierOrderStatusValues lookUpValues = new LookupTableValues.CodeValues.SupplierOrderStatusValues();
//                            supplierOrderList[0].SupplierOrderStatusLookup = lookUpValues.Cancelled;
//                            supplierOrderService.Update(supplierOrderList[0]);
                           
//                        }
//                        else
//                            throw new Exception("Supplier order not found");
//                    }
//                    // set grow order status = cancelled
//                    GrowerOrderService orderService = new GrowerOrderService();
//                    orderService.CancelPlacedOrder(userGuid, request.UserCode, request.GrowerOrderGuid);

//                    var status = new StatusObject(userGuid, true);
//                    GrowerOrderDataService orderUpdateService = new GrowerOrderDataService(status);
//                    orderUpdateService.RecalculateFeesbyGrowerOrderGuid(request.GrowerOrderGuid, true);

//                }
//            }
//            catch(Exception ex)
//            {
//                Response.Success = false;
//                Response.Message = ex.Message;
//                return;
//            }
        
//            Response.Success = true;
//        }

//        public CancelGrowerOrderResponse Response;
//    }
//}