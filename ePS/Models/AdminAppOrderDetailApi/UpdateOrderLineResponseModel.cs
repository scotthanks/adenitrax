﻿using AdminAppApiLibrary;
using DataServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.AdminAppOrderDetailApi
{
    public class UpdateOrderLineResponseModel
    {
        public UpdateOrderLineResponseModel(Guid userGuid, UpdateOrderLineRequest request)
        {
            Response = new UpdateOrderLineResponse();
            if (userGuid == Guid.Empty)
            {
                Response.Success = false;
                Response.Message = "Not authorized";
                return;
            }

            OrderLineUpdateService updateService = new OrderLineUpdateService();
            UpdateStatus result = updateService.UpdatePriceAndCost(userGuid, request.OrderLineGuid, 
                                        request.UserCode, request.Cost, request.Price);
            Response.Success = result.Success;
            Response.Message = result.Message;
        }

        public UpdateOrderLineResponse Response;
    }
}