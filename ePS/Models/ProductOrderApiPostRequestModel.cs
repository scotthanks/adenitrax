﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models
{
    public class ProductOrderApiPostRequestModel : RequestBase
    {
        [Obsolete]
        public string UserHandle { get; set; }

        public string ProgramCategoryCode { get; set; }
        
        public string ProductFormCode { get; set; }
        public string ShipWeekString { get; set; }
        public IEnumerable<ProductOrderApiOrderedItemType> OrderedItems { get; set; }

        public Guid ProductGuid { get; set; }
        public int OrderQty { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(ProductFormCode))
            {
                AddMessage("ProductFormCode is required.");
            }

            if (string.IsNullOrEmpty(ShipWeekString))
            {
                AddMessage("ShipWeekString is required.");
            }

            //TODO: OrderedItems is depricated. Remove this paragraph when the item is removed.
            if (OrderedItems != null)
            {
                int count = 0;
                foreach (var orderedItem in OrderedItems)
                {
                    ProductGuid = orderedItem.productGuid;
                    OrderQty = orderedItem.OrderQuantity;

                    count++;
                }

                if (count > 1)
                {
                    AddMessage("Multiple items can no longer be ordered at the same time.");
                }
            }

            if (ProductGuid == Guid.Empty)
            {
                AddMessage("ProductGuid is required.");
            }

            if (OrderQty <= 0)
            {
                AddMessage("OrderQty is required and must be greater than zero.");
            }
        }
    }
}