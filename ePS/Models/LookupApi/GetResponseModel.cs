﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.LookupApi
{
    public class GetResponseModel
    {
        public GetResponseModel(Guid userGuid, GetRequestModel request)
        {
            if (userGuid != Guid.Empty)
            {
                this.IsAuthenticated = true;
         
                var service = new BusinessObjectServices.LookupService();
                var lookupBusinessObjectList = service.GetAll();

                LookupList = new List<SelectListItemType>();                
                foreach (var lookup in lookupBusinessObjectList)
                {
                    LookupList.Add(new SelectListItemType()
                        {
                            Guid = lookup.Guid,
                            Code = lookup.Code,
                            DisplayName = lookup.DisplayName,
                            IsDefault = false,
                            IsSelected = false,
                            ParentPath = lookup.ParentPath
                        });
                }
            }
        }

        public bool IsAuthenticated { get; set; }
        public List<SelectListItemType> LookupList { get; set; } 
    }
}