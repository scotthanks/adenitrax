﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

using BusinessObjectsLibrary;

namespace ePS.Models.OrderApi
{
    public class GetAddressResponseModel
    {
        public GetAddressResponseModel(BusinessObjectsLibrary.GrowerShipToAddress address)
        {
            Address = new ShipToAddressType()
                {
                    GrowerAddressGuid = address.GrowerAddressGuid,
                    AddressGuid = address.AddressGuid,
                    Name = address.AddressName,
                    StreetAddress1 = address.StreetAddress1,
                    StreetAddress2 = address.StreetAddress2,
                    City = address.City,
                    StateCode = address.State.StateCode,
                    StateName = address.State.StateName,
                    Country = address.State.Country.CountryName,
                    CountryCode = address.State.Country.CountryCode,
                    ZipCode = address.ZipCode,
                    SpecialInstructions = address.SpecialInstructions,
                    SellerCustomerID = address.SellerCustomerID,
                    IsDefault = address.IsDefault,
                };
        }

        public ShipToAddressType Address { get; private set; }
    }
}