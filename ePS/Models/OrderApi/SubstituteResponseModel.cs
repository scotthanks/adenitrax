﻿using BusinessObjectServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class SubstituteResponseModel : ResponseBase
    {
        public SubstituteResponseModel(Guid userGuid, SubstituteRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {

                DataServiceLibrary.OrderLineTableService orderLineService = new DataServiceLibrary.OrderLineTableService();
                var orderLine = orderLineService.GetByGuid(request.OrderLineGuid);

                             
                var shipWeekService = new ShipWeekService();
                var shipWeek = shipWeekService.GetShipWeek(Convert.ToInt32(request.ShipWeek.Substring(0, 2)), Convert.ToInt32(request.ShipWeek.Substring(3, 4)));

                ReportedAvailabilityService raService = new ReportedAvailabilityService();
                raService.UpdateSalesSinceDateReported(orderLine.ProductGuid, orderLine.Product.ProductFormGuid, shipWeek.Guid, orderLine.QtyOrdered * -1);
                raService.UpdateSalesSinceDateReported(request.ProductGuid, orderLine.Product.ProductFormGuid, shipWeek.Guid, orderLine.QtyOrdered);
                
                var growerOrderService = new GrowerOrderService();

                growerOrderService.
                    SubstituteProductOnOrderLine
                    (
                        userGuid, 
                        request.UserCode, 
                        request.OrderLineGuid,
                        request.ProductGuid
                    );

                    

                Success = true;
            }
            else
            {
                Success = false;
            }
        }
    }
}