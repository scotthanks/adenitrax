﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class EditGrowerOrderLogResponseModel : ResponseBase
    {
        public EditGrowerOrderLogResponseModel(Guid userGuid, EditGrowerOrderLogRequestModel growerOrderLogRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(growerOrderLogRequest))
            {
                Success = true;
                var growerService = new BusinessObjectServices.GrowerService();
                var bRet = false;
                try
                {
                    bRet = growerService.EditGrowerOrderLog(userGuid, growerOrderLogRequest.GrowerOrderHistoryGuid, growerOrderLogRequest.Comment, growerOrderLogRequest.IsInternal);
                    if (bRet == false)
                    {
                        AddMessage("The event log was not edited.");
                        Success = false;
                    }
                }
                catch (ArgumentException argumentException)
                {
                    AddMessage(argumentException.Message);
                    Success = false;
                }

                
      
            }
        }

       
    }
}