﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class GetSummaryRequestModel : RequestBase
    {
        //TODO: Get rid of this enum (replace with Lookup Codes).
        public enum RequestTypeEnum
        {
            SummaryData,
         //   MyShoppingCartOrders,
         //   MyOpenOrders,
            //MyShippedOrders,
         //   MyCancelledOrders,
           // MyOrders,
            AllShoppingCartOrders,
            AllOpenOrders,
            AllShippedOrders,
            AllCancelledOrders,
            AllOrders,
        }

        public string RequestType { get; set; }

        public override void Validate()
        {
            RequestTypeEnum requestType;

            bool isValid = Enum.TryParse(RequestType, ignoreCase: true, result: out requestType);

            if (!isValid)
            {
                AddMessage(string.Format("The request type {0} is not valid.", RequestType));
            }
        }
    }
}