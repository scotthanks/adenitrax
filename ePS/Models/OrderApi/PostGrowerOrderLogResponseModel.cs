﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class PostGrowerOrderLogResponseModel : ResponseBase
    {
        public PostGrowerOrderLogResponseModel(Guid userGuid, PostGrowerOrderLogRequestModel growerOrderLogRequest,string Action)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(growerOrderLogRequest))
            {
                var growerService = new BusinessObjectServices.GrowerService();
                Success = true;
                try
                {
                    if (Action == "Add")
                    {
                        EventLogGuid = growerService.AddGrowerOrderLog(userGuid, growerOrderLogRequest.UserCode, growerOrderLogRequest.GrowerOrderGuid, growerOrderLogRequest.LogTypeLookupCode, growerOrderLogRequest.Text);
                        if (EventLogGuid == Guid.Empty)
                        {
                            AddMessage("The event log was not added.");
                            Success = false;
                        }
                       
                    
                    }
                    else if (Action == "Delete")
                    {
                        //passed im hisotry guid not the order guid
                      // var bRet = growerService.DeleteGrowerOrderLog(userGuid, growerOrderLogRequest.GrowerOrderGuid);
                        AddMessage("hello Scott");
                    }
                }
                catch (ArgumentException argumentException)
                {
                    AddMessage(argumentException.Message);
                    Success = false;
                }

               
            }
        }

        public Guid EventLogGuid { get; set; }
    }
}