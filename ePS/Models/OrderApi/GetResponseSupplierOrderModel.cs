﻿using System.Collections.Generic;
using System.Linq;
using BusinessObjectsLibrary;
using ePS.Types;
using System.Globalization;
using System;
using AdminAppApiLibrary;



namespace ePS.Models.OrderApi
{
    public class GetResponseSupplierOrderModel
    {
        public GetResponseSupplierOrderModel(SupplierOrder supplierOrder)
        {
            supplierOrder.TagRatioList.Sort();
            supplierOrder.ShipMethodList.Sort();

            var orderLineTypeList = supplierOrder.OrderLines.Select(orderLine => new GetOrderLineResponseModel(orderLine).OrderLine).ToList();
            orderLineTypeList.Sort();

            SupplierOrder = new SupplierOrderType
                {
                    SupplierOrderGuid = supplierOrder.SupplierOrderGuid,
                    SupplierCode = supplierOrder.Supplier.Code,
                    SupplierName = supplierOrder.Supplier.Name,
                    SupplierOrderNo = supplierOrder.SupplierOrderNo,
                    SupplierOrderStatusCode = supplierOrder.SupplierOrderStatus.Code,
                    OrderLines = orderLineTypeList.ToArray(),
                };

            if (supplierOrder.LastSentToSupplierDate > Convert.ToDateTime("1/1/2010"))
            {
                SupplierOrder.LastSentToSupplierDate= supplierOrder.LastSentToSupplierDate.ToString();
            }
            var supplierOrderFeeList = new List<SupplierOrderFeeType>();
            foreach (var supplierOrderFee in supplierOrder.SupplierOrderFeeList)
            {
                var supplierOrderFeeType = new SupplierOrderFeeType()
                {
                    Guid = supplierOrderFee.Guid,
                    SupplierOrderGuid = supplierOrderFee.Guid,
                    SupplierOrderFeeTypeCode = supplierOrderFee.SupplierOrderFeeType.Code,
                    SupplierOrderFeeTypeDescription = supplierOrderFee.SupplierOrderFeeType.Description,
                    SupplierOrderFeeStatusCode = supplierOrderFee.SupplierOrderFeeStatus.Code,
                    SupplierOrderFeeStatusDescription = supplierOrderFee.SupplierOrderFeeStatus.Description,
                    Amount = supplierOrderFee.Amount,
                    FeeDescription = supplierOrderFee.FeeDescription
                };

                supplierOrderFeeList.Add(supplierOrderFeeType);
            }
            SupplierOrder.SupplierOrderFeeList = supplierOrderFeeList.ToArray();

            var shipMethodList = new List<SelectListItemType>();
            foreach (var shipMethodBusinessObject in supplierOrder.ShipMethodList)
            {
                var shipMethod = new GetSelectListItemResponseModel(shipMethodBusinessObject).SelectListItem;
                shipMethodList.Add(shipMethod);

                if (shipMethod.Code == supplierOrder.ShipMethodDefault.Code)
                {
                    shipMethod.IsDefault = true;
                }

                if (shipMethod.Code == supplierOrder.ShipMethod.Code)
                {
                    shipMethod.IsSelected = true;
                }
            }
            SupplierOrder.ShipMethodList = shipMethodList.ToArray();

            var tagRatioList = new List<SelectListItemType>();
            foreach (var tagRatioBusinessObject in supplierOrder.TagRatioList)
            {
                var tagRatio = new GetSelectListItemResponseModel(tagRatioBusinessObject).SelectListItem;
                tagRatioList.Add(tagRatio);

                if (tagRatio.Code == supplierOrder.TagRatioDefault.Code)
                {
                    tagRatio.IsDefault = true;
                }

                if (tagRatio.Code == supplierOrder.TagRatio.Code)
                {
                    tagRatio.IsSelected = true;
                }
            }
            SupplierOrder.TagRatioList = tagRatioList.ToArray();

            var logList = new List<LogType>();
            foreach (var log in supplierOrder.GrowerOrderHistoryEvents)
            {
                logList.Add(new LogType()
                {
                    Guid = log.Guid,
                    //LogTime = log.EventTime,
                    LogTimeString = log.EventTime.ToString("g", CultureInfo.CreateSpecificCulture("en-us")),
                    Text = log.Comment,
                    LogTypeLookupCode = log.LogTypeLookupCode,
                    IsInternal = log.IsInternal,
                    PersonName = log.PersonName,
                    UserCode = log.UserCode
                });
            }
            SupplierOrder.Logs = logList.ToArray();
        }

        public SupplierOrderType SupplierOrder { get; set; }
    }
}