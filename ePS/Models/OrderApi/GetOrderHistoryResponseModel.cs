﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using System.Globalization;

namespace ePS.Models.OrderApi
{
    public class GetOrderHistoryResponseModel : ResponseBase
    {
        public List<GrowerOrderHistoryEvent> eventList { get; set; }

        public GetOrderHistoryResponseModel(Guid userGuid,Guid orderGuid,GetOrderHistoryRequestModel historyRequest)
             : base(userGuid, requiresAuthentication: true)
        {
           
            historyRequest.OrderGuid = orderGuid;
            var growerOrderService = new GrowerOrderService();
            historyRequest.eventList = growerOrderService.GetGrowerOrderHistory(orderGuid, true);
            eventList = historyRequest.eventList;
            Success = true;
        }
       
    }
}