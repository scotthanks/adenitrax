﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class GetDetailRequestModel : RequestBase
    {
        public Guid OrderGuid { get; set; }

        public string RequestType { get; set; }

        public override void Validate()
        {
            if (OrderGuid == Guid.Empty)
            {
                AddMessage("The OrderGuid is required.");
            }
        }
    }

    public class GetRequestDetailModelV2 : RequestBase
    {
        public Guid OrderGuid { get; set; }

        public string RequestType { get; set; }

        public override void Validate()
        {
            if (OrderGuid == Guid.Empty)
            {
                AddMessage("The OrderGuid is required.");
            }
        }
    }
}