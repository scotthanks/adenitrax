﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class EditGrowerOrderLogRequestModel : RequestBase
    {
        public Guid GrowerOrderHistoryGuid { get; set; }
        public string Comment { get; set; }
        public bool IsInternal { get; set; }
       

        public override void Validate()
        {
            if (GrowerOrderHistoryGuid == Guid.Empty)
            {
                AddMessage("GrowerOrderHistoryGuid is required.");
            }

            if (string.IsNullOrEmpty(Comment) )
            {
                AddMessage("Comment is required.");
            }
          

        }
    }
}