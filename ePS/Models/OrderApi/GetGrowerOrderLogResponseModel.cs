﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.OrderApi
{
    public class GetGrowerOrderLogResponseModel : ResponseBase
    {
        public List<Types.LogType> GrowerOrderLogs { get; set; }
 
        public GetGrowerOrderLogResponseModel(Guid userGuid, GetGrowerOrderLogRequestModel request)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(request))
            {
                var growerBusinessService = new BusinessObjectServices.GrowerService();

                var growerOrderLogs = growerBusinessService.GetGrowerOrderLogs(request.GrowerOrderGuid);

                GrowerOrderLogs = new List<LogType>();
            }
        }
    }
}