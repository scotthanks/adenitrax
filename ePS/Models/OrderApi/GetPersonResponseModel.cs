﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePS.Types;

namespace ePS.Models.OrderApi
{
    public class GetPersonResponseModel
    {
        public GetPersonResponseModel(BusinessObjectsLibrary.Person personBusinessObject)
        {
            PersonType = new PersonType()
                {
                    Name = personBusinessObject.FirstName + " " + personBusinessObject.LastName,
                    Email = personBusinessObject.Email,
                    PhoneNumber = personBusinessObject.Phone.FormattedPhoneNumber
                };
        }

        public PersonType PersonType { get; set; }
    }
}