﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class DeleteGrowerOrderLogResponseModel : ResponseBase
    {
        public DeleteGrowerOrderLogResponseModel(Guid userGuid, DeleteGrowerOrderLogRequestModel growerOrderLogRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(growerOrderLogRequest))
            {
                Success = true;
                var growerService = new BusinessObjectServices.GrowerService();
                var bRet = false;
                try
                {
                    bRet = growerService.DeleteGrowerOrderLog(userGuid, growerOrderLogRequest.GrowerOrderHistoryGuid);
                    if (bRet == false)
                    {
                        AddMessage("The event log was not deleted.");
                        Success = false;
                    }
                }
                catch (ArgumentException argumentException)
                {
                    AddMessage(argumentException.Message);
                    Success = false;
                }

                
      
            }
        }

       
    }
}