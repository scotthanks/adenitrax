﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataServiceLibrary;

namespace ePS.Models.OrderApi
{
    public class GetRequestSupplierFeeModel : RequestBase
    {
        public Guid SupplierOrderGuid { get; set; }

        public override void Validate( )
        {
            //ToDo: Scott, I added this code here.  You will want to build this out to validate the Supplier Order as well.
            if (this.SupplierOrderGuid == Guid.Empty)
            {
                AddMessage("SupplierOrderGuid is required.");
            };
        }
    }
}