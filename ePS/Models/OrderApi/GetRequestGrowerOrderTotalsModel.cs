﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataServiceLibrary;

namespace ePS.Models.OrderApi
{
    public class GetRequestGrowerOrderTotalsModel : RequestBase
    {
        public Guid GrowerOrderGuid { get; set; }
        public string Category { get; set; }
        public string Form { get; set; }
        public string ShipWeekString { get; set; }
        
       

        public override void Validate()
        {
            
            if (this.GrowerOrderGuid == Guid.Empty)
            {
                if (Category == "" || Form == "" || ShipWeekString == "")
                {
                    AddMessage("Either GrowerOrderGuid is required or Category, Form, and ShipWeekString is required");
                }
            };

           
        }
    }
}