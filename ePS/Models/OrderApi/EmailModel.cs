﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Net.Mail;
using ePS.Types;
using DataServiceLibrary;
using BusinessObjectServices;
using AdminAppApiLibrary;

// admin version only using BusinessServiceLibrary;

namespace ePSAdmin.Models.OrderDetailApi
{

    public class NameValueTemplate
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public NameValueTemplate( string name, string value )
        {
            Name = name;
            Value = value;
        }
    }

    public class NameValueSection
    {
        public List<NameValueTemplate> nameValueList = new List<NameValueTemplate>();
        public List<NameValueSection> childSectionList = new List<NameValueSection>();
        public string StartSectionMark { get; set; }
        public string EndSectionMark { get; set; }
        public NameValueSection(string startSectionMark, string endSectionMark)
        {
            StartSectionMark = startSectionMark;
            EndSectionMark = endSectionMark;
        }
    }

    public class EmailModel
    {
        string templateFilePath { get; set; }
        int rowNumber = -1;
        NameValueSection masterSection = new NameValueSection("", "");

        public EmailModel(string shipWeek, string templatePath, GrowerOrderType growerOrder)
        {
            this.rowNumber = -1;
        }

        //public EmailModel(int rowNumber, string shipWeek, string templatePath, OrderDetailCache orderDetailCache)
        public EmailModel(int rowNumber, string shipWeek, string templatePath, GrowerOrderType growerOrder, ILookupService lookupService, string headerText, string bodyText)
        {
            templateFilePath = templatePath;
            this.rowNumber = rowNumber;

            //#TrayBagTitle"
            var TrayBagTitle = "Trays";
            if (growerOrder.ProductFormCategoryName.Contains("Unrooted") ||
                growerOrder.ProductFormCategoryName.Contains("Callused"))
            {
                TrayBagTitle = "Bags";
            } 
            var User = growerOrder.PersonWhoPlacedOrder.Name;
            var GrowerOrderNumber = growerOrder.OrderNo;
            var ShipWeek = shipWeek;
            var Category = growerOrder.ProgramTypeName;
            var ProductFormCategory = growerOrder.ProductFormCategoryName;
            var CustomerPoNo = growerOrder.CustomerPoNo;
            var PromotionalCode = growerOrder.PromotionalCode;
            var CustomerDescription = growerOrder.OrderDescription;
            var OrderDate = growerOrder.DateEnteredString;

            var ShipToAddressName = "";
            var ShipToAddressCity = "";
            var ShipToAddressState = "";
            var ShipToAddressStreetAddress = "";
            var ShipToAddressStreetAddress2 = "";
            var ShipToAddressZipCode = "";

            ShipToAddressType shipToAddress = null;
            if (growerOrder.ShipToAddresses != null)
            {
                foreach (var addr in growerOrder.ShipToAddresses)
                {
                    if (addr.IsSelected)
                    {
                        shipToAddress = addr;
                        break;
                    }
                }
            }

            if (shipToAddress != null)
            {
                ShipToAddressName = shipToAddress.Name;
                ShipToAddressCity = shipToAddress.City;
                ShipToAddressState = shipToAddress.StateCode;
                ShipToAddressStreetAddress = shipToAddress.StreetAddress1;
                ShipToAddressStreetAddress2 = shipToAddress.StreetAddress2;
                ShipToAddressZipCode = shipToAddress.ZipCode;
            } 
            
            var PhoneNumber = growerOrder.GrowerPhone;

            var ProductFormCategoryName = growerOrder.ProductFormCategoryName;

            var PaymentMethod = "";
            string orderType = "";
            foreach (var method in growerOrder.PaymentTypeList)
            {
                if (method.IsSelected)
                {
                    PaymentMethod = method.Name;
                    orderType = method.Code;
                    break;
                }
            }
            var CardNumber = "";
            foreach (var card in growerOrder.CreditCards)
            {
                if (card.IsSelected)
                {
                    //CardNumber = card.CardType;
                    CardNumber = card.CardNumber;

                }
            }

            masterSection.nameValueList.Add(new NameValueTemplate("#user#", User));
            masterSection.nameValueList.Add(new NameValueTemplate("#HeaderText#", headerText));
            masterSection.nameValueList.Add(new NameValueTemplate("#BodyText#", bodyText));
            //masterSection.nameValueList.Add(new NameValueTemplate("#CardNumber#", CardNumber));
            string cardMarkUp = "";
            if (orderType == "Credit")
            {
                cardMarkUp = "<td width=\"62\" valign=\"top\" style=\"color: #287d87; vertical-align: top; text-align:left;\">";
                cardMarkUp += "<strong>Card Used:</strong>";
                cardMarkUp += "</td>";
                cardMarkUp += "<td width=\"331\" valign=\"top\" style=\"vertical-align:top; text-align:left;\">";
                cardMarkUp += CardNumber;
                cardMarkUp += "</td>";
            }
            else
            {
                cardMarkUp = "<td width=\"62\" valign=\"top\" style=\"color: #287d87; vertical-align: top; text-align:left;\">";
                //cardMarkUp += "<strong>Card Used:</strong>";
                cardMarkUp += "</td>";
                cardMarkUp += "<td width=\"331\" valign=\"top\" style=\"vertical-align:top; text-align:left;\">";
                //cardMarkUp += CardNumber;
                cardMarkUp += "</td>";
            }
            masterSection.nameValueList.Add(new NameValueTemplate("#CardNumber#", cardMarkUp));
            masterSection.nameValueList.Add(new NameValueTemplate("#PaymentMethod#", PaymentMethod));
            masterSection.nameValueList.Add(new NameValueTemplate("#GrowerOrderNumber#", GrowerOrderNumber));
            masterSection.nameValueList.Add(new NameValueTemplate("#ShipWeek#", ShipWeek));
            masterSection.nameValueList.Add(new NameValueTemplate("#Category#", Category));
            masterSection.nameValueList.Add(new NameValueTemplate("#ProductFormCategory#", ProductFormCategory));
            masterSection.nameValueList.Add(new NameValueTemplate("#CustomerPoNo#", CustomerPoNo));
            masterSection.nameValueList.Add(new NameValueTemplate("#ProductFormCategory#", ProductFormCategory));
            masterSection.nameValueList.Add(new NameValueTemplate("#CustomerDescription#", CustomerDescription));
            masterSection.nameValueList.Add(new NameValueTemplate("#ShipToAddressName#", ShipToAddressName));
            masterSection.nameValueList.Add(new NameValueTemplate("#ShipToAddressStreetAddress#", ShipToAddressStreetAddress));
            masterSection.nameValueList.Add(new NameValueTemplate("#ShipToAddressCity#", ShipToAddressCity));
            masterSection.nameValueList.Add(new NameValueTemplate("#ShipToAddressState#", ShipToAddressState));
            masterSection.nameValueList.Add(new NameValueTemplate("#ShipToAddressZipCode#", ShipToAddressZipCode));
            masterSection.nameValueList.Add(new NameValueTemplate("#PhoneNumber#", PhoneNumber.ToString()));
            masterSection.nameValueList.Add(new NameValueTemplate("#ProductFormCategoryName#", ProductFormCategoryName));
            masterSection.nameValueList.Add(new NameValueTemplate("#ShipToAddressStreetAddress2#", ShipToAddressStreetAddress2));
            masterSection.nameValueList.Add(new NameValueTemplate("#OrderDate#", OrderDate));
            

            int startRow = 0;
            int orderCount = growerOrder.SupplierOrders.Count();
            if (rowNumber >= 0)
            {
                startRow = rowNumber;
                orderCount = startRow + 1;
            }

            decimal OrderTotalCost = 0;
            int OrderTotalTrays = 0;
            int OrderTotalUnits = 0;
            for(int i = startRow; i< orderCount; i++)
            {
                SupplierOrderType supplierOrder = growerOrder.SupplierOrders[i];
                NameValueSection supplierSection = new NameValueSection("#StartSupplier#", "#EndSupplier#");
                //NameValueSection supplierChargesSection = new NameValueSection("#StartSupplierFee#", "#EndSupplierFee#");


                var SupplierName = supplierOrder.SupplierName;
                var SupplierOrderNo = supplierOrder.SupplierOrderNo;
                
                var ShipMethod = "not selected";
                foreach (var shipMethod in supplierOrder.ShipMethodList)
                {
                    if (shipMethod.IsSelected)
                    {
                        ShipMethod = shipMethod.Name;
                        break;
                    }
                }

                var TagRatio = "not selected";
                foreach (var tagRatio in supplierOrder.TagRatioList)
                {
                    if (tagRatio.IsSelected)
                    {
                        TagRatio = tagRatio.Name;
                        break;
                    }
                }

                supplierSection.nameValueList.Add(new NameValueTemplate("#ShipMethod#", ShipMethod));
                supplierSection.nameValueList.Add(new NameValueTemplate("#TagRatio#", TagRatio));
                supplierSection.nameValueList.Add(new NameValueTemplate("#SupplierName#", SupplierName));
                supplierSection.nameValueList.Add(new NameValueTemplate("#SupplierOrderNo#", SupplierOrderNo));
                supplierSection.nameValueList.Add(new NameValueTemplate("#TrayBagTitle#", TrayBagTitle));

                decimal chargesTotal = 0;
                decimal extentionTotal = 0;
                int TotalQuanityOrdered = 0;
                int TotalTrays = 0;
                foreach (OrderLineType orderLine in supplierOrder.OrderLines)
                {
                    NameValueSection childSection = new NameValueSection("#StartOrderLine#", "#EndOrderLine#");
                    TotalQuanityOrdered += orderLine.QuantityOrdered;
                    var QuantityOrdered = orderLine.QuantityOrdered;
                    childSection.nameValueList.Add(new NameValueTemplate("#QuantityOrdered#", QuantityOrdered.ToString()));

                    //int Tags = QuantityOrdered / orderLine.Multiple;
                    //childSection.nameValueList.Add(new NameValueTemplate("#Tags#", Tags.ToString()));

                    int QuantityOfTrays = orderLine.QuantityOrdered / orderLine.Multiple;
                    TotalTrays += QuantityOfTrays;
                    childSection.nameValueList.Add(new NameValueTemplate("#QuantityOfTrays#", QuantityOfTrays.ToString()));

                    var ProductForm = orderLine.ProductFormName;
                    childSection.nameValueList.Add(new NameValueTemplate("#ProductForm#", ProductForm));

                    childSection.nameValueList.Add(new NameValueTemplate("#SupplierIdentifier#", orderLine.SupplierProductIdentifier));
                    childSection.nameValueList.Add(new NameValueTemplate("#SupplierDescription#", orderLine.SupplierProductDescription));
                    childSection.nameValueList.Add(new NameValueTemplate("#SpeciesName#", orderLine.SpeciesName));
                    if(orderLine.Price > 0)
                        childSection.nameValueList.Add(new NameValueTemplate("#Price#", string.Format("{0:0.0000}", orderLine.Price)));
                    else
                        childSection.nameValueList.Add(new NameValueTemplate("#Price#", "TBD"));
                    childSection.nameValueList.Add(new NameValueTemplate("#OrderLineStatusCode#", lookupService.GetCodeName(orderLine.OrderLineStatusCode)));
                    childSection.nameValueList.Add(new NameValueTemplate("#VarietyName#", orderLine.VarietyName));

                    decimal ext = orderLine.Price * orderLine.QuantityOrdered;
                    extentionTotal += ext;
                    string extension = "TBD";
                    if(ext > 0)
                        extension = string.Format("{0:C}", ext);
                    childSection.nameValueList.Add(new NameValueTemplate("#Extension#", extension));

                    supplierSection.childSectionList.Add(childSection);

                }

                if (growerOrder.SupplierOrders[i].SupplierOrderFeeList != null && growerOrder.SupplierOrders[i].SupplierOrderFeeList.Length > 0)
                {
                    supplierSection.nameValueList.Add(new NameValueTemplate("#StartSupplierFeeComment#", ""));
                    supplierSection.nameValueList.Add(new NameValueTemplate("#EndSupplierFeeComment#", ""));
                    foreach (SupplierOrderFeeType supplierCharge in growerOrder.SupplierOrders[i].SupplierOrderFeeList)
                    {
                        NameValueSection childSection = new NameValueSection("#StartSupplierFee#", "#EndSupplierFee#");

                        if (supplierCharge.SupplierOrderFeeStatusDescription == "Estimated" || supplierCharge.SupplierOrderFeeStatusDescription == "Actual")
                            chargesTotal += supplierCharge.Amount;

                        if (supplierCharge.SupplierOrderFeeStatusDescription == "Estimated" || supplierCharge.SupplierOrderFeeStatusDescription == "Actual")
                            childSection.nameValueList.Add(new NameValueTemplate("#SupplierFeeAmount#", string.Format("{0:C}", supplierCharge.Amount)));
                        else
                            childSection.nameValueList.Add(new NameValueTemplate("#SupplierFeeAmount#", supplierCharge.SupplierOrderFeeStatusDescription));
                        
                        childSection.nameValueList.Add(new NameValueTemplate("#SupplierOrderFeeTypeDescription#", supplierCharge.SupplierOrderFeeTypeDescription));
                        
                        if (supplierCharge.SupplierOrderFeeStatusDescription == "Estimated")
                            childSection.nameValueList.Add(new NameValueTemplate("#SupplierOrderFeeStatusDescription#", supplierCharge.SupplierOrderFeeStatusDescription));
                        else
                            childSection.nameValueList.Add(new NameValueTemplate("#SupplierOrderFeeStatusDescription#", ""));
                        
                        childSection.nameValueList.Add(new NameValueTemplate("#SupplierFeeDescription#", supplierCharge.FeeDescription));
                        supplierSection.childSectionList.Add(childSection);
                    }
                } 
                else
                {
                    supplierSection.nameValueList.Add(new NameValueTemplate("#StartSupplierFeeComment#", "<!--"));
                    supplierSection.nameValueList.Add(new NameValueTemplate("#EndSupplierFeeComment#", "-->"));
                }

              
                string chrgTotal = string.Format("{0:C}", chargesTotal);
                string extTotal = string.Format("{0:C}", extentionTotal);
                if (chargesTotal > 0)
                    supplierSection.nameValueList.Add(new NameValueTemplate("#SupplierCharges#", chrgTotal));
                else
                    supplierSection.nameValueList.Add(new NameValueTemplate("#SupplierCharges#", "TBD"));

                supplierSection.nameValueList.Add(new NameValueTemplate("#ExtensionTotal#", extTotal));
                supplierSection.nameValueList.Add(new NameValueTemplate("#TotalQuanityOrdered#", TotalQuanityOrdered.ToString()));
                supplierSection.nameValueList.Add(new NameValueTemplate("#TotalTrays#", TotalTrays.ToString()));
                string supplierTotal = string.Format("{0:C}", chargesTotal + extentionTotal);
                supplierSection.nameValueList.Add(new NameValueTemplate("#SupplierTotal#", supplierTotal));
                OrderTotalTrays += TotalTrays;
                OrderTotalUnits += TotalQuanityOrdered;
                OrderTotalCost += chargesTotal + extentionTotal;

                masterSection.childSectionList.Add(supplierSection);
            }
            string ordTotal = string.Format("{0:C}", OrderTotalCost);
            masterSection.nameValueList.Add(new NameValueTemplate("#OrderTotalCost#", ordTotal));
            masterSection.nameValueList.Add(new NameValueTemplate("#OrderTotalTrays#", OrderTotalTrays.ToString()));
            masterSection.nameValueList.Add(new NameValueTemplate("#OrderTotalUnits#", OrderTotalUnits.ToString()));

            if (growerOrder.GrowerOrderFeeList != null && growerOrder.GrowerOrderFeeList.Length > 0)
            {
                masterSection.nameValueList.Add(new NameValueTemplate("#OpenOrderFeeCommentTag#", ""));
                masterSection.nameValueList.Add(new NameValueTemplate("#CloseOrderFeeCommentTag#", ""));
                masterSection.nameValueList.Add(new NameValueTemplate("#OrderTotalText#", ""));
                decimal feeTotal = 0;
                foreach (var fee in growerOrder.GrowerOrderFeeList)
                {
                    NameValueSection growerFeeSection = new NameValueSection("#StartOrderFee#", "#EndOrderFee#");
                    growerFeeSection.nameValueList.Add(new NameValueTemplate("#OrderFeeDescription#", fee.GrowerOrderFeeTypeDescription));
                    growerFeeSection.nameValueList.Add(new NameValueTemplate("#OrderFeeAmount#", string.Format("{0:c}", fee.Amount)));
                    masterSection.childSectionList.Add(growerFeeSection);
                    feeTotal += fee.Amount;
                }
                masterSection.nameValueList.Add(new NameValueTemplate("#OrderFeeTotal#", string.Format("{0:c}", feeTotal)));
                masterSection.nameValueList.Add(new NameValueTemplate("#AdjOrderTotalCost#", string.Format("{0:c}", feeTotal + OrderTotalCost)));
            }
            else
            {
                masterSection.nameValueList.Add(new NameValueTemplate("#OrderTotalText#", "Order Total*"));
                masterSection.nameValueList.Add(new NameValueTemplate("#OpenOrderFeeCommentTag#", "<!--"));
                masterSection.nameValueList.Add(new NameValueTemplate("#CloseOrderFeeCommentTag#", "-->"));
            }
           
            // get logs from all parts of order
            List<LogType> logList = new List<LogType>();
            foreach (LogType logItem in growerOrder.Logs)
            {
                if (!logItem.IsInternal)
                    logList.Add(logItem);
            }
            foreach (var supplier in growerOrder.SupplierOrders)
            {
                foreach (var logItem in supplier.Logs)
                    if(!logItem.IsInternal)
                        logList.Add(logItem);
                foreach (var orderLine in supplier.OrderLines)
                {
                    foreach (var logItem in orderLine.Logs)
                        if (!logItem.IsInternal)
                            logList.Add(logItem);
                }
            }

            // if no external log events, add empty log event to clear section start/end tags
            if (logList.Count == 0)
            {
                LogType logItem = new LogType();
                logItem.LogTimeString = "";
                logItem.Text = "";
                logItem.PersonName = "";
                logList.Add(logItem);
            }
            else
            {
                logList = logList.OrderBy(x => DateTime.Parse(x.LogTimeString)).ToList();
            }

            foreach (LogType logItem in logList)
            {
                NameValueSection logSection = new NameValueSection("#StartChangeLog#", "#EndChangeLog#");
                //logSection.nameValueList.Add(new NameValueTemplate("#ChangeLogDate#", logItem.LogTimeString));
                if (logItem.LogTimeString != "")
                {
                    logSection.nameValueList.Add(new NameValueTemplate("#ChangeLogDate#", logItem.LogTimeString));
                }
                else
                    logSection.nameValueList.Add(new NameValueTemplate("#ChangeLogDate#", ""));

                logSection.nameValueList.Add(new NameValueTemplate("#ChangeLogChange#", logItem.Text.Replace("\n", "<br/>")));
                var personName = logItem.PersonName;
                //if (logItem.PersonName == ePSAccount.PersonName)
                   // personName = logItem.UserCode;
                logSection.nameValueList.Add(new NameValueTemplate("#ChangeLogChangedBy#", personName));
                masterSection.childSectionList.Add(logSection);
            }
        


        }

        //public string ApplyTemplate()
        //{
        //    // process the global vars
        //    string text = File.ReadAllText(templateFilePath);
        //    foreach(var nvItem in masterSection.nameValueList)
        //    {
        //        text = text.Replace(nvItem.Name, nvItem.Value);
        //    }

        //    // process each top level section
        //    int stIndx = -1;
        //    int edIndx = -1;
        //    string sectionText = "";
        //    foreach (var section in masterSection.childSectionList)
        //    {
        //        if (stIndx <= 0)
        //        {
        //            stIndx = text.IndexOf(section.StartSectionMark) + section.StartSectionMark.Length;
        //            edIndx = text.IndexOf(section.EndSectionMark);
        //            sectionText = text.Substring(stIndx, edIndx - stIndx);
        //            stIndx -= section.StartSectionMark.Length;
        //            edIndx += section.EndSectionMark.Length;
        //            text = text.Remove(stIndx, edIndx - stIndx);
        //        }

        //        // process any child sections in the section text
        //        string sectionCopy = String.Copy(sectionText);
        //        int childstIndx = -1;
        //        int childedIndx = -1;
        //        string childsectionText = "";
        //        foreach (var childSection in section.childSectionList)
        //        {

        //            if (childstIndx <= 0)
        //            {
        //                childstIndx = sectionCopy.IndexOf(childSection.StartSectionMark) + childSection.StartSectionMark.Length;
        //                childedIndx = sectionCopy.IndexOf(childSection.EndSectionMark);
        //                childsectionText = sectionCopy.Substring(childstIndx, childedIndx - childstIndx);
        //                childstIndx -= childSection.StartSectionMark.Length;
        //                childedIndx += childSection.EndSectionMark.Length;
        //                sectionCopy = sectionCopy.Remove(childstIndx, childedIndx - childstIndx);
        //            }
        //            string childSectionCopy = String.Copy(childsectionText);

        //            foreach (var nvItem in childSection.nameValueList)
        //            {
        //                childSectionCopy = childSectionCopy.Replace(nvItem.Name, nvItem.Value);
        //            }
        //            sectionCopy = sectionCopy.Insert(childstIndx, childSectionCopy);
        //            childstIndx += childSectionCopy.Length;
        //        }


        //        foreach (var nvItem in section.nameValueList)
        //        {
        //            sectionCopy = sectionCopy.Replace(nvItem.Name, nvItem.Value);
        //        }
        //        text = text.Insert(stIndx, sectionCopy);
        //        stIndx += sectionCopy.Length;               
        //    }


        //    return text;
        //}

        public string ApplyTemplate()
        {
            // process the global vars
            string text = File.ReadAllText(templateFilePath);
            try
            {
               
                foreach (var nvItem in masterSection.nameValueList)
                {
                    text = text.Replace(nvItem.Name, nvItem.Value);
                }

                // process each top level section
                int stIndx = -1;
                int edIndx = -1;
                string sectionText = "";
                string sectionMark = "";
                foreach (var section in masterSection.childSectionList)
                {
                    if (stIndx <= 0 || (section.StartSectionMark != sectionMark && sectionMark != ""))
                    {
                        sectionMark = section.StartSectionMark;

                        //test if section present in this template
                        if (text.IndexOf(section.StartSectionMark) <= 0)
                            break;

                        stIndx = text.IndexOf(section.StartSectionMark) + section.StartSectionMark.Length;
                        edIndx = text.IndexOf(section.EndSectionMark);
                        sectionText = text.Substring(stIndx, edIndx - stIndx);
                        stIndx -= section.StartSectionMark.Length;
                        edIndx += section.EndSectionMark.Length;
                        text = text.Remove(stIndx, edIndx - stIndx);
                    }

                    // process any child sections in the section text
                    string sectionCopy = String.Copy(sectionText);
                    int childstIndx = -1;
                    int childedIndx = -1;
                    string childsectionText = "";
                    string startSectionMark = "";
                    foreach (var childSection in section.childSectionList)
                    {
                        if (childstIndx <= 0 || (childSection.StartSectionMark != startSectionMark && startSectionMark != ""))
                        {
                            startSectionMark = childSection.StartSectionMark;

                            // test if section present in this template
                            if (sectionCopy.IndexOf(childSection.StartSectionMark) <= 0)
                                break;

                            childstIndx = sectionCopy.IndexOf(childSection.StartSectionMark) + childSection.StartSectionMark.Length;
                            childedIndx = sectionCopy.IndexOf(childSection.EndSectionMark);
                            childsectionText = sectionCopy.Substring(childstIndx, childedIndx - childstIndx);
                            childstIndx -= childSection.StartSectionMark.Length;
                            childedIndx += childSection.EndSectionMark.Length;
                            sectionCopy = sectionCopy.Remove(childstIndx, childedIndx - childstIndx);
                        }
                        string childSectionCopy = String.Copy(childsectionText);

                        foreach (var nvItem in childSection.nameValueList)
                        {
                            childSectionCopy = childSectionCopy.Replace(nvItem.Name, nvItem.Value);
                        }
                        sectionCopy = sectionCopy.Insert(childstIndx, childSectionCopy);
                        childstIndx += childSectionCopy.Length;

                    }


                    foreach (var nvItem in section.nameValueList)
                    {
                        sectionCopy = sectionCopy.Replace(nvItem.Name, nvItem.Value);
                    }
                    text = text.Insert(stIndx, sectionCopy);
                    stIndx += sectionCopy.Length;
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
                throw new Exception(ex.Message);
            }
            return text;
        }

       
    }
}