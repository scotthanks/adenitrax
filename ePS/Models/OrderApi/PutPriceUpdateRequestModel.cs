﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.OrderApi
{
    public class PutPriceUpdateRequestModel : RequestBase
    {
        public Guid GrowerOrderGuid { get; set; }
        public Guid OrderLineGuid { get; set; }

        public override void Validate()
        {
            if (GrowerOrderGuid == Guid.Empty && OrderLineGuid == Guid.Empty)
            {
                AddMessage("Either GrowerOrderGuid or OrderLineGuid is required.");
            }
        }
    }
}