﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using Utility;

namespace ePS.Models.RegistrationApi
{
    [Obsolete]
    public class PutResponseModel
    {
        public List<FormErrorType> FormErrorList { get; set; }

        public bool Success
        {
            get { return !FormErrorList.Any(); }
        }

        public PutResponseModel(PutRequestModel request)
        {
            FormErrorList = new List<FormErrorType>();

            request.CompanyName = PreprocessStringField("CompanyName", request.CompanyName, isRequired: true);
            request.StreetAddress1 = PreprocessStringField("StreetAddress1", request.StreetAddress1, isRequired: true);
            request.StreetAddress2 = PreprocessStringField("StreetAddress2", request.StreetAddress2, isRequired: false);
            request.City = PreprocessStringField("City", request.City, isRequired: true);
            request.ZipCode = PreprocessStringField("ZipCode", request.ZipCode, isRequired: true);
            request.CompanyEmail = PreprocessStringField("CompanyEmail", request.CompanyEmail, isRequired: true);
            request.CompanyPhone = PreprocessStringField("CompanyPhone", request.CompanyPhone, isRequired: true);
            request.CompanyTypeCode = PreprocessStringField("CompanyTypeCode", request.CompanyTypeCode, isRequired: true);
            request.FirstName = PreprocessStringField("FirstName", request.FirstName, isRequired: true);
            request.LastName = PreprocessStringField("LastName", request.LastName, isRequired: true);
            request.PersonEmail = PreprocessStringField("Email", request.PersonEmail, isRequired: true);
            request.Password = PreprocessStringField("Password", request.Password, isRequired: true);
            request.Password2 = PreprocessStringField("Password2", request.Password, isRequired: true);
            request.PersonPhone = PreprocessStringField("PersonPhone", request.PersonPhone, isRequired: true);
            request.RoleCode = PreprocessStringField("RoleCode", request.RoleCode, isRequired: true);
            
            if (Success)
            {
                var emailRegEx =
                    new System.Text.RegularExpressions.Regex(
                        @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");

                if (!emailRegEx.IsMatch(request.PersonEmail))
                {
                    FormErrorList.Add(new FormErrorType("The email address \"{0}\" is not a valid address.", "Email", request.PersonEmail));
                }
            }

            PersonService personService = new PersonService();

            if (Success)
            {
                if (personService.Exists(request.PersonEmail))
                {
                    FormErrorList.Add(new FormErrorType(personService.ExistsReasonMessage(request.PersonEmail), "Email", request.PersonEmail));
                }
            }

            //if (Success)
            //{
            //    var growerService = new GrowerService();

            //    if (growerService.ExistsSimilar(request.CompanyName, request.ZipCode, new PhoneNumber(request.CompanyPhone)))
            //    {
            //        FormErrorList.Add(new FormErrorType("A company with the same name and Zip Code or phone number is already on file.", "CompanyName", request.CompanyName));                    
            //    }

            //    //if (Success)
            //    //{
            //    //    if (growerService.EmailExists(request.PersonEmail))
            //    //    {
            //    //        FormErrorList.Add(new FormErrorType("This email is already in use by a company.", "Email", request.PersonEmail));
            //    //    }
            //    //}
            //}

            if (Success)
            {
                string message;

                var returnedPerson = personService.Register
                    (
                        request.CompanyName,
                        request.StreetAddress1,
                        request.StreetAddress2,
                        request.City,
                        request.StateCode,
                        request.ZipCode,
                        request.CompanyEmail,
                        request.CompanyPhone,
                        request.CompanyTypeCode,
                        request.FirstName,
                        request.LastName,
                        request.PersonEmail,
                        request.Password,
                        request.PersonPhone,
                        request.RoleCode,
                        request.SubscribeToNewsLetter,
                        request.SubscribeToPromotions
                    );

                if (Success)
                {
                    var userProfileService = new UserProfileService();

                    bool success = userProfileService.CreateUser(request.PersonEmail, request.Password, returnedPerson.Grower.Guid, out message);

                    if (!success)
                    {
                        FormErrorList.Add(new FormErrorType(message, "Email", request.PersonEmail));
                    }
                    else
                    {
                        var userProfile = userProfileService.Get(request.PersonEmail);

                        returnedPerson.UserGuid = userProfile.UserGuid;

                        success = personService.SetUserGuid(returnedPerson.PersonGuid, userProfile.UserGuid);

                        if (!success)
                        {
                            FormErrorList.Add(new FormErrorType("Person could not be connected to the profile.", "Email"));

                           // UndoRegistrationAndRemoveProfile(personService, userProfileService, userProfile.UserName);
                        }
                        else
                        {
                            success = 
                                userProfileService.GrantRole
                                (
                                    userProfile.UserName,
                                    returnedPerson.IsAuthorizedToSeePrices,
                                    returnedPerson.IsAuthorizedToPlaceOrders,
                                    returnedPerson.IsAuthorizedToSeePrices
                                );

                            if (!success)
                            {
                                FormErrorList.Add(new FormErrorType("Unable to grant role.", "CompanyTypeCode"));
                              //  UndoRegistrationAndRemoveProfile(personService, userProfileService, userProfile.UserName);   
                            }
                        }
                    }
                }
            }
        }

       

        private string PreprocessStringField(string fieldName, string field, bool isRequired)
        {
            string cleanField = field;

            if (cleanField == null)
            {
                cleanField = "";
            }

            cleanField = cleanField.Trim();

            if (isRequired && string.IsNullOrWhiteSpace(cleanField))
            {
                FormErrorList.Add(new FormErrorType("The field {0} is required", fieldName));
            }

            return cleanField;
        }
    }
}