﻿using System;
using System.Collections.Generic;

using BusinessObjectsLibrary;

namespace ePS.Models.PatientApi
{
    public class GetClinicPatients : ResponseBase
    {
        public GetClinicPatients(Guid userGuid)
            : base(userGuid, requiresAuthentication: false)
        {
           
                
                //var service = new BusinessObjectServices.GrowerOrderService();
                //var theSupplierOrderFees = service.GetSupplierOrderFees(request.SupplierOrderGuid);
            var patientWounds = new List<ClinicPatient>();
               // foreach (BusinessObjectsLibrary.SupplierOrderFee theFee in theSupplierOrderFees)
               // {
            var wound = new ClinicPatient();
            wound.Guid = Guid.Empty;
            wound.PatientFirstName = "first";
            wound.PatientLastName = "last";
            wound.WoundShortName = "knee";
            wound.WoundDescription = "Bad cut on knee";
            wound.DisplayName = wound.PatientLastName + ", " + wound.PatientFirstName + "(" + wound.WoundShortName + ")";

         

            patientWounds.Add(wound);
           
             
                    
                
                    
            this.PatientWounds = patientWounds;
            this.Success = true;
            this.SuccessMessage = "Patient List Returned";
                            
                
            
        }

        public List<ClinicPatient> PatientWounds { get; private set; }
    }

}