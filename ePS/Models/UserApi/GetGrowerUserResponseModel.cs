﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;

namespace ePS.Models.UserApi
{
    public class GetGrowerUserResponseModel : ResponseBase
    {


        public GetGrowerUserResponseModel(Guid userGuid)
            : base(userGuid, requiresAuthentication: true)
        {
            var message = "";
            UserProfileService userProfileService = new UserProfileService();
            
            List<GrowerUser> growerUserList = new List<GrowerUser>();

            PersonService personService = new PersonService();
            
            var userPerson = personService.GetPersonFromUser(userGuid);
            var userProfile = userProfileService.GetUserProfile(userPerson.Email, out message);
            var userRole = userProfileService.GetUserRole(userProfile.UserId);
            if (userRole.RoleName == "CustomerAdministrator")
            {
                isAdmin = true;
            }
            else { isAdmin = false; }

            var personList = personService.GetGrowerPersons(userPerson.GrowerGuid);

            foreach (var person in personList)
            {

                var newGrowerUser = new GrowerUser();
                newGrowerUser.GrowerUserGuid = person.UserGuid;
                newGrowerUser.FirstName = person.FirstName;
                newGrowerUser.LastName = person.LastName;
                newGrowerUser.Email = person.Email;
                newGrowerUser.Phone = person.Phone;
                newGrowerUser.CompanyRole = person.PersonTypeLookupCode;
                newGrowerUser.SitePermission = person.UserRole;
       

                if (person.UserGuid == userGuid)
                {
                    newGrowerUser.IsCaller = true;
                }

                growerUserList.Add(newGrowerUser);
            }

            growerUsers = growerUserList;
           currentUserGuid = userGuid;
            if (growerUsers.Count > 0)
            {
                Success = true;
            }

        }

        public List<GrowerUser> growerUsers { get; set; }
        public Guid currentUserGuid { get; set; }
        public bool isAdmin{get; set; }
    }
}