﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjectServices;
using BusinessObjectsLibrary;

namespace ePS.Models.UserApi
{
    public class DeleteGrowerUserResponseModel : ResponseBase
    {

        public DeleteGrowerUserResponseModel(Guid userGuid, Guid growerUserGuid)
         : base(userGuid, requiresAuthentication: true)
        {

            PersonService personService = new PersonService();
            Success = personService.Deactivate(growerUserGuid);

        }
    }
}