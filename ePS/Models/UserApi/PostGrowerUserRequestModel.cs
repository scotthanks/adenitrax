﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.UserApi
{
    public class PostGrowerUserRequestModel
    {

        public Guid GrowerUserGuid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
       // public Utility.PhoneNumber Phone { get; set; }
        public string CompanyRole { get; set; }
        public string SitePermission { get; set; }

    }
}