﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.EmailPreferencesApi
{
    public class PutRequestModel : RequestBase
    {
        public override void Validate()
        {
            //ToDo: Validate the Guid Passed in - check to see that record exists
            if (this.EmailPreference.Guid == Guid.Empty)
            {
                this.AddMessage("Guid Supplied is not Valid");

            }


            //ToDo: validate the data submitted 
            if (string.IsNullOrEmpty(this.EmailPreference.Email))
            {
                this.AddMessage("Email address is required.");

            }
        }

        public EmailPreferenceType EmailPreference { get; set; }

    }
}