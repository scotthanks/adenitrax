﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessObjectServices;
using BusinessObjectsLibrary;
using ePS.Types;

namespace ePS.Models.ClaimApi
{
    public class GetResponseSummaryModel : ResponseBase
    {
        public GetResponseSummaryModel(Guid userGuid, GetSummaryRequestModel summaryRequest)
            : base(userGuid, requiresAuthentication: true)
        {
            if (ValidateAuthenticationAndRequest(summaryRequest))
            {
                //Retrieve Data from Data Server
                var ClaimService = new ClaimService();
                List<ClaimSummary> claimSummaryList;

                GetSummaryRequestModel.RequestTypeEnum requestType;
                bool valid = Enum.TryParse(summaryRequest.RequestType, ignoreCase: true, result: out requestType);
                if (!valid)
                {
                    throw new ApplicationException(string.Format("Request type {0} is not valid.", summaryRequest.RequestType));
                }

                if (requestType == GetSummaryRequestModel.RequestTypeEnum.SummaryData)
                    requestType = GetSummaryRequestModel.RequestTypeEnum.MyClaims;

                switch (requestType)
                {
                    case GetSummaryRequestModel.RequestTypeEnum.MyClaims:
                        claimSummaryList = ClaimService.GetMyClaims(userGuid);
                        break;

                    case GetSummaryRequestModel.RequestTypeEnum.AllClaims:
                        claimSummaryList = ClaimService.GetMyClaims(userGuid); //need to switch to all
                        break;
                    default:
                        throw new ApplicationException(string.Format("Request type {0} is not implemented.", requestType.ToString()));
                       
                }

                //Populate List
                var claimSummaryListItems = new List<ClaimSummaryType>();
                foreach (ClaimSummary item in claimSummaryList)
                {
                    var summaryItem = new ClaimSummaryType
                    {
                        ClaimGuid = item.ClaimGuid,
                        OrderNo = item.OrderNo,
                        ClaimNo = item.ClaimNo,
                        ClaimAmount= item.ClaimAmount,
                        ShipWeekString = item.ShipWeek.ShipWeekString,
                        ProgramTypeName = item.ProgramTypeName,
                        ProductFormCategoryName = item.ProductFormCategoryName,
                        ClaimStatusName = item.ClaimStatusName

                    };

                    claimSummaryListItems.Add(summaryItem);
                }
              



                SummaryData = claimSummaryListItems;

                Success = true;
            }
        }

        public IEnumerable<ClaimSummaryType> SummaryData { get; set; }
    }
}