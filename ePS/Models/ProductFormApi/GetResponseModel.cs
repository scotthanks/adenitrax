﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using BusinessObjectServices;


namespace ePS.Models.ProductFormApi
{
    public class GetResponseModel
    {
        public GetResponseModel(GetRequestModel request)
        {
            var service = new BusinessObjectServices.ProductFormService();

            var productFormList = service.GetProductForms(supplierGuid: request.SupplierGuid, productFormCategoryGuid:request.ProductFormCategoryGuid, active:request.Active);

            var list = new List<GetResponseProductFormType>();
            foreach (var productForm in productFormList)
            {
                var item = new GetResponseProductFormType
                {
                    Guid = productForm.Guid,
                    SupplierGuid = productForm.SupplierGuid,
                    ProductFormCategoryGuid = productForm.ProductFormCategoryGuid,
                    Code = productForm.Code,
                    Name = productForm.Name,
                    SalesUnitQty = productForm.SalesUnitQty,
                    TagBundleQty = productForm.TagBundleQty,
                    LineItemMinimumQty = productForm.LineItemMinimumQty,
                    IsActive = productForm.IsActive
                };

                list.Add(item);
            }

            this.ProductForms = list;
        }

        public IEnumerable<GetResponseProductFormType> ProductForms { get; set; }
    }
}