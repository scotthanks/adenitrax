﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ProductFormApi
{
    public class GetResponseProductFormType
    {
        public Guid Guid { get; set; }
        public Guid? SupplierGuid { get; set; }
        public Guid ProductFormCategoryGuid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int SalesUnitQty { get; set; }
        public int TagBundleQty { get; set; }
        public int LineItemMinimumQty { get; set; }
        public bool IsActive { get; set; }
    }
}