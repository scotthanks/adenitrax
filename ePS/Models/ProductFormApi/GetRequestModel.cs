﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.ProductFormApi
{
    public class GetRequestModel
    {
        public Guid? SupplierGuid { get; set; }
        public Guid? ProductFormCategoryGuid { get; set; }
        public bool? Active { get; set; }
    }
}
