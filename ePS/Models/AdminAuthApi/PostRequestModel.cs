﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;



namespace ePS.Models.AdminAuthApi
{
    public class PostRequestModel
    {


        public Guid token { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}