﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebMatrix.WebData;


namespace ePS.Models.AdminAuthApi
{



    public class PostResponseModel
    {
            public PostResponseModel(PostRequestModel request) {
        
                request.username = request.username ?? string.Empty;
                request.password = request.password ?? string.Empty;

                var isAuthenticated = WebSecurity.Login(request.username, request.password);


                this.Success = isAuthenticated;

        }

        public bool Success { get; set; }

    }
}