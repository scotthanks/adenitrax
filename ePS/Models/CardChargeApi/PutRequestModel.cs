﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePS.Models.CardChargeApi
{
    public class PutRequestModel
    {

          public string CardNumber {get; set;}
          public decimal Amount { get; set; }
          public string Description { get; set; }
          public string Expires { get; set; }

    }


}