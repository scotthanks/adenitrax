﻿model = {};
model.config = {
    urlGetAllowSubstitutions: "/api/OrderPreferences/AllowSubstitutions",
    urlPutAllowSubstitutions: "/api/OrderPreferences",
};

model.getAllowSubstitutions = function (onSuccess, onError) {

    $.ajax({
        url: model.config.urlGetAllowSubstitutions,
        type:"get",
        dataType: "json",
        timeout: 10000,
        beforeSend: function () {
        },
        success: onSuccess,
        error: onError
    });




};

model.putAllowSubstitutions = function (allowSubstituions, onSuccess, onError) {
    if (typeof allowSubstituions !== "boolean") { return false; };
    $.ajax({
        url: model.config.urlPutAllowSubstitutions + "/" + allowSubstituions,
        type: "put",
        dataType: "json",
        //data: { "newValue": allowSubstituions },
        timeout: 10000,
        beforeSend: function () {
        },
        success: onSuccess,
        error: onError
    });



};



viewModel = {};
viewModel.config = {
    selectors: {
        orderSubstitutionsRadioDisplayId: "order_substitutions_radio_group",
        orderSubstitutionsApprovedId: "order_substitutions_approved",
        orderSubstitutionsDeclinedId: "order_substitutions_declined",
        selectedClass: "selected",
        unselectedClass: "unselected",
    },
    buttons: {
        savePreferencesId: "order_preferences_save_button",
    },

};

viewModel.state = {
    orderSubstitutionsApproved: function () { return $("#" + viewModel.config.selectors.orderSubstitutionsApprovedId).hasClass(viewModel.config.selectors.selectedClass); },

};

viewModel.Init = function () {


    var onSuccess = function (data, textStatus, jqXHR) {
        if (!data.AllowSubstitutions) {
            //Substitutions not allowed - reset from default
            $("#" + viewModel.config.selectors.orderSubstitutionsApprovedId).removeClass(viewModel.config.selectors.selectedClass).addClass(viewModel.config.selectors.unselectedClass);
            $("#" + viewModel.config.selectors.orderSubstitutionsDeclinedId).removeClass(viewModel.config.selectors.unselectedClass).addClass(viewModel.config.selectors.selectedClass);
        };
        
    };
    var onError = function (jqXHR, textStatus, errorThrown) {
        //leave default in place
        eps_tools.FloatingMessage(null, null, "Error: Unable to retrieve value.", 2500);
    };
    model.getAllowSubstitutions(onSuccess, onError);





    //////////////// Action Buttons ///////////////////////
    $("#" + viewModel.config.buttons.savePreferencesId).on("click", function () {
        //alert("Order Substitutions Approved: " + viewModel.state.orderSubstitutionsApproved);
        window.location.href = "../../MyAccount";
    });


    //////////////////// Core Events //////////////////////////////
    //$("." + viewModel.config.selectors.selectedClass + ", ." + viewModel.config.selectors.unselectedClass).off().on("click", function () {
    $("#" + viewModel.config.selectors.orderSubstitutionsApprovedId + ", #" + viewModel.config.selectors.orderSubstitutionsDeclinedId).off().on("click", function () {
        if ($(this).hasClass(viewModel.config.selectors.unselectedClass)) {
            //alert("change being made");
            $("#" + viewModel.config.selectors.orderSubstitutionsRadioDisplayId).children("span." + viewModel.config.selectors.selectedClass).removeClass(viewModel.config.selectors.selectedClass).addClass(viewModel.config.selectors.unselectedClass);
            $(this).removeClass(viewModel.config.selectors.unselectedClass).addClass(viewModel.config.selectors.selectedClass);
            viewModel.orderSubstitutionsChange($(this));
        };
    });


};

viewModel.orderSubstitutionsChange = function (element, callbacks) {
    //alert(viewModel.state.orderSubstitutionsApproved());
    var position = element.offset();
    var y = Math.floor(position.top + ((element.height() - 35) / 2));
    var x = Math.floor(position.left + 50);
    var onSuccess = function (data, textStatus, jqXHR) {
        //alert(traverseObj(data, false));
        var msg = "Preference updated.";
        if (!data.IsAuthenticated) {
            msg = "Must be logged in.";
        } else if (!data.Success) {
            msg = data.Messages[0] || null;
        };
        eps_tools.FloatingMessage(x, y, msg, 2500);
    };
    var onError = function (jqXHR, textStatus, errorThrown) {
        eps_tools.FloatingMessage(x, y, "Status: " + textStatus + "Error: " + errorThrown, 2500);
    };
    model.putAllowSubstitutions(viewModel.state.orderSubstitutionsApproved(), onSuccess, onError);
};

/////////////// Utilities //////////////////////////////
viewModel.floatMessage = function (element, content, interval) {
    //floats message to right of element
    //if no element then does nothing
    if (typeof element === 'undefined') { return false; };

    var top, left;
    if (element == null) {
        //Leave values null
        top = null;
        left = null;
    }
    else if (element.length > 0) {
        var position = element.offset();
        //ToDo: code to center floating message vertically with element
        //assume floating message is 35 px tall
        top = Math.floor(position.top + ((element.height() - 35) / 2));
        left = Math.floor(position.left + element.width() + 30);
    };
    eps_tools.FloatingMessage(left, top, content, interval);
};
viewModel.ProcessEventChain = function (callbacks) {

    if (callbacks != undefined && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};
viewModel.ShipWeekCodeToShipWeekString = function (code) {
    if (typeof code !== "string" || code == null || code.length != 6) { return ""; };
    return code.substring(4, 6) + "/" + code.substring(0, 4);
    //return code;
};
////////////////////////////////////////////////////////


