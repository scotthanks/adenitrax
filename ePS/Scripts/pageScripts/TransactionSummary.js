﻿model = {};

model.InvoicesData = {};
model.InvoiceData = {};
model.PaymentsData = {};
model.PayData = {};

model.config = {
    api: {
        urlInvoicesDataApi: "/api/ledger/invoices/",
        urlInvoiceDataApi: function (orderGuid) { return "/api/ledger/invoice?OrderGuid=" + orderGuid },
        urlPaymentsDataApi:"/api/ledger/payments",
        urlInvoicePayApi: "/api/ledger/invoice/pay/",
    },
};

model.state = {
    returnAccountBalance: function () {
        return model.InvoicesData.AccountBalance || null;
    },

};

model.getInvoicesData = function (callbacks) {
    //alert(callbacks);
    //alert("bingo");
    $("#test_area_results_display_0, #test_area_results_display_1").empty();
    $.ajax({
        datatype: "json",
        url: model.config.api.urlInvoicesDataApi,
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //clear current state
            $("#test_area_results_display_0").html("<span>[" + this.type + "] URI: " + this.url + "</span>");
            model.InvoicesData = {};
        },
        success: function (response, textStatus, jqXHR) {
            model.InvoicesData = response;
            $("#test_area_results_display_1").html("Result:<br />" + traverseObj(model.InvoicesData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.processEventChain(callbacks);
        }
    });

};
model.getInvoiceData = function (orderGuid, callbacks) {
    //alert("bingo");
    $("#test_area_results_display_0, #test_area_results_display_1").empty();
    $.ajax({
        datatype: "json",
        url: model.config.api.urlInvoiceDataApi(orderGuid),
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //clear current state
            $("#test_area_results_display_0").html("<span>[" + this.type + "] URI: " + this.url + "</span>");
            model.InvoiceData = {};
        },
        success: function (response, textStatus, jqXHR) {
            model.InvoiceData = response;
            $("#test_area_results_display_1").html("Result:<br />" + traverseObj(model.InvoiceData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.processEventChain(callbacks);
        }
    });

};
model.getPaymentsData = function (callbacks) {
    //alert("bingo");
    $("#test_area_results_display_0, #test_area_results_display_1").empty();
    $.ajax({
        datatype: "json",
        url: model.config.api.urlPaymentsDataApi,
        type: "get",
        beforeSend: function (jqXHR, settings) {
            //clear current state
            $("#test_area_results_display_0").html("<span>[" + this.type + "] URI: " + this.url + "</span>");
            model.PaymentsData = {};
        },
        success: function (response, textStatus, jqXHR) {
            model.PaymentsData = response;
            $("#test_area_results_display_1").html("Result:<br />" + traverseObj(model.PaymentsData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.processEventChain(callbacks);
        }
    });

};
model.invoicePay = function (data, callbacks) {
    //alert(data);
    $("#test_area_results_display_0, #test_area_results_display_1").empty();
    $.ajax({
        datatype: "json",
        url: model.config.api.urlInvoicePayApi,
        type: "post",
        data: data,
        beforeSend: function (jqXHR, settings) {
            //clear current state
            $("#test_area_results_display_0").html("<span>[" + this.type + "] URI: " + this.url + "</span>");
            $("#test_area_results_display_0").append("<p>" + this.data + "</p>");
            model.PayData = {};
        },
        success: function (response, textStatus, jqXHR) {
            model.PayData = response;
            $("#test_area_results_display_1").html("Result:<br />" + traverseObj(model.PayData));
        },
        //error: function (jqXHR, textStatus, errorThrown) {
        //    alert("url: " + this.url + "\ntextStatus: " + textStatus + "\nerrorThrown " + errorThrown);
        //},
        complete: function (jqXHR, textStatus) {

            viewModel.processEventChain(callbacks);
        }
    });

};



viewModel = {};
viewModel.config = {
    pageMode: "all",
    statementOfAccountDisplay: {
        HeaderBarId: "statement_of_account_accordion_header_bar",
        AccordionDisplayId: "statement_of_account_accordion_display",
        AccordionExpandRate: function () { return 800 + (viewModel.state.invoicesCount() * 50); },
        tableId: "statement_of_account_table",
        columnsCount: 7,
        amountDueId: "total_amount_due",
    },
    transactionHistoryDisplay: {
        HeaderBarId: "transaction_history_accordion_header_bar",
        AccordionDisplayId: "transaction_history_accordion_display",
        AccordionExpandRate: function () { return 800 + (viewModel.state.paymentsCount() * 50); },
        tableId: "transaction_history_table",
        columnsCount: 6,
    },
};
viewModel.state = {
    statementOfAccountAccordionExpanded: true,
    transactionHistoryAccordionExpanded: true,
    invoicesCount: function () {
        model.InvoicesData.length || 0;
    },
    paymentsCount: function () {
        model.PaymentsData.length || 0;
    },
};


viewModel.Init = function () {
    //alert("init");
    //////  Define Core Events /////// 
    $("#" + viewModel.config.statementOfAccountDisplay.HeaderBarId).off().on("click", function () {
        viewModel.statementOfAccountAccordionToggle();
    });
    $("#" + viewModel.config.transactionHistoryDisplay.HeaderBarId).off().on("click", function () {
        viewModel.transactionHistoryAccordionToggle();
    });
    //alert("Page Mode: " + viewModel.config.pageMode);
    if (viewModel.config.pageMode == "statement") {
        viewModel.transactionHistoryAccordionCollapse();
        viewModel.statementOfAccountAccordionExpand();
    } else if (viewModel.config.pageMode == "history") {
        viewModel.statementOfAccountAccordionCollapse();
        viewModel.transactionHistoryAccordionExpand();
    } else {
        viewModel.config.pageMode = "all";
        viewModel.statementOfAccountAccordionExpand();
        viewModel.transactionHistoryAccordionExpand();
    };

    


    //////////////////// Test Buttons ///////////////////////////////
    $("#test_api_invoices").button().on("click", function () {
        model.getInvoicesData();
    });
    $("#test_api_payments").button().on("click", function () {
        model.getPaymentsData();
    });
    $("#test_api_invoice").button().on("click", function () {
        var orderGuid = "00000000-0000-0000-0000-000000000000";
        model.getInvoiceData(orderGuid);
    });
    $("#test_api_pay").button().on("click", function () {

        var data = {
            GrowerGuid: "00000000-0000-0000-0000-000000000000",
            OrderGuid: "00000000-0000-0000-0000-000000000000",
            CCReference: "09A58885"

        };
        model.invoicePay(data);
    });


};

viewModel.transactionHistoryRefresh = function () {

    var displayData = function () {

        var target = $("#" + viewModel.config.transactionHistoryDisplay.tableId + " tbody").empty();

        $.each(model.PaymentsData.TransactionList, function (i, transaction) {

            target.append(viewModel.transactionHistoryComposeRow(i, transaction));
            //target.append("<tr><td>???</td></tr>");

        });

        var due = model.state.returnAccountBalance();
        $("#" + viewModel.config.transactionHistoryDisplay.amountDueId).text("$" + numeral(due).format('0,0,0.00'));
    };

    model.getPaymentsData([displayData]);

};
viewModel.transactionHistoryComposeRow = function (rowNdx, transaction) {
    //alert("transactionHistoryComposeRow");
    var row = $("<tr />")
    .prop({
        "class": "tr_" + rowNdx
    });
    var colCount = viewModel.config.transactionHistoryDisplay.columnsCount;

    for (var i = 0; i < colCount; i++) {
        var cell = $("<td />")
        .prop({
            "class": "td_" + i
        });

        var cursor = transaction.TransactionType == "Invoice" ? "pointer" : "normal";
        cell.css({
            "cursor": cursor
        });

        switch (i) {
            case 0:
                cell.append(transaction.TransactionType || null);
                break;
            case 1:
                cell.append(transaction.TransactionId || null);
                break;
            case 2:
                cell.append(transaction.OrderNumber || null);
                break;
            case 3:
                var span = $("<span />")
                .prop({
                    "class": "money_displays"
                })
                .text(numeral(transaction.Amount).format('0,0,0.00'));
                cell.append(span);
                break;
            case 4:
                var d = new Date(transaction.Date);
                var date = d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear();
                var span = $("<span />")
                    .prop({
                        "class": "date_displays"
                    })
                    .text(date);
                cell.append(span);
                break;
            case 5:
                cell.append(transaction.PaymentId || null);
                break;
            default:
                cell.append("??");
        }


        row.append(cell);

    };

    return row;
};

viewModel.transactionHistoryAccordionExpand = function () {

    //set icon working
    $("#" + viewModel.config.transactionHistoryDisplay.HeaderBarId).css({ "background-image": "url('/images/icons/Loading_Availability.gif')" });

    viewModel.transactionHistoryRefresh();
    var target = $("#" + viewModel.config.transactionHistoryDisplay.AccordionDisplayId);

    target.show("blind", {}, viewModel.config.transactionHistoryDisplay.AccordionExpandRate(), function () {
        $("#" + viewModel.config.transactionHistoryDisplay.HeaderBarId).css({ "background-image": "url('/images/layout/MyAccountArrowDown.png')" });
        viewModel.state.transactionHistoryAccordionExpanded = true;
    });

};
viewModel.transactionHistoryAccordionCollapse = function () {

    var display = $("#" + viewModel.config.transactionHistoryDisplay.AccordionDisplayId);
    display.hide("blind", {}, viewModel.config.transactionHistoryDisplay.AccordionExpandRate(), function () {
        $("#" + viewModel.config.transactionHistoryDisplay.HeaderBarId).css({ "background-image": "url('/images/layout/cart_header_pointer.png')" });
        viewModel.state.transactionHistoryAccordionExpanded = false;
    });

};
viewModel.transactionHistoryAccordionToggle = function () {
    //alert("expanded: "+viewModel.state.transactionHistoryAccordionExpanded);
    if (viewModel.state.transactionHistoryAccordionExpanded) {
        viewModel.transactionHistoryAccordionCollapse();
    } else {
        viewModel.transactionHistoryAccordionExpand();
    };

};


viewModel.statementOfAccountRefresh = function () {

    var displayData = function () {

        var target = $("#" + viewModel.config.statementOfAccountDisplay.tableId + " tbody").empty();

        $.each(model.InvoicesData.TransactionList, function (i, transaction) {

            target.append(viewModel.statementOfAccountComposeRow(i, transaction));

        });

        var due = model.state.returnAccountBalance();
        $("#" + viewModel.config.statementOfAccountDisplay.amountDueId).text("$" + numeral(due).format('0,0,0.00'));
    };

    model.getInvoicesData([displayData]);

};
viewModel.statementOfAccountComposeRow = function (rowNdx, transaction) {
    //alert("statementOfAccountComposeRow");
    var row = $("<tr />")
    .prop({
        "class": "tr_" + rowNdx
    });
    var colCount = viewModel.config.statementOfAccountDisplay.columnsCount;

    for (var i = 0; i < colCount; i++) {
        var cell = $("<td />")
        .prop({
            "class": "td_" + i
        });

        var cursor = transaction.TransactionType == "Invoice" ? "pointer" : "normal";
        cell.css({
            "cursor": cursor
        });

        switch(i)
        {
            case 0:
                cell.append(transaction.TransactionType || null);
                break;
            case 1:
                cell.append(transaction.TransactionId || null);
                break;
            case 2:
                cell.append(transaction.OrderNumber || null);
                break;
            case 3:
                var amount = transaction.Amount;
                if (transaction.TransactionType == "Credit" || transaction.TransactionType == "Check") {
                    amount = -1 * amount;
                };

                var span = $("<span />")
                .prop({
                    "class": "money_displays"
                })
                .text(numeral(amount).format('0,0,0.00'));
                cell.append(span);
                break;
            case 4:
                var amount = transaction.AmountPaid;
                if (transaction.TransactionType == "Credit" || transaction.TransactionType == "Check") {
                    amount = -1 * amount;
                };
                var span = $("<span />")
                    .prop({
                        "class": "money_displays"
                    })
                    .text(numeral(amount).format('0,0,0.00'));
                cell.append(span);
                break;
            case 5:
                var amount = transaction.Balance;
                if (transaction.TransactionType == "Credit" || transaction.TransactionType == "Check") {
                    amount = -1 * amount;
                };
                var span = $("<span />")
                    .prop({
                        "class": "money_displays"
                    })
                    .text(numeral(amount).format('0,0,0.00'));
                cell.append(span);
                break;
            case 6:
                var d = new Date(transaction.Date);
                var date = d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear();
                var span = $("<span />")
                    .prop({
                        "class": "date_displays"
                    })
                    .text(date);
                cell.append(span);
                break;
            default:
                cell.append("??");
        }


        row.append(cell);

    };

    return row;
};

viewModel.statementOfAccountAccordionExpand = function () {

    //set icon working
    $("#" + viewModel.config.statementOfAccountDisplay.HeaderBarId).css({ "background-image": "url('/images/icons/Loading_Availability.gif')" });

    viewModel.statementOfAccountRefresh();
    var target = $("#" + viewModel.config.statementOfAccountDisplay.AccordionDisplayId);

    target.show("blind", {}, viewModel.config.statementOfAccountDisplay.AccordionExpandRate(), function () {
        $("#" + viewModel.config.statementOfAccountDisplay.HeaderBarId).css({ "background-image": "url('/images/layout/MyAccountArrowDown.png')" });
        viewModel.state.statementOfAccountAccordionExpanded = true;
    });

};
viewModel.statementOfAccountAccordionCollapse = function () {

    var display = $("#" + viewModel.config.statementOfAccountDisplay.AccordionDisplayId);
    display.hide("blind", {}, viewModel.config.statementOfAccountDisplay.AccordionExpandRate(), function () {
        $("#" + viewModel.config.statementOfAccountDisplay.HeaderBarId).css({ "background-image": "url('/images/layout/cart_header_pointer.png')" });
        viewModel.state.statementOfAccountAccordionExpanded = false;
    });

};
viewModel.statementOfAccountAccordionToggle = function () {
    //alert("expanded: "+viewModel.state.statementOfAccountAccordionExpanded);
    if (viewModel.state.statementOfAccountAccordionExpanded) {
        viewModel.statementOfAccountAccordionCollapse();
    } else {
        viewModel.statementOfAccountAccordionExpand();
    };

};


////////////////////// Utilities /////////////////////////
viewModel.processEventChain = function (callbacks) {
    //alert(callbacks);
    if (typeof callbacks !== 'undefined' && callbacks != null && callbacks.length > 0) {
        var callback = callbacks.shift();
        //alert(callback);
        callback(callbacks);
    }
};
