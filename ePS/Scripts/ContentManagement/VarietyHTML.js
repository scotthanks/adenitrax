﻿
<link href="SourceCode/ePS/Content/styles/Search.css" rel="stylesheet" type="text/css">
<div id="sr_variety_type_template"><div class="sr_variety_type_displays">
<table border="0" padding="0">
<tr>
<td id="td1"><a class="sr_variety_type_anchors"><img class="sr_variety_type_images" src="" alt=""  /></a></td>
<td id="td2">
   <table class="sr_variety_subtable">
   <tr>
   <td id="td3"><span class="sr_variety_result_name_spans">???</span> &nbsp&nbsp&nbsp&nbsp (variety result)</td>
   </tr>
   <tr>
   <td id="td4">Visit <a class="sr_variety_catalog"><span class="sr_variety_result_name_spans2">???</span></a> variety details in the catalog.</td>
   </tr>
   <tr>
   <td colspan="3" class="sr_variety_listingheader"><span class="sr_variety_result_name_spans3">???</span> found in the following availability listings:</td>
   </tr>
   <tr>
   <td class="sr_variety_program_td1">???</td>
   <td><a class="sr_variety2_avail_anchor1">???</a></td>
   <td><a class="sr_variety2_species_anchor1" >???</a></td>
   </tr> 
   <tr>
   <td class="sr_variety_program_td2">???</td>
   <td><a class="sr_variety2_avail_anchor2" >???</a></td>
   <td><a class="sr_variety2_species_anchor2" >???</a></td>
   </tr>                 
   <tr>
   <td class="sr_variety_program_td3">???</td>
   <td><a class="sr_variety2_avail_anchor3" >???</a></td>
   <td><a class="sr_variety2_species_anchor3" >???</a></td>
   </tr>                 
                                               
    </table>
</td>
</tr>
</table>
</div></div>



<div id="sr_variety_type_template"><div class="sr_variety_type_displays">
<table border="0" padding="0">
<tr>
<td id="td1"><a class="sr_variety_type_anchors"><img class="sr_variety_type_images" src="" alt=""  /></a></td>
<td id="td2">
   <table class="sr_variety_subtable">
   <tr>
   <td id="td3" colspan="3"><span class="sr_variety_result_name_spans">???</span> &nbsp&nbsp&nbsp&nbsp (variety result)</td>
   </tr>
   <tr>
   <td id="td4" colspan="3">Visit <a class="sr_variety_catalog"><span class="sr_variety_result_name_spans2">???</span></a> variety details in the catalog.</td>
   </tr>
   <tr>
   <td colspan="3" class="sr_variety_listingheader"><span class="sr_variety_result_name_spans3">???</span> found in the following availability listings:</td>
   </tr>
   <tr>
   <td class="sr_variety_program_td1">???</td>
   <td><a class="sr_variety2_avail_anchor1">???</a></td>
   <td><a class="sr_variety2_species_anchor1" >???</a></td>
   </tr> 
   <tr>
   <td class="sr_variety_program_td2">???</td>
   <td><a class="sr_variety2_avail_anchor2" >???</a></td>
   <td><a class="sr_variety2_species_anchor2" >???</a></td>
   </tr>                 
   <tr>
   <td class="sr_variety_program_td3">???</td>
   <td><a class="sr_variety2_avail_anchor3" >???</a></td>
   <td><a class="sr_variety2_species_anchor3" >???</a></td>
   </tr>                 
                                               
    </table>
</td>
</tr>
</table>
</div></div>
                    
CSS:    
.sr_variety_subtable {font-family:Helvetica,sans-serif; font-size:16px;padding:0px} 
.sr_variety_type_displays {padding:0px; border-bottom:1px solid #737474;} 
.sr_variety_type_images {width:120px; height: 120px; margin:5px; padding:0px;}
.sr_variety_result_name_spans { vertical-align:top; color:#287d87;font-family:Helvetica,sans-serif; font-size: 21px; }
.sr_variety_header { vertical-align:top; font-family:Helvetica,sans-serif; font-size: 14px; }
.sr_variety_catalog { font-family:Helvetica,sans-serif; font-size: 14px; }
.sr_variety_result_name_spans2 { font-family:Helvetica,sans-serif; font-size: 16px; color:#9bc718;}
.sr_variety_result_name_spans3 { font-family:Helvetica,sans-serif; font-size: 16px;}
.sr_variety2_avail_anchor1 { font-family:Helvetica,sans-serif; font-size: 16px; color:#9bc718;}
.sr_variety2_species_anchor1 { font-family:Helvetica,sans-serif; font-size: 16px; color:#9bc718;}
.sr_variety2_avail_anchor2 { font-family:Helvetica,sans-serif; color:#9bc718; font-size: 16px;}
.sr_variety2_species_anchor2 { font-family:Helvetica,sans-serif; color:#9bc718; font-size: 16px;}
.sr_variety2_avail_anchor3 { font-family:Helvetica,sans-serif; color:#9bc718; font-size: 16px;}
.sr_variety2_species_anchor3 { font-family:Helvetica,sans-serif; color:#9bc718; font-size: 16px;}
.sr_variety_listingheader {font-weight:bold;font-family:Helvetica,sans-serif;font-size: 16px;}



