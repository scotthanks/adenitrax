﻿
var model = {};

model.state = {};
model.open = {};
model.config = {};
model.PatientData = {};
model.VisitData = {};

//Generic Ajax Call
model.ajaxCall = function (element, uri, httpVerb, onSuccess, callbacks) {
    $.ajax({
        url: uri,
        dataType: "json",
        type: httpVerb,
    })
        .done(function (data, textStatus, jqXHR) {
            //alert(traverseObj(data, false));
            onSuccess(data);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            //alert("failure");
            var errorMsg = "TextStatus: " + textStatus + "\nerrorThrown: " + errorThrown;
            //testDisplay.append(errorMsg);
            //if (typeof element !== "undefined") {
            eps_tools.floatMessage(element, errorMsg, 5000);
            //};
        })
        .always(function () {
            vModel.ProcessEventChain(callbacks);
        });

};
model.config = {
    urlPatientApi: '/api/Patients',
    urlGetPatientData: function (guid) { return model.open.config.urlPatientApi + "?ClinicGuid=" + guid; },
    urlGetVisitData: function (guid) { return model.open.config.urlPatientApi + "/Visits?ClinicPatientGuid=" + guid; },
   
};

model.state = {
    currentPatientGuid: function () { return model.DetailData.PatientGuid || ""; },
    patientCount: function () {
        return model.open.PatientData.length || 0;
    },
   
};

model.open.getPatientData = function (callbacks) {
    //alert("open getSummaryData");
    $.ajax({
        datatype: "json",
        url: model.open.config.urlGetPatientApi,
        type: "get",
        beforeSend: function (jqXHR, settings) {
            model.PatientData = [];
        },
        success: function (response, textStatus, jqXHR) {
            model.state.isReadOnly = response.IsReadOnly;
            model.PatientData = response.PatientData || [];
        },
        complete: function (jqXHR, textStatus) {
            viewModel.ProcessEventChain(callbacks);
           
        }
    });
   
};
model.open.GetVisitData = function (patientClinicGuid, callbacks) {
    model.VisitData = {};
    model.VisitData.PatientClinicGuid = patientClinicGuid;
    $.ajax({
        datatype: "json",
        url: model.open.config.urlGetVisitData(patientClinicGuid),
        type: "get",
        beforeSend: function (jqXHR, settings) {
        },
        success: function (response, textStatus, jqXHR) {
         
            model.VisitData = response;
 
        },
        complete: function (jqXHR, textStatus) {
            vModel.ProcessEventChain(callbacks);
        }
    });
};

model.open.getPatientNdx = function (guid) {
    var ndx = null;
    $.each(model.open.SummaryData, function (i, item) {
        if (item.ClinicPatientGuid == guid) {
            ndx = i;
        }
    });
    return ndx;
};




////////////////////////////// viewModel //////////////////////////////////////
var viewModel = {};
viewModel.init = function () {
    alert("viewModel.init");


};