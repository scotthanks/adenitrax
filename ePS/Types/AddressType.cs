﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ePS.Types
{
    [DataContract]
    public class AddressType
    {
        [DataMember]
        public Guid AddressGuid { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string StreetAddress1 { get; set; }

        [DataMember]
        public string StreetAddress2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string StateCode { get; set; }

        [DataMember]
        public string StateName { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public string ZipCode { get; set; }
    }
}