﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace ePS.Types
{
    [DataContract]
    public class ProductAvailabilityDataType
    {
        [DataMember]
        public Guid ProductGuid { get; set; }

        [DataMember]
        public string Supplier { get; set; }

        [DataMember]
        public string SpeciesCode { get; set; }

        [DataMember]
        public string Species { get; set; }

        [DataMember]
        public string Product { get; set; }
       
        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string VarietyCode { get; set; }
        
        [DataMember]
        public string Form { get; set; }

        [DataMember]
        public int Min { get; set; }

        [DataMember]
        public int Mult { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public bool IsOrganic { get; set; }

        [DataMember]
        public bool IncludesDelivery { get; set; }
         
        [DataMember]
        public int OrderQuantity { get; set; }

        [DataMember]
        public bool IsCarted { get; set; }

        // For when multiple weeks of availabilities are being provided (such as on the availability page).

        [DataMember]
        public int[] Availabilities { get; set; }

        [DataMember]
        public string[] DisplayCodes { get; set; }

        // For when a single week of availability are being provided (such as when getting a list of substitutions).

        [DataMember]
        public int Availability { get { return Availabilities[0]; } }

        [DataMember]
        public string DisplayCode { get { return DisplayCodes[0]; } }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            //TODO: Finish this.
            stringBuilder.AppendFormat("ProductGuid={0}", ProductGuid);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("Product={0}", Product);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("Supplier={0}", Supplier);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("Form={0}", Form);

            return stringBuilder.ToString();
        }
    }
}