﻿





CREATE PROCEDURE [dbo].[GrowersGet]
	@NameLike NVARCHAR(100) = ''
AS
	SELECT g.ID,g.code,g.name,g.Guid,g.Email
		, CAST(g.PhoneAreaCode AS CHAR(3)) AS PhoneAreaCode
		,CAST(g.Phone AS CHAR(7))  AS Phone
	 ,gt.Name AS GrowerType
	 ,AdditionalAccountingEmail
	 ,AdditionalOrderEmail
	 ,IsBadGrower
	 FROM Grower g
	 JOIN Lookup gt ON g.GrowerTypeLookupGuid = gt.Guid
	 WHERE g.Name LIKE @NameLike + '%'
	ORDER BY g.name ASC