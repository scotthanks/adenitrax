﻿

CREATE procedure [dbo].[EventLogTableOperationAdd]
	@ProcedureName AS NVARCHAR(50),
	@TableName AS NVARCHAR(50),
	@RowGuid AS UNIQUEIDENTIFIER,
	@Operation AS NVARCHAR(50),
	@UserCode AS NCHAR(56) = NULL,
	@Comment AS NVARCHAR(200) = NULL
AS
	EXECUTE EventLogAdd
		@Comment = @Comment,
		@LogTypeLookupCode = @Operation,
		@ProcessLookupCode = @ProcedureName,
		@ObjectTypeLookupCode = @TableName,
		@ObjectGuid = @RowGuid,
		@UserCode = @UserCode