﻿CREATE PROCEDURE [dbo].[ShipWeekGetGuid]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NVARCHAR(56) = NULL,
	@ShipWeekCode AS NVARCHAR(50),
	@ProcessLookupCode AS NVARCHAR(50),
	@ShipWeekGuid AS UNIQUEIDENTIFIER OUT
AS
	SET @ShipWeekCode = LTRIM(RTRIM(@ShipWeekCode))

	SET @ShipWeekGuid = NULL

	IF @ShipWeekCode IS NOT NULL
		BEGIN
			SELECT @ShipWeekGuid = ShipWeekGuid 
			FROM ShipWeekView 
			WHERE ShipWeekCode = @ShipWeekCode

			IF @ShipWeekGuid IS NULL
				EXECUTE EventLogKeyNotFound
					@UserGuid=@UserGuid,
					@UserCode=@UserCode,
					@KeyName='ShipWeekCode',
					@Key=@ShipWeekCode,
					@ObjectTypeLookupCode='ShipWeek',
					@ProcessLookupCode=@ProcessLookupCode
		END