﻿


CREATE procedure [dbo].[EventLogAddBase]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@Comment AS NVARCHAR(2000) = NULL,
	@LogTypeLookupGuid AS UNIQUEIDENTIFIER = NULL,
	@ProcessLookupGuid AS UNIQUEIDENTIFIER = NULL,
	@ObjectTypeLookupGuid AS UNIQUEIDENTIFIER = NULL,
	@ObjectGuid AS UNIQUEIDENTIFIER = NULL,
	@PersonGuid AS UNIQUEIDENTIFIER = NULL,
	@ProcessId AS BIGINT = NULL,
	@EventLogGuid AS UNIQUEIDENTIFIER = NULL,
	@IntValue AS BIGINT = NULL,
	@FloatValue AS FLOAT = NULL,
	@GuidValue AS UNIQUEIDENTIFIER = NULL,
	@XmlData AS NVARCHAR(2000) = NULL,
	@VerbosityLookupGuid AS UNIQUEIDENTIFIER = NULL,
	@PriorityLookupGuid AS UNIQUEIDENTIFIER = NULL,
	@SeverityLookupGuid AS UNIQUEIDENTIFIER = NULL
AS
	IF @EventLogGuid IS NULL
		SET @EventLogGuid = newid()

	IF @ProcessId IS NULL
		SET @ProcessId = 0

	SET @UserCode = LTRIM(RTRIM(@UserCode))
	IF @UserCode = ''
		SET @UserCode = NULL

	IF @VerbosityLookupGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @VerbosityLookupGuid = NULL

	IF @VerbosityLookupGuid IS NULL
		EXECUTE LookupGetGuid
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@Path='Logging/Verbosity',
			@LookupCode='Normal',
			@ProcessLookupCode='EventLogAddBase',
			@LookupGuid=@VerbosityLookupGuid OUT

	IF @PriorityLookupGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @PriorityLookupGuid = NULL

	IF @PriorityLookupGuid IS NULL
		EXECUTE LookupGetGuid
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@Path='Logging/Priority',
			@LookupCode='Low',
			@ProcessLookupCode='EventLogAddBase',
			@LookupGuid=@PriorityLookupGuid OUT

	IF @SeverityLookupGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @SeverityLookupGuid = NULL

	IF @SeverityLookupGuid IS NULL
		EXECUTE LookupGetGuid
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@Path='Logging/Severity',
			@LookupCode='Info',
			@ProcessLookupCode='EventLogAddBase',
			@LookupGuid=@SeverityLookupGuid OUT
	
	INSERT INTO EventLog
    (
		[Guid],
        [LogTypeLookupGuid],
        [ObjectTypeLookupGuid],
        [ObjectGuid],
        [ProcessLookupGuid],
        [ProcessId],
        [PersonGuid],
		[UserCode],
        [EventTime],
        [SyncTime],
        [IntValue],
        [FloatValue],
		[GuidValue],
	    [Comment],
        [XmlData],
		[VerbosityLookupGuid],
		[PriorityLookupGuid],
		[SeverityLookupGuid]
	)
    VALUES
    (
		@EventLogGuid,
        @LogTypeLookupGuid,
        @ObjectTypeLookupGuid,
        @ObjectGuid,
        @ProcessLookupGuid,
        @ProcessId,
        @PersonGuid,
		@UserCode,
        CURRENT_TIMESTAMP,
        NULL, -- SyncTime
        @IntValue,
        @FloatValue,
		@GuidValue,
        @Comment,
        @XmlData,
		@VerbosityLookupGuid,
		@PriorityLookupGuid,
		@SeverityLookupGuid
	)