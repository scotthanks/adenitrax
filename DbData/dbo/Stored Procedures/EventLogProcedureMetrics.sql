﻿

--EXAMPLE:

--EXECUTE EventLogProcedureMetrics
--	@ProcedureName='AvailabilityDataGet',
--	@StartDate='2013-04-10'

CREATE PROCEDURE [dbo].[EventLogProcedureMetrics]
	@ProcedureName AS NVARCHAR(50),
	@StartDate AS DATETIME
AS
	SELECT
		EventLogView.ProcessLookupCode,
		EventLogView.XmlData AS [Parameters],
		EventLogView.EventTime AS EventStartTime,
		ProcedureCompleteEventLogView.EventTime AS EventEndTime,
		DATEDIFF(MILLISECOND, EventLogView.EventTime, ProcedureCompleteEventLogView.EventTime) AS Delta
	FROM EventLogView
		LEFT OUTER JOIN EventLogView AS ProcedureCompleteEventLogView
			ON 
				EventLogView.EventLogGuid = ProcedureCompleteEventLogView.ObjectGuid AND 
				ProcedureCompleteEventLogView.ProcessLookupCode = @ProcedureName AND
				ProcedureCompleteEventLogView.LogTypeLookupCode = 'ProcedureCallComplete'
	WHERE
		DATEADD(HOUR, -6, EventLogView.EventTime) >= @StartDate AND
		EventLogView.ProcessLookupCode = @ProcedureName AND
		EventLogView.LogTypeLookupCode = 'ProcedureCall'
	ORDER BY
		EventLogView.EventTime