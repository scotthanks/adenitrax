﻿create procedure dbo.LookupGetByPath
	@Expression AS NVARCHAR(200)
AS
	DECLARE @Count AS integer

	IF @Expression IS NOT NULL
		SET @Expression = LTRIM(RTRIM(@Expression))

	IF @Expression IS NULL OR @Expression = ''
		SET @Expression = '*'

	SET @Expression = REPLACE( @Expression, '*', '%')

	SELECT * FROM Lookup
	WHERE Path LIKE @Expression