﻿

CREATE PROCEDURE [dbo].[PersonRoleUpdate]
	 @PersonGuid UniqueIdentifier,
	 @RoleID int
	 
AS
	
	Declare @UserGuid UniqueIdentifier
	SELECT @UserGuid = ISNULL(UserGuid,newID())
	From 
	Person 
	Where Guid = @PersonGuid

	UPDATE epsMembershipProd.dbo.webpages_UsersInRoles 
	set roleID = @RoleID
    WHERE UserID  = (SELECT UserID from epsMembershipProd.dbo.UserProfile WHERE UserGuid = @UserGuid)