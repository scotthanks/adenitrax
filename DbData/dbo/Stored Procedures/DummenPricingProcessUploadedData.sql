﻿













CREATE PROCEDURE [dbo].[DummenPricingProcessUploadedData]
	 @JobNumber as bigint
	
AS
	
	print 'Starting'
	--populate the Guids
	update GrowerProductProgramSeasonPriceUpload
	set Growerguid = g.Guid,
	ProgramSeasonGuid = ps.Guid,
	Productguid = p.Guid
	from GrowerProductProgramSeasonPriceUpload u (nolock)
	Join Grower g (nolock) on g.Name = u.GrowerName
	Join ProgramSeason ps (nolock) on u.ProgramSeasonCode = ps.Code
	Join Product p (nolock) on p.ProgramGuid = ps.ProgramGuid 
		and p.SupplierIdentifier = u.SupplierIdentifier
	where RunNumber = @JobNumber
	--OPTION (RECOMPILE)

	print 'done Populating Guids, deleting.'


	--Delete from program seasons for the growers
	delete from GrowerProductProgramSeasonPrice
	where ID in (
		select ID 
		from GrowerProductProgramSeasonPrice p (nolock)
		where Growerguid in (select distinct GrowerGuid from GrowerProductProgramSeasonPriceUpload where RunNumber = @JobNumber)
			AND ProgramSeasonGuid in (select distinct ProgramSeasonGuid from GrowerProductProgramSeasonPriceUpload where RunNumber = @JobNumber)
	)
	--OPTION (RECOMPILE)

	print 'done deleting, inserting prices.'

	--Insert the incoming data
	Insert into GrowerProductProgramSeasonPrice 
	SELECT
		newID(),
		DeactivatedDate,
		GrowerGuid ,
		ProgramSeasonGuid,
		ProductGuid,
		CuttingCost,
		RoyaltyCost,
		FreightCost,
		TagCost,
		getdate()
	FROM
		GrowerProductProgramSeasonPriceUpload (nolock)
	WHERE RunNumber = @JobNumber
	and GrowerGuid is not null
	and ProgramSeasonGuid is not null
	and ProductGuid is not null
	--OPTION (RECOMPILE)

	print 'done inserting setting Not Loaded Counts.'
	Declare @NotLoadedCount bigint
	
	select @NotLoadedCount = count(0)
	From GrowerProductProgramSeasonPriceUpload (nolock)
	WHERE RunNumber = @JobNumber
		and 
			(GrowerGuid is null
			OR ProgramSeasonGuid is null
			OR ProductGuid is null)
	 
	If @NotLoadedCount > 0
	BEGIN
		Insert into DummenPricingUploadJobMessage
		values(
		newid()
		,(select Guid from DummenPricingUploadJob where ID = @JobNumber)
		,getdate()
		,'Process'
		,'Important'
		,'Some items were not loaded: ' + cast(@NotLoadedCount as nvarchar(6)) + ' items.'
		) 
	END
	
	print 'done'