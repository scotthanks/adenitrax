﻿CREATE procedure [dbo].[TripleGrowerPersonAdd]
	@GrowerCode AS NCHAR(50),
	@PersonOrUserGuid AS UNIQUEIDENTIFIER,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL
AS
	DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
	DECLARE @GrowerPersonLookupGuid AS UNIQUEIDENTIFIER

	DECLARE @PathExpression AS NVARCHAR(200)

	SELECT @GrowerGuid = GrowerGuid FROM GrowerView WHERE GrowerCode = @GrowerCode
	IF (@GrowerGuid IS NULL)
		BEGIN
            EXECUTE EventLogAdd
                @UserGuid=@UserGuid ,
                @UserCode=@UserCode ,
                @Comment='@GrowerCode not found.' ,
                @LogTypeLookupCode='InvalidParameters' ,
                @ProcessLookupCode='TripleGrowerPersonAdd' ,
                @SeverityLookupCode='Error'

			RETURN
		END

	DECLARE @PersonGuid AS UNIQUEIDENTIFIER
	SELECT @PersonGuid = PersonGuid FROM PersonView WHERE PersonGuid = @PersonOrUserGuid OR UserGuid = @PersonOrUserGuid
	IF (@PersonGuid IS NULL)
		BEGIN
            EXECUTE EventLogAdd
                @UserGuid=@UserGuid ,
                @UserCode=@UserCode ,
                @Comment='Person or User not found in Person table.' ,
                @LogTypeLookupCode='InvalidParameters' ,
                @ProcessLookupCode='TripleGrowerPersonAdd' ,
                @SeverityLookupCode='Failure'
			RETURN
		END
	
	EXECUTE TripleAdd 
		@PredicateLookupCode='GrowerPerson', 
		@SubjectGuid=@GrowerGuid, 
		@ObjectGuid=@PersonGuid