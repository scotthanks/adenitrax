﻿







CREATE PROCEDURE [dbo].[DatabaseCleanupAndChecks]
	
	
AS
	Declare @dCurrentMondayDate  Datetime
	
	Declare @sText  nvarchar(2000)
	Declare @RC int

	set @sText = 'Date Run: ' + cast(getdate() as varchar(30)) + '  Clear Out Old Order Lines From Not Yet In Cart'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','DatabaseCleanup','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'  
	
	delete from orderline where guid in (
	select 	ol.Guid 
	--,o.dateentered,osl.name,g.name,ol.* 
	from Orderline ol
	Join SupplierOrder so on OL.supplierorderguid = so.guid
	Join growerorder o on so.growerorderguid = o.guid
	JOIN lookup osl ON ol.OrderLineStatusLookupGuid = osl.Guid
	Join Grower g on o.growerguid = g.guid
	where ol.OrderLineStatusLookupGuid = (select guid from lookup where Path = 'Code/OrderLineStatus/PreCart')
	and o.dateentered < dateadd(day,-14,getdate())
	)

	set @sText = 'Date Run: ' + cast(getdate() as varchar(30)) + '  Clear Out Old Order Lines From cart'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','DatabaseCleanup','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'    


	delete from orderline where guid in (
	select	ol.Guid 
	--,o.dateentered,osl.name,g.name,ol.* 
	from Orderline ol
	Join SupplierOrder so on OL.supplierorderguid = so.guid
	Join growerorder o on so.growerorderguid = o.guid
	JOIN lookup osl ON ol.OrderLineStatusLookupGuid = osl.Guid
	Join Grower g on o.growerguid = g.guid
	where ol.OrderLineStatusLookupGuid = (select guid from lookup where path = 'Code/OrderLineStatus/Pending')
	and o.dateentered < dateadd(day,-14,getdate())
	)

    set @sText = 'Date Run: ' + cast(getdate() as varchar(30)) + '  Clear Out Supplier Orders with no line items'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','DatabaseCleanup','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'    
    
	delete from SupplierOrderFee where supplierOrderguid in (
	select	so.Guid 
	from growerorder o
	left Join SupplierOrder so on o.guid = so.growerorderguid
	left Join  Orderline ol on so.guid = ol.supplierorderguid 
	where ol.ID is null
	)
	
	delete from SupplierOrder where guid in (
	select	so.Guid 
	from growerorder o
	left Join SupplierOrder so on o.guid = so.growerorderguid
	left Join  Orderline ol on so.guid = ol.supplierorderguid 
	where ol.ID is null
	)
  
  set @sText = 'Date Run: ' + cast(getdate() as varchar(30)) + '  Clear Out Grower Orders with no supplier Orders'
	EXECUTE @RC = dbo.EventLogAdd NULL,NULL,@sText ,'InternalProcDebug','DatabaseCleanup','EventLog',
					 NULL,0,NULL,0,0,NULL,'','Normal','Low','Info'    
    
	delete from growerorderFee where growerorderguid in (
	select	o.Guid --,so.id
	--,go.dateentered,osl.name,g.name,ol.* 
	from growerorder o
	left Join SupplierOrder so on o.guid = so.growerorderguid
	where so.ID is null
	)

	delete from growerorderHistory where growerorderguid in (
	select	o.Guid --,so.id
	--,go.dateentered,osl.name,g.name,ol.* 
	from growerorder o
	left Join SupplierOrder so on o.guid = so.growerorderguid
	where so.ID is null
	)
	
	delete from growerorder where guid in (
	select	o.Guid --,so.id
	--,go.dateentered,osl.name,g.name,ol.* 
	from growerorder o
	left Join SupplierOrder so on o.guid = so.growerorderguid
	where so.ID is null
	)