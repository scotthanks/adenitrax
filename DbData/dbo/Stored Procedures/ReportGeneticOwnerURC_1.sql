﻿
CREATE PROCEDURE [dbo].[ReportGeneticOwnerURC]
	@GeneticOwnerGuid AS UNIQUEIDENTIFIER = null,
	@MonthStartCode AS NVARCHAR(6),
	@MonthEndCode AS NVARCHAR(6)


AS

Declare @StartDate datetime
Declare @EndDate datetime

set @StartDate = (cast((left(@MonthStartCode,4) + '-' + right(@MonthStartCode,2) + '-1') as datetime))
set @EndDate = (cast((left(@MonthEndCode,4) + '-' + right(@MonthEndCode,2) + '-1') as datetime))
set @EndDate = dateadd(month,1,@EndDate)
set @EndDate = dateadd(day,-1,@EndDate)


SELECT ge.Name as GeneticOwner
,v.Name as VarietyName
,cast(datepart(Year,sw.MondayDate) as varchar(4)) + RIGHT('00' + cast(datepart(Month,sw.MondayDate) as varchar(2)),2)  as MonthCode
,Sum(ol.QtyShipped) as URCQty


from OrderLineViewShowInactiveProduct ol
Join Product p on ol.ProductGuid = p.Guid
Join Variety v on p.VarietyGuid = v.Guid
Join GeneticOwner ge on v.GeneticOwnerGuid = ge.Guid
Join ProductForm pf on p.ProductFormGuid = pf.Guid
Join ProductFormCategory pfc on pf.ProductFormCategoryGuid = pfc.Guid
Join ShipWeek sw on ol.ShipWeekGuid = sw.Guid



Where v.GeneticOwnerGuid= @GeneticOwnerGuid
and pfc.Code = 'URC'
and sw.MondayDate between @StartDate and @EndDate
and ol.QtyShipped > 0

Group by 
ge.Name
,v.Name 
,sw.MondayDate

Order By
ge.Name
,v.Name
,sw.MondayDate

--select cast(datepart(Year,MondayDate) as varchar(4)) + RIGHT('00' + cast(datepart(Month,MondayDate) as varchar(2)),2)  from shipweek