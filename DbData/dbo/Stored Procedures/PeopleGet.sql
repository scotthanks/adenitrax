﻿




CREATE PROCEDURE [dbo].[PeopleGet]
	 @FirstNameLike as nvarchar(50) ='',
	 @LastNameLike as nvarchar(50) = '',
	 @EmailLike as nvarchar(70)= '',
	 @GrowerNameLike as nvarchar(50)= ''
	
AS
	;with UR (UserGuid,RoleID,RoleName)

	as
	(
		SELECT p.UserGuid,min(uR.RoleID) as RoleID,Min(R.RoleName) as RoleName
        FROM epsMembershipProd.dbo.UserProfile P
        LEFT JOIN epsMembershipProd.dbo.webpages_UsersInRoles uR on p.UserID = uR.UserID 
        LEFT JOIN epsMembershipProd.dbo.webpages_Roles r on ur.RoleID = r.RoleID
              
        GROUP BY p.UserGuid
	)

	SELECT Top 100
	p.ID
	,p.Guid
	,p.FirstName
	,p.LastName
    ,p.email
	,'(' + cast(p.PhoneAreaCode as char(3)) + ') ' + substring(cast(p.Phone as char(7)),1,3) + '-' + substring(cast(p.Phone as char(7)),4,4) as Phone
	,p.GrowerGuid
	,g.Name as GrowerName
	,gt.Name as GrowerType
	,cast(ur.RoleID as varchar(3)) as UserRole
	,g.IsBadGrower
	
	FROM  Person p
    JOIN Grower g on p.GrowerGuid = g.guid
	JOIN Lookup gt on g.GrowerTypeLookupGuid = gt.guid
	JOIN UR  on p.UserGuid = UR.UserGuid
    
	WHERE 1=1
	AND p.FirstName like @FirstNameLike + '%'
	AND p.LastName like @LastNameLike + '%'
	AND p.Email like @EmailLike + '%'
	AND g.Name like @GrowerNameLike + '%'
	
	ORDER BY p.ID desc