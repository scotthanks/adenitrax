﻿






CREATE PROCEDURE [dbo].[VarietiesGet]
	@SpeciesStartsWith nVarChar(50)= '',
	@SpeciesList nVarChar(2000) = ''
AS

Declare @likeString nvarchar (50)
Set @likeString = @SpeciesStartsWith + '%'
;

With tProd (VarietyGuid,TheCount)
as  
(
	Select VarietyGuid,count(*) as TheCount 
	FROM Product 
	GROUP By VarietyGuid
)

Select 
	
rtrim(v.Code) as Code,
rtrim(v.Name) as Name,
v.DateDeactivated,
v.ResearchStatus,
sp.Name as Species,
IsNULL(se.Name,'') as Series,
o.Name as GeneticOwner,
ISNULL(tProd.TheCount,0) as ProductCount

FROM 
Variety v
Join Species sp on v.SpeciesGuid = sp.Guid
LEFT JOIN tProd on v.Guid = tProd.Varietyguid
Join GeneticOwner o on v.GeneticOwnerGuid = o.Guid
left Join Series se on v.SeriesGuid = se.Guid
		
WHERE sp.Name like @likeString
and (@SpeciesList = ''
	OR rtrim(sp.Code) in (select value from dbo.split(',',@SpeciesList))
	)
	

ORDER BY sp.Name,v.Name