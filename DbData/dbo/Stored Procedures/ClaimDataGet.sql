﻿


CREATE PROCEDURE [dbo].[ClaimDataGet]
	@UserCode AS NVARCHAR(56) = null,
	@GrowerGuid AS UNIQUEIDENTIFIER = null,
	@GrowerClaimGuid AS UNIQUEIDENTIFIER = null
	
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('GrowerClaimGuid',@GrowerClaimGuid,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerClaimDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	
	If @GrowerGuid is null
		set @GrowerGuid = (select growerguid from person where userguid = @UserCode)


	DECLARE @LocalTempClaimData TABLE
	(
		ClaimGuid UNIQUEIDENTIFIER,
		GrowerGuid UNIQUEIDENTIFIER,
		ClaimStatusLookupGUID  UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempClaimData
	SELECT
		ClaimGuid,
		GrowerGuid,
		ClaimStatusLookupGUID
		
	FROM ClaimView
	WHERE
		@GrowerGuid = GrowerGuid


	SELECT ClaimView.*
	FROM ClaimView
	WHERE ClaimView.ClaimGuid IN
	(SELECT DISTINCT(ClaimGuid) FROM @LocalTempClaimData)

		
	SELECT Lookup.*
	FROM Lookup
	WHERE Lookup.Guid IN
	(
		SELECT DISTINCT(ClaimStatusLookupGUID) FROM @LocalTempClaimData
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerClaimDataGet',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserCode=@UserCode