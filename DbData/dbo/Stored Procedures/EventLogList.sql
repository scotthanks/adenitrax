﻿


CREATE PROCEDURE [dbo].[EventLogList]
	@Filter AS NVARCHAR(100) = null
AS
	SET @Filter = '%' + LTRIM(RTRIM(@Filter)) + '%'

	IF @Filter IS NULL
		SET @Filter = '%'

	SELECT TOP 100
		* 
	FROM EventLogView
		WHERE
			UserCode LIKE @Filter OR
			LogTypeLookupCode LIKE @Filter OR
			ObjectTypeLookupCode LIKE @Filter OR
			ProcessLookupCode LIKE @Filter OR
			UserCode LIKE @Filter OR
			PersonLastName LIKE @Filter OR
			PersonFirstName LIKE @Filter

			
	ORDER BY EventTime DESC