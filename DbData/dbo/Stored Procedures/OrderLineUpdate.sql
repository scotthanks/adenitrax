﻿






CREATE PROCEDURE [dbo].[OrderLineUpdate]
	@UserGuid UNIQUEIDENTIFIER = NULL,
	@OrderLineGuid UNIQUEIDENTIFIER = NULL,
	@QtyOrdered INTEGER,
	@Price DECIMAL(8,4) = NULL,
	@SupplierOrGrower NVARCHAR(20),
	@ActualQuantity INTEGER OUT,
	@GrowerOrderGuidOut UNIQUEIDENTIFIER OUT

	
AS
	SET @ActualQuantity = 0


	
	DECLARE @OrderLineStatusLookupSortSequence AS FLOAT
	DECLARE @SellerCode as nvarchar(10)
	DECLARE @NewOrderLineGuid as uniqueidentifier
	DECLARE @NewOrderLineStatusLookupGuid as uniqueidentifier
	

	DECLARE @RowCount AS INT

	
	DECLARE @GrowerOrderGuid UniqueIdentifier
	DECLARE @SupplierOrderGuid UniqueIdentifier
	DECLARE @ProductGuid UniqueIdentifier
	DECLARE @VarietyName NVARCHAR(100)
	DECLARE @ShipWeekGuid AS UNIQUEIDENTIFIER
	DECLARE @OrderLineStatusLookupOrderedSortSequence AS FLOAT
	
	SELECT @OrderLineStatusLookupOrderedSortSequence = LookupView.SortSequence
	FROM LookupView
	WHERE Path = 'Code/OrderLineStatus/Ordered'
	
	SELECT
		@ProductGuid = ProductGuid,
		@SellerCode = SellerCode,
		@SupplierOrderGuid = SupplierOrderGuid,
		@GrowerOrderGuid = GrowerOrderGuid,
		@GrowerOrderGuidOut = GrowerOrderGuid,
		@OrderLineStatusLookupSortSequence = OrderLineStatusLookupSortSequence,
		@VarietyName = VarietyName
	FROM OrderLineView
	WHERE
		OrderLineGuid=@OrderLineGuid 
	Declare @UserCode NCHAR(56)
	Declare @PersonGuid as uniqueidentifier
	set @PersonGuid = (select top 1 guid from person where UserGuid = @UserGuid)
	set @UserCode = (select top 1 UserCode from person where UserGuid = @UserGuid)

	Declare @theDate datetime
	set @theDate = getdate()

	Declare @LogGuid uniqueidentifier
	Declare @sComment nvarchar(2000)

	
	SELECT @ShipWeekGuid = ShipWeekGuid
	FROM SupplierOrderView
	WHERE SupplierOrderGuid = @SupplierOrderGuid

	DECLARE @SalesUnitQty AS INTEGER
	DECLARE @AvailabilityTypeLookupCode AS NCHAR(20)
	DECLARE @LineItemMinimumQty AS INTEGER
	DECLARE @QuantityAvailable AS INTEGER
	SELECT
		@QuantityAvailable = ReportedAvailabilityQty,
		@SalesUnitQty = SalesUnitQty,
		@AvailabilityTypeLookupCode = AvailabilityTypeLookupCode,
		@LineItemMinimumQty = LineItemMinimumQty
	FROM ReportedAvailabilityView
	WHERE
		ProductGuid = @ProductGuid AND
		ShipWeekGuid = @ShipWeekGuid

	IF @SalesUnitQty = 0
		SET @SalesUnitQty = 1

	IF @LineItemMinimumQty <= 0
		SET @LineItemMinimumQty = 1

	IF @AvailabilityTypeLookupCode = 'OPEN'
		SET @QuantityAvailable = 999999999
	ELSE IF @QuantityAvailable < 0 OR @AvailabilityTypeLookupCode = 'NA'
		SET @QuantityAvailable = 0

	DECLARE @OriginalQtyOrdered AS INTEGER
	SET @OriginalQtyOrdered = @QtyOrdered

	IF @QtyOrdered < 0
		SET @QtyOrdered = 0

	-- Set @LineItemMinimumQty to the next higher multiple if not already a multiple.
	SET @LineItemMinimumQty = (@LineItemMinimumQty + @SalesUnitQty - 1) / @SalesUnitQty * @SalesUnitQty

	-- Set @QuantityAvailable to the next lower multiple if not already a multiple.
	SET @QuantityAvailable = @QuantityAvailable / @SalesUnitQty * @SalesUnitQty

	-- Round @QtyOrdered to the nearest multiple if not already a multiple.
	SET @QtyOrdered = (@QtyOrdered + @SalesUnitQty/2) / @SalesUnitQty * @SalesUnitQty

	-- If @QtyOrdered is too low (but not zero) move up to @LineItemMinimum
	IF @QtyOrdered != 0 AND @QtyOrdered < @LineItemMinimumQty
		SET @QtyOrdered = @LineItemMinimumQty


	Declare @CurrentQTYOrdered as Integer    --added by Scott on 10/14/15  
	set @CurrentQTYOrdered = (SELECT QtyOrdered from OrderLine where Guid = @OrderLineGuid)

	-- If @QtyOrdered is too high move down to @QuantityAvailable
	IF (@QtyOrdered - @CurrentQTYOrdered)  > @QuantityAvailable
		AND @OrderLineStatusLookupSortSequence <= 1  --added by Scott on 10/14/15  (1 is Cart)
		SET @QtyOrdered = @QuantityAvailable
	IF (@QtyOrdered - @CurrentQTYOrdered)  > @QuantityAvailable	--added by Scott on 10/14/15  (1 is Cart)
		AND @OrderLineStatusLookupSortSequence > 1 
		AND @QtyOrdered > @CurrentQTYOrdered
		SET @QtyOrdered = @CurrentQTYOrdered

	

	SET @ActualQuantity = @QtyOrdered

	

	SELECT @OriginalQtyOrdered = QtyOrdered
	FROM OrderLine
	WHERE Guid = @OrderLineGuid
	
	IF @QtyOrdered != @CurrentQTYOrdered
	BEGIN 
			
		IF @QtyOrdered > 0  
		BEGIN  --Qty > 0
			IF @Price = 0
				SET @Price = NULL
							
			IF @SellerCode = 'DMO' AND @OriginalQtyOrdered  = 0 
			BEGIN --Add New Line and leave origninal line cancel
					
				EXECUTE OrderLineAdd
				@UserCode = @UserCode,
				@SupplierOrderGuid = @SupplierOrderGuid,
				@ProductGuid = @ProductGuid,
				@QtyOrdered = @QtyOrdered,
				@Price = @Price,
				@OrderLineGuid = @NewOrderLineGuid OUT,
				@ReturnRecordSet = 0

				IF @NewOrderLineGuid IS NOT NULL
				BEGIN
					If @SupplierOrGrower = 'Supplier'
					BEGIN
						select @NewOrderLineStatusLookupGuid =  Guid 
						from Lookup where path  = 'Code/OrderLineStatus/SupplierAdd'
					END
					ELSE
					BEGIN
						SELECT @NewOrderLineStatusLookupGuid = Guid 
						FROM Lookup WHERE path  = 'Code/OrderLineStatus/GrowerEdit'
					END
						 

					UPDATE OrderLine SET OrderLineStatusLookupGuid = @NewOrderLineStatusLookupGuid 
					WHERE Guid = @NewOrderLineGuid
					
				END

			END --End Add Line and leave cancel
			ELSE 
			BEGIN  --Else Update
					
				If @SupplierOrGrower = 'Supplier'
				BEGIN
					select @NewOrderLineStatusLookupGuid =  Guid 
					from Lookup where path  = 'Code/OrderLineStatus/SupplierEdit'
				END
				ELSE
				BEGIN
					SELECT @NewOrderLineStatusLookupGuid = Guid 
					FROM Lookup WHERE path  = 'Code/OrderLineStatus/GrowerEdit'
				END
					
					
					
				--select * from lookup where path like 'Code/OrderLineStatus/%'
				UPDATE OrderLine 
				SET 
					QtyOrdered = @QtyOrdered,
					ActualPrice = ISNULL(@Price, ActualPrice),
					DateLastChanged = GetDate(),
					DateQtyLastChanged = GetDate(),
					OrderLineStatusLookupGuid = Case when OrderLineStatusLookupGuid in (select guid from lookup where path in('Code/OrderLineStatus/PreCart','Code/OrderLineStatus/Pending'))
														then OrderLineStatusLookupGuid 
														else @NewOrderLineStatusLookupGuid end
				WHERE Guid = @OrderLineGuid

				SET @RowCount = @@ROWCOUNT

				IF @@ROWCOUNT = 1
				BEGIN  --log event
					EXECUTE EventLogAdd
						@LogTypeLookupCode='RowUpdate',
						@ProcessLookupCode='OrderLineProcessTransaction',
						@ObjectTypeLookupCode='OrderLine',
						@ObjectGuid=@OrderLineGuid,
						@UserGuid=@UserGuid,
						@UserCode=@UserCode,
						@FloatValue=@QtyOrdered	
				END  --log event
				ELSE
				BEGIN  --log failed event
					EXECUTE EventLogAdd
						@Comment='@@ROWCOUNT not 1 after attempted update (See IntValue).',
						@LogTypeLookupCode='DatabaseError',
						@ProcessLookupCode='OrderLineProcessTransaction',
						@ObjectTypeLookupCode='OrderLine',
						@UserGuid=@UserGuid,
						@UserCode=@UserCode,
						@IntValue=@RowCount,
						@FloatValue=@QtyOrdered

					SET @ActualQuantity = 0
				END  --log failed event
								
				IF @OrderLineStatusLookupSortSequence > 1
				BEGIN  --add history
					set @sComment = 'Order for ' + cast(@OriginalQtyOrdered as nvarchar(10)) + ' ' + @VarietyName + ' changed to ' + cast(@QtyOrdered as varchar(10))
					set @LogGuid = (select guid from lookup where path = 'Logging/LogType/Grower/OrderLineChange')
					EXEC GrowerOrderHistoryAdd 
						@UserGuid,@UserCode,
						@GrowerOrderGuid,
						@sComment,0,@PersonGuid,@theDate,@LogGuid
				END --add history

						
			END --else Update
	
		END	 --Qty > 0
		ELSE
		BEGIN  --qty = 0
			--SELECT
			--	@OrderLineStatusLookupSortSequence = OrderLineStatusLookupSortSequence
			--FROM OrderLineView
			--WHERE OrderLineGuid=@OrderLineGuid
		
			-- If this is just a cart order, we can delete the line.
			IF @OrderLineStatusLookupSortSequence < @OrderLineStatusLookupOrderedSortSequence
			BEGIN
				DELETE OrderLine WHERE Guid = @OrderLineGuid
			END
			ELSE
				-- But if this is a placed order, we can only cancel it.
			BEGIN
				If @SupplierOrGrower = 'Supplier'
				BEGIN
					select @NewOrderLineStatusLookupGuid = Guid 
					from Lookup where path  = 'Code/OrderLineStatus/SupplierCancelled'
				END
				ELSE
				BEGIN
					SELECT @NewOrderLineStatusLookupGuid =  Guid 
					FROM Lookup WHERE path  = 'Code/OrderLineStatus/GrowerCancelled'
				END
			
				UPDATE OrderLine
					SET
						OrderLine.OrderLineStatusLookupGuid = @NewOrderLineStatusLookupGuid,
						OrderLine.QtyOrdered = 0,
						OrderLine.DateLastChanged = GetDate(),
						OrderLine.DateQtyLastChanged = GetDate()
				FROM OrderLine
				WHERE OrderLine.Guid = @OrderLineGuid

									
				set @sComment = 'Order for ' + cast(@OriginalQtyOrdered as nvarchar(10)) + ' ' + @VarietyName + ' cancelled.'
				set @LogGuid = (select guid from lookup where path = 'Logging/LogType/Grower/OrderLineCancel')
				EXEC GrowerOrderHistoryAdd 
					@UserGuid,@UserCode,
					@GrowerOrderGuid,
					@sComment,0,@PersonGuid,@theDate,@LogGuid

			END
		END --qty = 0
	

		--Handle Multiple forms or types
		EXEC GrowerOrderResetProgramCategoryType @UserGuid,@GrowerOrderGuid
	

		--now handle Availability
		IF @OrderLineStatusLookupSortSequence >= @OrderLineStatusLookupOrderedSortSequence
		BEGIN
			UPDATE ReportedAvailability
			SET SalesSinceDateReported = SalesSinceDateReported + (@ActualQuantity - @OriginalQtyOrdered)
			WHERE productGuid = @ProductGuid
			AND ShipWeekGuid = @ShipWeekGuid

			--TO DO handle Linked Weeks
		END
	END -- qty changing  



	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('ActualQuantity',@ActualQuantity,1) +
		dbo.XmlSegment('OrderLineGuid',@OrderLineGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='OrderLineProcessTransaction',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OrderLineGuid,
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@XmlData=@ReturnValues