﻿






CREATE PROCEDURE [dbo].[DashboardGetDummen]
	 @FiscalYear AS NVARCHAR(50)
	
	 

AS

--SELECT * FROM shipweek WHERE year = 2017 ORDER BY week

DECLARE @Year INT
SET @Year = 0
IF @FiscalYear != 'All'
	SET @Year = (SELECT CAST(LEFT(@FiscalYear,4) AS int))

SELECT	CASE WHEN ols.Name = 'Cancelled' THEN 'Cancelled'  WHEN gs.name IN ('Invoiced','Paid','Write Off') THEN gs.Name ELSE ols.Name END AS Status,
		CASE WHEN ols.Code = 'Cancelled' THEN 'Cancelled'  WHEN gs.Code IN ('Invoiced','Paid','WriteOff') THEN RTRIM(gs.Code) ELSE RTRIM(ols.Code) END AS StatusCode,
		COUNT(DISTINCT o.ID) AS Orders,
		COUNT(ol.ID) AS OrderLines,
		ols.SortSequence,
		ISNULL(SUM(ActualPrice * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS Price,
		ISNULL(SUM(gpp.CuttingCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS CuttingCost,
		ISNULL(SUM(gpp.RoyaltyCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS RoyaltyCost,
		ISNULL(SUM(gpp.FreightCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS FreightCost,
		ISNULL(SUM(gpp.CuttingCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) * .02 AS Profit
FROM GrowerOrder o
JOIN SupplierOrder so ON o.Guid = so.GrowerOrderGuid
JOIN OrderLine ol ON so.Guid = ol.SupplierOrderGuid 
JOIN Product p ON ol.ProductGuid = p.Guid
JOIN Program pr ON p.ProgramGuid = pr.Guid
JOIN ShipWeek sw ON o.shipweekGuid = sw.Guid
JOIN ProgramSeason ps ON ps.Programguid = pr.Guid
	AND sw.MondayDate BETWEEN ps.StartDate AND ps.EndDate 
JOIN Seller se ON pr.sellerGuid = se.Guid
JOIN Lookup ols ON ol.OrderLineStatusLookupGuid = ols.Guid
JOIN Lookup gs ON o.GrowerOrderStatusLookupGuid = gs.Guid

LEFT JOIN GrowerProductProgramSeasonPrice gpp ON ol.ProductGuid = gpp.ProductGuid
			AND gpp.GrowerGuid = o.GrowerGuid 
			AND gpp.ProgramSeasonGuid = ps.Guid 

WHERE 0=0
AND se.Code = 'DMO'
AND CASE WHEN @FiscalYear = 'All' THEN 1  
	WHEN  @Year = sw.Year AND sw.week >= 14 THEN 1
	WHEN  @Year + 1 = sw.Year AND sw.week <= 13 THEN 1
	ELSE 0 END  = 1
AND o.growerguid NOT IN 
	(SELECT subjectguid FROM Triple WHERE predicatelookupguid = 
		(SELECT guid FROM lookup WHERE path = 'Predicate/TestGrower')
	) 
GROUP BY
	CASE WHEN ols.Name = 'Cancelled' THEN 'Cancelled'  WHEN gs.name IN ('Invoiced','Paid','Write Off') THEN gs.Name ELSE ols.Name END,
	CASE WHEN ols.Code = 'Cancelled' THEN 'Cancelled'  WHEN gs.Code IN ('Invoiced','Paid','WriteOff') THEN RTRIM(gs.Code) ELSE RTRIM(ols.Code) END,
	ols.SortSequence
ORDER BY ols.SortSequence