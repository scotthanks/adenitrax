﻿


--EXAMPLE:

--DECLARE @GrowerGuid AS UNIQUEIDENTIFIER

--EXECUTE GrowerGetGuid
--	@UserCode='rick.harrison',
--	@PersonGuid=null,
--	@ProcessLookupCode='Test',
--	@GrowerGuid=@GrowerGuid OUT

--SELECT @GrowerGuid AS GrowerGuid

CREATE PROCEDURE [dbo].[GrowerGetGuid]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NVARCHAR(56) = NULL,
	@PersonGuid AS UNIQUEIDENTIFIER = NULL,
	@ProcessLookupCode AS NVARCHAR(50),
	@GrowerGuid AS UNIQUEIDENTIFIER OUT
AS
	IF @UserGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @UserGuid = NULL

	SET @UserCode = LTRIM(RTRIM(@UserCode))
	IF @UserCode = ''
		SET @UserCode = NULL

	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('PersonGuid',@PersonGuid,1) +
		dbo.XmlSegment('ProcessLookupCode',@ProcessLookupCode,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerGetGuid',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	EXECUTE PersonGetGuid
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@ProcessLookupCode=@ProcessLookupCode,
		@PersonGuid=@PersonGuid OUT

	IF @PersonGuid IS NULL
		EXECUTE EventLogAdd
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@ProcessLookupCode=@ProcessLookupCode,
			@LogTypeLookupCode='PersonNotAssociatedWithProcedureCall'

	SET @GrowerGuid = (SELECT GrowerGuid FROM PersonView WHERE PersonGuid=@PersonGuid)

	IF @GrowerGuid IS NULL AND @PersonGuid IS NOT NULL
		EXECUTE EventLogAdd
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@ProcessLookupCode=@ProcessLookupCode,
			@LogTypeLookupCode='PersonNotAssociatedWithAGrower',
			@ObjectTypeLookupCode='Person',
			@ObjectGuid=@PersonGuid

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerGetGuid',
		@ObjectGuid=@OriginalEventLogGuid,
		@XmlData=@ReturnValues