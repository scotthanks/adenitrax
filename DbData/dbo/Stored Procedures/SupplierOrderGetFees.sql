﻿







CREATE PROCEDURE [dbo].[SupplierOrderGetFees]
	@SupplierOrderGuid AS UNIQUEIDENTIFIER
AS
	

	SELECT
		ID,GUID,DateDeactivated,SupplierOrderGuid, 
		FeeTypeLookupGuid,Amount,FeeStatXLookupGuid,FeeDescription,IsManual
		
	FROM SupplierOrderFee 
	WHERE SupplierOrderGuid = @SupplierOrderGuid