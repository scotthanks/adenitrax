﻿CREATE PROCEDURE [dbo].[ProductFormCategoryGetGuid]
	@UserCode AS NVARCHAR(56) = NULL,
	@ProductFormCategoryCode AS NVARCHAR(50),
	@ProcessLookupCode AS NVARCHAR(50),
	@ProductFormCategoryGuid AS UNIQUEIDENTIFIER OUT
AS
	SET @ProductFormCategoryCode = LTRIM(RTRIM(@ProductFormCategoryCode))

	SET @ProductFormCategoryGuid = NULL

	IF @ProductFormCategoryCode IS NOT NULL
		BEGIN
			SELECT @ProductFormCategoryGuid = ProductFormCategoryGuid 
			FROM ProductFormCategoryView 
			WHERE ProductFormCategoryCode = @ProductFormCategoryCode

			IF @ProductFormCategoryGuid IS NULL
				EXECUTE EventLogKeyNotFound
					@UserCode=@UserCode,
					@KeyName='ProductFormCategoryCode',
					@Key=@ProductFormCategoryCode,
					@ObjectTypeLookupCode='ProductFormCategory',
					@ProcessLookupCode=@ProcessLookupCode
		END