﻿







CREATE PROCEDURE [dbo].[DashboardGetEPS]
	 @FiscalYear as nvarchar(50),
	 @IncludeInternal as bit = 0

AS

SELECT case when LS.Name = 'Cancelled' then 'Cancelled'  
when GS.name in ('Invoiced','Paid','Write Off') then GS.Name else LS.Name end as Status
,count(DISTINCT G.ID) as Orders
,count(L.ID) as OrderLines
,cast(LS.SortSequence as decimal(14,1)) as SortSequence
,SUM(QtyOrdered) as CuttingsOrdered
,SUM(QtyShipped) as CuttingsShipped
,ISNULL(SUM(ActualCost * case when QtyShipped=0 then QtyOrdered else QtyShipped end),0) as Cost
,ISNULL(SUM(ActualPrice * case when QtyShipped=0 then QtyOrdered else QtyShipped end),0) as Price
,SUM(ActualPrice * case when QtyShipped=0 then QtyOrdered else QtyShipped end) - SUM(ActualCost * case when QtyShipped=0 then QtyOrdered else QtyShipped end)  as Profit

FROM GrowerOrder G
JOIN SupplierOrder S on G.Guid = s.GrowerOrderGuid
JOIN OrderLine L on S.Guid = L.SupplierOrderGuid 
JOIN Product p on L.ProductGuid = p.Guid 
JOIN Program pr on p.ProgramGuid = pr.Guid 
JOIN Seller se on pr.sellerGuid = se.Guid 
JOIN Lookup LS on L.OrderLineStatusLookupGuid = LS.Guid 
JOIN Lookup GS on G.GrowerOrderStatusLookupGuid = GS.Guid 
JOIN ShipWeek sw on G.shipweekGuid = sw.Guid 
WHERE 0=0  
	AND (sw.FiscalYear =@FiscalYear OR @FiscalYear = 'All')
	AND se.Code = 'EPS'
	AND ( g.growerguid not in (SELECT subjectguid from Triple where predicatelookupguid = (select guid from lookup where path = 'Predicate/TestGrower')) OR @IncludeInternal = 1)
Group By 
	case when LS.Name = 'Cancelled' then 'Cancelled'  when GS.name in ('Invoiced','Paid','Write Off') then GS.Name else LS.Name end
	,LS.SortSequence
Order by LS.SortSequence