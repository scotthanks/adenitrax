﻿







CREATE PROCEDURE [dbo].[AvailabilityUploadProcessUploadedData]
	 @JobNumber as bigint,
	 @WeekNumber as int

	 
AS
	DECLARE @SupplierCode nvarchar(20)

	--Get the supplier Code
	set @SupplierCode = (select SupplierCode from ReportedAvailabilityUploadJob where ID = @JobNumber)

	--This updates the IDs where they are blank
    UPDATE ReportedAvailabilityUpdate
    SET SupplierIdentifier =  (SELECT p.SupplierIdentifier FROM Product P JOIN Program pr ON P.ProgramGuid = PR.Guid
    WHERE p.SupplierDescription = RAU.SupplierDescription AND pr.SupplierGUID = (SELECT guid FROM Supplier WHERE code = RAU.SupplierCode)) 
    FROM ReportedAvailabilityUpdate RAU
    WHERE RunNumber = @JobNumber  AND [Week] = @WeekNumber AND RAU.SupplierIdentifier = ''
           
    --This now updates all based on ID
    UPDATE ReportedAvailabilityUpdate
    SET SupplierGuid =(SELECT guid FROM Supplier WHERE code = RAU.SupplierCode),
        ProductGuid = (	SELECT p.Guid 
						FROM Product P JOIN Program pr ON P.ProgramGuid = PR.Guid
						WHERE	p.SupplierIdentifier = RAU.SupplierIdentifier 
							AND pr.SupplierGUID = (SELECT guid FROM Supplier WHERE code = RAU.SupplierCode)),
        ShipWeekGuid = (SELECT sw.Guid FROM ShipWeek sw WHERE sw.year = RAU.YEAR AND sw.Week = RAU.Week),
        AvailabilityTypeLookupGuid = (SELECT lu.Guid FROM Lookup lu WHERE path = 'Code/AvailabilityType/' + RAU.AvailabiltiyType)
    FROM ReportedAvailabilityUpdate RAU
    WHERE RunNumber = @JobNumber  AND [Week] = @WeekNumber


	--for Grolink - update the RC 4 weeks out.
	If @SupplierCode = 'GRL'
	BEGIN


		Insert into ReportedAvailabilityUpdate 
		SELECT
		u.DateReported, u.SupplierCode,u.SupplierGuid,
		p2.SupplierIdentifier,p2.SupplierDescription,p2.Guid,
		u.Year,u.Week,
		(select Guid from ShipWeek where ShipWeek.ContinuousWeekNumber = sw.ContinuousWeekNumber + 4),
		u.Qty,
		u.AvailabiltiyType,u.AvailabilityTypeLookupGuid,
		u.RunNumber,
		u.AvailabilityUpdated,
		u.MultiplyByUnit,
		u.SellingUnit

		FROM ReportedAvailabilityUploadJob j
		Join ReportedAvailabilityUpdate u on j.ID = u.RunNumber
		Join ShipWeek sw on u.ShipWeekGuid = sw.Guid
		Join product p on u.ProductGuid = p.Guid
		Join Program pr on p.ProgramGuid = pr.guid
		Join Product p2 on p.Varietyguid = p2.VarietyGuid
			AND p2.ProgramGuid = (select pr2.Guid from Program pr2 
								  where Code = Replace(pr.Code,'u',''))    --This gets the RC program from the URC program
		Where j.ID = @JobNumber
		
	END
   
   --this updates for Eur RC
    UPDATE ReportedAvailabilityUpdate
	SET MultiplyByUnit = 1
    FROM ReportedAvailabilityUpdate RAU 
    JOIN Product P on RAU.ProductGuid = p.GUID 
    JOIN ProductForm pf on p.ProductFormGuid = pf.GUID 
    WHERE RunNumber = @JobNumber
		and pf.Code like 'EUR_RC%'

    
	UPDATE ReportedAvailabilityUpdate
	SET SellingUnit = pf.SalesUnitQty
    FROM ReportedAvailabilityUpdate RAU 
    JOIN Product P on RAU.ProductGuid = p.GUID 
    JOIN ProductForm pf on p.ProductFormGuid = pf.GUID 
    WHERE RunNumber = @JobNumber
		and MultiplyByUnit = 1

  --Update the Actual table
	UPDATE dbo.ReportedAvailability
	SET DateReported = RAU.DateReported,
		Qty = RAU.Qty * RAU.SellingUnit,
		AvailabilityTypeLookupGuid = RAU.AvailabilityTypeLookupGuid
	FROM dbo.ReportedAvailability RA
	JOIN dbo.ReportedAvailabilityUpdate RAU ON RA.ProductGuid = RAU.ProductGuid AND RA.Shipweekguid = RAU.shipweekguid
	WHERE RunNumber = @JobNumber  AND [Week] = @WeekNumber
		

    
          
    --Add a message to the job         
	INSERT INTO ReportedAvailabilityUploadJobMessage Values(newID(),
	(select Guid from ReportedAvailabilityUploadJob where ID = @JobNumber),
	getdate(),'Info','Normal',
		'Week Complete: ' + cast(@WeekNumber as nvarchar(2))
		)