﻿












CREATE PROCEDURE [dbo].[ReportDummenQuoteSummary]
	

AS
select g.Name as GrowerName
,ps.Code as ProgramSeasonCode
,ps.name as ProgramSeasonName
,pr.Code as ProgramCode
,pr.name as ProgramName
,count(*) as ProductCount
from GrowerProductProgramSeasonPrice gpp
Join Grower g on g.Guid = gpp.growerguid
Join programSeason ps on gpp.programSeasonguid = ps.guid
Join Product p on gpp.ProductGuid = p.Guid
Join program pr on p.programguid = pr.guid
Group by 
g.Name,ps.Code,ps.name,pr.Code, pr.name
Order By
g.Name,ps.Code,ps.name,pr.Code, pr.name