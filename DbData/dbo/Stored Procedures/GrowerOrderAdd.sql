﻿


--EXAMPLE:

--DECLARE @GrowerOrderGuid as UNIQUEIDENTIFIER

--EXECUTE GrowerOrderAdd
--	@UserCode='rick.harrison',
--	@GrowerGuid='92CFACCB-5DF3-4832-8328-384B3BED6E8F',
--	@ShipWeekCode='201320',
--	@programTypeCode='VA',
--	@ProductFormCategoryCode='URC',
--	@CustomerPoNo='TestPoNumber',
--	@Description='This is my order.',
--	@GrowerOrderGuid=@GrowerOrderGuid OUT

--SELECT @GrowerOrderGuid as OrderGuid

--&&& FIRST PROCEDURE WITH NO PARMS, PROCEDURE CALL AND PROCEDURE CALL COMPLETE

CREATE PROCEDURE [dbo].[GrowerOrderAdd]
	@UserGuid AS UNIQUEIDENTIFIER=NULL,
	@UserCode AS NCHAR(56)=NULL,
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@ShipWeekCode AS NCHAR(6),
	@ProductFormCategoryCode AS NCHAR(20),
	@ProgramTypeCode AS NCHAR(20),
	@OrderTypeLookupCode AS NVARCHAR(10) = 'Order',
	@CustomerPoNo AS NCHAR(10) = '',
	@PromotionalCode AS NVARCHAR(20) = '',
	@Description AS NVARCHAR(50) = '',
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = NULL OUT
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',LEFT(@UserGuid,4)+'...',1) +
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('ShipWeekCode',@ShipWeekCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('OrderTypeLookupCode',@OrderTypeLookupCode,1) +
		dbo.XmlSegment('PromotionalCode',@PromotionalCode,1) +
		dbo.XmlSegment('CustomerPoNo',@CustomerPoNo,1) +
		dbo.XmlSegment('Description',@Description,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerOrderAdd',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @PersonGuid AS UNIQUEIDENTIFIER
	EXECUTE PersonGetGuid
		@UserGuid = @UserGuid,
		@UserCode = @UserCode,
		@ProcessLookupCode='GrowerOrderAdd',
		@PersonGuid = @PersonGuid OUT

	DECLARE @VerifiedGrowerGuid AS UNIQUEIDENTIFIER
	DECLARE @GrowerId AS bigint
	SELECT
		@VerifiedGrowerGuid = GrowerGuid,
		@GrowerId = GrowerId
	FROM GrowerView 
	WHERE GrowerGuid = @GrowerGuid

	IF @VerifiedGrowerGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='GrowerGuid is not valid',
				@LogTypeLookupCode='InvalidParameters',
				@ProcessLookupCode='GrowerOrderAdd',
				@GuidValue=@GrowerGuid,
				@SeverityLookupCode='Failure'

			RETURN
		END

	DECLARE @ShipWeekGuid AS UNIQUEIDENTIFIER
	SELECT @ShipWeekGuid = ShipWeekGuid FROM ShipWeekView WHERE ShipWeekCode = @ShipWeekCode
	IF @ShipWeekGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='ShipWeekGuid is not valid',
				@LogTypeLookupCode='InvalidParameters',
				@ProcessLookupCode='GrowerOrderAdd',
				@GuidValue=@ShipWeekGuid,
				@SeverityLookupCode='Failure'
			RETURN
		END

	DECLARE @ProductFormCategoryGuid AS UNIQUEIDENTIFIER
	SELECT @ProductFormCategoryGuid = ProductFormCategoryGuid 
	FROM ProductFormCategoryView 
	WHERE ProductFormCategoryCode = @ProductFormCategoryCode

	IF @ProductFormCategoryGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='ProductFormCategoryCode is not valid',
				@LogTypeLookupCode='InvalidParameters',
				@ProcessLookupCode='GrowerOrderAdd',
				@SeverityLookupCode='Failure'

			RETURN
		END

	DECLARE @ProgramTypeGuid AS UNIQUEIDENTIFIER
	SELECT @ProgramTypeGuid = ProgramTypeGuid 
	FROM ProgramTypeView 
	WHERE ProgramTypeCode = @ProgramTypeCode

	IF @ProgramTypeGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='ProgramTypeCode is not valid',
				@LogTypeLookupCode='InvalidParameters',
				@ProcessLookupCode='GrowerOrderAdd',
				@SeverityLookupCode='Failure'

			RETURN
		END

	DECLARE @OrderTypeLookupGuid AS UNIQUEIDENTIFIER
	SELECT @OrderTypeLookupGuid = LookupGuid
	FROM LookupView
	WHERE LookupCode = @OrderTypeLookupCode AND Path LIKE 'Code/OrderType/%'

	IF @OrderTypeLookupGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='OrderTypeCode is not valid',
				@LogTypeLookupCode='InvalidParameters',
				@ProcessLookupCode='GrowerOrderAdd',
				@SeverityLookupCode='Failure'

			RETURN
		END

	IF @CustomerPoNo IS NULL
		SET @CustomerPoNo = ''
	SET @CustomerPoNo = UPPER(LTRIM(RTRIM(@CustomerPoNo)))

	SET @CustomerPoNo = LTRIM(RTRIM(@CustomerPoNo))
	
	IF @PromotionalCode IS NULL
		SET @PromotionalCode = ''
	
	IF @Description IS NULL
		SET @Description = ''

	SET @Description = LTRIM(RTRIM(@Description))

	SET @GrowerOrderGuid = newid()

	DECLARE @GrowerDefaultShipToAddressGuid AS UNIQUEIDENTIFIER
	SELECT TOP 1
		@GrowerDefaultShipToAddressGuid=GrowerAddressGuid
	FROM GrowerAddressView
	WHERE GrowerGuid=@VerifiedGrowerGuid
	ORDER BY DefaultAddress DESC

	DECLARE @GrowerDefaultPaymentTypeLookupGuid AS UNIQUEIDENTIFIER
	SELECT @GrowerDefaultPaymentTypeLookupGuid=DefaultPaymentTypeLookupGuid
	FROM GrowerView
	WHERE GrowerGuid=@VerifiedGrowerGuid

	DECLARE @GrowerDefaultCreditCardGuid AS UNIQUEIDENTIFIER
	SELECT TOP 1
		@GrowerDefaultCreditCardGuid=GrowerCreditCardGuid
	FROM GrowerCreditCardView
	WHERE GrowerGuid=@VerifiedGrowerGuid
	ORDER BY IsDefaultCard DESC

	INSERT INTO GrowerOrder
	(
		Guid,
		GrowerGuid,
		ShipWeekGuid,
		ProductFormCategoryGuid,
		ProgramTypeGuid,
		OrderNo,
		Description,
		CustomerPoNo,
		OrderTypeLookupGuid,
		DateEntered,
		ShipToAddressGuid,
		PaymentTypeLookupGuid,
		GrowerCreditCardGuid,
		PersonGuid,
		PromotionalCode
	)
	VALUES
	(
		@GrowerOrderGuid,
		@VerifiedGrowerGuid,
		@ShipWeekGuid,
		@ProductFormCategoryGuid,
		@ProgramTypeGuid,
		'unassigned', -- OrderNo
		@Description,
		@CustomerPoNo,
		@OrderTypeLookupGuid,
		CURRENT_TIMESTAMP,
		@GrowerDefaultShipToAddressGuid,
		@GrowerDefaultPaymentTypeLookupGuid,
		@GrowerDefaultCreditCardGuid,
		@PersonGuid,
		@PromotionalCode
	)


	SELECT @GrowerOrderGuid = Guid
	FROM GrowerOrder
	WHERE Guid = @GrowerOrderGuid

	DECLARE @RowCount AS INT
	SET @RowCount = @@ROWCOUNT

	IF @RowCount = 1
		BEGIN
			DECLARE @OrderNo AS NVARCHAR(20)
			SET @OrderNo = 'A' + '00' + CAST(@@IDENTITY AS NVARCHAR(20))
			UPDATE GrowerOrder SET OrderNo = @OrderNo WHERE Guid = @GrowerOrderGuid

			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@LogTypeLookupCode='RowAdd',
				@ProcessLookupCode='GrowerOrderAdd',
				@ObjectTypeLookupCode='GrowerOrder',
				@ObjectGuid=@GrowerOrderGuid
		END
	ELSE
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='@@ROWCOUNT not 1 after attempted write (See IntValue).',
				@LogTypeLookupCode='DatabaseError',
				@ProcessLookupCode='GrowerOrderAdd',
				@ObjectTypeLookupCode='GrowerOrder',
				@IntValue=@RowCount

			SET @GrowerOrderGuid = NULL
		END

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerOrderAdd',
		@ObjectTypeLookupCode='GrowerOrder',
		@ObjectGuid=@GrowerOrderGuid,
		@XmlData=@ReturnValues