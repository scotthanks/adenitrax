﻿CREATE PROCEDURE [dbo].[ListPersons]

AS
	
	SELECT * FROM Person WHERE DateDeactivated  is null ORDER BY LastName, FirstName;