﻿


CREATE PROCEDURE [dbo].[GrowerOrderUpdate]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@CustomerPoNo AS NVARCHAR(100),
	@Description AS NVARCHAR(50),
	@ShipToAddressGuid AS UNIQUEIDENTIFIER,
	@PaymentTypeLookupCode AS NCHAR(20),
	@GrowerCreditCardGuid AS UNIQUEIDENTIFIER = NULL,
	@PromotionalCode as NVARCHAR(20),
	@Success AS BIT OUT
AS
	SET @Success = 0
	
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('CustomerPoNo',@CustomerPoNo,1) +
		dbo.XmlSegment('Description',@Description,1) +
		dbo.XmlSegment('ShipToAddressGuid',@ShipToAddressGuid,1) +
		dbo.XmlSegment('PaymentTypeLookupCode',@PaymentTypeLookupCode,1) +
		dbo.XmlSegment('GrowerCreditCardGuid',@GrowerCreditCardGuid,1) + 
		dbo.XmlSegment('PromotionalCode',@PromotionalCode,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerOrderUpdate',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @PaymentTypeLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserGuid=@UserGuid,
		@ProcessLookupCode='GrowerOrderUpdate',
		@Path='Code/PaymentType',
		@LookupCode=@PaymentTypeLookupCode,
		@LookupGuid=@PaymentTypeLookupGuid OUT

	DECLARE @RowCount AS INTEGER

	if @GrowerCreditCardGuid = '00000000-0000-0000-0000-000000000000'
	Begin
		set @GrowerCreditCardGuid = null
	end
	UPDATE GrowerOrder
	SET
		CustomerPoNo=@CustomerPoNo,
		[Description]=@Description,
		ShipToAddressGuid=@ShipToAddressGuid,
		PaymentTypeLookupGuid=@PaymentTypeLookupGuid,
		GrowerCreditCardGuid=@GrowerCreditCardGuid,
		PromotionalCode = @PromotionalCode
	WHERE Guid=@GrowerOrderGuid

	SET @RowCount=@@ROWCOUNT

	IF @RowCount != 1
		EXECUTE EventLogAdd
			@Comment='RowCount after updating GrowerOrder table was not 1',
			@IntValue=@RowCount,
			@LogTypeLookupCode='RowUpdateError',
			@ProcessLookupCode='GrowerOrderUpdate',
			@UserGuid=@UserGuid,
			@GuidValue=@OriginalEventLogGuid
	ELSE
		SET @Success = 1

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('Success',@Success,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerOrderUpdate',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@XmlData=@ReturnValues