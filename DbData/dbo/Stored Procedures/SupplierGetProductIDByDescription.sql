﻿
create procedure [dbo].[SupplierGetProductIDByDescription]
	@SupplierCode AS NVARCHAR(10),
	@ProductDescription AS NVARCHAR(100)
AS
	declare @iLen integer
	set @iLen = len(@ProductDescription)

	
	SELECT SupplierIdentifier,pfc.code
	FROM Product p
	Join Program pr on p.ProgramGuid  = pr.guid
	Join Supplier s on pr.SupplierGuid = s.Guid
	Join ProductForm pf on p.ProductformGuid = pf.Guid
	Join ProductFormCategory pfc on pf.ProductFormCategoryGuid = pfc.guid

	WHERE s.Code = @SupplierCode
	and left(p.SupplierDescription,@iLen) = @ProductDescription