﻿





CREATE PROCEDURE [dbo].[GrowerOrderRecalcFees]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@IncludeSupplierOrderFees AS BIT
AS
	
	--Recalc the supplier fees if needed
	If @IncludeSupplierOrderFees = 1
	BEGIN
		
		DECLARE @SupplierOrderGuid uniqueidentifier
		DECLARE so_Cursor CURSOR FOR  
		SELECT Guid 
		FROM SupplierOrder 
		WHERE GrowerOrderGuid =  @GrowerOrderGuid
		OPEN so_Cursor   
		FETCH NEXT FROM so_Cursor INTO @SupplierOrderGuid   

		WHILE @@FETCH_STATUS = 0   
		BEGIN   
			--PRINT @supplierOrderGuid
			Exec SupplierOrderRecalcFees @UserGuid,null,@supplierOrderGuid
			FETCH NEXT FROM so_Cursor INTO @SupplierOrderGuid   
		END   

		CLOSE so_Cursor   
		DEALLOCATE so_Cursor
	END

	--Now calc the GrowerOrder Fees
	------Delete all the fees and start over
	Delete from GrowerOrderfee where GrowerOrderguid = @GrowerOrderGuid AND IsManual = 0
	
	DECLARE @FeeAmount decimal(8,2)
	Declare @PromotionalCode nvarchar(20)
	DECLARE @DiscountPercent decimal(8,3)
	DECLARE @SpeciesGuid uniqueidentifier
	DECLARE @SeriesGuid uniqueidentifier
	DECLARE @SupplierGuid uniqueidentifier
	DECLARE @ProgramTypeGuid uniqueidentifier
	DECLARE @ProductFormCategoryGuid uniqueidentifier
	DECLARE @ShipWeekStartGuid uniqueidentifier
	DECLARE @ShipWeekEndGuid uniqueidentifier
	
	
	select   @PromotionalCode= coalesce(Code,''),
	@DiscountPercent =  coalesce(DiscountPercent,0),
	@SpeciesGuid = SpeciesGuid,
	@SeriesGuid = SeriesGuid,
	@SupplierGuid = SupplierGuid,
	@ShipWeekStartGuid = ShipWeekStartGuid,
	@ShipWeekEndGuid = ShipWeekEndGuid,
	@ProgramTypeGuid = ProgramTypeGuid,
	@ProductFormCategoryGuid = ProductFormCategoryGuid
	From PromoCode where Code = (select PromotionalCode from GrowerOrder where Guid = @GrowerOrderGuid)
	
	IF @PromotionalCode ='RAKER10'
	BEGIN
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
		JOIN Variety v on olv.VarietyGuid = v.Guid
		Join Species sp on v.Speciesguid = sp.guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and olv.SupplierCode = 'RAK'
		and olv.OrderLineStatusLookupSortSequence >=0
		)
	
		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'EPSDiscount',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
	
		RETURN
	END	

	ELSE IF @PromotionalCode ='VIVERO5'
	BEGIN
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) 
		from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 	
		JOIN variety v on olv.VarietyGuid = v.Guid
		
		where GrowerOrderGuid = @GrowerOrderGuid
		and olv.SupplierCode in ('VMX')
		and v.SeriesGuid in (select Guid from SEries where code in ('SE01060','SE01085','SE00605','SE01860','SE00225'))
		and olv.OrderLineStatusLookupSortSequence >=0
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'EPSDiscount',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0

		
		RETURN
	END	

	ELSE IF @PromotionalCode ='WENKE50'
	BEGIN
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) 
		from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 	
		Join ShipWeek sw on olv.ShipWeekGuid = sw.Guid
		
		where GrowerOrderGuid = @GrowerOrderGuid
		and olv.SupplierCode in ('WEN','SUN')
		and olv.ProgramCode in( 'WELRCA','WELRCG','WENLINA','WENLINE','WENRCA','WENRCE','WENRCG'
								,'SUNLINA','SUNRCA','SUNRCE','SUNRCG')
		and sw.Year = 2016 and sw.week < 21
		and olv.OrderLineStatusLookupSortSequence >=0
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'EPSDiscount',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0

		
		RETURN
	END	
	ELSE IF @PromotionalCode ='7BEFORE17'
	BEGIN
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) 
		from OrderLineViewShowInactiveProduct olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 	
		Join ShipWeek sw on olv.ShipWeekGuid = sw.Guid
		
		where GrowerOrderGuid = @GrowerOrderGuid
		and olv.ProductFormCategoryCode = 'RC'
		and olv.ProgramTypeCode in('VA','GER')
	
		and sw.Year = 2017 and sw.week < 23
		and olv.OrderLineStatusLookupSortSequence >=0
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'EPSDiscount',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0

		
		RETURN
	END	
	
	ELSE IF @PromotionalCode ='KNOXGERB'
	BEGIN
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 	
		JOIN ProductFormCategory pfc on olv.ProductFormCategoryGuid = pfc.Guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and olv.SupplierCode = 'KNO'
		and olv.ProductFormCode = 'KNO_LIN_105'
		and olv.SpeciesCode = 'GER'
		and olv.OrderLineStatusLookupSortSequence >=0
		)
	
		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'EPSDiscount',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0

		
		RETURN
	END	

	
	

	ELSE IF @PromotionalCode ='PW25'
	BEGIN
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) 		
				from OrderLineView olv
				JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
				JOIN Variety v on olv.VarietyGuid = v.Guid
				Join GeneticOwner o on v.GeneticOwnerguid = o.guid
				where GrowerOrderGuid = @GrowerOrderGuid
				and o.Code = 'PRW'
				and olv.SupplierCode = 'EUR'
				and olv.OrderLineStatusLookupSortSequence >=0
		)
		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'EPSDiscount',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END	

	ELSE IF @PromotionalCode ='DGIREGAL'
	BEGIN
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) 		
				from OrderLineView olv
				JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
				JOIN Variety v on olv.VarietyGuid = v.Guid
				Join Species sp on v.Speciesguid = sp.guid
				where GrowerOrderGuid = @GrowerOrderGuid
				and olv.SupplierCode = 'DGI'
				and sp.Code = 'PLH'
				and olv.ShipWeekCode < '201606'
				and olv.OrderLineStatusLookupSortSequence >=0
		)
	
		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'Supplier_2_5',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END	
	
	ELSE IF @SpeciesGuid IS NOT NULL AND @ProductFormCategoryGuid IS NOT NULL
	BEGIN
		--E.G.WK21DEAL
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
		JOIN Variety v on olv.VarietyGuid = v.Guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and v.SpeciesGuid = @SpeciesGuid
		and olv.ProductFormCategoryGuid = @ProductFormCategoryGuid
		and olv.OrderLineStatusLookupSortSequence >=0
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'WeeklyDeal',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END
	ELSE IF @SeriesGuid IS NOT NULL AND @SupplierGuid IS NOT NULL
	BEGIN
		--E.G.SENETTI
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
		JOIN Variety v on olv.VarietyGuid = v.Guid
		JOIN ProductFormCategory pfc on olv.ProductFormCategoryGuid = pfc.Guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and v.SeriesGuid = @SeriesGuid
		and olv.SupplierGuid = @SupplierGuid
		and pfc.Code <> 'PRE'
		and olv.OrderLineStatusLookupSortSequence >=0
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'EPSDiscount',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END	
	ELSE IF @SeriesGuid IS NOT NULL
	BEGIN
		--E.G.WK17DEAL
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
		JOIN Variety v on olv.VarietyGuid = v.Guid
		JOIN ProductFormCategory pfc on olv.ProductFormCategoryGuid = pfc.Guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and v.SeriesGuid = @SeriesGuid
		and pfc.Code <> 'PRE'
		and olv.OrderLineStatusLookupSortSequence >=0
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'WeeklyDeal',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END	
	ELSE IF @ProgramTypeGuid IS NOT NULL
	BEGIN
		--E.G.WK15DEAL
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
		JOIN ProductFormCategory pfc on olv.ProductFormCategoryGuid = pfc.Guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and olv.ProgramTypeGuid = @ProgramTypeGuid
		and pfc.Code <> 'PRE'
		and olv.OrderLineStatusLookupSortSequence >=0
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'WeeklyDeal',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END	
	ELSE IF @SpeciesGuid IS NOT NULL
	BEGIN
		--E.G.WK13DEAL
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
		JOIN ProductFormCategory pfc on olv.ProductFormCategoryGuid = pfc.Guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and SpeciesGuid = @SpeciesGuid
		and pfc.Code <> 'PRE'
		and olv.OrderLineStatusLookupSortSequence >=0
		--and shipweekcode between (select ShipWeekCode from ShipWeekView where ShipWeekGuid = @ShipWeekStartGuid) 
		--	and (select ShipWeekCode from ShipWeekView where ShipWeekGuid = @ShipWeekEndGuid)
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'WeeklyDeal',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END	
	
	ELSE IF @SupplierGuid IS NOT NULL
	BEGIN
		--E.G.GRONSELL30
		set @FeeAmount = (select coalesce(sum(ol.QtyOrdered * ol.ActualPrice),0.00) from OrderLineView olv
		JOIN OrderLine ol on olv.OrderLineGuid = ol.guid 
		JOIN ProductFormCategory pfc on olv.ProductFormCategoryGuid = pfc.Guid
		where GrowerOrderGuid = @GrowerOrderGuid
		and olv.SupplierGuid = @SupplierGuid
		and olv.OrderLineStatusLookupSortSequence >=0
		and olv.ShipWeekCode >= (select shipweekCode from ShipWeekView where ShipWeekGuid =@ShipWeekStartGuid)   
		and olv.ShipWeekCode <= (select shipweekCode from ShipWeekView where ShipWeekGuid =@ShipWeekEndGuid)   
		)

		Exec GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		'GRONSELL30',
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0
		
		RETURN
	END	
	
	ELSE IF @DiscountPercent > 0 
	BEGIN
		DECLARE @FeeTypePath AS NVARCHAR(100)
		SET  @FeeTypePath = ( 
		SELECT CASE WHEN @PromotionalCode = 'LOYAL5' THEN  'LoyalCustomerDiscount'
		WHEN @PromotionalCode = 'INTRO5' THEN 'FirstOrderDiscount'
		ELSE 'Code/GrowerOrderFeeType/FirstOrderDiscount' END)
			
		--E.G.iNTRO5
		SET @FeeAmount = (SELECT SUM(ol.QtyOrdered * ol.ActualPrice) FROM OrderLineView olv
		JOIN OrderLine ol ON olv.OrderLineGuid = ol.guid 
		WHERE GrowerOrderGuid = @GrowerOrderGuid
		AND olv.OrderLineStatusLookupSortSequence >=0)

		EXEC GrowerOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',
		@GrowerOrderGuid,
		@FeeTypePath,
		@DiscountPercent,
		@FeeAmount,
		'Actual',
		'Promotional Discount Applied',
		0

		RETURN
	END