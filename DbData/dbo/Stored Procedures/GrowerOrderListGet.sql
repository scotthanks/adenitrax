﻿










CREATE PROCEDURE [dbo].[GrowerOrderListGet]
	 @OrderNo AS NVARCHAR(10) ='',
	 @GrowerNameLike AS NVARCHAR(50) = '',
	 @FiscalYear AS NVARCHAR(10)= 'All',
	 @WeekNumber AS INT = 0,
	 @OrderStatusCode AS NVARCHAR(50) = 'All',
	 @SellerCode NVARCHAR(50),
	 @FromDashboard AS INT = 0
	 

AS
	;WITH   zz(GrowerOrderGuid,SellerCode,IsBadGrower,GrowerName)
	AS
	(
	SELECT DISTINCT o.Guid AS GrowerOrderGuid,se.Code AS SellerCode,g.IsBadGrower,g.Name AS GrowerName
	FROM GrowerOrder o (NOLOCK)
	JOIN Grower g (NOLOCK) ON o.GrowerGuid = g.Guid
	JOIN SupplierOrder so (NOLOCK) ON o.Guid = so.GrowerOrderGuid
	JOIN Orderline ol (NOLOCK) ON so.Guid = ol.SupplierOrderGuid
	JOIN Product p (NOLOCK) ON ol.ProductGuid = p.guid
	JOIN Program pr (NOLOCK) ON p.ProgramGuid = pr.Guid
	JOIN Seller se (NOLOCK) ON pr.SellerGuid = se.Guid
	WHERE 
	 (o.OrderNo = @OrderNo OR @OrderNo = '')
		AND	(se.Code = @SellerCode OR @SellerCode = 'All')
		AND g.Name LIKE @GrowerNameLike + '%'
	)

	SELECT TOP 300
	o.Guid AS OrderGuid
	,zz.GrowerName AS GrowerName
	,o.CustomerPoNo
	,RTRIM(ov.GrowerOrderStatusLookupCode) AS GrowerOrderStatusCode
	,CASE WHEN RTRIM(ov.GrowerOrderStatusLookupCode) IN ('Invoiced','Paid') 
		THEN ov.GrowerOrderStatusLookupName 
		ELSE LowestOrderLineStatusLookupName END AS GrowerOrderStatusName
	,ov.InvoiceNo
	,CASE WHEN RTRIM(ov.GrowerOrderStatusLookupCode) IN ('Invoiced','Paid') 
		THEN ov.GrowerOrderStatusLookupCode ELSE ov.LowestOrderLineStatusLookupCode 
		END AS OrderStatus
	,ov.LowestOrderLineStatusLookupCode AS LowestOrderLineStatusCode
	,o.[Description] AS OrderDescription
	,o.OrderNo AS OrderNo
	,ov.QtyOrderedCount AS OrderQty
	,ov.QtyOrderedCount AS OrderQty
	,p.FirstName + ' ' + p.LastName AS PersonWhoPlacedOrder
	,pfc.Name AS ProductFormCategoryName
	,pt.Name AS ProgramTypeName
	,SUBSTRING(ov.ShipWeekCode,5,2) + '|' + SUBSTRING(ov.ShipWeekCode,1,4) AS ShipWeekString
	,zz.IsBadGrower
	FROM GrowerOrderSummaryByShipWeekGroupByOrderView2 ov (NOLOCK)
	JOIN GrowerOrder o (NOLOCK) ON ov.GrowerOrderGuid = o.Guid
	JOIN Person p (NOLOCK) ON ov.PersonGuid = p.Guid
	JOIN ProductFormCategory pfc (NOLOCK) ON ov.ProductFormCategoryGuid = pfc.Guid
	JOIN ProgramType pt (NOLOCK) ON ov.ProgramTypeGuid = pt.Guid
	JOIN ShipWeek sw (NOLOCK) ON ov.ShipWeekGuid = sw.Guid
	JOIN zz ON zz.GrowerOrderGuid = o.Guid


	WHERE 
		1 = 1
		AND (ov.LowestOrderLineStatusLookupCode NOT IN ('PreCart','Pending') OR @FromDashboard = 1)
		AND (sw.FiscalYear = @FiscalYear OR @FiscalYear = 'All')
		AND (sw.[Week] = @WeekNumber OR @WeekNumber = 0)
		AND (RTRIM(ov.LowestOrderLineStatusLookupCode) = @OrderStatusCode OR @OrderStatusCode = 'All')

	
       
	ORDER BY o.OrderNo DESC
	OPTION(RECOMPILE)