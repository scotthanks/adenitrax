﻿







--EXAMPLE:

--EXECUTE ShipWeekDataGet
--  @UserGuid='7714B7E5-7148-404B-9E2E-93EE8B7E3B63',
--  @UserCode='rick.harrison',
--  @BeginShipWeekCode='201407',
--  @NumberOfWeeks=10

CREATE PROCEDURE [dbo].[ShipWeekDataGet]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NVARCHAR(56) = NULL,
	@BeginShipWeekCode AS NCHAR(6),
	@NumberOfWeeks AS INTEGER
AS

	DECLARE @StartShipWeekContinuousWeekNumber AS INTEGER
	SELECT
		@StartShipWeekContinuousWeekNumber = ShipWeekContinuousWeekNumber
	FROM ShipWeekView
	WHERE
		ShipWeekCode = @BeginShipWeekCode

--select * from shipweek order by year, week
	SELECT * 
	FROM ShipWeek
	WHERE
		ContinuousWeekNumber >= @StartShipWeekContinuousWeekNumber AND
		ContinuousWeekNumber < @StartShipWeekContinuousWeekNumber + @NumberOfWeeks
		--ContinuousWeekNumber >= 157 AND
		--ContinuousWeekNumber < 157 + @NumberOfWeeks
	Order By ContinuousWeekNumber

	--select * from shipweek where week = 1 and year = 2016