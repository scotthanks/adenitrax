﻿

--EXAMPLE:

--DECLARE @GrowerOrderGuid as UNIQUEIDENTIFIER
--DECLARE @OrderTrxStatusLookupCode as NCHAR(10)

--EXECUTE GrowerOrderGetOrAdd
--  @UserCode='scotth',
--	@GrowerGuid='9CB1183C-20AC-4049-9206-25C558CA5BEF',
--	@ShipWeekCode='201320',
--	@ProductFormCategoryCode='URC',
--	@CustomerPoNo='TestPoNumber',
--	@Description='This is my order.',
--	@GrowerOrderGuid=@GrowerOrderGuid OUT

--SELECT
--  @GrowerOrderGuid as GrowerOrderGuid,
--  @OrderTrxStatusLookupCode as OrderTrxStatusLookupCode

CREATE PROCEDURE [dbo].[GrowerOrderGetOrAdd]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56),
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@ShipWeekCode AS NCHAR(6),
	@ProductFormCategoryCode AS NCHAR(20),
	@ProgramTypeCode AS NCHAR(20),
	@OrderLineStatusLookupCodeList AS NVARCHAR(1000),
	@OrderTypeLookupCode AS NVARCHAR(10) = 'Order',
	@CustomerPoNo AS NCHAR(10) = '',
	@PromotionalCode AS NVARCHAR(20) = '',
	@Description AS NVARCHAR(50) = '',
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = NULL OUT
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',LEFT(@UserGuid,4)+'...',1) +
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('ShipWeekCode',@ShipWeekCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('OrderLineStatusLookupCodeList',@OrderLineStatusLookupCodeList,1) +
		dbo.XmlSegment('OrderTypeLookupCode',@OrderTypeLookupCode,1) +
		dbo.XmlSegment('CustomerPoNo',@CustomerPoNo,1) +
		dbo.XmlSegment('PromotionalCode',@PromotionalCode,1) +
		dbo.XmlSegment('Description',@Description,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerOrderGetOrAdd',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @VerifiedGrowerGuid AS UNIQUEIDENTIFIER
	DECLARE @GrowerId AS bigint
	SELECT
		@VerifiedGrowerGuid = GrowerGuid,
		@GrowerId = GrowerId
	FROM GrowerView 
	WHERE GrowerGuid = @GrowerGuid

	IF @VerifiedGrowerGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid ,
				@UserCode=@UserCode ,
				@Comment='GrowerGuid is not valid' ,
				@LogTypeLookupCode='InvalidParameters' ,
				@ProcessLookupCode='GrowerOrderGetOrAdd' ,
				@GuidValue=@GrowerGuid ,
				@SeverityLookupCode='Failure'

			RETURN
		END

	DECLARE @ShipWeekGuid AS UNIQUEIDENTIFIER
	EXECUTE ShipWeekGetGuid
		@UserCode=@UserCode,
		@ShipWeekCode=@ShipWeekCode,
		@ProcessLookupCode='GrowerOrderGetOrAdd',
		@ShipWeekGuid=@ShipWeekGuid OUT

	DECLARE @ProductFormCategoryGuid AS UNIQUEIDENTIFIER
	EXECUTE ProductFormCategoryGetGuid
		@UserCode=@UserCode,
		@ProductFormCategoryCode=@ProductFormCategoryCode,
		@ProcessLookupCode='GrowerOrderGetOrAdd',
		@ProductFormCategoryGuid=@ProductFormCategoryGuid OUT

	DECLARE @ProgramTypeGuid AS UNIQUEIDENTIFIER
	EXECUTE ProgramTypeGetGuid
		@UserCode=@UserCode,
		@ProgramTypeCode=@ProgramTypeCode,
		@ProcessLookupCode='GrowerOrderGetOrAdd',
		@ProgramTypeGuid=@ProgramTypeGuid OUT

	DECLARE @OrderTypeLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserCode=@UserCode,
		@Path='Code/OrderType',
		@LookupCode=@OrderTypeLookupCode,
		@ProcessLookupCode='GrowerOrderGetOrAdd',
		@LookupGuid=@OrderTypeLookupGuid OUT

	SET @OrderLineStatusLookupCodeList = ',' + LTRIM(RTRIM(@OrderLineStatusLookupCodeList)) + ','

	IF @CustomerPoNo IS NULL
		SET @CustomerPoNo = ''
	SET @CustomerPoNo = UPPER(LTRIM(RTRIM(@CustomerPoNo)))

	SET @CustomerPoNo = LTRIM(RTRIM(@CustomerPoNo))

	IF @PromotionalCode IS NULL
		SET @PromotionalCode = ''

	IF @Description IS NULL
		SET @Description = ''

	SET @Description = LTRIM(RTRIM(@Description))

	SET @GrowerOrderGuid = NULL

	-- Look for GrowerOrders with a specified OrderLineStatusLookupCode. Note status is on the OrderLine, so we have to look there.
	SELECT @GrowerOrderGuid = GrowerOrderGuid
	FROM OrderLineView
	WHERE
		GrowerGuid = @GrowerGuid AND
		ShipWeekGuid = @ShipWeekGuid AND
		--ProductFormCategoryCode = @ProductFormCategoryCode AND --scott commented out Jan 25, 2015
		--ProgramTypeCode = @ProgramTypeCode AND --scott commented out Jan 25, 2015
		OrderTypeLookupCode = @OrderTypeLookupCode AND
		@OrderLineStatusLookupCodeList LIKE '%,' + LTRIM(RTRIM(OrderLineStatusLookupCode)) + ',%'

	IF @GrowerOrderGuid IS NULL
		EXECUTE GrowerOrderAdd
			@UserGuid = @UserGuid,
			@UserCode = @UserCode,
			@GrowerGuid = @GrowerGuid,
			@ShipWeekCode = @ShipWeekCode,
			@ProductFormCategoryCode = @ProductFormCategoryCode,
			@ProgramTypeCode = @ProgramTypeCode,
			@OrderTypeLookupCode = @OrderTypeLookupCode,
			@CustomerPoNo = @CustomerPoNo,
			@PromotionalCode = @PromotionalCode,
			@Description = @Description,
			@GrowerOrderGuid = @GrowerOrderGuid OUT

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerOrderGetOrAdd',
		@ObjectTypeLookupCode='GrowerOrder',
		@ObjectGuid=@GrowerOrderGuid,
		@XmlData=@ReturnValues