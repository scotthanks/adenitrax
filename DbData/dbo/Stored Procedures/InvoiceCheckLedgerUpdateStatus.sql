﻿




CREATE PROCEDURE [dbo].[InvoiceCheckLedgerUpdateStatus]
	@UserGuid AS UNIQUEIDENTIFIER,
	@InternalIDNumber  NVARCHAR(50)
	
AS
	

	Declare @OrderNo nVarChar(10)
	Declare @PaidAmount decimal(8,2)
	Declare @InvoiceAmount decimal(8,2)


	select @PaidAmount = sum(Amount),@OrderNo = o.OrderNo
	from ledger gl
	 left join Grower g on gl.GrowerGuid =  g.guid
	  left join Supplier s on gl.SupplierGuid =  s.guid
	 Join Lookup et on gl.EntryTypeLookupGuid = et.guid
	 Join Lookup glt on gl.GLAccountLookupGuid = glt.guid
	 left join growerOrder o on gl.ParentGuid = o.guid
	--left join supplierOrder so on gl.ParentGuid = so.guid
	--left join growerOrder o2 on so.growerorderguid = o2.guid
	where InternalIdNumber = @InternalIDNumber
	and et.code = 'Credit'
	and glt.code ='AccountsRecievable'
	Group by o.OrderNo


	select @InvoiceAmount = sum(Amount)
	from ledger gl
	 left join Grower g on gl.GrowerGuid =  g.guid
	  left join Supplier s on gl.SupplierGuid =  s.guid
	 Join Lookup et on gl.EntryTypeLookupGuid = et.guid
	 Join Lookup glt on gl.GLAccountLookupGuid = glt.guid
	 left join growerOrder o on gl.ParentGuid = o.guid
	--left join supplierOrder so on gl.ParentGuid = so.guid
	--left join growerOrder o2 on so.growerorderguid = o2.guid
	where InternalIdNumber = @InternalIDNumber
	and et.code = 'Debit'
	and glt.code ='AccountsRecievable'
	
		--select @OrderNo, @PaidAmount,@InvoiceAmount
	
	IF @PaidAmount = @InvoiceAmount
	BEGIN
		UPDATE GrowerOrder
		SET GrowerOrderStatusLookupGuid  = (select guid from Lookup where path = 'Code/GrowerOrderStatus/Paid')
			--select * from lookup where code = 'paid'
		WHERE ORderNo=@OrderNo
	END