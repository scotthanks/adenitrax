﻿

CREATE procedure [dbo].[CheckPhoneExists]
	@theType AS nvarchar(20),
	@theAreaCode AS decimal(3,0),
	@thePhone AS decimal(7,0),
	@bExists as Bit output
	
AS
	Declare @iCount int
	If @theType = 'Person'
	BEGIN
		set @iCount = (select count(*) 
						from person 
						where PhoneAreaCode =  @theAreaCode
							AND Phone = @thePhone)
	END
	else --grower
	BEGIN
		set @iCount = (select count(*) 
					from	Grower 
					where	PhoneAreaCode =  @theAreaCode
							AND Phone = @thePhone)
	END

	IF @iCount > 0 
		select @bExists = 1
	Else
		select @bExists = 0

	print 'b exists = ' + cast(@bExists as varchar(1))
	
	--select @bExists