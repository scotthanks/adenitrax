﻿





--EXAMPLE:
--EXECUTE GrowerOrderSummaryByShipWeekGetData
--	@UserCode='7714B7E5-7148-404B-9E2E-93EE8B7E3B63',
--	@OrderTypeLookupCode='Order',
--	@OrderLineStatusLookupCode='Pending'

--EXECUTE GrowerOrderSummaryByShipWeekGetData
--	@UserCode='rick.harrison',
--	@OrderTypeLookupCode='Order',
--	@OrderLineStatusLookupCode='Pending'

CREATE procedure [dbo].[GrowerOrderSummaryByShipWeekGetData]
	@UserCode AS NCHAR(56) = NULL,
	@GrowerGuid AS UNIQUEIDENTIFIER = NULL,
	@ShipWeekCode AS NCHAR(6) = NULL,
	@OrderTypeLookupCode AS NCHAR(20) = NULL,
	@GrowerOrderStatusLookupCode AS NCHAR(20) = NULL,
	@SupplierOrderStatusLookupCode AS NCHAR(20) = NULL,
	@OrderLineStatusLookupCode AS NCHAR(20) = NULL,
	@ShowJustMyOrders AS BIT = 1
AS
	DECLARE @UserGuid AS UNIQUEIDENTIFIER
	IF dbo.IsUniqueIdentifier(@UserCode) = 1
		SET @UserGuid = dbo.ExtractUniqueIdentifier( @userCode)

	IF @UserCode IS NULL
		SET @UserCode = ''

	IF (@UserCode = '' AND @UserGuid IS NOT NULL) OR dbo.IsUniqueIdentifier(@UserCode) = 1
		SELECT @UserCode=UserCode
		FROM Person
		WHERE UserGuid = @UserGuid

	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('ShipWeekCode',@ShipWeekCode,1) + 
		dbo.XmlSegment('OrderTypeLookupCode',@OrderTypeLookupCode,1) +
		dbo.XmlSegment('GrowerOrderStatusLookupCode',@GrowerOrderStatusLookupCode,1) +
		dbo.XmlSegment('SupplierOrderStatusLookupCode',@SupplierOrderStatusLookupCode,1) +
		dbo.XmlSegment('OrderLineStatusLookupCode',@OrderLineStatusLookupCode,1)+
		dbo.XmlSegment('ShowJustMyOrders',@ShowJustMyOrders,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserCode=@UserCode,
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerOrderSummaryByShipWeekGetData',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	-- Get the GrowerGuid from the UserCode if we are just showing one person's orders.
	IF @UserCode IS NOT NULL AND LTRIM(RTRIM(@UserCode)) != '' AND @ShowJustMyOrders = 1
		SELECT @GrowerGuid = GrowerGuid FROM PersonView WHERE UserCode = @UserCode

	-- If we are showing just one person's orders and the GrowerGuid is NULL, make it an empty Guid (causing no results to be returned)	
	IF @GrowerGuid IS NULL AND @ShowJustMyOrders = 1
		SET @GrowerGuid = CAST(CAST(0 AS binary) AS UNIQUEIDENTIFIER)

	SET @ShipWeekCode = LTRIM(RTRIM(@ShipWeekCode))
	IF @ShipWeekCode = ''
		SET @ShipWeekCode = NULL

	SET @OrderTypeLookupCode = LTRIM(RTRIM(@OrderTypeLookupCode))
	IF @OrderTypeLookupCode = ''
		SET @OrderTypeLookupCode = NULL

	SET @SupplierOrderStatusLookupCode = LTRIM(RTRIM(@SupplierOrderStatusLookupCode))
	IF @SupplierOrderStatusLookupCode = ''
		SET @SupplierOrderStatusLookupCode = NULL

	DECLARE @LocalTempGrowerSummaryByShipWeekData TABLE
	(
		GrowerGuid UNIQUEIDENTIFIER,
		GrowerOrderGuid UNIQUEIDENTIFIER,
		ProgramTypeGuid UNIQUEIDENTIFIER,
		ProductFormCategoryGuid UNIQUEIDENTIFIER,
		ShipWeekGuid UNIQUEIDENTIFIER,
		OrderTypeLookupGuid UNIQUEIDENTIFIER,
		GrowerOrderStatusLookupGuid UNIQUEIDENTIFIER,
		LowestOrderLineStatusLookupGuid UNIQUEIDENTIFIER,
		LowestSupplierOrderStatusLookupGuid UNIQUEIDENTIFIER,
		PersonGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempGrowerSummaryByShipWeekData
		SELECT
			GrowerGuid,
			GrowerOrderGuid,
			ProgramTypeGuid,
			ProductFormCategoryGuid,
			ShipWeekGuid,
			OrderTypeLookupGuid,
			GrowerOrderStatusLookupGuid,
			LowestOrderLineStatusLookupGuid,
			LowestSupplierOrderStatusLookupGuid,
			PersonGuid
	FROM GrowerOrderSummaryByShipWeekView
	WHERE
		(@GrowerGuid IS NULL OR GrowerGuid=@GrowerGuid) AND
		(@ShipWeekCode IS NULL OR ShipWeekCode=@ShipWeekCode) AND
		(@OrderTypeLookupCode IS NULL OR OrderTypeLookupCode=@OrderTypeLookupCode) AND
		(@GrowerOrderStatusLookupCode IS NULL OR GrowerOrderStatusLookupCode=@GrowerOrderStatusLookupCode) AND
		(@SupplierOrderStatusLookupCode IS NULL OR LowestSupplierOrderStatusLookupCode=@SupplierOrderStatusLookupCode) AND
		(
			(@OrderLineStatusLookupCode IS NULL OR LowestOrderLineStatusLookupCode=@OrderLineStatusLookupCode) OR
			(@OrderLineStatusLookupCode='Pending' AND CountOfPendingStatusLines > 0)
		) 
--Commented out by Scott Nov. 4, 2013		
		--AND QtyOrderedCount > 0
	GROUP BY
		GrowerGuid,
		GrowerOrderGuid,
		ProgramTypeGuid,
		ProductFormCategoryGuid,
		ShipWeekGuid,
		OrderTypeLookupGuid,
		GrowerOrderStatusLookupGuid,
		LowestSupplierOrderStatusLookupGuid,
		LowestOrderLineStatusLookupGuid,
		PersonGuid

	SELECT ShipWeek.*
	FROM ShipWeek
	WHERE ShipWeek.Guid IN
	(SELECT DISTINCT(ShipWeekGuid) FROM @LocalTempGrowerSummaryByShipWeekData)

	SELECT ProgramType.*
	FROM ProgramType
	WHERE ProgramType.Guid IN
	(SELECT DISTINCT(ProgramTypeGuid) FROM @LocalTempGrowerSummaryByShipWeekData)

	SELECT ProductFormCategory.*
	FROM ProductFormCategory
	WHERE ProductFormCategory.Guid IN
	(SELECT DISTINCT(ProductFormCategoryGuid) FROM @LocalTempGrowerSummaryByShipWeekData)

	SELECT Grower.*
	FROM Grower
	WHERE Grower.Guid IN
	(SELECT DISTINCT(GrowerGuid) FROM @LocalTempGrowerSummaryByShipWeekData)


	;WITH zz (GrowerOrderGuid,NavigationProgramTypeCode,NavigationProductFormCategoryCode)
	AS
		(Select top 1 
		olv.GrowerOrderGuid
		,olv.ProgramTypeCode as NavigationProgramTypeCode
		,olv.ProductFormCategoryCode as NavigationProductFormCategoryCode
		FROM GrowerOrder
		Join OrderLineView olv on GrowerOrder.Guid = olv.GrowerOrderGuid
		WHERE GrowerOrder.Guid IN
		(SELECT DISTINCT(GrowerOrderGuid) FROM @LocalTempGrowerSummaryByShipWeekData)
		Group by olv.GrowerOrderGuid,olv.ProgramTypeCode,olv.ProductFormCategoryCode
		Order by count(*) desc
		)
	
	SELECT GrowerOrder.ID
	,GrowerOrder.Guid
	,GrowerOrder.DateDeactivated
	,GrowerOrder.GrowerGuid
	,GrowerOrder.ShipWeekGuid
	,GrowerOrder.OrderNo
	,GrowerOrder.Description
	,GrowerOrder.CustomerPONO
	,GrowerOrder.OrderTypeLookupGuid
	,GrowerOrder.ProductFormCategoryGuid
	,GrowerOrder.ProgramTypeguid
	,GrowerOrder.ShipToAddressGuid
	,GrowerOrder.PaymentTypeLookupGuid
	,GrowerOrder.GrowerOrderStatusLookupGuid
	,GrowerOrder.DateEntered
	,GrowerOrder.GrowerCreditCardGuid
	,GrowerOrder.PersonGuid
	,GrowerOrder.PromotionalCode
	,GrowerOrder.GrowerShipMethodCode
	,GrowerOrder.SellerGuid
	,Coalesce(zz.NavigationProgramTypeCode,'VA') as NavigationProgramTypeCode
	,Coalesce(zz.NavigationProductFormCategoryCode,'RC') as NavigationProductFormCategoryCode
	FROM GrowerOrder
	Join zz  on GrowerOrder.Guid = zz.GrowerOrderGuid
	




	SELECT  *
	--Rick original line
	--FROM GrowerOrderSummaryByShipWeekView
	--Scott change on Nov 1, 2013
	FROM GrowerOrderSummaryByShipWeekGroupByOrderView
	
	WHERE
		(@GrowerGuid IS NULL OR GrowerGuid=@GrowerGuid) AND
		(@ShipWeekCode IS NULL OR ShipWeekCode=@ShipWeekCode) AND
		(@OrderTypeLookupCode IS NULL OR OrderTypeLookupCode=@OrderTypeLookupCode) AND
		(@GrowerOrderStatusLookupCode IS NULL OR GrowerOrderStatusLookupCode=@GrowerOrderStatusLookupCode) AND
		(@SupplierOrderStatusLookupCode IS NULL OR LowestSupplierOrderStatusLookupCode=@SupplierOrderStatusLookupCode) AND
		(
			(@OrderLineStatusLookupCode IS NULL OR LowestOrderLineStatusLookupCode=@OrderLineStatusLookupCode) OR
			(@OrderLineStatusLookupCode='Pending' AND CountOfPendingStatusLines > 0)
		) 
		--Commented out by Scott Nov. 4, 2013	
		--AND  QtyOrderedCount > 0

	SELECT *
	FROM Lookup
	WHERE Guid IN
	(
		SELECT DISTINCT(OrderTypeLookupGuid) FROM @LocalTempGrowerSummaryByShipWeekData
		UNION
		SELECT DISTINCT(GrowerOrderStatusLookupGuid) FROM @LocalTempGrowerSummaryByShipWeekData
		UNION
		SELECT DISTINCT(LowestSupplierOrderStatusLookupGuid) FROM @LocalTempGrowerSummaryByShipWeekData
		UNION
		SELECT DISTINCT(LowestOrderLineStatusLookupGuid) FROM @LocalTempGrowerSummaryByShipWeekData
	)

	SELECT Person.*
	FROM Person
	WHERE Guid IN
	(SELECT DISTINCT(PersonGuid) FROM @LocalTempGrowerSummaryByShipWeekData)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerOrderSummaryByShipWeekGetData',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserCode=@UserCode