﻿




create PROCEDURE [dbo].[catalog_SuppliersGet]
	@ProgramTypeCode AS NCHAR(50) = null,
	@ProductFormCategoryCode AS NCHAR(50) = null,
	@GeneticOwnerCodeList AS NVARCHAR(500) = null,
	@SpeciesCodeList AS NVARCHAR(500) = null,
	@UserGuid AS UNIQUEIDENTIFIER = Null
AS
	Declare @CountryGuid AS UNIQUEIDENTIFIER
	If  NOT @UserGuid = '{00000000-0000-0000-0000-000000000000}'
	    set @CountryGuid = (select s.CountryGuid
							From Person p
							Join Grower g on p.Growerguid = g.Guid
							Join Address a on g.AddressGuid = a.Guid
							Join State s on a.StateGuid = s.Guid
							WHERE p.UserGuid = @UserGuid)
	
	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF LEN(RTRIM(@ProgramTypeCode)) > 3
		BEGIN
			SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE '%' + RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SET @ProgramTypeCode = '*'
		END

	PRINT '@ProgramTypeCode=' + @ProgramTypeCode

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF LEN(RTRIM(@ProductFormCategoryCode)) > 3
		BEGIN
			SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE '%' + RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SET @ProductFormCategoryCode = '*'
		END

	PRINT '@ProductFormCategoryCode=' + @ProductFormCategoryCode

	IF @GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%*%'
		SET @GeneticOwnerCodeList = NULL
	ELSE
		SET @GeneticOwnerCodeList = ',' + @GeneticOwnerCodeList + ','

	IF @SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%*%'
		SET @SpeciesCodeList = NULL
	ELSE
		SET @SpeciesCodeList = ',' + @SpeciesCodeList + ','

	DECLARE @LocalTempSupplierData TABLE
	(
		SupplierGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempSupplierData
	SELECT
		SupplierGuid
	FROM ProductView
	WHERE
		(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
		(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
		(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(GeneticOwnerCode) + ',%') AND
		(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(SpeciesCode) + ',%')
	GROUP BY
		SupplierGuid
    
	If Not @CountryGuid IS NULL
		Delete from @LocalTempSupplierData Where SupplierGuid not in 
		(
		Select pr.SupplierGuid
		From ProgramView pr
		Join ProgramCountry pc on pr.ProgramGuid = pc.ProgramGuid
		
		Where pc.CountryGuid = @CountryGuid AND
		(@ProgramTypeCode IS NULL OR pr.ProgramTypeCode=@ProgramTypeCode) AND
		(@ProductFormCategoryCode IS NULL OR pr.ProductFormCategoryCode=@ProductFormCategoryCode)
		Group by pr.SupplierGuid
		)


	SELECT Supplier.*
	FROM Supplier
	WHERE Supplier.Guid IN
	(SELECT DISTINCT(SupplierGuid) FROM @LocalTempSupplierData)