﻿CREATE PROCEDURE [dbo].[EventLogKeyNotFound]
	@UserCode AS NVARCHAR(56) = NULL,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@KeyName AS NVARCHAR(50),
	@Key AS NVARCHAR(50),
	@ObjectTypeLookupCode AS NVARCHAR(50),
	@ProcessLookupCode AS NVARCHAR(50)
AS
	DECLARE @Comment AS NVARCHAR(50)

	SET @Comment = 'The ' + @KeyName + ' ' + @Key + ' is not valid.'

	EXECUTE EventLogAdd
		@LogTypeLookupCode='KeyNotFound',
		@ProcessLookupCode=@ProcessLookupCode,
		@ObjectTypeLookupCode=@ObjectTypeLookupCode,
		@UserCode=@UserCode,
		@UserGuid=@UserGuid,
		@Comment=@Comment