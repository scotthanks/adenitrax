﻿





CREATE PROCEDURE [dbo].[MarkGrowerOrderReminder]
	@OrderLineGuid AS uniqueidentifier,
	@Days int
	
AS
	

IF @Days = 2
BEGIN
	UPDATE GrowerOrderReminder set Reminder2Day = 1 where OrderLineGuid = @OrderLineGuid
END
ELSE
BEGIN
	UPDATE GrowerOrderReminder set Reminder7Day = 1 where OrderLineGuid = @OrderLineGuid
END