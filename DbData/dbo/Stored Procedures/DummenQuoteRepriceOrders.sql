﻿





CREATE PROCEDURE [dbo].[DummenQuoteRepriceOrders]
	@DaysBack as Int
AS


Declare @GrowerName as nvarchar(50)
Declare @ProgramSeasonGuid as uniqueidentifier
Declare @StartYear int
Declare @StartWeek int
Declare @EndYear int
Declare @EndWeek int 
Declare @fromDate datetime

Select @fromDate = dateadd(day,-1 * @DaysBack,getdate())

DECLARE dg_Cursor CURSOR FOR  

	select distinct g.Name, ps.Guid as ProgramSeasonGuid,
	--ps.Guid,ps.Code,
	--ps.StartDate,
		--dateadd(week, datediff(week, 0, ps.StartDate), 7),
		sws.year,
		sws.week,
	--ps.EndDate,
	--	dateadd(week, datediff(week, 0, ps.EndDate), 7),
		swe.year,
		swe.week
	From GrowerProductProgramSeasonPrice pr
	Join Grower g on pr.GrowerGuid = g.Guid
	Join ProgramSeason ps on pr.ProgramSeasonGuid = ps.Guid
	Join Shipweek sws on sws.MondayDate = dateadd(week, datediff(week, 0, ps.StartDate), 7)
	Join Shipweek swe on swe.MondayDate = dateadd(week, datediff(week, 0, ps.EndDate), 7)
	--where pr.LastUpdateDate >= @fromDate

	

OPEN dg_Cursor   
FETCH NEXT FROM dg_Cursor INTO @GrowerName,@ProgramSeasonGuid,@StartYear,@StartWeek,@EndYear,@EndWeek   

WHILE @@FETCH_STATUS = 0   
BEGIN   
		Print @GrowerName   + '  -  ' + cast(@ProgramSeasonGuid as varchar(36))
		exec GrowerRepriceOrders @GrowerName,@ProgramSeasonGuid,@StartYear,@StartWeek,@EndYear,@EndWeek
		
		FETCH NEXT FROM dg_Cursor INTO @GrowerName,@ProgramSeasonGuid,@StartYear,@StartWeek,@EndYear,@EndWeek
	 
END   

CLOSE dg_Cursor   
DEALLOCATE dg_Cursor