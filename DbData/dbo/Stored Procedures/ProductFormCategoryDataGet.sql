﻿
--EXAMPLE:

--EXECUTE ProductFormCategoryDataGet

CREATE PROCEDURE [dbo].[ProductFormCategoryDataGet]
	@ProgramTypeCode AS NCHAR(50) = null,
	@ProductFormCategoryCode AS NCHAR(50) = null
AS
	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF LEN(RTRIM(@ProgramTypeCode)) > 3
		BEGIN
			SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE '%' + RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SET @ProgramTypeCode = '*'
		END

	PRINT '@ProgramTypeCode=' + @ProgramTypeCode

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF LEN(RTRIM(@ProductFormCategoryCode)) > 3
		BEGIN
			SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE '%' + RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SET @ProductFormCategoryCode = '*'
		END

	PRINT '@ProductFormCategoryCode=' + @ProductFormCategoryCode

	DECLARE @LocalTempData TABLE
	(
		ProgramTypeGuid UNIQUEIDENTIFIER,
		ProductFormCategoryGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempData
	SELECT
		ProgramTypeGuid,
		ProductFormCategoryGuid
	FROM ProductFormCategoryProgramTypeCombinationView
	WHERE
		(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
		(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode)

	PRINT CAST(@@ROWCOUNT AS NVARCHAR(10)) + ' Combinations found.'

	SELECT ProgramType.*
	FROM ProgramType
	WHERE ProgramType.Guid IN
	(SELECT DISTINCT(ProgramTypeGuid) FROM @LocalTempData)

	SELECT ProductFormCategory.*
	FROM ProductFormCategory
	WHERE ProductFormCategory.Guid IN
	(SELECT DISTINCT(ProductFormCategoryGuid) FROM @LocalTempData)

	SELECT * FROM @LocalTempData