﻿







CREATE PROCEDURE [dbo].[GrowerAvailabilityGet]
	@GrowerGuid uniqueidentifier,
	@ProgramCodes nvarchar(max),
	@StartWeek int,
	@EndWeek int

AS
	
	DECLARE @pad_characters VARCHAR(10)
	DECLARE @GrowerUsesDummenPriceMethod bit
	DECLARE @Header nvarchar(max)
	DECLARE @Footer nvarchar(max)


	SET @pad_characters = '0000000000'
	SET @GrowerUsesDummenPriceMethod  = (Select UseDummenPriceMethod 
										 FROM Grower 
										 WHERE Guid = @GrowerGuid)

	
	select @Header = '<EPSAvailabiilty><SendDate>' + CONVERT(VARCHAR(24),GETDATE(),126) + '</SendDate>'
	 + '<StartWeek>' + cast(@StartWeek as varchar(10))  + '</StartWeek>'
	 + '<EndWeek>' + cast(@EndWeek as varchar(10))  + '</EndWeek>'
	 + '<ProgramCodes>' 
		 + '<Program>' + 
			replace(@ProgramCodes,',','</Program><Program>')
		 + '</Program>'  
	 + '</ProgramCodes>'
	 + '<ItemWeeks>'

    select @Footer =  '</ItemWeeks>' 
					+ '</EPSAvailabiilty>'



	
	SELECT value as Code
	INTO #Programs
	FROM split(',',@ProgramCodes)
	
	
	SELECT  @Header  + (
		SELECT top 100000 ProductID, ShipWeek,Quantity,Status
		FROM 
			(SELECT top 100000
				P.ID as ProductID 
				,sw.ShipWeekCode as ShipWeek
				,ra.Qty as Quantity
				,rtrim(rat.Code) as Status
			FROM GrowerProductProgramSeasonPrice gpp
			Join Grower g on g.Guid = gpp.GrowerGuid
			Join Product p on gpp.ProductGuid = p.Guid
			Join Program pr on p.ProgramGuid = pr.Guid
			Join Seller se on pr.SellerGuid = se.Guid
			Join #Programs tp on pr.Code = tp.Code
			Join ReportedAvailability ra on p.Guid = ra.ProductGuid
			Join Lookup rat on rat.Guid = ra.AvailabilityTypeLookupGuid
			Join ShipweekView sw on ra.ShipWeekGuid = sw.ShipWeekGuid

			where g.Guid =   @GrowerGuid
			and sw.ShipWeekCode >= @StartWeek 
			and sw.ShipWeekCode <= @EndWeek
			and se.Code = 'DMO'

			UNION
		
			SELECT top 100000 
				P.ID as ProductID 
				,sw.ShipWeekCode as ShipWeek
				,ra.Qty as Quantity
				,rtrim(rat.Code) as Status
			FROM GrowerProgram gp
			Join Grower g on g.Guid = gp.GrowerGuid
			Join Program pr on gp.ProgramGuid = pr.Guid
			Join Seller se on pr.SellerGuid = se.Guid
			Join #Programs tp on pr.Code = tp.Code
			Join Product p on pr.Guid = p.ProgramGuid
			Join ReportedAvailability ra on p.Guid = ra.ProductGuid
			Join Lookup rat on rat.Guid = ra.AvailabilityTypeLookupGuid
			Join ShipweekView sw on ra.ShipWeekGuid = sw.ShipWeekGuid

			where g.Guid =   @GrowerGuid
			and sw.ShipWeekCode >= @StartWeek 
			and sw.ShipWeekCode <= @EndWeek
			and se.Code = 'EPS'
			)
		as theData
		Order by 		1,2
		FOR XML PATH ('ItemWeek') 

	) + @Footer as XMLString;  


	Drop table #Programs