﻿

CREATE PROCEDURE [dbo].[catalog_SpeciesSuppliersGet]
	@categoryCode AS NCHAR(50) = null,
	@productFormCode AS NCHAR(50) = null
	
AS


	
	SELECT Distinct
		SpeciesCode,SupplierCode
	FROM ProductView
	WHERE
		ProgramTypeCode= @categoryCode AND
		ProductFormCategoryCode = @productFormCode
		
	Order BY
		SpeciesCode,SupplierCode