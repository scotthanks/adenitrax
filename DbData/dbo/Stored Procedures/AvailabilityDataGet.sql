﻿













--EXAMPLES:

--EXECUTE AvailabilityDataGet
--	@UserCode='scotth',
--	@ShipWeekCode='201402',
--	@WeeksBefore=1, 
--	@weeksAfter=3, 
--	@ProgramTypeCode='GER',
--	@ProductFormCategoryCode='RC',
--	@SupplierCodeList='RAK',
--	@GeneticOwnerCodeList='*',
--	@SpeciesCodeList='PLS',
--	@VarietyCodeList='*',
--	@ProductGuid=NULL,
--	@VarietyMatchOptional=1


--EXECUTE AvailabilityDataGet
--	@UserCode='rick.harrison',
--	@ShipWeekCode='201402',
--	@WeeksBefore=1, 
--	@weeksAfter=3, 
--	@ProgramTypeCode='VA',
--	@ProductFormCategoryCode='RC',
--	@SupplierCodeList='LIN,MLM,WEL',
--	@SpeciesCodeList='ACA,ACH,AGE,ALT,ANA,ANG,ARC,ARG,ART,ASP,BAC,BAS,BEA,BEP,BEG,BID,BRA,CLN,CAL,CPT,CAS,CHR,CLE,COL,CON,CVL,CRD,COR,COS,CRO,CUP,DAH,DIA,DIS,DIP,DOR,DRA,DUR,EUP,EUR,EVO,FEL,FCH,GAL,GAM,GAU,GAZ,GMI,GLE,GRA,HEL,HTP,HEU,IMP,NGI,IMW,IPO,IRE,IVY,KWK,LAM,LAN,LAV,LOL,LOB,LOP,LOT,LYS,MAN,MEC,MEN,NAS,NEM,NIE,OST,OTO,OXA,PAC,PEN,PAS,SPT,PER,PTC,PET,PHL,PLE,POR,RHO,RUE,SAL,SAN,SCA,SET,SNA,STR,THU,THY,TOR,WNJ,TRI,VER,VIN,VIV,VIO',
--	@VarietyMatchOptional=1

--EXECUTE AvailabilityDataGet
--	@UserCode='rick.harrison',
--	@ShipWeekCode='201330',
--	@WeeksBefore=1, 
--	@weeksAfter=3, 
--	@ProgramTypeCode='VA',
--	@ProductFormCategoryCode='RC', 
--	@SupplierCodeList='DUW,MLM,SYN,WEL',
--	@GeneticOwnerCodeList=NULL,
--	@SpeciesCodeList=NULL, 
--	@VarietyCodeList='S1070',
--	@VarietyMatchOptional=1

--EXECUTE AvailabilityDataGet
--	@UserCode='rick.harrison',
--	@ShipWeekCode='201330',
--	@WeeksBefore=1, 
--	@weeksAfter=3, 
--	@ProductGuid='BE4421E4-E90E-4B60-8D58-FD893D2A0631'

--EXECUTE AvailabilityDataGet
--	@UserCode='rick.harrison',
--	@ShipWeekCode='201418',
--	@WeeksBefore=1, 
--	@weeksAfter=3, 
--	@ProgramTypeCode='VA',
--	@ProductFormCategoryCode='RC', 
--	@SupplierCodeList='LIN,MLM,WEL',
--	@GeneticOwnerCodeList=NULL,
--	@SpeciesCodeList='ACA,ACH,AGE,ALT,ANA,ANG,ARC,ARG,ART,ASP,BAC,BAS,BEA,BEP,BEG,BID,BRA,CLN,CAL,CPT,CAS,CHR,CLE,COL,CON,CVL,CRD,COR,COS,CRO,CUP,DAH,DIA,DIS,DIP,DOR,DRA,DUR,EUP,EUR,EVO,FEL,FCH,GAL,GAM,GAU,GAZ,GMI,GLE,GRA,HEL,HTP,HEU,IMP,NGI,IMW,IPO,IRE,IVY,KWK,LAM,LAN,LAV,LOL,LOB,LOP,LOT,LYS,MAN,MEC,MEN,NAS,NEM,NIE,OST,OTO,OXA,PAC,PEN,PAS,SPT,PER,PTC,PET,PHL,PLE,POR,RHO,RUE,SAL,SAN,SCA,SET,SNA,STR,THU,THY,TOR,WNJ,TRI,VER,VIN,VIV,VIO', 
--	@VarietyCodeList='*',
--	@VarietyMatchOptional=1

CREATE PROCEDURE [dbo].[AvailabilityDataGet]
	@UserGuid AS UNIQUEIDENTIFIER=NULL,
	@UserCode AS NVARCHAR(56)=NULL,
	@GrowerGuid AS UNIQUEIDENTIFIER=NULL,
	@ShipWeekCode AS NCHAR(6),
	@WeeksBefore AS INTEGER = 0,
	@WeeksAfter AS INTEGER = 0,
	@ProgramTypeCode AS NCHAR(50) = NULL,
	@ProductFormCategoryCode AS NCHAR(50) = NULL,
	@SupplierCodeList AS NVARCHAR(3000) = NULL,
	@GeneticOwnerCodeList AS NVARCHAR(3000) = NULL,
	@SpeciesCodeList AS NVARCHAR(3000) = NULL,
	@VarietyCodeList AS NVARCHAR(3000) = NULL,
	@ProductGuid AS UNIQUEIDENTIFIER = NULL,
	@VarietyMatchOptional AS BIT = 0,
	@IncludePossibleSubstitutes AS BIT = 0,
	@OrganicOnly AS BIT = 0,
	@SellerCode AS NVARCHAR(30) = NULL
AS
	DECLARE @bDebug as BIT = 0
	
	If @bDebug = 1  print 'start' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('ShipWeekCode',@ShipWeekCode,1) +
		dbo.XmlSegment('WeeksBefore',@WeeksBefore,1) +
		dbo.XmlSegment('WeeksAfter',@WeeksAfter,1) +
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('SupplierCodeList',@SupplierCodeList,1) +
		dbo.XmlSegment('GeneticOwnerCodeList',@GeneticOwnerCodeList,1) +
		dbo.XmlSegment('SpeciesCodeList',@SpeciesCodeList,1) +
		dbo.XmlSegment('VarietyCodeList',@VarietyCodeList,1) +
		dbo.XmlSegment('ProductGuid',@ProductGuid,1) +
		dbo.XmlSegment('VarietyMatchOptional',@VarietyMatchOptional,1) +
		dbo.XmlSegment('IncludePossibleSubstitutes',@IncludePossibleSubstitutes,1) +
		dbo.XmlSegment('OrganicOnly',@OrganicOnly,1),
		0
	)



	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER	
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='AvailabilityDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	IF @ProductGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @ProductGuid = NULL

	IF @WeeksBefore IS NULL OR @WeeksBefore < 0
		Set @WeeksBefore = 0

	IF @WeeksAfter IS NULL OR @WeeksAfter < 0
		SET @WeeksAfter = 0

	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF @SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%*%'
		SET @SupplierCodeList = NULL
	ELSE
		SET @SupplierCodeList = ',' + @SupplierCodeList + ','

	IF @GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%*%'
		SET @GeneticOwnerCodeList = NULL
	ELSE
		SET @GeneticOwnerCodeList = ',' + @GeneticOwnerCodeList + ','

	IF @SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%*%'
		SET @SpeciesCodeList = NULL
	ELSE
		SET @SpeciesCodeList = ',' + @SpeciesCodeList + ','

	IF @VarietyCodeList IS NULL OR @VarietyCodeList LIKE '%*%'
		SET @VarietyCodeList = NULL
	ELSE
		SET @VarietyCodeList = ',' + @VarietyCodeList + ','

	DECLARE @ProductFormCode AS NCHAR(50)
	SET @ProductFormCode = NULL
	IF @IncludePossibleSubstitutes = 1
		BEGIN
			IF @ProductGuid IS NULL
				BEGIN
					EXECUTE EventLogAdd
						@UserGuid=@UserGuid,
						@UserCode=@UserCode,
						@Comment='ProductGuid must be specified with IncludePossibleSubstitutions flag.',
						@LogTypeLookupCode='InvalidParameters',
						@ProcessLookupCode='AvailabilityDataGet',
						@ObjectGuid=@OriginalEventLogGuid
					RETURN
				END
			ELSE
				BEGIN
					SELECT
						@ProgramTypeCode = ProgramTypeCode,
						@ProductFormCode = ProductFormCode,
						@SupplierCodeList = SupplierCode
					FROM ProductView
						WHERE ProductGuid = @ProductGuid

					SET @SupplierCodeList = ',' + LTRIM(RTRIM(@SupplierCodeList)) + ','

					SET @ProductGuid = NULL
				END
		END

	IF @GrowerGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		SET @GrowerGuid = NULL

	IF @GrowerGuid IS NULL
		EXECUTE GrowerGetGuid
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@PersonGuid=NULL,
			@ProcessLookupCode='AvailabilityDataGet',
			@GrowerGuid=@GrowerGuid OUT


	-- KLUDGE!!! THIS MAKES THE PROCEDURE WORK CORRECTLY, BUT I DON'T LIKE IT.
	SELECT @SpeciesCodeList = SpeciesCode FROM VarietyView WHERE @VarietyCodeList LIKE '%,' + RTRIM(VarietyCode) + ',%'
	SET @SpeciesCodeList = ',' + RTRIM(@SpeciesCodeList) + ','

	DECLARE @LocalTempShipWeeks TABLE
	(
		ShipWeekGuid UNIQUEIDENTIFIER
	)
	
	If @bDebug = 1  print 'start LocalTempShipWeeks' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	INSERT INTO @LocalTempShipWeeks
	SELECT ShipWeekView.ShipWeekGuid
	FROM ShipWeekView
		CROSS JOIN ShipWeekView AS SelectedShipWeek
		CROSS JOIN ShipWeekView AS FirstShipWeek
		CROSS JOIN ShipWeekView AS LastShipWeek
	WHERE
		SelectedShipWeek.ShipWeekCode = @ShipWeekCode AND
		FirstShipWeek.ShipWeekContinuousWeekNumber = SelectedShipWeek.ShipWeekContinuousWeekNumber - @WeeksBefore AND
		LastShipWeek.ShipWeekContinuousWeekNumber = SelectedShipWeek.ShipWeekContinuousWeekNumber + @WeeksAfter AND
		ShipWeekView.ShipWeekContinuousWeekNumber >= FirstShipWeek.ShipWeekContinuousWeekNumber AND
		ShipWeekView.ShipWeekContinuousWeekNumber <= LastShipWeek.ShipWeekContinuousWeekNumber
	
	If @bDebug = 1  print 'end LocalTempShipWeeks' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	--DECLARE @LocalTempAvailability TABLE
	--(
	--	ShipWeekGuid UNIQUEIDENTIFIER,
	--	ProgramTypeGuid UNIQUEIDENTIFIER,
	--	ProductFormCategoryGuid UNIQUEIDENTIFIER,
	--	ProductFormGuid UNIQUEIDENTIFIER,
	--	SpeciesGuid UNIQUEIDENTIFIER,
	--	SupplierGuid UNIQUEIDENTIFIER,
	--	VarietyGuid UNIQUEIDENTIFIER,
	--	ProgramGuid UNIQUEIDENTIFIER,
	--	ProductGuid UNIQUEIDENTIFIER,
	--	ReportedAvailabilityGuid UNIQUEIDENTIFIER
	--)

	If @bDebug = 1  print 'start LocalTempAvailability' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	--INSERT INTO @LocalTempAvailability
	--SELECT
	--	ShipWeekGuid,
	--	ProgramTypeGuid,
	--	ProductFormCategoryGuid,
	--	ProductFormGuid,
	--	SpeciesGuid,
	--	SupplierGuid,
	--	VarietyGuid,
	--	ProgramGuid,
	--	ProductGuid,
	--	ReportedAvailabilityGuid
	--FROM AvailabilityView
	--WHERE
	--	ShipWeekGuid IN (SELECT ShipWeekGuid from @LocalTempShipWeeks) AND
	--	(ISNULL(@ProductGuid, ProductGuid) = ProductGuid) AND
	--	(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
	--	(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
	--	(@ProductFormCode IS NULL OR ProductFormCode=@ProductFormCode) AND
	--	(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(SupplierCode) + ',%') AND
	--	(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(GeneticOwnerCode) + ',%') AND
	--	(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(SpeciesCode) + ',%') AND
	--	(@VarietyMatchOptional = 1 OR @VarietyCodeList IS NULL OR @VarietyCodeList LIKE '%,' + RTRIM(VarietyCode) + ',%')
	--GROUP BY
	--	ShipWeekGuid,
	--	ProgramTypeGuid,
	--	ProductFormCategoryGuid,
	--	ProductFormGuid,
	--	SpeciesGuid,
	--	SupplierGuid,
	--	VarietyGuid,
	--	VarietyCode,
	--	ProgramGuid,
	--	ProductGuid,
	--	ReportedAvailabilityGuid
	create table #LocalTempAvailability(
		ShipWeekGuid uniqueidentifier NOT NULL,
		ProgramTypeGuid uniqueidentifier NOT NULL,
		ProductFormCategoryGuid uniqueidentifier NOT NULL,
		ProductFormGuid uniqueidentifier NOT NULL,
		SpeciesGuid uniqueidentifier NOT NULL,
		SupplierGuid uniqueidentifier NOT NULL,
		VarietyGuid uniqueidentifier NOT NULL,
		ProgramGuid uniqueidentifier NOT NULL,
		ProductGuid uniqueidentifier NOT NULL,
		ReportedAvailabilityGuid uniqueidentifier NOT NULL,
	)
	If @SellerCode = 'EPS'
		BEGIN
			INSERT INTO #LocalTempAvailability
			SELECT
				av.ShipWeekGuid,
				av.ProgramTypeGuid,
				av.ProductFormCategoryGuid,
				av.ProductFormGuid,
				av.SpeciesGuid,
				av.SupplierGuid,
				av.VarietyGuid,
				av.ProgramGuid,
				av.ProductGuid,
				av.ReportedAvailabilityGuid
			--INTO #LocalTempAvailability
			FROM ReportedAvailabilityView av
			--left Join ProgramSeason ps on ps.Programguid = av.ProgramGuid
				--and av.ShipWeekMondayDate between ps.StartDate and ps.EndDate
			--left Join GrowerProductProgramSeasonPrice gp on av.ProductGuid = gp.ProductGuid
			--	and gp.GrowerGuid = @GrowerGuid and gp.ProgramSeasonGuid = ps.Guid
	
			WHERE
				av.ShipWeekGuid IN (SELECT ShipWeekGuid from @LocalTempShipWeeks) AND
				--(case	when 
				--			@SellerCode = 'EPS' and
							 --ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid 
							--then 1
					--	when 
					--		@SellerCode = 'DMO' 
					--		AND gp.ProductGuid = av.ProductGuid 
					--		AND gp.DateDeactivated IS NULL 
					--		then 1
					--	else 0
					--end) = 1 
					--AND
				--(ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid) AND
				(@ProgramTypeCode IS NULL OR av.ProgramTypeCode=@ProgramTypeCode) AND
				(@ProductFormCategoryCode IS NULL OR av.ProductFormCategoryCode=@ProductFormCategoryCode) AND
				(@ProductFormCode IS NULL OR av.ProductFormCode=@ProductFormCode) AND
				(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(av.SupplierCode) + ',%') AND
				(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(av.GeneticOwnerCode) + ',%') AND
				(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(av.SpeciesCode) + ',%') AND
				(@VarietyMatchOptional = 1 OR @VarietyCodeList IS NULL OR @VarietyCodeList LIKE '%,' + RTRIM(av.VarietyCode) + ',%') AND
				(@OrganicOnly = 0 OR av.IsOrganic = 1 )  --AND
				--(av.IsExclusive = 0 OR gp.ProductGuid is not null)
			GROUP BY
				av.ShipWeekGuid,
				av.ProgramTypeGuid,
				av.ProductFormCategoryGuid,
				av.ProductFormGuid,
				av.SpeciesGuid,
				av.SupplierGuid,
				av.VarietyGuid,
				av.VarietyCode,
				av.ProgramGuid,
				av.ProductGuid,
				av.ReportedAvailabilityGuid
		END
	Else
	BEGIN
		INSERT INTO #LocalTempAvailability
		SELECT
			av.ShipWeekGuid,
			av.ProgramTypeGuid,
			av.ProductFormCategoryGuid,
			av.ProductFormGuid,
			av.SpeciesGuid,
			av.SupplierGuid,
			av.VarietyGuid,
			av.ProgramGuid,
			av.ProductGuid,
			av.ReportedAvailabilityGuid
		--INTO #LocalTempAvailability2
		FROM ReportedAvailabilityView av
		Join ProgramSeason ps on ps.Programguid = av.ProgramGuid
			and av.ShipWeekMondayDate between ps.StartDate and ps.EndDate
		Join GrowerProductProgramSeasonPrice gp on av.ProductGuid = gp.ProductGuid
			and gp.GrowerGuid = @GrowerGuid and gp.ProgramSeasonGuid = ps.Guid
	
		WHERE
			av.ShipWeekGuid IN (SELECT ShipWeekGuid from @LocalTempShipWeeks) --AND
			--(case	when 
			--			@SellerCode = 'EPS' 
			--			and ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid then 1
			--		when 
			--			@SellerCode = 'DMO' 
						AND gp.ProductGuid = av.ProductGuid 
						AND gp.DateDeactivated IS NULL 
				--		then 1
				--	else 0
			--	end) = 1 AND
			--(ISNULL(@ProductGuid, av.ProductGuid) = av.ProductGuid) 
			AND
			(@ProgramTypeCode IS NULL OR av.ProgramTypeCode=@ProgramTypeCode) AND
			(@ProductFormCategoryCode IS NULL OR av.ProductFormCategoryCode=@ProductFormCategoryCode) AND
			(@ProductFormCode IS NULL OR av.ProductFormCode=@ProductFormCode) AND
			(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(av.SupplierCode) + ',%') AND
			(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(av.GeneticOwnerCode) + ',%') AND
			(@SpeciesCodeList IS NULL OR @SpeciesCodeList LIKE '%,' + RTRIM(av.SpeciesCode) + ',%') AND
			(@VarietyMatchOptional = 1 OR @VarietyCodeList IS NULL OR @VarietyCodeList LIKE '%,' + RTRIM(av.VarietyCode) + ',%') AND
			(@OrganicOnly = 0 OR av.IsOrganic = 1 )  
			--AND
			--(av.IsExclusive = 0 OR gp.ProductGuid is not null)
		GROUP BY
			av.ShipWeekGuid,
			av.ProgramTypeGuid,
			av.ProductFormCategoryGuid,
			av.ProductFormGuid,
			av.SpeciesGuid,
			av.SupplierGuid,
			av.VarietyGuid,
			av.VarietyCode,
			av.ProgramGuid,
			av.ProductGuid,
			av.ReportedAvailabilityGuid
	END
	
	
	
	
	If @bDebug = 1  print 'end LocalTempAvailability' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	DECLARE @LocalTempPreCartOrders TABLE
	(
		OrderLineGuid UNIQUEIDENTIFIER
	)
	If @bDebug = 1  print 'start LocalTempPreCartOrders' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	INSERT INTO @LocalTempPreCartOrders
	(
		OrderLineGuid
	)
	SELECT
		OrderLineView.OrderLineGuid
	FROM OrderLineView
	WHERE
		GrowerGuid=@GrowerGuid AND
		ProductGuid IN (SELECT DISTINCT(ProductGuid) FROM #LocalTempAvailability) AND
		ShipWeekCode = @ShipWeekCode AND
		OrderLineStatusLookupCode = 'PreCart'

	If @bDebug = 1  print 'end LocalTempPreCartOrders' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	DECLARE @LocalTempCartOrders TABLE
	(
		OrderLineGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempCartOrders
	(
		OrderLineGuid
	)
	SELECT
		OrderLineView.OrderLineGuid
	FROM OrderLineView
	WHERE
		GrowerGuid=@GrowerGuid AND
		ProductGuid IN (SELECT DISTINCT(ProductGuid) FROM #LocalTempAvailability) AND
		ShipWeekCode = @ShipWeekCode AND
		OrderLineStatusLookupCode = 'Pending'

	If @bDebug = 1  print 'start all queries' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	SELECT Lookup.*
	FROM Lookup
	WHERE Path LIKE 'Code/AvailabilityType/%'

	SELECT ShipWeek.*
	FROM ShipWeek
	WHERE ShipWeek.Guid IN
	(SELECT DISTINCT( ShipWeekGuid) FROM #LocalTempAvailability)

	SELECT ProgramType.*
	FROM ProgramType
	WHERE ProgramType.Guid IN
	(SELECT DISTINCT(ProgramTypeGuid) FROM #LocalTempAvailability)

	SELECT ProductFormCategory.*
	FROM ProductFormCategory
	WHERE ProductFormCategory.Guid IN
	(SELECT DISTINCT(ProductFormCategoryGuid) FROM #LocalTempAvailability)

	SELECT ProductForm.*
	FROM ProductForm
	WHERE ProductForm.Guid IN
	(SELECT DISTINCT(ProductFormGuid) FROM #LocalTempAvailability)

	SELECT Species.*
	FROM Species
	WHERE Species.Guid IN
	(SELECT DISTINCT(SpeciesGuid) FROM #LocalTempAvailability)

	SELECT Supplier.*
	FROM Supplier
	WHERE Supplier.Guid IN
	(SELECT DISTINCT(SupplierGuid) FROM #LocalTempAvailability)

	SELECT Variety.*
	FROM Variety
	WHERE Variety.Guid IN
	(SELECT DISTINCT(VarietyGuid) FROM #LocalTempAvailability)

	SELECT Program.*
	FROM Program
	WHERE Program.Guid IN
	(SELECT DISTINCT(ProgramGuid) FROM #LocalTempAvailability)

	SELECT Product.*
	FROM Product
	WHERE Product.Guid IN
	(SELECT DISTINCT(ProductGuid) FROM #LocalTempAvailability)

	SELECT ReportedAvailability.*
	FROM ReportedAvailability
	WHERE ReportedAvailability.Guid IN
	(SELECT DISTINCT(ReportedAvailabilityGuid) FROM #LocalTempAvailability)

	SELECT OrderLine.*,
	olv.ShipWeekCode
	FROM OrderLine
	Join OrderLineView olv on OrderLine.Guid = olv.OrderLineGuid
	WHERE OrderLine.Guid IN
	(SELECT DISTINCT(OrderLineGuid) FROM @LocalTempPreCartOrders)

	SELECT OrderLine.*,
	olv.ShipWeekCode
	FROM OrderLine
	Join OrderLineView olv on OrderLine.Guid = olv.OrderLineGuid
	WHERE OrderLine.Guid IN
	(SELECT DISTINCT(OrderLineGuid) FROM @LocalTempCartOrders)

	If @bDebug = 1  print 'end all queries' + + (CONVERT( VARCHAR(24), GETDATE(), 121))

	DECLARE @ShipWeekMondayDate AS DATETIME

	SELECT @ShipWeekMondayDate=ShipWeekMondayDate
	FROM ShipWeekView
	WHERE ShipWeekCode=@ShipWeekCode

	If @SellerCode = 'EPS' 
	BEGIN
		

		DECLARE @PriceQuerySetup1StartTime AS DATETIME
		SET @PriceQuerySetup1StartTime = CURRENT_TIMESTAMP

		DECLARE @GrowerHasVolumes AS BIT
		SELECT @GrowerHasVolumes = COUNT(*) FROM GrowerVolumeView where GrowerGuid = @GrowerGuid

		DECLARE @LocalTempProductGrowerSeasonPriceView TABLE
		(
			ProductGuid UNIQUEIDENTIFIER,
			GrowerGuid UNIQUEIDENTIFIER,
			ProgramSeasonGuid UNIQUEIDENTIFIER,
			SeasonStartDate DATETIME,
			SeasonEndDate DATETIME,
			PriceGuid UNIQUEIDENTIFIER 
		)

		DECLARE @LocalTempProductGrowerSeasonPriceViewCount AS INTEGER
		SET @LocalTempProductGrowerSeasonPriceViewCount = 0

		IF @GrowerHasVolumes = 1
			BEGIN

				If @bDebug = 1 print 'Before 1st INSERT INTO @LocalTempProductGrowerSeasonPriceView --' + CONVERT( VARCHAR(24), GETDATE(), 121)
			

				INSERT INTO @LocalTempProductGrowerSeasonPriceView
				(
					ProductGuid,
					GrowerGuid,
					ProgramSeasonGuid,
					SeasonStartDate,
					SeasonEndDate,
					PriceGuid
				)		
				SELECT
					ProductGrowerSeasonVolumeLevelPriceViewX.ProductGuid,
					ProductGrowerSeasonVolumeLevelPriceViewX.GrowerGuid,
					ProductGrowerSeasonVolumeLevelPriceViewX.ProgramSeasonGuid,
					ProductGrowerSeasonVolumeLevelPriceViewX.SeasonStartDate,
					ProductGrowerSeasonVolumeLevelPriceViewX.SeasonEndDate,
					ProductGrowerSeasonVolumeLevelPriceViewX.PriceGuid
				FROM ProductGrowerSeasonVolumeLevelPriceViewX
				WHERE
					ProductGrowerSeasonVolumeLevelPriceViewX.GrowerGuid = @GrowerGuid 
					 AND ProductGrowerSeasonVolumeLevelPriceViewX.ProductGuid IN
						(SELECT DISTINCT(ProductGuid) FROM #LocalTempAvailability) 
	--					(SELECT ProductGuid FROM #Scotttemp) 
					AND @ShipWeekMondayDate>=ProductGrowerSeasonVolumeLevelPriceViewX.SeasonStartDate 
					AND @ShipWeekMondayDate<=ProductGrowerSeasonVolumeLevelPriceViewX.SeasonEndDate
	
			--	If @bDebug = 1 print 'Drop temp table --' + (CONVERT( VARCHAR(24), GETDATE(), 121))
		
		

				SET @LocalTempProductGrowerSeasonPriceViewCount = @@ROWCOUNT
			
				If @bDebug = 1 print 'After 1st INSERT INTO @LocalTempProductGrowerSeasonPriceView --' + (CONVERT( VARCHAR(24), GETDATE(), 121))
		
			END


		DECLARE @LocalTempProductGrowerSeasonPriceViewCount2 AS INTEGER

		DECLARE @PriceQuerySetup1Milliseconds AS INTEGER
		SET @PriceQuerySetup1Milliseconds = DATEDIFF(MILLISECOND, @PriceQuerySetup1StartTime, CURRENT_TIMESTAMP);

		DECLARE @PriceQuerySetup2StartTime AS DATETIME
		SET @PriceQuerySetup2StartTime = CURRENT_TIMESTAMP
	
		If @bDebug = 1 print 'Before 2nd INSERT INTO @LocalTempProductGrowerSeasonPriceView --' + (CONVERT( VARCHAR(24), GETDATE(), 121))
	


		INSERT INTO @LocalTempProductGrowerSeasonPriceView
		(
			ProductGuid,
			GrowerGuid,
			ProgramSeasonGuid,
			SeasonStartDate,
			SeasonEndDate,
			PriceGuid
		)
		SELECT
			ProductSeasonBasePriceView.ProductGuid,
			@GrowerGuid,
			ProductSeasonBasePriceView.ProgramSeasonGuid,
			ProductSeasonBasePriceView.SeasonStartDate,
			ProductSeasonBasePriceView.SeasonEndDate,
			ProductSeasonBasePriceView.PriceGuid
		FROM dbo.ProductSeasonBasePriceViewX AS ProductSeasonBasePriceView WITH (NOEXPAND)
		WHERE
			ProductSeasonBasePriceView.ProductGuid IN
				(SELECT DISTINCT(ProductGuid) FROM #LocalTempAvailability) AND
			ProductSeasonBasePriceView.ProductGuid NOT IN
				(SELECT DISTINCT(ProductGuid) FROM @LocalTempProductGrowerSeasonPriceView) AND
			@ShipWeekMondayDate>=ProductSeasonBasePriceView.SeasonStartDate AND
			@ShipWeekMondayDate<=ProductSeasonBasePriceView.SeasonEndDate

		SET @LocalTempProductGrowerSeasonPriceViewCount2 = @@ROWCOUNT
	
		If @bDebug = 1 print 'After 2nd INSERT INTO @LocalTempProductGrowerSeasonPriceView --' + (CONVERT( VARCHAR(24), GETDATE(), 121))

		--DECLARE @PriceQuerySetup2Milliseconds AS INTEGER
		--SET @PriceQuerySetup2Milliseconds = DATEDIFF(MILLISECOND, @PriceQuerySetup2StartTime, CURRENT_TIMESTAMP);

		--DECLARE @ProductGrowerSeasonPriceViewQueryStartTime AS DATETIME
		--SET @ProductGrowerSeasonPriceViewQueryStartTime = CURRENT_TIMESTAMP

		If @bDebug = 1 print 'before LocalTempProductGrowerSeasonPriceView --' + (CONVERT( VARCHAR(24), GETDATE(), 121))

		SELECT *
		,cast(0 as decimal(8,4)) as DummenPrice FROM @LocalTempProductGrowerSeasonPriceView

		--DECLARE @ProductGrowerSeasonPriceViewQueryMilliseconds AS INTEGER
		--SET @ProductGrowerSeasonPriceViewQueryMilliseconds = DATEDIFF(MILLISECOND, @ProductGrowerSeasonPriceViewQueryStartTime, CURRENT_TIMESTAMP);

		--DECLARE @PriceQueryStartTime AS DATETIME
		--SET @PriceQueryStartTime = CURRENT_TIMESTAMP
	
		SELECT Price.*
		FROM Price
		WHERE Guid IN
		(
			SELECT DISTINCT(PriceGuid)
			FROM @LocalTempProductGrowerSeasonPriceView
		)

		--DECLARE @PriceQueryMilliseconds AS INTEGER
		--SET @PriceQueryMilliseconds = DATEDIFF(MILLISECOND, @PriceQueryStartTime, CURRENT_TIMESTAMP);
	END --END of EPS Pricing
	ELSE
	BEGIN --Dummen Price Method
		Select 
			av.ProductGuid,
			gp.GrowerGuid,
			gp.ProgramSeasonGuid,
			ps.StartDate as SeasonStartDate,
			ps.EndDate as SeasonEndDate,
			av.ProductGuid as PriceGuid, --This is the big fake out for Dummen
			gp.CuttingCost + gp.RoyaltyCost as DummenPrice
		From  #LocalTempAvailability av
		join shipWeek sw on av.shipweekGuid = sw.guid
		Join ProgramSeason ps on ps.Programguid = av.ProgramGuid
			and sw.MondayDate between ps.StartDate and ps.EndDate
		Join GrowerProductProgramSeasonPrice gp on av.ProductGuid = gp.ProductGuid
			and gp.GrowerGuid = @GrowerGuid and gp.ProgramSeasonGuid = ps.Guid 
		
		--This is the price fake out
		SELECT
			gp.ID as ID,
			gp.Guid as Guid,
			null as DateDeactivated,
			(select guid from Country where code = 'US') as CountryGuid,
			gp.Guid as PriceGroupGuid,
			gp.Guid as VolumeLevelGuid,
			gp.CuttingCost + gp.RoyaltyCost as RegularCost,
			gp.CuttingCost + gp.RoyaltyCost as EODCost,
			cast(0 as float) as RegularMUPercent,
			cast(0 as float) as EODMUPercent,
			gp.CuttingCost,
			gp.RoyaltyCost,
			gp.FreightCost

		From  #LocalTempAvailability av
		join shipWeek sw on av.shipweekGuid = sw.guid
		Join ProgramSeason ps on ps.Programguid = av.ProgramGuid
			and sw.MondayDate between ps.StartDate and ps.EndDate
		Join GrowerProductProgramSeasonPrice gp on av.ProductGuid = gp.ProductGuid
			and gp.GrowerGuid = @GrowerGuid and gp.ProgramSeasonGuid = ps.Guid 

		/*
		SELECT Price.*
		FROM Price
		WHERE Guid IN
		(
			SELECT DISTINCT(PriceGuid)
			FROM @LocalTempProductGrowerSeasonPriceView
		)
		*/
	END
		
	DECLARE @ShipWeekDate AS DATETIME
	SELECT @ShipWeekDate = MIN(ShipWeekMondayDate )
	FROM ShipWeekView
	WHERE ShipWeekCode = @ShipWeekCode




    SELECT ProgramSeason .*
    FROM ProgramSeason
    WHERE Guid IN
    (
        SELECT ProgramSeasonGuid FROM ProgramSeasonView
        WHERE
            ProgramGuid IN (SELECT DISTINCT( ProgramGuid) FROM #LocalTempAvailability) AND
            StartDate <= @ShipWeekDate AND EndDate >= @ShipWeekDate
    )

	--EXECUTE EventLogAdd
	--	@UserGuid=@UserGuid,
	--	@UserCode=@UserCode,
	--	@LogTypeLookupCode='Debug',
	--	@ProcessLookupCode='AvailabilityDataGet',
	--	@SeverityLookupCode='Performance',
	--	@Comment='Price Query Setup 1 Milliseconds',
	--	@IntValue=@PriceQuerySetup1Milliseconds,
	--	@FloatValue=@LocalTempProductGrowerSeasonPriceViewCount

	--EXECUTE EventLogAdd
	--	@UserGuid=@UserGuid,
	--	@UserCode=@UserCode,
	--	@LogTypeLookupCode='Debug',
	--	@ProcessLookupCode='AvailabilityDataGet',
	--	@SeverityLookupCode='Performance',
	--	@Comment='Price Query Setup 2 Milliseconds',
	--	@IntValue=@PriceQuerySetup2Milliseconds,
	--	@FloatValue=@LocalTempProductGrowerSeasonPriceViewCount2

	--EXECUTE EventLogAdd
	--	@UserGuid=@UserGuid,
	--	@UserCode=@UserCode,
	--	@LogTypeLookupCode='Debug',
	--	@ProcessLookupCode='AvailabilityDataGet',
	--	@SeverityLookupCode='Performance',
	--	@Comment='ProductGrowerSeasonPriceView Query Milliseconds',
	--	@IntValue=@ProductGrowerSeasonPriceViewQueryMilliseconds

	--EXECUTE EventLogAdd
	--	@UserGuid=@UserGuid,
	--	@UserCode=@UserCode,
	--	@LogTypeLookupCode='Debug',
	--	@ProcessLookupCode='AvailabilityDataGet',
	--	@SeverityLookupCode='Performance',
	--	@Comment='Price Query Milliseconds',
	--	@IntValue=@PriceQueryMilliseconds


	Drop Table #LocalTempAvailability
	
	If @bDebug = 1  print 'end' + + (CONVERT( VARCHAR(24), GETDATE(), 121))