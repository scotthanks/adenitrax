﻿







CREATE PROCEDURE [dbo].[SupplierOrderReprice]
	@SupplierOrderGuid AS UNIQUEIDENTIFIER
	
AS
	

Declare @OrderNo varchar(50)
Declare @SeasonCode varchar(50)
Declare @EOD AS Bit
Declare @RollingWeeksEOD as int
Declare @EODDate as DateTime
Declare @DateEntered as DateTime
Declare @ShipWeekMondayDate as DateTime
Declare @ProgramSeasonGuid AS UNIQUEIDENTIFIER
Declare @GrowerGuid AS UNIQUEIDENTIFIER
Declare @SellerCode AS nvarchar(10)
Declare @UseDummenPriceMethod AS Bit

DECLARE so_Cursor CURSOR FOR  
select distinct ps.Guid
	  From SupplierOrder so
	  Join OrderLine ol on so.guid = ol.SupplierOrderguid
	  Join Product p on ol.Productguid = p.guid
	  Join Program pr on p.ProgramGuid = pr.guid
	  Join ProgramSeason ps on ps.Programguid = pr.Guid
	  Join GrowerOrder o on o.guid = so.GrowerOrderGuid
	  join ShipWeek sw on o.ShipweekGuid = sw.guid
		And sw.MondayDate Between ps.StartDate and ps.EndDate
	  Where so.guid = @SupplierOrderGuid

OPEN so_Cursor   
FETCH NEXT FROM so_Cursor INTO @ProgramSeasonGuid   

WHILE @@FETCH_STATUS = 0   
BEGIN   
		

	select  
	   @SeasonCode = Season.Code,
	   @RollingWeeksEOD = Season.RollingWeeksEOD,
	   @EODDate = Season.EODDate,
	   @DateEntered = Season.DateEntered,
	   @ShipWeekMondayDate =Season.MondayDate,
	   @SellerCode = Season.SellerCode,
	   @UseDummenPriceMethod = Season.UseDummenPriceMethod,
	   @GrowerGuid = Season.GrowerGuid
	From
	  (select distinct ps.Code, ps.RollingWeeksEOD,ps.EODDate,o.DateEntered,sw.MondayDate
	  ,se.Code as SellerCode,g.UseDummenPriceMethod,g.Guid as GrowerGuid
		  From SupplierOrder so
		  Join OrderLine ol on so.guid = ol.SupplierOrderguid
		  Join Product p on ol.Productguid = p.guid
		  Join Program pr on p.ProgramGuid = pr.guid
		  Join Seller se on pr.SellerGuid = se.guid
		  Join ProgramSeason ps on ps.Programguid = pr.Guid
		  Join GrowerOrder o on o.guid = so.GrowerOrderGuid
		  Join Grower g on o.Growerguid = g.Guid
		  join ShipWeek sw on o.ShipweekGuid = sw.guid
			And sw.MondayDate Between ps.StartDate and ps.EndDate
		  Where so.guid = @SupplierOrderGuid
		  AND ps.Guid = @ProgramSeasonGuid
		  ) Season


	If @RollingWeeksEOD > 0 
		BEGIN
			if datediff(day,@DateEntered,@ShipWeekMondayDate)/7 >= @RollingWeeksEOD
				set @EOD = 1
			Else
				set @EOD = 0
		END
	ELSE
		BEGIN
			If @DateEntered <= dateadd(d,1,@EODDate)
				set @EOD = 1
			Else
				set @EOD = 0
		END

	--Print  @SeasonCode + '--' + cast(@EODDate as varchar(30)) + '--' + 	cast(@RollingWeeksEOD as varchar(2)) + '--' + cast(@EOD as varchar(2)) 	+ cast(@DateEntered as varchar(30))
	If @SellerCode  = 'DMO' AND @UseDummenPriceMethod = 1
	BEGIN
		UPDATE Orderline
		SET ActualCost = isnull(gp.CuttingCost,0) + isnull(gp.RoyaltyCost,0),
		ActualPrice = isnull(gp.CuttingCost,0) + isnull(gp.RoyaltyCost,0),
		ActualPriceUsedGuid = gp.Guid, --took out the fk
		ActualCostUsedGuid =  gp.Guid  --took out the fk
		FROM Orderline ol
		JOIN SupplierOrder so on ol.SupplierOrderGuid = so.Guid
		JOIN GrowerOrder o on so.GrowerOrderGuid = o.Guid
		JOIN shipWeek sw on o.shipweekGuid = sw.guid
		JOIN Product p on ol.ProductGuid = p.guid
		JOIN ProgramSeason ps on ps.Programguid = p.ProgramGuid
			AND sw.MondayDate between ps.StartDate and ps.EndDate 
		LEFT JOIN GrowerProductProgramSeasonPrice gp on ol.ProductGuid = gp.ProductGuid
			AND gp.GrowerGuid = @GrowerGuid 
			AND gp.ProgramSeasonGuid = ps.Guid 
		WHERE 
		so.Guid = @SupplierOrderGuid


	END
	
	ELSE -- Normal Reprice
	
	BEGIN
	
		Create Table #Reprice (
		OrderNo nvarchar(30),
		SupplierOrderID bigint,
		OrderLineID bigint,
		GrowerGuid uniqueidentifier,
		ProductGuid uniqueidentifier,
		SupplierDescription nvarchar(100),
		RegularCost Decimal(8,4),
		RegularPrice Decimal(8,4),
		EODCost Decimal(8,4),
		EODPrice Decimal(8,4),
		ActualPriceUsedGuid uniqueidentifier,
		CurrentCost Decimal(8,4),
		CurrentPrice Decimal(8,4),
		ProgramSeasonCode nvarchar(20),
		LevelNumber int,
		GrowerVolumeID bigint
		 )

		--Start with Base Price
		Insert into #Reprice (
		OrderNo,SupplierOrderID,OrderLineID,GrowerGuid,ProductGuid,SupplierDescription,RegularCost,RegularPrice,
		EODCost,EODPrice,ActualPriceUsedGuid,
		CurrentCost,CurrentPrice,ProgramSeasonCode,LevelNumber,GrowerVolumeID)
		select 
		o.orderno,so.ID as SupplierOrderID,ol.ID as OrderLineID,o.GrowerGuid,p.guid,p.supplierdescription,
		pr.RegularCost,pr.RegularCost + (pr.regularCost * pr.RegularMUPercent/100) as RegularPrice,
		pr.EODCost,pr.EODCost + (pr.EODCost * pr.EODMUPercent/100) as EODPrice,pr.guid,
		ol.ActualCost as CurrentCost,ol.ActualPrice as CurrentPrice,
		@SeasonCode as ProgramSeasonCode,1,0 as GrowerVolumeID

		from growerorder o
		Join supplierorder so on o.guid = so.growerorderguid
		Join orderline ol on so.guid = ol.supplierorderguid
		Join product p on ol.productguid = p.guid
		Join ProductSeasonBasePriceViewX pvx on pvx.ProductGuid = p.Guid
			AND pvx.ProgramSeasonGuid  = @ProgramSeasonGuid
		Join Price pr on pvx.PriceGuid = pr.guid		
		where so.Guid = @SupplierOrderGuid  
	

		--Now update based on grower volume
		Update #Reprice
		set  RegularCost = pr.RegularCost,
		RegularPrice =  pr.RegularCost + (pr.regularCost * pr.RegularMUPercent/100),
		EODCost = pr.EODCost,
		EODPrice =  pr.EODCost + (pr.EODCost * pr.EODMUPercent/100),
		ActualPriceUsedGuid = pr.guid,
		LevelNumber = vl.LevelNumber,
		GrowerVolumeID = gv.ID
		From #Reprice r
		JOIN ProductPriceGroupProgramSeason  ppgs ON r.ProductGuid = ppgs.ProductGuid
		JOIN ProgramSeason ps ON ppgs.ProgramSeasonGuid = ps.Guid and ps.code = r.ProgramSeasonCode
		JOIN PriceGroup pg ON ppgs.PriceGroupGuid = pg.Guid
		JOIN Price pr ON ppgs.PriceGroupGuid = pr.PriceGroupGuid 
		JOIN VolumeLevel vl ON pr.VolumeLevelGuid = vl.Guid AND
					ppgs.ProgramSeasonGuid = vl.ProgramSeasonGuid
		JOIN GrowerVolume gv on r.growerguid = gv.growerguid
			AND ps.Guid = gv.ProgramSeasonGuid
			AND coalesce(gv.ActualVolumeLevelGuid,gv.EstimatedVolumeLevelGuid,gv.QuotedVolumeLevelGuid) = vl.guid

		--Select * from #Reprice  order by orderno,SupplierOrderID,supplierdescription

		--Update the Actual Table
		Update Orderline
		Set ActualCost = Case when @EOD = 1 then EODCost else r.RegularCost end,
		ActualPrice = Case when @EOD = 1 then EODPrice else r.RegularPrice end,
		ActualPriceUsedGuid = r.ActualPriceUsedGuid,
		ActualCostUsedGuid = r.ActualPriceUsedGuid
		From #Reprice r
		Join Orderline ol on r.OrderlineID = ol.ID


		--Drop the Table
		Drop Table #Reprice
	END


	FETCH NEXT FROM so_Cursor INTO @ProgramSeasonGuid   
END   

CLOSE so_Cursor   
DEALLOCATE so_Cursor