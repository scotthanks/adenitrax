﻿
CREATE PROCEDURE [dbo].[GrowerOrderGetGrowerShipWeekOrders]
	@UserGuid AS UNIQUEIDENTIFIER,
	@ShipWeekCode as NVarchar(6)
AS
	Declare @Year int = 0
	Declare @Week int = 0 
	If @ShipWeekCode != 'ALLALL'
	BEGIN
		set @Year = left(@ShipWeekCode,4)
		set @Week = right(@ShipWeekCode,2)
	END

	SELECT
		MAX(rtrim(o.OrderNo)) as OrderNo
	,MAX(rtrim(o.CustomerPoNo)) as CustomerPoNo
	,MAX(o.Guid) as GrowerOrderGuid
	,MAX(sw.Guid) as ShipWeekGuid
	,Sum(ol.QtyOrdered) as QtyOrderedCount

	FROM 
	Person p 
	Join Grower g on p.GrowerGuid = g.Guid
	Join GrowerOrder o on g.Guid = o.GrowerGuid
	Join SupplierOrder so on o.Guid = so.GrowerOrderGuid
	join OrderLine ol on so.Guid = ol.SupplierOrderGuid
	join lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
	Join ShipWeek sw on o.ShipWeekGuid  = sw.Guid

	--Join GrowerOrderSummaryByShipWeekGroupByOrderView osv on o.Guid = osv.GrowerOrderGuid
	
	WHERE p.UserGuid = @UserGuid
	ANd ((sw.Year = @Year and sw.WEek = @Week) OR @ShipWeekCode = 'ALLALL') 
	--AND osv.LowestOrderLineStatusLookupCode not in ('PreCart','Pending','Cancelled','GrowerCancelled','SupplierCancelled','Shipped')
	

	Group By o.OrderNo,o.CustomerPoNo,o.Guid,sw.Guid
	Having MAX(ols.SortSequence) > 1
	AND MIN(ols.SortSequence) < 8
--	select * from lookup where path like '%orderlinestatus%' order by sortSequence