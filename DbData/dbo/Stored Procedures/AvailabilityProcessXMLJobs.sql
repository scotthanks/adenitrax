﻿




CREATE PROCEDURE [dbo].[AvailabilityProcessXMLJobs]

AS

Declare @MaxID bigint
	set @MaxID = (select Max(ID) - 200 from epsb2bProd.dbo.AvailabilityIntegrationXML)
DECLARE @UploadID INT
DECLARE @DataProcessing CURSOR
	SET @DataProcessing = CURSOR FOR
		SELECT ID
		FROM epsb2bProd.dbo.AvailabilityIntegrationXML
		where uploadStatus = 'Data Received'
		and  ID > @MaxID
		ORDER BY ID
	OPEN @DataProcessing
	
	FETCH NEXT
	FROM @DataProcessing INTO @UploadID
	
	WHILE @@FETCH_STATUS = 0
	
	BEGIN
		--print 'Starting job: ' + cast(@UploadID as varchar(10))
		exec AvailabilityProcessXMLJob @UploadID
	
		FETCH NEXT
		FROM @DataProcessing INTO @UploadID
	END

CLOSE @DataProcessing
DEALLOCATE @DataProcessing

--now handle DORO and DORB for Dummen
Declare @maxDoroID bigint
select  @maxDoroID = max(ID) from epsb2bProd.dbo.AvailabilityIntegrationXML
where SupplierCodeExternal = 'DORO'
Declare @maxDorbID bigint
select  @maxDorbID = max(ID) from epsb2bProd.dbo.AvailabilityIntegrationXML
where SupplierCodeExternal = 'DORB'

Update  epsb2bProd.dbo.AvailabilityIntegrationXML set PairedID = @maxDoroID where ID = @maxDorbID
--print 'Paired DGU: ' + cast(@maxDorbID as varchar(10))
exec AvailabilityProcessXMLJob @maxDorbID


--now handle DECK and DMNS
Declare @maxDeckID bigint
select  @maxDeckID = max(ID) from epsb2bProd.dbo.AvailabilityIntegrationXML
where SupplierCodeExternal = 'DECK'
Declare @maxDmnsID bigint
select  @maxDmnsID = max(ID) from epsb2bProd.dbo.AvailabilityIntegrationXML
where SupplierCodeExternal = 'DMNS'

Update  epsb2bProd.dbo.AvailabilityIntegrationXML set PairedID = @maxDeckID where ID = @maxDmnsID
--print 'Paired DES: ' + cast(@maxDmnsID as varchar(10))
exec AvailabilityProcessXMLJob @maxDmnsID

delete from [epsDataProd].[dbo].[AvailabilityIntegrationDetail] where [AvailabilityIntegrationID] < @MaxID
delete from [epsb2bProd].[dbo].[AvailabilityIntegrationXML] where [ID] < @MaxID