﻿
CREATE PROCEDURE [dbo].[GrowerAddressAdd]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@Name AS NVARCHAR(50),
	@StreetAddress1 AS NVARCHAR(50),
	@StreetAddress2 AS NVARCHAR(50),
	@City AS NVARCHAR(50),
	@StateCode AS NCHAR(2),
	@ZipCode AS NCHAR(12),
	@PhoneAreaCode AS DECIMAL(3),
	@Phone AS DECIMAL(7),
	@SpecialInstructions AS VARCHAR(200),
	@IsDefault AS BIT = 0,
	@AddressGuid AS UNIQUEIDENTIFIER OUT,
	@GrowerAddressGuid AS UNIQUEIDENTIFIER OUT
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('Name',@Name,1) +
		dbo.XmlSegment('StreetAddress1',@StreetAddress1,1) +
		dbo.XmlSegment('StreetAddress2',@StreetAddress2,1) +
		dbo.XmlSegment('City',@City,1) +
		dbo.XmlSegment('StateCode',@StateCode,1) +
		dbo.XmlSegment('ZipCode',@ZipCode,1) +
		dbo.XmlSegment('PhoneAreaCode',@PhoneAreaCode,1) +
		dbo.XmlSegment('Phone',@Phone,1) +
		dbo.XmlSegment('SpecialInstructions',@SpecialInstructions,1) +
		dbo.XmlSegment('IsDefault',@IsDefault,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerAddressAdd',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	EXECUTE AddressAdd
		@UserGuid=@UserGuid,
		@Name=@Name,
		@StreetAddress1=@StreetAddress1,
		@StreetAddress2=@StreetAddress2,
		@City=@City,
		@StateCode=@StateCode,
		@ZipCode=@ZipCode,
		@AddressGuid=@AddressGuid OUT

	SET @AddressGuid = @AddressGuid

	IF @AddressGuid IS NULL
		EXECUTE EventLogAdd
			@Comment='Address was not added.',
			@LogTypeLookupCode='RowAddError',
			@ProcessLookupCode='GrowerAddressAdd',
			@UserGuid=@UserGuid,
			@GuidValue=@OriginalEventLogGuid

	SET @GrowerAddressGuid = NEWID()

	EXECUTE GrowerAddressAddExisting
		@userGuid=@UserGuid,
		@GrowerGuid=@GrowerGuid,
		@AddressGuid=@AddressGuid,
		@PhoneAreaCode=@PhoneAreaCode,
		@Phone=@Phone,
		@SpecialInstructions=@SpecialInstructions,
		@IsDefault=@IsDefault,
		@GrowerAddressGuid=@GrowerAddressGuid OUT

	SET @GrowerAddressGuid = @GrowerAddressGuid

	IF @GrowerAddressGuid IS NULL
		EXECUTE EventLogAdd
			@Comment='Grower Address was not added.',
			@LogTypeLookupCode='RowAddError',
			@ProcessLookupCode='GrowerAddressAdd',
			@UserGuid=@UserGuid,
			@GuidValue=@OriginalEventLogGuid

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('AddressGuid',@AddressGuid,1) + 
		dbo.XmlSegment('GrowerAddressGuid',@GrowerAddressGuid,1),
		0
	)

	SET @AddressGuid = @AddressGuid
	SET @GrowerAddressGuid = @GrowerAddressGuid

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerAddressAdd',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@XmlData=@ReturnValues