﻿









CREATE PROCEDURE [dbo].[OrderLineAdminUpdate]
	@OrderLineGuid AS UNIQUEIDENTIFIER,
	@QtyOrdered AS INT,
	@QtyShipped AS INT,
	@ActualPrice AS DECIMAL(8,4),
	@ActualCost AS DECIMAL(8,4),
	@OrderLineStatusCode AS NVARCHAR(50)
	

AS
	DECLARE @OrderLineStatusLookupGuid as UNIQUEIDENTIFIER
	DECLARE @GrowerOrderGuid UniqueIdentifier
	DECLARE @ProductGuid UniqueIdentifier
	DECLARE @ShipWeekGuid UniqueIdentifier
	DECLARE @OriginalQty int
	Declare @LogGuid uniqueidentifier
	Declare @UserGuid uniqueidentifier
	Declare @PersonGuid uniqueidentifier
	Declare @theDate DateTime
	Declare @sComment nvarchar(2000)
	Declare @VarietyName nvarchar(200)

	SELECT 
	@PersonGuid = guid
	,@UserGuid = userguid
	FROM Person 
	WHERE 
	firstname  = 'Customer' 
	AND lastname = 'Service'

	
	SELECT --GrowerOrderGuid,* FROM OrderLineViewShowInactiveProduct WHERE OrderLineGuid = '89ed0964-e34c-4b8c-9f63-9e42f673af96'
	@GrowerOrderGuid = GrowerOrderGuid
	,@OriginalQty = QtyOrdered
	,@ProductGuid = ProductGuid
	,@ShipWeekGuid = ShipWeekGuid
	,@VarietyName = VarietyName
	FROM
		dbo.OrderLineViewShowInactiveProduct
	WHERE 
	OrderLineGuid = @OrderLineGuid

	IF @QtyOrdered != @OriginalQty
	BEGIN
		--add modification comment
		SET @sComment = 'Order for ' + CAST(@OriginalQty AS NVARCHAR(10)) + ' ' + @VarietyName + ' changed to ' + CAST(@QtyOrdered AS VARCHAR(10))
		SET @LogGuid = (SELECT guid FROM lookup WHERE path = 'Logging/LogType/Grower/OrderLineChange')
		SET @theDate = (SELECT GETDATE())
		EXEC GrowerOrderHistoryAdd 
			@UserGuid,NULL,
			@GrowerOrderGuid,
			@sComment,0,@PersonGuid,@theDate,@LogGuid
	
		--update availability
		UPDATE ReportedAvailability
		SET SalesSinceDateReported = SalesSinceDateReported + (@QtyOrdered - @OriginalQty)
		WHERE productGuid = @ProductGuid
		AND ShipWeekGuid = @ShipWeekGuid
	END
	

	--select * from lookup where path like 'Code/OrderLineStatus/%'
	--this should be eliminated and code should match path
	IF @OrderLineStatusCode = 'GrowerNotifiedAfterSupplierConfirmed'
	BEGIN
		SET @OrderLineStatusCode = 'CustomerNotifiedAfterSupplierConfirmed'
	END

	SET @OrderLineStatusLookupGuid = (SELECT Guid FROM Lookup WHERE path  = 'Code/OrderLineStatus/' + @OrderLineStatusCode)

	--Update line
	UPDATE OrderLine
	SET
		QtyOrdered=@QtyOrdered
		,QtyShipped=@QtyShipped
		,ActualPrice=@ActualPrice
		,ActualCost=@ActualCost
		,OrderLineStatusLookupGuid=@OrderLineStatusLookupGuid
		
	WHERE Guid=@OrderLineGuid

	
	IF @QtyOrdered != @OriginalQty
	BEGIN
		EXEC GrowerOrderRecalcFees @UserGuid, @GrowerOrderGuid, 1
	END