﻿














CREATE PROCEDURE [dbo].[ReportDummenZeroPrice]
	

AS
SELECT 
	g.Name AS GrowerName,su.Name AS SellerName,o.OrderNo,sw.Year,sw.WEek,
	ol.LineNumber,ols.Name AS Status,p.SupplierIdentifier,p.SupplierDescription,
	ol.QtyOrdered,ol.ActualPrice,ol.ActualCost
FROM OrderLine ol 
	JOIN Product p ON ol.ProductGuid = p.guid
	JOIN Lookup ols ON ol.OrderLineStatusLookupGuid = ols.guid
	JOIN SupplierOrder so ON so.Guid = ol.SupplierOrderguid
	JOIN Supplier su ON so.SupplierGuid = su.guid
	JOIN growerOrder o ON o.guid = so.GrowerOrderguid
	JOIN shipweek sw ON o.shipweekguid = sw.Guid
	JOIN Grower g ON g.Guid = o.GrowerGuid
	JOIN GrowerSeller gs ON g.Guid = gs.GrowerGuid
	JOIN Seller s ON gs.SellerGuid = s.Guid
WHERE s.Guid = (SELECT guid FROM seller WHERE code = 'DMO')
	AND ol.ActualPrice = 0
	AND QtyOrdered > 0
	--and ols.Name != 'Cancelled'
	AND g.name NOT IN ('DummenTest')
ORDER BY sw.Year,sw.WEek,g.Name,o.OrderNo,ol.LineNumber,ol.ActualPrice,ol.ActualCost