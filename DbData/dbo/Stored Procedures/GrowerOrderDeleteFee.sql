﻿

CREATE PROCEDURE [dbo].[GrowerOrderDeleteFee]
	@GrowerOrderFeeGuid AS UNIQUEIDENTIFIER

AS

	Delete GrowerOrderFee
    Where Guid = @GrowerOrderFeeGuid