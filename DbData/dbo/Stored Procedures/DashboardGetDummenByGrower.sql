﻿








CREATE PROCEDURE [dbo].[DashboardGetDummenByGrower]
	 @FiscalYear AS NVARCHAR(50)
	
	 

AS

DECLARE @Year INT
SET @Year = 0
IF @FiscalYear != 'All'
	SET @Year = (SELECT CAST(LEFT(@FiscalYear,4) AS int))

SELECT	g.Name AS Grower,
		COUNT(DISTINCT o.ID) AS Orders,
		COUNT(ol.ID) AS OrderLines,
		ISNULL(SUM(ActualPrice * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS Price,
		ISNULL(SUM(gpp.CuttingCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS CuttingCost,
		ISNULL(SUM(gpp.RoyaltyCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS RoyaltyCost,
		ISNULL(SUM(gpp.FreightCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) AS FreightCost,
		ISNULL(SUM(gpp.CuttingCost * CASE WHEN QtyShipped=0 THEN QtyOrdered ELSE QtyShipped END),0) * .02 AS Profit
FROM Grower g 
JOIN GrowerOrder o ON g.Guid = o.GrowerGuid
JOIN SupplierOrder so ON o.Guid = so.GrowerOrderGuid
JOIN OrderLine ol ON so.Guid = ol.SupplierOrderGuid 
JOIN Product p ON ol.ProductGuid = p.Guid
JOIN Program pr ON p.ProgramGuid = pr.Guid
JOIN ShipWeek sw ON o.shipweekGuid = sw.Guid
JOIN ProgramSeason ps ON ps.Programguid = pr.Guid
	AND sw.MondayDate BETWEEN ps.StartDate AND ps.EndDate 
JOIN Seller se ON pr.sellerGuid = se.Guid
JOIN Lookup ols ON ol.OrderLineStatusLookupGuid = ols.Guid
JOIN Lookup gs ON o.GrowerOrderStatusLookupGuid = gs.Guid

LEFT JOIN GrowerProductProgramSeasonPrice gpp ON ol.ProductGuid = gpp.ProductGuid
			AND gpp.GrowerGuid = o.GrowerGuid 
			AND gpp.ProgramSeasonGuid = ps.Guid 

WHERE 0=0
AND se.Code = 'DMO'
AND CASE WHEN @FiscalYear = 'All' THEN 1  
	WHEN  @Year = sw.Year AND sw.week >= 14 THEN 1
	WHEN  @Year + 1 = sw.Year AND sw.week <= 13 THEN 1
	ELSE 0 END  = 1
AND ols.Code NOT IN('Cancelled','PreCart','Pending','Write Off')
AND o.growerguid NOT IN 
	(SELECT subjectguid FROM Triple WHERE predicatelookupguid = 
		(SELECT guid FROM lookup WHERE path = 'Predicate/TestGrower')
	) 
GROUP BY
g.Name
ORDER BY 4 DESC
--select * from lookup where path like '%orderlinestatus%' order by sortsequence