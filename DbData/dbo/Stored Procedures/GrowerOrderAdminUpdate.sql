﻿





CREATE PROCEDURE [dbo].[GrowerOrderAdminUpdate]
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@CustomerPoNo AS NVARCHAR(100),
	@OrderDescription AS NVARCHAR(50),
	@PromotionalCode AS NVARCHAR(20),
	@ShipWeekGuid AS UNIQUEIDENTIFIER,
	@ShipToAddressGuid AS UNIQUEIDENTIFIER,
	@PaymentTypeLookupGuid AS UNIQUEIDENTIFIER,
	@CreditCardGuid AS UNIQUEIDENTIFIER = NULL

AS

	Declare @UserGuid uniqueidentifier
	Declare @CurrentPromotionalCode NVARCHAR(20)

	SELECT 
		@CurrentPromotionalCode = PromotionalCode
	
	FROM
		GrowerOrder
	WHERE 
		Guid = @GrowerOrderGuid

	SELECT 
		@UserGuid = userguid
	FROM Person 
	WHERE 
	firstname  = 'Customer' 
	AND lastname = 'Service'



	if @CreditCardGuid = '00000000-0000-0000-0000-000000000000'
		set @CreditCardGuid = null


	UPDATE GrowerOrder
	SET
		CustomerPoNo=@CustomerPoNo
		,[Description]=@OrderDescription
		,PromotionalCode=@PromotionalCode
		,ShipWeekGuid=@ShipWeekGuid
		,ShipToAddressGuid=@ShipToAddressGuid
		,PaymentTypeLookupGuid=@PaymentTypeLookupGuid
		,GrowerCreditCardGuid=@CreditCardGuid
	WHERE Guid=@GrowerOrderGuid

	

	If @CurrentPromotionalCode != @PromotionalCode
	BEGIN
		exec GrowerOrderRecalcFees @UserGuid, @GrowerOrderGuid, 0
	End