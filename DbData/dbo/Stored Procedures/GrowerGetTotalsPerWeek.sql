﻿
CREATE PROCEDURE [dbo].[GrowerGetTotalsPerWeek]
	@UserGuid AS UNIQUEIDENTIFIER,
	@ProductFormCategoryCode AS NVARCHAR(30),
	@ProgramTypeCode AS NVARCHAR(30)
AS
	
	SELECT
	 sw.ShipWeekYear as Year
	,sw.ShipWeekWeek as Week
	,sw.ShipWeekCode
	,Sum(Case when ols.Code = 'Pending' then ol.QtyOrdered else 0 end) as Carted
	,Sum(Case when ols.Code = 'PreCart' then ol.QtyOrdered else 0 end) as Uncarted
                   
	FROM Person pe
	JOIN Grower g on pe.GrowerGuid = g.Guid
	JOIN GrowerOrder o on g.Guid = o.GrowerGuid
	JOIN ShipWeekView sw on o.ShipWeekGuid  = sw.ShipWeekGuid
	JOIN SupplierOrder so on so.GrowerOrderGuid = o.Guid
	JOIN Orderline ol on ol.SupplierOrderGuid = so.guid
	JOIN Product p on ol.ProductGuid = p.guid
	JOIN Program pr on p.ProgramGuid = pr.guid
	JOIN ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.Guid
	JOIN ProgramType pt on pr.ProgramTypeGuid = pt.Guid
	JOIN Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
	WHERE pe.UserGuid = @UserGuid
		AND ols.Code in ('PreCart','Pending')   
		AND pt.Code = @ProgramTypeCode
		AND pfc.Code = @ProductFormCategoryCode                                    
	
	GROUP BY 
	sw.ShipWeekYear
	,sw.ShipWeekWeek
	,sw.ShipWeekCode
	,sw.ShipWeekContinuousWeekNumber
	Order by sw.ShipWeekContinuousWeekNumber