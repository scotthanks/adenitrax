﻿












CREATE PROCEDURE [dbo].[B2BSupplierOrderListGet]
	 @OrderNo as nvarchar(10) ='',
	 @GrowerNameLike as nvarchar(50) = '',
	 @FiscalYear as nvarchar(10)= 'All',
	 @WeekNumber as int = 0,
	 @OrderStatusCode as nvarchar(50) = 'All',
	 @SellerCode nvarchar(50)
	 

AS
	;With  zz(GrowerOrderGuid,B2BCount)
	AS
	(
	SELECT  o.Guid as GrowerOrderGuid,count(b2b.ID) as B2BCount
	FROM GrowerOrder o
	left JOIN [epsB2BProd].[dbo].[B2BIntegrationXML] b2b on b2b.OrderGuid = o.Guid 
	--and  b2b.CommunicationDirection = 'ToSeller'
	
	Group by o.Guid
	)

	Select top 1000
	o.Guid as OrderGuid
	,g.Code as GrowerCode
	,g.Name as GrowerName
	,o.CustomerPoNo
	,rtrim(ov.GrowerOrderStatusLookupCode) as GrowerOrderStatusCode
	,case when rtrim(ov.GrowerOrderStatusLookupCode) in ('Invoiced','Paid') 
		then ov.GrowerOrderStatusLookupName 
		else LowestOrderLineStatusLookupName end as GrowerOrderStatusName
	,ov.InvoiceNo
	,case when rtrim(ov.GrowerOrderStatusLookupCode) in ('Invoiced','Paid') 
		then ov.GrowerOrderStatusLookupCode else ov.LowestOrderLineStatusLookupCode 
		end as OrderStatus
	,so.ID as SupplierOrderID
	,rtrim(s.Code) as SupplierCode
	,s.Name as SupplierName
	,rtrim(se.Code) as SellerCode
	,ov.LowestOrderLineStatusLookupCode as LowestOrderLineStatusCode
	,o.[Description] as OrderDescription
	,o.OrderNo as OrderNo
	,ov.QtyOrderedCount as OrderQty
	,ov.QtyOrderedCount as OrderQty
	,p.FirstName + ' ' + p.LastName as PersonWhoPlacedOrder
	,pfc.Name as ProductFormCategoryName
	,pt.Name as ProgramTypeName
	,substring(ov.ShipWeekCode,5,2) + '|' + substring(ov.ShipWeekCode,1,4) as ShipWeekString
	,zz.B2BCount
	from GrowerOrderSummaryByShipWeekGroupByOrderView2 ov
	Join GrowerOrder o on ov.GrowerOrderGuid = o.Guid
	Join SupplierOrder so on so.GrowerOrderGuid = o.Guid
	Join Supplier s on s.Guid = so.SupplierGuid
	Join Seller se on s.SellerGuid = se.Guid
	Join Grower g on ov.GrowerGuid = g.Guid
	Join Person p on ov.PersonGuid = p.Guid
	Join ProductFormCategory pfc on ov.ProductFormCategoryGuid = pfc.Guid
	Join ProgramType pt on ov.ProgramTypeGuid = pt.Guid
	Join ShipWeek sw on ov.ShipWeekGuid = sw.Guid
	Join zz on o.Guid = zz.GrowerOrderGuid

	WHERE 
		1 = 1
		AND s.IsB2BOrdering = 1
		AND ov.LowestOrderLineStatusLookupCode not in ('PreCart','Pending') 
		AND (o.OrderNo = @OrderNo OR @OrderNo = '')
		AND g.Name like @GrowerNameLike + '%'
		AND (sw.FiscalYear = @FiscalYear or @FiscalYear = 'All')
		AND (sw.[Week] = @WeekNumber or @WeekNumber = 0)
		AND (rtrim(ov.LowestOrderLineStatusLookupCode) = @OrderStatusCode or @OrderStatusCode = 'All')
		AND (
			se.Code = @SellerCode 
			OR @SellerCode = 'All')
	
	
		
       
	ORDER BY o.OrderNo desc