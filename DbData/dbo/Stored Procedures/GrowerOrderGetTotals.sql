﻿







CREATE PROCEDURE [dbo].[GrowerOrderGetTotals]
	@GrowerOrderGuid AS UNIQUEIDENTIFIER
AS
	

	SELECT
	rtrim(ols.Code) as Status,Sum(ol.QtyOrdered) as QtyOrdered
	FROM GrowerOrder o
	JOIN SupplierOrder so on so.GrowerOrderGuid = o.Guid
	JOIN Orderline ol on ol.SupplierOrderGuid = so.guid
	JOIN Lookup ols on ol.OrderLineStatusLookupGuid = ols.Guid
	WHERE GrowerOrderGuid =@GrowerOrderGuid

	GROUP BY ols.Code