﻿CREATE PROCEDURE [dbo].[ProgramTypeGetGuid]
	@UserCode AS NVARCHAR(56) = NULL,
	@ProgramTypeCode AS NVARCHAR(50),
	@ProcessLookupCode AS NVARCHAR(50),
	@ProgramTypeGuid AS UNIQUEIDENTIFIER OUT
AS
	SET @ProgramTypeCode = LTRIM(RTRIM(@ProgramTypeCode))

	SET @ProgramTypeGuid = NULL

	IF @ProgramTypeCode IS NOT NULL
		BEGIN
			SELECT @ProgramTypeGuid = ProgramTypeGuid 
			FROM ProgramTypeView 
			WHERE ProgramTypeCode = @ProgramTypeCode

			IF @ProgramTypeGuid IS NULL
				EXECUTE EventLogKeyNotFound
					@UserCode=@UserCode,
					@KeyName='ProgramTypeCode',
					@Key=@ProgramTypeCode,
					@ObjectTypeLookupCode='ProgramType',
					@ProcessLookupCode=@ProcessLookupCode
		END