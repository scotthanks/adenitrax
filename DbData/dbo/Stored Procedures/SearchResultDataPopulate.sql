﻿








CREATE procedure [dbo].[SearchResultDataPopulate]
	@SearchTermGuid AS UNIQUEIDENTIFIER,
	@GetOnlyPage1 AS bit,
	@iDebug as bit
AS

--Relavence categories
--1 = Vareity by exact match -- done
--2 = Series by Exact Match -- done
--3 = Species by Exact Match -- done
--4 = Supplier by Exact Match
--5 Suppliers by Supplier Name -- done
--6 = Series by Series Name -- done
--7 = Varieties by Series Name
--8 = Varieties by Variety Name -- done
--9 = Varieties by Stripped Name -- done
--10 = Species by Species Name -- done
--15 = Add Series to Species Name -- done
--18 = Genetic Owner by Name -- done
--20 = WebPages by HTML -- done


	Declare @SearchTerm nvarchar(200)
	Declare @CurrentCount bigint
	Declare @PageSize bigint

	set @SearchTerm = (select SearchString from SearchTerm where guid = @SearchTermGuid)
	set @PageSize = 10
	If len(@SearchTerm) >2
	BEGIN
		Delete from SearchTermSearchResult where SearchTermGuid = @SearchTermGuid

--------Find Varieties by Exact Match on Variety Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,1
		From Variety v
		Join SearchResult sr on sr.ResultType = 'Variety'
			AND sr.ResultCode = v.Code
		WHERE  
		v.DateDeactivated is null
		AND v.Name = @SearchTerm
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
	
	
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN
		
--------Find Series by Exact Match on Series Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,2
		From Series s
		Join SearchResult sr on sr.ResultType = 'Series'
			AND sr.ResultCode = s.Code
		WHERE 
		s.DateDeactivated is null 
		AND s.Name =  @SearchTerm 
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN

--------Find Species by Exact Match on Species Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,3
		From Species sp
		Join SearchResult sr on sr.ResultType = 'Species'
			AND sr.ResultCode = sp.Code
		WHERE  sp.Name =  @SearchTerm 
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN
		
--------Find Supplier by Exact Match on Supplier Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,4
		From Supplier s
		Join SearchResult sr on sr.ResultType = 'Supplier'
			AND sr.ResultCode = s.Code
		WHERE s.Name = @SearchTerm 
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN


--------Find Supplier by Supplier Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,5
		From Supplier s
		Join SearchResult sr on sr.ResultType = 'Supplier'
			AND sr.ResultCode = s.Code
		WHERE s.Name like '%' + @SearchTerm + '%'
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
--------Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN

		--Find Series by Series Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,6
		From Series s
		Join SearchResult sr on sr.ResultType = 'Series'
			AND sr.ResultCode = s.Code
		WHERE 
		s.DateDeactivated is null 
		and s.Name like '%' + @SearchTerm + '%'
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN
			
--------Find Varieties by Series Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,7
		From Variety v
		Join series se on v.seriesguid = se.guid
		Join SearchResult sr on sr.ResultType = 'Variety'
			AND sr.ResultCode = v.Code
		WHERE 
		v.DateDeactivated is null
		AND se.Name like '%' + @SearchTerm + '%'
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)

		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN

--------Find Varieties by Variety Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,8
		From Variety v
		Join SearchResult sr on sr.ResultType = 'Variety'
			AND sr.ResultCode = v.Code
		WHERE  v.DateDeactivated is null
		AND v.Name like '%' + @SearchTerm + '%'
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
	
	
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN
		
-------Find Varieties by stripped Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,9
		From Variety v
		Join SearchResult sr on sr.ResultType = 'Variety'
			AND sr.ResultCode = v.Code
		WHERE  v.DateDeactivated is null
		AND v.NameStripped = + replace(replace(replace(replace(replace(@SearchTerm,' ',''),'™',''),'®',''),'''',''),'-','')	
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
	
	
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN
	
--------Find Varieties by Species Name


		
--------Find Varieties by Product Supplier Description
		
--------Find Varieties by HTML desc

--------Find Species by Species Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,10
		From Species sp
		Join SearchResult sr on sr.ResultType = 'Species'
			AND sr.ResultCode = sp.Code
		WHERE  sp.Name like '%' + @SearchTerm + '%'
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN

--------Add Series to Species Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,15
		From Series s
		Join Species sp on s.speciesguid = sp.guid
		Join SearchResult sr on sr.ResultType = 'Series'
			AND sr.ResultCode = s.Code
		WHERE 
		s.DateDeactivated is null
		AND sp.Name like '%' + @SearchTerm + '%' 
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN

		
--------Find Species by Variety Name
		
				
--------Find Varieties by GO Name
		
--------something about color
		
-------Find Genetic Owner by Variety Name
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,18
		From GeneticOwner o
		Join SearchResult sr on sr.ResultType = 'GeneticOwner'
			AND sr.ResultCode = o.Code
		WHERE  o.Name like '%' + @SearchTerm + '%'
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
	
	
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN

--------WebPages by HTML
		INSERT INTO SearchTermSearchResult
		SELECT newid(),NULL,@SearchTermGuid,sr.Guid,0,20
		From epsContentDev..PageContent pc
		Join SearchResult sr on sr.ResultType = 'WebPage'
			AND sr.ResultCode = pc.Description
		WHERE  pc.HTML like '%' + @SearchTerm + '%'
		and not exists (select 1 from SearchTermSearchResult stsr 
						where stsr.SearchTermGuid = @SearchTermGuid
						and stsr.SearchResultGuid = sr.Guid)
		
		--Check and exit if got page of results
		set @CurrentCount = (select count(*) from SearchTermSearchResult WHERE SearchTermGuid = @SearchTermGuid)
		if @CurrentCount >= @PageSize AND @GetOnlyPage1 = 1 
			RETURN





--------FINALLY
		 
		 Update SearchTerm set ResultCount = (
			Select count(*) FROM SearchTermSearchResult
			WHERE SearchTermGuid = @SearchTermGuid)
		 Where guid = @SearchTermGuid

		 Update SearchTermSearchResult set SortOrder = thesortorder
		 From SearchTermSearchResult JOIN
		 (select row_number() OVER (Order by stsr.RelevanceCategory,sr.ResultName) as thesortorder,
			stsr.guid,s.SearchString,stsr.RelevanceCategory,sr.ResultName
			from searchterm s
			 join SearchTermSearchResult stsr on s.Guid = stsr.searchtermguid
			 Join SearchResult sr on stsr.SearchResultGuid = sr.Guid

			 Where s.Guid =@SearchTermGuid ) theTable on SearchTermSearchResult.Guid = theTable.GUID

		




	END