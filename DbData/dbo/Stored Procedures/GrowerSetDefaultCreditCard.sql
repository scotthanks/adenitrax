﻿






CREATE PROCEDURE [dbo].[GrowerSetDefaultCreditCard]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@GrowerGuid AS UNIQUEIDENTIFIER,
	@GrowerCreditCardGuid AS UNIQUEIDENTIFIER
AS
	
	Update GrowerCreditCard set DefaultCard = 0 where GrowerGuid = @GrowerGuid
	Update GrowerCreditCard set DefaultCard = 1 where Guid = @GrowerCreditCardGuid