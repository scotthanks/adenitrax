﻿




CREATE PROCEDURE [dbo].[GrowerRepriceOrders]
	@GrowerName as nvarchar(50),
	@ProgramSeasonGuid as uniqueidentifier,
	@StartYear int,
	@StartWeek int,
	@EndYear int,
	@EndWeek int 
AS

Declare @SupplierOrderGuid uniqueidentifier


DECLARE rp_Cursor CURSOR FOR  
	select distinct so.Guid 
	from OrderLine ol
	Join Product p on ol.ProductGuid = p.Guid
	Join ProgramSeason ps on p.ProgramGuid = ps.ProgramGuid 
		and ps.Guid = @ProgramSeasonGuid
	Join SupplierOrder so on ol.SupplierOrderGuid = so.Guid
	join growerOrder o on o.guid = so.GrowerOrderguid
	join shipweek sw on o.shipweekguid = sw.Guid
	join Grower g on g.Guid = o.GrowerGuid
	Join GrowerSeller gs on g.Guid = gs.GrowerGuid
	join Seller s on gs.SellerGuid = s.Guid
	where 
	s.Guid = (select guid from seller where code = 'DMO')
	AND (
			(sw.Year = @StartYear and sw.week >= @StartWeek)
			OR
			(sw.Year > @StartYear and sw.Year < @EndYear)
			OR
			(sw.Year = @EndYear and sw.week <= @EndWeek )
		)
	and g.Name = @GrowerName
OPEN rp_Cursor   
FETCH NEXT FROM rp_Cursor INTO @SupplierOrderGuid   

WHILE @@FETCH_STATUS = 0   
BEGIN   
	
		Exec SupplierOrderReprice @SupplierOrderGuid
		Print @SupplierOrderGuid   
		FETCH NEXT FROM rp_Cursor INTO @SupplierOrderGuid 
		
END   

CLOSE rp_Cursor   
DEALLOCATE rp_Cursor