﻿








CREATE PROCEDURE [dbo].[GrowerOrderGetGuid]
	@UserGuid AS UNIQUEIDENTIFIER,
	@ShipWeekCode AS varchar(10),
	@ProgramTypeCode AS varchar(10),
	@ProductFormCategoryCode AS varchar(10),
	@Placed as bit = 0,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER OUT
AS
	
	

	DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
	DECLARE @ShipWeekYear AS int
	DECLARE @ShipWeekWeek AS int
	
	SET @ShipWeekYear = (select cast(left(@ShipWeekCode,4) as int))
	SET @ShipWeekWeek = (select cast(right(@ShipWeekCode,2) as int))
	
	SET @GrowerGuid = (SELECT GrowerGuid 
						FROM Person 
						WHERE UserGuid=@UserGuid
						)
	
	If @Placed = 1
		SET @GrowerOrderGuid = (SELECT top 1 o.Guid FROM 
							GrowerOrder o
							Join ShipWeek sw on o.shipweekguid = sw.guid
							Join GrowerOrderLowestOrderLineStatusView lsv on o.Guid = lsv.GrowerOrderGuid
							Join Lookup lsvc on lsvc.Guid = lsv.OrderLineStatusLookupGuid
						--	Join ProgramType pt on o.ProgramTypeGuid = pt.guid
						--	Join ProductFormCategory pfc on o.ProductFormCategoryGuid = pfc.guid
							WHERE o.GrowerGuid=@GrowerGuid
							AND sw.Year = @ShipWeekYear and sw.week = @ShipWeekWeek
							and lsvc.SortSequence > 1
						--	AND pt.Code = @ProgramTypeCode
						--	AND pfc.Code = @ProductFormCategoryCode
							Order by o.DateEntered desc
							)
	Else
		SET @GrowerOrderGuid = (SELECT top 1 o.Guid FROM 
							GrowerOrder o
							Join ShipWeek sw on o.shipweekguid = sw.guid
			
							
						--	Join ProgramType pt on o.ProgramTypeGuid = pt.guid
						--	Join ProductFormCategory pfc on o.ProductFormCategoryGuid = pfc.guid
							WHERE o.GrowerGuid=@GrowerGuid
							AND sw.Year = @ShipWeekYear and sw.week = @ShipWeekWeek
							
						--	AND pt.Code = @ProgramTypeCode
						--	AND pfc.Code = @ProductFormCategoryCode
							Order by o.DateEntered desc
							)
	If @GrowerOrderGuid is null
		set @GrowerOrderGuid = '00000000-0000-0000-0000-000000000000'