﻿







CREATE PROCEDURE [dbo].[SpeciesDataGet]
	@UserGuid AS UniqueIdentifier,
	@SellerCode AS NCHAR(10) = null,
	@ProgramTypeCode AS NCHAR(50) = null,
	@ProductFormCategoryCode AS NCHAR(50) = null,
	@GeneticOwnerCodeList AS NVARCHAR(500) = null,
	@SupplierCodeList AS NVARCHAR(500) = null
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('GeneticOwnerCodeList',@GeneticOwnerCodeList,1) +
		dbo.XmlSegment('SupplierCodeList',@SupplierCodeList,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER	
	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='SpeciesDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	Declare @GrowerGuid as UniqueIdentifier


	select @GrowerGuid = p.GrowerGuid
	from Person p
	Join Grower g on p.GrowerGuid = g.Guid
	 where UserGuid = @UserGuid
	 


	IF @ProgramTypeCode Like '%*%'
		SET @ProgramTypeCode = NULL

	IF LEN(RTRIM(@ProgramTypeCode)) > 3
		BEGIN
			SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SELECT @ProgramTypeCode = Code FROM ProgramType WHERE Name LIKE '%' + RTRIM(@ProgramTypeCode) + '%'

			IF @ProgramTypeCode IS NULL
				SET @ProgramTypeCode = '*'
		END

	PRINT '@ProgramTypeCode=' + @ProgramTypeCode

	IF @ProductFormCategoryCode Like '%*%'
		SET @ProductFormCategoryCode = NULL

	IF LEN(RTRIM(@ProductFormCategoryCode)) > 3
		BEGIN
			SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SELECT @ProductFormCategoryCode = Code FROM ProductFormCategory WHERE Name LIKE '%' + RTRIM(@ProductFormCategoryCode) + '%'

			IF @ProductFormCategoryCode IS NULL
				SET @ProductFormCategoryCode = '*'
		END

	PRINT '@ProductFormCategoryCode=' + @ProductFormCategoryCode

	IF @GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%*%'
		SET @GeneticOwnerCodeList = NULL
	ELSE
		SET @GeneticOwnerCodeList = ',' + @GeneticOwnerCodeList + ','

	PRINT '@GeneticOwnerCodeList=' + @GeneticOwnerCodeList

	IF @SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%*%'
		SET @SupplierCodeList = NULL
	ELSE
		SET @SupplierCodeList = ',' + @SupplierCodeList + ','

	PRINT '@SupplierCodeList=' + @SupplierCodeList

	DECLARE @LocalTempSpeciesData TABLE
	(
		SpeciesGuid UNIQUEIDENTIFIER
	)

	If @SellerCode = 'EPS' OR @UserGuid is null Or @UserGuid = '00000000-0000-0000-0000-000000000000'
	BEGIN
		
		INSERT INTO @LocalTempSpeciesData
		SELECT
			ProductView.SpeciesGuid
		FROM ProductView
		
		WHERE
			(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) AND
			(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
			(@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(GeneticOwnerCode) + ',%') AND
			(@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(SupplierCode) + ',%') 
		GROUP BY
			ProductView.SpeciesGuid
		
	END
	ELSE
	BEGIN
		--all Dummen use the dumen quote method now
		INSERT INTO @LocalTempSpeciesData
		SELECT
			ProductView.SpeciesGuid
		FROM ProductView
		Join GrowerProductProgramSeasonPrice gp on ProductView.ProductGuid = gp.ProductGuid
			AND gp.GrowerGuid = @GrowerGuid
		WHERE
			(@ProgramTypeCode IS NULL OR ProgramTypeCode=@ProgramTypeCode) 
			AND (@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) 
			AND (@GeneticOwnerCodeList IS NULL OR @GeneticOwnerCodeList LIKE '%,' + RTRIM(GeneticOwnerCode) + ',%') 
			AND (@SupplierCodeList IS NULL OR @SupplierCodeList LIKE '%,' + RTRIM(SupplierCode) + ',%') 
			AND gp.DateDeactivated IS NULL
		GROUP BY
			ProductView.SpeciesGuid
		
	
	END
	

	PRINT CAST(@@ROWCOUNT AS NVARCHAR(10)) + ' Species found.'

	SELECT Species.*
	FROM Species
	WHERE Species.Guid IN
	(SELECT DISTINCT(SpeciesGuid) FROM @LocalTempSpeciesData)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='SpeciesDataGet',
		@ObjectGuid=@OriginalEventLogGuid