﻿


CREATE PROCEDURE [dbo].[AvailabilityUploadSetAllDataNA]
	 @JobNumber as bigint,
	 @SupplierCode as nvarchar(10),
	 @StartYear as int,
	 @EndYear as int,
	 @StartWeek as int,
	 @EndWeek as int
	 
	
	
	 
AS
--This is repeatd on the first week but needed to clear out the NA
--This updates the IDs where they are blank
UPDATE ReportedAvailabilityUpdate
SET SupplierIdentifier =  
	(SELECT p.SupplierIdentifier 
		FROM Product P 
		JOIN Program pr ON P.ProgramGuid = PR.Guid
WHERE p.SupplierDescription = RAU.SupplierDescription 
	AND pr.SupplierGUID = (SELECT guid 
							FROM Supplier 
							WHERE code = RAU.SupplierCode)
							) 
FROM ReportedAvailabilityUpdate RAU
WHERE RunNumber = @JobNumber  
	AND [Week] = @StartWeek 
	AND RAU.SupplierIdentifier = ''
           
--This updates all guids based on ID
UPDATE ReportedAvailabilityUpdate
SET SupplierGuid =(SELECT guid 
					FROM Supplier 
					WHERE code = RAU.SupplierCode),
    ProductGuid = (SELECT p.Guid 
					FROM Product P 
					JOIN Program pr ON P.ProgramGuid = PR.Guid
					WHERE p.SupplierIdentifier = RAU.SupplierIdentifier 
						AND pr.SupplierGUID = (SELECT guid 
												FROM Supplier 
												WHERE code = RAU.SupplierCode)
												),
    ShipWeekGuid = (SELECT sw.Guid 
					FROM ShipWeek sw 
					WHERE sw.year = RAU.YEAR 
					AND sw.Week = RAU.Week),
    AvailabilityTypeLookupGuid = (SELECT lu.Guid 
									FROM Lookup lu 
									WHERE path = 'Code/AvailabilityType/' + RAU.AvailabiltiyType
									)
FROM ReportedAvailabilityUpdate RAU
WHERE RunNumber = @JobNumber  AND [Week] = @StartWeek




		
if @StartYear = @EndYear
BEGIN
	UPDATE ReportedAvailability
	SET availabilitytypelookupguid = 
			(select guid 
			 from lookup 
			 where path = 'Code/AvailabilityType/NA'),
		 QTY=0,
		 SalesSinceDateReported=0,
		 DateReported=getdate()
	FROM ReportedAvailability RA 
	JOIN Product p on RA.ProductGuid = p.guid 
	JOIN Program pr on p.ProgramGuid = pr.guid 
	JOIN Supplier s on pr.supplierguid = s.guid 
	JOIN ShipWeek sw on RA.shipweekguid = sw.guid 
	WHERE s.code = @SupplierCode
		AND sw.Year = @StartYear
		AND sw.Week BETWEEN  @StartWeek AND @EndWeek
		AND pr.Guid in (
			SELECT DISTINCT pr1.Guid 
			FROM Program pr1 
			JOIN Product p1 on pr1.Guid = p1.ProgramGuid 
			JOIN  ReportedAvailabilityUpdate rau on p1.Guid = rau.ProductGuid
            WHERE rau.RunNumber = @JobNumber
        )            
END
ELSE
BEGIN
	UPDATE ReportedAvailability
	SET availabilitytypelookupguid = 
		(select guid from lookup where path = 'Code/AvailabilityType/NA'),
		QTY = 0,
		SalesSinceDateReported = 0,
		DateReported = getdate()
	from ReportedAvailability RA 
	JOIN Product p on RA.ProductGuid = p.guid
	JOIN Program pr on p.ProgramGuid = pr.guid
	JOIN Supplier s on pr.supplierguid = s.guid
	JOIN ShipWeek sw on RA.shipweekguid = sw.guid
	WHERE s.code = @SupplierCode
		AND sw.Year = @StartYear
		AND sw.Week >=  @StartWeek
		AND pr.Guid in (
			SELECT DISTINCT pr1.Guid 
			FROM Program pr1
			JOIN Product p1 on pr1.Guid = p1.ProgramGuid 
			JOIN ReportedAvailabilityUpdate rau on p1.Guid = rau.ProductGuid
			WHERE rau.RunNumber = @JobNumber
        )
                

	UPDATE ReportedAvailability
	SET availabilitytypelookupguid = (select guid from lookup where path = 'Code/AvailabilityType/NA'),QTY=0,SalesSinceDateReported=0,DateReported=getdate()
	from ReportedAvailability RA 
	JOIN Product p on RA.ProductGuid = p.guid 
	JOIN Program pr on p.ProgramGuid = pr.guid 
	JOIN Supplier s on pr.supplierguid = s.guid 
	JOIN ShipWeek sw on RA.shipweekguid = sw.guid 
	WHERE s.code = @SupplierCode
		AND sw.Year = @EndYear
		AND sw.Week <=  @EndWeek
		AND pr.Guid in (
			SELECT DISTINCT pr1.Guid 
			FROM Program pr1 
			JOIN Product p1 on pr1.Guid = p1.ProgramGuid 
			JOIN ReportedAvailabilityUpdate rau on p1.Guid = rau.ProductGuid
			WHERE rau.RunNumber = @JobNumber
          )
        
END