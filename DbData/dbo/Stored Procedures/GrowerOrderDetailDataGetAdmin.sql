﻿




--EXAMPLE:
--EXECUTE GrowerOrderDetailDataGet @GrowerOrderGuid='A9061AE7-0DA5-4563-ADEB-B8FC90B0C073'

CREATE PROCEDURE [dbo].[GrowerOrderDetailDataGetAdmin]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NVARCHAR(56) = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@IncludePriceData AS BIT = 0
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) + 
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('IncludePriceData',@IncludePriceData,1),
		0
	)

	DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
	SELECT @GrowerGuid=GrowerGuid
	FROM GrowerOrderView
	WHERE GrowerOrderGuid=@GrowerOrderGuid
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerOrderDetailDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @LocalTempData TABLE
	(
		GrowerOrderGuid UNIQUEIDENTIFIER,
		GrowerGuid UNIQUEIDENTIFIER,
		ShipWeekGUid UNIQUEIDENTIFIER,
		SupplierOrderGuid UNIQUEIDENTIFIER,
		SupplierGuid UNIQUEIDENTIFIER,
		OrderLineGuid UNIQUEIDENTIFIER,
		ActualPriceUsedGuid UNIQUEIDENTIFIER,
		ProductGuid UNIQUEIDENTIFIER,
		ProductFormCategoryGuid UNIQUEIDENTIFIER,
		ProductFormGuid UNIQUEIDENTIFIER,
		VarietyGuid UNIQUEIDENTIFIER,
		ProgramGuid UNIQUEIDENTIFIER,
		OrderTypeLookupGuid UNIQUEIDENTIFIER,
		SupplierOrderStatusLookupGuid UNIQUEIDENTIFIER,
		OrderLineStatusLookupGuid UNIQUEIDENTIFIER,
		PersonGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempData
	SELECT
		so.GrowerOrderGuid,
		o.GrowerGuid,
		o.ShipWeekGuid,
		ol.SupplierOrderGuid,
		so.SupplierGuid,
		ol.Guid as OrderLineGuid,
		ol.ActualPriceUsedGuid,
		ol.ProductGuid,
		pf.ProductFormCategoryGuid,
		p.ProductFormGuid,
		p.VarietyGuid,
		p.ProgramGuid,
		o.OrderTypeLookupGuid,
		so.SupplierOrderStatusLookupGuid,
		ol.OrderLineStatusLookupGuid,
		o.PersonGuid
	FROM OrderLine ol
		INNER JOIN SupplierOrder so
			ON ol.SupplierOrderGuid = so.Guid
		INNER JOIN GrowerOrder o
			ON so.GrowerOrderGuid = o.Guid
		INNER JOIN Product p
			ON ol.ProductGuid = p.Guid
		INNER JOIN ProductForm pf
			ON p.ProductFormGuid = pf.Guid
		INNER JOIN Lookup AS ols
			ON ol.OrderLineStatusLookupGuid = ols.guid
	WHERE
		GrowerOrderGuid = @GrowerOrderGuid
	GROUP BY
		so.GrowerOrderGuid,
		o.GrowerGuid,
		o.ShipWeekGUid,
		ol.SupplierOrderGuid,
		so.SupplierGuid,
		ol.Guid,
		ol.ActualPriceUsedGuid,
		ol.ProductGuid,
		pf.ProductFormCategoryGuid,
		p.ProductFormGuid,
		p.VarietyGuid,
		p.ProgramGuid,
		o.OrderTypeLookupGuid,
		so.SupplierOrderStatusLookupGuid,
		ol.OrderLineStatusLookupGuid,
		o.PersonGuid

	DECLARE @LocalTempProductGrowerSeasonPriceView TABLE
	(
		ProductGuid UNIQUEIDENTIFIER,
		GrowerGuid UNIQUEIDENTIFIER,
		ProgramSeasonGuid UNIQUEIDENTIFIER,
		SeasonStartDate DATETIME,
		SeasonEndDate DATETIME,
		PriceGuid UNIQUEIDENTIFIER
	)

	DECLARE @ShipWeekDate AS DATETIME
	SELECT @ShipWeekDate = MIN(ShipWeekMondayDate)
	FROM ShipWeekView
	WHERE ShipWeekGuid IN (SELECT DISTINCT(ShipWeekGuid) FROM @LocalTempData)

	IF @IncludePriceData = 1
		BEGIN
			INSERT INTO @LocalTempProductGrowerSeasonPriceView
				SELECT
					ProductGrowerSeasonPriceView.ProductGuid,
					ProductGrowerSeasonPriceView.GrowerGuid,
					ProductGrowerSeasonPriceView.ProgramSeasonGuid,
					ProductGrowerSeasonPriceView.SeasonStartDate,
					ProductGrowerSeasonPriceView.SeasonEndDate,
					ProductGrowerSeasonPriceView.PriceGuid
				FROM dbo.ProductGrowerSeasonPriceView
				WHERE
					ProductGrowerSeasonPriceView.ProductGuid IN (SELECT DISTINCT(ProductGuid) FROM @LocalTempData) AND
					ProductGrowerSeasonPriceView.GrowerGuid IN (SELECT DISTINCT(GrowerGuid) FROM @LocalTempData) AND
					ProductGrowerSeasonPriceView.SeasonStartDate <= @ShipWeekDate AND ProductGrowerSeasonPriceView.SeasonEndDate >= @ShipWeekDate
		END

	DECLARE @LocalGrowerAddressData TABLE
	(
		GrowerAddressGuid UNIQUEIDENTIFIER,
		AddressGuid UNIQUEIDENTIFIER,
		StateGuid UNIQUEIDENTIFIER,
		CountryGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalGrowerAddressData
	SELECT
		GrowerAddressGuid,
		AddressGuid,
		StateGuid,
		CountryGuid
	FROM GrowerAddressView
	WHERE
		GrowerGuid IN
		(
			SELECT DISTINCT(GrowerGuid)
			FROM @LocalTempData
		)
	
	;WITH zz (GrowerOrderGuid,NavigationProgramTypeCode,NavigationProductFormCategoryCode)
	AS
		(Select top 1 
		olv.GrowerOrderGuid
		,olv.ProgramTypeCode as NavigationProgramTypeCode
		,olv.ProductFormCategoryCode as NavigationProductFormCategoryCode
		FROM GrowerOrder
		Join OrderLineViewshowinactiveproduct olv on GrowerOrder.Guid = olv.GrowerOrderGuid
		WHERE GrowerOrder.Guid IN
		(SELECT DISTINCT(GrowerOrderGuid) FROM @LocalTempData)
		Group by olv.GrowerOrderGuid,olv.ProgramTypeCode,olv.ProductFormCategoryCode
		Order by count(*) desc
		)
	
	SELECT GrowerOrder.ID
	,GrowerOrder.Guid
	,GrowerOrder.DateDeactivated
	,GrowerOrder.GrowerGuid
	,GrowerOrder.ShipWeekGuid
	,GrowerOrder.OrderNo
	,GrowerOrder.Description
	,GrowerOrder.CustomerPONO
	,GrowerOrder.OrderTypeLookupGuid
	,GrowerOrder.ProductFormCategoryGuid
	,GrowerOrder.ProgramTypeguid
	,GrowerOrder.ShipToAddressGuid
	,GrowerOrder.PaymentTypeLookupGuid
	,GrowerOrder.GrowerOrderStatusLookupGuid
	,GrowerOrder.DateEntered
	,GrowerOrder.GrowerCreditCardGuid
	,GrowerOrder.PersonGuid
	,GrowerOrder.PromotionalCode
	,GrowerOrder.GrowerShipMethodCode
	,GrowerOrder.SellerGuid
	,Coalesce(zz.NavigationProgramTypeCode,'VA') as NavigationProgramTypeCode
	,Coalesce(zz.NavigationProductFormCategoryCode,'RC') as NavigationProductFormCategoryCode
	,gos.Code AS GrowerOrderStatusCode
	,gos.Name AS GrowerOrderStatusName
	,gos.Code AS OrderTypeCode
	,pfc.Code AS ProductFormCategoryCode
	,pfc.Name AS ProductFormCategoryName
	,pt.Code AS ProgramTypeCode
	,pt.Name AS ProgramTypeName

	FROM GrowerOrder
	JOIN Lookup gos ON gos.Guid = GrowerOrder.GrowerOrderStatusLookupGuid
	JOIN Lookup got ON got.Guid = GrowerOrder.OrderTypeLookupGuid
	JOIN ProductFormCategory pfc ON pfc.Guid = GrowerOrder.ProductFormCategoryGuid
	JOIN ProgramType pt ON pt.Guid = GrowerOrder.ProgramTypeGuid
	Join zz  on GrowerOrder.Guid = zz.GrowerOrderGuid
	
	
	SELECT Grower.*
	FROM Grower
	WHERE Grower.Guid IN
	(SELECT DISTINCT(GrowerGuid) FROM @LocalTempData)

	SELECT ShipWeek.*
	FROM ShipWeek
	WHERE ShipWeek.Guid IN
	(SELECT DISTINCT(ShipWeekGuid) FROM @LocalTempData)

	SELECT SupplierOrder.*
	FROM SupplierOrder
	WHERE SupplierOrder.Guid IN
	(SELECT DISTINCT(SupplierOrderGuid) FROM @LocalTempData)

	SELECT Supplier.*
	FROM Supplier
	WHERE Supplier.Guid IN
	(SELECT DISTINCT(SupplierGuid) FROM @LocalTempData)

	SELECT OrderLine.*,olv.ShipWeekCode
	,olv.OrderLineStatusLookupCode
	,olv.OrderLineStatusLookupName
	,olv.ProgramGuid
	,CAST(olv.OrderLineStatusLookupSortSequence AS DECIMAL(10,1)) AS OrderLineStatusLookupSortSequence
	,p.ID AS ProductID
	,p.Code AS ProductCode
	,p.SupplierDescription
	,p.SupplierIdentifier
	,pf.Name AS ProductFormName
	,pf.LineItemMinimumQty
	,pf.SalesUnitQty
	,v.Code AS VarietyCode
	,v.Name AS VarietyName
	,sp.Code AS SpeciesCode
	,sp.name AS SpeciesName
	
	
	FROM OrderLine
	join OrderLineViewShowInactiveProduct olv on OrderLine.Guid = olv.OrderLineGuid
	join Product p  on OrderLine.ProductGuid = p.Guid
	join Variety v  on p.VarietyGuid = v.Guid
	join Species sp  on v.SpeciesGuid = sp.Guid
	join ProductForm pf  on p.ProductFormGuid = pf.Guid
	WHERE OrderLine.Guid IN
	(SELECT DISTINCT(OrderLineGuid) FROM @LocalTempData)
	ORDER BY 
	OrderLine.SupplierOrderGuid
	,sp.name
	,v.Name


	--SELECT Product.*
	--FROM Product
	--WHERE Product.Guid IN
	--(SELECT DISTINCT(ProductGuid) FROM @LocalTempData)

	--SELECT ProductFormCategory.*
	--FROM ProductFormCategory
	--WHERE ProductFormCategory.Guid IN
	--(SELECT DISTINCT(ProductFormCategoryGuid) FROM @LocalTempData)

	--SELECT ProductForm.*
	--FROM ProductForm
	--WHERE ProductForm.Guid IN
	--(SELECT DISTINCT(ProductFormGuid) FROM @LocalTempData)

	--SELECT Variety.*
	--FROM Variety
	--WHERE Variety.Guid IN
	--(SELECT DISTINCT(VarietyGuid) FROM @LocalTempData)

	--SELECT Program.*
	--FROM Program
	--WHERE Program.Guid IN
	--(SELECT DISTINCT(ProgramGuid) FROM @LocalTempData)

	SELECT ga.Guid AS GrowerAddressGuid,a.*,[State].Code AS StateCode
	FROM GrowerAddress ga
	JOIN Address a ON ga.AddressGuid = a.Guid
	JOIN [State] ON a.StateGuid = [State].Guid
	WHERE ga.Guid IN
	(SELECT DISTINCT(GrowerAddressGuid) FROM @LocalGrowerAddressData)

	--SELECT [Address].*,[State].Code AS StateCode
	--FROM [Address]
	--JOIN [State] ON [Address].StateGuid = [State].Guid
	--WHERE [Address].Guid IN
	--(SELECT DISTINCT(AddressGuid) FROM @LocalGrowerAddressData)

	--SELECT State.*
	--FROM State
	--WHERE Guid IN
	--(SELECT DISTINCT(StateGuid) FROM @LocalGrowerAddressData)

	--SELECT Country.*
	--FROM Country
	--WHERE Guid IN
	--(SELECT DISTINCT(CountryGuid) FROM @LocalGrowerAddressData)

	SELECT GrowerCreditCard.*
	FROM GrowerCreditCard
	WHERE Guid IN
	(
		SELECT GrowerCreditCardGuid 
		FROM GrowerCreditCardView
		WHERE GrowerGuid IN
		(SELECT DISTINCT(GrowerGuid) FROM @LocalTempData)
	)

	--SELECT *
	--,cast(0 as decimal(8,4)) as DummenPrice
	--FROM @LocalTempProductGrowerSeasonPriceView

	--SELECT Price.*
	--FROM Price
	--WHERE
	--	Price.Guid IN
	--		(SELECT DISTINCT(ActualPriceUsedGuid) FROM @LocalTempData) OR 
	--	Price.Guid IN
	--		(SELECT DISTINCT(PriceGuid) FROM @LocalTempProductGrowerSeasonPriceView)

	--EXECUTE EventLogAdd
	--	@UserGuid=@UserGuid,
	--	@UserCode=@UserCode,
	--	@LogTypeLookupCode='Debug',
	--	@ProcessLookupCode='GrowerOrderDetailDataGet',
	--	@IntValue=5,
	--	@AddedEventLogGuid=@OriginalEventLogGuid OUT

	SELECT ProgramTagRatioView.*,trl.Code AS TagRatioCode,trl.Name AS TagRatioName
	FROM ProgramTagRatioView
	JOIN Lookup trl ON ProgramTagRatioView.TagRatioLookupGuid = trl.Guid
	WHERE ProgramTagRatioView.ProgramGuid IN
	(SELECT DISTINCT(ProgramGuid) FROM @LocalTempData)

	SELECT ProgramShipMethodView.*,sm.Code AS ShipMethodCode,sm.Name AS ShipMethodName
	FROM ProgramShipMethodView
	JOIN Lookup sm ON ProgramShipMethodView.ShipMethodLookupGuid = sm.Guid
	WHERE ProgramShipMethodView.ProgramGuid IN
	(SELECT DISTINCT(ProgramGuid) FROM @LocalTempData)

	SELECT ReportedAvailability.*
	FROM ReportedAvailability
	WHERE
		ReportedAvailability.ProductGuid IN (SELECT DISTINCT(ProductGuid) FROM @LocalTempData) AND
		ReportedAvailability.ShipWeekGuid IN (SELECT DISTINCT(ShipWeekGuid) FROM @LocalTempData)

	--SELECT ProgramSeason.*
	--FROM ProgramSeason
	--WHERE Guid IN 
	--(
	--	SELECT ProgramSeasonGuid FROM ProgramSeasonView
	--	WHERE
	--		ProgramGuid IN (SELECT DISTINCT(ProgramGuid) FROM @LocalTempData) AND
	--		StartDate <= @ShipWeekDate AND EndDate >= @ShipWeekDate
	--)

	SELECT Person.*
	FROM Person
	WHERE Guid IN
	(SELECT DISTINCT(PersonGuid) FROM @LocalTempData)

	--SELECT SupplierOrderFee.*
	--FROM SupplierOrderFee
	--WHERE SupplierOrderGuid IN
	--(SELECT DISTINCT(SupplierOrderGuid) FROM @LocalTempData)
	
	--SELECT GrowerOrderFee.*
	--FROM GrowerOrderFee
	--WHERE GrowerOrderGuid IN
	--(SELECT DISTINCT(GrowerOrderGuid) FROM @LocalTempData)

	--SELECT GrowerOrderHistory.*
	--FROM GrowerOrderHistory
	--WHERE GrowerOrderGuid IN
	--(SELECT DISTINCT(GrowerOrderGuid) FROM @LocalTempData)

	
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerOrderDetailDataGet',
		@ObjectTypeLookupCode='GrowerOrder',
		@ObjectGuid=@GrowerOrderGuid