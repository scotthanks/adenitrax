﻿



CREATE PROCEDURE [dbo].[GrowerOrderAddPreCartLines]
	@UserCode AS NCHAR(56) = NULL,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@ShipWeekCode AS NCHAR(6),
	@GrowerOrderToGuid AS UNIQUEIDENTIFIER,
	@OrderLinesChanged AS INTEGER = NULL OUT
AS
	DECLARE @MaxLineNumber int
	IF @UserCode IS NOT NULL AND @UserCode != '' AND dbo.IsUniqueIdentifier(@UserCode) = 1
		BEGIN
			SET @UserGuid = dbo.ExtractUniqueIdentifier(@UserCode)

			SELECT @UserCode = UserCode
			FROM Person
			WHERE UserGuid = @UserGuid
		END

	IF @UserCode IS NULL OR @UserCode = '' AND @UserGuid IS NOT NULL
			SELECT @UserCode = UserCode
			FROM Person
			WHERE UserGuid = @UserGuid


	
	


	SET @OrderLinesChanged = -1
	
	DECLARE @ShipWeekYear AS int
	DECLARE @ShipWeekWeek AS int
	SET @ShipWeekYear = (select cast(left(@ShipWeekCode,4) as int))
	SET @ShipWeekWeek = (select cast(right(@ShipWeekCode,2) as int))
	
	DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
	DECLARE @PersonGuid AS UNIQUEIDENTIFIER
	SELECT @GrowerGuid = GrowerGuid, @PersonGuid = Guid
	FROM Person
	WHERE UserGuid = @UserGuid
	
	Declare @GrowerOrderFromGuid AS UNIQUEIDENTIFIER
	SET @GrowerOrderFromGuid = (SELECT top 1 o.Guid FROM 
							GrowerOrder o
							Join ShipWeek sw on o.shipweekguid = sw.guid
							Join GrowerOrderLowestOrderLineStatusView lsv on o.Guid = lsv.GrowerOrderGuid
							Join Lookup lsvc on lsvc.Guid = lsv.OrderLineStatusLookupGuid
							WHERE o.GrowerGuid=@GrowerGuid
							AND sw.Year = @ShipWeekYear and sw.week = @ShipWeekWeek
							and lsvc.Code ='PreCart'
							Order by o.DateEntered desc
							)
	
	--Capture the incoming items
	Select OrderLineGuid,ProductGuid
	into #IncomingOrderLines
	From OrderLineView olv
	Where olv.GrowerOrderGuid = @GrowerOrderFromGuid AND
	olv.OrderLineStatusLookupCode = 'PreCart'

	--Capture lines with incoming and match
	Select OrderLineGuid,ProductGuid
	into #IncomingDuplicteOrderLines
	From #IncomingOrderLines
	Where ProductGuid in 
	(Select ProductGuid from OrderLineView olv
		Where olv.GrowerOrderGuid = @GrowerOrderToGuid 
		)


	DECLARE @ToOrderLineStatusGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@Path='Code/OrderLineStatus',
		@LookupCode='Ordered',
		@ProcessLookupCode='OrderLineChangeStatus',
		@LookupGuid=@ToOrderLineStatusGuid OUT

	--get max Line number from destination order.
	SET @MaxLineNumber = (select isnull(Max(ol.LineNumber),0) from supplierOrder so
							JOIN OrderLine ol on so.Guid = ol.SupplierOrderGuid
							WHERE so.GrowerOrderGuid = @GrowerOrderToGuid) 
	--SELECT ROW_NUMBER() OVER (Order by Id) AS RowNumber, Field1, Field2, Field3
	--FROM User
	--Update Line numbers
	Update OrderLine
	set LineNumber = OrderLine.LineNumber + @MaxLineNumber
	FROM OrderLine
	INNER JOIN OrderLineView ON OrderLine.Guid = OrderLineView.OrderLineGuid
	WHERE
		RTRIM(OrderLineStatusLookupCode) = 'PreCart' AND
		--@ShipWeekCode = OrderLineView.ShipWeekCode AND
		@GrowerOrderFromGuid = OrderLineView.GrowerOrderGuid

	UPDATE OrderLine
		SET OrderLineStatusLookupGuid =  @ToOrderLineStatusGuid,
		DateLastChanged = Getdate()
	FROM OrderLine
	INNER JOIN OrderLineView ON OrderLine.Guid = OrderLineView.OrderLineGuid
	WHERE
		RTRIM(OrderLineStatusLookupCode) = 'PreCart' AND
		--@ShipWeekCode = OrderLineView.ShipWeekCode AND
		@GrowerOrderFromGuid = OrderLineView.GrowerOrderGuid
			

	SET @OrderLinesChanged = @@ROWCOUNT

	

	----Add Supplier Orders if needed
	Declare @SupplierGuid uniqueidentifier
	Declare @NewSupplierOrderGuid uniqueidentifier
				
	
	DECLARE @ProgramTypeCode AS NCHAR(20)
	DECLARE @ProductFormCategoryCode AS NCHAR(20)
	DECLARE so_Cursor CURSOR FOR  
	Select Distinct SupplierGuid
	From SupplierOrderView sov
	Where GrowerOrderGuid = @GrowerOrderFromGuid
	AND SupplierGuid NOT IN(
		Select DISTINCT sov2.SupplierGuid
		FROM SupplierOrderView sov2
		Where GrowerOrderGuid = @GrowerOrderToGuid
	)
	OPEN so_Cursor   
	FETCH NEXT FROM so_Cursor INTO @SupplierGuid   
	WHILE @@FETCH_STATUS = 0   
	BEGIN   
			set @ProgramTypeCode = (select top 1 rtrim(pt.Code)
			From SupplierOrder so
			join OrderLine ol on so.Guid = ol.SupplierOrderGuid
			Join Product p on ol.ProductGuid = p.Guid
			Join Program pr on p.ProgramGuid = pr.guid
			Join ProgramType pt on pr.ProgramTypeGuid = pt.guid
			where so.GrowerOrderGuid = @GrowerOrderFromGuid
			)

			set @ProductFormCategoryCode = (select top 1 rtrim(pfc.Code)
			From SupplierOrder so
			join OrderLine ol on so.Guid = ol.SupplierOrderGuid
			Join Product p on ol.ProductGuid = p.Guid
			Join Program pr on p.ProgramGuid = pr.guid
			Join ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.guid
			where so.GrowerOrderGuid = @GrowerOrderFromGuid
			)
			
			
			
			Exec SupplierOrderAdd @UserGuid,@UserCode,@GrowerOrderToGuid,@SupplierGuid,@ProgramTypeCode,@ProductFormCategoryCode,'',@NewSupplierOrderGuid out
			FETCH NEXT FROM so_Cursor INTO @SupplierGuid   
	END  
	CLOSE so_Cursor   
	DEALLOCATE so_Cursor 

	--Insert history
	Insert into GrowerOrderhistory 
	SELECT NewID(), Null,
	@GrowerOrderToGuid,
	'Order for ' + cast(olv.QtyOrdered as nvarchar(10)) + ' ' + olv.VarietyName + ' added.',
	0,@PersonGuid,GetDate(),
	(select guid from lookup where path = 'Logging/LogType/Grower/OrderLineAdd')
	FROM OrderLine ol
	JOIN OrderLineView olv ON ol.Guid = olv.OrderLineGuid
	WHERE
		olv.GrowerOrderGuid = @GrowerOrderFromGuid AND
		olv.OrderLineStatusLookupCode = 'Ordered'
		AND ol.productGuid not in
			(Select ProductGuid from 
				#IncomingDuplicteOrderLines idol 
			)
			
	
	--Move to new Order				
	UPDATE OrderLine
		SET SupplierOrderGuid =  sov.SupplierOrderGuid			
	FROM OrderLine
	JOIN OrderLineView olv  ON OrderLine.Guid = olv.OrderLineGuid
	JOIN SupplierOrderView sov
		On olv.GrowerGuid = sov.GrowerGuid 
		AND olv.ShipWeekGuid = sov.ShipWeekGuid
		AND olv.SupplierGuid = sov.SupplierGuid
	WHERE
		olv.GrowerOrderGuid = @GrowerOrderFromGuid AND
		sov.GrowerOrderGuid =  @GrowerOrderToGuid AND  
		olv.OrderLineStatusLookupCode = 'Ordered'

	--update sales since last update on Availability
	Update ReportedAvailability
	set SalesSinceDateReported = SalesSinceDateReported + olv.QtyOrdered
	From ReportedAvailability ra
	Join OrderLineView olv on ra.ProductGuid = olv.ProductGuid
		AND ra.ShipWeekGuid = olv.ShipWeekGuid
	
	Where olv.GrowerOrderGuid = @GrowerOrderToGuid AND
	olv.OrderLineStatusLookupCode = 'Ordered'

	--To Do Linked weeks
                                           
	
	

	--Add comments for products merged
	INSERT INTO GrowerOrderhistory 
	SELECT NEWID(), NULL,
	@GrowerOrderToGuid,
	'Order Lines for ' + olv.VarietyName + ' combined to a new total of ' + CAST(SUM(olv.QtyOrdered) AS NVARCHAR(10))  + '.',
	0,@PersonGuid,GETDATE(),
	(SELECT guid FROM lookup WHERE path = 'Logging/LogType/Grower/OrderLineChange')
	FROM  OrderLine ol
	JOIN OrderLineView olv ON ol.Guid = olv.OrderLineGuid
	WHERE
		olv.GrowerOrderGuid = @GrowerOrderToGuid  
		AND ol.ProductGuid in 
		(Select ProductGuid from 
		#IncomingDuplicteOrderLines idol 
		)
		
	GROUP BY olv.GrowerOrderGuid,olv.ProductGuid,olv.VarietyName
	HAVING COUNT(*) > 1

	--select lookup.* from lookup where path like '%orderlinestatus%'
	--Merge dupliacte Line Items
	UPDATE OrderLine 
	SET QtyOrdered = zz.QtyOrdered,
	DateLastChanged = GETDATE(),
	DateQtyLastChanged =GETDATE(),
	OrderLineStatusLookupGuid = (SELECT guid FROM lookup WHERE path = 'Code/OrderLineStatus/GrowerEdit')
	FROM  
	OrderLine ol 
	JOIN (
			SELECT MIN(ol2.ID) AS OrderLineID, SUM(olv.QtyOrdered) AS QtyOrdered
			FROM OrderLine ol2
			JOIN OrderLineView olv ON ol2.Guid = olv.OrderLineGuid
			WHERE 
			olv.GrowerOrderGuid = @GrowerOrderToGuid 
			AND  (olv.SellerCode != 'DMO' OR olv.QtyOrdered > 0 )  --Don't merge cancelled DMO lines.
			AND olv.ProductGuid IN (
				SELECT ProductGuid FROM #IncomingDuplicteOrderLines
				)
			GROUP BY olv.GrowerOrderGuid,olv.ProductGuid
		) zz ON ol.ID = zz.OrderLineID

	DELETE FROM OrderLine
	WHERE ID IN (
		SELECT MAX(ol.ID)
		FROM 
		 
		OrderLine ol 
		JOIN OrderLineView olv ON ol.Guid = olv.OrderLineGuid
		WHERE
			olv.GrowerOrderGuid = @GrowerOrderToGuid
			AND (olv.SellerCode != 'DMO' OR ol.qtyOrdered > 0)
			AND olv.ProductGuid IN (
				SELECT ProductGuid FROM #IncomingDuplicteOrderLines
				)
		GROUP BY olv.GrowerOrderGuid,olv.ProductGuid
		HAVING COUNT(*) > 1
	)

	

	EXEC GrowerOrderCleanupOrphans NULL, @GrowerOrderFromGuid
		

	DROP TABLE #IncomingOrderLines
	DROP TABLE #IncomingDuplicteOrderLines