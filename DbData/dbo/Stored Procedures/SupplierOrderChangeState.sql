﻿
CREATE procedure [dbo].[SupplierOrderChangeState]
	@UserCode NVARCHAR(53),
	@OrderStatusLookupCode NVARCHAR(50),
	@NewOrderStatusLookupCode NVARCHAR(50),
	@GrowerOrderGuid UNIQUEIDENTIFIER = null,
	@SupplierOrderGuid UNIQUEIDENTIFIER = null
AS
	DECLARE @ParametersXmlData AS NVARCHAR(MAX)
	SET @ParametersXmlData = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('OrderStatusLookupCode',@OrderStatusLookupCode,1) +
		dbo.XmlSegment('NewOrderStatusLookupCode',@NewOrderStatusLookupCode,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('SupplierOrderGuid',@SupplierOrderGuid,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='SupplierOrderChangeState',
		@ObjectTypeLookupCode='SupplierOrder',
		@UserCode=@UserCode,
		@XmlData=@ParametersXmlData,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	SET @OrderStatusLookupCode = LTRIM(RTRIM(@OrderStatusLookupCode))
	SET @NewOrderStatusLookupCode = LTRIM(RTRIM(@NewOrderStatusLookupCode))

	DECLARE @OrderStatusLookupGuid AS UNIQUEIDENTIFIER
	SELECT @OrderStatusLookupGuid = LookupGuid
	FROM LookupView
	WHERE
		Path LIKE 'Code/OrderStatus/%' AND
		LookupCode = @OrderStatusLookupCode

	DECLARE @NewOrderStatusLookupGuid AS UNIQUEIDENTIFIER
	SELECT @NewOrderStatusLookupGuid = LookupGuid
	FROM LookupView
	WHERE
		Path LIKE 'Code/OrderStatus/%' AND
		LookupCode = @NewOrderStatusLookupCode

	IF 
		@OrderStatusLookupGuid IS NULL OR 
		@NewOrderStatusLookupGuid IS NULL OR 
		@OrderStatusLookupGuid = @NewOrderStatusLookupGuid
			BEGIN
				SET @ParametersXmlData = dbo.XmlSegment
				(
					'Data',
					dbo.XmlSegment('OrderStatusLookupGuid',@OrderStatusLookupGuid,1) +
					dbo.XmlSegment('NewOrderStatusLookupGuid',@NewOrderStatusLookupGuid,1),
					0
				)

				EXECUTE EventLogAdd
					@Comment='Lookup code is null or invalid, or begin state equals end state.',
					@LogTypeLookupCode='InvalidParameters',
					@ProcessLookupCode='SupplierOrderChangeState',
					@ObjectTypeLookupCode='Parameters',
					@UserCode=@UserCode,
					@XmlData=@ParametersXmlData

				RETURN
			END

	IF 
		@OrderStatusLookupCode = 'Pending' AND 
		@GrowerOrderGuid IS NULL AND 
		@SupplierOrderGuid IS NULL
			BEGIN
				EXECUTE EventLogAdd
					@Comment='Cannot change state from Pending without specifying an order.',
					@LogTypeLookupCode='InvalidParameters',
					@ProcessLookupCode='SupplierOrderChangeState',
					@ObjectTypeLookupCode='Parameters',
					@UserCode=@UserCode,
					@XmlData=@ParametersXmlData

				RETURN
			END

	DECLARE @LocalTemp TABLE
	(
		SupplierOrderGuid UNIQUEIDENTIFIER,
		GrowerOrderGuid UNIQUEIDENTIFIER,
		ProductGuid UNIQUEIDENTIFIER,
		VarietyGuid UNIQUEIDENTIFIER,
		SpeciesGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTemp
	SELECT
		SupplierOrderGuid,
		GrowerOrderGuid,
		ProductGuid,
		VarietyGuid,
		SpeciesGuid
	FROM OrderLineView
	WHERE
		OrderLineStatusLookupGuid =  @OrderStatusLookupGuid AND
		GrowerOrderGuid = ISNULL(@GrowerOrderGuid, GrowerOrderGuid) AND
		SupplierOrderGuid = ISNULL(@SupplierOrderGuid, SupplierOrderGuid)
	GROUP BY
		SupplierOrderGuid,
		GrowerOrderGuid,
		ProductGuid,
		VarietyGuid,
		SpeciesGuid

	DECLARE @PersonGuid AS UNIQUEIDENTIFIER
	DECLARE @LogTypeLookupGuid AS UNIQUEIDENTIFIER
	DECLARE @ProcessLookupGuid AS UNIQUEIDENTIFIER
	DECLARE @ObjectTypeLookupGuid AS UNIQUEIDENTIFIER

	EXECUTE EventLogGetGuids
		@LogTypeLookupCode='TableRowUpdate',
		@ProcessLookupCode='SupplierOrderChangeState',
		@ObjectTypeLookupCode='SupplierOrder',
		@UserCode=@UserCode,
		@LogTypeLookupGuid=@LogTypeLookupGuid OUT,
		@ProcessLookupGuid=@ProcessLookupGuid OUT,
		@ObjectTypeLookupGuid=@ObjectTypeLookupGuid OUT,
		@PersonGuid=@PersonGuid OUT

	DECLARE @XmlData AS NVARCHAR(MAX)
	SET @XmlData = dbo.XmlSegment
	(
		'AdditionalData',
		dbo.XmlSegment('OrderStatusLookupCode',@OrderStatusLookupCode,1) +
		dbo.XmlSegment('OrderStatusLookupGuid',@OrderStatusLookupGuid,1) +
		dbo.XmlSegment('NewOrderStatusLookupCode',@NewOrderStatusLookupCode,1) +
		dbo.XmlSegment('NewOrderStatusLookupGuid',@NewOrderStatusLookupGuid,1),
		0
	)

	DECLARE @Now DATETIME
	SET @Now = CURRENT_TIMESTAMP

	BEGIN TRANSACTION

	UPDATE SupplierOrder
		SET SupplierOrderStatusLookupGuid = @NewOrderStatusLookupGuid
	WHERE Guid IN (SELECT DISTINCT(SupplierOrderGuid) FROM @LocalTemp)

	INSERT INTO EventLog
	(
		Guid,
		LogTypeLookupGuid,
		ObjectTypeLookupGuid,
		ObjectGuid,
		ProcessLookupGuid,
		PersonGuid,
		EventTIme,
		GuidValue,
		XmlData,
		ProcessId
	)
	SELECT
		NewId(),
		@LogTypeLookupGuid,
		@ObjectTypeLookupGuid,
		SupplierOrderGuid,
		@ProcessLookupGuid,
		@PersonGuid,
		@Now,
		@OriginalEventLogGuid,
		@XmlData,
		0
	FROM @LocalTemp

	COMMIT TRANSACTION

	SELECT * From GrowerOrder
	WHERE Guid IN
	(SELECT DISTINCT(GrowerOrderGuid) FROM @LocalTemp)

	SELECT * From SupplierOrder
	WHERE Guid IN
	(SELECT DISTINCT(SupplierOrderGuid) FROM @LocalTemp)

	DECLARE @SupplierOrderCount AS INTEGER
	SET @SupplierOrderCount = @@ROWCOUNT

	SELECT * From Product
	WHERE Guid IN
	(SELECT DISTINCT(ProductGuid) FROM @LocalTemp)

	SELECT * From Variety
	WHERE Guid IN
	(SELECT DISTINCT(VarietyGuid) FROM @LocalTemp)

	SELECT * From Species
	WHERE Guid IN
	(SELECT DISTINCT(SpeciesGuid) FROM @LocalTemp)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallEnd',
		@ProcessLookupCode='SupplierOrderChangeState',
		@ObjectTypeLookupCode='SupplierOrder',
		@UserCode=@UserCode,
		@IntValue=@SupplierOrderCount