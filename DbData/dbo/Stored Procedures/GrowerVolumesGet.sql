﻿





CREATE PROCEDURE [dbo].[GrowerVolumesGet]
	@GrowerGuid uniqueidentifier
AS
	SELECT gv.Guid,rtrim(ps.Name) as ProgramSeason,ql.LevelNumber
	FROM Grower g 
	Join GrowerVolume gv on g.guid = gv.growerguid 
	Join ProgramSeason ps on gv.ProgramSeasonGuid = ps.Guid 
	Join VolumeLevel ql on gv.QuotedVolumeLevelGuid = ql.Guid 
	WHERE g.Guid =@GrowerGuid
	and ps.StartDate > dateadd(yy,-1,getdate()) 
	ORDER BY ps.Name