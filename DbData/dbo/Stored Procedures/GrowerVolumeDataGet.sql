﻿



--EXAMPLE:
/*
Declare @GrowerGuid1 uniqueidentifier
set @GrowerGuid1 = (select guid from grower where name = 'Scott''s Greenhouse')
EXECUTE GrowerVolumeDataGet  @GrowerGuid= @GrowerGuid1, @SupplierCodeList = 'HMA,VMX,RAK'
*/

CREATE procedure [dbo].[GrowerVolumeDataGet]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@PersonGuid AS UNIQUEIDENTIFIER = NULL,
	@GrowerGuid AS UNIQUEIDENTIFIER = NULL,
	@SupplierCodeList AS NVARCHAR(3000)
AS

	SET @SupplierCodeList = ',' + @SupplierCodeList + ','
	
	IF @GrowerGuid IS NULL AND (@PersonGuid IS NOT NULL OR @UserGuid IS NOT NULL)
		SELECT @GrowerGuid = GrowerGuid
		FROM PersonView
		WHERE
			(@PersonGuid IS NOT NULL AND PersonGuid=@PersonGuid) OR
			(@UserGuid IS NOT NULL AND UserGuid=@UserGuid)

	IF @GrowerGuid IS NOT NULL
		BEGIN
			
			Select s.Guid as SupplierGuid,s.code as SupplierCode,s.Name as SupplierName,ps.Guid as ProgramSeasonGuid,ps.name
				
				,vl.LevelNumber, vl.VolumeLow, vl.VolumeHigh
				,vlt.Name as RangeType
				,Cast('Base Level' as char(50)) as CurrentLevelType
			Into #VolumeResult
			FROM Supplier s
			
			JOIN Program pr on s.Guid = pr.SupplierGuid
			JOIN ProgramSeason ps on pr.Guid = ps.ProgramGuid 
				AND ps.StartDate < getdate()
				AND ps.EndDate > getdate()
			Join VolumeLevel vl on vl.ProgramSeasonGuid = ps.Guid
				and vl.LevelNumber = 1
			Join Lookup vlt on vl.VolumeLevelTypeLookupGuid = vlt.Guid
			
			
			
			WHERE --g.Guid = @GrowerGuid AND
			@SupplierCodeList LIKE '%,' + RTRIM(s.Code) + ',%'


			Update #VolumeResult
			set 
			LevelNumber = Coalesce(vlq.LevelNumber,vla.LevelNumber,vle.LevelNumber,vr.LevelNumber)
			,VolumeLow =  Coalesce(vlq.VolumeLow,vla.VolumeLow,vle.VolumeLow,vr.VolumeLow)
			,VolumeHigh = Coalesce(vlq.VolumeHigh,vla.VolumeHigh,vle.VolumeHigh,vr.VolumeHigh)
			,CurrentLevelType = case when vlq.LevelNumber is not null then 'Quoted Level by ePlantSource '
									 when vla.LevelNumber is not null then 'Actual Level from Supplier'
									 when vla.LevelNumber is not null then 'Estimated Level by Grower' else 'Base Level' end
			From #VolumeResult vr
			Join GrowerVolume gv on vr.ProgramSeasonGuid = gv.ProgramSeasonGuid
			LEFT JOIN VolumeLevel vlq on vlq.Guid = gv.QuotedVolumeLevelGuid
			LEFT JOIN VolumeLevel vla on vla.Guid = gv.ActualVolumeLevelGuid
			LEFT JOIN VolumeLevel vle on vle.Guid = gv.EstimatedVolumeLevelGuid
			
			Where gv.GrowerGuid = @GrowerGuid


			--debug
			--Select * from #VolumeResult order by SupplierCode

			SELECT 
			SupplierGuid
			,SupplierCode
			,SupplierName
			,Max(LevelNumber)
			,Max(VolumeLow)
			,Max(VolumeHigh)
			,Max(RangeType)
			,Max(CurrentLevelType)
			FROM  #VolumeResult
			GROUP BY 
			SupplierGuid
			,SupplierCode
			,SupplierName
			ORDER BY
			SupplierName


			Drop Table #VolumeResult
		END