﻿

CREATE procedure [dbo].[EventLogGetGuids]
	@LogTypeLookupCode AS NCHAR(50),
	@ProcessLookupCode AS NCHAR(50),
	@ObjectTypeLookupCode AS NCHAR(50),
	@UserCode AS NCHAR(56) = NULL,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@VerbosityLookupCode AS NCHAR(50) = NULL,
	@PriorityLookupCode AS NCHAR(50) = NULL,
	@SeverityLookupCode AS NCHAR(50) = NULL,
	@LogTypeLookupGuid AS UNIQUEIDENTIFIER OUT,
	@ProcessLookupGuid AS UNIQUEIDENTIFIER OUT,
	@ObjectTypeLookupGuid AS UNIQUEIDENTIFIER OUT,
	@PersonGuid AS UNIQUEIDENTIFIER OUT,
	@VerbosityLookupGuid AS UNIQUEIDENTIFIER OUT,
	@PriorityLookupGuid AS UNIQUEIDENTIFIER OUT,
	@SeverityLookupGuid AS UNIQUEIDENTIFIER OUT
AS
	SET @UserCode = LTRIM(RTRIM(@UserCode))
	IF @UserCode = ''
		SET @UserCode = NULL

	

	EXECUTE LookupGetOrAdd
		@ParentPath='Logging/LogType',
		@Code=@LogTypeLookupCode,
		@Default='Debug',
		@LookupGuid=@LogTypeLookupGuid OUT

	EXECUTE LookupGetOrAdd
		@ParentPath='Logging/Process',
		@Code=@ProcessLookupCode,
		@Default='Debug',
		@LookupGuid=@ProcessLookupGuid OUT

	EXECUTE LookupGetOrAdd
		@ParentPath='Logging/ObjectType',
		@Code=@ObjectTypeLookupCode,
		@Default='Debug',
		@LookupGuid=@ObjectTypeLookupGuid OUT

	EXECUTE LookupGetOrAdd
		@ParentPath='Logging/Verbosity',
		@Code=@VerbosityLookupCode,
		@Default='Normal',
		@LookupGuid=@VerbosityLookupGuid OUT

	EXECUTE LookupGetOrAdd
		@ParentPath='Logging/Priority',
		@Code=@PriorityLookupCode,
		@Default='Low',
		@LookupGuid=@PriorityLookupGuid OUT

	EXECUTE LookupGetOrAdd
		@ParentPath='Logging/Severity',
		@Code=@SeverityLookupCode,
		@Default='Info',
		@LookupGuid=@SeverityLookupGuid OUT