﻿




CREATE PROCEDURE [dbo].[B2BAvailUpdateLastAndNextSend]
	 @ProgramCodes as nvarchar(500),
	 @GrowerGuid nvarchar(50)
	
	 

AS
	Declare @theGuid uniqueidentifier
	set @theGuid =  (select Guid from Grower where Guid = @GrowerGuid)

	Update GrowerProgram
	set AvailLastSendDate = getdate(),
	AvailNextSendDate = dateadd(HOUR, gp.AvailHoursBetweenSend, gp.AvailNextSendDate)
	From GrowerProgram gp
	Join Program p on p.Guid = gp.ProgramGuid
	WHERE 
	gp.GrowerGuid = @theGuid
	and p.Code = @ProgramCodes  --to do, make this take multiple just in case