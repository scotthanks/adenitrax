﻿





CREATE PROCEDURE [dbo].[DummenPricingUploadAddMessage]
	 @JobNumber as bigint,
	 @MessageType as nvarchar(20),
	 @MessageState as nvarchar(20),
	 @Message as nvarchar(600)
	 

AS
		Insert into DummenPricingUploadJobMessage values(
		NewID(),
		(select Guid from DummenPricingUploadJob where ID = @JobNumber),
		getdate(),
		@MessageType,
		@MessageState,
		@Message
		)