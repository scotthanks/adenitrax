﻿





CREATE procedure [dbo].[OrderLineAdd]
	@UserCode AS NCHAR(56),
	@SupplierOrderGuid AS UNIQUEIDENTIFIER,
	@ProductGuid AS UNIQUEIDENTIFIER,
	@QtyOrdered AS INT,
	@Price AS DECIMAL(8,4) = NULL,
	@OrderLineGuid AS UNIQUEIDENTIFIER OUT,
	@ReturnRecordSet AS BIT = 1
AS
	DECLARE @Guid AS UNIQUEIDENTIFIER
	DECLARE @GrowerOrderGuid AS UNIQUEIDENTIFIER
	DECLARE @MaxLineNumber int
	SET @Guid = newid()

	DECLARE @OrderLineStatusLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserCode=@UserCode,
		@Path='Code/OrderLineStatus',
		@LookupCode='PreCart',
		@ProcessLookupCode='OrderLineAdd',
		@LookupGuid=@OrderLineStatusLookupGuid OUT

	DECLARE @ChangeTypeLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserCode=@UserCode,
		@Path='Code/ChangeType',
		@LookupCode='NoChange',
		@ProcessLookupCode='OrderLineAdd',
		@LookupGuid=@ChangeTypeLookupGuid OUT

	IF @Price IS NULL
		SET @Price = 0.0
	
	SET @GrowerOrderGuid = (select GrowerOrderGuid from SupplierOrder where Guid = @SupplierOrderGuid)
	SET @MaxLineNumber = (select isnull(Max(ol.LineNumber),0) from supplierOrder so
							JOIN OrderLine ol on so.Guid = ol.SupplierOrderGuid
							WHERE so.GrowerOrderGuid = @GrowerOrderGuid) 
						

	INSERT INTO OrderLine
	(
		Guid,
		SupplierOrderGuid,
		ProductGuid,
		QtyOrdered,
		QtyShipped,
		ActualPrice,
		OrderLineStatusLookupGuid,
		ChangeTypeLookupGuid,
		DateEntered,
		DateLastChanged,
		DateQTYLastChanged,
		LineNumber
	)
	VALUES
	(
		@Guid,
		@SupplierOrderGuid,
		@ProductGuid,
		@QtyOrdered,
		0.0, -- QqyShipped
		@Price, -- ActualPrice
		@OrderLineStatusLookupGuid,
		@ChangeTypeLookupGuid,
		GetDate(),
		GetDate(),
		GetDate(),
	    floor((@MaxLineNumber + 10) / 10) * 10  --this gets the next "10" Line Number
	)

	DECLARE @RowCount AS INT
	SET @RowCount = @@ROWCOUNT

	IF @@ROWCOUNT = 1
		BEGIN
			EXECUTE EventLogAdd
				@LogTypeLookupCode='RowAdd',
				@ProcessLookupCode='OrderLineAdd',
				@ObjectTypeLookupCode='OrderLine',
				@ObjectGuid=@OrderLineGuid,
				@UserCode=@UserCode

			SET @OrderLineGuid = @Guid
		END
	ELSE
		BEGIN
			EXECUTE EventLogAdd
				@Comment='@@ROWCOUNT not 1 after attempted write (See IntValue).',
				@LogTypeLookupCode='DatabaseError',
				@ProcessLookupCode='OrderLineAdd',
				@ObjectTypeLookupCode='OrderLine',
				@IntValue=@RowCount,
				@UserCode=@UserCode

			SET @OrderLineGuid = NULL
		END
	
	

	IF @ReturnRecordSet = 1
		SELECT * FROM OrderLine WHERE Guid = @OrderLineGuid

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='RowAdd',
		@ObjectTypeLookupCode='OrderLine',
		@ObjectGuid=@OrderLineGuid,
		@UserCode=@UserCode