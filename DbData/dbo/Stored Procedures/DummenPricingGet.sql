﻿




CREATE PROCEDURE [dbo].[DummenPricingGet]
	@GrowerGuid AS uniqueidentifier,
	@ProgramSeasonGuid AS uniqueidentifier
AS
select --count(*)--distinct ps.name
g.Guid as GrowerGuid
,g.Name as GrowerName
,ps.Guid as ProgramSeasonGuid
,ps.Name as ProgramSeasonName
,sp.Name as SpeciesName
,v.Name as VarietyName
,p.Guid as ProductGuid
,p.ID as ProductID
,p.SupplierIdentifier
,p.SupplierDescription
,gp.CuttingCost
,gp.RoyaltyCost
,gp.FreightCost
,gp.TagCost
,gp.LastUpdateDate
,cast(Case when p.DateDeactivated is null then 1 else 0 end as bit) as IsActive

from 
GrowerProductProgramSeasonPrice gp
Join Grower g on gp.GrowerGuid = g.Guid
Join ProgramSeason ps on gp.ProgramSeasonGuid = ps.Guid
Join Product p on gp.ProductGuid = p.Guid
Join Variety v on p.VarietyGuid = v.Guid
join Species sp on v.SpeciesGuid = sp.Guid
WHERE g.Guid = @GrowerGuid
AND (ps.Guid = @ProgramSeasonGuid OR  @ProgramSeasonGuid = '00000000-0000-0000-0000-000000000000')

Order by g.Name, ps.code,sp.Name,v.Name,p.supplierDescription