﻿







CREATE PROCEDURE [dbo].[B2BAvailSendListGet]
	 @CompareDate as datetime
	
	 

AS
	DECLARE @StartWeek varchar(10)
	DECLARE @ContinuousWeekNumber int
	DECLARE @EndWeek varchar(10)

	select @StartWeek = shipweekcode ,
	@ContinuousWeekNumber = ShipWeekContinuousWeekNumber
	from ShipWeekCurrentView
	
	

	SELECT g.name as GrowerName
	,rtrim(pr.Code) as ProgramCode
	,pr.name as ProgramName
	,DateAdd(HOUR,0,gp.AvailNextSendDate) as AvailNextSendDate
	,@StartWeek as StartWeek
	,(select shipweekCode
		from ShipWeekView
		Where 
			ShipWeekContinuousWeekNumber  = @ContinuousWeekNumber + AvailWeeks) as EndWeek
	,case when gp.AvailNextSendDate < @CompareDate then 'Ready to Send'
	when @CompareDate <= gp.AvailNextSendDate  then 'Waiting for Send Date'
	else 'unknown' end  as Status
	,g.Guid as GrowerGuid
	FROM GrowerProgram gp
    JOIN Grower g on gp.GrowerGuid = g.Guid
	JOIN Program pr on gp.ProgramGuid = pr.Guid
    WHERE gp.AvailHoursBetweenSend > 0
		AND gp.AvailNextSendDate is not null
		
	ORDER BY gp.AvailNextSendDate,g.name,pr.name