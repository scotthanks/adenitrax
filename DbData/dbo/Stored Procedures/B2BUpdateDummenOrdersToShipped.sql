﻿



CREATE PROCEDURE [dbo].[B2BUpdateDummenOrdersToShipped]
	 

AS
	UPDATE Orderline
	SET orderlinestatusLookupGuid = (
		SELECT guid FROM lookup WHERE path  = 'Code/OrderLineStatus/Shipped')
		,QtyShipped = QtyOrdered
	WHERE guid IN
	(
		SELECT ol.Guid
		FROM growerorder o
		JOIN SupplierOrder so ON so.growerorderguid = o.guid
		JOIN Orderline OL ON  OL.supplierorderguid = so.guid
		JOIN Product p ON ol.productguid = p.guid
		JOIN Program pr ON p.programguid = pr.guid
		JOIN Seller se ON pr.sellerguid = se.guid
		JOIN lookup osl ON ol.OrderLineStatusLookupGuid = osl.Guid

		WHERE 1 = 1
		AND se.Code = 'DMO'

		AND osl.name NOT IN ('Pending','Not Yet In Cart')
		AND osl.name NOT IN ('Shipped','Cancelled','Pending','Not Yet In Cart')
		AND o.shipweekguid = (SELECT shipweekguid
						FROM shipweekcurrentview)
	)