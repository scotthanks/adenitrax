﻿

CREATE procedure [dbo].[SearchPopulateSearchResults]
	@iDebug as bit
AS
	DECLARE @bDoVarieties bit = 1
	DECLARE @bDoSeries bit = 1
	DECLARE @bDoSpecies bit = 1
	DECLARE @bDoSuppliers bit = 1
	DECLARE @bDoGeneticOwners bit = 0
	DECLARE @bDoWebPages bit = 1

	DECLARE @SearchResultGuid uniqueidentifier
	DECLARE @SeriesGuid uniqueidentifier
	DECLARE @SeriesCode nVarchar(50)
	DECLARE @SeriesName nVarchar(100)
	DECLARE @VarietyGuid uniqueidentifier
	DECLARE @VarietyCode nVarchar(50)
	DECLARE @VarietyName nVarchar(100)
	DECLARE @SpeciesGuid uniqueidentifier
	DECLARE @SpeciesCode nVarchar(50)
	DECLARE @SpeciesName nVarchar(100)
	DECLARE @GeneticOwnerGuid uniqueidentifier
	DECLARE @GeneticOwnerCode nVarchar(50)
	DECLARE @GeneticOwnerName nVarchar(100)
	DECLARE @SupplierGuid uniqueidentifier
	DECLARE @SupplierCode nVarchar(50)
	DECLARE @SupplierName nVarchar(100)
	DECLARE @WebPageGuid uniqueidentifier
	DECLARE @WebPageDescription nVarchar(100)
	DECLARE @WebPagePath nVarchar(255)

	If @bDoVarieties = 1 
	BEGIN

		DECLARE @getVarieties CURSOR
		SET @getVarieties = CURSOR FOR
		SELECT v.Guid,rtrim(v.Code),v.name,rtrim(s.code),s.name,o.Name
		FROM Variety v
		JOIN Species s on v.SpeciesGuid = s.Guid
		Join GeneticOwner o on v.GeneticOwnerGuid = o.guid
		Where v.DateDeactivated is not null
		Order by s.Name,v.Name
		OPEN @getVarieties
		FETCH NEXT
		FROM @getVarieties INTO @VarietyGuid,@VarietyCode,@VarietyName,@SpeciesCode,@SpeciesName,@GeneticOwnerName
	
	
	------------Do the Varieties---------
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @iDebug = 1
				PRINT @VarietyCode + ' -- ' + @VarietyName + ' -- ' +   @SpeciesCode + ' -- ' + @SpeciesName
			  set @SearchResultGuid = (select guid from SearchResult 
									where ResultType = 'Variety' 
										and ResultCode = @VarietyCode)
			IF @iDebug = 1
				PRINT 'Guid is ' + cast(@SearchResultGuid as varchar(36))
									
			--Insert if needed
			If @SearchResultGuid is null
				BEGIN
					IF @iDebug = 1
						PRINT 'Inserting'

					INSERT INTO SearchResult values(newID(),
					'Variety',@VarietyCode,@VarietyName,
					@VarietyName,@GeneticOwnerName,
					'','','',
					'','','',0,
					'','','',0,
					'','','',0,
					'','','','',
					'','','','',
					'','','',''
					)
				END
		
			--Update
			set @SearchResultGuid = (select Guid from SearchResult 
									where ResultType = 'Variety' 
										and ResultCode = @VarietyCode)			
			Update SearchResult set
			ResultCode = @VarietyCode, ResultName = @VarietyName,
			HeaderText = @VarietyName,SubHeaderText = @GeneticOwnerName
			Where guid = @SearchResultGuid
			
			
			DECLARE @ProgramCode nVarchar(50)
			DECLARE @ProgramName nVarchar(100)
			DECLARE @ProgramTypeCode nVarchar(50)
			DECLARE @ProductFormCategoryCode nVarchar(50)
			DECLARE @ProductCount int
			DECLARE @iCount int
			set @iCount = 0
		
			DECLARE @getPrograms CURSOR
			SET @getPrograms = CURSOR FOR
			SELECT TOP 3 
			prt.name + '-' + pfc.name,rtrim(prt.code),rtrim(pfc.code),Count(*)
			FROM Variety v
			JOIN Product p on p.VarietyGuid = v.Guid
			Join Program pr on p.ProgramGuid = pr.Guid
			Join ProgramType prt on pr.ProgramTypeGuid = prt.Guid
			Join ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.Guid
			WHERE v.guid = @VarietyGuid 
				AND  p.DateDeactivated is null
			GROUP BY prt.name + '-' + pfc.name,prt.code,pfc.code
			ORDER BY Count(*) Desc,prt.name + '-' + pfc.name
			OPEN @getPrograms
			FETCH NEXT
			FROM @getPrograms INTO @ProgramName,@ProgramTypeCode,@ProductFormCategoryCode,@ProductCount
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
			
				set @iCount = @iCount + 1
			
				IF @iDebug = 1
					PRINT @iCount
			
				IF @iCount = 1
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 1 -- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						CatalogProductformCategoryCode = @ProgramTypeCode,
						CatalogProgramTypeCode = @ProductFormCategoryCode,
						Program1Name = @ProgramName,
						Program1ProductformCategoryCode = @ProductFormCategoryCode,
						Program1ProgramTypeCode = @ProgramTypeCode,
						Program1ProductCount= @ProductCount,
						Species1Code = @SpeciesCode,
						Species1Name = @SpeciesName,
						Species1ProgramTypeCode =@ProgramTypeCode,
						Species1ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
				ELSE If @iCount = 2
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 2-- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						Program2Name = @ProgramName,
						Program2ProductformCategoryCode = @ProductFormCategoryCode,
						Program2ProgramTypeCode = @ProgramTypeCode,
						Program2ProductCount= @ProductCount,
						Species2Code = @SpeciesCode,
						Species2Name = @SpeciesName,
						Species2ProgramTypeCode =@ProgramTypeCode,
						Species2ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
				ELSE
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 3-- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						Program3Name = @ProgramName,
						Program3ProductformCategoryCode = @ProductFormCategoryCode,
						Program3ProgramTypeCode = @ProgramTypeCode,
						Program3ProductCount= @ProductCount,
						Species3Code = @SpeciesCode,
						Species3Name = @SpeciesName,
						Species3ProgramTypeCode =@ProgramTypeCode,
						Species3ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
				--If @iCount = 3
				--	set @iCount = 0
					
			
				FETCH NEXT
				FROM @getPrograms INTO @ProgramName,@ProgramTypeCode,@ProductFormCategoryCode,@ProductCount

			END
			
			CLOSE @getPrograms
			DEALLOCATE @getPrograms
		

			Update SearchResult set 
			ImageURI = (select 'http://epsstorage.blob.core.windows.net/catalogimages/' + imf.Name + '.jpg'
						From Variety v
						Join ImageSet ims on v.ImageSetGuid = ims.guid
						Join Imagefile imf on imf.ImageSetGuid = ims.guid
						Join Lookup ift on imf.ImageFileTypeLookupGuid = ift.guid
						Where v.Guid = @VarietyGuid
							AND ift.Code = 'Thumbnail' )
			where Guid = @SearchResultGuid


			FETCH NEXT
			FROM @getVarieties INTO @VarietyGuid,@VarietyCode,@VarietyName,@SpeciesCode,@SpeciesName,@GeneticOwnerName
		END
		
		
		
		
		CLOSE @getVarieties
		DEALLOCATE @getVarieties

		

	END
-------Do the Series-----------------
	IF @bDoSeries = 1 
		BEGIN
		DECLARE @getSeries CURSOR
		SET @getSeries = CURSOR FOR
		SELECT se.Guid,rtrim(se.Code),se.name,rtrim(s.code),s.name,max(o.Name)
		FROM Series se
		JOIN Variety v on v.seriesguid = se.guid
		JOIN Species s on se.SpeciesGuid = s.Guid
		Join GeneticOwner o on v.GeneticOwnerGuid = o.guid
		GROUP BY se.Guid,se.Code,se.name,s.code,s.name
		Order by s.Name,se.Name
		OPEN @getSeries
		FETCH NEXT
		FROM @getSeries INTO @SeriesGuid,@SeriesCode,@SeriesName,@SpeciesCode,@SpeciesName,@GeneticOwnerName
	
	
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @iDebug = 1
				PRINT @SeriesCode + ' -- ' + @SeriesName + ' -- ' +   @SpeciesCode + ' -- ' + @SpeciesName
			  set @SearchResultGuid = (select guid from SearchResult 
									where ResultType = 'Series' 
										and ResultCode = @SeriesCode)
			IF @iDebug = 1
				PRINT 'Guid is ' + cast(@SearchResultGuid as varchar(36))
									
			--Insert if needed
			If @SearchResultGuid is null
				BEGIN
					IF @iDebug = 1
						PRINT 'Inserting'

					INSERT INTO SearchResult values(newID(),
					'Series',@SeriesCode,@SeriesName,
					@SeriesName,@GeneticOwnerName,
					'','','',
					'','','',0,
					'','','',0,
					'','','',0,
					'','','','',
					'','','','',
					'','','',''
					)
				END
		
			--Update
			set @SearchResultGuid = (select Guid from SearchResult 
									where ResultType = 'Series' 
										and ResultCode = @SeriesCode)			
			
			Update SearchResult set
			ResultCode = @SeriesCode, ResultName = @SeriesName,
			HeaderText = @SeriesName,SubHeaderText = @GeneticOwnerName
			Where guid = @SearchResultGuid
			
			
			set @iCount = 0
		
			SET @getPrograms = CURSOR FOR
			SELECT TOP 3 
			prt.name + '-' + pfc.name,rtrim(prt.code),rtrim(pfc.code),Count(*)
			FROM Series s
			JOIN Variety v on v.SeriesGuid  = s.Guid
			JOIN Product p on p.VarietyGuid = v.Guid
			Join Program pr on p.ProgramGuid = pr.Guid
			Join ProgramType prt on pr.ProgramTypeGuid = prt.Guid
			Join ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.Guid
			WHERE s.guid = @SeriesGuid 
				AND  p.DateDeactivated is null
			GROUP BY prt.name + '-' + pfc.name,prt.code,pfc.code
			ORDER BY Count(*) Desc,prt.name + '-' + pfc.name
			OPEN @getPrograms
			FETCH NEXT
			FROM @getPrograms INTO @ProgramName,@ProgramTypeCode,@ProductFormCategoryCode,@ProductCount
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
			
				set @iCount = @iCount + 1
			
				IF @iDebug = 1
					PRINT @iCount
			
				IF @iCount = 1
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 1 -- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						CatalogProductformCategoryCode = @ProgramTypeCode,
						CatalogProgramTypeCode = @ProductFormCategoryCode,
						Program1Name = @ProgramName,
						Program1ProductformCategoryCode = @ProductFormCategoryCode,
						Program1ProgramTypeCode = @ProgramTypeCode,
						Program1ProductCount= @ProductCount,
						Species1Code = @SpeciesCode,
						Species1Name = @SpeciesName,
						Species1ProgramTypeCode =@ProgramTypeCode,
						Species1ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
				ELSE If @iCount = 2
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 2-- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						Program2Name = @ProgramName,
						Program2ProductformCategoryCode = @ProductFormCategoryCode,
						Program2ProgramTypeCode = @ProgramTypeCode,
						Program2ProductCount= @ProductCount,
						Species2Code = @SpeciesCode,
						Species2Name = @SpeciesName,
						Species2ProgramTypeCode =@ProgramTypeCode,
						Species2ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
				ELSE
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 3-- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						Program3Name = @ProgramName,
						Program3ProductformCategoryCode = @ProductFormCategoryCode,
						Program3ProgramTypeCode = @ProgramTypeCode,
						Program3ProductCount= @ProductCount,
						Species3Code = @SpeciesCode,
						Species3Name = @SpeciesName,
						Species3ProgramTypeCode =@ProgramTypeCode,
						Species3ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
					
			
				FETCH NEXT
				FROM @getPrograms INTO @ProgramName,@ProgramTypeCode,@ProductFormCategoryCode,@ProductCount

			END
			
			CLOSE @getPrograms
			DEALLOCATE @getPrograms
			
			--Fill image
			set @VarietyGuid = (select top 1 v.Guid 
								From Series s JOIN Variety v on s.guid = v.seriesguid
								WHERE s.Code = @SeriesCode)

			Update SearchResult set 
			ImageURI = (select 'http://epsstorage.blob.core.windows.net/catalogimages/' + imf.Name + '.jpg'
						From Variety v
						Join ImageSet ims on v.ImageSetGuid = ims.guid
						Join Imagefile imf on imf.ImageSetGuid = ims.guid
						Join Lookup ift on imf.ImageFileTypeLookupGuid = ift.guid
						Where v.Guid = @VarietyGuid
							AND ift.Code = 'Thumbnail' )
			where Guid = @SearchResultGuid


			FETCH NEXT
			FROM @getSeries INTO @SeriesGuid,@SeriesCode,@SeriesName,@SpeciesCode,@SpeciesName,@GeneticOwnerName
		END
		CLOSE @getSeries
		DEALLOCATE @getSeries
	END
--Do the Species
	IF @bDoSpecies = 1 
		BEGIN
		DECLARE @getSpecies CURSOR
		SET @getSpecies = CURSOR FOR
		SELECT s.Guid,rtrim(s.Code),s.name
		FROM Species s
		Order by s.Name
		OPEN @getSpecies
		FETCH NEXT
		FROM @getSpecies INTO @SpeciesGuid,@SpeciesCode,@SpeciesName
	
	
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @iDebug = 1
				PRINT @SpeciesCode + ' -- ' + @SpeciesName
			  set @SearchResultGuid = (select guid from SearchResult 
									where ResultType = 'Species' 
										and ResultCode = @SpeciesCode)
			IF @iDebug = 1
				PRINT 'Guid is ' + cast(@SearchResultGuid as varchar(36))
									
			--Insert if needed
			If @SearchResultGuid is null
				BEGIN
					IF @iDebug = 1
						PRINT 'Inserting'

					INSERT INTO SearchResult values(newID(),
					'Species',@SpeciesCode,@SpeciesName,
					@SpeciesName,'',
					'','','',
					'','','',0,
					'','','',0,
					'','','',0,
					'','','','',
					'','','','',
					'','','',''
					)
				END
		
			--Update
			set @SearchResultGuid = (select Guid from SearchResult 
									where ResultType = 'Species' 
										and ResultCode = @SpeciesCode)			
			Update SearchResult set
			ResultCode = @SpeciesCode, ResultName = @SpeciesName,
			HeaderText = @SpeciesName,SubHeaderText = @GeneticOwnerName
			Where guid = @SearchResultGuid
			
			--To Do -- Fill in data
			
			--Fill image
			set @VarietyGuid = (select top 1 v.Guid 
								From Species s JOIN Variety v on s.guid = v.Speciesguid
								WHERE s.Code = @SpeciesCode)

			Update SearchResult set 
			ImageURI = (select 'http://epsstorage.blob.core.windows.net/catalogimages/' + imf.Name + '.jpg'
						From Variety v
						Join ImageSet ims on v.ImageSetGuid = ims.guid
						Join Imagefile imf on imf.ImageSetGuid = ims.guid
						Join Lookup ift on imf.ImageFileTypeLookupGuid = ift.guid
						Where v.Guid = @VarietyGuid
							AND ift.Code = 'Thumbnail' )
			where Guid = @SearchResultGuid




		
			FETCH NEXT
			FROM @getSpecies INTO @SpeciesGuid,@SpeciesCode,@SpeciesName
		END
		CLOSE @getSpecies
		DEALLOCATE @getSpecies
	END
--Do the Suppliers
	IF @bDoSuppliers = 1 
		BEGIN
		DECLARE @getSuppliers CURSOR
		SET @getSuppliers = CURSOR FOR
		SELECT s.Guid,rtrim(s.Code),s.name
		FROM Supplier s
		
		Order by s.Name
		OPEN @getSuppliers
		FETCH NEXT
		FROM @getSuppliers INTO @SupplierGuid,@SupplierCode,@SupplierName
	
	
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @iDebug = 1
				PRINT @SupplierCode + ' -- ' + @SupplierName 
			  set @SearchResultGuid = (select guid from SearchResult 
									where ResultType = 'Supplier' 
										and ResultCode = @SupplierCode)
			IF @iDebug = 1
				PRINT 'Guid is ' + cast(@SearchResultGuid as varchar(36))
									
			--Insert if needed
			If @SearchResultGuid is null
				BEGIN
					IF @iDebug = 1
						PRINT 'Inserting'

					INSERT INTO SearchResult values(newID(),
					'Supplier',@SupplierCode,@SupplierName,
					@SupplierName,'',
					'','','',
					'','','',0,
					'','','',0,
					'','','',0,
					'','','','',
					'','','','',
					'','','',''
					)
				END
		
			--Update
			set @SearchResultGuid = (select Guid from SearchResult 
									where ResultType = 'Supplier' 
										and ResultCode = @SupplierCode)			
			

			Update SearchResult set
			ResultCode = @SupplierCode, 
			ResultName = @SupplierName,
			HeaderText = @SupplierName,
			SubHeaderText = ''
			Where guid = @SearchResultGuid
			
			
			set @iCount = 0
		
			SET @getPrograms = CURSOR FOR
			SELECT TOP 3 
			prt.name + '-' + pfc.name,rtrim(prt.code),rtrim(pfc.code),Count(*)
			FROM Supplier s
			Join Program pr on pr.SupplierGuid = s.Guid
			JOIN Product p on p.ProgramGuid = pr.Guid
			Join ProgramType prt on pr.ProgramTypeGuid = prt.Guid
			Join ProductFormCategory pfc on pr.ProductFormCategoryGuid = pfc.Guid
			WHERE s.guid = @SupplierGuid 
				AND  p.DateDeactivated is null
			GROUP BY prt.name + '-' + pfc.name,prt.code,pfc.code
			ORDER BY Count(*) Desc,prt.name + '-' + pfc.name
			OPEN @getPrograms
			FETCH NEXT
			FROM @getPrograms INTO @ProgramName,@ProgramTypeCode,@ProductFormCategoryCode,@ProductCount
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
			
				set @iCount = @iCount + 1
			
				IF @iDebug = 1
					PRINT @iCount
			
				IF @iCount = 1
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 1 -- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						CatalogProductformCategoryCode = @ProgramTypeCode,
						CatalogProgramTypeCode = @ProductFormCategoryCode,
						Program1Name = @ProgramName,
						Program1ProductformCategoryCode = @ProductFormCategoryCode,
						Program1ProgramTypeCode = @ProgramTypeCode,
						Program1ProductCount= @ProductCount,
						Species1Code = @SpeciesCode,
						Species1Name = @SpeciesName,
						Species1ProgramTypeCode =@ProgramTypeCode,
						Species1ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
				ELSE If @iCount = 2
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 2-- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						Program2Name = @ProgramName,
						Program2ProductformCategoryCode = @ProductFormCategoryCode,
						Program2ProgramTypeCode = @ProgramTypeCode,
						Program2ProductCount= @ProductCount,
						Species2Code = @SpeciesCode,
						Species2Name = @SpeciesName,
						Species2ProgramTypeCode =@ProgramTypeCode,
						Species2ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
				ELSE
					BEGIN
						IF @iDebug = 1
							PRINT 'Update 3-- ' + cast(@SearchResultGuid as varchar(36))
				
						UPDATE SearchResult SET 
						Program3Name = @ProgramName,
						Program3ProductformCategoryCode = @ProductFormCategoryCode,
						Program3ProgramTypeCode = @ProgramTypeCode,
						Program3ProductCount= @ProductCount,
						Species3Code = @SpeciesCode,
						Species3Name = @SpeciesName,
						Species3ProgramTypeCode =@ProgramTypeCode,
						Species3ProductformCategoryCode = @ProductFormCategoryCode
						WHERE guid = @SearchResultGuid
					END
					
			
				FETCH NEXT
				FROM @getPrograms INTO @ProgramName,@ProgramTypeCode,@ProductFormCategoryCode,@ProductCount

			END
			
			CLOSE @getPrograms
			DEALLOCATE @getPrograms
			
			--Fill image
			set @VarietyGuid = (select top 1 v.Guid 
								From Supplier s 
								JOIN Program pr on s.Guid = pr.SupplierGuid
								JOIN Product p on p.ProgramGuid = pr.guid
								JOIN Variety v on p.varietyguid = v.guid
								WHERE s.Code = @SupplierCode)

			Update SearchResult set 
			ImageURI = (select 'http://epsstorage.blob.core.windows.net/catalogimages/' + imf.Name + '.jpg'
						From Variety v
						Join ImageSet ims on v.ImageSetGuid = ims.guid
						Join Imagefile imf on imf.ImageSetGuid = ims.guid
						Join Lookup ift on imf.ImageFileTypeLookupGuid = ift.guid
						Where v.Guid = @VarietyGuid
							AND ift.Code = 'Thumbnail' )
			where Guid = @SearchResultGuid


		
			FETCH NEXT
			FROM @getSuppliers INTO @SupplierGuid,@SupplierCode,@SupplierName
		END
		CLOSE @getSuppliers
		DEALLOCATE @getSuppliers
	END

--Do the GeneticOwners
	IF @bDoGeneticOwners = 1 
		BEGIN
		DECLARE @getGeneticOwners CURSOR
		SET @getGeneticOwners = CURSOR FOR
		SELECT o.Guid,rtrim(o.Code),o.name
		FROM GeneticOwner o
		
		Order by o.Name
		OPEN @getGeneticOwners
		FETCH NEXT
		FROM @getGeneticOwners INTO @GeneticOwnerGuid,@GeneticOwnerCode,@GeneticOwnerName
	
	
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @iDebug = 1
				PRINT @GeneticOwnerCode + ' -- ' + @GeneticOwnerName 
			  set @SearchResultGuid = (select guid from SearchResult 
									where ResultType = 'GeneticOwner' 
										and ResultCode = @GeneticOwnerCode)
			IF @iDebug = 1
				PRINT 'Guid is ' + cast(@SearchResultGuid as varchar(36))
									
			--Insert if needed
			If @SearchResultGuid is null
				BEGIN
					IF @iDebug = 1
						PRINT 'Inserting'

					INSERT INTO SearchResult values(newID(),
					'GeneticOwner',@GeneticOwnerCode,@GeneticOwnerName,
					@GeneticOwnerName,'',
					'','','',
					'','','',0,
					'','','',0,
					'','','',0,
					'','','','',
					'','','','',
					'','','',''
					)
				END
		
			--Update
			set @SearchResultGuid = (select Guid from SearchResult 
									where ResultType = 'GeneticOwner' 
										and ResultCode = @GeneticOwnerCode)			
			--To Do -- Fill in data
		   --Update
			Update SearchResult
			set ImageURI = 'http://epsstorage.blob.core.windows.net/websiteimages/ePlantSourceLogo.png'		
			where guid = @SearchResultGuid


		
			FETCH NEXT
			FROM @getGeneticOwners INTO @GeneticOwnerGuid,@GeneticOwnerCode,@GeneticOwnerName
		END
		CLOSE @getGeneticOwners
		DEALLOCATE @getGeneticOwners
	END


-------Do the Web Pages
	IF @bDoWebPages = 1 
		BEGIN
		DECLARE @getWebPages CURSOR
		SET @getWebPages = CURSOR FOR
		SELECT Guid,Description,Path
		FROM epsContentDev..PageContent pc
		ORDER BY pc.Description

		OPEN @getWebPages
		FETCH NEXT
		FROM @getWebPages INTO @WebPageGuid,@WebPageDescription,@WebPagePath
	
	
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			IF @iDebug = 1
				PRINT @WebPageDescription + ' -- ' + @WebPagePath 
			  set @SearchResultGuid = (select guid from SearchResult 
									where ResultType = 'WebPage' 
										and ResultCode = @WebPageDescription)
			IF @iDebug = 1
				PRINT 'Guid is ' + cast(@SearchResultGuid as varchar(36))
									
			--Insert if needed
			If @SearchResultGuid is null
				BEGIN
					IF @iDebug = 1
						PRINT 'Inserting'

					INSERT INTO SearchResult values(newID(),
					'WebPage',@WebPageDescription,@WebPagePath,
					@WebPageDescription,'',
					'','','',
					'','','',0,
					'','','',0,
					'','','',0,
					'','','','',
					'','','','',
					'','','',''
					)
				END
		

			--To Do -- Fill in data
			--Update
			Update SearchResult
			set ImageURI = 'http://epsstorage.blob.core.windows.net/websiteimages/ePlantSourceLogo.png'		
			where guid = @SearchResultGuid
			
		
		
			FETCH NEXT
			FROM @getWebPages INTO @WebPageGuid,@WebPageDescription,@WebPagePath
		END
		CLOSE @getWebPages
		DEALLOCATE @getWebPages
	END


--Finally
Update SearchResult set ResultName = replace(ResultName,'™','')
Update SearchResult set ResultName = replace(ResultName,'®','')