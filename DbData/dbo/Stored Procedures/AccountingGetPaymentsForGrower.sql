﻿








CREATE PROCEDURE [dbo].[AccountingGetPaymentsForGrower]
	 @GrowerGuid as uniqueidentifier
	
	 

AS
select 
    AssetLedger.TransactionGuid as PaymentTransactionGuid,
	AssetLedger.ParentGuid as PaymentParentGuid,
	case when theGl2.Code = 'UnappliedCreditCardPayments' then 'Credit Card'
	     when theGl2.Code = 'UnappliedPayments' then 'Check'
		 when theGl2.Code = 'UnappliedCredits' then 'Credit' end as  PaymentType,
	AssetLedger.InternalIdNumber as InternalIdNumber, 
	AssetLedger.ExternalIdNumber as ExternalIdNumber, 
    AssetLedger.Amount, 
    AssetLedger.TransactionDate as PaymentDate
	--, 
 --   AssetLedger.EntryTypeLookupGuid as AssetEntryTypeLookupGuid, 
	,theType.code,thetype.name
	,theGL.code,theGL.name
	,theGL2.code,theGL2.name


 --   AssetLedger.ParentGuid as AssetParentGuid,
 --   AssetLedger.GLAccountLookupGuid,
	--LiablityLedger.EntryTypeLookupGuid as LiablityLedgerEntryTypeLookupGuid, 
   , LiablityLedger.InternalIdNumber as LiablityLedgerInternalIdNumber
   , LiablityLedger.ExternalIdNumber as LiablityLedgerExternalIdNumber 
	--LiablityLedger.ParentGuid as LiablityParentGuid,
	--LiablityLedger.GLAccountLookupGuid 
from Ledger as AssetLedger 
    join Ledger as LiablityLedger on LiablityLedger.TransactionGuid = AssetLedger.TransactionGuid
	Join lookup theType on AssetLedger.EntryTypeLookupGuid = theType.Guid
	Join lookup theGL on AssetLedger.GLAccountLookupGuid = theGL.Guid
	Join lookup theGL2 on LiablityLedger.GLAccountLookupGuid = theGL2.Guid
where 
    AssetLedger.GrowerGuid = @GrowerGuid --'9CB1183C-20AC-4049-9206-25C558CA5BEF'--
	and theType.Code = 'Debit' 
	and (--1 = 1 OR
		theGL.code = 'Cash' and theGl2.Code =  'UnappliedPayments'
		OR theGL.code = 'Cash' and theGl2.Code =  'UnappliedCredits'
		OR theGL.code = 'Cash' and theGl2.Code =  'UnappliedCreditCardPayments'
		--OR theGL.code = 'UnappliedCreditCardPayments' and theGl2.Code =  'UnappliedCreditCardPayments'
		)
	--and theGl2.Code in ('UnappliedCredits', 'UnappliedPayments','UnappliedCreditCardPayments')                          
	-- 
	order by AssetLedger.TransactionDate