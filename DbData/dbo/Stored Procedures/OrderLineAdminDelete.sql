﻿









CREATE PROCEDURE [dbo].[OrderLineAdminDelete]
	@OrderLineGuid AS UNIQUEIDENTIFIER
	
	

AS
	Declare @UserGuid uniqueidentifier
	DECLARE @GrowerOrderGuid UniqueIdentifier

	SELECT 
	@UserGuid = userguid
	FROM Person 
	WHERE 
	firstname  = 'Customer' 
	AND lastname = 'Service'

	SELECT 
	@GrowerOrderGuid = GrowerOrderGuid
	FROM
		OrderLineView
	WHERE 
	OrderLineGuid = @OrderLineGuid


	--delete line
	Delete from OrderLine	
	WHERE Guid=@OrderLineGuid

	
	--recalc the fees
	exec GrowerOrderRecalcFees @UserGuid, @GrowerOrderGuid, 1