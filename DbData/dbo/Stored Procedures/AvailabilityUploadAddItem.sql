﻿




CREATE PROCEDURE [dbo].[AvailabilityUploadAddItem]
	 @JobNumber as bigint,
	 @SupplierCode as nvarchar(10),
	 @SupplierID as nvarchar(50),
	 @SupplierDesc as nvarchar(100),
	 @StartYear as int,
	 @Week as int,
	 @Value as int,
	 @AvailType as nvarchar(10)

	 
AS
		Insert into ReportedAvailabilityUpdate values(
			getDate(),
			@SupplierCode,
			NULL,
			@SupplierID,
			@SupplierDesc,
			NULL,
			@StartYear,
			@Week,
			NULL,
			@Value,
			@AvailType,
			NULL,
			@JobNumber,
			0,
			(select AvailabilityInSalesUnit From Supplier where Code = @SupplierCode),
			1
		)