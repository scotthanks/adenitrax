﻿









CREATE PROCEDURE [dbo].[SupplierOrderRecalcFees]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@SupplierOrderGuid AS UNIQUEIDENTIFIER
AS
	DECLARE @VerifiedSupplierOrderGuid AS UNIQUEIDENTIFIER
	DECLARE @SupplierCode AS varchar(10)
	DECLARE @SellerCode AS varchar(10)
	DECLARE @ProgramCode AS varchar(10)
	DECLARE @CountryCode AS varchar(10)
	DECLARE @StateCode AS varchar(10)
	DECLARE @StateGuid AS UNIQUEIDENTIFIER
	DECLARE @ShipMethodGuid AS UNIQUEIDENTIFIER
	DECLARE @ShipMethod AS varchar(10)
	DECLARE @TagRatioCode AS varchar(10)
	DECLARE @nBoxes AS numeric(14,4)
	DECLARE @nFreight AS numeric(14,4)
	DECLARE @nPrice AS numeric(14,4)
	DECLARE @nTagPrice AS numeric(14,4)
	DECLARE @iQty AS int
	DECLARE @iBoxes AS int
	DECLARE @iTrays AS int
	DECLARE @iTagRatio AS int
	DECLARE @iTagBundles AS int
	DECLARE @iWeek AS int
	DECLARE @bIsPartialBox AS bit
	Declare @sEstimatedFreightText as varchar(2000)
	Declare @sBelowMinimumText as varchar(2000)
	Declare @sBoxingText as varchar(2000)
	Declare @FeeAmount AS decimal(6,2)
	Declare @FeeDescription AS nVarchar(500)
	DECLARE @bFreightCalcStandard AS bit
	DECLARE @bFreightCalcFromPrice AS bit
	DECLARE @bTagsCalcStandard AS bit
	DECLARE @nBelowMinCharge AS decimal(6,2)
	DECLARE @iBelowMinLevel AS int
	DECLARE @BelowMinLevelType AS uniqueidentifier
	DECLARE @bGrowerUsesDummenPriceMethod AS bit
	Declare @iDebug AS int


	SET @iDebug = 0
	SELECT
		@VerifiedSupplierOrderGuid = so.Guid,
		@SupplierCode = rtrim(s.Code),
		@ShipMethodGuid = so.ShipMethodLookupGuid,
		@ProgramCode = rtrim(Coalesce(pr.Code,'MULTIPLE')),
		@CountryCode = rtrim(c.Code),
		@StateCode = rtrim(st.Code),
		@TagRatioCode = rtrim(tr.Code),
		@StateGuid = st.guid,
		@iWeek = sw.Week,
		@bFreightCalcStandard = s.FreightCalcStandard,
		@bFreightCalcFromPrice = s.FreightCalcFromPrice,
		@bTagsCalcStandard = s.TagsCalcStandard,
		@nBelowMinCharge = s.BelowMinCharge,
		@iBelowMinLevel = s.BelowMinLevel,
		@BelowMinLevelType = s.BelowMinLevelTypeGuid,
		@SellerCode = rtrim(se.Code),
		@bGrowerUsesDummenPriceMethod = g.UseDummenPriceMethod
	FROM SupplierOrder so
	Join GrowerOrder o on so.GrowerOrderGuid = o.Guid
	Join GrowerAddress ga on o.ShipToAddressGuid = ga.guid
	Join Address a on ga.AddressGuid = a.guid
	Join State st on a.stateguid = st.guid
	Join Country c on st.countryguid = c.guid
	Join Supplier s on so.SupplierGuid = s.Guid
	Join Grower g on g.Guid = o.GrowerGuid
	Join ShipWeek sw on o.ShipWeekGuid = sw.Guid
	Join Lookup tr on so.TagRatioLookupGuid = tr.Guid
	LEFT JOIN Program pr on so.SupplierGuid = pr.SupplierGuid
		AND o.ProgramTypeGuid = pr.ProgramTypeGuid
		AND o.ProductFormCategoryGuid = pr.ProductFormCategoryGuid
	LEFT JOIN Seller se on pr.SellerGuid = se.Guid
	WHERE so.Guid = @SupplierOrderGuid

	--print 'Got Here 1' 

	


	DECLARE @ProgramGuid AS UNIQUEIDENTIFIER
	SELECT 
		@ProgramGuid = pr.Guid

	FROM SupplierOrder so
	Join GrowerOrder o on so.GrowerOrderGuid = o.guid
	Join Program pr on o.ProgramTypeGuid = pr.ProgramTypeGuid 
		AND o.ProductFormCategoryGuid = pr.ProductFormCategoryGuid
		AND so.SupplierGuid = pr.SupplierGuid
	WHERE
		so.guid = @SupplierOrderGuid 

--	print 'Got Here 2' 	
	IF @VerifiedSupplierOrderGuid IS NULL
	BEGIN
		--print 'ha'
		EXECUTE EventLogAdd
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@Comment='SupplierOrderGuid is not valid',
			@LogTypeLookupCode='ParameterError',
			@ProcessLookupCode='SupplierOrderRecalcFees',
			@ObjectTypeLookupCode='SupplierOrder',
			@GuidValue=@SupplierOrderGuid

		RETURN
	END
		--print 'Got Here 3' 	
------Delete all the fees and start over, keep the manual ones
	Delete from SupplierOrderfee where SupplierOrderguid = @SupplierOrderGuid and IsManual = 0

	If @SupplierCode in('AGR','ATH','BEE','BUR','CEN','CFF','CFT','DGI','EUR','GNS','GRE',
						'GED','GRL','GUL','HDS','HIQ','HMA','KIE','KNO','KUB',
						'LIN','LUC','MIC','MIL','MLM','PEA','PGR','PLU','PSI','RAK','ROS',
						'SKA','SPE','SUN','TER','VMX','WEL','WEN',
						'DAR','DCA','DEA','DES','DET','DFS','DFL','DGU','DGS','DMX')
						 
	BEGIN


		SELECT @sBelowMinimumText =	'A below minimum charge is assessed for orders that do not meet minimum requirements. '
		SELECT @sBoxingText =	'Boxing charges are assessed by supplier. <BR> Please visit supplier pages for more details on boxing fees and when they apply.'


		If @ShipMethodGuid in (select Guid from lookup 
									where path in('Code/ShipMethod/AIR','Code/ShipMethod/UPS 2nd Day Air',
										'Code/ShipMethod/UPS Ground','Code/ShipMethod/FEDEX')
										)
		BEGIN

										
			--Calculate number of Boxes, and freight cost for order
			SELECT @iBoxes = Sum(iBoxes), @nBoxes = Sum(nBoxes),
			@nFreight = Coalesce(Sum(iBoxes * CostPerBox),0)
		
			FROM 
			(SELECT CEILING(sum((ol.qtyordered/(1.0 * pf.SalesUnitQty))/SalesUnitsPerPackingUnit))as iBoxes,
							sum((ol.qtyordered/(1.0 * pf.SalesUnitQty))/SalesUnitsPerPackingUnit)as nBoxes,
							fpz.CostPerBox ,sbpf.supplierboxguid
			From GrowerOrder o
			Join SupplierOrder so on o.Guid = so.GrowerOrderGuid
			Join Orderline ol on so.guid = ol.SupplierOrderGuid
			Join product pr on ol.ProductGuid = pr.guid
			Join productform pf on pr.Productformguid = pf.guid
			join supplierBoxProductForm sbpf on pf.guid = sbpf.ProductFormGuid
			join supplierBox sb on sbpf.SupplierBoxGuid = sb.Guid
			JOIN GrowerAddress ga on o.ShipToAddressGuid = ga.Guid
			JOIN Address a on ga.AddressGuid = a.Guid
			JOIN FreightSupplierStateZone fsz on a.StateGuid = fsz.StateGuid 
					AND so.SupplierGuid = fsz.SupplierGuid
			Join FreightProgramZoneBoxCost fpz on fpz.ZoneLookupGuid = fsz.ZoneLookupGuid 
								AND fpz.ProgramGuid = pr.ProgramGuid
								AND sbpf.SupplierBoxGuid = fpz.SupplierBoxGuid
			where so.guid = @SupplierOrderGuid
			and sb.Name not in ('Rack','Pallet')
			Group by sbpf.supplierboxguid,fpz.CostPerBox) as BoxTypeCounts

			SELECT   @bIsPartialBox = case when (@nBoxes > 0 and @nBoxes < 1) then 1 else 0 end

			SELECT @sEstimatedFreightText = 'Freight charge is estimated based on FedEx 2nd day shipments, your shipment has an estimated ' + cast (coalesce(@iBoxes,0) as varchar(5)) + ' box'
			SELECT @sEstimatedFreightText = @sEstimatedFreightText + case when coalesce(@iBoxes,0) = 1 then '' else 'es' end + '.'
			If @nBoxes <> @iBoxes
			BEGIN
				SELECT @sEstimatedFreightText = @sEstimatedFreightText + ' <BR>Your order has a partial box. (' + cast(cast(coalesce(@nBoxes,0) as float)as varchar(9)) + ' boxes)' 
			END
			SELECT @sEstimatedFreightText = @sEstimatedFreightText + ' <BR>If Priority 1 shipments are required, freight cost will increase.' 


		END

		IF @ShipMethodGuid IN (SELECT Guid FROM lookup 
									WHERE path IN('Code/ShipMethod/Pallet','Code/ShipMethod/Rack'))
										
		BEGIN							
			If @ShipMethodGuid  = (select Guid from lookup where path = 'Code/ShipMethod/Pallet')
				set  @ShipMethod = 'Pallet'
			If @ShipMethodGuid  = (select Guid from lookup where path = 'Code/ShipMethod/Rack')
				set  @ShipMethod = 'Rack'
			
			--Calculate number of Pallets or Racks, and freight cost for order
			SELECT @iBoxes = Sum(iBoxes), @nBoxes = Sum(nBoxes),
			@nFreight = Coalesce(Sum(iBoxes * CostPerBox),0)
		
			FROM 
			(SELECT		   CEILING(case when @ShipMethod = 'Pallet' 
								 then sum((ol.qtyordered/(1.0 * pf.SalesUnitQty))/ISNULL(NULLIF(SalesUnitsPerPallet,0),1))
								 else sum((ol.qtyordered/(1.0 * pf.SalesUnitQty))/ISNULL(NULLIF(SalesUnitsPerRack,0),1))
								 end
								 )
								 as iBoxes,
							case when @ShipMethod = 'Pallet' 
								 then sum((ol.qtyordered/(1.0 * pf.SalesUnitQty))/ISNULL(NULLIF(SalesUnitsPerPallet,0),1))
								 else sum((ol.qtyordered/(1.0 * pf.SalesUnitQty))/ISNULL(NULLIF(SalesUnitsPerRack,0),1))
								 end							
								as nBoxes,
							fpz.CostPerBox ,sb.guid
			From GrowerOrder o
			Join SupplierOrder so on o.Guid = so.GrowerOrderGuid
			Join Orderline ol on so.guid = ol.SupplierOrderGuid
			Join product pr on ol.ProductGuid = pr.guid
			Join productform pf on pr.Productformguid = pf.guid
			Join supplierBox sb on so.SupplierGuid = sb.SupplierGuid and sb.Name = @ShipMethod
				--join supplierBoxProductForm sbpf on pf.guid = sbpf.ProductFormGuid
			JOIN GrowerAddress ga on o.ShipToAddressGuid = ga.Guid
			JOIN Address a on ga.AddressGuid = a.Guid
			JOIN FreightSupplierStateZone fsz on a.StateGuid = fsz.StateGuid 
					AND so.SupplierGuid = fsz.SupplierGuid
			Join FreightProgramZoneBoxCost fpz on fpz.ZoneLookupGuid = fsz.ZoneLookupGuid 
								AND fpz.ProgramGuid = pr.ProgramGuid
								AND sb.Guid = fpz.SupplierBoxGuid
			where so.guid = @SupplierOrderGuid
			
			Group by sb.Guid,fpz.CostPerBox) as BoxTypeCounts

			SELECT   @bIsPartialBox = case when (@nBoxes > 0 and @nBoxes < 1) then 1 else 0 end

			SELECT @sEstimatedFreightText = 'Your shipment has an estimated ' + cast (coalesce(@iBoxes,0) as varchar(5)) + ' ' + @ShipMethod
			SELECT @sEstimatedFreightText = @sEstimatedFreightText + case when coalesce(@iBoxes,0) = 1 then '' else 's' end + '.'
			IF @nBoxes <> @iBoxes
			BEGIN
				SELECT @sEstimatedFreightText = @sEstimatedFreightText + ' <BR>Your order has a partial ' + @ShipMethod + '. (' + CAST(CAST(COALESCE(@nBoxes,0) AS FLOAT)AS VARCHAR(9)) + ' ' + @ShipMethod + 's)' 
			END


		END

		
	END
	
		--print 'Got Here 4' 		
	-------Do Dummen Freight and Tags
	
	If @SellerCode = 'DMO' and @bGrowerUsesDummenPriceMethod = 1
	BEGIN
		--print 'Got Here 4.1' 	
	
		SELECT 		
			@nFreight =  Sum(gpp.FreightCost * ol.QtyOrdered)
			,@nTagPrice =  Sum(gpp.TagCost * ol.QtyOrdered)
			,@iQty = Sum(ol.QtyOrdered)
		FROM Orderline ol
		JOIN SupplierOrder so on ol.SupplierOrderGuid = so.Guid
		JOIN GrowerOrder o on so.GrowerOrderGuid = o.Guid
		JOIN shipWeek sw on o.shipweekGuid = sw.guid
		Join product pr on ol.ProductGuid = pr.guid
		JOIN ProgramSeason ps on ps.Programguid = pr.ProgramGuid
			AND sw.MondayDate between ps.StartDate and ps.EndDate 
		JOIN GrowerProductProgramSeasonPrice gpp on gpp.ProductGuid = pr.Guid
			AND gpp.GrowerGuid = o.GrowerGuid
			AND gpp.ProgramSeasonGuid = ps.Guid
		WHERE so.guid = @SupplierOrderGuid
	
	
	
		If @ShipMethodGuid in (select Guid from lookup 
										where path in('Code/ShipMethod/AIR','Code/ShipMethod/UPS 2nd Day Air',
											'Code/ShipMethod/UPS Ground','Code/ShipMethod/FEDEX',
											'Code/ShipMethod/Pallet','Code/ShipMethod/Rack')
										)
		BEGIN
		
		
		
			Exec SupplierOrderAddUpdateFee 
				'00000000-0000-0000-0000-000000000000',
				@SupplierOrderGuid,
				'Freight',
				1,
				@nFreight,
				'Estimated',
				'Freight based on Quoted freight price',
				0
		END

		IF @TagRatioCode IN ('TagInc','1To1','2To1','3To1','4To1','5To1','6To1','7To1','8To1','9To1')
		BEGIN
			If @TagRatioCode <> 'TagInc'
			BEGIN
				SELECT @iTagRatio = substring(@TagRatioCode,1,1)
			END
			ELSE
			BEGIN
				SELECT @iTagRatio = 1
			END

			set @FeeAmount = coalesce(@nTagPrice,0)
			set @FeeDescription = 'Tag Cost based on Quoted tag price'
			--set @FeeDescription = 'Number of Tag Bundles is ' + cast(@iTagBundles as varchar(5))
					
			EXEC SupplierOrderAddUpdateFee 
				'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
				'Tags',
				1,
				@FeeAmount,
				'Estimated',
				@FeeDescription,
				0	
	
		END


	END
	
	--print 'Got Here 5' 
		
	If @SupplierCode in('ATH','BEE','BUR','CEN','EUR','GED','GRL','GNS','GUL','HMA'
						,'KIE','KNO','MIC','PEA','PSI','RAK','ROS','SUN','TER','WEN','VMX'
						)
	BEGIN
	   Select 
		@iTrays = Sum(ol.QtyOrdered/pf.SalesUnitQty),
		@nPrice = Sum(ol.QtyOrdered * ol.ActualPrice),
		@iQty = Sum(ol.QtyOrdered)
		FROM SupplierOrder so 
		Join Orderline ol on so.guid = ol.SupplierOrderGuid
		Join product pr on ol.ProductGuid = pr.guid
		Join productform pf on pr.Productformguid = pf.guid
				
		WHERE so.guid = @SupplierOrderGuid
	END
-----------------Do Freight Estimate not Canada
If @bFreightCalcStandard = 1 
	OR @ProgramCode in ('TERRCA','TERRCPER','EURLNRSHR','EURRCA','EURRCG','EURRCPER','EURRCSUC',
						'EURRRA','EURRRG','EURRRPER','EURRRSHR','EURRRSUC')
BEGIN
	If @CountryCode = 'CA'
		BEGIN
			
			Exec SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',
			@SupplierOrderGuid,
			'Freight',
			1,
			0,
			'TBD',
			'Freight to Canada will be determined at invoice time.',
			0
			
		END
	ELSE
		BEGIN
			IF @ShipMethodGuid IN (SELECT Guid FROM lookup 
									WHERE path IN('Code/ShipMethod/AIR','Code/ShipMethod/UPS 2nd Day Air',
										'Code/ShipMethod/UPS Ground','Code/ShipMethod/FEDEX',
										'Code/ShipMethod/Pallet','Code/ShipMethod/Rack')
									)
			BEGIN
				
				IF @nFreight = 0

					Exec SupplierOrderAddUpdateFee 
					'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
					'Freight',
					1,
					@nFreight,
					'TBD',
					@sEstimatedFreightText,
					0
				ELSE
					EXEC SupplierOrderAddUpdateFee 
					'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
					'Freight',
					1,
					@nFreight,
					'Estimated',
					@sEstimatedFreightText,
					0
			END
		END

END
--PRINT 'gothere 6'
-----------------Do Freight from Price-------------
If @bFreightCalcFromPrice = 1 and @bGrowerUsesDummenPriceMethod = 0
BEGIN
	
	
	;WITH baseLevel (ProgramSeasonGuid,VolumeLevelGuid)
	AS 
		(SELECT ps.Guid as ProgramSeasonGuid,vl.Guid as BaseVolumeLevelGuid
		FROM ProgramSeason ps
		Join VolumeLevel vl on ps.Guid = vl.ProgramSeasonGuid and vl.LevelNumber = 1

	)                
	SELECT @nFreight = sum (ISNULL(pr.freightcost * ol.QtyOrdered,0))
		
	FROM
	GrowerOrder o
	Join SupplierOrder so on so.GrowerORderGuid = o.Guid
	Join Grower g on o.GrowerGuid = g.Guid
	join ShipWeek sw on o.ShipWeekGuid =sw.guid
	Join Orderline ol on so.guid = ol.SupplierOrderGuid
	Join Product p on ol.ProductGuid = p.guid

	Join ProgramSeason ps on p.ProgramGuid = ps.ProgramGuid			
		AND sw.MondayDate BETWEEN ps.StartDate and ps.EndDate
	Join ProductPriceGroupProgramSeason pgps ON pgps.ProgramSeasonGuid = ps.Guid
		AND pgps.ProductGuid = p.Guid
	left Join GrowerVolume gv on gv.ProgramSeasonGuid = ps.Guid
		and gv.GrowerGuid = g.Guid
	Join baseLevel on baseLevel.ProgramSeasonGuid = ps.Guid
	Join VolumeLevel vl on 
			coalesce(gv.QuotedVolumeLevelGuid,gv.ActualVolumeLevelGuid,
						gv.EstimatedVolumeLevelGuid,baseLevel.VolumeLevelGuid)
					= vl.Guid
	Join Price pr on pgps.PriceGroupGuid = pr.PriceGroupGuid
		and pr.VolumeLevelGuid = vl.Guid
	WHERE so.guid = @SupplierOrderGuid
	
	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Freight',
	1,
	@nFreight,
	'Estimated',
	@sEstimatedFreightText,
	0


END

--print 'Got Here 6' 

------------Freight TBD------------
If @SupplierCode in('BUR','BEE','FOR','HIQ','LPO','LIN','PAC')
			OR @ProgramCode IN ('EURURCA','EURURCPER','EURURCSUC')  --Euro Amerocan URC are TBD
BEGIN
	
	Exec SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Freight',
		1,
		0,
		'TBD',
		'Freight is pre-pay and add to the invoice.',
		0
	
END

-----------------Do Tags
If @bTagsCalcStandard = 1 
BEGIN
	If @TagRatioCode in ('TagInc','1To1','2To1','3To1','4To1','5To1','6To1','7To1','8To1','9To1')
	BEGIN
		If @TagRatioCode <> 'TagInc'
			SELECT @iTagRatio = substring(@TagRatioCode,1,1)
		ELSE
			SELECT @iTagRatio = 1
				
		--Calculate tag price and tag bundles 
					
		select @nTagPrice = 0,@iTagBundles = 0
		Select 
		@iTagBundles = Sum(Case when pf.TagBundlesPerSalesUnit = 0 then Ceiling((cast(ol.QtyOrdered as float)/@iTagRatio)/pf.TagBundleQty) 
								else pf.TagBundlesPerSalesUnit * (cast(ol.QtyOrdered as float)/pf.SalesUnitQty) end),
		@nTagPrice = Sum(Case when pf.TagBundlesPerSalesUnit = 0 then Ceiling((cast(ol.QtyOrdered as float)/@iTagRatio)/pf.TagBundleQty) * pf.TagBundleQty * pg.tagcost
								else pf.TagBundlesPerSalesUnit * (cast(ol.QtyOrdered as float)/pf.SalesUnitQty) * pf.TagBundleQty * pg.tagcost end)
		-- Sum(ol.QtyOrdered)
		--ol.ID,ol.QtyOrdered,ol.ActualPrice,pf.SalesUnitQty
		--,pg.tagcost,pf.TagBundleQty,
		--Ceiling(ol.QtyOrdered/pf.TagBundleQty) as iBundles,
		--Ceiling(ol.QtyOrdered/pf.TagBundleQty) * pf.TagBundleQty * pg.tagcost as nTagCost
		FROM SupplierOrder so 
		Join GrowerOrder o on o.guid = so.GrowerOrderGuid
		Join Orderline ol on so.guid = ol.SupplierOrderGuid
		Join product pr on ol.ProductGuid = pr.guid
		Join productform pf on pr.Productformguid = pf.guid
		Join shipweek sw on o.shipWeekguid = sw.guid
		Join ProgramSeason ps on ps.programguid = pr.programguid 
			and sw.MondayDate between ps.StartDate and ps.EndDate
		Join ProductPriceGroupProgramSeason ppgps on pr.Guid = ppgps.ProductGuid
			and ppgps.ProgramSeasonGuid = ps.guid
		Join PriceGroup pg on ppgps.pricegroupguid = pg.guid			
		WHERE so.guid = @SupplierOrderGuid

			
		IF @SupplierCode = 'EUR'
			BEGIN
				set @FeeAmount = coalesce(@nTagPrice,0)
				set @FeeDescription = 'Number of Tag Bundles is ' + cast(@iTagBundles as varchar(5))
					
				EXEC SupplierOrderAddUpdateFee 
				'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
				'RMT',
				1,
				@FeeAmount,
				'Estimated',
				@FeeDescription,
				0
					
				--select * from lookup where path  like 'Code/SupplierOrderFeeType%'
			END
		ELSE
			BEGIN
				set @FeeAmount = coalesce(@nTagPrice,0)
				set @FeeDescription = 'Number of Tag Bundles is ' + cast(@iTagBundles as varchar(5))
					
				EXEC SupplierOrderAddUpdateFee 
				'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
				'Tags',
				1,
				@FeeAmount,
				'Estimated',
				@FeeDescription,
				0				
					
			END
	END
END

-----------Dummen specific fees-----
If @SupplierCode in ('DES','DET')
BEGIN
	--Premium Ice Crystal
	Declare @bHasIceCrystal bit
	set @bHasIceCrystal = (select case when count(*) > 0 then 1 else 0 end
						   from OrderLineViewShowInactiveProduct
						   Where SupplierOrderGuid = @SupplierOrderGuid
						   and VarietyCode = 'D1195')
	IF @bHasIceCrystal  = 1
	BEGIN
		set @FeeAmount = (Select QtyOrdered * 0.1 
							from OrderLineViewShowInactiveProduct
						   Where SupplierOrderGuid = @SupplierOrderGuid
						   and VarietyCode = 'D1195' )
		set @FeeDescription = 'Marketing fee of 0.1 per cutting for Premium Ice Crystal.'
		
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'MarketingFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END
	--select * from lookup where path like 'Code/SupplierOrderFeeType%' order by Path

END

  -- print 'Got Here 7'                       

-----------Plug Connection-----
If @SupplierCode = 'PLU'
BEGIN
	--freight Standard	
		
	--Partial Box
	If @bIsPartialBox = 1 --New rule only applies if less than one full box
	BEGIN
		set @FeeAmount = 20
		set @FeeDescription = 'Your order has a partial box. (' + cast(cast(@nBoxes as float)as varchar(9)) + ' boxes)'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'PartialBoxFee',
		1,
		@FeeAmount,
		'Estimated',
		@FeeDescription,
		0	
		
	END
	--Winter Box
	IF @StateCode IN('AK','CO','CT','DE','ID','IL','IN','IA','KS','KY',
						'ME','MD','MA','MI','MN','MO','MT','NE','NH','NJ',
						'NY','ND','OH','PA','RI','UT','WV','WY')  
			AND @CountryCode = 'US'
			AND (@iWeek >= 40 OR @iWeek <= 12)
	BEGIN
		
		set @FeeAmount = @iBoxes * 8
		set @FeeDescription = 'Week 40 through 12 requires a Winter Box in your state ($8/box)'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'WinterBoxFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0			
	END
END

-----------Welby
If @SupplierCode = 'WEL'
BEGIN
	--freight Standard

	--Boxing
	set @FeeAmount = Coalesce(@iBoxes * 7.00,0)
	set @FeeDescription = @sBoxingText

	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0	
		
	
		
END
-----------Micandy
If @SupplierCode = 'MIC'
BEGIN
	--Freight Standard
	--Tags Standard
	--Boxing Fee
	set @FeeAmount = 0
	set @FeeDescription = @sBoxingText

	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'TBD',
	@FeeDescription,
	0	
		
END

-----------Casa Flora Florida
If @SupplierCode = 'CFF'
BEGIN
			
	--freight Standard
		
	--Large Order Discount
	Select 
	@iTrays = Sum(ol.QtyOrdered/pf.SalesUnitQty),
	@nPrice = Sum(ol.QtyOrdered * ol.ActualPrice),
	@iQty = Sum(ol.QtyOrdered)
	FROM SupplierOrder so 
	Join Orderline ol on so.guid = ol.SupplierOrderGuid
	Join product pr on ol.ProductGuid = pr.guid
	Join productform pf on pr.Productformguid = pf.guid		
	WHERE so.guid = @SupplierOrderGuid
	AND pf.Code in ('CFF_TCL_PER_072',	'CFF_TCL_TRO_072')

	IF @iTrays >=20 
	BEGIN
		set @FeeAmount = @iQty * -.02
		set @FeeDescription = 'Discount is $.02 per cell'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Large Order Discount',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
		
	END
END
-----------Casa Flora Texas
If @SupplierCode = 'CFT'
BEGIN
			
--freight Standard

--Large Order Discount
	Select 
	@iTrays = Sum(ol.QtyOrdered/pf.SalesUnitQty),
	@nPrice = Sum(ol.QtyOrdered * ol.ActualPrice),
	@iQty = Sum(ol.QtyOrdered)
	FROM SupplierOrder so 
	Join Orderline ol on so.guid = ol.SupplierOrderGuid
	Join product pr on ol.ProductGuid = pr.guid
	Join productform pf on pr.Productformguid = pf.guid		
	WHERE so.guid = @SupplierOrderGuid
	AND pf.Code in ('CFT_TCL_PER_072',	'CFT_TCL_TRO_072')

	IF @iTrays >=20 
	BEGIN
		set @FeeAmount = @iQty * -.02
		set @FeeDescription = 'Discount is $.02 per cell'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Large Order Discount',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	
	END
END
-----------Gulley
If @SupplierCode = 'GUL'
BEGIN
			
	--freight Standard
	-- Tags Standard

	--Boxing Fee
	set @FeeAmount = Coalesce(@iTrays * 1.90,0)
	set @FeeDescription = @sBoxingText

	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0	
END	
-----------Sunbelt
If @SupplierCode = 'SUN'
BEGIN
	--freight Standard
	--Tags Standard

	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 8.50,0)
	set @FeeDescription = @sBoxingText

	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0	
	
END
-----------Wenke
If @SupplierCode = 'WEN'
BEGIN
			
	--freight Standard
	--Tags Standard

	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 8.50,0)
	set @FeeDescription = @sBoxingText

	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0	
	
END

-----------Euro American
If @SupplierCode = 'EUR'
BEGIN
	--Minimum for URC Annuals and Succulents
	If  @ProgramCode in ('EURLNRSHR','EURRCA','EURRCG','EURRCPER','EURRCSUC')
	BEGIN
		if @iTrays <  8
		BEGIN
			set @FeeAmount = 20
			set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iTrays as varchar(6)) + ' trays and the minimum is 8 trays.'

			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'BelowMinFee',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0	
		END
	END
		
	--Minimum for URC Annuals and Succulents
	IF  @ProgramCode IN ('EURURCA','EURURCSUC','EURURCPER')
	BEGIN
	

		--Below Min for URC
		if @iQty <  3000
		BEGIN
			set @FeeAmount = 20
			set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 3000 cuttings.'

			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'BelowMinFee',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0	
				
		END

		Declare @iOsteo AS int
		SET @iOsteo = (Select Sum(QtyOrdered) 
		from OrderLine ol
		Join product p on ol.ProductGuid = p.guid
		Join Variety v on p.varietyguid = v.guid
		Join species sp on v.SpeciesGuid  = sp.guid
		Where Ol.SupplierOrderGuid = @SupplierOrderGuid
		and sp.code = 'OST'
		)
			 
		IF @iOsteo >  0
		BEGIN
			set @FeeAmount = 75
			set @FeeDescription = 'A Handling Fee of $75 is assessed for all Osteospermum orders.'

			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'ClearanceHandling',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0	
				
		END
	END
END
-----------Terra Nova
If @SupplierCode = 'TER'
BEGIN
		--select * from lookup where path like 'Code/SupplierOrderFeeType%'
	If @CountryCode = 'CA'
	BEGIN
		set @FeeAmount = 185
		set @FeeDescription = 'Import Fee'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'ImportFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END
		
	--Freight for URC (RC above)
	IF  @ProgramCode IN ('TERURCA','TERURCPER','MULTIPLE')
	BEGIN
		set @FeeAmount = 0
		set @FeeDescription = 'Freight will be determined at invoice time.'

		Exec SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Freight',
		1,
		@FeeAmount,
		'TBD',
		@FeeDescription,
		0	
		
		--Below Min for URC
		IF @iQty <  3500
		BEGIN
			set @FeeAmount = 35
			set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 3500 cuttings.'

			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'BelowMinFee',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0	
		END
	END

END
	--freight Standard for Rooted
	-- Tags Standard
-----------Agri-Starts
If @SupplierCode = 'AGR'
BEGIN
			
	--freight standard
	-- Tags Standard
	If @CountryCode = 'CA'
	BEGIN
		--Phyto fee
		set @FeeAmount = 65
		set @FeeDescription = 'Phyto charge is added to each order.'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Phyto',
		1,
		@FeeAmount,
		'TBD',
		@FeeDescription,
		0	
	END
END
	
-----------Headstart-----	
--If @SupplierCode ='HDS'
--BEGIN
	--freight standard
		
--END

-----------Peace Tree-----	
If @SupplierCode ='PEA'
BEGIN
	--freight standard
	--tags standard
	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 9.00,0)
	set @FeeDescription = @sBoxingText

	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0	
		
	--Below Minimum
	--PEACOMHOC,PEALINE,PEALINPER,PEARRE
	If  (@ProgramCode in ('PEACOMHOC','PEALINE','PEALINPER'))

		AND (@iQty < 500)
	BEGIN
		set @FeeAmount = 50
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 500.'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END
		
	IF @CountryCode = 'CA'
	BEGIN
		--Phyto fee
		set @FeeAmount = 50
		set @FeeDescription = 'Phyto charge is added to each order shipped to Canada.'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Phyto',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END

	IF @ProgramCode = 'PEACOMHOC'
	BEGIN
		--MAD fee
		set @FeeAmount = @iQty * .06
		set @FeeDescription = 'Marketing Fee is added to Hort Couture items'

		Exec SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'HC MAD Fee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
		
		--Pot fee
		set @FeeAmount = @iQty * .13
		set @FeeDescription = 'Marketing Fee is added to Hort Couture items'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'HC Pots',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
			
	END
END
-----------Grolink-----
If @SupplierCode in('GRL','GED')
BEGIN
	--Boxing Fee
	set @FeeAmount = @iBoxes * 4.50
	set @FeeDescription = @sBoxingText

	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0	
	
	--freight for Rooted
	If @CountryCode = 'CA'
	BEGIN
		set @FeeAmount = 0
		set @FeeDescription = 'Freight to Canada will be determined at invoice time.'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Freight',
		1,
		@FeeAmount,
		'TBD',
		@FeeDescription,
		0	
	END
	ELSE
	BEGIN
		IF @ShipMethodGuid IN (SELECT Guid FROM lookup 
								WHERE path IN('Code/ShipMethod/AIR','Code/ShipMethod/UPS 2nd Day Air',
									'Code/ShipMethod/UPS Ground','Code/ShipMethod/FEDEX')
								)
		BEGIN
			

			If @ProgramCode in ('GRLURCM')
			BEGIN
				set @FeeAmount = 0
				set @FeeDescription = 'Freight cost is included in the listed price of the product.'

				EXEC SupplierOrderAddUpdateFee 
				'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
				'Freight',
				1,
				@FeeAmount,
				'Included',
				@FeeDescription,
				0	
			END
			If @ProgramCode in ('GRLRCM')
			BEGIN
				set @FeeAmount = coalesce(@nFreight,0)
				set @FeeDescription = @sEstimatedFreightText

				EXEC SupplierOrderAddUpdateFee 
				'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
				'Freight',
				1,
				@FeeAmount,
				'Estimated',
				@FeeDescription,
				0	
			END
			IF @ProgramCode IN ('MULTIPLE')
			BEGIN
				set @FeeAmount = 0
				set @FeeDescription = 'Freight will be determined at invoice time.'

				EXEC SupplierOrderAddUpdateFee 
				'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
				'Freight',
				1,
				@FeeAmount,
				'TBD',
				@FeeDescription,
				0	
				
			END
		END
	END
		
	IF (@ProgramCode  IN ('GRLRCM','MULTIPLE') AND @iQty < 500) OR
		    (@ProgramCode  = 'GRLURCM' AND @iQty < 1000)
	BEGIN
		set @FeeAmount = 20
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 500 (Rooted) or 1000 (Unrooted).'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END

END
------------Kientzler
If @SupplierCode ='KIE'
BEGIN
	--Freight
	set @FeeAmount = 0
	set @FeeDescription = 'Freight cost is included in the listed price of the product.'

	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Freight',
	1,
	@FeeAmount,
	'Included',
	@FeeDescription,
	0	
		
	--Below Minimum
	IF @iQty < 2000
	BEGIN
		set @FeeAmount = 50
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 2000.'

		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END
END
-----------Lucas-----
If @SupplierCode ='LUC'
BEGIN
		--freight standard
		
		--Boxing Fee
		If @ProgramCode in ('MULTIPLE')
		BEGIN
			set @FeeAmount = 0
			set @FeeDescription = 'Boxing Charge is to be determined'
			
			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'Boxing',
			1,
			@FeeAmount,
			'TBD',
			@FeeDescription,
			0	
			
		END
		If @ProgramCode in ('LUCRCA','LUCRCP','LUCRCM')
		BEGIN
			set @FeeAmount = Coalesce(@iBoxes * 4.00,0)
			set @FeeDescription = @sBoxingText
			
			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'Boxing',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0	
			
		END
		IF @ProgramCode = 'LUCRCG'
		BEGIN
			set @FeeAmount = Coalesce(@iBoxes * 6.12,0)
			set @FeeDescription = @sBoxingText
			
			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'Boxing',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0	
		END
END

-----------Millstadt-----
If @SupplierCode ='MIL'




BEGIN
	--freight standard
		
	--Boxing Fee
	If @ProgramCode = 'MILRCPOI'
		BEGIN
			set @FeeAmount = Coalesce(@iBoxes * 7.00,0)	
		END
	ELSE
		BEGIN
			SET @FeeAmount = COALESCE(@iBoxes * 6.00,0)
		END
	set @FeeDescription = @sBoxingText
			
	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0
END

-----------Malmborg-----
If @SupplierCode ='MLM'
BEGIN
	--freight standard
	--Tags Standard

	--Winter Box
	If (@iWeek >= 1 and @iWeek <= 13)
	BEGIN
		set @FeeAmount = @iBoxes * 4.00
		set @FeeDescription = 'Week 1 through 13 requires a Winter Box.  Your Order has ' + cast (@iBoxes as varchar(5)) + ' boxes.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'WinterBoxFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END
				
	--Order Minuimum
	--2 Boxes - a $20.00 charge for orders under minimum - minimum 26 strip 400 plants - minimum 50 tray 200 plants
	IF @nBoxes < 2
	BEGIN
		set @FeeAmount = 20
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (cast(@nBoxes as float) as varchar(6)) + ' boxes and the minimum is 2 boxes.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END
END
-----------Green Meadow-----
If @SupplierCode ='GRE'
BEGIN
	--freight standard

	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 5.00,0)
	set @FeeDescription = @sBoxingText
			
	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0
END
-----------Center-----
If @SupplierCode ='CEN'
BEGIN	
	--freight standard
	--tags standard		

	--below minimum
	If @iTrays < 4
	BEGIN
		set @FeeAmount = @nPrice * .10
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iTrays as varchar(6)) + ' trays and the minimum is 4.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END

	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 6.80,0)
	set @FeeDescription = @sBoxingText
			
	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0
END	
-----------Gro N Sell-----
If @SupplierCode ='GNS'
BEGIN
	--Freight Standard
	--Tags Standard
		
	--Boxing Fee
	--Hard coded to eliminat the Pallet sampler from the box count for the fee
	If exists (select 1 from 
				OrderLine ol
				Join Product p on ol.ProductGuid = p.guid
				Join ProductForm pf on p.productformGuid = pf.guid
				where ol.SupplierOrderGuid = @SupplierOrderGuid 
				and pf.code = 'GNS_RC_V_BBS')
	BEGIN
		set @iBoxes = @iBoxes - (select Sum(QtyOrdered) from 
				OrderLine ol
				Join Product p on ol.ProductGuid = p.guid
				Join ProductForm pf on p.productformGuid = pf.guid
				where ol.SupplierOrderGuid = @SupplierOrderGuid 
				and pf.code = 'GNS_RC_V_BBS')
	END


	if @iBoxes > 0 
	BEGIN
		set @FeeAmount = Coalesce(@iBoxes * 1.70,0)
		set @FeeDescription = @sBoxingText
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Boxing',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END	
	--Below minimum
	IF @iTrays < 6 AND 
				NOT EXISTS(SELECT 1 FROM 
							OrderLine ol
							JOIN Product p ON ol.ProductGuid = p.guid
							JOIN ProductForm pf ON p.productformGuid = pf.guid
							WHERE ol.SupplierOrderGuid = @SupplierOrderGuid 
							AND pf.code = 'GNS_RC_V_BBS'
							)
	BEGIN
		set @FeeAmount = 20
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iTrays as varchar(6)) + ' trays and the minimum is 6 trays.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END
END

-----------PSI-----
If @SupplierCode ='PSI'
BEGIN
		
	--Freight
	set @FeeAmount = 0
	set @FeeDescription = 'Freight cost is included in the listed price of the product.'
			
	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Freight',
	1,
	@FeeAmount,
	'Included',
	@FeeDescription,
	0
		
	--Below Minum
	IF @iQty <  2000
	BEGIN
		set @FeeAmount = 50
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 2000 cuttings.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END
END

-----------Duwayne's-----
If @SupplierCode ='DGI'
BEGIN
			
	--freight standard
	--tags standard

	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 8.00,0)
	set @FeeDescription = @sBoxingText
			
	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0

END
-----------Knox-----
If @SupplierCode ='KNO'
BEGIN	
	--freight standard
	--tags standard
		
	--Below Minimum
	--$10.00   Seed min = 4 trays  
	--Garden Mums and Aster min = 8 trays 
	--Veg and Poin min = 4 trays
	If (@ProgramCode in ('KNORCM') and @iTrays < 8)
		or (@ProgramCode in ('KNORCA','KNORCPOI','KNOLINA','KNOLINVH','KNOLINPER','KNOLINPP','MULTIPLE') and @iTrays < 4)
	BEGIN
		set @FeeAmount = 10
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iTrays as varchar(6)) + ' trays and the minimum is 8 trays.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END
		
	--Boxing for KNO_RC_A_GER_050, KNO_RC_A_GER_072
	--this first query is to figure out how many boxes of this form
	Declare @iGerberaBoxes int
	SELECT @iGerberaBoxes = Sum(iGerberaBoxes)
	FROM 
	(SELECT CEILING(sum((ol.qtyordered/(1.0 * pf.SalesUnitQty))/SalesUnitsPerPackingUnit))as iGerberaBoxes
	From GrowerOrder o
	Join SupplierOrder so on o.Guid = so.GrowerOrderGuid
	Join Orderline ol on so.guid = ol.SupplierOrderGuid
	Join product pr on ol.ProductGuid = pr.guid
	Join productform pf on pr.Productformguid = pf.guid
	join supplierBoxProductForm sbpf on pf.guid = sbpf.ProductFormGuid
	JOIN GrowerAddress ga on o.ShipToAddressGuid = ga.Guid
	JOIN Address a on ga.AddressGuid = a.Guid
	JOIN FreightSupplierStateZone fsz on a.StateGuid = fsz.StateGuid 
			AND so.SupplierGuid = fsz.SupplierGuid
	Join FreightProgramZoneBoxCost fpz on fpz.ZoneLookupGuid = fsz.ZoneLookupGuid 
						AND fpz.ProgramGuid = pr.ProgramGuid
						AND sbpf.SupplierBoxGuid = fpz.SupplierBoxGuid
	where so.guid = @SupplierOrderGuid
	and pf.code in ('KNO_RC_A_GER_050','KNO_RC_A_GER_072')
	Group by sbpf.supplierboxguid,fpz.CostPerBox) as BoxTypeCounts

	IF @iGerberaBoxes > 0 
	BEGIN
		set @FeeAmount = Coalesce(@iGerberaBoxes * 5,0)
		set @FeeDescription = @sBoxingText
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Boxing',
		1,
		@FeeAmount,
		'Estimated',
		@FeeDescription,
		0
	END
END -- Knox
	
	
--------Vivero-----------
If @SupplierCode ='VMX'
BEGIN
	--Freight
	set @FeeAmount = 0
	set @FeeDescription = 'Freight cost is included in the listed price of the product.'
--	PRINT 'in vmx 1'		
	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Freight',
	1,
	@FeeAmount,
	'Included',
	@FeeDescription,
	0
	
--	PRINT 'in vmx 2'
	--Phyto fee
	set @FeeAmount = 40
	set @FeeDescription = 'Phyto charge is added to each order.'
			
	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Phyto',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0
--	PRINT 'in vmx 3'
	--Below Minimum
	IF @iQty < 2000
	BEGIN
		set @FeeAmount = 35
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 2000.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END

END
--print 'Got Here 8' 
--------Raker-----------------
If @SupplierCode ='RAK'
BEGIN
	IF @ShipMethodGuid in (select Guid from lookup 
								where path in('Code/ShipMethod/AIR','Code/ShipMethod/UPS 2nd Day Air',
									'Code/ShipMethod/UPS Ground','Code/ShipMethod/FEDEX')
								)
		BEGIN
			--PRINT 'Got Here 8.1' 
			set @FeeAmount = 0
			set @FeeDescription = 'Freight cost is included in the listed price of the product.'
			
			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'Freight',
			1,
			@FeeAmount,
			'Included',
			@FeeDescription,
			0
		END
	ELSE
		BEGIN
		--	PRINT 'Got Here 8.2' 
			set @FeeAmount = 0
			set @FeeDescription = 'Freight will be determined at invoice time.'
			
			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'Freight',
			1,
			@FeeAmount,
			'TBD',
			@FeeDescription,
			0
		END
		
	--PRINT 'Got Here 8.3' 
	If @iTrays < 16
	BEGIN
		--PRINT 'Got Here 8.4' 
		set @FeeAmount = 15
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iTrays as varchar(6)) + ' trays and the minimum is 16 trays.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
		
	END
	--PRINT 'Got Here 8.5' 
	IF @ProgramCode = 'RAKCOMHOC'
	BEGIN
		--MAD fee
		set @FeeAmount = @iQty * .06
		set @FeeDescription = 'Marketing Fee is added to Hort Couture items'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'HC MAD Fee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
		
	END
	--PRINT 'Got Here 8.6' 

END
--HMA------------------
If @SupplierCode ='HMA'
BEGIN		
	set @FeeAmount = 0
	set @FeeDescription = 'Freight cost is included in the listed price of the product.'
			
	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Freight',
	1,
	@FeeAmount,
	'Included',
	@FeeDescription,
	0
	
	
	
	--Below Minimum
	IF @CountryCode = 'CA'
	BEGIN
		If @iQty < 3500
		BEGIN
			set @FeeAmount = 65
			set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 3500 cuttings.'
			
			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'BelowMinFee',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0
		END
	END
	ELSE
	BEGIN
	IF @iQty < 2500
		BEGIN
			set @FeeAmount = 65
			set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 2500 cuttings.'
			
			EXEC SupplierOrderAddUpdateFee 
			'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
			'BelowMinFee',
			1,
			@FeeAmount,
			'Actual',
			@FeeDescription,
			0
		END
	END
		
END

------Burnaby Lake---------------
If @SupplierCode ='BUR'
BEGIN
	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 6.50,0)
	set @FeeDescription = @sBoxingText
			
	Exec SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0
	
	IF @iQty >= 5000 
	BEGIN
		set @FeeAmount = case 
							when @iQty < 10000 then @iQty * -.008 
							when @iQty < 20000 then @iQty * -.015 
							when @iQty < 40000 then @iQty * -.023 
							else @iQty * -.030 
						end
		set @FeeDescription ='Discount on order because Qty ordered is more than 5,000' 
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Large Order Discount',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END
END

------High Q------------------
If @SupplierCode ='HIQ'
BEGIN
	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 4.00,0)
	set @FeeDescription = @sBoxingText
			
	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0
END

--------Athena-------------------
If @SupplierCode ='ATH'
BEGIN
	--Freight
	If @CountryCode = 'CA'
	BEGIN
		set @FeeAmount = 0
		set @FeeDescription = 'Freight to Canada will be determined at invoice time.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Freight',
		1,
		@FeeAmount,
		'TBD',
		@FeeDescription,
		0
	END
	ELSE
	BEGIN
		set @FeeAmount = 0
		set @FeeDescription = 'Freight cost is included in the listed price of the product.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Freight',
		1,
		@FeeAmount,
		'Included',
		@FeeDescription,
		0
	END

	--Below Minimum
	IF @iQty <  2000
	BEGIN
		set @FeeAmount = 25
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 2000 cuttings.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END

END

-----BeekenKamp--------------
If @SupplierCode ='BEE'
BEGIN
		

	--Below Minimum
	if @iQty <  2000
	BEGIN
		set @FeeAmount = 75
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 2000 cuttings.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END

END

----Linwell------------------
	If @SupplierCode ='LIN'
	BEGIN
		--Boxing Fee
		set @FeeAmount = Coalesce(@iBoxes * 4.00,0)
		set @FeeDescription = @sBelowMinimumText + ' <BR> Your order has ' + cast (@iQty as varchar(6)) + ' cuttings and the minimum is 2000 cuttings.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'BelowMinFee',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0
	END

-----Sunshine------------------
If @SupplierCode ='SSH'
BEGIN
	If @CountryCode = 'CA'
	BEGIN
		--Phyto fee
		set @FeeAmount = 60
		set @FeeDescription = 'Phyto charge is added to each order for Canada.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Phyto',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END
END
--------Skagit----------------
If @SupplierCode ='SKA'
BEGIN
	If @CountryCode = 'CA'
	BEGIN
		--Phyto fee
		set @FeeAmount = 50
		set @FeeDescription = 'Phyto charge is added to each order for Canada.'
			
		EXEC SupplierOrderAddUpdateFee 
		'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
		'Phyto',
		1,
		@FeeAmount,
		'Actual',
		@FeeDescription,
		0	
	END
END
-----Roseville------------------
IF @SupplierCode ='ROS'
BEGIN
	--Boxing Fee
	set @FeeAmount = Coalesce(@iBoxes * 4.00,0)
	set @FeeDescription = @sBoxingText
			
	EXEC SupplierOrderAddUpdateFee 
	'00000000-0000-0000-0000-000000000000',@SupplierOrderGuid,
	'Boxing',
	1,
	@FeeAmount,
	'Actual',
	@FeeDescription,
	0
END



-------Speedling----------------------------
--If @SupplierCode ='SPE'
--BEGIN
	--freight standard
	
--END
	
		
	IF @iDebug = 1
	BEGIN	
		PRINT '@ProgramGuid = ' + CAST( @ProgramGuid AS VARCHAR(36))
		PRINT '@ProgramCode = ' + CAST( @ProgramCode AS VARCHAR(36))
		PRINT '@SupplierOrderGuid = '+CAST(  @SupplierOrderGuid AS VARCHAR(36))
		PRINT '@SupplierCode = '+ CAST(  @SupplierCode AS VARCHAR(36))
		PRINT '@nBoxes = ' +CAST(  COALESCE(@nBoxes,0) AS VARCHAR(36))
		PRINT '@iTrays = ' +CAST(   COALESCE(@iTrays,0) AS VARCHAR(36))
		PRINT '@bIsPartialBox = ' + CAST(  @bIsPartialBox AS VARCHAR(36))
		PRINT '@ShipMethodGuid = ' + CAST(  @ShipMethodGuid AS VARCHAR(36))
		PRINT '@CountryCode = ' + CAST(  @CountryCode AS VARCHAR(36))
		 SELECT  * FROM supplierorderfee WHERE supplierorderguid = @SupplierOrderGuid
	END