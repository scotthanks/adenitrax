﻿


--EXAMPLE:
--
--EXECUTE GrowerDataGet
--  @GrowerGuid='92CFACCB-5DF3-4832-8328-384B3BED6E8F'

--EXECUTE GrowerDataGet
--  @PersonGuid='026C26B3-CDF9-41DB-AE44-9F82E5164A01'

--EXECUTE GrowerDataGet
--  @UserGuid='7714B7E5-7148-404B-9E2E-93EE8B7E3B63'

CREATE procedure [dbo].[GrowerDataGet]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@PersonGuid AS UNIQUEIDENTIFIER = NULL,
	@GrowerGuid AS UNIQUEIDENTIFIER = NULL
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('PersonGuid',@PersonGuid,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	IF @GrowerGuid IS NULL AND (@PersonGuid IS NOT NULL OR @UserGuid IS NOT NULL)
		SELECT @GrowerGuid = GrowerGuid
		FROM PersonView
		WHERE
			(@PersonGuid IS NOT NULL AND PersonGuid=@PersonGuid) OR
			(@UserGuid IS NOT NULL AND UserGuid=@UserGuid)

	IF @GrowerGuid IS NOT NULL
		BEGIN
			SELECT * FROM Grower
			WHERE Guid = @GrowerGuid

			SELECT * FROM GrowerAddress
			WHERE GrowerGuid = @GrowerGuid

			SELECT * FROM Address
			WHERE Guid IN
			(
				SELECT AddressGuid
				FROM GrowerAddress
				WHERE GrowerGuid = @GrowerGuid
			)

			SELECT * FROM GrowerCreditCard
			WHERE GrowerGuid = @GrowerGuid
			AND DateDeactivated is NULL
		END

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerDataGet',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid