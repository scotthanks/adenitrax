﻿




CREATE PROCEDURE [dbo].[DummenPricingUploadJobGetMessages]
	 @JobNumber as bigint
	
	 

AS
		Select 
	
		MessageTime,
		MessageType,
		MessageState,
		Message
		
		from 
		DummenPricingUploadJobMessage
		Where DummenPricingUploadJobGuid = (select guid from DummenPricingUploadJob where ID = @JobNumber)

		ORDER BY ID desc