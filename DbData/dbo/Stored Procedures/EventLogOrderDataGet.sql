﻿--EXAMPLES:
--EXECUTE EventLogOrderDataGet @GrowerOrderGuid='FAEC8E9E-E950-4CC1-96B9-2D1FC370382F'

CREATE PROCEDURE [dbo].[EventLogOrderDataGet]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NVARCHAR(56) = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = NULL,
	@SupplierOrderGuid AS UNIQUEIDENTIFIER = NULL,
	@OrderLineGuid AS UNIQUEIDENTIFIER = NULL
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('SupplierOrderGuid',@SupplierOrderGuid,1) +
		dbo.XmlSegment('OrderLineGuid',@OrderLineGuid,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER	
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='EventLogOrderDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	SELECT *
	FROM EventLog
	WHERE Guid IN
	(
		SELECT EventLogGuid
		FROM EventLogView
		WHERE
			(@GrowerOrderGuid IS NULL OR  GrowerOrderGuid = @GrowerOrderGuid) AND
			(@SupplierOrderGuid IS NULL OR  SupplierOrderGuid = @SupplierOrderGuid) AND
			(@OrderLineGuid IS NULL OR  OrderLineGuid = @OrderLineGuid)
	)

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='EventLogOrderDataGet',
		@ObjectGuid=@OriginalEventLogGuid