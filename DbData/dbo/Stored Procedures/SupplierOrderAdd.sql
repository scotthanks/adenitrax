﻿


--EXAMPLE:

--DECLARE @GrowerOrderGuid as UNIQUEIDENTIFIER
--DECLARE @OrderTrxStatusLookupCode as NCHAR(10)
--DECLARE @SupplierOrderGuid as UNIQUEIDENTIFIER

--EXECUTE GrowerOrderGetOrAdd
--	@GrowerGuid='9CB1183C-20AC-4049-9206-25C558CA5BEF',
--	@ShipWeekCode='201320',
--	@ProductFormCategoryCode='URC',
--	@CustomerPoNo='TestPoNumber',
--	@Description='This is my order.',
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@OrderTrxStatusLookupCode=@OrderTrxStatusLookupCode OUT

--SELECT
--  @GrowerOrderGuid as GrowerOrderGuid,
--  @OrderTrxStatusLookupCode as OrderTrxStatusLookupCode

--EXECUTE SupplierOrderAdd
--	@GrowerOrderGuid=@GrowerOrderGuid,
--  @SupplierGuid='A070F7DF-A563-453E-A558-0F7524FCE705',
--	@PoNo='TestSupplierPoNumber',
--	@SupplierOrderGuid=@SupplierOrderGuid OUT

--SELECT @SupplierOrderGuid as SupplierOrderGuid

CREATE PROCEDURE [dbo].[SupplierOrderAdd]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER,
	@SupplierGuid AS UNIQUEIDENTIFIER,
	@ProgramTypeCode AS NCHAR(20),
	@ProductFormCategoryCode AS NCHAR(20),
	@PoNo AS NCHAR(30) = '',
	@SupplierOrderGuid AS UNIQUEIDENTIFIER = NULL OUT
AS
	DECLARE @VerifiedGrowerOrderGuid AS UNIQUEIDENTIFIER
	SELECT
		@VerifiedGrowerOrderGuid = GrowerOrderGuid
	FROM GrowerOrderView 
	WHERE GrowerOrderGuid = @GrowerOrderGuid

	DECLARE @DefaultTagRatioLookupGuid AS UNIQUEIDENTIFIER
	SELECT @DefaultTagRatioLookupGuid=TagRatioLookupGuid
	FROM TripleProgramTagRatioDefaultLookupView
	WHERE 
		SupplierGuid = @SupplierGuid AND
		ProgramTypeCode=@ProgramTypeCode AND
		ProductFormCategoryCode=@ProductFormCategoryCode

	DECLARE @DefaultShipMethodLookupGuid AS UNIQUEIDENTIFIER
	SELECT @DefaultShipMethodLookupGuid=ShipMethodLookupGuid
	FROM TripleProgramShipMethodDefaultLookupView
	WHERE
		SupplierGuid=@SupplierGuid AND
		ProgramTypeCode=@ProgramTypeCode AND
		ProductFormCategoryCode=@ProductFormCategoryCode
		
	IF @VerifiedGrowerOrderGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='GrowerOrderGuid is not valid',
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='SupplierOrderAdd',
				@ObjectTypeLookupCode='SupplierOrder',
				@GuidValue=@GrowerOrderGuid

			RETURN
		END

	DECLARE @VerifiedSupplierGuid AS UNIQUEIDENTIFIER
	SELECT
		@VerifiedSupplierGuid = SupplierGuid
	FROM SupplierView 
	WHERE SupplierGuid = @SupplierGuid

	IF @VerifiedSupplierGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@UserGuid=@UserGuid,
				@UserCode=@UserCode,
				@Comment='SupplierGuid is not valid',
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='SupplierOrderAdd',
				@ObjectTypeLookupCode='SupplierOrder',
				@GuidValue=@SupplierGuid

			RETURN
		END

	IF @PoNo IS NULL
		SET @PoNo = ''
	SET @PoNo = UPPER(LTRIM(RTRIM(@PoNo)))

	SET @PoNo = LTRIM(RTRIM(@PoNo))

	DECLARE @SupplierOrderNo AS NCHAR(30)
	SET @SupplierOrderNo = 'unassigned'

	DECLARE @SupplierOrderStatusLookupGuid AS UNIQUEIDENTIFIER
	SELECT @SupplierOrderStatusLookupGuid = LookupGuid
	FROM LookupView
	WHERE LookupCode='NotShipped' AND Path LIKE 'Code/SupplierOrderStatus/%'

	SET @SupplierOrderGuid = newid()

	INSERT INTO SupplierOrder
	(
		Guid,
		GrowerOrderGuid,
		SupplierGuid,
		PoNo,
		SupplierOrderNo,
		TagRatioLookupGuid,
		ShipMethodLookupGuid,
		SupplierOrderStatusLookupGuid,
		TotalCost
	)
	VALUES
	(
		@SupplierOrderGuid,
		@GrowerOrderGuid,
		@SupplierGuid,
		@PoNo,
		@SupplierOrderNo,
		@DefaultTagRatioLookupGuid,
		@DefaultShipMethodLookupGuid,
		@SupplierOrderStatusLookupGuid,
		0.00 --TotalCost
	)

	DECLARE @RowCount AS INT
	SET @RowCount = @@ROWCOUNT

	IF @@ROWCOUNT = 1
		BEGIN
			EXECUTE EventLogAdd
				@LogTypeLookupCode='RowAdd',
				@ProcessLookupCode='SupplierOrderAdd',
				@ObjectTypeLookupCode='SupplierOrder',
				@ObjectGuid=@SupplierOrderGuid,
				@UserCode=@UserCode
		END
	ELSE
		BEGIN
			EXECUTE EventLogAdd
				@Comment='@@ROWCOUNT not 1 after attempted write (See IntValue).',
				@LogTypeLookupCode='DatabaseError',
				@ProcessLookupCode='SupplierOrderAdd',
				@ObjectTypeLookupCode='SupplierOrder',
				@IntValue=@RowCount,
				@UserCode=@UserCode

			SET @SupplierOrderGuid = NULL
		END





	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='SupplierOrderAdd',
		@ObjectTypeLookupCode='SupplierOrder',
		@ObjectGuid=@SupplierOrderGuid