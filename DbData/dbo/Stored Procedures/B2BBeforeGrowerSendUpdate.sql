﻿







CREATE PROCEDURE [dbo].[B2BBeforeGrowerSendUpdate]
	 @GrowerOrderGuid as uniqueidentifier
	 
	 

AS
	--select * from lookup where path like '%orderlines%' order by sortsequence
	--Code/OrderLineStatus/Cancelled --
	--Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed  --confirmed
	Update OrderLine
	Set OrderLineStatusLookupGuid = (select Guid from Lookup where path = 'Code/OrderLineStatus/Cancelled')
	FROM OrderLine ol
	Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid

	WHERE 
		1 = 1
		AND olv.GrowerOrderGuid = @GrowerOrderGuid
		AND ol.QtyOrdered = 0
		
	Update OrderLine
	Set OrderLineStatusLookupGuid = (select Guid from Lookup where path = 'Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed')
	FROM OrderLine ol
	Join OrderLineViewShowInactiveProduct olv on ol.Guid = olv.OrderLineGuid

	WHERE 
		1 = 1
		AND olv.GrowerOrderGuid = @GrowerOrderGuid
		AND ol.QtyOrdered != 0
		AND olv.OrderLineStatusLookupSortSequence between 4 and 7