﻿





CREATE PROCEDURE [dbo].[ValidateSupplierID]
	 @JobNumber as bigint,
	 @SupplierCode as nvarchar(10),
	 @SupplierID as nvarchar(50),
	 @SupplierDescription as nvarchar(100) ,
	 @ProductCount as bigint Output

AS
	DECLARE @iProductCount bigint
	DECLARE @iExcludedCount bigint
	DECLARE @sExcludedReason nvarchar(50)

	if @SupplierID <> ''
		BEGIN

			set @iProductCount = (	
				SELECT count(*) as TheCount FROM Product p
				JOIN Program pr on p.ProgramGuid = pr.Guid
				JOIN Supplier s  on pr.SupplierGuid = S.Guid
				WHERE s.Code = @SupplierCode
					AND p.SupplierIdentifier = @SupplierID 
				)
				
		END
	ELSE
		BEGIN
			set @iProductCount = (	
				SELECT count(*) as TheCount FROM Product P
				JOIN Program PR on P.ProgramGuid = pr.Guid
				JOIN Supplier S  on PR.SupplierGuid = S.Guid
				WHERE S.Code = @SupplierCode
					AND P.SupplierDescription = @SupplierDescription
				)
		END

	if @iProductCount = 0
	BEGIN
		if @SupplierID <> ''
			BEGIN
				SELECT 
				@iExcludedCount = count(*),
				@sExcludedReason =  Max(Reason)  
					FROM ProductExclusion pe
                    JOIN Supplier s  on pe.SupplierGuid = s.Guid
					WHERE s.Code = @SupplierCode
						AND pe.SupplierIdentifier = @SupplierID							
			END
		ELSE
			BEGIN
				SELECT 
				@iExcludedCount = count(*),
				@sExcludedReason =  Max(Reason)  
				FROM ProductExclusion pe
                JOIN Supplier s  on pe.SupplierGuid = s.Guid
				WHERE s.Code = @SupplierCode
					AND pe.SupplierDescription = @SupplierDescription
				
			END

		if @iExcludedCount = 1
			BEGIN
				INSERT INTO ReportedAvailabilityUploadJobMessage Values(newID(),
				(select Guid from ReportedAvailabilityUploadJob where ID = @JobNumber),
				getdate(),'Info','Normal',
					'Excluded on Purpose, the Reason is: ' + @sExcludedReason + ' -- '  + @SupplierID +  ' -- ' + @SupplierDescription
					)
			END
		ELSE
			BEGIN
				INSERT INTO ReportedAvailabilityUploadJobMessage Values(newID(),
				(select Guid from ReportedAvailabilityUploadJob where ID = @JobNumber),
				getdate(),'Info','Normal',
					'Product Not found: ' + @SupplierID +  ' -- ' + @SupplierDescription
					)
			END
	END
                
 --   if @iProductCount > 1               
 --   BEGIN
	--	INSERT INTO ReportedAvailabilityUploadJobMessage Values(newID(),
	--		(select Guid from ReportedAvailabilityUploadJob where ID = @JobNumber),
	--		getdate(),'Info','Normal',
	--				'Too Many Product found: (' + cast(@iProductCount as nvarchar(3)) +  ') '  + @SupplierID +  ' -- ' + @SupplierDescription
	--				)
	--END
	              
                    
    set @ProductCount = @iProductCount