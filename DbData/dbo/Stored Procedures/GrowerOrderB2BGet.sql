﻿




CREATE PROCEDURE [dbo].[GrowerOrderB2BGet]
	 @GrowerOrderGuid as uniqueidentifier
	 
	 

AS
	;With  zz(GrowerOrderGuid,ShipMethodCode,ShipMethodName,TagRatioCode,TagRatioName)
	AS
	(
	SELECT  o.Guid as GrowerOrderGuid,
	Max(rtrim(sosm.Code)) as ShipMethodCode,Max(rtrim(sosm.Name)) as ShipMethodName,
	Max(rtrim(sotr.Code)) as TagRatioCode,Max(rtrim(sotr.Name)) as TagRatioName
	FROM GrowerOrder o
	Join SupplierOrder so on o.Guid = so.GrowerOrderGuid
	Join lookup sosm on so.ShipMethodLookupGuid = sosm.Guid
	Join lookup sotr on so.TagRatioLookupGuid = sotr.Guid
	Where o.Guid = @GrowerOrderGuid
	Group by o.Guid
	)
	



	Select 
	o.Guid as OrderGuid
	,g.Name as GrowerName
	,rtrim(o.CustomerPoNo) as CustomerPoNo
	,o.[Description] as OrderDescription
	,rtrim(o.OrderNo) as OrderNo
	,'' as SellerOrderNo
	,rtrim(pfc.Code) as ProductFormCategoryCode
	,RIGHT('00'+CAST(sw.Week AS VARCHAR(2)),2) + '|' + cast(sw.Year as varchar(4)) as ShipWeekString
	,o.DateEntered as OrderDate
	,o.DateEntered as OrderTime
	,sw.MondayDate as ShipDate
	,zz.ShipMethodCode as ShipMethodCode
	,zz.ShipMethodName as ShipMethodName
	,'' as SubstitutionOK
	,'' as FreightPaymentMethod
	,ga.SellerCustomerID as SellerNumber
	,'' as AirportCode
	,'' as BackOrderOK
	,o.Description as HeaderComments
	,'' as ShippingInstructions
	,'EPS' as SalesRepName
	,zz.TagRatioCode as TagRatioCode
	,zz.TagRatioName as TagRatioName
	,cast(g.ID as varchar(20)) as GrowerID
	,g.Name as GrowerAddressName
	,p.FirstName + ' ' + p.LastName as GrowerContact
	,a.StreetAddress1 as GrowerAddress1
	,a.StreetAddress2 as GrowerAddress2
	,a.City as GrowerCity
	,a.StateCode as GrowerStateCode
	,rtrim(a.CountryCode) as GrowerCountryCode
	,rtrim(a.ZipCode) as GrowerZipCode
	,'' as GrowerFax
	,cast(g.PhoneAreaCode as varchar(3)) + cast(g.Phone as varchar(7)) as GrowerPhone
	,case when a.CountryCode = 'CA' then 'CAN' else 'USA' end as GrowerCountryCode2
	,cast(oa.AddressID as varchar(20)) as ShipToID
	,oa.AddressName as ShipToName
	,p.FirstName + ' ' + p.LastName as ShipToContact
	,oa.StreetAddress1 as ShipToAddress1
	,oa.StreetAddress2 as ShipToAddress2
	,oa.City as ShipToCity
	,oa.StateCode as ShipToStateCode
	,rtrim(oa.CountryCode) as ShipToCountryCode
	,rtrim(oa.ZipCode) as ShipToZipCode
	,'' as ShipToFax
	,cast(ga.PhoneAreaCode as varchar(3)) + cast(ga.Phone as varchar(7)) as ShipToPhone
	,case when oa.CountryCode = 'CA' then 'CAN' else 'USA' end as ShipToCountryCode2      
    ,ga.B2BUrl
	,ga.B2BCustomerName
	,g.B2BOrderURL as GrowerURLForOrder
	,g.Email as GrowerEmail
	,o.GrowerShipMethodCode
	,cast(o.RevisionNumber as bigint) as RevisionNumber
	from GrowerOrder o 
	Join Grower g on o.GrowerGuid = g.Guid
	Join Person p on o.PersonGuid = p.Guid
	Join AddressView a on g.AddressGuid = a.AddressGuid
	Join GrowerAddress ga on o.ShipToAddressGuid = ga.Guid
	Join AddressView oa on ga.AddressGuid = oa.AddressGuid
	Join ProductFormCategory pfc on o.ProductFormCategoryGuid = pfc.Guid
	Join ShipWeek sw on o.ShipWeekGuid = sw.Guid
	Join zz on o.Guid = zz.GrowerOrderGuid

	

	WHERE 
		1 = 1
		AND o.Guid = @GrowerOrderGuid