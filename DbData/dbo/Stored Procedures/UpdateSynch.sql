﻿

CREATE procedure [dbo].[UpdateSynch]
	@ObjectName AS NVARCHAR(200),
	@dDate AS DateTime
AS
	Update SynchControl set LastSynchCompleteDate = @dDate
	WHERE ObjectName = @ObjectName