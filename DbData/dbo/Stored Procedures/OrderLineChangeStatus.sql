﻿








--EXAMPLE:
--DECLARE @OrderLinesChanged AS INTEGER
--DECLARE @OrderLinesDeleted AS INTEGER

--EXECUTE OrderLineChangeStatus
--	@UserGuid='7714b7e5-7148-404b-9e2e-93ee8b7e3b63',
--	@GrowerOrderGUid='2de9d823-e86d-4fea-8f98-ba6ed1422ec0',
--	@FromOrderLineStatusCode='Pending',
--	@ToOrderLineStatusCode='Pending',
--	@DeleteOrderLinesWithOrderLineStatusCode='Pending',
--	@OrderLinesChanged=@OrderLinesChanged OUT,
--	@OrderLinesDeleted=@OrderLinesDeleted OUT

--SELECT
--	@OrderLinesChanged AS OrderLinesChanged,
--	@OrderLinesDeleted AS OrderLinesDeleted

CREATE procedure [dbo].[OrderLineChangeStatus]
	@UserCode AS NCHAR(56) = NULL,
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@ShipWeekCode AS NCHAR(6) = NULL,
	@ProgramTypeCode AS NCHAR(20) = NULL,
	@ProductFormCategoryCode AS NCHAR(20) = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = NULL,
	@SupplierOrderGuid AS UNIQUEIDENTIFIER = NULL,
	@OrderLineGuid AS UNIQUEIDENTIFIER = NULL,
	@FromOrderLineStatusCode AS NCHAR(50) = NULL,
	@FromOrderLineStatusCodeList AS NVARCHAR(500) = NULL,
	@ToOrderLineStatusCode AS NCHAR(50) = NULL,
	@DeleteOrderLinesWithOrderLineStatusCode AS NCHAR(30) = NULL,
	@OrderLinesChanged AS INTEGER = NULL OUT,
	@OrderLinesDeleted AS INTEGER = NULL OUT
AS
	IF @UserCode IS NOT NULL AND @UserCode != '' AND dbo.IsUniqueIdentifier(@UserCode) = 1
		BEGIN
			SET @UserGuid = dbo.ExtractUniqueIdentifier(@UserCode)

			SELECT @UserCode = UserCode
			FROM Person
			WHERE UserGuid = @UserGuid
		END

	IF @UserCode IS NULL OR @UserCode = '' AND @UserGuid IS NOT NULL
			SELECT @UserCode = UserCode
			FROM Person
			WHERE UserGuid = @UserGuid

	IF @GrowerOrderGuid='00000000-0000-0000-0000-000000000000'
		SET @GrowerOrderGuid = NULL

	IF @SupplierOrderGuid='00000000-0000-0000-0000-000000000000'
		SET @SupplierOrderGuid = NULL

	IF @OrderLineGuid='00000000-0000-0000-0000-000000000000'
		SET @OrderLineGuid = NULL

	SET @FromOrderLineStatusCodeList = LTRIM(RTRIM(@FromOrderLineStatusCodeList))
--print @FromOrderLineStatusCodeList + '--' + @FromOrderLineStatusCodeList
	IF @FromOrderLineStatusCodeList = ''
		SET @FromOrderLineStatusCodeList = NULL
--print @FromOrderLineStatusCodeList + '--' + @FromOrderLineStatusCodeList
	IF @FromOrderLineStatusCodeList IS NULL
		SET @FromOrderLineStatusCodeList = @FromOrderLineStatusCode
--print @FromOrderLineStatusCodeList + '--' + @FromOrderLineStatusCodeList
	-- It's OK to set the FromOrderLineStatusCodeList to null if (and only if) you know the order line guid.
	IF @FromOrderLineStatusCodeList IS NULL AND @OrderLineGuid IS NOT NULL
		SELECT @FromOrderLineStatusCodeList = OrderLineStatusLookupCode
		FROM OrderLineView
		WHERE OrderLineGuid = @OrderLineGuid
--print @FromOrderLineStatusCodeList + '--' + @FromOrderLineStatusCodeList
	IF @FromOrderLineStatusCodeList IS NOT NULL
		SET @FromOrderLineStatusCodeList = ',' + @FromOrderLineStatusCodeList + ','
	--	SET @FromOrderLineStatusCodeList = @FromOrderLineStatusCodeList + ','
--		
--print @FromOrderLineStatusCodeList + '--' + @FromOrderLineStatusCodeList



	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('UserGuid',LEFT(@UserGuid,4)+'...',1) +
		dbo.XmlSegment('ShipWeekCode',@ShipWeekCode,1) +
		dbo.XmlSegment('ProgramTypeCode',@ProgramTypeCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('SupplierOrderGuid',@SupplierOrderGuid,1) +
		dbo.XmlSegment('OrderLineGuid',@OrderLineGuid,1) +
		dbo.XmlSegment('FromOrderLineStatusCode',@FromOrderLineStatusCode,1) +
		dbo.XmlSegment('FromOrderLineStatusCodeList',@FromOrderLineStatusCodeList,1) +
		dbo.XmlSegment('ToOrderLineStatusCode',@ToOrderLineStatusCode,1) +
		dbo.XmlSegment('DeleteOrderLinesWithOrderLineStatusCode',@DeleteOrderLinesWithOrderLineStatusCode,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='OrderLineChangeStatus',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	SET @OrderLinesChanged = -1
	SET @OrderLinesDeleted = -1

	DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
	IF (@GrowerOrderGuid IS NULL AND @SupplierOrderGuid IS NULL AND @OrderLineGuid IS NULL)
		EXECUTE GrowerGetGuid
			@UserGuid=@UserGuid,
			@UserCode=@UserCode,
			@ProcessLookupCode='OrderLineChangeStatus',
			@GrowerGuid=@GrowerGuid OUT

	-- A ship week must be selected.
	IF @ShipWeekCode IS NULL AND @GrowerOrderGuid IS NULL AND @SupplierOrderGuid IS NULL AND @OrderLineGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@userGuid=@UserGuid,
				@UserCode=@UserCode,
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='OrderLineChangeStatus',
				@ObjectTypeLookupCode='EventLog',
				@ObjectGuid=@OriginalEventLogGuid,
				@Comment='The ShipWeekCode or at least one OrderGuid parameter must be provided.'

			RETURN
		END

	-- One GrowerOrderGuid, SupplierOrderGuid, OrderLineGuid must be selected (
	IF
		@GrowerOrderGuid IS NULL AND
		@SupplierOrderGuid IS NULL AND
		@OrderLineGuid IS NULL AND
		(
			@GrowerGuid IS NULL OR
			@ProgramTypeCode IS NULL OR
			@ProductFormCategoryCode IS NULL
		)
			BEGIN
				EXECUTE EventLogAdd
					@UserGuid=@UserGuid,
					@UserCode=@UserCode,
					@LogTypeLookupCode='ParameterError',
					@ProcessLookupCode='OrderLineChangeStatus',
					@ObjectTypeLookupCode='EventLog',
					@ObjectGuid=@OriginalEventLogGuid,
					@Comment='Unless the GrowerOrderGuid, SupplierOrderGuid or OrderLineGuid is non-null, the GrowerGuid (derived from UserGuid), ProgramTypeCode and ProductFormCategoryCode must all be selected.'

				RETURN
			END

	DECLARE @ToOrderLineStatusGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@Path='Code/OrderLineStatus',
		@LookupCode=@ToOrderLineStatusCode,
		@ProcessLookupCode='OrderLineChangeStatus',
		@LookupGuid=@ToOrderLineStatusGuid OUT

	BEGIN TRANSACTION

		-- This is a "lock hint" that tells SQL Server to lock the Grower row during the transaciton. Without it, we could get order line statuses in a confused state.
		SELECT @GrowerGuid=Guid from Grower WITH (ROWLOCK,XLOCK) WHERE Guid=@GrowerGuid

		IF IsNUll(@FromOrderLineStatusCode,'') != @ToOrderLineStatusCode
			BEGIN
				UPDATE OrderLine
					SET OrderLineStatusLookupGuid =  @ToOrderLineStatusGuid,
					DateLastChanged = Getdate()
				FROM OrderLine
					INNER JOIN OrderLineView
						ON OrderLine.Guid = OrderLineView.OrderLineGuid
				WHERE
					@FromOrderLineStatusCodeList LIKE '%' + RTRIM(OrderLineStatusLookupCode) + '%' AND
					(ISNULL(@ShipWeekCode, OrderLineView.ShipWeekCode) = OrderLineView.ShipWeekCode 
						OR @ShipWeekCode = 'ALLALL')
					AND
					ISNULL(@GrowerGuid, OrderLineView.GrowerGuid) = OrderLineView.GrowerGuid AND
					ISNULL(@GrowerOrderGuid, OrderLineView.GrowerOrderGuid) = OrderLineView.GrowerOrderGuid AND
					ISNULL(@SupplierOrderGuid, OrderLineView.SupplierOrderGuid) = OrderLineView.SupplierOrderGuid AND
					ISNULL(@OrderLineGuid, OrderLineView.OrderLineGuid) = OrderLineView.OrderLineGuid
			END

		SET @OrderLinesChanged = @@ROWCOUNT

		IF @OrderLinesChanged > 0 AND @FromOrderLineStatusCode = 'Pending' AND (@ToOrderLineStatusCode = 'Ordered' or @ToOrderLineStatusCode = 'GrowerNotified')
			BEGIN

				UPDATE GrowerOrder 
					SET DateEntered = CURRENT_TIMESTAMP
					--, 
					--PersonGuid = (SELECT Guid
					--			FROM Person
					--			WHERE UserCode = @UserCode)
				WHERE Guid = @GrowerOrderGuid
			END
		
		


		--Move all items to Cancelled once the qty is 0 and the status is in the below list
		UPDATE OrderLine
			SET OrderLineStatusLookupGuid = 
			case 
				when OrderLine.qtyordered = 0 and OrderLineStatusLookupGuid = (select guid from lookup where path = 'Code/OrderLineStatus/SupplierNotified')   
					then  (select guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
				when OrderLine.qtyordered = 0 and OrderLineStatusLookupGuid = (select guid from lookup where path = 'Code/OrderLineStatus/SupplierConfirmed')   
					then  (select guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
				when OrderLine.qtyordered = 0 and OrderLineStatusLookupGuid = (select guid from lookup where path = 'Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed')   
					then  (select guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
				--when OrderLine.qtyordered = 0 and OrderLineStatusLookupGuid = (select guid from lookup where path = 'Code/OrderLineStatus/GrowerCancelled')   
					--then  (select guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
				when OrderLine.qtyordered = 0 and OrderLineStatusLookupGuid = (select guid from lookup where path = 'Code/OrderLineStatus/SupplierCancelled')   
					then  (select guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
				when OrderLine.qtyordered = 0 and OrderLineStatusLookupGuid = (select guid from lookup where path = 'Code/OrderLineStatus/Shipped')   
					then  (select guid from lookup where path = 'Code/OrderLineStatus/Cancelled')
				else OrderLineStatusLookupGuid end
		FROM OrderLine
			JOIN SupplierOrder so on OrderLine.SupplierOrderGuid = so.guid
		WHERE
			so.GrowerOrderGuid = @GrowerOrderGuid 
			OR so.GrowerOrderGuid = (SELECT so2.GrowerOrderGuid FROM SupplierOrder so2 
									JOIN Orderline ol2 on ol2.supplierOrderGuid = so2.guid 
									WHERE ol2.guid =  @OrderLineGuid)
			


		DELETE OrderLine
		FROM OrderLine
			INNER JOIN OrderLineView
				ON OrderLine.Guid = OrderLineView.OrderLineGuid
		WHERE
			OrderLineStatusLookupCode = @DeleteOrderLinesWithOrderLineStatusCode AND
			(ISNULL(@ShipWeekCode, OrderLineView.ShipWeekCode) = OrderLineView.ShipWeekCode
				OR @ShipWeekCode = 'ALLALL') 
			AND
			ISNULL(@GrowerGuid, OrderLineView.GrowerGuid) = OrderLineView.GrowerGuid AND
			ISNULL(@GrowerOrderGuid, OrderLineView.GrowerOrderGuid) = OrderLineView.GrowerOrderGuid AND
			ISNULL(@SupplierOrderGuid, OrderLineView.SupplierOrderGuid) = OrderLineView.SupplierOrderGuid AND
			ISNULL(@OrderLineGuid, OrderLineView.OrderLineGuid) = OrderLineView.OrderLineGuid

		SET @OrderLinesDeleted = @@ROWCOUNT

		IF @OrderLinesDeleted > 0
			BEGIN
				EXECUTE GrowerOrderCleanupOrphans
					@GrowerGuid=@GrowerGuid,
					@GrowerOrderGuid=@GrowerOrderGuid
			END

		
	COMMIT TRANSACTION

	


	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('OrderLinesChanged',@OrderLinesChanged,1) +
		dbo.XmlSegment('OrderLinesDeleted',@OrderLinesDeleted,1),
		0
	)

	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='OrderLineChangeStatus',
		@XmlData=@ReturnValues