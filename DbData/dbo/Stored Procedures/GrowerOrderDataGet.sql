﻿CREATE PROCEDURE [dbo].[GrowerOrderDataGet]
	@UserCode AS NVARCHAR(56) = null,
	@GrowerGuid AS UNIQUEIDENTIFIER = null,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER = null,
	@ShipWeekCode AS NCHAR(6) = null,
	@ProductFormCategoryCode AS NCHAR(20) = null,
	@OrderTypeLookupCodeList AS NVARCHAR(500) = null,
	@OrderStatusLookupCodeList AS NVARCHAR(500) = null
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('GrowerOrderGuid',@GrowerOrderGuid,1) +
		dbo.XmlSegment('ShipWeekCode',@ShipWeekCode,1) +
		dbo.XmlSegment('ProductFormCategoryCode',@ProductFormCategoryCode,1) +
		dbo.XmlSegment('OrderTypeLookupCodeList',@OrderTypeLookupCodeList,1) +
		dbo.XmlSegment('OrderStatusLookupCodeList',@OrderStatusLookupCodeList,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerOrderDataGet',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	IF @OrderTypeLookupCodeList IS NULL OR @OrderTypeLookupCodeList LIKE '%*%'
		SET @OrderTypeLookupCodeList = NULL
	ELSE
		SET @OrderTypeLookupCodeList = ',' + @OrderTypeLookupCodeList + ','

	IF @OrderStatusLookupCodeList IS NULL OR @OrderStatusLookupCodeList LIKE '%*%'
		SET @OrderStatusLookupCodeList = NULL
	ELSE
		SET @OrderStatusLookupCodeList = ',' + @OrderStatusLookupCodeList + ','

	DECLARE @ShipWeekGuid AS UNIQUEIDENTIFIER
	SELECT @ShipWeekGuid = ShipWeekGuid FROM ShipWeekView WHERE ShipWeekCode = @ShipWeekCode

	DECLARE @LocalTempGrowerOrderData TABLE
	(
		GrowerOrderGuid UNIQUEIDENTIFIER,
		SupplierOrderGuid UNIQUEIDENTIFIER,
		GrowerGuid UNIQUEIDENTIFIER,
		SupplierGuid UNIQUEIDENTIFIER,
		ShipWeekGuid UNIQUEIDENTIFIER,
		OrderTypeLookupGuid UNIQUEIDENTIFIER,
		SupplierOrderStatusLookupGuid UNIQUEIDENTIFIER
	)

	INSERT INTO @LocalTempGrowerOrderData
	SELECT
		GrowerOrderGuid,
		SupplierOrderGuid,
		GrowerGuid,
		SupplierGuid,
		ShipWeekGuid,
		OrderTypeLookupGuid,
		SupplierOrderStatusLookupGuid
	FROM SupplierOrderView
	WHERE
		(@GrowerGuid = GrowerGuid) AND
		(@GrowerOrderGuid IS NULL OR @GrowerOrderGuid = GrowerOrderGuid) AND
		(@ShipWeekCode IS NULL OR @ShipWeekCode = ShipWeekCode) AND
		(@ProductFormCategoryCode IS NULL OR ProductFormCategoryCode=@ProductFormCategoryCode) AND
		(@OrderTypeLookupCodeList IS NULL OR @OrderTypeLookupCodeList LIKE '%,' + RTRIM(OrderTypeLookupCode) + ',%') AND
		(@OrderStatusLookupCodeList IS NULL OR @OrderStatusLookupCodeList LIKE '%,' + RTRIM(SupplierOrderStatusLookupCode) + ',%')
	GROUP BY
		GrowerOrderGuid,
		SupplierOrderGuid,
		GrowerGuid,
		SupplierGuid,
		ShipWeekGuid,
		OrderTypeLookupGuid,
		SupplierOrderStatusLookupGuid

	SELECT Supplier.*
	FROM Supplier
	WHERE Supplier.Guid IN
	(SELECT DISTINCT(SupplierGuid) FROM @LocalTempGrowerOrderData)

	SELECT SupplierOrder.*
	FROM SupplierOrder
	WHERE SupplierOrder.Guid IN
	(SELECT DISTINCT(SupplierOrderGuid) FROM @LocalTempGrowerOrderData)

	SELECT Grower.*
	FROM Grower
	WHERE Grower.Guid IN
	(SELECT DISTINCT(GrowerGuid) FROM @LocalTempGrowerOrderData)

	SELECT GrowerOrder.*
	FROM GrowerOrder
	WHERE GrowerOrder.Guid IN
	(SELECT DISTINCT(GrowerOrderGuid) FROM @LocalTempGrowerOrderData)

	SELECT ShipWeek.*
	FROM ShipWeek
	WHERE ShipWeek.Guid IN
	(SELECT DISTINCT(ShipWeekGuid) FROM @LocalTempGrowerOrderData)
	
	SELECT Lookup.*
	FROM Lookup
	WHERE Lookup.Guid IN
	(
		(SELECT DISTINCT(OrderTypeLookupGuid) FROM @LocalTempGrowerOrderData)
		UNION
		(SELECT DISTINCT(SupplierOrderStatusLookupGuid) FROM @LocalTempGrowerOrderData)
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerOrderDataGet',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserCode=@UserCode