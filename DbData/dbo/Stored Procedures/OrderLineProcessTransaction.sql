﻿












--EXAMPLE:
--DECLARE @GrowerOrderGuid as UNIQUEIDENTIFIER
--DECLARE @OrderTrxStatusLookupCode as NCHAR(20)
--DECLARE @SupplierOrderGuid as UNIQUEIDENTIFIER

--EXECUTE GrowerOrderGetOrAdd
--	@GrowerGuid='9CB1183C-20AC-4049-9206-25C558CA5BEF',
--	@ShipWeekCode='201320',
--	@ProductFormCategoryCode='URC',
--	@CustomerPoNo='TestPoNumber',
--	@Description='This is my order.',
--	@GrowerOrderGuid=@GrowerOrderGuid OUT,
--	@OrderTrxStatusLookupCode=@OrderTrxStatusLookupCode OUT

--SELECT
--	@GrowerOrderGuid as GrowerOrderGuid,
--	@OrderTrxStatusLookupCode as OrderTrxStatusLookupCode

--EXECUTE SupplierOrderGetOrAdd
--	@GrowerOrderGuid=@GrowerOrderGuid,
--	@SupplierGuid='A070F7DF-A563-453E-A558-0F7524FCE705',
--	@PoNo='TestSupplierPoNumber',
--	@SupplierOrderGuid=@SupplierOrderGuid OUT,
--	@OrderTrxStatusLookupCode=@OrderTrxStatusLookupCode OUT

--SELECT
--	@SupplierOrderGuid as SupplierOrderGuid,
--	@OrderTrxStatusLookupCode as OrderTrxStatusLookupCode

--DECLARE @OrderLineTrxStatusLookupCode AS NCHAR(20)
--DECLARE @OrderLineGuid AS UNIQUEIDENTIFIER

--EXECUTE OrderLineProcessTransaction
--	@SupplierOrderGuid=@SupplierOrderGuid,
--	@ProductGuid='0905CE79-E69F-4A88-99E9-0448A091D617',
--	@Quantity=0,
--	@OrderLineTrxStatusLookupCode=@OrderLineTrxStatusLookupCode OUT,
--	@OrderLineGuid=@OrderLineGuid OUT

--SELECT
--	@OrderLineTrxStatusLookupCode AS OrderLineTrxStatusLookupCode,
--	@OrderLineGuid AS OrderLineGuid

CREATE procedure [dbo].[OrderLineProcessTransaction]
	@UserGuid UNIQUEIDENTIFIER = NULL,
	@UserCode NCHAR(56) = NULL,
	@SupplierOrderGuid UNIQUEIDENTIFIER = NULL,
	@ProductGuid UNIQUEIDENTIFIER = NULL,
	@OrderLineToUpdateGuid UNIQUEIDENTIFIER = NULL,
	@QtyOrdered INTEGER,
	@Price DECIMAL(8,4) = NULL,
	@SupplierOrGrower nvarchar(20),
	@OrderLineTrxStatusLookupCode NCHAR(20) = NULL OUT,
	@ActualQuantity INTEGER OUT,
	@OrderLineGuid UNIQUEIDENTIFIER = NULL OUT
AS
	SET @ActualQuantity = 0

	-- OBSOLETE DEPRICATED
	IF dbo.IsUniqueIdentifier(@UserCode) = 1 AND @UserGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
	BEGIN
		SET @UserGuid = dbo.ExtractUniqueIdentifier(@UserCode)

		SELECT @UserCode = UserCode
		FROM Person
		WHERE UserGuid = @UserGuid
	END

	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserCode',@UserCode,1) +
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('SupplierOrderGuid',@SupplierOrderGuid,1) +
		dbo.XmlSegment('ProductGuid',@ProductGuid,1) +
		dbo.XmlSegment('OrderLineToUpdateGuid',@OrderLineToUpdateGuid,1) +
		dbo.XmlSegment('QtyOrdered',@QtyOrdered,1) +
		dbo.XmlSegment('Price',@Price,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='OrderLineProcessTransaction',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @OrderLineStatusLookupOrderedSortSequence AS FLOAT
	DECLARE @OrderLineStatusLookupSortSequence AS FLOAT
	DECLARE @SellerCode as nvarchar(10)
	DECLARE @NewOrderLineGuid as uniqueidentifier
	DECLARE @NewOrderLineStatusLookupGuid as uniqueidentifier
	
	SELECT @OrderLineStatusLookupOrderedSortSequence = LookupView.SortSequence
	FROM LookupView
	WHERE Path = 'Code/OrderLineStatus/Ordered'

	-- If OrderLineToUpdateGuid is specified instead of SupplierOrderGuid and ProductGuid and SellerGuid
	-- that means that this is an order line update.
	-- In that case, we just fill in the SupplierOrderGuid and ProductGuid based on the OrderLineGuid.
	IF @ProductGuid IS NULL OR @SupplierOrderGuid IS NULL
	BEGIN
		IF @OrderLineToUpdateGuid IS NOT NULL
		BEGIN
			SELECT
				@SupplierOrderGuid=SupplierOrderGuid,
				@ProductGuid=ProductGuid,
				@SellerCode= SellerCode
			FROM OrderLineView
			WHERE OrderLineGuid=@OrderLineToUpdateGuid

			SET @OrderLineGuid = NULL
		END
		ELSE
		BEGIN
			EXECUTE EventLogAdd
				@Comment='SupplierOrderGuid and ProductGuid are required unless the OrderLineToUpdateGuid is supplied.',
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='OrderLineProcessTransaction',
				@UserCode=@UserCode,
				@UserGuid=@UserGuid
		END
	END

	DECLARE @RowCount AS INT
	
	
	SET @OrderLineTrxStatusLookupCode = 'Requested'

	DECLARE @GrowerOrderGuid UniqueIdentifier
	DECLARE @VarietyName NVARCHAR(100)
	
	SELECT
		@OrderLineGuid = OrderLineGuid,
		@GrowerOrderGuid = GrowerOrderGuid,
		@OrderLineStatusLookupSortSequence = OrderLineStatusLookupSortSequence,
		@VarietyName = VarietyName
	FROM OrderLineView
	WHERE
		SupplierOrderGuid=@SupplierOrderGuid AND
		ProductGuid=@ProductGuid

	Declare @PersonGuid as uniqueidentifier
	set @PersonGuid = (select top 1 guid from person where UserGuid = @UserCode or UserCode = @UserCode or UserGuid = @UserGuid)

	Declare @theDate datetime
	set @theDate = getdate()

	Declare @LogGuid uniqueidentifier
	Declare @sComment nvarchar(2000)

	DECLARE @ShipWeekGuid AS UNIQUEIDENTIFIER
	SELECT @ShipWeekGuid = ShipWeekGuid
	FROM SupplierOrderView
	WHERE SupplierOrderGuid = @SupplierOrderGuid

	DECLARE @SalesUnitQty AS INTEGER
	DECLARE @AvailabilityTypeLookupCode AS NCHAR(20)
	DECLARE @LineItemMinimumQty AS INTEGER
	DECLARE @QuantityAvailable AS INTEGER
	SELECT
		@QuantityAvailable = ReportedAvailabilityQty,
		@SalesUnitQty = SalesUnitQty,
		@AvailabilityTypeLookupCode = AvailabilityTypeLookupCode,
		@LineItemMinimumQty = LineItemMinimumQty
	FROM ReportedAvailabilityView
	WHERE
		ProductGuid = @ProductGuid AND
		ShipWeekGuid = @ShipWeekGuid

	IF @SalesUnitQty = 0
		SET @SalesUnitQty = 1

	IF @LineItemMinimumQty <= 0
		SET @LineItemMinimumQty = 1

	IF @AvailabilityTypeLookupCode = 'OPEN'
		SET @QuantityAvailable = 999999999
	ELSE IF @QuantityAvailable < 0 OR @AvailabilityTypeLookupCode = 'NA'
		SET @QuantityAvailable = 0

	DECLARE @OriginalQtyOrdered AS INTEGER
	SET @OriginalQtyOrdered = @QtyOrdered

	IF @QtyOrdered < 0
		SET @QtyOrdered = 0

	-- Set @LineItemMinimumQty to the next higher multiple if not already a multiple.
	SET @LineItemMinimumQty = (@LineItemMinimumQty + @SalesUnitQty - 1) / @SalesUnitQty * @SalesUnitQty

	-- Set @QuantityAvailable to the next lower multiple if not already a multiple.
	SET @QuantityAvailable = @QuantityAvailable / @SalesUnitQty * @SalesUnitQty

	-- Round @QtyOrdered to the nearest multiple if not already a multiple.
	SET @QtyOrdered = (@QtyOrdered + @SalesUnitQty/2) / @SalesUnitQty * @SalesUnitQty

	-- If @QtyOrdered is too low (but not zero) move up to @LineItemMinimum
	IF @QtyOrdered != 0 AND @QtyOrdered < @LineItemMinimumQty
		SET @QtyOrdered = @LineItemMinimumQty


	Declare @CurrentQTYOrdered as Integer    --added by Scott on 10/14/15  
	set @CurrentQTYOrdered = (SELECT QtyOrdered from OrderLine where Guid = @OrderLineGuid)

	-- If @QtyOrdered is too high move down to @QuantityAvailable
	IF (@QtyOrdered - @CurrentQTYOrdered)  > @QuantityAvailable
		AND @OrderLineStatusLookupSortSequence <= 1  --added by Scott on 10/14/15  (1 is Cart)
		SET @QtyOrdered = @QuantityAvailable
	IF (@QtyOrdered - @CurrentQTYOrdered)  > @QuantityAvailable	--added by Scott on 10/14/15  (1 is Cart)
		AND @OrderLineStatusLookupSortSequence > 1 
		AND @QtyOrdered > @CurrentQTYOrdered
		SET @QtyOrdered = @CurrentQTYOrdered

	IF @OriginalQtyOrdered != 0
	BEGIN
		IF @OriginalQtyOrdered > @QuantityAvailable 
			SET @OrderLineTrxStatusLookupCode = 'AdjustAvail'
		ELSE
		IF @OriginalQtyOrdered < @LineItemMinimumQty
			SET @OrderLineTrxStatusLookupCode = 'AdjustMin'
		ELSE
		IF @OriginalQtyOrdered != @QtyOrdered
			SET @OrderLineTrxStatusLookupCode = 'AdjustMult'
	END

	SET @ActualQuantity = @QtyOrdered

	IF @OrderLineGuid IS NULL
	BEGIN  --add line
		IF @QtyOrdered != 0
		BEGIN
			EXECUTE OrderLineAdd
				@UserCode = @UserCode,
				@SupplierOrderGuid = @SupplierOrderGuid,
				@ProductGuid = @ProductGuid,
				@QtyOrdered = @QtyOrdered,
				@Price = @Price,
				@OrderLineGuid = @OrderLineGuid OUT,
				@ReturnRecordSet = 0

			IF @OrderLineGuid IS NOT NULL
			BEGIN
				SET @OrderLineTrxStatusLookupCode = 'Added'
				SELECT
					@GrowerOrderGuid = GrowerOrderGuid
				FROM OrderLineView
				WHERE
					OrderLineGuid=@OrderLineGuid
							
			END
		END
		ELSE
		BEGIN
			SET @OrderLineTrxStatusLookupCode = 'NoChange'
		END
	END --add line
	ELSE
	BEGIN  --update line
		SELECT @OriginalQtyOrdered = QtyOrdered
		FROM OrderLine
		WHERE Guid = @OrderLineGuid
			
		IF @QtyOrdered > 0
		BEGIN  --Qty > 0
					

			IF @OriginalQtyOrdered = @QtyOrdered
			BEGIN
				SET @OrderLineTrxStatusLookupCode = 'NoChange'
			END

			ELSE
			BEGIN  --change
				IF @Price = 0
					SET @Price = NULL
							
				IF @SellerCode = 'DMO' and @OriginalQtyOrdered  = 0 
				BEGIN --Add Line and leave origninal line cancel
					
					EXECUTE OrderLineAdd
					@UserCode = @UserCode,
					@SupplierOrderGuid = @SupplierOrderGuid,
					@ProductGuid = @ProductGuid,
					@QtyOrdered = @QtyOrdered,
					@Price = @Price,
					@OrderLineGuid = @NewOrderLineGuid OUT,
					@ReturnRecordSet = 0

					IF @NewOrderLineGuid IS NOT NULL
					BEGIN
						If @SupplierOrGrower = 'Supplier'
						BEGIN
							select @NewOrderLineStatusLookupGuid =  Guid 
							from Lookup where path  = 'Code/OrderLineStatus/SupplierAdd'
						END
						ELSE
						BEGIN
							select @NewOrderLineStatusLookupGuid = Guid 
							from Lookup where path  = 'Code/OrderLineStatus/GrowerEdit'
						END
						 

						Update OrderLine set OrderLineStatusLookupGuid = @NewOrderLineStatusLookupGuid where Guid = @NewOrderLineGuid
						SET @OrderLineTrxStatusLookupCode = 'Added'
						SELECT
							@GrowerOrderGuid = GrowerOrderGuid
						FROM OrderLineView
						WHERE
							OrderLineGuid=@NewOrderLineGuid
							
					END

				END --Add Line and leave cancel
				ELSE 
				BEGIN  --Else Update
					
					If @SupplierOrGrower = 'Supplier'
					BEGIN
						select @NewOrderLineStatusLookupGuid =  Guid 
						from Lookup where path  = 'Code/OrderLineStatus/SupplierEdit'
					END
					ELSE
					BEGIN
						select @NewOrderLineStatusLookupGuid = Guid 
						from Lookup where path  = 'Code/OrderLineStatus/GrowerEdit'
					END
					
					
					
					--select * from lookup where path like 'Code/OrderLineStatus/%'
					UPDATE OrderLine 
					SET 
						QtyOrdered = @QtyOrdered,
						ActualPrice = ISNULL(@Price, ActualPrice),
						DateLastChanged = GetDate(),
						DateQtyLastChanged = GetDate(),
						OrderLineStatusLookupGuid = Case when OrderLineStatusLookupGuid in (select guid from lookup where path in('Code/OrderLineStatus/PreCart','Code/OrderLineStatus/Pending'))
														 then OrderLineStatusLookupGuid 
														 else @NewOrderLineStatusLookupGuid end
					WHERE Guid = @OrderLineGuid

					SET @RowCount = @@ROWCOUNT

					IF @@ROWCOUNT = 1
					BEGIN  --log event
						EXECUTE EventLogAdd
							@LogTypeLookupCode='RowUpdate',
							@ProcessLookupCode='OrderLineProcessTransaction',
							@ObjectTypeLookupCode='OrderLine',
							@ObjectGuid=@OrderLineGuid,
							@UserGuid=@UserGuid,
							@UserCode=@UserCode,
							@FloatValue=@QtyOrdered

						SET @OrderLineTrxStatusLookupCode = 'Updated'
					END  --log event
					ELSE
					BEGIN  --log failed event
						EXECUTE EventLogAdd
							@Comment='@@ROWCOUNT not 1 after attempted update (See IntValue).',
							@LogTypeLookupCode='DatabaseError',
							@ProcessLookupCode='OrderLineProcessTransaction',
							@ObjectTypeLookupCode='OrderLine',
							@UserGuid=@UserGuid,
							@UserCode=@UserCode,
							@IntValue=@RowCount,
							@FloatValue=@QtyOrdered

						SET @ActualQuantity = 0
						SET @OrderLineTrxStatusLookupCode = 'Failure'
					END  --log failed event
								
					If @OrderLineStatusLookupSortSequence > 1
					BEGIN  --add history
						set @sComment = 'Order for ' + cast(@OriginalQtyOrdered as nvarchar(10)) + ' ' + @VarietyName + ' changed to ' + cast(@QtyOrdered as varchar(10))
						set @LogGuid = (select guid from lookup where path = 'Logging/LogType/Grower/OrderLineChange')
						Exec GrowerOrderHistoryAdd 
							@UserGuid,@UserCode,
							@GrowerOrderGuid,
							@sComment,0,@PersonGuid,@theDate,@LogGuid
					END --add history

						
				END --else Update
			END	--change
		END	 --Qty > 0
		ELSE
		BEGIN  --qty = 0
			--SELECT
			--	@OrderLineStatusLookupSortSequence = OrderLineStatusLookupSortSequence
			--FROM OrderLineView
			--WHERE OrderLineGuid=@OrderLineGuid

			-- If this is just a cart order, we can delete the line.
			IF (@OrderLineStatusLookupSortSequence < @OrderLineStatusLookupOrderedSortSequence)
			BEGIN
				DELETE OrderLine WHERE Guid = @OrderLineGuid

				SET @RowCount = @@ROWCOUNT

				IF @@ROWCOUNT = 1
				BEGIN
					SET @ActualQuantity = 0

					EXECUTE EventLogAdd
						@LogTypeLookupCode='RowDelete',
						@ProcessLookupCode='OrderLineProcessTransaction',
						@ObjectTypeLookupCode='OrderLine',
						@ObjectGuid=@OrderLineGuid,
						@UserGuid=@UserGuid,
						@UserCode=@UserCode

					SET @OrderLineTrxStatusLookupCode = 'Deleted'
				END
				ELSE
				BEGIN
					SET @ActualQuantity = 0

					EXECUTE EventLogAdd
						@Comment='@@ROWCOUNT not 1 after attempted delete (See IntValue).',
						@LogTypeLookupCode='DatabaseError',
						@ProcessLookupCode='OrderLineProcessTransaction',
						@ObjectTypeLookupCode='OrderLine',
						@UserGuid=@UserGuid,
						@UserCode=@UserCode,
						@IntValue=@RowCount

					SET @OrderLineTrxStatusLookupCode = 'Failure'
				END
			END
			ELSE
				-- But if this is a placed order, we can only cancel it.
			BEGIN
				UPDATE OrderLine
					SET
						--OrderLine.OrderLineStatusLookupGuid = LookupView.LookupGuid,
						OrderLine.QtyOrdered = 0,
						OrderLine.DateLastChanged = GetDate(),
						OrderLine.DateQtyLastChanged = GetDate()
				FROM OrderLine
					INNER JOIN LookupView
						ON LookupView.Path = 'Code/OrderLineStatus/Cancelled'
				WHERE OrderLine.Guid = @OrderLineGuid

				SET @RowCount = @@ROWCOUNT

				IF @@ROWCOUNT = 1
				BEGIN
					SET @ActualQuantity = 0

					EXECUTE EventLogAdd
						@LogTypeLookupCode='RowUpdate',
						@ProcessLookupCode='OrderLineProcessTransaction',
						@ObjectTypeLookupCode='OrderLine',
						@UserGuid=@UserGuid,
						@UserCode=@UserCode,
						@ObjectGuid=@OrderLineGuid

					SET @OrderLineTrxStatusLookupCode = 'Cancelled'
				END
				ELSE
				BEGIN
					SET @ActualQuantity = 0

					EXECUTE EventLogAdd
						@Comment='@@ROWCOUNT not 1 after attempted update (See IntValue).',
						@LogTypeLookupCode='DatabaseError',
						@ProcessLookupCode='OrderLineProcessTransaction',
						@ObjectTypeLookupCode='OrderLine',
						@UserGuid=@UserGuid,
						@UserCode=@UserCode,
						@IntValue=@RowCount

					SET @OrderLineTrxStatusLookupCode = 'Failure'
				END
							
							
				set @sComment = 'Order for ' + cast(@OriginalQtyOrdered as nvarchar(10)) + ' ' + @VarietyName + ' cancelled.'
				set @LogGuid = (select guid from lookup where path = 'Logging/LogType/Grower/OrderLineCancel')
				Exec GrowerOrderHistoryAdd 
					@UserGuid,@UserCode,
					@GrowerOrderGuid,
					@sComment,0,@PersonGuid,@theDate,@LogGuid




			END
		END --qty = 0
	END --update line

	--Handle Multiple forms or types
	exec GrowerOrderResetProgramCategoryType @UserGuid,@GrowerOrderGuid
	
	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('OrderLineTrxStatusLookupCode',@OrderLineTrxStatusLookupCode,1) +
		dbo.XmlSegment('ActualQuantity',@ActualQuantity,1) +
		dbo.XmlSegment('OrderLineGuid',@OrderLineGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='OrderLineProcessTransaction',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@XmlData=@ReturnValues