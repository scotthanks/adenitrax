﻿--EXAMPLES:
--DECLARE @Success AS BIT
--EXECUTE GrowerAddressDelete @UserGuid='7714B7E5-7148-404B-9E2E-93EE8B7E3B63', @GrowerAddressGuid='25a85ce4-3c26-4405-943b-0783ba019b1c', @Success=@Success OUT
--SELECT @Success AS Success

CREATE PROCEDURE [dbo].[GrowerAddressDelete]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerAddressGuid AS UNIQUEIDENTIFIER,
	@Success AS BIT OUT
AS
	SET @Success = 1

	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('GrowerAddressGuid',@GrowerAddressGuid,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='GrowerAddressDelete',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @RowCount AS INTEGER

	DECLARE @AddressGuid AS UNIQUEIDENTIFIER
	SELECT @AddressGuid=AddressGuid
	FROM GrowerAddress
	WHERE Guid=@GrowerAddressGuid

	IF @AddressGuid IS NULL
		BEGIN
			EXECUTE EventLogAdd
				@Comment='The Address was not found.',
				@LogTypeLookupCode='ParameterError',
				@ProcessLookupCode='GrowerAddressDelete',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			SET @Success = 0

			RETURN
		END

	DECLARE @IsDefault BIT
	SELECT @IsDefault=DefaultAddress
	FROM GrowerAddress
	WHERE Guid=@GrowerAddressGuid

	-- If there is more than one default address, it doesn't matter, so clear out the IsDefault flag.
	IF @IsDefault = 1
		BEGIN
			DECLARE @DefaultAddressCount AS INTEGER
			SELECT @DefaultAddressCount = COUNT(*)
			FROM GrowerAddressView
				INNER JOIN GrowerAddressView AS ThisGrowerAddressView
					ON GrowerAddressView.GrowerGuid = ThisGrowerAddressView.GrowerGuid
			WHERE
				ThisGrowerAddressView.GrowerAddressGuid = @GrowerAddressGuid AND
				GrowerAddressView.DefaultAddress = 1

			IF @DefaultAddressCount > 1
				SET @IsDefault = 0
		END

	if @IsDefault = 1
		BEGIN
			EXECUTE EventLogAdd
				@Comment='Cannot delete the default address.',
				@IntValue=@RowCount,
				@LogTypeLookupCode='RowDeleteError',
				@ProcessLookupCode='GrowerAddressDelete',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			SET @Success = 0

			RETURN
		END

	-- Note: We don't actually delete the address, we just deactivated it, because it may still be referenced by an order.
	UPDATE GrowerAddress
	SET
		DateDeactivated = CURRENT_TIMESTAMP,
		DefaultAddress = 0
	WHERE Guid=@GrowerAddressGuid

	SET @RowCount=@@ROWCOUNT

	IF @RowCount != 1
		BEGIN
			EXECUTE EventLogAdd
				@Comment='RowCount after deleting GrowerAddress was not 1',
				@IntValue=@RowCount,
				@LogTypeLookupCode='RowDeleteError',
				@ProcessLookupCode='GrowerAddressDelete',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			SET @Success = 0
			RETURN
		END

	-- Note: We don't deactivate the Address if it's the Grower's primary address (otherwise it will deactivate the grower).
	DECLARE @GrowerPrimaryAddressGuid UNIQUEIDENTIFIER
	SELECT @GrowerPrimaryAddressGuid = Grower.AddressGuid
	FROM Grower
		INNER JOIN GrowerAddress
			ON Grower.Guid = GrowerAddress.GrowerGuid
		INNER JOIN [Address]
			ON GrowerAddress.AddressGuid = Address.Guid
	WHERE Grower.AddressGuid = @GrowerAddressGuid

	IF @AddressGuid != @GrowerPrimaryAddressGuid
		BEGIN
			-- Note: We don't actually delete the address, we just deactivated it, because it will still be referenced by a GrowerAddress.
			UPDATE Address
			SET DateDeactivated = CURRENT_TIMESTAMP
			WHERE Guid=@AddressGuid

			SET @RowCount=@@ROWCOUNT

			IF @RowCount != 1
				BEGIN
					EXECUTE EventLogAdd
						@Comment='RowCount after deleting Address was not 1',
						@IntValue=@RowCount,
						@LogTypeLookupCode='RowDeleteError',
						@ProcessLookupCode='GrowerAddressDelete',
						@UserGuid=@UserGuid,
						@GuidValue=@OriginalEventLogGuid

					SET @Success = 0
				END
			ELSE
				SET @AddressGuid = NULL
		END

	SET @Success = 1

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('Success',@Success,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='GrowerAddressDelete',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@XmlData=@ReturnValues