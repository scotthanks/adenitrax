﻿


CREATE PROCEDURE [dbo].[catalog_CategoriesGet]

AS
	
	declare @cats as table ( ProgramTypeGuid uniqueidentifier);
		insert into @cats (ProgramTypeGuid)
			SELECT  distinct CategoryTable.ProgramTypeGuid
				FROM ProgramTypeView CategoryTable
				join ProductFormCategoryProgramTypeCombinationView CombinationView 
					on CategoryTable.ProgramTypeCode = CombinationView.ProgramTypeCode;
		Select ProgramTypeGuid as CategoryGuid, ProgramTypeCode as CategoryCode, ProgramTypeName as CategoryName from ProgramTypeView 
			where ProgramTypeGuid IN (select * from @cats)