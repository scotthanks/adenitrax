﻿



CREATE PROCEDURE [dbo].[CancelledOrdersGet]
	@UserGuid AS uniqueidentifier
AS

SELECT 
o.ID as OrderID
,ov.GrowerOrderGuid
,o.OrderNo
,o.CustomerPoNo
,right(ov.ShipWeekCode,2) + '|' + left(ov.ShipWeekCode,4) as ShipWeekString
,pfc.Name as ProductFormCategorymName
,pt.Name as ProgramTypeName
,o.PromotionalCode
,o.Description as OrderDescription
,g.Code as GrowerCode
,g.Name as GrowerName
,ov.GrowerOrderStatusLookupCode as GrowerOrderStatusCode
,ov.GrowerOrderStatusLookupName as GrowerOrderStatusName
,ov.QtyOrderedCount as OrderQty
FROM Person p
JOIN GrowerOrderSummaryByShipWeekGroupByOrderView ov on p.GrowerGuid = ov.GrowerGuid
JOIN GrowerOrder o on ov.growerorderguid = o.guid
JOIN shipweek sw on o.shipweekguid = sw.guid
JOIN Grower g on o.GrowerGuid = g.Guid
JOIN ProductFormCategory pfc on o.ProductFormCategoryGuid = pfc.Guid
JOIN ProgramType pt on o.ProgramTypeGuid = pt.Guid
JOIN Lookup lol on ov.LowestOrderLineStatusLookupGuid  = lol.Guid
Where
p.UserGuid = @UserGuid
and lol.SortSequence = 9                                     
Order by sw.ContinuousWeekNumber