﻿









CREATE PROCEDURE [dbo].[B2BSupplierOrderAndLinesUpdate]
	 @GrowerOrderGuid as uniqueidentifier,
	 @SellerCode as nVarchar(10),
	 @SupplierCode as nVarChar(10)

AS
IF @SellerCode = 'DMO'
BEGIN
	--Update relevant order lines then the supplier order 
	

	Update OrderLine Set 
		OrderLineStatusLookupGuid = (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/SupplierNotified')
	WHERE Guid in (
		Select olv.OrderLineGuid
		FROM 
			OrderLineView olv
			Join OrderLine ol on ol.Guid = olv.OrderLineGuid
			Join SupplierOrder so on ol.SupplierOrderGuid = so.Guid
		WHERE
			olv.GrowerOrderGuid = @GrowerOrderGuid
			AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/PreCart')
			AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Cancelled')
			AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Shipped')
			AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Write Off')
			AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/Pending')
			AND ol.OrderLineStatusLookupGuid != (SELECT Guid from Lookup where path = 'Code/OrderLineStatus/CustomerNotifiedAfterSupplierConfirmed')
		  
	) 
		
	Update SupplierOrder Set DateLastSentToSupplier = getdate()
	where GrowerOrderGuid = @GrowerOrderGuid
END	
ELSE
BEGIN
	print 'To Do'
END
		

		select path from lookup where path like '%orderlinestatus%'