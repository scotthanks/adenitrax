﻿


CREATE procedure [dbo].[GrowerOrderHistoryDataGet]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NVARCHAR(56) = NULL,
	@GrowerOrderGuid AS UNIQUEIDENTIFIER
AS
	



	SELECT GrowerOrderHistory.*
	FROM GrowerOrderHistory
	WHERE GrowerOrderGuid = @GrowerOrderGuid
	
	ORDER BY EventDate