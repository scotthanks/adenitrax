﻿
--EXAMPLE:
--DECLARE @Begin AS DATETIME
--SET @Begin = CURRENT_TIMESTAMP

--DECLARE @LookupGuid AS UNIQUEIDENTIFIER
--DELETE Lookup WHERE Code = 'LookupGetOrAddTest'
--DELETE Lookup WHERE Path = 'TestData/SubDir'

--EXECUTE LookupGetOrAdd @ParentPath='TestData',@Code='LookupGetOrAddTest',@LookupGuid=@LookupGuid OUT
--SELECT @LookupGuid AS LookupGuid
--SELECT * FROM Lookup WHERE Code = 'LookupGetOrAddTest'

--EXECUTE LookupGetOrAdd @ParentPath='TestData',@Code='LookupGetOrAddTest',@LookupGuid=@LookupGuid OUT
--SELECT @LookupGuid AS LookupGuid
--SELECT * FROM Lookup WHERE Code = 'LookupGetOrAddTest'

--DELETE Lookup WHERE Code = 'LookupGetOrAddTest'

--EXECUTE LookupGetOrAdd @ParentPath='TestData/SubDir',@Code='LookupGetOrAddTest',@LookupGuid=@LookupGuid OUT
--SELECT @LookupGuid AS LookupGuid
--SELECT * FROM Lookup WHERE Code = 'LookupGetOrAddTest'

--EXECUTE LookupGetOrAdd @ParentPath='TestData/SubDir',@Code='LookupGetOrAddTest',@LookupGuid=@LookupGuid OUT
--SELECT @LookupGuid AS LookupGuid
--SELECT * FROM Lookup WHERE Code = 'LookupGetOrAddTest'

--SELECT * FROM EventLogView WHERE ProcessLookupCode = 'LookupAdd' AND EventTime >= @Begin

--DELETE Lookup WHERE Code = 'LookupGetOrAddTest'
--DELETE Lookup WHERE Path = 'TestData/SubDir'

CREATE procedure [dbo].[LookupGetOrAdd]
	@ParentPath AS NVARCHAR(200),
	@Code AS NVARCHAR(50),
	@Default AS NVARCHAR(50) = 'Debug',
	@LookupGuid AS UNIQUEIDENTIFIER OUT
AS
	SET @LookupGuid = NULL

	SET @Default = LTRIM(RTRIM(@Default))
	IF @Default = ''
		SET @Default = NULL

	SET @Code = LTRIM(RTRIM(@Code))
	IF @Code = ''
		SET @Code = NULL
	IF @Code IS NULL
		SET @Code = @Default

	IF @Code IS NULL
		RETURN

	DECLARE @ParentGuid AS UNIQUEIDENTIFIER
	DECLARE @ParentCount AS INT
	SELECT
		@ParentGuid = Guid,
		@ParentCount = COUNT(*) 
	FROM Lookup 
	WHERE Path = @ParentPath
	GROUP BY
		Guid,
		Path

	IF @ParentGuid IS NULL
		BEGIN
			DECLARE @NRows AS INT
			SELECT @NRows=COUNT(*) FROM SPLIT('/',@ParentPath)
			IF @NRows = 2
				BEGIN
					DECLARE @ParentParentPath AS NVARCHAR(200)
					SELECT @ParentParentPath=Value FROM SPLIT('/',@ParentPath) WHERE RowId = @NRows - 1

					-- Create the base folder if it's not there.
					DECLARE @ParentParentGuid AS UNIQUEIDENTIFIER
					SELECT @ParentParentGuid = Guid FROM Lookup WHERE ParentLookupGuid IS NULL and Code = @ParentParentPath
					IF @ParentParentGuid IS NULL
						EXECUTE LookupAdd
							@ParentExpression='',
							@Code=@ParentParentPath

					-- Automatically create the parent if it's not there.
					PRINT 'Create parent with path of ' + @ParentPath
					DECLARE @ParentCode AS NVARCHAR(200)
					SELECT @ParentCode=Value FROM SPLIT('/',@ParentPath) WHERE RowId = @NRows
					EXECUTE LookupAdd
						@ParentExpression=@ParentParentPath, 
						@Code=@ParentCode, 
						@LookupGuid=@ParentGuid OUT, 
						@ReturnRecordset=0

					SET @ParentCount = 1

					SELECT @ParentPath = Path FROM Lookup WHERE Guid = @ParentGuid
				END
		END

	IF @ParentCount = 1 AND @ParentGuid IS NOT NULL
		BEGIN
			SELECT @LookupGuid = Guid
			FROM Lookup
			WHERE
				ParentLookupGuid = @ParentGuid AND
				Code = @Code

			IF @LookupGuid IS NULL
				EXECUTE LookupAdd
					@ParentExpression = @ParentPath,
					@Code = @Code,
					@Name = '(generated)',
					@LookupGuid = @LookupGuid OUT,
					@ReturnRecordset = 0
		END