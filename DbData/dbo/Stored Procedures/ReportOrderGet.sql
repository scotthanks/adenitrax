﻿












CREATE PROCEDURE [dbo].[ReportOrderGet]
	@GrowerGuid AS UNIQUEIDENTIFIER = null,
	@SupplierGuid AS UNIQUEIDENTIFIER = null,
	@FromYear AS Integer = 2016,
	@FromWeek AS Integer = 1,
	@ToYear AS Integer = 2016,
	@ToWeek AS Integer = 53

AS
Declare @startWeek bigint
Declare @endWeek bigint
IF @FromYear = 2017 and @FromWeek = 53
	select @startWeek = ContinuousWeekNumber from shipweek where year = @FromYear and week = 52
ELSE
	select @startWeek = ContinuousWeekNumber from shipweek where year = @FromYear and week = @FromWeek
IF @ToYear = 2017 and @ToWeek = 53
	select @endWeek = ContinuousWeekNumber from shipweek where year = @ToYear and week = 52
else
	select @endWeek = ContinuousWeekNumber from shipweek where year = @ToYear and week = @ToWeek

SELECT g.Name AS GrowerName
,su.Name as SupplierName
,o.OrderNo
,customerpono
,sw.year
,sw.week
,ol.LineNumber
,v.Name as Variety
,pf.Name as ProductForm
,p.SupplierIdentifier
,p.SupplierDescription
,ol.QtyOrdered
,ol.QtyShipped
,tr.Code as TagRatio
,osl.name as OrderLineStatus
,Cast(ISNULL(gpp.CuttingCost,0) as decimal(8,4)) as CuttingCost
,Cast(ISNULL(gpp.RoyaltyCost,0) as decimal(8,4)) as RoyaltyCost
,Cast(ISNULL(gpp.FreightCost,0) as decimal(8,4)) as FreightCost
,Cast(ISNULL(gpp.TagCost,0) as decimal(8,4)) as TagCost
,a.StreetAddress1 as ShipToStreetAddress1
,st.Code as ShipToState

from growerorder o
Join SupplierOrder so on so.growerorderguid = o.guid
Join Supplier su on so.SupplierGuid = su.guid
Join Orderline OL on  OL.supplierorderguid = so.guid
Join Product p on ol.productguid = p.guid
Join Program pr on p.programguid = pr.guid
join ShipWeek sw on o.ShipweekGuid = sw.guid
Join GrowerAddress ga on o.ShipToAddressGuid = ga.Guid
Join Address a on ga.AddressGuid = a.Guid
Join State st on a.StateGuid = st.Guid
left Join ProgramSeason ps on ps.Programguid = pr.Guid
			And sw.MondayDate Between ps.StartDate and ps.EndDate

--Join Seller se on pr.sellerguid = se.guid
left Join productform pf on p.productformguid = pf.guid
left Join variety v on p.varietyguid = v.guid
left Join supplier s on so.supplierguid = s.guid
LEFT JOIN Grower g ON o.GrowerGuid  = g.guid
LEFT JOIN Person Pe on pe.guid = o.PersonGuid
LEFT JOIN lookup tr on so.TagRatioLookupGuid = tr.guid
LEFT JOIN lookup sm on so.shipmethodLookupGuid = sm.guid
left JOIN lookup osl ON ol.OrderLineStatusLookupGuid = osl.Guid
left JOIN lookup gsl ON o.GrowerOrderStatusLookupGuid = gsl.Guid
LEFT JOIN GrowerProductProgramSeasonPrice gpp on g.Guid = gpp.GrowerGuid
	AND p.Guid = gpp.ProductGuid
	AND ps.Guid = gpp.ProgramSeasonGuid
WHERE 1 = 1
--and se.Code = 'DMO'
and (g.Guid = @GrowerGuid OR @GrowerGuid = '00000000-0000-0000-0000-000000000000')
and (su.Guid = @SupplierGuid OR @SupplierGuid = '00000000-0000-0000-0000-000000000000')
and sw.ContinuousWeekNumber between @startWeek and @endWeek
ORDER BY sw.Year,sw.Week,g.Name,s.Name,o.OrderNo ,ol.LineNumber