﻿
CREATE PROCEDURE [dbo].[ProgramTypeAndProductFormCategoryDataGet]
AS
	SELECT ProgramType.*
	FROM ProgramType
	WHERE DateDeactivated IS NULL

	SELECT ProductFormCategory.*
	FROM ProductFormCategory
	WHERE DateDeactivated IS NULL

	SELECT ProductFormCategoryProgramTypeCombinationView.*
	FROM ProductFormCategoryProgramTypeCombinationView