﻿









--EXAMPLES:

--DECLARE @PersonGuid AS UNIQUEIDENTIFIER
--DECLARE @GrowerGuid AS UNIQUEIDENTIFIER
--DECLARE @GrowerAddressGuid AS UNIQUEIDENTIFIER
--DECLARE @AddressGuid AS UNIQUEIDENTIFIER
--DECLARE @GrowerPersonTripleGuid AS UNIQUEIDENTIFIER
--DECLARE @UserGuid AS UNIQUEIDENTIFIER
--SET @UserGuid='7714B7E5-7148-404B-9E2E-93EE8B7E3B63'

--EXECUTE PersonRegister
--	@UserGuid=@UserGuid,
--	@CompanyName='Test Grower 6',
--	@StreetAddress1='6666 Grower Street',
--	@StreetAddress2='',
--	@City='Grower City',
--	@StateCode='GA',
--	@GrowerEmail='grower@grower.com',
--	@ZipCode='12345',
--	@GrowerPhoneAreaCode=123,
--	@GrowerPhoneNumber=1236666,
--	@GrowerTypeLookupCode='Retail',
--	@FirstName='Test',
--	@LastName='Grower',
-- 	@PersonEmail='person@person.com',
--	@Password='thisisit',
--	@PersonPhoneAreaCode=234,
--	@PersonPhoneNumber=2346666,
--	@PersonTypeLookupCode='Grower',
--	@IsAuthorizedToPlaceOrders=1,
--	@SubscribeToNewsLetter=1,
--	@PersonGuid=@PersonGuid OUT,
--	@GrowerGuid=@GrowerGuid OUT,
--	@GrowerAddressGuid=@GrowerAddressGuid OUT,
--	@AddressGuid=@AddressGuid OUT,
--	@GrowerPersonTripleGuid=@GrowerPersonTripleGuid OUT,
--	@Delete=0

--SELECT
--	@PersonGuid AS PersonGuid,
--	@GrowerGuid AS GrowerGuid,
--	@GrowerAddressGuid AS GrowerAddressGuid,
--	@AddressGuid AS AddressGuid,
--	@GrowerPersonTripleGuid AS GrowerPersonTripleGuid

--EXECUTE PersonRegister
--	@UserGuid=@UserGuid,
--	@CompanyName='Test Grower',
--	@StreetAddress1='123 Grower Street',
--	@StreetAddress2='',
--	@City='Grower City',
--	@StateCode='GA',
--	@GrowerEmail='grower@grower.com',
--	@ZipCode='12345',
--	@GrowerPhoneAreaCode=123,
--	@GrowerPhoneNumber=1231234,
--	@GrowerTypeLookupCode='Retail',
--	@FirstName='Test',
--	@LastName='Grower',
-- 	@PersonEmail='person@person.com',
--	@Password='thisisit',
--	@PersonPhoneAreaCode=234,
--	@PersonPhoneNumber=2342345,
--	@PersonTypeLookupCode='Grower',
--	@IsAuthorizedToPlaceOrders=1,
--	@SubscribeToNewsLetter=1,
--	@PersonGuid=@PersonGuid OUT,
--	@GrowerGuid=@GrowerGuid OUT,
--	@GrowerAddressGuid=@GrowerAddressGuid OUT,
--	@AddressGuid=@AddressGuid OUT,
--	@GrowerPersonTripleGuid=@GrowerPersonTripleGuid OUT,
--	@Delete=1

--SELECT
--	@PersonGuid AS PersonGuid,
--	@GrowerGuid AS GrowerGuid,
--	@GrowerAddressGuid AS GrowerAddressGuid,
--	@AddressGuid AS AddressGuid,
--	@GrowerPersonTripleGuid AS GrowerPersonTripleGuid
	
--delete triple where subjectguid in (select guid from grower where name = 'test grower')
--delete person where firstname = 'test' and lastname = 'person'
--delete groweraddress where growerguid in (select guid from grower where name = 'test grower')
--delete grower where name = 'Test Grower'
--delete address where guid not in (select addressguid from grower) and guid not in (select addressguid from groweraddress) and guid not in (select addressguid from supplier)

CREATE procedure [dbo].[PersonRegister]
	@UserGuid AS UNIQUEIDENTIFIER,
	@CompanyName AS NVARCHAR(50),
	@StreetAddress1 AS NVARCHAR(50),
	@StreetAddress2 AS NVARCHAR(50),
	@City AS NVARCHAR(50),
	@StateCode AS NVARCHAR(2),
	@GrowerEmail AS NVARCHAR(50),
	@ZipCode AS NVARCHAR(12),
	@GrowerPhoneAreaCode AS DECIMAL(3),
	@GrowerPhoneNumber AS DECIMAL(7),
	@GrowerTypeLookupCode AS NVARCHAR(20),
	@FirstName AS NVARCHAR(50),
	@LastName AS NVARCHAR(50),
 	@PersonEmail AS NVARCHAR(50),
	@Password AS NVARCHAR(50),
	@PersonPhoneAreaCode AS DECIMAL(3),
	@PersonPhoneNumber AS DECIMAL(7),
	@PersonTypeLookupCode AS NVARCHAR(50),
	@IsAuthorizedToPlaceOrders AS BIT,
	@SubscribeToNewsLetter AS BIT,	
	@SubscribeToPromotions AS BIT,
	@PersonGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@GrowerGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@GrowerAddressGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@AddressGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@GrowerPersonTripleGuid AS UNIQUEIDENTIFIER = NULL OUT,
	@Delete AS BIT = 0
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('CompanyName',@CompanyName,1) +
		dbo.XmlSegment('StreetAddress1',@StreetAddress1,1) +
		dbo.XmlSegment('StreetAddress2',@StreetAddress2,1) +
		dbo.XmlSegment('City',@City,1) +
		dbo.XmlSegment('StateCode',@StateCode,1) +
		dbo.XmlSegment('ZipCode',@ZipCode,1) +
		dbo.XmlSegment('GrowerEmail',@GrowerEmail,1) +
		dbo.XmlSegment('GrowerPhoneAreaCode',@GrowerPhoneAreaCode,1) +
		dbo.XmlSegment('GrowerPhoneNumber',@GrowerPhoneNumber,1) +
		dbo.XmlSegment('GrowerTypeCode',@GrowerTypeLookupCode,1) +
		dbo.XmlSegment('FirstName',@FirstName,1) +
		dbo.XmlSegment('LastName',@LastName,1) +
		dbo.XmlSegment('PersonEmail',@PersonEmail,1) +
		dbo.XmlSegment('Password',@Password,1) +
		dbo.XmlSegment('PersonPhoneAreaCode',@PersonPhoneAreaCode,1) +
		dbo.XmlSegment('PersonPhoneNumber',@PersonPhoneNumber,1) +
		dbo.XmlSegment('RoleCode',@PersonTypeLookupCode,1) +
		dbo.XmlSegment('IsAuthorizedToPlaceOrders',@IsAuthorizedToPlaceOrders,1) +
		dbo.XmlSegment('SubscribeToNewsLetter',@SubscribeToNewsLetter,1) +
		dbo.XmlSegment('SubscribeToPromotions',@SubscribeToPromotions,1) +
		dbo.XmlSegment('PersonGuid',@PersonGuid,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('GrowerAddressGuid',@GrowerAddressGuid,1) +
		dbo.XmlSegment('AddressGuid',@AddressGuid,1) +
		dbo.XmlSegment('GrowerPersonTripleGuid',@GrowerPersonTripleGuid,1) +
		dbo.XmlSegment('Delete',@Delete,1),
		0
	)

	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserCode=@PersonEmail,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='PersonRegister',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	IF @CompanyName IS NULL SET @CompanyName = ''
	IF @StreetAddress1 IS NULL SET @StreetAddress1 = ''
	IF @StreetAddress2 IS NULL SET @StreetAddress2 = ''
	IF @City IS NULL SET @City = ''
	IF @StateCode IS NULL SET @StateCode = ''
	IF @ZipCode IS NULL SET @ZipCode = ''
	IF @GrowerEmail IS NULL SET @GrowerEmail = ''
	IF @GrowerPhoneAreaCode IS NULL SET @GrowerPhoneAreaCode = 0
	IF @GrowerPhoneNumber IS NULL SET @GrowerPhoneNumber = 0
	IF @GrowerTypeLookupCode IS NULL SET @GrowerTypeLookupCode = 0
	IF @FirstName IS NULL SET @FirstName = ''
	IF @LastName IS NULL SET @LastName = ''
	IF @PersonEmail IS NULL SET @PersonEmail = ''
	IF @Password IS NULL SET @Password = ''
	IF @PersonPhoneAreaCode IS NULL SET @PersonPhoneAreaCode = 0
	IF @PersonPhoneNumber IS NULL SET @PersonPhoneNumber = 0
	IF @PersonTypeLookupCode IS NULL SET @PersonTypeLookupCode = ''
	IF @IsAuthorizedToPlaceOrders IS NULL SET @IsAuthorizedToPlaceOrders = 0
	IF @SubscribeToNewsLetter IS NULL SET @SubscribeToNewsLetter = 0
	IF @SubscribeToPromotions IS NULL SET @SubscribeToPromotions = 0
	
	SET @CompanyName = LTRIM(RTRIM(@CompanyName))
	SET @StreetAddress1 = LTRIM(RTRIM(@StreetAddress1))
	SET @StreetAddress2 = LTRIM(RTRIM(@StreetAddress2))
	SET @City = LTRIM(RTRIM(@City))
	SET @StateCode = LTRIM(RTRIM(@StateCode))
	SET @ZipCode = LTRIM(RTRIM(@ZipCode))
	SET @GrowerEmail = LTRIM(RTRIM(@GrowerEmail))
	SET @GrowerTypeLookupCode = LTRIM(RTRIM(@GrowerTypeLookupCode))
	SET @FirstName = LTRIM(RTRIM(@FirstName))
	SET @LastName = LTRIM(RTRIM(@LastName))
	SET @PersonEmail = LTRIM(RTRIM(@PersonEmail))
	SET @Password = LTRIM(RTRIM(@Password))
	SET @PersonTypeLookupCode = LTRIM(RTRIM(@PersonTypeLookupCode))

	IF 
		@CompanyName = '' OR
		@StreetAddress1 = '' OR
		@City = '' OR
		@StateCode = '' OR
		@ZipCode = '' OR
		@GrowerEmail = '' OR
		@GrowerPhoneAreaCode = 0 OR
		@GrowerPhoneNumber = 0 OR
		@GrowerTypeLookupCode = '' OR
		@FirstName = '' OR
		@LastName = '' OR
		@PersonEmail = '' OR
		@PersonPhoneAreaCode = 0 OR
		@PersonPhoneNumber = 0 OR
		@Password = '' OR
		@PersonTypeLookupCode = ''
			BEGIN
				EXECUTE EventLogAdd
					@Comment='None of the input parameters (except StreetAddress2) may be null, blank or zero.',
					@LogTypeLookupCode='ParameterError',
					@ProcessLookupCode='PersonRegister',
					@ObjectGuid=@OriginalEventLogGuid,
					@UserCode=@PersonEmail

				RETURN
			END

	DECLARE @PersonTypeLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserCode=@PersonEmail,
		@Path='Code/PersonType',
		@LookupCode=@PersonTypeLookupCode,
		@ProcessLookupCode='PersonRegister',
		@LookupGuid=@PersonTypeLookupGuid OUT

	IF @PersonTypeLookupGuid IS NULL
		RETURN

	DECLARE @GrowerTypeLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserCode=@PersonEmail,
		@Path='Code/GrowerType',
		@LookupCode=@GrowerTypeLookupCode,
		@ProcessLookupCode='PersonRegister',
		@LookupGuid=@GrowerTypeLookupGuid OUT

	IF @GrowerTypeLookupGuid IS NULL
		RETURN

	DECLARE @DefaultPaymentTypeLookupGuid AS UNIQUEIDENTIFIER
	EXECUTE LookupGetGuid
		@UserCode=@PersonEmail,
		@Path='Code/PaymentType',
		@LookupCode='Invoice',
		@ProcessLookupCode='PersonRegister',
		@LookupGuid=@DefaultPaymentTypeLookupGuid OUT

	IF @DefaultPaymentTypeLookupGuid IS NULL
		RETURN

	DECLARE @ErrorCount AS INTEGER
	SET @ErrorCount = 0

	IF @Delete IS NOT NULL AND @Delete = 0
		BEGIN

			SET @PersonGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
			SET @GrowerGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
			SET @GrowerAddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
			SET @AddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
			SET @GrowerPersonTripleGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)

			BEGIN TRANSACTION

				SET @PersonGuid = NEWID()

				INSERT INTO Person
				(
					Guid,
					DateDeactivated,
					FirstName,
					LastName,
					UserGuid,
					UserCode,
					Email,
					PhoneAreaCode,
					Phone,
					SubscribeNewsletter,
					PersonTypeLookupGuid,
					PersonGetsOrderEmail,
					SubscribePromotions
				)
				VALUES
				(
					@PersonGuid,
					NULL,
					@FirstName,
					@LastName,
					@UserGuid,
					@PersonEmail,
					@PersonEmail,
					@PersonPhoneAreaCode,
					@PersonPhoneNumber,
					@SubscribeToNewsletter,
					@PersonTypeLookupGuid,
					1,
					@SubscribeToPromotions
				)

				IF @@ROWCOUNT != 1
					BEGIN
						SET @ErrorCount = @ErrorCount + 1

						EXECUTE EventLogAdd
							@Comment='Person was not added.',
							@LogTypeLookupCode='RowAddError',
							@ProcessLookupCode='PersonRegister',
							@ObjectTypeLookupCode='Person',
							@ObjectGuid=@OriginalEventLogGuid,
							@IntValue=@ErrorCount,
							@UserCode=@PersonEmail
					END

				EXECUTE AddressAdd
					@UserGuid=@UserGuid,
					@Name=@CompanyName,
					@StreetAddress1=@StreetAddress1,
					@StreetAddress2=@StreetAddress2,
					@City=@City,
					@StateCode=@StateCode,
					@ZipCode=@ZipCode,
					@AddressGuid=@AddressGuid OUT

				IF @AddressGuid IS NULL
					EXECUTE EventLogAdd
						@Comment='Address was not added.',
						@LogTypeLookupCode='RowAddError',
						@ProcessLookupCode='GrowerAddressAdd',
						@UserGuid=@UserGuid,
						@GuidValue=@OriginalEventLogGuid

				SET @GrowerGuid=NEWID()
				-- KLUDGE (We have to set the grower id to something before we get the Id)
				DECLARE @TempGrowerCode AS NCHAR(20)
				SET @TempGrowerCode = LEFT(CAST(@GrowerGuid AS NVARCHAR(50)), 20)

				DECLARE @RowCount AS INTEGER

				INSERT INTO Grower
				(
					Guid,
					DateDeactivated,
					Name,
					Code,
					IsActivated,
					AddressGuid,
					AllowSubstitutions,
					CreditLimit,
					CreditLimitTemporaryIncrease,
					CreditLimitTemporaryIncreaseExpiration,
					Email,
					PhoneAreaCode,
					Phone,
					GrowerTypeLookupGuid,
					DefaultPaymentTypeLookupGuid,
					SalesTaxNumber,
					RequestedLineOfCreditStatusLookupGuid,
					SellerGuid,
					OnlySpeciesIncluded
				)
				VALUES
				(
					@GrowerGuid,
					NULL,
					@CompanyName,
					@TempGrowerCode,					
					1, --IsActivated
					@AddressGuid,
					1, --AllowSubstitutions
					2000,
					0,
					NULL,
					@GrowerEmail,
					@GrowerPhoneAreaCode,
					@GrowerPhoneNumber,
					@GrowerTypeLookupGuid,
					@DefaultPaymentTypeLookupGuid,
					'', --SalesTaxNumber
					(select guid from lookup where path = 'Code/CreditAppStatus/None'),
					(select guid from seller where code = 'EPS'),
					0
				)

				SET @RowCount=@@ROWCOUNT

				IF @@ROWCOUNT != 1
					BEGIN
						SET @ErrorCount = @ErrorCount + 1

						EXECUTE EventLogAdd
							@Comment='Grower was not added.',
							@LogTypeLookupCode='RowAddError',
							@ProcessLookupCode='PersonRegister',
							@ObjectTypeLookupCode='Grower',
							@ObjectGuid=@OriginalEventLogGuid,
							@IntValue=@RowCount,
							@UserCode=@PersonEmail
					END
				ELSE
					BEGIN
						UPDATE Grower 
							SET Code = CAST(7000000 + Id AS NVARCHAR(20)) 
						WHERE Guid=@GrowerGuid
					END

				
				INSERT INTO GrowerSeller VALUES
				(
					newID(),
					@GrowerGuid,
					(select guid from seller where code = 'EPS'),
					1
				)

				EXECUTE GrowerAddressAddExisting
					@UserGuid=@UserGuid,
					@GrowerGuid=@GrowerGuid,
					@AddressGuid=@AddressGuid,
					@PhoneAreaCode=@GrowerPhoneAreaCode,
					@Phone=@GrowerPhoneNumber,
					@SpecialInstructions='',
					@GrowerAddressGuid=@GrowerAddressGuid OUT

				IF @GrowerAddressGuid IS NULL
					BEGIN
						SET @ErrorCount = @ErrorCount + 1

						EXECUTE EventLogAdd
							@Comment='GrowerAddress was not added.',
							@LogTypeLookupCode='RowAddError',
							@ProcessLookupCode='PersonRegister',
							@ObjectTypeLookupCode='GrowerAddress',
							@ObjectGuid=@OriginalEventLogGuid,
							@IntValue=@ErrorCount,
							@UserCode=@PersonEmail
					END
				
				--updated by Scott Nov. 6, 2013
				UPDATE Person set GrowerGuid = @GrowerGuid where Guid = @PersonGuid
				
				--Will take this out and delete triple records soon
				EXECUTE TripleAdd 
					@PredicateLookupCode='GrowerPerson', 
					@SubjectGuid=@GrowerGuid, 
					@ObjectGuid=@PersonGuid,
					@TripleGuid=@GrowerPersonTripleGuid OUT,
					@ReturnRecordset=0

				IF @GrowerPersonTripleGuid IS NULL
					BEGIN
						SET @ErrorCount = @ErrorCount + 1

						EXECUTE EventLogAdd
							@Comment='GrowerPersonTriple was not added.',
							@LogTypeLookupCode='RowAddError',
							@ProcessLookupCode='PersonRegister',
							@ObjectTypeLookupCode='GrowerPersonTriple',
							@ObjectGuid=@OriginalEventLogGuid,
							@IntValue=@ErrorCount,
							@UserCode=@PersonEmail
					END

			IF @ErrorCount > 1
				BEGIN
					ROLLBACK TRANSACTION

					SET @PersonGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
					SET @GrowerGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
					SET @GrowerAddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
					SET @AddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
					SET @GrowerPersonTripleGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)

					EXECUTE EventLogAdd
						@Comment='One or more records were not added, registration rolled back.',
						@LogTypeLookupCode='RollBack',
						@ProcessLookupCode='PersonRegister',
						@ObjectGuid=@OriginalEventLogGuid,
						@IntValue=@ErrorCount,
						@UserCode=@PersonEmail
				END
			ELSE
				BEGIN
					COMMIT TRANSACTION

					SELECT * FROM Person WHERE Guid = @PersonGuid;

					SELECT * FROM Grower WHERE Guid = @GrowerGuid;

					SELECT * FROM GrowerAddress WHERE GrowerGuid = @GrowerGuid;

					SELECT * FROM Address WHERE Guid IN (SELECT AddressGuid FROM GrowerAddress WHERE GrowerGuid = @GrowerGuid);

					SELECT * FROM State WHERE Guid IN 
					(SELECT StateGuid FROM Address WHERE Guid IN (SELECT AddressGuid FROM GrowerAddress WHERE GrowerGuid = @GrowerGuid));

					SELECT * FROM Country WHERE Guid IN 
					(SELECT CountryGuid FROM State WHERE Guid IN (Select StateGuid FROM Address WHERE Guid IN (SELECT AddressGuid FROM GrowerAddress WHERE GrowerGuid = @GrowerGuid)));

					SELECT * FROM Lookup WHERE Guid IN (@GrowerTypeLookupGuid, @PersonTypeLookupGuid)
				END
		END
	ELSE
		BEGIN
				SET @PersonGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
				SET @GrowerGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
				SET @GrowerAddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
				SET @AddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
				SET @GrowerPersonTripleGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)

				DELETE Triple WHERE Guid=@GrowerPersonTripleGuid

				IF @@ROWCOUNT != 1
					BEGIN
						SET @ErrorCount = @ErrorCount + 1

						EXECUTE EventLogAdd
							@Comment='GrowerPersonTriple was not deleted.',
							@LogTypeLookupCode='RowDeleteError',
							@ProcessLookupCode='PersonRegister',
							@ObjectTypeLookupCode='GrowerPersonTriple',
							@ObjectGuid=@OriginalEventLogGuid,
							@IntValue=@ErrorCount,
							@UserCode=@PersonEmail
					END
				ELSE
					SET @GrowerPersonTripleGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)

				DECLARE @GrowerAddressDeleteSuccess AS BIT

				IF @GrowerAddressGuid IS NOT NULL
					BEGIN
						EXECUTE GrowerAddressDelete
							@UserGuid=@UserGuid,
							@GrowerAddressGuid=@GrowerAddressGuid,
							@Success=@GrowerAddressDeleteSuccess OUT


						IF @@ROWCOUNT != 1
							BEGIN
								SET @ErrorCount = @ErrorCount + 1

								EXECUTE EventLogAdd
									@Comment='GrowerAddress was not deleted.',
									@LogTypeLookupCode='RowDeleteError',
									@ProcessLookupCode='PersonRegister',
									@ObjectTypeLookupCode='GrowerAddress',
									@ObjectGuid=@OriginalEventLogGuid,
									@IntValue=@ErrorCount,
									@UserCode=@PersonEmail
							END
						ELSE
							SET @GrowerAddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
					END
				ELSE IF @AddressGuid IS NOT NULL
					BEGIN
						DELETE Address WHERE Guid=@AddressGuid

						IF @@ROWCOUNT != 1
							BEGIN
								SET @ErrorCount = @ErrorCount + 1

								EXECUTE EventLogAdd
									@Comment='Address was not deleted.',
									@LogTypeLookupCode='RowDeleteError',
									@ProcessLookupCode='PersonRegister',
									@ObjectTypeLookupCode='Address',
									@ObjectGuid=@OriginalEventLogGuid,
									@IntValue=@ErrorCount,
									@UserCode=@PersonEmail
							END
						ELSE
							SET @AddressGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
					END

				DELETE Grower WHERE Guid=@GrowerGuid

				IF @@ROWCOUNT != 1
					BEGIN
						SET @ErrorCount = @ErrorCount + 1

						EXECUTE EventLogAdd
							@Comment='Grower was not deleted.',
							@LogTypeLookupCode='RowDeleteError',
							@ProcessLookupCode='PersonRegister',
							@ObjectTypeLookupCode='Grower',
							@ObjectGuid=@OriginalEventLogGuid,
							@IntValue=@ErrorCount,
							@UserCode=@PersonEmail
					END
				ELSE
					SET @GrowerGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)

				DELETE Person WHERE Guid=@PersonGuid

				IF @@ROWCOUNT != 1
					BEGIN
						SET @ErrorCount = @ErrorCount + 1

						EXECUTE EventLogAdd
							@Comment='Person was not deleted.',
							@LogTypeLookupCode='RowDeleteError',
							@ProcessLookupCode='PersonRegister',
							@ObjectTypeLookupCode='Person',
							@ObjectGuid=@OriginalEventLogGuid,
							@IntValue=@ErrorCount,
							@UserCode=@PersonEmail
					END
				ELSE
					SET @PersonGuid = CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
		END

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('PersonGuid',@PersonGuid,1) +
		dbo.XmlSegment('GrowerGuid',@GrowerGuid,1) +
		dbo.XmlSegment('GrowerAddressGuid',@GrowerAddressGuid,1) +
		dbo.XmlSegment('AddressGuid',@AddressGuid,1) +
		dbo.XmlSegment('GrowerAddressTripleGuid',@AddressGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='PersonRegister',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserCode=@PersonEmail,
		@XmlData=@ReturnValues