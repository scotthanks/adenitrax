﻿
CREATE PROCEDURE [dbo].[AddressAdd]
	@UserGuid AS UNIQUEIDENTIFIER,
	@Name AS NVARCHAR(50),
	@StreetAddress1 AS NVARCHAR(50),
	@StreetAddress2 AS NVARCHAR(50),
	@City AS NVARCHAR(50),
	@StateCode AS NCHAR(2),
	@ZipCode AS NCHAR(12),
	@AddressGuid AS UNIQUEIDENTIFIER OUT
AS
	DECLARE @Parms AS NVARCHAR(MAX)
	SET @Parms = dbo.XmlSegment
	(
		'Parameters',
		dbo.XmlSegment('UserGuid',@UserGuid,1) +
		dbo.XmlSegment('Name',@Name,1) +
		dbo.XmlSegment('StreetAddress1',@StreetAddress1,1) +
		dbo.XmlSegment('StreetAddress2',@StreetAddress2,1) +
		dbo.XmlSegment('City',@City,1) +
		dbo.XmlSegment('StateCode',@StateCode,1) +
		dbo.XmlSegment('ZipCode',@ZipCode,1),
		0
	)
	
	DECLARE @OriginalEventLogGuid AS UNIQUEIDENTIFIER
	EXECUTE EventLogAdd
		@UserGuid=@UserGuid,
		@LogTypeLookupCode='ProcedureCall',
		@ProcessLookupCode='AddressAdd',
		@ObjectTypeLookupCode='Parameters',
		@XmlData=@Parms,
		@AddedEventLogGuid=@OriginalEventLogGuid OUT

	DECLARE @StateGuid AS UNIQUEIDENTIFIER
	SELECT @StateGuid = StateGuid
	FROM StateView WHERE StateCode=@StateCode

	IF @StateGuid IS NULL
		EXECUTE EventLogAdd
			@Comment='The State code was not found.',
			@LogTypeLookupCode='ParameterError',
			@ProcessLookupCode='GrowerAddressAdd',
			@UserGuid=@UserGuid,
			@GuidValue=@OriginalEventLogGuid

	DECLARE @RowCount AS INTEGER

	SET @AddressGuid = NEWID()

	INSERT INTO Address
	(
		Guid,
		Name,
		StreetAddress1,
		StreetAddress2,
		City,
		StateGuid,
		ZipCode
	)
	VALUES
	(
		@AddressGuid,
		@Name,
		@StreetAddress1,
		@StreetAddress2,
		@City,
		@StateGuid,
		@ZipCode
	)

	SET @RowCount=@@ROWCOUNT

	IF @RowCount != 1
		BEGIN
			EXECUTE EventLogAdd
				@Comment='RowCount after adding Address was not 1',
				@IntValue=@RowCount,
				@LogTypeLookupCode='RowAddError',
				@ProcessLookupCode='GrowerAddressAdd',
				@UserGuid=@UserGuid,
				@GuidValue=@OriginalEventLogGuid

			SET @AddressGuid = NULL
		END
	ELSE
		SET @AddressGuid = @AddressGuid

	DECLARE @ReturnValues AS NVARCHAR(MAX)
	SET @ReturnValues = dbo.XmlSegment
	(
		'ReturnValues',
		dbo.XmlSegment('AddressGuid',@AddressGuid,1),
		0
	)

	EXECUTE EventLogAdd
		@LogTypeLookupCode='ProcedureCallComplete',
		@ProcessLookupCode='AddressAdd',
		@ObjectTypeLookupCode='EventLog',
		@ObjectGuid=@OriginalEventLogGuid,
		@UserGuid=@UserGuid,
		@XmlData=@ReturnValues