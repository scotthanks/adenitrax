﻿create procedure dbo.BillingListGet
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@PayTypeCode AS NCHAR(50),
	@OrderStatusCode AS NCHAR(50),
	@OrderGuid AS UNIQUEIDENTIFIER = NULL
AS
