﻿



CREATE PROCEDURE [dbo].[GrowerCreditCardUpdate]
	@UserGuid AS UNIQUEIDENTIFIER,
	@GrowerCreditCardGuid AS UNIQUEIDENTIFIER,
	@CardDescription AS NVARCHAR(100),
	@DateDeactivated as DateTime
AS
	




		
	
		UPDATE GrowerCreditCard
		SET
			CardDescription = Case when @CardDescription <> '_NA' then @CardDescription else CardDescription end ,
			DateDeactivated = case when @DateDeactivated > '1/1/2000' then @DateDeactivated else NULL end
		WHERE Guid=@GrowerCreditCardGuid