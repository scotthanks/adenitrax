﻿

--EXAMPLE:

--DECLARE @AddedEventLogGuid AS UNIQUEIDENTIFIER

--EXECUTE EventLogAdd @Comment='Test EventLogAdd', @AddedEventLogGuid=@AddedEventLogGuid OUT 

--SELECT @AddedEventLogGuid AS AddedEventLogGuid

CREATE PROCEDURE [dbo].[EventLogAdd]
	@UserGuid AS UNIQUEIDENTIFIER = NULL,
	@UserCode AS NCHAR(56) = NULL,
	@Comment AS NVARCHAR(2000) = NULL,
	@LogTypeLookupCode AS NCHAR(50) = NULL,
	@ProcessLookupCode AS NCHAR(50) = NULL,
	@ObjectTypeLookupCode AS NCHAR(50) = NULL,
	@ObjectGuid AS UNIQUEIDENTIFIER = NULL,
	@ProcessId AS BIGINT = NULL,
	@EventLogGuid AS UNIQUEIDENTIFIER = NULL,
	@IntValue AS BIGINT = NULL,
	@FloatValue AS FLOAT = NULL,
	@GuidValue AS UNIQUEIDENTIFIER = NULL,
	@XmlData AS NVARCHAR(2000) = NULL,
	@VerbosityLookupCode AS NCHAR(50) = NULL,
	@PriorityLookupCode AS NCHAR(50) = NULL,
	@SeverityLookupCode AS NCHAR(50) = NULL,
	@AddedEventLogGuid AS UNIQUEIDENTIFIER = NULL OUT
AS
	--PRINT 'EventLogAdd:'
	PRINT '@Comment = ' + @Comment
	--PRINT '@LogTypeLookupCode = ' + RTRIM(@LogTypeLookupCode)
	--PRINT '@ProcessLookupCode = ' + RTRIM(@ProcessLookupCode)
	--PRINT '@ObjectTypeLookupCode = ' + RTRIM(@ObjectTypeLookupCode)
	--PRINT '@ObjectGuid = ' +  CAST(@ObjectGuid AS NCHAR(36))
	--PRINT '@UserCode = ' + RTRIM(@UserCode)
	--PRINT '@ProcessId = ' + CAST(@ProcessId AS NCHAR(20))
	--PRINT '@EventLogGuid = ' +  CAST(@EventLogGuid AS NCHAR(36))
	--PRINT '@IntValue = ' + CAST(@IntValue AS NCHAR(20))
	--PRINT '@FloatValue = ' + CAST(@FloatValue AS NVARCHAR(20))
	--PRINT '@GuidValue = ' +  CAST(@GuidValue AS NCHAR(36))
	--PRINT '@XmlData = ' +  @XmlData

	SET @UserCode = LTRIM(RTRIM(@UserCode))
	IF @UserCode = ''
		SET @UserCode = NULL

	IF @EventLogGuid IS NULL
		SET @EventLogGuid = newid()

	SET @AddedEventLogGuid = @EventLogGuid

	DECLARE @PersonGuid AS UNIQUEIDENTIFIER
	DECLARE @LogTypeLookupGuid AS UNIQUEIDENTIFIER
	DECLARE @ProcessLookupGuid AS UNIQUEIDENTIFIER
	DECLARE @ObjectTypeLookupGuid AS UNIQUEIDENTIFIER
	DECLARE @VerbosityLookupGuid AS UNIQUEIDENTIFIER
	DECLARE @PriorityLookupGuid AS UNIQUEIDENTIFIER
	DECLARE @SeverityLookupGuid AS UNIQUEIDENTIFIER

	EXECUTE EventLogGetGuids
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@LogTypeLookupCode=@LogTypeLookupCode,
		@ProcessLookupCode=@ProcessLookupCode,
		@ObjectTypeLookupCode=@ObjectTypeLookupCode,
		@VerbosityLookupCode=@VerbosityLookupCode,
		@PriorityLookupCode=@PriorityLookupCode,
		@SeverityLookupCode=@SeverityLookupCode,
		@LogTypeLookupGuid=@LogTypeLookupGuid OUT,
		@ProcessLookupGuid=@ProcessLookupGuid OUT,
		@ObjectTypeLookupGuid=@ObjectTypeLookupGuid OUT,
		@PersonGuid=@PersonGuid OUT,
		@VerbosityLookupGuid=@VerbosityLookupGuid OUT,
		@PriorityLookupGuid=@PriorityLookupGuid OUT,
		@SeverityLookupGuid=@SeverityLookupGuid OUT

	EXECUTE EventLogAddBase
		@UserGuid=@UserGuid,
		@UserCode=@UserCode,
		@Comment=@Comment,
		@LogTypeLookupGuid=@LogTypeLookupGuid,
		@ProcessLookupGuid=@ProcessLookupGuid,
		@ObjectTypeLookupGuid=@ObjectTypeLookupGuid,
		@VerbosityLookupGuid=@VerbosityLookupGuid,
		@PriorityLookupGuid=@PriorityLookupGuid,
		@SeverityLookupGuid=@SeverityLookupGuid,
		@ObjectGuid=@ObjectGuid,
		@PersonGuid=@PersonGuid,
		@ProcessId=@ProcessId,
		@EventLogGuid=@EventLogGuid,
		@IntValue=@IntValue,
		@FloatValue=@FloatValue,
		@GuidValue=@GuidValue,
		@XmlData=@XmlData