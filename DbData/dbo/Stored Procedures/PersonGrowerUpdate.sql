﻿


CREATE PROCEDURE [dbo].[PersonGrowerUpdate]
	 @PersonGuid UniqueIdentifier,
	 @GrowerGuid UniqueIdentifier

	 
AS

	UPDATE Person 
	set GrowerGuid = @GrowerGuid
    WHERE Guid  = @PersonGuid