﻿




CREATE PROCEDURE [dbo].[SupplierLevelsGet]
	@SupplierGuid uniqueidentifier
AS
	SELECT Distinct vl.LevelNumber
	From supplier s
	Join program p on s.guid = p.supplierguid
	Join programseason ps on p.guid = ps.Programguid
	Join VolumeLevel vl on ps.Guid = vl.ProgramSeasonGuid
	WHERE s.Guid = @SupplierGuid
		AND ps.code = 
			(	SELECT MAX(ps2.code) 
				from supplier s2
				Join program p2 on s2.guid = p2.supplierguid
				Join programSeason ps2 on p2.guid = ps2.Programguid 
				where s2.Guid = @SupplierGuid
			)
     Order by vl.LevelNumber