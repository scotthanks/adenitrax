﻿










CREATE PROCEDURE [dbo].[B2BOrderHistoryGet]
	 @OrderGuid as uniqueidentifier

AS
	select 
	b.OrderGuid
    ,o.GrowerGuid
    ,b.B2BType
    ,b.B2BStatus
    ,b.LoadedDateTime
    ,b.CommunicationDirection
    ,b.StatusMessage
    ,g.Name as GrowerName
    ,o.OrderNo
    ,b.XMLData
    ,b.ResponseXMLData
	from epsb2bprod.dbo.B2BIntegrationXML b
	Join growerorder o on b.OrderGuid = o.Guid
	Join Grower g on o.GrowerGuid = g.Guid
   
	where b.OrderGuid =@OrderGuid