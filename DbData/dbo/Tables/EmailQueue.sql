﻿CREATE TABLE [dbo].[EmailQueue] (
    [Id]                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                 UNIQUEIDENTIFIER NOT NULL,
    [EmailSubject]         NVARCHAR (200)   NOT NULL,
    [EmailBody]            NVARCHAR (MAX)   NOT NULL,
    [DateToSend]           DATETIME         NOT NULL,
    [SendStatus]           NVARCHAR (20)    NOT NULL,
    [DateSent]             DATETIME         NULL,
    [OrderLineUpdateCount] INT              NOT NULL,
    [OrderLineAddCount]    INT              NOT NULL,
    [OrderLineCancelCount] INT              NOT NULL,
    [EmailType]            NVARCHAR (50)    DEFAULT ('') NOT NULL,
    [EmailTo]              NVARCHAR (100)   DEFAULT ('') NOT NULL,
    [EmailCC]              NVARCHAR (100)   DEFAULT ('') NOT NULL,
    [EmailBCC]             NVARCHAR (100)   DEFAULT ('') NOT NULL,
    [OrderGuid]            UNIQUEIDENTIFIER NULL,
    [EmailFrom]            NVARCHAR (100)   DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__EmailQueue__A2B5777C57CF26BF] PRIMARY KEY CLUSTERED ([Guid] ASC)
);



