﻿CREATE TABLE [dbo].[Config] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [Code]            NVARCHAR (30)    NOT NULL,
    [Value]           NVARCHAR (200)   NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    CONSTRAINT [PK__Config__A2B5777C57CF26BF] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_Config_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);



