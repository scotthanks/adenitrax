﻿CREATE TABLE [dbo].[SupplierOrder] (
    [Id]                            BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                          UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]               DATETIME         NULL,
    [GrowerOrderGuid]               UNIQUEIDENTIFIER NOT NULL,
    [SupplierGuid]                  UNIQUEIDENTIFIER NOT NULL,
    [PoNo]                          NVARCHAR (30)    NOT NULL,
    [SupplierOrderNo]               NVARCHAR (30)    NOT NULL,
    [TotalCost]                     DECIMAL (8, 2)   NOT NULL,
    [SupplierOrderStatusLookupGuid] UNIQUEIDENTIFIER CONSTRAINT [DF__SupplierO__Suppl__245D67DE] DEFAULT ('FFB383EC-4CE9-4C50-87A4-234E07D15180') NOT NULL,
    [TagRatioLookupGuid]            UNIQUEIDENTIFIER NULL,
    [ShipMethodLookupGuid]          UNIQUEIDENTIFIER NULL,
    [TrackingNumber]                NVARCHAR (200)   CONSTRAINT [DF__SupplierO__Track__3C89F72A] DEFAULT ('') NOT NULL,
    [ExpectedDeliveredDate]         DATETIME         NULL,
    [DateLastSenttoSupplier]        DATETIME         NULL,
    [TrackingComment]               NVARCHAR (200)   DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__Supplier__A2B5777C87195F3B] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_SupplierOrder_GrowerOrder] FOREIGN KEY ([GrowerOrderGuid]) REFERENCES [dbo].[GrowerOrder] ([Guid]),
    CONSTRAINT [FK_SupplierOrder_Guid] FOREIGN KEY ([SupplierOrderStatusLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_SupplierOrder_ShipMethodLookupGuid] FOREIGN KEY ([ShipMethodLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_SupplierOrder_Supplier] FOREIGN KEY ([SupplierGuid]) REFERENCES [dbo].[Supplier] ([Guid]),
    CONSTRAINT [FK_SupplierOrder_TagRatioLookupGuid] FOREIGN KEY ([TagRatioLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid])
);











