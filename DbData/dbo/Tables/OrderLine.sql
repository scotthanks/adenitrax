﻿CREATE TABLE [dbo].[OrderLine] (
    [Id]                        BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                      UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]           DATETIME         NULL,
    [SupplierOrderGuid]         UNIQUEIDENTIFIER NOT NULL,
    [ProductGuid]               UNIQUEIDENTIFIER NOT NULL,
    [QtyOrdered]                INT              NOT NULL,
    [QtyShipped]                INT              NOT NULL,
    [ActualPriceUsedGuid]       UNIQUEIDENTIFIER NULL,
    [ActualPrice]               DECIMAL (8, 4)   NOT NULL,
    [OrderLineStatusLookupGuid] UNIQUEIDENTIFIER CONSTRAINT [DF__OrderLine__Order__2739D489] DEFAULT ('F268924E-C461-4BDA-88E1-BC1F2F882C85') NOT NULL,
    [ChangeTypeLookupGuid]      UNIQUEIDENTIFIER CONSTRAINT [DF__OrderLine__Chang__282DF8C2] DEFAULT ('DBAD2E8D-B056-4CE8-B56D-94DC7E6B4769') NOT NULL,
    [DateEntered]               DATETIME         NULL,
    [DateLastChanged]           DATETIME         NULL,
    [ActualCost]                DECIMAL (8, 4)   CONSTRAINT [DF__OrderLine__Actua__7EC1CEDB] DEFAULT ((0)) NULL,
    [ActualCostUsedGuid]        UNIQUEIDENTIFIER NULL,
    [DateQtyLastChanged]        DATETIME         NULL,
    [LineNumber]                INT              DEFAULT ((0)) NOT NULL,
    [LineComment]               NVARCHAR (200)   DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__OrderLin__A2B5777CF6A8CFD2] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_OrderLine_Product] FOREIGN KEY ([ProductGuid]) REFERENCES [dbo].[Product] ([Guid]),
    CONSTRAINT [FK_OrderLine_SupplierOrder] FOREIGN KEY ([SupplierOrderGuid]) REFERENCES [dbo].[SupplierOrder] ([Guid])
);









