﻿CREATE TABLE [dbo].[FreightSupplierStateZone] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [SupplierGuid]    UNIQUEIDENTIFIER NOT NULL,
    [StateGuid]       UNIQUEIDENTIFIER NOT NULL,
    [ZoneLookupGuid]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__FreightS__A2B5777C19797E41] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_FreightSupplierStateZone_State] FOREIGN KEY ([StateGuid]) REFERENCES [dbo].[State] ([Guid]),
    CONSTRAINT [FK_FreightSupplierStateZone_Supplier] FOREIGN KEY ([SupplierGuid]) REFERENCES [dbo].[Supplier] ([Guid]),
    CONSTRAINT [FK_FreightSupplierStateZone_Zone] FOREIGN KEY ([ZoneLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_Supplier_State] UNIQUE NONCLUSTERED ([SupplierGuid] ASC, [StateGuid] ASC)
);



