﻿CREATE TABLE [dbo].[Person] (
    [Id]                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                 UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]      DATETIME         NULL,
    [FirstName]            NVARCHAR (50)    NOT NULL,
    [LastName]             NVARCHAR (50)    NOT NULL,
    [UserGuid]             UNIQUEIDENTIFIER NULL,
    [UserId]               BIGINT           NULL,
    [UserCode]             NVARCHAR (56)    NOT NULL,
    [Email]                NVARCHAR (70)    CONSTRAINT [DF__Person__EMail__2EDAF651] DEFAULT ('') NOT NULL,
    [PhoneAreaCode]        DECIMAL (3)      CONSTRAINT [DF__Person__PhoneAre__2FCF1A8A] DEFAULT ((0)) NOT NULL,
    [Phone]                DECIMAL (7)      CONSTRAINT [DF__Person__Phone__30C33EC3] DEFAULT ((0)) NOT NULL,
    [SubscribeNewsletter]  BIT              CONSTRAINT [DF__Person__Subscrib__31B762FC] DEFAULT ((0)) NOT NULL,
    [PersonTypeLookupGuid] UNIQUEIDENTIFIER CONSTRAINT [DF__Person__PersonTy__32AB8735] DEFAULT ('85C6C752-ED22-49A7-9671-066BAA8FE7D6') NOT NULL,
    [GrowerGuid]           UNIQUEIDENTIFIER NULL,
    [PersonGetsOrderEmail] BIT              CONSTRAINT [DF__Person__PersonGe__76EBA2E9] DEFAULT ((0)) NOT NULL,
    [SubscribePromotions]  BIT              CONSTRAINT [DF__Person__Subscrib__62AFA012] DEFAULT ((0)) NOT NULL,
    [IsInnovator]          BIT              CONSTRAINT [DF__Person__IsInnova__25C68D63] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Person__3214EC0799BFC7B9] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid]),
    CONSTRAINT [FK_Person_PersonTypeLookupGuid] FOREIGN KEY ([PersonTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_Person_GUID] UNIQUE NONCLUSTERED ([Guid] ASC)
);











