﻿CREATE TABLE [dbo].[GrowerOrder] (
    [Id]                          BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                        UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]             DATETIME         NULL,
    [GrowerGuid]                  UNIQUEIDENTIFIER NOT NULL,
    [ShipWeekGuid]                UNIQUEIDENTIFIER NOT NULL,
    [OrderNo]                     NVARCHAR (10)    NOT NULL,
    [Description]                 NVARCHAR (50)    NOT NULL,
    [CustomerPoNo]                NVARCHAR (100)   NOT NULL,
    [OrderTypeLookupGuid]         UNIQUEIDENTIFIER NOT NULL,
    [ProductFormCategoryGuid]     UNIQUEIDENTIFIER NOT NULL,
    [ProgramTypeGuid]             UNIQUEIDENTIFIER NOT NULL,
    [ShipToAddressGuid]           UNIQUEIDENTIFIER NULL,
    [PaymentTypeLookupGuid]       UNIQUEIDENTIFIER CONSTRAINT [DF__GrowerOrd__Payme__208CD6FA] DEFAULT ('E1E73B39-E4F0-488D-AC93-87AFE75025A7') NOT NULL,
    [GrowerOrderStatusLookupGuid] UNIQUEIDENTIFIER CONSTRAINT [DF__GrowerOrd__Growe__2180FB33] DEFAULT ('75B575DB-3D9D-4B47-B0A6-46D63C21D86B') NOT NULL,
    [DateEntered]                 DATETIME         NOT NULL,
    [GrowerCreditCardGuid]        UNIQUEIDENTIFIER NULL,
    [PersonGuid]                  UNIQUEIDENTIFIER CONSTRAINT [Default_Personguid] DEFAULT ('E0817A03-04CA-4F34-815C-F13B0215A58C') NOT NULL,
    [PromotionalCode]             NVARCHAR (20)    CONSTRAINT [DF__GrowerOrd__Disco__695C9DA1] DEFAULT ('') NOT NULL,
    [GrowerShipMethodCode]        NVARCHAR (20)    DEFAULT ('') NOT NULL,
    [SellerGuid]                  UNIQUEIDENTIFIER DEFAULT ('CDF5C0F2-D183-4AB4-9D99-59C97E7424C8') NOT NULL,
    [RevisionNumber]              INT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__GrowerOr__A2B5777CD50EE904] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerOrder_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_GrowerCreditCard] FOREIGN KEY ([GrowerCreditCardGuid]) REFERENCES [dbo].[GrowerCreditCard] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_Guid] FOREIGN KEY ([OrderTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_PaymentTypeLookupGuid] FOREIGN KEY ([PaymentTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_Person] FOREIGN KEY ([PersonGuid]) REFERENCES [dbo].[Person] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_ProductFormCategory] FOREIGN KEY ([ProductFormCategoryGuid]) REFERENCES [dbo].[ProductFormCategory] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_ProgramType] FOREIGN KEY ([ProgramTypeGuid]) REFERENCES [dbo].[ProgramType] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_ShipToAddressGuid] FOREIGN KEY ([ShipToAddressGuid]) REFERENCES [dbo].[GrowerAddress] ([Guid]),
    CONSTRAINT [FK_GrowerOrder_ShipWeek] FOREIGN KEY ([ShipWeekGuid]) REFERENCES [dbo].[ShipWeek] ([Guid])
);













