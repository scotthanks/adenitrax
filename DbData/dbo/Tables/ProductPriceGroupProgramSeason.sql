﻿CREATE TABLE [dbo].[ProductPriceGroupProgramSeason] (
    [Id]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]              UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]   DATETIME         NULL,
    [ProductGuid]       UNIQUEIDENTIFIER NOT NULL,
    [ProgramSeasonGuid] UNIQUEIDENTIFIER NOT NULL,
    [PriceGroupGuid]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__ProductP__A2B5777CC93B5671] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProductPriceGroupProgramSeason_PriceGroup] FOREIGN KEY ([PriceGroupGuid]) REFERENCES [dbo].[PriceGroup] ([Guid]),
    CONSTRAINT [FK_ProductPriceGroupProgramSeason_ProductGuid] FOREIGN KEY ([ProductGuid]) REFERENCES [dbo].[Product] ([Guid]),
    CONSTRAINT [FK_ProductPriceGroupProgramSeason_ProgramSeason] FOREIGN KEY ([ProgramSeasonGuid]) REFERENCES [dbo].[ProgramSeason] ([Guid]),
    CONSTRAINT [UC_Product_ProgramSeason] UNIQUE NONCLUSTERED ([ProductGuid] ASC, [ProgramSeasonGuid] ASC)
);



