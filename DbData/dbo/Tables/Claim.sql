﻿CREATE TABLE [dbo].[Claim] (
    [Id]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                  UNIQUEIDENTIFIER NOT NULL,
    [GrowerOrderGuid]       UNIQUEIDENTIFIER NOT NULL,
    [ClaimReasonLookupGuid] UNIQUEIDENTIFIER NULL,
    [Comment]               NVARCHAR (1000)  NOT NULL,
    [ClaimDate]             DATETIME         NOT NULL,
    [ImageSetGuid]          UNIQUEIDENTIFIER NULL,
    [CreditMemoLedgerGuid]  UNIQUEIDENTIFIER NULL,
    [ClaimStatusLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    [ClaimNo]               NVARCHAR (10)    NOT NULL,
    CONSTRAINT [PK__Claim__A2B5777C440C2A7C] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Claim_ClaimReasonLookup] FOREIGN KEY ([ClaimReasonLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Claim_ClaimStatusLookup] FOREIGN KEY ([ClaimStatusLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_Claim_CreditMemoLedger] FOREIGN KEY ([CreditMemoLedgerGuid]) REFERENCES [dbo].[Ledger] ([Guid]),
    CONSTRAINT [FK_Claim_GrowerOrder] FOREIGN KEY ([GrowerOrderGuid]) REFERENCES [dbo].[GrowerOrder] ([Guid]),
    CONSTRAINT [FK_Claim_ImageSet] FOREIGN KEY ([ImageSetGuid]) REFERENCES [dbo].[ImageSet] ([Guid])
);





