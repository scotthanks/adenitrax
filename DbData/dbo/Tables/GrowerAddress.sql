﻿CREATE TABLE [dbo].[GrowerAddress] (
    [Id]                  BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]     DATETIME         NULL,
    [GrowerGuid]          UNIQUEIDENTIFIER NOT NULL,
    [AddressGuid]         UNIQUEIDENTIFIER NOT NULL,
    [PhoneAreaCode]       DECIMAL (3)      CONSTRAINT [DF__GrowerAdd__Phone__1AD3FDA4] DEFAULT ((0)) NOT NULL,
    [Phone]               DECIMAL (7)      CONSTRAINT [DF__GrowerAdd__Phone__1BC821DD] DEFAULT ((0)) NOT NULL,
    [DefaultAddress]      BIT              CONSTRAINT [DF__GrowerAdd__Defau__1CBC4616] DEFAULT ((0)) NOT NULL,
    [SpecialInstructions] NVARCHAR (200)   CONSTRAINT [DF__GrowerAdd__Speci__1DB06A4F] DEFAULT ('') NOT NULL,
    [SellerCustomerID]    NVARCHAR (20)    DEFAULT ('') NOT NULL,
    [B2BCustomerName]     NVARCHAR (100)   DEFAULT ('') NOT NULL,
    [B2BUrl]              NVARCHAR (100)   DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__GrowerAd__A2B5777CE9FFBBB7] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerAddress_Address] FOREIGN KEY ([AddressGuid]) REFERENCES [dbo].[Address] ([Guid]),
    CONSTRAINT [FK_GrowerAddress_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid])
);







