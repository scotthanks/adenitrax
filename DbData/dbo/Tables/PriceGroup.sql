﻿CREATE TABLE [dbo].[PriceGroup] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [Code]            NVARCHAR (30)    NOT NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [TagCost]         DECIMAL (8, 5)   NOT NULL,
    CONSTRAINT [PK__PriceGro__A2B5777C68ECA229] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_PriceGroup_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);







