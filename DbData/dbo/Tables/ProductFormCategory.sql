﻿CREATE TABLE [dbo].[ProductFormCategory] (
    [Id]                                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                                 UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]                      DATETIME         NULL,
    [Code]                                 NVARCHAR (10)    NOT NULL,
    [Name]                                 NVARCHAR (50)    NOT NULL,
    [IsRooted]                             BIT              NOT NULL,
    [AvailabilityWeekCalcMethodLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__ProductF__A2B5777CB53D2B35] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProductFormCategory_AvailabilityWeekCalcMethodLookup] FOREIGN KEY ([AvailabilityWeekCalcMethodLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_ProductFormCategory_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);





