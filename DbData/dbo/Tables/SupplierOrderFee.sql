﻿CREATE TABLE [dbo].[SupplierOrderFee] (
    [Id]                 BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]               UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]    DATETIME         NULL,
    [SupplierOrderGuid]  UNIQUEIDENTIFIER NOT NULL,
    [FeeTypeLookupGuid]  UNIQUEIDENTIFIER NOT NULL,
    [Amount]             DECIMAL (6, 2)   NOT NULL,
    [FeeStatXLookupGuid] UNIQUEIDENTIFIER NULL,
    [FeeDescription]     NVARCHAR (500)   CONSTRAINT [DF__SupplierO__FeeDe__335592AB] DEFAULT ('') NOT NULL,
    [IsManual]           BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Supplier__A2B5777C9FED850E] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_SupplierOrderFee_FeeStatusLookupGuid] FOREIGN KEY ([FeeStatXLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_SupplierOrderFee_FeeTypeLookupGuid] FOREIGN KEY ([FeeTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_SupplierOrderFee_SupplierOrder] FOREIGN KEY ([SupplierOrderGuid]) REFERENCES [dbo].[SupplierOrder] ([Guid])
);













