﻿CREATE TABLE [dbo].[PromoCode] (
    [Id]                      BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                    UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]         DATETIME         NULL,
    [Code]                    NVARCHAR (20)    NOT NULL,
    [Name]                    NVARCHAR (50)    NOT NULL,
    [EffectiveDate]           DATETIME         NOT NULL,
    [ExpirationDate]          DATETIME         NOT NULL,
    [DiscountPercent]         DECIMAL (8, 3)   NOT NULL,
    [SpeciesGuid]             UNIQUEIDENTIFIER NULL,
    [ShipWeekStartGuid]       UNIQUEIDENTIFIER NULL,
    [ShipWeekEndGuid]         UNIQUEIDENTIFIER NULL,
    [ProductFormCategoryGuid] UNIQUEIDENTIFIER NULL,
    [ProgramTypeGuid]         UNIQUEIDENTIFIER NULL,
    [SeriesGuid]              UNIQUEIDENTIFIER NULL,
    [SingleUse]               BIT              DEFAULT ((0)) NOT NULL,
    [SupplierGuid]            UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__PromoCode__A2B5777C95C52057] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_PromoCode_ProductFornCategory] FOREIGN KEY ([ProductFormCategoryGuid]) REFERENCES [dbo].[ProductFormCategory] ([Guid]),
    CONSTRAINT [FK_PromoCode_ProgramType] FOREIGN KEY ([ProgramTypeGuid]) REFERENCES [dbo].[ProgramType] ([Guid]),
    CONSTRAINT [FK_PromoCode_Series] FOREIGN KEY ([SeriesGuid]) REFERENCES [dbo].[Series] ([Guid]),
    CONSTRAINT [FK_PromoCode_ShipWeekEnd] FOREIGN KEY ([ShipWeekEndGuid]) REFERENCES [dbo].[ShipWeek] ([Guid]),
    CONSTRAINT [FK_PromoCode_ShipWeekStart] FOREIGN KEY ([ShipWeekStartGuid]) REFERENCES [dbo].[ShipWeek] ([Guid]),
    CONSTRAINT [FK_PromoCode_Species] FOREIGN KEY ([SpeciesGuid]) REFERENCES [dbo].[Species] ([Guid])
);









