﻿CREATE TABLE [dbo].[Program] (
    [Id]                                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                                 UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]                      DATETIME         NULL,
    [Code]                                 NVARCHAR (10)    NOT NULL,
    [Name]                                 NVARCHAR (50)    NOT NULL,
    [ProgramTypeGuid]                      UNIQUEIDENTIFIER NOT NULL,
    [SupplierGuid]                         UNIQUEIDENTIFIER NOT NULL,
    [ProductFormCategoryGuid]              UNIQUEIDENTIFIER NOT NULL,
    [MinimumOrderPackingUnitQty]           INT              NOT NULL,
    [OrderMultiplePackingUnitQty]          INT              CONSTRAINT [DF__Program__OrderMu__3C69FB99] DEFAULT ((0)) NOT NULL,
    [ShippingUnitWarningMinimumQty]        INT              CONSTRAINT [DF__Program__Shippin__3D5E1FD2] DEFAULT ((0)) NOT NULL,
    [ShippingUnitValidMinimumQty]          INT              CONSTRAINT [DF__Program__Shippin__3E52440B] DEFAULT ((0)) NOT NULL,
    [AvailabilityWeekCalcMethodLookupGuid] UNIQUEIDENTIFIER NULL,
    [SellerGuid]                           UNIQUEIDENTIFIER DEFAULT ('CDF5C0F2-D183-4AB4-9D99-59C97E7424C8') NOT NULL,
    CONSTRAINT [PK__Program__A2B5777C723639DD] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Program_ProductFormCategory] FOREIGN KEY ([ProductFormCategoryGuid]) REFERENCES [dbo].[ProductFormCategory] ([Guid]),
    CONSTRAINT [FK_Program_ProgramType] FOREIGN KEY ([ProgramTypeGuid]) REFERENCES [dbo].[ProgramType] ([Guid]),
    CONSTRAINT [FK_Program_Supplier] FOREIGN KEY ([SupplierGuid]) REFERENCES [dbo].[Supplier] ([Guid]),
    CONSTRAINT [UC_Program_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);









