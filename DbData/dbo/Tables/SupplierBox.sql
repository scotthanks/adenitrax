﻿CREATE TABLE [dbo].[SupplierBox] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [Code]            NVARCHAR (10)    NOT NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [SupplierGuid]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__Supplier__A2B5777CE5D7F473] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_SupplierBox_Supplier] FOREIGN KEY ([SupplierGuid]) REFERENCES [dbo].[Supplier] ([Guid]),
    CONSTRAINT [UC_SupplierBox_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);



