﻿CREATE TABLE [dbo].[SearchVarietyHelper] (
    [Id]                      BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                    UNIQUEIDENTIFIER NOT NULL,
    [VarietyGuid]             UNIQUEIDENTIFIER NULL,
    [ShipWeekString]          NVARCHAR (10)    NOT NULL,
    [ProductFormCategoryGuid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__SearchVarietyHelper] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProductFormCategory_Guid] FOREIGN KEY ([ProductFormCategoryGuid]) REFERENCES [dbo].[ProductFormCategory] ([Guid]),
    CONSTRAINT [UC_Variety_ProductForm] UNIQUE NONCLUSTERED ([VarietyGuid] ASC, [ProductFormCategoryGuid] ASC)
);



