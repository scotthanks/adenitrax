﻿CREATE TABLE [dbo].[SearchEvent] (
    [Id]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]       UNIQUEIDENTIFIER NOT NULL,
    [SearchTerm] NVARCHAR (200)   NOT NULL,
    [EventTime]  DATETIME         NOT NULL,
    [UserGuid]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__SearchEv__A2B5777C088E7B15] PRIMARY KEY CLUSTERED ([Guid] ASC)
);

