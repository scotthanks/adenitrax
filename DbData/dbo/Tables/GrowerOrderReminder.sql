﻿CREATE TABLE [dbo].[GrowerOrderReminder] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [GrowerOrderGuid] UNIQUEIDENTIFIER NOT NULL,
    [OrderLineGuid]   UNIQUEIDENTIFIER NOT NULL,
    [Reminder2Day]    BIT              DEFAULT ((0)) NOT NULL,
    [Reminder7Day]    BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__GrowerOrRem__A2B5777CD50EE904] PRIMARY KEY CLUSTERED ([Guid] ASC)
);





