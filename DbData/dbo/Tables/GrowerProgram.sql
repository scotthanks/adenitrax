﻿CREATE TABLE [dbo].[GrowerProgram] (
    [Id]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                  UNIQUEIDENTIFIER NOT NULL,
    [GrowerGuid]            UNIQUEIDENTIFIER NOT NULL,
    [ProgramGuid]           UNIQUEIDENTIFIER NOT NULL,
    [AvailLastSendDate]     DATETIME         NULL,
    [AvailNextSendDate]     DATETIME         NULL,
    [AvailHoursBetweenSend] INT              DEFAULT ((0)) NOT NULL,
    [AvailWeeks]            INT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__GrowerProgram__Guid] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerProgram_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid]),
    CONSTRAINT [FK_GrowerProgram_Program] FOREIGN KEY ([ProgramGuid]) REFERENCES [dbo].[Program] ([Guid]),
    CONSTRAINT [uc_GrowerProgram] UNIQUE NONCLUSTERED ([GrowerGuid] ASC, [ProgramGuid] ASC)
);





