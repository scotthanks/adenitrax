﻿CREATE TABLE [dbo].[SupplierBoxProductForm] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [SupplierBoxGuid] UNIQUEIDENTIFIER NOT NULL,
    [ProductFormGuid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__Supplier__A2B5777C45833265] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_SupplierBoxProductForm_ProductForm] FOREIGN KEY ([ProductFormGuid]) REFERENCES [dbo].[ProductForm] ([Guid]),
    CONSTRAINT [FK_SupplierBoxProductForm_SupplierBox] FOREIGN KEY ([SupplierBoxGuid]) REFERENCES [dbo].[SupplierBox] ([Guid]),
    CONSTRAINT [UC_ProductForm_Box] UNIQUE NONCLUSTERED ([SupplierBoxGuid] ASC, [ProductFormGuid] ASC)
);



