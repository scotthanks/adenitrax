﻿CREATE TABLE [dbo].[SearchResult] (
    [Guid]                            UNIQUEIDENTIFIER NOT NULL,
    [ResultType]                      NVARCHAR (30)    NOT NULL,
    [ResultCode]                      NVARCHAR (50)    NOT NULL,
    [ResultName]                      NVARCHAR (100)   NOT NULL,
    [HeaderText]                      NVARCHAR (100)   NULL,
    [SubHeaderText]                   NVARCHAR (100)   NULL,
    [ImageURI]                        NVARCHAR (300)   NULL,
    [CatalogProductformCategoryCode]  NVARCHAR (10)    NULL,
    [CatalogProgramTypeCode]          NVARCHAR (10)    NULL,
    [Program1Name]                    NVARCHAR (50)    NULL,
    [Program1ProductformCategoryCode] NVARCHAR (10)    NULL,
    [Program1ProgramTypeCode]         NVARCHAR (10)    NULL,
    [Program1ProductCount]            INT              NULL,
    [Program2Name]                    NVARCHAR (50)    NULL,
    [Program2ProductformCategoryCode] NVARCHAR (10)    NULL,
    [Program2ProgramTypeCode]         NVARCHAR (10)    NULL,
    [Program2ProductCount]            INT              NULL,
    [Program3Name]                    NVARCHAR (50)    NULL,
    [Program3ProductformCategoryCode] NVARCHAR (10)    NULL,
    [Program3ProgramTypeCode]         NVARCHAR (10)    NULL,
    [Program3ProductCount]            INT              NULL,
    [Species1Code]                    NVARCHAR (10)    NULL,
    [Species1Name]                    NVARCHAR (50)    NULL,
    [Species1ProgramTypeCode]         NVARCHAR (10)    NULL,
    [Species1ProductformCategoryCode] NVARCHAR (10)    NULL,
    [Species2Code]                    NVARCHAR (10)    NULL,
    [Species2Name]                    NVARCHAR (30)    NULL,
    [Species2ProgramTypeCode]         NVARCHAR (10)    NULL,
    [Species2ProductformCategoryCode] NVARCHAR (10)    NULL,
    [Species3Code]                    NVARCHAR (10)    NULL,
    [Species3Name]                    NVARCHAR (30)    NULL,
    [Species3ProgramTypeCode]         NVARCHAR (10)    NULL,
    [Species3ProductformCategoryCode] NVARCHAR (10)    NULL,
    [Program1AvailabilityWeek]        NVARCHAR (10)    NULL,
    [Program2AvailabilityWeek]        NVARCHAR (10)    NULL,
    [Program3AvailabilityWeek]        NVARCHAR (10)    NULL,
    CONSTRAINT [PK__SearchRe__A2B5777C9450F324] PRIMARY KEY CLUSTERED ([Guid] ASC)
);











