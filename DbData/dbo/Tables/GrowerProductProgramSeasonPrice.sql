﻿CREATE TABLE [dbo].[GrowerProductProgramSeasonPrice] (
    [Id]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]              UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]   DATETIME         NULL,
    [GrowerGuid]        UNIQUEIDENTIFIER NOT NULL,
    [ProgramSeasonGuid] UNIQUEIDENTIFIER NOT NULL,
    [ProductGuid]       UNIQUEIDENTIFIER NOT NULL,
    [CuttingCost]       DECIMAL (8, 4)   NOT NULL,
    [RoyaltyCost]       DECIMAL (8, 4)   NOT NULL,
    [FreightCost]       DECIMAL (8, 4)   NOT NULL,
    [TagCost]           DECIMAL (8, 4)   NOT NULL,
    [LastUpdateDate]    DATETIME         DEFAULT ('1/1/2010') NOT NULL,
    CONSTRAINT [PK__GrowerProductProgram__A2B5777CC93B5671] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_GrowerProductProgramSeasonPrice_Grower] FOREIGN KEY ([GrowerGuid]) REFERENCES [dbo].[Grower] ([Guid]),
    CONSTRAINT [FK_GrowerProductProgramSeasonPrice_Product] FOREIGN KEY ([ProductGuid]) REFERENCES [dbo].[Product] ([Guid]),
    CONSTRAINT [FK_GrowerProductProgramSeasonPrice_ProgramSeason] FOREIGN KEY ([ProgramSeasonGuid]) REFERENCES [dbo].[ProgramSeason] ([Guid]),
    CONSTRAINT [UC_Grower_ProgramSeason_Product] UNIQUE NONCLUSTERED ([GrowerGuid] ASC, [ProgramSeasonGuid] ASC, [ProductGuid] ASC)
);




GO
CREATE NONCLUSTERED INDEX [GrowerProductProgramSeasonPrice_Guid]
    ON [dbo].[GrowerProductProgramSeasonPrice]([Id] ASC)
    INCLUDE([Guid]);

