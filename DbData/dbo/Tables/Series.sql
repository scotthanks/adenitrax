﻿CREATE TABLE [dbo].[Series] (
    [Id]                  BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]     DATETIME         NULL,
    [Code]                NVARCHAR (30)    NOT NULL,
    [Name]                NVARCHAR (50)    NOT NULL,
    [SpeciesGuid]         UNIQUEIDENTIFIER NOT NULL,
    [DescriptionHTMLGUID] UNIQUEIDENTIFIER NULL,
    [CultureLibraryLink]  NVARCHAR (200)   CONSTRAINT [DF__Series__CultureL__070CFC19] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK__Series__A2B5777CCCB3CCB6] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Series_Species] FOREIGN KEY ([SpeciesGuid]) REFERENCES [dbo].[Species] ([Guid]),
    CONSTRAINT [UC_Series_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);







