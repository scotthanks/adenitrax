﻿CREATE TABLE [dbo].[ReportedAvailability] (
    [Id]                         BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                       UNIQUEIDENTIFIER NOT NULL,
    [DateReported]               DATETIME         NOT NULL,
    [ProductGuid]                UNIQUEIDENTIFIER NOT NULL,
    [ShipWeekGuid]               UNIQUEIDENTIFIER NOT NULL,
    [Qty]                        INT              NOT NULL,
    [AvailabilityTypeLookupGuid] UNIQUEIDENTIFIER CONSTRAINT [DF__ReportedA__Avail__151B244E] DEFAULT ('E2061EB9-264C-43EF-A1C2-E2F142425102') NOT NULL,
    [SalesSinceDateReported]     INT              CONSTRAINT [DF__ReportedA__Sales__0EF836A4] DEFAULT ((0)) NULL,
    CONSTRAINT [PK__Reported__A2B5777CD83B5C7E] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ReportedAvailability_AvailabilityTypeLookupGuid] FOREIGN KEY ([AvailabilityTypeLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [FK_ReportedAvailability_Product] FOREIGN KEY ([ProductGuid]) REFERENCES [dbo].[Product] ([Guid]),
    CONSTRAINT [FK_ReportedAvailability_ShipWeek] FOREIGN KEY ([ShipWeekGuid]) REFERENCES [dbo].[ShipWeek] ([Guid])
);






GO
CREATE NONCLUSTERED INDEX [idx_ReportedAvailability_ShipWeekGuid]
    ON [dbo].[ReportedAvailability]([ShipWeekGuid] ASC)
    INCLUDE([AvailabilityTypeLookupGuid], [Guid], [ProductGuid]);

