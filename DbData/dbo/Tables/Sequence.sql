﻿CREATE TABLE [dbo].[Sequence] (
    [Id]             BIGINT        NOT NULL,
    [SequenceName]   NVARCHAR (50) NOT NULL,
    [SequenceNumber] BIGINT        NOT NULL,
    CONSTRAINT [PK__tmp_ms_x__3214EC07B3EEFA41] PRIMARY KEY CLUSTERED ([Id] ASC)
);



