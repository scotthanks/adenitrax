﻿CREATE TABLE [dbo].[ChallengeEvent] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated] DATETIME         NULL,
    [ChallengeGuid]   UNIQUEIDENTIFIER NOT NULL,
    [PersonGuid]      UNIQUEIDENTIFIER NOT NULL,
    [EventDate]       DATETIME         NOT NULL,
    [Points]          INT              NOT NULL,
    CONSTRAINT [PK__Challeng__A2B5777CF436B53F] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ChallengeEvent_Challenge] FOREIGN KEY ([ChallengeGuid]) REFERENCES [dbo].[Challenge] ([Guid]),
    CONSTRAINT [FK_ChallengeEvent_Person] FOREIGN KEY ([PersonGuid]) REFERENCES [dbo].[Person] ([Guid]),
    CONSTRAINT [UC_ChallengeEvent_Guid] UNIQUE NONCLUSTERED ([Guid] ASC)
);

