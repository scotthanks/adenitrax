﻿CREATE TABLE [dbo].[Triple] (
    [Id]                  BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                UNIQUEIDENTIFIER NOT NULL,
    [SubjectGuid]         UNIQUEIDENTIFIER NOT NULL,
    [PredicateLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    [ObjectGuid]          UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__Triple__A2B5777C8D23C9DE] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_Triple_PredicateLookupGuid] FOREIGN KEY ([PredicateLookupGuid]) REFERENCES [dbo].[Lookup] ([Guid]),
    CONSTRAINT [UC_Triple_SubjectPredicateObject] UNIQUE NONCLUSTERED ([SubjectGuid] ASC, [PredicateLookupGuid] ASC, [ObjectGuid] ASC)
);



