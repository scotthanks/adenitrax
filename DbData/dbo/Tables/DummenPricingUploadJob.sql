﻿CREATE TABLE [dbo].[DummenPricingUploadJob] (
    [Id]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]       UNIQUEIDENTIFIER NOT NULL,
    [UserGuid]   UNIQUEIDENTIFIER NOT NULL,
    [UploadDate] DATETIME         NOT NULL,
    CONSTRAINT [PK__DummenPricingUploadJob] PRIMARY KEY CLUSTERED ([Guid] ASC)
);

