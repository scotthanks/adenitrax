﻿CREATE TABLE [dbo].[ProductExclusion] (
    [Id]                  BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                UNIQUEIDENTIFIER NOT NULL,
    [SupplierGuid]        UNIQUEIDENTIFIER NOT NULL,
    [SupplierIdentifier]  NVARCHAR (50)    NOT NULL,
    [Reason]              NVARCHAR (50)    NOT NULL,
    [SupplierDescription] NVARCHAR (200)   NOT NULL,
    CONSTRAINT [PK__ProductE__A2B5777C0931F60F] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProductExclusion_SupplierGuid] FOREIGN KEY ([SupplierGuid]) REFERENCES [dbo].[Supplier] ([Guid])
);



