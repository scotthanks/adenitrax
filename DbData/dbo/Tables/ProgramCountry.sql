﻿CREATE TABLE [dbo].[ProgramCountry] (
    [Id]          BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]        UNIQUEIDENTIFIER NOT NULL,
    [ProgramGuid] UNIQUEIDENTIFIER NOT NULL,
    [CountryGuid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__ProgramC__A2B5777C6B7B3779] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProgramCountry_Country] FOREIGN KEY ([CountryGuid]) REFERENCES [dbo].[Country] ([Guid]),
    CONSTRAINT [FK_ProgramCountry_Program] FOREIGN KEY ([ProgramGuid]) REFERENCES [dbo].[Program] ([Guid])
);



