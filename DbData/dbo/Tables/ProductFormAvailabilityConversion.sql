﻿CREATE TABLE [dbo].[ProductFormAvailabilityConversion] (
    [Id]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                  UNIQUEIDENTIFIER NOT NULL,
    [FirstProductFormGuid]  UNIQUEIDENTIFIER NOT NULL,
    [SecondProductFormGuid] UNIQUEIDENTIFIER NOT NULL,
    [ConversionPercent]     DECIMAL (14, 4)  NOT NULL,
    [LeadTimeWeeks]         INT              NOT NULL,
    CONSTRAINT [PK__ProductF__A2B5777CD7213789] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [FK_ProductFormAvailabilityConversion_FirstProductForm] FOREIGN KEY ([FirstProductFormGuid]) REFERENCES [dbo].[ProductForm] ([Guid]),
    CONSTRAINT [FK_ProductFormAvailabilityConversion_SecondProductForm] FOREIGN KEY ([SecondProductFormGuid]) REFERENCES [dbo].[ProductForm] ([Guid])
);



