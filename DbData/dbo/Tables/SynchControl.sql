﻿CREATE TABLE [dbo].[SynchControl] (
    [Id]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                  UNIQUEIDENTIFIER NOT NULL,
    [ObjectName]            NVARCHAR (50)    NOT NULL,
    [LastSynchCompleteDate] DATETIME         NOT NULL,
    CONSTRAINT [PK__SynchCon__A2B5777C7D9CB632] PRIMARY KEY CLUSTERED ([Guid] ASC)
);

