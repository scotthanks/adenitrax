﻿CREATE TABLE [dbo].[GrowerProductProgramSeasonPriceUpload] (
    [Id]                 BIGINT           IDENTITY (1, 1) NOT NULL,
    [RunNumber]          BIGINT           NOT NULL,
    [UploadDate]         DATETIME         NOT NULL,
    [GrowerName]         NVARCHAR (50)    NOT NULL,
    [GrowerGuid]         UNIQUEIDENTIFIER NULL,
    [ProgramSeasonCode]  NVARCHAR (20)    NOT NULL,
    [ProgramSeasonGuid]  UNIQUEIDENTIFIER NULL,
    [SupplierIdentifier] NVARCHAR (50)    NOT NULL,
    [ProductGuid]        UNIQUEIDENTIFIER NULL,
    [CuttingCost]        DECIMAL (8, 4)   NOT NULL,
    [RoyaltyCost]        DECIMAL (8, 4)   NOT NULL,
    [FreightCost]        DECIMAL (8, 4)   NOT NULL,
    [TagCost]            DECIMAL (8, 4)   NOT NULL,
    [DeactivatedDate]    DATETIME         NULL
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_RunNumber_GrowerName_ProgramSeasonCode_SupplierIdentifier]
    ON [dbo].[GrowerProductProgramSeasonPriceUpload]([RunNumber] ASC, [GrowerName] ASC, [ProgramSeasonCode] ASC, [SupplierIdentifier] ASC);

