﻿CREATE TABLE [dbo].[ShipWeek] (
    [Id]                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [Guid]                 UNIQUEIDENTIFIER NOT NULL,
    [DateDeactivated]      DATETIME         NULL,
    [Week]                 INT              NOT NULL,
    [Year]                 INT              NOT NULL,
    [MondayDate]           DATETIME         NOT NULL,
    [ContinuousWeekNumber] INT              NOT NULL,
    [FiscalYear]           NVARCHAR (10)    NULL,
    CONSTRAINT [PK__ShipWeek__A2B5777C0902A3FE] PRIMARY KEY CLUSTERED ([Guid] ASC)
);







