﻿CREATE TABLE [dbo].[ImageSet] (
    [Guid]                   UNIQUEIDENTIFIER NOT NULL,
    [Id]                     BIGINT           IDENTITY (1, 1) NOT NULL,
    [Code]                   NVARCHAR (50)    NOT NULL,
    [Name]                   NVARCHAR (100)   NOT NULL,
    [Caption]                NVARCHAR (200)   NOT NULL,
    [Description]            NVARCHAR (500)   NOT NULL,
    [ImageSetTypeLookupGuid] UNIQUEIDENTIFIER NOT NULL,
    [Notes]                  NVARCHAR (500)   NOT NULL,
    [OriginalImageFileGuid]  UNIQUEIDENTIFIER NULL,
    [DateDeactivated]        DATETIME         NULL,
    CONSTRAINT [PK__ImageSet__A2B5777C4317F2D1] PRIMARY KEY CLUSTERED ([Guid] ASC),
    CONSTRAINT [UC_ImageSet_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);





