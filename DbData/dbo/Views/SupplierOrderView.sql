﻿

CREATE view [dbo].[SupplierOrderView]
AS
	SELECT
		SupplierOrder.Id AS SupplierOrderId,
		SupplierOrder.Guid AS SupplierOrderGuid,
		SupplierOrder.PoNo AS SupplierPoNo,
		GrowerOrderView.GrowerOrderGuid,
		GrowerOrderView.GrowerOrderId,
		GrowerOrderView.OrderNo AS GrowerOrderNo,
		GrowerOrderView.Description AS GrowerOrderDescription,
		GrowerOrderView.CustomerPoNo,
		GrowerOrderView.GrowerGuid,
		GrowerOrderView.GrowerId,
		GrowerOrderView.GrowerCode,
		GrowerOrderView.GrowerName,
		GrowerOrderView.ShipWeekGuid,
		GrowerOrderView.ShipWeekId,
		GrowerOrderView.ShipWeekCode,
		GrowerOrderView.ProductFormCategoryGuid,
		GrowerOrderView.ProductFormCategoryId,
		GrowerOrderView.ProductFormCategoryCode,
		GrowerOrderView.ProductFormCategoryName,
		GrowerOrderView.IsRooted,
		GrowerOrderView.ProgramTypeGuid,
		GrowerOrderView.ProgramTypeId,
		GrowerOrderView.ProgramTypeCode,
		GrowerOrderView.ProgramTypeName,
		GrowerOrderView.OrderTypeLookupGuid,
		GrowerOrderView.OrderTypeLookupCode,
		GrowerOrderView.GrowerOrderStatusLookupGuid,
		GrowerOrderView.GrowerOrderStatusLookupCode,
		GrowerOrderView.GrowerOrderStatusLookupName,
		GrowerOrderView.PersonGuid,
		GrowerOrderView.UserGuid,
		SupplierView.SupplierId,
		SupplierView.SupplierGuid,
		SupplierView.SupplierCode,
		SupplierView.SupplierName,
		SupplierView.AddressGuid,
		SupplierView.AddressId,
		SupplierView.AddressName,
		SupplierView.StreetAddress1,
		SupplierView.StreetAddress2,
		SupplierView.City,
		SupplierView.StateGuid,
		SupplierView.StateId,
		SupplierView.StateCode,
		SupplierView.CountryGuid,
		SupplierView.CountryId,
		SupplierView.CountryCode,
		SupplierView.CountryName,
		SupplierView.ZipCode,
		SupplierOrderStatusLookupView.LookupGuid AS SupplierOrderStatusLookupGuid,
		SupplierOrderStatusLookupView.LookupId AS SupplierOrderStatusLookupId,
		SupplierOrderStatusLookupView.LookupCode AS SupplierOrderStatusLookupCode,
		SupplierOrderStatusLookupView.LookupName As SupplierOrderStatusLookupName,
		SupplierOrderStatusLookupView.SortSequence AS SupplierOrderStatusLookupSortSequence,
		SupplierView.SellerGuid,
		SupplierView.SellerCode

	FROM SupplierOrder
		LEFT OUTER JOIN GrowerOrderView
			ON SupplierOrder.GrowerOrderGuid = GrowerOrderView.GrowerOrderGuid
		INNER JOIN SupplierView
			ON SupplierOrder.SupplierGuid = SupplierView.SupplierGuid
		INNER JOIN LookupView AS SupplierOrderStatusLookupView
			ON SupplierOrder.SupplierOrderStatusLookupGuid = SupplierOrderStatusLookupView.LookupGuid