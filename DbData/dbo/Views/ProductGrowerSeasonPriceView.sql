﻿CREATE VIEW [dbo].[ProductGrowerSeasonPriceView]
AS
	SELECT
		ProductSeasonBasePriceView.ProductGuid,
		GrowerView.GrowerGuid,
		ProductSeasonBasePriceView.ProgramSeasonGuid,
		ProductSeasonBasePriceView.SeasonStartDate,
		ProductSeasonBasePriceView.SeasonEndDate,
		CASE WHEN ProductGrowerSeasonVolumeLevelPriceView.PriceGuid IS NOT NULL THEN
			ProductGrowerSeasonVolumeLevelPriceView.PriceGuid
		ELSE
			ProductSeasonBasePriceView.PriceGuid
		END AS PriceGuid
	FROM ProductSeasonBasePriceView
		CROSS JOIN GrowerView
		LEFT OUTER JOIN ProductGrowerSeasonVolumeLevelPriceView ON
			ProductSeasonBasePriceView.ProductGuid=ProductGrowerSeasonVolumeLevelPriceView.ProductGuid AND
			ProductSeasonBasePriceView.ProgramSeasonGuid=ProductGrowerSeasonVolumeLevelPriceView.ProgramSeasonGuid AND
			GrowerView.GrowerGuid=ProductGrowerSeasonVolumeLevelPriceView.GrowerGuid
	WHERE
		ProductGrowerSeasonVolumeLevelPriceView.GrowerGuid IS NULL OR
		GrowerView.GrowerGuid = ProductGrowerSeasonVolumeLevelPriceView.GrowerGuid