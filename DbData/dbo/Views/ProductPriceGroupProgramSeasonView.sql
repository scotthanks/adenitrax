﻿
CREATE view [dbo].[ProductPriceGroupProgramSeasonView]
AS
	SELECT
		ProductPriceGroupProgramSeason.Id AS ProductPriceGroupProgramSeasonId,
		ProductPriceGroupProgramSeason.Guid AS ProductPriceGroupProgramSeasonGuid,
		ProductView.ProductGuid,
		ProductView.ProductCode,
		ProgramSeasonView.ProgramSeasonGuid,
		ProgramSeasonView.ProgramSeasonCode,
		ProgramSeasonView.ProgramSeasonName,
		ProgramSeasonView.StartDate as SeasonStartDate,
		ProgramSeasonView.EndDate as SeasonEndDate,
		ProgramSeasonView.EODDate,
		ProgramSeasonView.ProgramCode,
		ProgramSeasonView.ProgramTypeCode,
		ProgramSeasonView.SupplierCode,
		ProgramSeasonView.SupplierName,
		PriceGroupView.PriceGroupGuid
	FROM ProductPriceGroupProgramSeason
		INNER JOIN ProductView
			ON ProductPriceGroupProgramSeason.ProductGuid = ProductView.ProductGuid
		INNER JOIN ProgramSeasonView
			ON ProductPriceGroupProgramSeason.ProgramSeasonGuid = ProgramSeasonView.ProgramSeasonGuid
		INNER JOIN PriceGroupView
			ON ProductPriceGroupProgramSeason.PriceGroupGuid = PriceGroupView.PriceGroupGuid
		WHERE ProductPriceGroupProgramSeason.DateDeactivated IS NULL