﻿
CREATE view [dbo].[TripleProgramShipMethodDefaultLookupView]
AS
	SELECT
		PredicateLookup.Code AS PredicateLookupCode,
		ProgramView.ProgramCode AS ProgramCode,
		ProgramView.SupplierGuid AS SupplierGuid,
		ProgramView.ProgramTypeCode AS ProgramTypeCode,
		ProgramView.ProductFormCategoryCode AS ProductFormCategoryCode,
		ShipMethodDefaultLookupView.LookupCode AS ShipMethodLookupCode,
		PredicateLookup.Name AS PredicateLookupName,
		ShipMethodDefaultLookupView.LookupName AS ShipMethodLookupName,
		TripleView.TripleId,
		TripleView.TripleGuid,
		TripleView.SubjectGuid AS ProgramGuid,
		TripleView.PredicateLookupGuid,
		TripleView.ObjectGuid AS LookupGuid,
		PredicateLookup.Id AS PredicateLookupId,
		ShipMethodDefaultLookupView.LookupId AS ShipMethodLookupId,
		ShipMethodDefaultLookupView.LookupGuid AS ShipMethodLookupGuid
	FROM TripleView
		INNER JOIN ProgramView
			ON TripleView.SubjectGuid = ProgramView.ProgramGuid
		INNER JOIN Lookup AS PredicateLookup
			ON TripleView.PredicateLookupGuid = PredicateLookup.Guid
		INNER JOIN LookupView AS ShipMethodDefaultLookupView
			ON
				TripleView.ObjectGuid = ShipMethodDefaultLookupView.LookupGuid AND
				ShipMethodDefaultLookupView.Path LIKE 'Code/ShipMethod/%'
	WHERE PredicateLookup.Code = 'ProgramShipMethodDefault'