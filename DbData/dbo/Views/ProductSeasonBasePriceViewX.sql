﻿
CREATE view [dbo].[ProductSeasonBasePriceViewX] WITH SCHEMABINDING
AS
	SELECT
		Product.Guid AS ProductGuid,
		VolumeLevel.ProgramSeasonGuid,
		ProgramSeason.StartDate AS SeasonStartDate,
		ProgramSeason.EndDate AS SeasonEndDate,
		Price.Guid AS PriceGuid
	FROM dbo.Product
		INNER JOIN dbo.ProductPriceGroupProgramSeason
			ON Product.Guid = ProductPriceGroupProgramSeason.ProductGuid
		INNER JOIN dbo.ProgramSeason
			ON ProductPriceGroupProgramSeason.ProgramSeasonGuid = ProgramSeason.Guid
		INNER JOIN dbo.PriceGroup
			ON ProductPriceGroupProgramSeason.PriceGroupGuid = PriceGroup.Guid
		INNER JOIN dbo.Price
			ON ProductPriceGroupProgramSeason.PriceGroupGuid = Price.PriceGroupGuid 
		INNER JOIN dbo.VolumeLevel
			ON Price.VolumeLevelGuid = VolumeLevel.Guid AND
			   ProductPriceGroupProgramSeason.ProgramSeasonGuid = VolumeLevel.ProgramSeasonGuid
		WHERE
			VolumeLow=0 AND
			ProductPriceGroupProgramSeason.DateDeactivated IS NULL
GO
CREATE UNIQUE CLUSTERED INDEX [ixProductSeasonBasePriceViewX_ProductVolumeLevelSeason]
    ON [dbo].[ProductSeasonBasePriceViewX]([ProductGuid] ASC, [ProgramSeasonGuid] ASC, [SeasonStartDate] ASC);

