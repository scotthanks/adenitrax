﻿
CREATE view [dbo].[TripleView]
AS
	SELECT
		Lookup.Code AS PredicateLookupCode,
		Triple.SubjectGuid,
		Triple.ObjectGuid,
		Lookup.Name AS PredicateLookupName,
		Triple.Id AS TripleId,
		Triple.Guid AS TripleGuid,
		Triple.PredicateLookupGuid
	FROM Triple
		INNER JOIN Lookup
			ON PredicateLookupGuid = Lookup.Guid