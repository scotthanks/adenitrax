﻿
CREATE VIEW [dbo].[VolumeLevelView]
AS
	SELECT
		VolumeLevel.Code AS VolumeLevelCode,
		VolumeLevel.Name AS VolumeLevelName,
		VolumeLevel.VolumeLow,
		VolumeLevel.VolumeHigh,
		ProgramSeasonView.ProgramSeasonCode,
		ProgramSeasonView.ProgramSeasonName,
		ProgramSeasonView.StartDate AS SeasonStartDate,
		ProgramSeasonView.EndDate AS SeasonEndDate,
		ProgramSeasonView.EODDate,
		ProgramSeasonView.ProgramCode,
		ProgramSeasonView.ProgramTypeCode,
		ProgramSeasonView.SupplierCode,
		ProgramSeasonView.SupplierName,
		ProgramSeasonView.ProductFormCategoryCode,
		VolumeLevel.Id AS VolumeLevelId,
		VolumeLevel.Guid AS VolumeLevelGuid,
		ProgramSeasonView.ProgramSeasonId,
		ProgramSeasonView.ProgramSeasonGuid,
		ProgramSeasonView.ProgramGuid,
		ProgramSeasonView.ProgramTypeGuid,
		ProgramSeasonView.DefaultPeakWeekNumber,
		ProgramSeasonView.DefaultSeasonEndWeekNumber,
		ProgramSeasonView.SupplierGuid,
		ProgramSeasonView.ProductFormCategoryGuid
	FROM VolumeLevel
		INNER JOIN ProgramSeasonView
			ON VolumeLevel.ProgramSeasonGuid = ProgramSeasonView.ProgramSeasonGuid
	WHERE
		VolumeLevel.DateDeactivated IS NULL