﻿-- Should be empty
CREATE VIEW [dbo].[ProductGrowerSeasonPriceCountView]
AS
	SELECT
		ProductGuid,
		GrowerGuid,
		ProgramSeasonGuid,
		COUNT(PriceGuid) PriceCount
	FROM [ProductGrowerSeasonPriceView]
	GROUP BY
		ProductGuid,
		GrowerGuid,
		ProgramSeasonGuid
	HAVING COUNT(PriceGuid) > 1