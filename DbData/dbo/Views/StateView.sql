﻿
create view [dbo].[StateView]
AS
	SELECT
		Id AS StateId,
		Guid AS StateGuid,
		Code AS StateCode,
		Name AS StateName,
		CountryView.CountryId,
		CountryView.CountryGuid,
		CountryView.CountryCode,
		CountryView.CountryName
	FROM State
		INNER JOIN CountryView
			ON State.CountryGuid = CountryView.CountryGuid