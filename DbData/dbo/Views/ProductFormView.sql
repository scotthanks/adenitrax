﻿

CREATE VIEW [dbo].[ProductFormView]
AS
	SELECT
		ProductForm.Guid as ProductFormGuid,
		ProductForm.Code as ProductFormCode,
		ProductForm.SalesUnitQty,
		ProductForm.LineItemMinimumQty,
		ProductFormCategoryView.ProductFormCategoryGuid,
		ProductFormCategoryView.ProductFormCategoryCode
	FROM ProductForm with (nolock)
		INNER JOIN ProductFormCategoryView with (nolock) ON ProductForm.ProductFormCategoryGuid = ProductFormCategoryView.ProductFormCategoryGuid
			
	WHERE
		ProductForm.DateDeactivated IS NULL