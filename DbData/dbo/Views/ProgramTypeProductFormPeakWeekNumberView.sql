﻿CREATE VIEW [dbo].[ProgramTypeProductFormPeakWeekNumberView]
AS
	SELECT
		ProgramTypeCode,
		ProductFormCategoryCode,
		MIN(DefaultPeakWeekNumber) as DefaultPeekWeekNumber,
		MIN(DefaultSeasonEndWeekNumber) as DefaultSeasonEndWeekNumber,
		CASE WHEN MIN(DefaultPeakWeekNumber) = MAX(DefaultPeakWeekNumber) THEN 1 ELSE 0 END AS AllDefaultPeakWeekNumbersAreTheSame,
		CASE WHEN MIN(DefaultSeasonEndWeekNumber) = MAX(DefaultSeasonEndWeekNumber) THEN 1 ELSE 0 END AS AllDefaultSeasonEndWeekNumbersAreTheSame
	FROM ProgramView
	GROUP BY
		ProgramTypeCode,
		ProductFormCategoryCode