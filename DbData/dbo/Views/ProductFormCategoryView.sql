﻿
CREATE VIEW [dbo].[ProductFormCategoryView]
AS
	SELECT
		ProductFormCategory.Guid as ProductFormCategoryGuid,
		ProductFormCategory.Id as ProductFormCategoryId,
		ProductFormCategory.Code as ProductFormCategoryCode,
		ProductFormCategory.Name as Name,
		ProductFormCategory.IsRooted as IsRooted
	FROM ProductFormCategory
	WHERE
		ProductFormCategory.DateDeactivated IS NULL