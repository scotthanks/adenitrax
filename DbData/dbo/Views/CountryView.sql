﻿create view dbo.CountryView
AS
	SELECT
		Id AS CountryId,
		Guid As CountryGuid,
		Code AS CountryCode,
		Name AS CountryName
	FROM Country