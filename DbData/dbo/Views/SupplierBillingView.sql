﻿
CREATE VIEW [dbo].[SupplierBillingView]
	AS 
select
ProductOrderedCost,
ProductShippedCost,
FeeTotal,
SupplierOrderGuid,
OrderNumber,
GrowerName,
GrowerOrderDescription,
CustomerPoNo,
ShipWeekCode,
GrowerOrderStatusLookupCode,
SupplierName,
SupplierPoNo,
SupplierGuid,
PayDate,
ePsCheckNumber,
RecordDate,
SupplierInvoiceNumber,
ePsAssignedInvoiceNumber,
ePsCheckAmount,
SupplierInvoiceAmount
from
(select 
	SupplierOrderView.SupplierOrderGuid as SupplierOrderGuid,
	SupplierOrderView.GrowerOrderNo as OrderNumber,
	SupplierOrderView.GrowerName as GrowerName,
	SupplierOrderView.GrowerOrderDescription as GrowerOrderDescription,
	SupplierOrderView.CustomerPoNo as CustomerPoNo, 
	SupplierOrderView.ShipWeekCode as ShipWeekCode, 
	SupplierOrderView.GrowerOrderStatusLookupCode as GrowerOrderStatusLookupCode,
	SupplierOrderView.SupplierName as SupplierName,
	SupplierOrderView.SupplierPoNo as SupplierPoNo,
	SupplierOrderView.SupplierGuid as SupplierGuid
from SupplierOrderView) SupplierTable
left join
(
	select Ledger.TransactionDate as PayDate, Ledger.ParentGuid as InvoiceSupplierOrderGuid, 
	Ledger.ExternalIdNumber as ePsCheckNumber, Ledger.Amount as ePsCheckAmount from Ledger
    where Ledger.EntryTypeLookupGuid = '07D9C62F-73D5-4905-91BC-662E0269A217' -- debit
	 AND Ledger.GLAccountLookupGuid = 'FE952852-454E-4657-82E7-2E292D2B3C02' -- accounts payable
) LedgerPayInvoiceTable
on LedgerPayInvoiceTable.InvoiceSupplierOrderGuid = SupplierTable.SupplierOrderGuid
left join
(
	select Ledger.TransactionDate as RecordDate, Ledger.ParentGuid as PaySupplierPrderGuid, 
	Ledger.ExternalIdNumber as SupplierInvoiceNumber, Ledger.InternalIdNumber as ePsAssignedInvoiceNumber, 
	Ledger.Amount as SupplierInvoiceAmount from Ledger
    where Ledger.EntryTypeLookupGuid = '9B7335B3-4D1E-4EA7-99F6-D3266D18D536' -- credit
	 AND Ledger.GLAccountLookupGuid = 'FE952852-454E-4657-82E7-2E292D2B3C02' -- accounts payable
) LedgerRecordInvoiceTable
on LedgerRecordInvoiceTable.PaySupplierPrderGuid = SupplierTable.SupplierOrderGuid
join
	(select 
		SupplierOrder.Guid as sguid, 
		Sum(SupplierOrderFee.Amount) as FeeTotal 
	from
		SupplierOrder
		left outer join SupplierOrderFee on SupplierOrderFee.SupplierOrderGuid = SupplierOrder.Guid
		Group by 
		SupplierOrder.Guid
		) FeeTable
on FeeTable.sguid = SupplierTable.SupplierOrderGuid
join
	(select 
		SupplierOrder.Guid as sOrderGuid,
		Sum(OrderLine.ActualCost*OrderLine.QtyOrdered) as ProductOrderedCost, 
		Sum(OrderLine.ActualCost*OrderLine.QtyShipped) as ProductShippedCost 
	from SupplierOrder
		join OrderLine on OrderLine.SupplierOrderGuid = SupplierOrder.Guid
	Group by
	SupplierOrder.Guid) CostTable
on CostTable.sOrderGuid = SupplierTable.SupplierOrderGuid