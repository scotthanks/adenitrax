﻿CREATE view [dbo].[GrowerCreditCardView]
AS
	SELECT
		Id AS GrowerCreditCardId,
		Guid AS GrowerCreditCardGuid,
		AuthorizeNetSerialNumber,
		CardDescription,
		DefaultCard AS IsDefaultCard,
		GrowerView.GrowerGuid,
		GrowerView.GrowerCode,
		GrowerView.GrowerName,
		GrowerView.GrowerIsActivated
	FROM GrowerCreditCard
		INNER JOIN GrowerView
			ON GrowerCreditCard.GrowerGuid = GrowerView.GrowerGuid
	WHERE
		GrowerCreditCard.DateDeactivated IS NULL