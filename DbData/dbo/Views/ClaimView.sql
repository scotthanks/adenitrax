﻿





CREATE view [dbo].[ClaimView]
AS
	SELECT
		Claim.Id AS ClaimId,
		Claim.Guid AS ClaimGuid,
		Claim.GrowerOrderGuid,
		Claim.ClaimReasonLookupGuid,
		Claim.Comment,
		Claim.ClaimDate,
		Claim.CreditMemoLedgerGuid,
		Claim.ClaimStatusLookupGuid,
		Claim.ClaimNo,
		GrowerOrderView.GrowerGuid,
		GrowerOrderView.OrderNo,
		GrowerOrderView.Description,
		GrowerOrderView.CustomerPoNo,
		ShipWeekView.ShipWeekGuid,
		ShipWeekView.ShipWeekId,
		ShipWeekView.ShipWeekCode,
		GrowerOrderView.ProgramTypeName,
		GrowerOrderView.ProductFormCategoryName,
		Sum(ClaimOrderLine.QtyClaimed) as TotalQtyClaimed,
		Sum(OrderLine.ActualPrice * ClaimOrderLine.QtyClaimed) as ClaimAmount
	FROM Claim
		INNER JOIN ClaimOrderLine
			ON Claim.Guid = ClaimOrderLine.ClaimGuid
		INNER JOIN OrderLine
			ON ClaimOrderLine.OrderLineGuid = OrderLine.Guid
		INNER JOIN GrowerOrderView
			ON Claim.GrowerOrderGuid = GrowerOrderView.GrowerOrderGuid
		INNER JOIN ShipWeekView
			ON GrowerOrderView.ShipWeekGuid = ShipWeekView.ShipWeekGuid
	Group By
		Claim.Id,
		Claim.Guid,
		Claim.GrowerOrderGuid,
		Claim.ClaimReasonLookupGuid,
		Claim.Comment,
		Claim.ClaimDate,
		Claim.CreditMemoLedgerGuid,
		Claim.ClaimStatusLookupGuid,
		Claim.ClaimNo,
		GrowerOrderView.GrowerGuid,
		GrowerOrderView.OrderNo,
		GrowerOrderView.Description,
		GrowerOrderView.CustomerPoNo,
		ShipWeekView.ShipWeekGuid,
		ShipWeekView.ShipWeekId,
		ShipWeekView.ShipWeekCode,
		GrowerOrderView.ProgramTypeName,
		GrowerOrderView.ProductFormCategoryName