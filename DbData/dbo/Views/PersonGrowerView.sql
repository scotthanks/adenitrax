﻿


CREATE view [dbo].[PersonGrowerView]
AS
	SELECT
		PersonView.FirstName,
		PersonView.LastName,
		PersonView.UserCode,
		PersonView.PersonTypeLookupGuid,
		PersonView.PersonTypeLookupCode,
		PersonView.PersonTypeLookupName,
		GrowerView.GrowerCode,
		GrowerView.GrowerName,
		GrowerView.GrowerIsActivated,
		GrowerView.AllowSubstitutions,
		GrowerView.CreditLimit,
		GrowerView.CreditLimitTemporaryIncrease,
		GrowerView.Email,
		GrowerView.PhoneAreaCode,
		GrowerView.Phone,
		GrowerView.SalesTaxNumber,
		GrowerView.AddressName,
		GrowerView.StreetAddress1,
		GrowerView.StreetAddress2,
		GrowerView.City,
		GrowerView.ZipCode,
		GrowerView.StateCode,
		GrowerView.StateName,
		GrowerView.CountryCode,
		GrowerView.CountryName,
		GrowerView.GrowerTypeLookupCode,
		GrowerView.GrowerTypeLookupName,
		GrowerView.DefaultPaymentTypeLookupCode,
		GrowerView.DefaultPaymentTypeLookupName,
		PersonView.UserGuid,
		PersonView.PersonId,
		PersonView.PersonGuid,
		PersonView.UserId,
		GrowerView.GrowerId,
		GrowerView.GrowerGuid,
		GrowerView.AddressId,
		GrowerView.AddressGuid,
		GrowerView.StateGuid,
		GrowerView.StateId,
		GrowerView.CountryGuid,
		GrowerView.CountryId,
		GrowerView.GrowerTypeLookupId,
		GrowerView.GrowerTypeLookupGuid,
		GrowerView.DefaultPaymentTypeLookupId,
		GrowerView.DefaultPaymentTypeLookupGuid
	FROM PersonView
		--changed by Scott on Nov. 6, 2013
		--INNER JOIN TripleGrowerPersonView
		--	ON PersonView.PersonGuid = TripleGrowerPersonView.PersonGuid
		--INNER JOIN GrowerView
		--	ON TripleGrowerPersonView.GrowerGuid = GrowerView.GrowerGuid
	INNER JOIN GrowerView
	ON PersonView.GrowerGuid = GrowerView.GrowerGuid