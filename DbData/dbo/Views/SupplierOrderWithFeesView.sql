﻿

CREATE view [dbo].[SupplierOrderWithFeesView]
AS
	SELECT
		SupplierOrder.Id AS SupplierOrderId,
		SupplierOrder.Guid AS SupplierOrderGuid,
		
		ISNULL(SUM(SupplierOrderFee.Amount),0) as Amount
	FROM GrowerOrder
	JOIN SupplierOrder on GrowerOrder.Guid = SupplierOrder.GrowerOrderGuid
		
		LEFt Join SupplierOrderFee on SupplierOrder.Guid = SupplierOrderFee.SupplierOrderGuid
	GROUP BY GrowerOrder.Guid,SupplierOrder.Id,SupplierOrder.Guid