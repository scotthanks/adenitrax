﻿CREATE VIEW [dbo].[EventLogView] 
AS
	SELECT
		DATEADD(HOUR, -6, EventLog.EventTime) as EventTime,
		LogTypeLookupView.LookupCode AS LogTypeLookupCode,
		ObjectTypeLookupView.LookupCode AS ObjectTypeLookupCode,
		ProcessLookupView.LookupCode AS ProcessLookupCode,
		EventLog.Comment,
		EventLog.UserCode AS UserCode,
		PersonView.PersonGuid AS PersonGuid,
		PersonView.FirstName AS PersonFirstName,
		PersonView.LastName AS PersonLastName,
		EventLog.IntValue,
		EventLog.FloatValue,
		EventLog.GuidValue,
		EventLog.ObjectGuid,
		EventLog.Id AS EventLogId,
		EventLog.Guid AS EventLogGuid,
		EventLog.ProcessId,
		EventLog.SyncTime,
		EventLog.XmlData,
		LogTypeLookupView.LookupGuid AS LogTypeLookupGuid,
		LogTypeLookupView.LookupId AS LogTypeLookupId,
		ObjectTypeLookupView.LookupId AS ObjectTypeLookupId,
		ObjectTypeLookupView.LookupGuid AS ObjectTypeLookupGuid,
		ProcessLookupView.LookupGuid AS ProcessLookupGuid,
		ProcessLookupView.LookupId AS ProcessLookupId,
		PersonView.PersonId AS PersonId,
		OrderLine.Guid AS OrderLineGuid,
		CASE WHEN SupplierOrder.Guid IS NOT NULL THEN
			SupplierOrder.Guid
		ELSE
			SupplierOrderThroughOrderLine.Guid
		END AS SupplierOrderGuid,
		CASE WHEN GrowerOrder.Guid IS NOT NULL THEN
			GrowerOrder.Guid
		ELSE
			GrowerOrderThroughSupplierOrder.Guid
		END AS GrowerOrderGuid
	FROM EventLog
		INNER JOIN LookupView AS LogTypeLookupView
			ON EventLog.LogTypeLookupGuid = LogTypeLookupView.LookupGuid
		INNER JOIN LookupView AS ObjectTypeLookupView
			ON EventLog.ObjectTypeLookupGuid = ObjectTypeLookupView.LookupGuid
		INNER JOIN LookupView AS ProcessLookupView
			ON EventLog.ProcessLookupGuid = ProcessLookupView.LookupGuid
		LEFT OUTER JOIN PersonView
			ON EventLog.PersonGuid = PersonView.PersonGuid
		LEFT OUTER JOIN Lookup AS ObjectTypeLookup
			ON EventLog.ObjectTypeLookupGuid = ObjectTypeLookup.Guid
		LEFT OUTER JOIN OrderLine
			ON EventLog.ObjectGuid = OrderLine.Guid AND ObjectTypeLookup.Code = 'OrderLine'
		LEFT OUTER JOIN SupplierOrder
			ON EventLog.ObjectGuid = SupplierOrder.Guid AND ObjectTypeLookup.Code = 'SupplierOrder'
		LEFT OUTER JOIN GrowerOrder
			ON EventLog.ObjectGuid = GrowerOrder.Guid AND ObjectTypeLookup.Code = 'GrowerOrder'
		LEFT OUTER JOIN SupplierOrder AS SupplierOrderThroughOrderLine
			ON OrderLine.SupplierOrderGuid = SupplierOrderThroughOrderLine.Guid
		LEFT OUTER JOIN GrowerOrder AS GrowerOrderThroughSupplierOrder
			ON SupplierOrder.GrowerOrderGuid = GrowerOrderThroughSupplierOrder.Guid OR
			   SupplierOrderThroughOrderLine.GrowerOrderGuid = GrowerOrderThroughSupplierOrder.Guid