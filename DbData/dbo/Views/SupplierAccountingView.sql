﻿CREATE VIEW [dbo].[SupplierAccountingView]
AS
	SELECT
		[Id] AS SupplierId,
		[Guid] AS SupplierGuid,
		[Code] AS SupplierCode,
		[Name] AS SupplierName,
		[EMail] AS SupplierEmail,
		AddressView.AddressGuid,
		AddressView.AddressId,
		AddressView.AddressName,
		AddressView.StreetAddress1,
		AddressView.StreetAddress2,
		AddressView.City,
		AddressView.StateGuid,
		AddressView.StateId,
		AddressView.StateCode,
		AddressView.CountryGuid,
		AddressView.CountryId,
		AddressView.CountryCode,
		AddressView.CountryName,
		AddressView.ZipCode
	FROM Supplier
		INNER JOIN AddressView
			ON Supplier.AddressGuid = AddressView.AddressGuid