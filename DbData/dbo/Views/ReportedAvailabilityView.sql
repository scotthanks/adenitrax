﻿




CREATE VIEW [dbo].[ReportedAvailabilityView]
AS
	SELECT
		ShipWeekView.ShipWeekCode,
--		ReportedAvailability.Qty AS ReportedAvailabilityQty,
		ReportedAvailability.Qty - ReportedAvailability.SalesSinceDateReported AS ReportedAvailabilityQty,
		AvailabilityTypeLookup.LookupCode AS AvailabilityTypeLookupCode,
		ProductView.ProductCode,
		ProductView.ProgramTypeCode,
		ProductView.ProductFormCode,
		ProductView.GeneticOwnerCode,
		ProductView.ProductFormCategoryCode,
		ProductView.ProgramCode,
		ProductView.SupplierCode,
		ProductView.SupplierName,
		ProductView.VarietyCode,
		ProductView.VarietyName,
		ReportedAvailability.Guid AS ReportedAvailabilityGuid,
		ProductView.ProductGuid,
		ProductView.ProgramGuid,
		ProductView.ProgramTypeGuid,
		ProductView.SupplierGuid,
		ProductView.VarietyGuid,
		ProductView.SpeciesGuid,
		ProductView.SpeciesCode,
		ProductView.SpeciesName,
		ProductView.GeneticOwnerGuid,
		ProductView.ProductFormGuid,
		ProductView.SalesUnitQty,
		ProductView.LineItemMinimumQty,
		ProductView.ProductFormCategoryGuid,
		ProductView.ProductSupplierIdentifier,
		ProductView.SellerCode,
		ProductView.IsOrganic,
		ProductView.IsExclusive,
		ShipWeekView.ShipWeekGuid,
		ShipWeekView.ShipWeekYear,
		ShipWeekView.ShipWeekWeek,
		ShipWeekView.ShipWeekMondayDate,
		ShipWeekView.ShipWeekContinuousWeekNumber,
		AvailabilityTypeLookup.LookupGuid AS AvailabilityTypeLookupGuid
	FROM ReportedAvailability with (nolock)
		INNER JOIN ProductView with (nolock)
			ON ReportedAvailability.ProductGuid = ProductView.ProductGuid
		INNER JOIN ShipWeekView with (nolock)
			ON ReportedAvailability.ShipWeekGuid = ShipWeekView.ShipWeekGuid
		INNER JOIN LookupView AS AvailabilityTypeLookup with (nolock)
			ON ReportedAvailability.AvailabilityTypeLookupGuid = AvailabilityTypeLookup.LookupGuid