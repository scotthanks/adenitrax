﻿CREATE view [dbo].[TripleProgramShipMethodShownLookupView]
AS
	SELECT
		TripleProgramShipMethodLookupView.PredicateLookupCode,
		TripleProgramShipMethodLookupView.ProgramCode,
		TripleProgramShipMethodLookupView.ShipMethodLookupCode,
		TripleProgramShipMethodLookupView.PredicateLookupName,
		TripleProgramShipMethodLookupView.ShipMethodLookupName,
		TripleProgramShipMethodLookupView.TripleId,
		TripleProgramShipMethodLookupView.TripleGuid,
		TripleProgramShipMethodLookupView.ProgramGuid,
		TripleProgramShipMethodLookupView.PredicateLookupGuid,
		TripleProgramShipMethodLookupView.LookupGuid,
		TripleProgramShipMethodLookupView.PredicateLookupId,
		TripleProgramShipMethodLookupView.ShipMethodLookupId,
		TripleProgramShipMethodLookupView.ShipMethodLookupGuid
	FROM TripleProgramShipMethodLookupView
	WHERE TripleProgramShipMethodLookupView.PredicateLookupCode = 'ProgramShipMethodShown'