﻿CREATE view [dbo].[ProgramShipMethodView]
AS
    SELECT
		ProgramView.ProgramGuid,
		TripleProgramShipMethodShownLookupView.ShipMethodLookupGuid,
		CASE
			WHEN TripleProgramShipMethodDefaultLookupView.ShipMethodLookupGuid IS NOT NULL THEN 
				CAST(1 AS BIT) 
			ELSE 
				CAST(0 AS BIT) 
		END AS IsDefault
	FROM ProgramView
        INNER JOIN TripleProgramShipMethodShownLookupView
                ON ProgramView.ProgramGuid=TripleProgramShipMethodShownLookupView.ProgramGuid
        LEFT OUTER JOIN TripleProgramShipMethodDefaultLookupView
                ON ProgramView.ProgramGuid=TripleProgramShipMethodDefaultLookupView.ProgramGuid AND
					TripleProgramShipMethodDefaultLookupView.LookupGuid = TripleProgramShipMethodShownLookupView.LookupGuid
	GROUP BY  
		ProgramView.ProgramGuid,
		TripleProgramShipMethodShownLookupView.ShipMethodLookupGuid,
		TripleProgramShipMethodDefaultLookupView.ShipMethodLookupGuid