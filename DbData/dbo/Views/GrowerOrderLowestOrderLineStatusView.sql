﻿
CREATE view [dbo].[GrowerOrderLowestOrderLineStatusView]
AS
	SELECT
		GrowerOrderView.GrowerOrderGuid,
		MIN(OrderLineStatusLookupView.LookupGuid) AS OrderLineStatusLookupGuid
	FROM GrowerOrderView
		INNER JOIN
			(
				SELECT
					GrowerOrderView.GrowerOrderGuid,
					MIN(SortSequence) AS MinSortSequence
				FROM GrowerOrderView
					INNER JOIN SupplierOrderView
						ON GrowerOrderView.GrowerOrderGuid = SupplierOrderView.GrowerOrderGuid
					INNER JOIN OrderLineViewShowInactiveProduct OrderLineView
						ON SupplierOrderView.SupplierOrderGuid = OrderLineView.SupplierOrderGuid
					INNER JOIN LookupView AS OrderLineStatusView
						ON OrderLineView.OrderLineStatusLookupGuid = OrderLineStatusView.LookupGuid
				GROUP BY
					GrowerOrderView.GrowerOrderGuid
			) GrowerOrderLowestOrderLineSequenceNumberView
				ON GrowerOrderView.GrowerOrderGuid = GrowerOrderLowestOrderLineSequenceNumberView.GrowerOrderGuid
		INNER JOIN LookupView OrderLineStatusLookupView
			ON
				GrowerOrderLowestOrderLineSequenceNumberView.MinSortSequence = OrderLineStatusLookupView.SortSequence AND
				OrderLineStatusLookupView.Path LIKE 'Code/OrderLineStatus/%'
	GROUP BY GrowerOrderView.GrowerOrderGuid