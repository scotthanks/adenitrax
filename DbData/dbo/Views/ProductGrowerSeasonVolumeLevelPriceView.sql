﻿--SELECT * FROM ProductGrowerSeasonVolumeLevelPriceView WHERE GrowerVolumeAndPriceHaveSameSeason = 0
--SELECT * FROM ProductGrowerSeasonVolumeLevelPriceView WHERE ProductPriceGroupProgramSeasonAndPriceHaveSameSeason = 0

CREATE VIEW [dbo].[ProductGrowerSeasonVolumeLevelPriceView]
AS
	SELECT
		ProductView.ProductGuid,
		GrowerVolumeView.GrowerGuid,
		GrowerVolumeView.ProgramSeasonGuid,
		ProductPriceGroupProgramSeasonView.SeasonStartDate,
		ProductPriceGroupProgramSeasonView.SeasonEndDate,
		PriceView.PriceGuid,
		CASE WHEN GrowerVolumeView.ProgramSeasonGuid=PriceView.ProgramSeasonGuid THEN
			CAST(1 AS BIT)
		ELSE
			CAST(0 AS BIT)
		END AS GrowerVolumeAndPriceHaveSameSeason,
		CASE WHEN ProductPriceGroupProgramSeasonView.ProgramSeasonGuid=PriceView.ProgramSeasonGuid THEN
			CAST(1 AS BIT)
		ELSE
			CAST(0 AS BIT)
		END AS ProductPriceGroupProgramSeasonAndPriceHaveSameSeason
	FROM GrowerVolumeView
		INNER JOIN ProductView
			ON GrowerVolumeView.ProgramGuid=ProductView.ProgramGuid
		INNER JOIN ProductPriceGroupProgramSeasonView
			ON
				ProductView.ProductGuid = ProductPriceGroupProgramSeasonView.ProductGuid AND
				GrowerVolumeView.ProgramSeasonGuid = ProductPriceGroupProgramSeasonView.ProgramSeasonGuid
		INNER JOIN PriceView
			ON
				GrowerVolumeView.VolumeLevelGuid = PriceView.VolumeLevelGuid AND
				ProductPriceGroupProgramSeasonView.PriceGroupGuid = PriceView.PriceGroupGuid
