﻿
CREATE VIEW [dbo].[ImageSetView]
AS
	SELECT
		ImageSet.Guid AS ImageSetGuid,
		ImageSet.Id AS ImageSetId,
		ImageSet.Code AS ImageSetCode,
		ImageSet.Name AS ImageSetName,
		ImageSet.Caption,
		ImageSet.Description,
		ImageSet.OriginalImageFileGuid,
		ThumbnailImageFileView.ImageFileGuid AS ThumbnailImageFileGuid,
		ThumbnailImageFileView.FileExtensionLookupGuid AS ThumbnailImageFileFileExtensionLookupGuid
	FROM ImageSet with (nolock)
		INNER JOIN Lookup AS ImageSetTypeLookup with (nolock)
			ON ImageSet.ImageSetTypeLookupGuid = ImageSetTypeLookup.Guid
		LEFT OUTER JOIN ImageFileView AS ThumbnailImageFileView with (nolock) ON
				ImageSet.Guid = ThumbnailImageFileView.ImageSetGuid AND 
				ThumbnailImageFileView.ImageFileTypeLookupCode = 'Thumbnail'
	WHERE
		ImageSet.DateDeactivated IS NULL AND
		ImageSetTypeLookup.DateDeactivated IS NULL