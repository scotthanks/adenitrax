﻿
CREATE view [dbo].[GrowerAddressView]
AS
	SELECT
		GrowerView.GrowerCode,
		GrowerView.GrowerName,
		GrowerView.GrowerIsActivated,
		GrowerView.AllowSubstitutions,
		GrowerView.CreditLimit,
		GrowerView.CreditLimitTemporaryIncrease,
		GrowerView.Email,
		GrowerView.PhoneAreaCode,
		GrowerView.Phone,
		GrowerView.SalesTaxNumber,
		GrowerView.GrowerTypeLookupCode,
		GrowerView.GrowerTypeLookupName,
		GrowerView.DefaultPaymentTypeLookupCode,
		GrowerView.DefaultPaymentTypeLookupName,
		AddressView.AddressName,
		AddressView.StreetAddress1,
		AddressView.StreetAddress2,
		AddressView.City,
		AddressView.ZipCode,
		AddressView.StateCode,
		AddressView.StateName,
		AddressView.CountryCode,
		AddressView.CountryName,
		GrowerView.AddressGuid AS GrowerMainAddressGuid,
		GrowerAddress.Guid AS GrowerAddressGuid,
		GrowerAddress.Id AS GrowerAddressId,
		GrowerAddress.PhoneAreaCode AS GrowerAddressPhoneAreaCode,
		GrowerAddress.Phone AS GrowerAddressPhone,
		GrowerAddress.SpecialInstructions,
		GrowerAddress.DefaultAddress,
		GrowerView.GrowerId,
		GrowerView.GrowerGuid,
		AddressView.AddressGuid,
		AddressView.StateGuid,
		AddressView.StateId,
		AddressView.CountryGuid,
		AddressView.CountryId,
		GrowerView.GrowerTypeLookupId,
		GrowerView.GrowerTypeLookupGuid,
		GrowerView.DefaultPaymentTypeLookupId,
		GrowerView.DefaultPaymentTypeLookupGuid
	FROM GrowerView
		INNER JOIN GrowerAddress
			ON GrowerView.GrowerGuid = GrowerAddress.GrowerGuid
		INNER JOIN AddressView ON
			GrowerAddress.AddressGuid = AddressView.AddressGuid
	WHERE DateDeactivated IS NULL