﻿CREATE view [dbo].[TripleFileExtensionMimeTypeView]
AS
	SELECT
		PredicateLookup.Code AS PredicateLookupCode,
		SubjectLookup.Code AS FileExtensionLookupCode,
		ObjectLookup.Code AS MimeTypeLookupCode,
		SubjectLookup.Name AS FileExtensionLookupName,
		PredicateLookup.Name AS PredicateLookupName,
		ObjectLookup.Name AS MimeTypeLookupName,
		TripleView.TripleId,
		TripleView.TripleGuid,
		TripleView.SubjectGuid AS FileExtensionLookupGuid,
		TripleView.PredicateLookupGuid,
		TripleView.ObjectGuid AS MimeTypeLookupGuid,
		SubjectLookup.Id AS FileExtensionLookupId,
		PredicateLookup.Id AS PredicateLookupId,
		ObjectLookup.Id AS MimeTypeLookupId
	FROM TripleView
		INNER JOIN Lookup AS SubjectLookup
			ON TripleView.SubjectGuid = SubjectLookup.Guid
		INNER JOIN Lookup AS PredicateLookup
			ON TripleView.PredicateLookupGuid = PredicateLookup.Guid
		INNER JOIN Lookup AS ObjectLookup
			ON TripleView.ObjectGuid = ObjectLookup.Guid
	WHERE PredicateLookup.Path = 'Predicate/FileExtensionMimeType'