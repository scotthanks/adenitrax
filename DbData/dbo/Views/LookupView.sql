﻿

CREATE view [dbo].[LookupView]
as
	SELECT
		Lookup.Guid AS LookupGuid,
		Lookup.Id AS LookupId,
		Lookup.Code AS LookupCode,
		Lookup.Name AS LookupName,
		Lookup.SortSequence,
		Lookup.Path AS Path,
		Lookup.ParentLookupGuid,
		ParentLookup.Code AS ParentLookupCode,
		ParentLookup.Name AS ParentLookupName,
		ParentLookup.Path AS ParentLookupPath,
		CASE WHEN ParentLookup.SequenceChildren = 1 THEN
			ISNULL(ParentLookup.Path,'') + '/' + FORMAT(Lookup.SortSequence + 10000, '00000.00000') + '/' + Lookup.Code
		ELSE
			Lookup.Path
		END AS SortKey
	FROM Lookup with (nolock)
		LEFT OUTER JOIN Lookup  AS ParentLookup with (nolock)
			ON Lookup.ParentLookupGuid = ParentLookup.Guid