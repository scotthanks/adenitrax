﻿CREATE view [dbo].[ProductSeasonBasePriceView]
AS
	SELECT
		ProductView.ProductGuid,
		PriceView.ProgramSeasonGuid,
		ProductPriceGroupProgramSeasonView.SeasonStartDate,
		ProductPriceGroupProgramSeasonView.SeasonEndDate,
		PriceView.PriceGuid
	FROM ProductView
		INNER JOIN ProductPriceGroupProgramSeasonView
			ON ProductView.ProductGuid = ProductPriceGroupProgramSeasonView.ProductGuid
		INNER JOIN PriceView
			ON ProductPriceGroupProgramSeasonView.PriceGroupGuid = PriceView.PriceGroupGuid AND
			   ProductPriceGroupProgramSeasonView.ProgramSeasonGuid = PriceView.ProgramSeasonGuid
	WHERE VolumeLow=0