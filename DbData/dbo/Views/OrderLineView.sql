﻿





CREATE view [dbo].[OrderLineView]
AS
	SELECT
		SupplierOrderView.GrowerOrderNo,
		SupplierOrderView.SupplierPoNo,
		SupplierOrderView.GrowerOrderDescription,
		SupplierOrderView.ProductFormCategoryCode AS GrowerOrderProductFormCategoryCode,
		SupplierOrderView.ProductFormCategoryName AS GrowerOrderProductFormCategoryName,
		SupplierOrderView.ProgramTypeCode AS GrowerOrderProgramTypeCode,
		SupplierOrderView.ProgramTypeName AS GrowerOrderProgramTypeName,
		SupplierOrderView.PersonGuid,
		OrderLineStatusLookupView.LookupCode AS OrderLineStatusLookupCode,
		OrderLineStatusLookupView.LookupName AS OrderLineStatusLookupName,
		OrderLineStatusLookupView.SortSequence AS OrderLineStatusLookupSortSequence,
		SupplierOrderView.SupplierOrderStatusLookupCode,
		SupplierOrderView.SupplierOrderStatusLookupName,
		OrderLine.Id AS OrderLineId,
		OrderLine.Guid AS OrderLineGuid,
		OrderLine.QtyOrdered,
		OrderLine.QtyShipped,
		OrderLine.ActualPriceUsedGuid,
		ProductView.VarietyCode,
		ProductView.SpeciesCode,
		ProductView.GeneticOwnerCode,
		ProductView.ProductFormCode,
		ProductView.ProductFormCategoryCode,
		ProductView.ProductCode,
		SupplierOrderView.SupplierOrderId,
		SupplierOrderView.SupplierOrderGuid,
		SupplierOrderView.GrowerOrderGuid,
		SupplierOrderView.GrowerOrderId,
		SupplierOrderView.CustomerPoNo,
		SupplierOrderView.GrowerGuid,
		SupplierOrderView.GrowerId,
		SupplierOrderView.GrowerCode,
		SupplierOrderView.GrowerName,
		SupplierOrderView.ShipWeekGuid,
		SupplierOrderView.ShipWeekId,
		SupplierOrderView.ShipWeekCode,
		SupplierOrderView.ProductFormCategoryGuid AS GrowerOrderProductFormCategoryGuid,
		SupplierOrderView.ProductFormCategoryId AS GrowerOrderProductFormCategoryId,
		SupplierOrderView.IsRooted,
		SupplierOrderView.ProgramTypeGuid AS GrowerOrderProgramTypeGuid,
		SupplierOrderView.ProgramTypeId AS GrowerOrderProgramTypeId,
		SupplierOrderView.OrderTypeLookupGuid,
		SupplierOrderView.OrderTypeLookupCode,
		SupplierOrderView.GrowerOrderStatusLookupGuid,
		SupplierOrderView.GrowerOrderStatusLookupCode,
		SupplierOrderView.GrowerOrderStatusLookupName,
		SupplierOrderView.SupplierId,
		SupplierOrderView.SupplierGuid,
		SupplierOrderView.SupplierCode,
		SupplierOrderView.SupplierName,
		SupplierOrderView.AddressGuid,
		SupplierOrderView.AddressId,
		SupplierOrderView.AddressName,
		SupplierOrderView.StreetAddress1,
		SupplierOrderView.StreetAddress2,
		SupplierOrderView.City,
		SupplierOrderView.StateGuid,
		SupplierOrderView.StateId,
		SupplierOrderView.StateCode,
		SupplierOrderView.CountryGuid,
		SupplierOrderView.CountryId,
		SupplierOrderView.CountryCode,
		SupplierOrderView.CountryName,
		SupplierOrderView.ZipCode,
		SupplierOrderView.SupplierOrderStatusLookupGuid,
		SupplierOrderView.SupplierOrderStatusLookupId,
		SupplierOrderView.SupplierOrderStatusLookupSortSequence,
		SupplierOrderView.UserGuid,
		ProductView.ProductGuid,
		ProductView.ProgramGuid,
		ProductView.ProgramCode,
		ProductView.ProgramTypeGuid,
		ProductView.ProgramTypeCode,
		ProductView.SupplierGuid AS ProductSupplierGuid,
		ProductView.VarietyGuid,
		ProductView.SpeciesGuid,
		ProductView.GeneticOwnerGuid,
		ProductView.VarietyName,
		ProductView.VarietyActiveDate,
		ProductView.VarietyDeactiveDate,
		ProductView.VarietyColorDescription,
		ProductView.VarietyColorLookupGuid,
		ProductView.VarietyColorLookupCode,
		ProductView.VarietyHabitLookupGuid,
		ProductView.VarietyHabitLookupCode,
		ProductView.VarietyVigorLookupGuid,
		ProductView.VarietyVigorLookupCode,
		ProductView.VarietyImageSetGuid,
		ProductView.VarietyThumbnailImageFileGuid,
		ProductView.VarietyThumbnailImageFileFileExtensionLookupGuid,
		ProductView.ProductFormGuid,
		ProductView.ProductFormCategoryGuid,
		OrderLineStatusLookupView.LookupGuid AS OrderLineStatusLookupGuid,
		CASE WHEN SupplierOrderView.SupplierGuid = ProductView.SupplierGuid THEN 1 ELSE 0 END AS SupplierGuidsMatch,
		CASE WHEN SupplierOrderView.ProductFormCategoryGuid = ProductView.ProductFormCategoryGuid THEN 1 ELSE 0 END AS ProductFormCategoryGuidsMatch,
		CASE WHEN SupplierOrderView.ProgramTypeGuid = ProductView.ProgramTypeGuid THEN 1 ELSE 0 END AS ProgramTypeGuidsMatch,
		SupplierOrderView.SellerGuid,
		SupplierOrderView.SellerCode
	FROM OrderLine
		INNER JOIN SupplierOrderView
			ON OrderLine.SupplierOrderGuid = SupplierOrderView.SupplierOrderGuid
		INNER JOIN ProductView
			ON OrderLine.ProductGuid = ProductView.ProductGuid
		--INNER JOIN ProductForm
		--	ON Product.ProductFormGuid = ProductForm.Guid
		--INNER JOIN ProductFormCategory
		--	ON ProductForm.ProductFormCategoryGuid = ProductFormCategory.Guid
		--INNER JOIN ProgramView
		--	ON Product.ProgramGuid = ProgramView.ProgramGuid
		--INNER JOIN VarietyView
		--	ON Product.VarietyGuid = VarietyView.VarietyGuid
		INNER JOIN LookupView AS OrderLineStatusLookupView
			ON OrderLine.OrderLineStatusLookupGuid = OrderLineStatusLookupView.LookupGuid