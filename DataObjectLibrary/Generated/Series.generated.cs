using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Series : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Code,
			Name,
			SpeciesGuid,
			DescriptionHTMLGUID,
			CultureLibraryLink,
		}

		new public enum ForeignKeyFieldEnum
		{
			SpeciesGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Species),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public Guid SpeciesGuid { get; set; }
		public Guid? DescriptionHTMLGUID { get; set; }
		public string CultureLibraryLink { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "SpeciesGuid":
					value = GuidToString( SpeciesGuid);
					break;
				case "DescriptionHTMLGUID":
					value = GuidToString( DescriptionHTMLGUID);
					break;
				case "CultureLibraryLink":
					value = StringToString( CultureLibraryLink);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "SpeciesGuid":
					SpeciesGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DescriptionHTMLGUID":
					DescriptionHTMLGUID = GuidFromString( value, isNullable: true);
					break;
				case "CultureLibraryLink":
					CultureLibraryLink = (string)StringFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "SpeciesGuid":
						value = SpeciesGuid;
						break;
					case "DescriptionHTMLGUID":
						value = DescriptionHTMLGUID;
						break;
					case "CultureLibraryLink":
						value = CultureLibraryLink;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "SpeciesGuid":
						SpeciesGuid = (Guid)value;
						break;
					case "DescriptionHTMLGUID":
						DescriptionHTMLGUID = (Guid?)value;
						break;
					case "CultureLibraryLink":
						CultureLibraryLink = (string)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Species Lazy Loading Properties and Methods

        private Lazy<Species> _lazySpecies;

        public Species Species
        {
            get
            {
                return _lazySpecies == null ? null : _lazySpecies.Value;
            }
            set
            {
                _lazySpecies = new Lazy<Species>(() => value);
            }
        }

        public bool SpeciesIsLoaded
        {
            get
            {
                return _lazySpecies == null ? false : _lazySpecies.IsValueCreated;
            }
        }

        public void SetLazySpecies(Lazy<Species> lazySpecies)
        {
            _lazySpecies = lazySpecies;
        }

		#endregion

        //#region PromoCode Lazy Loading Properties and Methods

        //private Lazy<List<PromoCode>> _lazyPromoCodeList;

        //public List<PromoCode> PromoCodeList
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? null : _lazyPromoCodeList.Value;
        //    }
        //}

        //public bool PromoCodeListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? false : _lazyPromoCodeList.IsValueCreated;
        //    }
        //}

        //public void SetLazyPromoCodeList(Lazy<List<PromoCode>> lazyPromoCodeList)
        //{
        //    _lazyPromoCodeList = lazyPromoCodeList;
        //}

        //#endregion

		#region Variety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyVarietyList;

        public List<Variety> VarietyList
        {
            get
            {
                return _lazyVarietyList == null ? null : _lazyVarietyList.Value;
            }
        }

        public bool VarietyListIsLoaded
        {
            get
            {
                return _lazyVarietyList == null ? false : _lazyVarietyList.IsValueCreated;
            }
        }

        public void SetLazyVarietyList(Lazy<List<Variety>> lazyVarietyList)
        {
            _lazyVarietyList = lazyVarietyList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "SpeciesGuid={0}",this.SpeciesGuid);
			result.Append(", ");
			result.AppendFormat( "DescriptionHTMLGUID={0}",this.DescriptionHTMLGUID);
			result.Append(", ");
			result.AppendFormat( "CultureLibraryLink={0}",this.CultureLibraryLink);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
