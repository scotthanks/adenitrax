using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class ReportedAvailability : TableObjectBase
    {
        public enum ColumnEnum
        {
            Id,
            Guid,
            DateReported,
            ProductGuid,
            ShipWeekGuid,
            Qty,
            AvailabilityTypeLookupGuid,
            SalesSinceDateReported,
        }

        new public enum ForeignKeyFieldEnum
        {
            ProductGuid,
            ShipWeekGuid,
            AvailabilityTypeLookupGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(Product),
                        typeof(ShipWeek),
                        typeof(Lookup),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public DateTime DateReported { get; set; }
        public Guid ProductGuid { get; set; }
        public Guid ShipWeekGuid { get; set; }
        public int Qty { get; set; }
        public Guid AvailabilityTypeLookupGuid { get; set; }
        public int? SalesSinceDateReported { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "Id":
                    value = LongToString(Id);
                    break;
                case "Guid":
                    value = GuidToString(Guid);
                    break;
                case "DateReported":
                    value = DateTimeToString(DateReported);
                    break;
                case "ProductGuid":
                    value = GuidToString(ProductGuid);
                    break;
                case "ShipWeekGuid":
                    value = GuidToString(ShipWeekGuid);
                    break;
                case "Qty":
                    value = IntToString(Qty);
                    break;
                case "AvailabilityTypeLookupGuid":
                    value = GuidToString(AvailabilityTypeLookupGuid);
                    break;
                case "SalesSinceDateReported":
                    value = IntToString(SalesSinceDateReported);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "Id":
                    Id = (long)LongFromString(value, isNullable: false);
                    break;
                case "Guid":
                    Guid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "DateReported":
                    DateReported = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "ProductGuid":
                    ProductGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ShipWeekGuid":
                    ShipWeekGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "Qty":
                    Qty = (int)IntFromString(value, isNullable: false);
                    break;
                case "AvailabilityTypeLookupGuid":
                    AvailabilityTypeLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "SalesSinceDateReported":
                    SalesSinceDateReported = IntFromString(value, isNullable: true);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "Id":
                        value = Id;
                        break;
                    case "Guid":
                        value = Guid;
                        break;
                    case "DateReported":
                        value = DateReported;
                        break;
                    case "ProductGuid":
                        value = ProductGuid;
                        break;
                    case "ShipWeekGuid":
                        value = ShipWeekGuid;
                        break;
                    case "Qty":
                        value = Qty;
                        break;
                    case "AvailabilityTypeLookupGuid":
                        value = AvailabilityTypeLookupGuid;
                        break;
                    case "SalesSinceDateReported":
                        value = SalesSinceDateReported;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "Id":
                        Id = (long)value;
                        break;
                    case "Guid":
                        Guid = (Guid)value;
                        break;
                    case "DateReported":
                        DateReported = (DateTime)value;
                        break;
                    case "ProductGuid":
                        ProductGuid = (Guid)value;
                        break;
                    case "ShipWeekGuid":
                        ShipWeekGuid = (Guid)value;
                        break;
                    case "Qty":
                        Qty = (int)value;
                        break;
                    case "AvailabilityTypeLookupGuid":
                        AvailabilityTypeLookupGuid = (Guid)value;
                        break;
                    case "SalesSinceDateReported":
                        SalesSinceDateReported = (int?)value;
                        break;
                }
            }
        }

        #region Lazy Loading Logic

        #region Product Lazy Loading Properties and Methods

        private Lazy<Product> _lazyProduct;

        public Product Product
        {
            get
            {
                return _lazyProduct == null ? null : _lazyProduct.Value;
            }
            set
            {
                _lazyProduct = new Lazy<Product>(() => value);
            }
        }

        public bool ProductIsLoaded
        {
            get
            {
                return _lazyProduct == null ? false : _lazyProduct.IsValueCreated;
            }
        }

        public void SetLazyProduct(Lazy<Product> lazyProduct)
        {
            _lazyProduct = lazyProduct;
        }

        #endregion

        #region ShipWeek Lazy Loading Properties and Methods

        private Lazy<ShipWeek> _lazyShipWeek;

        public ShipWeek ShipWeek
        {
            get
            {
                return _lazyShipWeek == null ? null : _lazyShipWeek.Value;
            }
            set
            {
                _lazyShipWeek = new Lazy<ShipWeek>(() => value);
            }
        }

        public bool ShipWeekIsLoaded
        {
            get
            {
                return _lazyShipWeek == null ? false : _lazyShipWeek.IsValueCreated;
            }
        }

        public void SetLazyShipWeek(Lazy<ShipWeek> lazyShipWeek)
        {
            _lazyShipWeek = lazyShipWeek;
        }

        #endregion

        #region AvailabilityTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyAvailabilityTypeLookup;

        public Lookup AvailabilityTypeLookup
        {
            get
            {
                return _lazyAvailabilityTypeLookup == null ? null : _lazyAvailabilityTypeLookup.Value;
            }
            set
            {
                _lazyAvailabilityTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool AvailabilityTypeLookupIsLoaded
        {
            get
            {
                return _lazyAvailabilityTypeLookup == null ? false : _lazyAvailabilityTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyAvailabilityTypeLookup(Lazy<Lookup> lazyAvailabilityTypeLookup)
        {
            _lazyAvailabilityTypeLookup = lazyAvailabilityTypeLookup;
        }

        #endregion

        #endregion

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("Id={0}", this.Id);
            result.Append(", ");
            result.AppendFormat("Guid={0}", this.Guid);
            result.Append(", ");
            result.AppendFormat("DateReported={0}", this.DateReported);
            result.Append(", ");
            result.AppendFormat("ProductGuid={0}", this.ProductGuid);
            result.Append(", ");
            result.AppendFormat("ShipWeekGuid={0}", this.ShipWeekGuid);
            result.Append(", ");
            result.AppendFormat("Qty={0}", this.Qty);
            result.Append(", ");
            result.AppendFormat("AvailabilityTypeLookupGuid={0}", this.AvailabilityTypeLookupGuid);
            result.Append(", ");
            result.AppendFormat("SalesSinceDateReported={0}", this.SalesSinceDateReported);

            return result.ToString();
        }

        new protected static bool GetHasIdColumn() { return true; }
        new protected static bool GetHasGuidColumn() { return true; }
        new protected static bool GetHasDateDeactivatedColumn() { return false; }
        new protected static bool GetHasCodeColumn() { return false; }
        new protected static bool GetHasNameColumn() { return false; }
        new protected static bool GetDateDeactivatedIsNullable() { return false; }
    }
}
