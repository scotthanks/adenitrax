using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Supplier : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			AddressGuid,
			Name,
			Code,
			EMail,
			PhoneAreaCode,
			Phone,
			AvailabilityInSalesUnit,
		}

		new public enum ForeignKeyFieldEnum
		{
			AddressGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Address),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid AddressGuid { get; set; }
		public string Name { get; set; }
		public string EMail { get; set; }
		public Decimal PhoneAreaCode { get; set; }
		public Decimal Phone { get; set; }
		public bool AvailabilityInSalesUnit { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "AddressGuid":
					value = GuidToString( AddressGuid);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "EMail":
					value = StringToString( EMail);
					break;
				case "PhoneAreaCode":
					value = DecimalToString( PhoneAreaCode);
					break;
				case "Phone":
					value = DecimalToString( Phone);
					break;
				case "AvailabilityInSalesUnit":
					value = BoolToString( AvailabilityInSalesUnit);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "AddressGuid":
					AddressGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "EMail":
					EMail = (string)StringFromString( value, isNullable: false);
					break;
				case "PhoneAreaCode":
					PhoneAreaCode = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "Phone":
					Phone = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "AvailabilityInSalesUnit":
					AvailabilityInSalesUnit = (bool)BoolFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "AddressGuid":
						value = AddressGuid;
						break;
					case "Name":
						value = Name;
						break;
					case "Code":
						value = Code;
						break;
					case "EMail":
						value = EMail;
						break;
					case "PhoneAreaCode":
						value = PhoneAreaCode;
						break;
					case "Phone":
						value = Phone;
						break;
					case "AvailabilityInSalesUnit":
						value = AvailabilityInSalesUnit;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "AddressGuid":
						AddressGuid = (Guid)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "EMail":
						EMail = (string)value;
						break;
					case "PhoneAreaCode":
						PhoneAreaCode = (Decimal)value;
						break;
					case "Phone":
						Phone = (Decimal)value;
						break;
					case "AvailabilityInSalesUnit":
						AvailabilityInSalesUnit = (bool)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Address Lazy Loading Properties and Methods

        private Lazy<Address> _lazyAddress;

        public Address Address
        {
            get
            {
                return _lazyAddress == null ? null : _lazyAddress.Value;
            }
            set
            {
                _lazyAddress = new Lazy<Address>(() => value);
            }
        }

        public bool AddressIsLoaded
        {
            get
            {
                return _lazyAddress == null ? false : _lazyAddress.IsValueCreated;
            }
        }

        public void SetLazyAddress(Lazy<Address> lazyAddress)
        {
            _lazyAddress = lazyAddress;
        }

		#endregion

        //#region FreightSupplierStateZone Lazy Loading Properties and Methods

        //private Lazy<List<FreightSupplierStateZone>> _lazyFreightSupplierStateZoneList;

        //public List<FreightSupplierStateZone> FreightSupplierStateZoneList
        //{
        //    get
        //    {
        //        return _lazyFreightSupplierStateZoneList == null ? null : _lazyFreightSupplierStateZoneList.Value;
        //    }
        //}

        //public bool FreightSupplierStateZoneListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyFreightSupplierStateZoneList == null ? false : _lazyFreightSupplierStateZoneList.IsValueCreated;
        //    }
        //}

        //public void SetLazyFreightSupplierStateZoneList(Lazy<List<FreightSupplierStateZone>> lazyFreightSupplierStateZoneList)
        //{
        //    _lazyFreightSupplierStateZoneList = lazyFreightSupplierStateZoneList;
        //}

        //#endregion

        //#region Ledger Lazy Loading Properties and Methods

        //private Lazy<List<Ledger>> _lazyLedgerList;

        //public List<Ledger> LedgerList
        //{
        //    get
        //    {
        //        return _lazyLedgerList == null ? null : _lazyLedgerList.Value;
        //    }
        //}

        //public bool LedgerListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyLedgerList == null ? false : _lazyLedgerList.IsValueCreated;
        //    }
        //}

        //public void SetLazyLedgerList(Lazy<List<Ledger>> lazyLedgerList)
        //{
        //    _lazyLedgerList = lazyLedgerList;
        //}

        //#endregion

        //#region ProductExclusion Lazy Loading Properties and Methods

        //private Lazy<List<ProductExclusion>> _lazyProductExclusionList;

        //public List<ProductExclusion> ProductExclusionList
        //{
        //    get
        //    {
        //        return _lazyProductExclusionList == null ? null : _lazyProductExclusionList.Value;
        //    }
        //}

        //public bool ProductExclusionListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyProductExclusionList == null ? false : _lazyProductExclusionList.IsValueCreated;
        //    }
        //}

        //public void SetLazyProductExclusionList(Lazy<List<ProductExclusion>> lazyProductExclusionList)
        //{
        //    _lazyProductExclusionList = lazyProductExclusionList;
        //}

        //#endregion

		#region ProductForm Lazy Loading Properties and Methods

        private Lazy<List<ProductForm>> _lazyProductFormList;

        public List<ProductForm> ProductFormList
        {
            get
            {
                return _lazyProductFormList == null ? null : _lazyProductFormList.Value;
            }
        }

        public bool ProductFormListIsLoaded
        {
            get
            {
                return _lazyProductFormList == null ? false : _lazyProductFormList.IsValueCreated;
            }
        }

        public void SetLazyProductFormList(Lazy<List<ProductForm>> lazyProductFormList)
        {
            _lazyProductFormList = lazyProductFormList;
        }

		#endregion

		#region Program Lazy Loading Properties and Methods

        private Lazy<List<Program>> _lazyProgramList;

        public List<Program> ProgramList
        {
            get
            {
                return _lazyProgramList == null ? null : _lazyProgramList.Value;
            }
        }

        public bool ProgramListIsLoaded
        {
            get
            {
                return _lazyProgramList == null ? false : _lazyProgramList.IsValueCreated;
            }
        }

        public void SetLazyProgramList(Lazy<List<Program>> lazyProgramList)
        {
            _lazyProgramList = lazyProgramList;
        }

		#endregion

        //#region PromoCode Lazy Loading Properties and Methods

        //private Lazy<List<PromoCode>> _lazyPromoCodeList;

        //public List<PromoCode> PromoCodeList
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? null : _lazyPromoCodeList.Value;
        //    }
        //}

        //public bool PromoCodeListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? false : _lazyPromoCodeList.IsValueCreated;
        //    }
        //}

        //public void SetLazyPromoCodeList(Lazy<List<PromoCode>> lazyPromoCodeList)
        //{
        //    _lazyPromoCodeList = lazyPromoCodeList;
        //}

        //#endregion

        //#region ReportedAvailabilityUpdate Lazy Loading Properties and Methods

        //private Lazy<List<ReportedAvailabilityUpdate>> _lazyReportedAvailabilityUpdateList;

        //public List<ReportedAvailabilityUpdate> ReportedAvailabilityUpdateList
        //{
        //    get
        //    {
        //        return _lazyReportedAvailabilityUpdateList == null ? null : _lazyReportedAvailabilityUpdateList.Value;
        //    }
        //}

        //public bool ReportedAvailabilityUpdateListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyReportedAvailabilityUpdateList == null ? false : _lazyReportedAvailabilityUpdateList.IsValueCreated;
        //    }
        //}

        //public void SetLazyReportedAvailabilityUpdateList(Lazy<List<ReportedAvailabilityUpdate>> lazyReportedAvailabilityUpdateList)
        //{
        //    _lazyReportedAvailabilityUpdateList = lazyReportedAvailabilityUpdateList;
        //}

        //#endregion

        //#region SupplierBox Lazy Loading Properties and Methods

        //private Lazy<List<SupplierBox>> _lazySupplierBoxList;

        //public List<SupplierBox> SupplierBoxList
        //{
        //    get
        //    {
        //        return _lazySupplierBoxList == null ? null : _lazySupplierBoxList.Value;
        //    }
        //}

        //public bool SupplierBoxListIsLoaded
        //{
        //    get
        //    {
        //        return _lazySupplierBoxList == null ? false : _lazySupplierBoxList.IsValueCreated;
        //    }
        //}

        //public void SetLazySupplierBoxList(Lazy<List<SupplierBox>> lazySupplierBoxList)
        //{
        //    _lazySupplierBoxList = lazySupplierBoxList;
        //}

        //#endregion

		#region SupplierOrder Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrder>> _lazySupplierOrderList;

        public List<SupplierOrder> SupplierOrderList
        {
            get
            {
                return _lazySupplierOrderList == null ? null : _lazySupplierOrderList.Value;
            }
        }

        public bool SupplierOrderListIsLoaded
        {
            get
            {
                return _lazySupplierOrderList == null ? false : _lazySupplierOrderList.IsValueCreated;
            }
        }

        public void SetLazySupplierOrderList(Lazy<List<SupplierOrder>> lazySupplierOrderList)
        {
            _lazySupplierOrderList = lazySupplierOrderList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "AddressGuid={0}",this.AddressGuid);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "EMail={0}",this.EMail);
			result.Append(", ");
			result.AppendFormat( "PhoneAreaCode={0}",this.PhoneAreaCode);
			result.Append(", ");
			result.AppendFormat( "Phone={0}",this.Phone);
			result.Append(", ");
			result.AppendFormat( "AvailabilityInSalesUnit={0}",this.AvailabilityInSalesUnit);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
