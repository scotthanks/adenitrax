using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class EventLog : TableObjectBase
    {
        public enum ColumnEnum
        {
            Id,
            Guid,
            LogTypeLookupGuid,
            ObjectTypeLookupGuid,
            ObjectGuid,
            ProcessLookupGuid,
            ProcessId,
            PersonGuid,
            EventTime,
            SyncTime,
            IntValue,
            FloatValue,
            Comment,
            XmlData,
            GuidValue,
            UserCode,
            VerbosityLookupGuid,
            PriorityLookupGuid,
            SeverityLookupGuid,
        }

        new public enum ForeignKeyFieldEnum
        {
            LogTypeLookupGuid,
            ObjectTypeLookupGuid,
            ProcessLookupGuid,
            PersonGuid,
            VerbosityLookupGuid,
            PriorityLookupGuid,
            SeverityLookupGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(Lookup),
                        typeof(Lookup),
                        typeof(Lookup),
                        typeof(Person),
                        typeof(Lookup),
                        typeof(Lookup),
                        typeof(Lookup),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Guid LogTypeLookupGuid { get; set; }
        public Guid ObjectTypeLookupGuid { get; set; }
        public Guid? ObjectGuid { get; set; }
        public Guid ProcessLookupGuid { get; set; }
        public long ProcessId { get; set; }
        public Guid? PersonGuid { get; set; }
        public DateTime EventTime { get; set; }
        public DateTime? SyncTime { get; set; }
        public long? IntValue { get; set; }
        public Double? FloatValue { get; set; }
        public string Comment { get; set; }
        public string XmlData { get; set; }
        public Guid? GuidValue { get; set; }
        public string UserCode { get; set; }
        public Guid? VerbosityLookupGuid { get; set; }
        public Guid? PriorityLookupGuid { get; set; }
        public Guid? SeverityLookupGuid { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "Id":
                    value = LongToString(Id);
                    break;
                case "Guid":
                    value = GuidToString(Guid);
                    break;
                case "LogTypeLookupGuid":
                    value = GuidToString(LogTypeLookupGuid);
                    break;
                case "ObjectTypeLookupGuid":
                    value = GuidToString(ObjectTypeLookupGuid);
                    break;
                case "ObjectGuid":
                    value = GuidToString(ObjectGuid);
                    break;
                case "ProcessLookupGuid":
                    value = GuidToString(ProcessLookupGuid);
                    break;
                case "ProcessId":
                    value = LongToString(ProcessId);
                    break;
                case "PersonGuid":
                    value = GuidToString(PersonGuid);
                    break;
                case "EventTime":
                    value = DateTimeToString(EventTime);
                    break;
                case "SyncTime":
                    value = DateTimeToString(SyncTime);
                    break;
                case "IntValue":
                    value = LongToString(IntValue);
                    break;
                case "FloatValue":
                    value = DoubleToString(FloatValue);
                    break;
                case "Comment":
                    value = StringToString(Comment);
                    break;
                case "XmlData":
                    value = StringToString(XmlData);
                    break;
                case "GuidValue":
                    value = GuidToString(GuidValue);
                    break;
                case "UserCode":
                    value = StringToString(UserCode);
                    break;
                case "VerbosityLookupGuid":
                    value = GuidToString(VerbosityLookupGuid);
                    break;
                case "PriorityLookupGuid":
                    value = GuidToString(PriorityLookupGuid);
                    break;
                case "SeverityLookupGuid":
                    value = GuidToString(SeverityLookupGuid);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "Id":
                    Id = (long)LongFromString(value, isNullable: false);
                    break;
                case "Guid":
                    Guid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "LogTypeLookupGuid":
                    LogTypeLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ObjectTypeLookupGuid":
                    ObjectTypeLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ObjectGuid":
                    ObjectGuid = GuidFromString(value, isNullable: true);
                    break;
                case "ProcessLookupGuid":
                    ProcessLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProcessId":
                    ProcessId = (long)LongFromString(value, isNullable: false);
                    break;
                case "PersonGuid":
                    PersonGuid = GuidFromString(value, isNullable: true);
                    break;
                case "EventTime":
                    EventTime = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "SyncTime":
                    SyncTime = DateTimeFromString(value, isNullable: true);
                    break;
                case "IntValue":
                    IntValue = LongFromString(value, isNullable: true);
                    break;
                case "FloatValue":
                    FloatValue = DoubleFromString(value, isNullable: true);
                    break;
                case "Comment":
                    Comment = StringFromString(value, isNullable: true);
                    break;
                case "XmlData":
                    XmlData = StringFromString(value, isNullable: true);
                    break;
                case "GuidValue":
                    GuidValue = GuidFromString(value, isNullable: true);
                    break;
                case "UserCode":
                    UserCode = StringFromString(value, isNullable: true);
                    break;
                case "VerbosityLookupGuid":
                    VerbosityLookupGuid = GuidFromString(value, isNullable: true);
                    break;
                case "PriorityLookupGuid":
                    PriorityLookupGuid = GuidFromString(value, isNullable: true);
                    break;
                case "SeverityLookupGuid":
                    SeverityLookupGuid = GuidFromString(value, isNullable: true);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "Id":
                        value = Id;
                        break;
                    case "Guid":
                        value = Guid;
                        break;
                    case "LogTypeLookupGuid":
                        value = LogTypeLookupGuid;
                        break;
                    case "ObjectTypeLookupGuid":
                        value = ObjectTypeLookupGuid;
                        break;
                    case "ObjectGuid":
                        value = ObjectGuid;
                        break;
                    case "ProcessLookupGuid":
                        value = ProcessLookupGuid;
                        break;
                    case "ProcessId":
                        value = ProcessId;
                        break;
                    case "PersonGuid":
                        value = PersonGuid;
                        break;
                    case "EventTime":
                        value = EventTime;
                        break;
                    case "SyncTime":
                        value = SyncTime;
                        break;
                    case "IntValue":
                        value = IntValue;
                        break;
                    case "FloatValue":
                        value = FloatValue;
                        break;
                    case "Comment":
                        value = Comment;
                        break;
                    case "XmlData":
                        value = XmlData;
                        break;
                    case "GuidValue":
                        value = GuidValue;
                        break;
                    case "UserCode":
                        value = UserCode;
                        break;
                    case "VerbosityLookupGuid":
                        value = VerbosityLookupGuid;
                        break;
                    case "PriorityLookupGuid":
                        value = PriorityLookupGuid;
                        break;
                    case "SeverityLookupGuid":
                        value = SeverityLookupGuid;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "Id":
                        Id = (long)value;
                        break;
                    case "Guid":
                        Guid = (Guid)value;
                        break;
                    case "LogTypeLookupGuid":
                        LogTypeLookupGuid = (Guid)value;
                        break;
                    case "ObjectTypeLookupGuid":
                        ObjectTypeLookupGuid = (Guid)value;
                        break;
                    case "ObjectGuid":
                        ObjectGuid = (Guid?)value;
                        break;
                    case "ProcessLookupGuid":
                        ProcessLookupGuid = (Guid)value;
                        break;
                    case "ProcessId":
                        ProcessId = (long)value;
                        break;
                    case "PersonGuid":
                        PersonGuid = (Guid?)value;
                        break;
                    case "EventTime":
                        EventTime = (DateTime)value;
                        break;
                    case "SyncTime":
                        SyncTime = (DateTime?)value;
                        break;
                    case "IntValue":
                        IntValue = (long?)value;
                        break;
                    case "FloatValue":
                        FloatValue = (Double?)value;
                        break;
                    case "Comment":
                        Comment = (string)value;
                        break;
                    case "XmlData":
                        XmlData = (string)value;
                        break;
                    case "GuidValue":
                        GuidValue = (Guid?)value;
                        break;
                    case "UserCode":
                        UserCode = (string)value;
                        break;
                    case "VerbosityLookupGuid":
                        VerbosityLookupGuid = (Guid?)value;
                        break;
                    case "PriorityLookupGuid":
                        PriorityLookupGuid = (Guid?)value;
                        break;
                    case "SeverityLookupGuid":
                        SeverityLookupGuid = (Guid?)value;
                        break;
                }
            }
        }

        #region Lazy Loading Logic

        #region LogTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyLogTypeLookup;

        public Lookup LogTypeLookup
        {
            get
            {
                return _lazyLogTypeLookup == null ? null : _lazyLogTypeLookup.Value;
            }
            set
            {
                _lazyLogTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool LogTypeLookupIsLoaded
        {
            get
            {
                return _lazyLogTypeLookup == null ? false : _lazyLogTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyLogTypeLookup(Lazy<Lookup> lazyLogTypeLookup)
        {
            _lazyLogTypeLookup = lazyLogTypeLookup;
        }

        #endregion

        #region ObjectTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyObjectTypeLookup;

        public Lookup ObjectTypeLookup
        {
            get
            {
                return _lazyObjectTypeLookup == null ? null : _lazyObjectTypeLookup.Value;
            }
            set
            {
                _lazyObjectTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ObjectTypeLookupIsLoaded
        {
            get
            {
                return _lazyObjectTypeLookup == null ? false : _lazyObjectTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyObjectTypeLookup(Lazy<Lookup> lazyObjectTypeLookup)
        {
            _lazyObjectTypeLookup = lazyObjectTypeLookup;
        }

        #endregion

        #region ProcessLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyProcessLookup;

        public Lookup ProcessLookup
        {
            get
            {
                return _lazyProcessLookup == null ? null : _lazyProcessLookup.Value;
            }
            set
            {
                _lazyProcessLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ProcessLookupIsLoaded
        {
            get
            {
                return _lazyProcessLookup == null ? false : _lazyProcessLookup.IsValueCreated;
            }
        }

        public void SetLazyProcessLookup(Lazy<Lookup> lazyProcessLookup)
        {
            _lazyProcessLookup = lazyProcessLookup;
        }

        #endregion

        #region Person Lazy Loading Properties and Methods

        private Lazy<Person> _lazyPerson;

        public Person Person
        {
            get
            {
                return _lazyPerson == null ? null : _lazyPerson.Value;
            }
            set
            {
                _lazyPerson = new Lazy<Person>(() => value);
            }
        }

        public bool PersonIsLoaded
        {
            get
            {
                return _lazyPerson == null ? false : _lazyPerson.IsValueCreated;
            }
        }

        public void SetLazyPerson(Lazy<Person> lazyPerson)
        {
            _lazyPerson = lazyPerson;
        }

        #endregion

        #region VerbosityLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyVerbosityLookup;

        public Lookup VerbosityLookup
        {
            get
            {
                return _lazyVerbosityLookup == null ? null : _lazyVerbosityLookup.Value;
            }
            set
            {
                _lazyVerbosityLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool VerbosityLookupIsLoaded
        {
            get
            {
                return _lazyVerbosityLookup == null ? false : _lazyVerbosityLookup.IsValueCreated;
            }
        }

        public void SetLazyVerbosityLookup(Lazy<Lookup> lazyVerbosityLookup)
        {
            _lazyVerbosityLookup = lazyVerbosityLookup;
        }

        #endregion

        #region PriorityLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyPriorityLookup;

        public Lookup PriorityLookup
        {
            get
            {
                return _lazyPriorityLookup == null ? null : _lazyPriorityLookup.Value;
            }
            set
            {
                _lazyPriorityLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool PriorityLookupIsLoaded
        {
            get
            {
                return _lazyPriorityLookup == null ? false : _lazyPriorityLookup.IsValueCreated;
            }
        }

        public void SetLazyPriorityLookup(Lazy<Lookup> lazyPriorityLookup)
        {
            _lazyPriorityLookup = lazyPriorityLookup;
        }

        #endregion

        #region SeverityLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazySeverityLookup;

        public Lookup SeverityLookup
        {
            get
            {
                return _lazySeverityLookup == null ? null : _lazySeverityLookup.Value;
            }
            set
            {
                _lazySeverityLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool SeverityLookupIsLoaded
        {
            get
            {
                return _lazySeverityLookup == null ? false : _lazySeverityLookup.IsValueCreated;
            }
        }

        public void SetLazySeverityLookup(Lazy<Lookup> lazySeverityLookup)
        {
            _lazySeverityLookup = lazySeverityLookup;
        }

        #endregion

        #endregion

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("Id={0}", this.Id);
            result.Append(", ");
            result.AppendFormat("Guid={0}", this.Guid);
            result.Append(", ");
            result.AppendFormat("LogTypeLookupGuid={0}", this.LogTypeLookupGuid);
            result.Append(", ");
            result.AppendFormat("ObjectTypeLookupGuid={0}", this.ObjectTypeLookupGuid);
            result.Append(", ");
            result.AppendFormat("ObjectGuid={0}", this.ObjectGuid);
            result.Append(", ");
            result.AppendFormat("ProcessLookupGuid={0}", this.ProcessLookupGuid);
            result.Append(", ");
            result.AppendFormat("ProcessId={0}", this.ProcessId);
            result.Append(", ");
            result.AppendFormat("PersonGuid={0}", this.PersonGuid);
            result.Append(", ");
            result.AppendFormat("EventTime={0}", this.EventTime);
            result.Append(", ");
            result.AppendFormat("SyncTime={0}", this.SyncTime);
            result.Append(", ");
            result.AppendFormat("IntValue={0}", this.IntValue);
            result.Append(", ");
            result.AppendFormat("FloatValue={0}", this.FloatValue);
            result.Append(", ");
            result.AppendFormat("Comment={0}", this.Comment);
            result.Append(", ");
            result.AppendFormat("XmlData={0}", this.XmlData);
            result.Append(", ");
            result.AppendFormat("GuidValue={0}", this.GuidValue);
            result.Append(", ");
            result.AppendFormat("UserCode={0}", this.UserCode);
            result.Append(", ");
            result.AppendFormat("VerbosityLookupGuid={0}", this.VerbosityLookupGuid);
            result.Append(", ");
            result.AppendFormat("PriorityLookupGuid={0}", this.PriorityLookupGuid);
            result.Append(", ");
            result.AppendFormat("SeverityLookupGuid={0}", this.SeverityLookupGuid);

            return result.ToString();
        }

        new protected static bool GetHasIdColumn() { return true; }
        new protected static bool GetHasGuidColumn() { return true; }
        new protected static bool GetHasDateDeactivatedColumn() { return false; }
        new protected static bool GetHasCodeColumn() { return false; }
        new protected static bool GetHasNameColumn() { return false; }
        new protected static bool GetDateDeactivatedIsNullable() { return false; }
    }
}
