using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class ShipWeek : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Week,
			Year,
			MondayDate,
			ContinuousWeekNumber,
			FiscalYear,
		}

		new public enum ForeignKeyFieldEnum
		{
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public int Week { get; set; }
		public int Year { get; set; }
		public DateTime MondayDate { get; set; }
		public int ContinuousWeekNumber { get; set; }
		public string FiscalYear { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Week":
					value = IntToString( Week);
					break;
				case "Year":
					value = IntToString( Year);
					break;
				case "MondayDate":
					value = DateTimeToString( MondayDate);
					break;
				case "ContinuousWeekNumber":
					value = IntToString( ContinuousWeekNumber);
					break;
				case "FiscalYear":
					value = StringToString( FiscalYear);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Week":
					Week = (int)IntFromString( value, isNullable: false);
					break;
				case "Year":
					Year = (int)IntFromString( value, isNullable: false);
					break;
				case "MondayDate":
					MondayDate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "ContinuousWeekNumber":
					ContinuousWeekNumber = (int)IntFromString( value, isNullable: false);
					break;
				case "FiscalYear":
					FiscalYear = StringFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Week":
						value = Week;
						break;
					case "Year":
						value = Year;
						break;
					case "MondayDate":
						value = MondayDate;
						break;
					case "ContinuousWeekNumber":
						value = ContinuousWeekNumber;
						break;
					case "FiscalYear":
						value = FiscalYear;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Week":
						Week = (int)value;
						break;
					case "Year":
						Year = (int)value;
						break;
					case "MondayDate":
						MondayDate = (DateTime)value;
						break;
					case "ContinuousWeekNumber":
						ContinuousWeekNumber = (int)value;
						break;
					case "FiscalYear":
						FiscalYear = (string)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyGrowerOrderList;

        public List<GrowerOrder> GrowerOrderList
        {
            get
            {
                return _lazyGrowerOrderList == null ? null : _lazyGrowerOrderList.Value;
            }
        }

        public bool GrowerOrderListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderList == null ? false : _lazyGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderList(Lazy<List<GrowerOrder>> lazyGrowerOrderList)
        {
            _lazyGrowerOrderList = lazyGrowerOrderList;
        }

		#endregion

        //#region ProductFormAvailabilityLinkedWeeks Lazy Loading Properties and Methods

        //private Lazy<List<ProductFormAvailabilityLinkedWeeks>> _lazyProductFormAvailabilityLinkedWeeksList;

        //public List<ProductFormAvailabilityLinkedWeeks> ProductFormAvailabilityLinkedWeeksList
        //{
        //    get
        //    {
        //        return _lazyProductFormAvailabilityLinkedWeeksList == null ? null : _lazyProductFormAvailabilityLinkedWeeksList.Value;
        //    }
        //}

        //public bool ProductFormAvailabilityLinkedWeeksListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyProductFormAvailabilityLinkedWeeksList == null ? false : _lazyProductFormAvailabilityLinkedWeeksList.IsValueCreated;
        //    }
        //}

        //public void SetLazyProductFormAvailabilityLinkedWeeksList(Lazy<List<ProductFormAvailabilityLinkedWeeks>> lazyProductFormAvailabilityLinkedWeeksList)
        //{
        //    _lazyProductFormAvailabilityLinkedWeeksList = lazyProductFormAvailabilityLinkedWeeksList;
        //}

        //#endregion

		#region ReportedAvailability Lazy Loading Properties and Methods

        private Lazy<List<ReportedAvailability>> _lazyReportedAvailabilityList;

        public List<ReportedAvailability> ReportedAvailabilityList
        {
            get
            {
                return _lazyReportedAvailabilityList == null ? null : _lazyReportedAvailabilityList.Value;
            }
        }

        public bool ReportedAvailabilityListIsLoaded
        {
            get
            {
                return _lazyReportedAvailabilityList == null ? false : _lazyReportedAvailabilityList.IsValueCreated;
            }
        }

        public void SetLazyReportedAvailabilityList(Lazy<List<ReportedAvailability>> lazyReportedAvailabilityList)
        {
            _lazyReportedAvailabilityList = lazyReportedAvailabilityList;
        }

		#endregion

		
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Week={0}",this.Week);
			result.Append(", ");
			result.AppendFormat( "Year={0}",this.Year);
			result.Append(", ");
			result.AppendFormat( "MondayDate={0}",this.MondayDate);
			result.Append(", ");
			result.AppendFormat( "ContinuousWeekNumber={0}",this.ContinuousWeekNumber);
			result.Append(", ");
			result.AppendFormat( "FiscalYear={0}",this.FiscalYear);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
