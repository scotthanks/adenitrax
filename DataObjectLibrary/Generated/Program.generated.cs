using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Program : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Code,
			Name,
			ProgramTypeGuid,
			SupplierGuid,
			ProductFormCategoryGuid,
			MinimumOrderPackingUnitQty,
			OrderMultiplePackingUnitQty,
			ShippingUnitWarningMinimumQty,
			ShippingUnitValidMinimumQty,
			AvailabilityWeekCalcMethodLookupGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			ProgramTypeGuid,
			SupplierGuid,
			ProductFormCategoryGuid,
			AvailabilityWeekCalcMethodLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(ProgramType),
						typeof(Supplier),
						typeof(ProductFormCategory),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public Guid ProgramTypeGuid { get; set; }
		public Guid SupplierGuid { get; set; }
		public Guid ProductFormCategoryGuid { get; set; }
		public int MinimumOrderPackingUnitQty { get; set; }
		public int OrderMultiplePackingUnitQty { get; set; }
		public int ShippingUnitWarningMinimumQty { get; set; }
		public int ShippingUnitValidMinimumQty { get; set; }
		public Guid? AvailabilityWeekCalcMethodLookupGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "ProgramTypeGuid":
					value = GuidToString( ProgramTypeGuid);
					break;
				case "SupplierGuid":
					value = GuidToString( SupplierGuid);
					break;
				case "ProductFormCategoryGuid":
					value = GuidToString( ProductFormCategoryGuid);
					break;
				case "MinimumOrderPackingUnitQty":
					value = IntToString( MinimumOrderPackingUnitQty);
					break;
				case "OrderMultiplePackingUnitQty":
					value = IntToString( OrderMultiplePackingUnitQty);
					break;
				case "ShippingUnitWarningMinimumQty":
					value = IntToString( ShippingUnitWarningMinimumQty);
					break;
				case "ShippingUnitValidMinimumQty":
					value = IntToString( ShippingUnitValidMinimumQty);
					break;
				case "AvailabilityWeekCalcMethodLookupGuid":
					value = GuidToString( AvailabilityWeekCalcMethodLookupGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "ProgramTypeGuid":
					ProgramTypeGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "SupplierGuid":
					SupplierGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ProductFormCategoryGuid":
					ProductFormCategoryGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "MinimumOrderPackingUnitQty":
					MinimumOrderPackingUnitQty = (int)IntFromString( value, isNullable: false);
					break;
				case "OrderMultiplePackingUnitQty":
					OrderMultiplePackingUnitQty = (int)IntFromString( value, isNullable: false);
					break;
				case "ShippingUnitWarningMinimumQty":
					ShippingUnitWarningMinimumQty = (int)IntFromString( value, isNullable: false);
					break;
				case "ShippingUnitValidMinimumQty":
					ShippingUnitValidMinimumQty = (int)IntFromString( value, isNullable: false);
					break;
				case "AvailabilityWeekCalcMethodLookupGuid":
					AvailabilityWeekCalcMethodLookupGuid = GuidFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "ProgramTypeGuid":
						value = ProgramTypeGuid;
						break;
					case "SupplierGuid":
						value = SupplierGuid;
						break;
					case "ProductFormCategoryGuid":
						value = ProductFormCategoryGuid;
						break;
					case "MinimumOrderPackingUnitQty":
						value = MinimumOrderPackingUnitQty;
						break;
					case "OrderMultiplePackingUnitQty":
						value = OrderMultiplePackingUnitQty;
						break;
					case "ShippingUnitWarningMinimumQty":
						value = ShippingUnitWarningMinimumQty;
						break;
					case "ShippingUnitValidMinimumQty":
						value = ShippingUnitValidMinimumQty;
						break;
					case "AvailabilityWeekCalcMethodLookupGuid":
						value = AvailabilityWeekCalcMethodLookupGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "ProgramTypeGuid":
						ProgramTypeGuid = (Guid)value;
						break;
					case "SupplierGuid":
						SupplierGuid = (Guid)value;
						break;
					case "ProductFormCategoryGuid":
						ProductFormCategoryGuid = (Guid)value;
						break;
					case "MinimumOrderPackingUnitQty":
						MinimumOrderPackingUnitQty = (int)value;
						break;
					case "OrderMultiplePackingUnitQty":
						OrderMultiplePackingUnitQty = (int)value;
						break;
					case "ShippingUnitWarningMinimumQty":
						ShippingUnitWarningMinimumQty = (int)value;
						break;
					case "ShippingUnitValidMinimumQty":
						ShippingUnitValidMinimumQty = (int)value;
						break;
					case "AvailabilityWeekCalcMethodLookupGuid":
						AvailabilityWeekCalcMethodLookupGuid = (Guid?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region ProgramType Lazy Loading Properties and Methods

        private Lazy<ProgramType> _lazyProgramType;

        public ProgramType ProgramType
        {
            get
            {
                return _lazyProgramType == null ? null : _lazyProgramType.Value;
            }
            set
            {
                _lazyProgramType = new Lazy<ProgramType>(() => value);
            }
        }

        public bool ProgramTypeIsLoaded
        {
            get
            {
                return _lazyProgramType == null ? false : _lazyProgramType.IsValueCreated;
            }
        }

        public void SetLazyProgramType(Lazy<ProgramType> lazyProgramType)
        {
            _lazyProgramType = lazyProgramType;
        }

		#endregion

		#region Supplier Lazy Loading Properties and Methods

        private Lazy<Supplier> _lazySupplier;

        public Supplier Supplier
        {
            get
            {
                return _lazySupplier == null ? null : _lazySupplier.Value;
            }
            set
            {
                _lazySupplier = new Lazy<Supplier>(() => value);
            }
        }

        public bool SupplierIsLoaded
        {
            get
            {
                return _lazySupplier == null ? false : _lazySupplier.IsValueCreated;
            }
        }

        public void SetLazySupplier(Lazy<Supplier> lazySupplier)
        {
            _lazySupplier = lazySupplier;
        }

		#endregion

		#region ProductFormCategory Lazy Loading Properties and Methods

        private Lazy<ProductFormCategory> _lazyProductFormCategory;

        public ProductFormCategory ProductFormCategory
        {
            get
            {
                return _lazyProductFormCategory == null ? null : _lazyProductFormCategory.Value;
            }
            set
            {
                _lazyProductFormCategory = new Lazy<ProductFormCategory>(() => value);
            }
        }

        public bool ProductFormCategoryIsLoaded
        {
            get
            {
                return _lazyProductFormCategory == null ? false : _lazyProductFormCategory.IsValueCreated;
            }
        }

        public void SetLazyProductFormCategory(Lazy<ProductFormCategory> lazyProductFormCategory)
        {
            _lazyProductFormCategory = lazyProductFormCategory;
        }

		#endregion

		#region AvailabilityWeekCalcMethodLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyAvailabilityWeekCalcMethodLookup;

        public Lookup AvailabilityWeekCalcMethodLookup
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodLookup == null ? null : _lazyAvailabilityWeekCalcMethodLookup.Value;
            }
            set
            {
                _lazyAvailabilityWeekCalcMethodLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool AvailabilityWeekCalcMethodLookupIsLoaded
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodLookup == null ? false : _lazyAvailabilityWeekCalcMethodLookup.IsValueCreated;
            }
        }

        public void SetLazyAvailabilityWeekCalcMethodLookup(Lazy<Lookup> lazyAvailabilityWeekCalcMethodLookup)
        {
            _lazyAvailabilityWeekCalcMethodLookup = lazyAvailabilityWeekCalcMethodLookup;
        }

		#endregion

        //#region FreightProgramZoneBoxCost Lazy Loading Properties and Methods

        //private Lazy<List<FreightProgramZoneBoxCost>> _lazyFreightProgramZoneBoxCostList;

        //public List<FreightProgramZoneBoxCost> FreightProgramZoneBoxCostList
        //{
        //    get
        //    {
        //        return _lazyFreightProgramZoneBoxCostList == null ? null : _lazyFreightProgramZoneBoxCostList.Value;
        //    }
        //}

        //public bool FreightProgramZoneBoxCostListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyFreightProgramZoneBoxCostList == null ? false : _lazyFreightProgramZoneBoxCostList.IsValueCreated;
        //    }
        //}

        //public void SetLazyFreightProgramZoneBoxCostList(Lazy<List<FreightProgramZoneBoxCost>> lazyFreightProgramZoneBoxCostList)
        //{
        //    _lazyFreightProgramZoneBoxCostList = lazyFreightProgramZoneBoxCostList;
        //}

        //#endregion

		#region Product Lazy Loading Properties and Methods

        private Lazy<List<Product>> _lazyProductList;

        public List<Product> ProductList
        {
            get
            {
                return _lazyProductList == null ? null : _lazyProductList.Value;
            }
        }

        public bool ProductListIsLoaded
        {
            get
            {
                return _lazyProductList == null ? false : _lazyProductList.IsValueCreated;
            }
        }

        public void SetLazyProductList(Lazy<List<Product>> lazyProductList)
        {
            _lazyProductList = lazyProductList;
        }

		#endregion

        //#region ProgramCountry Lazy Loading Properties and Methods

        //private Lazy<List<ProgramCountry>> _lazyProgramCountryList;

        //public List<ProgramCountry> ProgramCountryList
        //{
        //    get
        //    {
        //        return _lazyProgramCountryList == null ? null : _lazyProgramCountryList.Value;
        //    }
        //}

        //public bool ProgramCountryListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyProgramCountryList == null ? false : _lazyProgramCountryList.IsValueCreated;
        //    }
        //}

        //public void SetLazyProgramCountryList(Lazy<List<ProgramCountry>> lazyProgramCountryList)
        //{
        //    _lazyProgramCountryList = lazyProgramCountryList;
        //}

        //#endregion

		#region ProgramSeason Lazy Loading Properties and Methods

        private Lazy<List<ProgramSeason>> _lazyProgramSeasonList;

        public List<ProgramSeason> ProgramSeasonList
        {
            get
            {
                return _lazyProgramSeasonList == null ? null : _lazyProgramSeasonList.Value;
            }
        }

        public bool ProgramSeasonListIsLoaded
        {
            get
            {
                return _lazyProgramSeasonList == null ? false : _lazyProgramSeasonList.IsValueCreated;
            }
        }

        public void SetLazyProgramSeasonList(Lazy<List<ProgramSeason>> lazyProgramSeasonList)
        {
            _lazyProgramSeasonList = lazyProgramSeasonList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "ProgramTypeGuid={0}",this.ProgramTypeGuid);
			result.Append(", ");
			result.AppendFormat( "SupplierGuid={0}",this.SupplierGuid);
			result.Append(", ");
			result.AppendFormat( "ProductFormCategoryGuid={0}",this.ProductFormCategoryGuid);
			result.Append(", ");
			result.AppendFormat( "MinimumOrderPackingUnitQty={0}",this.MinimumOrderPackingUnitQty);
			result.Append(", ");
			result.AppendFormat( "OrderMultiplePackingUnitQty={0}",this.OrderMultiplePackingUnitQty);
			result.Append(", ");
			result.AppendFormat( "ShippingUnitWarningMinimumQty={0}",this.ShippingUnitWarningMinimumQty);
			result.Append(", ");
			result.AppendFormat( "ShippingUnitValidMinimumQty={0}",this.ShippingUnitValidMinimumQty);
			result.Append(", ");
			result.AppendFormat( "AvailabilityWeekCalcMethodLookupGuid={0}",this.AvailabilityWeekCalcMethodLookupGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
