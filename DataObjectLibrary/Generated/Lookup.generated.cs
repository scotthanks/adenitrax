using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Lookup : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			ParentLookupGuid,
			DateDeactivated,
			Code,
			Name,
			SortSequence,
			Path,
			SequenceChildren,
			CategoryLookupGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			ParentLookupGuid,
			CategoryLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Lookup),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid? ParentLookupGuid { get; set; }
		public string Name { get; set; }
		public Double SortSequence { get; set; }
		public string Path { get; set; }
		public bool SequenceChildren { get; set; }
		public Guid? CategoryLookupGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "ParentLookupGuid":
					value = GuidToString( ParentLookupGuid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "SortSequence":
					value = DoubleToString( SortSequence);
					break;
				case "Path":
					value = StringToString( Path);
					break;
				case "SequenceChildren":
					value = BoolToString( SequenceChildren);
					break;
				case "CategoryLookupGuid":
					value = GuidToString( CategoryLookupGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ParentLookupGuid":
					ParentLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "SortSequence":
					SortSequence = (Double)DoubleFromString( value, isNullable: false);
					break;
				case "Path":
					Path = StringFromString( value, isNullable: true);
					break;
				case "SequenceChildren":
					SequenceChildren = (bool)BoolFromString( value, isNullable: false);
					break;
				case "CategoryLookupGuid":
					CategoryLookupGuid = GuidFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "ParentLookupGuid":
						value = ParentLookupGuid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "SortSequence":
						value = SortSequence;
						break;
					case "Path":
						value = Path;
						break;
					case "SequenceChildren":
						value = SequenceChildren;
						break;
					case "CategoryLookupGuid":
						value = CategoryLookupGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "ParentLookupGuid":
						ParentLookupGuid = (Guid?)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "SortSequence":
						SortSequence = (Double)value;
						break;
					case "Path":
						Path = (string)value;
						break;
					case "SequenceChildren":
						SequenceChildren = (bool)value;
						break;
					case "CategoryLookupGuid":
						CategoryLookupGuid = (Guid?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region ParentLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyParentLookup;

        public Lookup ParentLookup
        {
            get
            {
                return _lazyParentLookup == null ? null : _lazyParentLookup.Value;
            }
            set
            {
                _lazyParentLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ParentLookupIsLoaded
        {
            get
            {
                return _lazyParentLookup == null ? false : _lazyParentLookup.IsValueCreated;
            }
        }

        public void SetLazyParentLookup(Lazy<Lookup> lazyParentLookup)
        {
            _lazyParentLookup = lazyParentLookup;
        }

		#endregion

		#region CategoryLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyCategoryLookup;

        public Lookup CategoryLookup
        {
            get
            {
                return _lazyCategoryLookup == null ? null : _lazyCategoryLookup.Value;
            }
            set
            {
                _lazyCategoryLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool CategoryLookupIsLoaded
        {
            get
            {
                return _lazyCategoryLookup == null ? false : _lazyCategoryLookup.IsValueCreated;
            }
        }

        public void SetLazyCategoryLookup(Lazy<Lookup> lazyCategoryLookup)
        {
            _lazyCategoryLookup = lazyCategoryLookup;
        }

		#endregion

        //#region ClaimReasonClaim Lazy Loading Properties and Methods

        //private Lazy<List<Claim>> _lazyClaimReasonClaimList;

        //public List<Claim> ClaimReasonClaimList
        //{
        //    get
        //    {
        //        return _lazyClaimReasonClaimList == null ? null : _lazyClaimReasonClaimList.Value;
        //    }
        //}

        //public bool ClaimReasonClaimListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyClaimReasonClaimList == null ? false : _lazyClaimReasonClaimList.IsValueCreated;
        //    }
        //}

        //public void SetLazyClaimReasonClaimList(Lazy<List<Claim>> lazyClaimReasonClaimList)
        //{
        //    _lazyClaimReasonClaimList = lazyClaimReasonClaimList;
        //}

        //#endregion

        //#region ClaimStatusClaim Lazy Loading Properties and Methods

        //private Lazy<List<Claim>> _lazyClaimStatusClaimList;

        //public List<Claim> ClaimStatusClaimList
        //{
        //    get
        //    {
        //        return _lazyClaimStatusClaimList == null ? null : _lazyClaimStatusClaimList.Value;
        //    }
        //}

        //public bool ClaimStatusClaimListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyClaimStatusClaimList == null ? false : _lazyClaimStatusClaimList.IsValueCreated;
        //    }
        //}

        //public void SetLazyClaimStatusClaimList(Lazy<List<Claim>> lazyClaimStatusClaimList)
        //{
        //    _lazyClaimStatusClaimList = lazyClaimStatusClaimList;
        //}

        //#endregion

        #region LogTypeEventLog Lazy Loading Properties and Methods

        private Lazy<List<EventLog>> _lazyLogTypeEventLogList;

        public List<EventLog> LogTypeEventLogList
        {
            get
            {
                return _lazyLogTypeEventLogList == null ? null : _lazyLogTypeEventLogList.Value;
            }
        }

        public bool LogTypeEventLogListIsLoaded
        {
            get
            {
                return _lazyLogTypeEventLogList == null ? false : _lazyLogTypeEventLogList.IsValueCreated;
            }
        }

        public void SetLazyLogTypeEventLogList(Lazy<List<EventLog>> lazyLogTypeEventLogList)
        {
            _lazyLogTypeEventLogList = lazyLogTypeEventLogList;
        }

        #endregion

        #region ObjectTypeEventLog Lazy Loading Properties and Methods

        private Lazy<List<EventLog>> _lazyObjectTypeEventLogList;

        public List<EventLog> ObjectTypeEventLogList
        {
            get
            {
                return _lazyObjectTypeEventLogList == null ? null : _lazyObjectTypeEventLogList.Value;
            }
        }

        public bool ObjectTypeEventLogListIsLoaded
        {
            get
            {
                return _lazyObjectTypeEventLogList == null ? false : _lazyObjectTypeEventLogList.IsValueCreated;
            }
        }

        public void SetLazyObjectTypeEventLogList(Lazy<List<EventLog>> lazyObjectTypeEventLogList)
        {
            _lazyObjectTypeEventLogList = lazyObjectTypeEventLogList;
        }

        #endregion

        #region ProcessEventLog Lazy Loading Properties and Methods

        private Lazy<List<EventLog>> _lazyProcessEventLogList;

        public List<EventLog> ProcessEventLogList
        {
            get
            {
                return _lazyProcessEventLogList == null ? null : _lazyProcessEventLogList.Value;
            }
        }

        public bool ProcessEventLogListIsLoaded
        {
            get
            {
                return _lazyProcessEventLogList == null ? false : _lazyProcessEventLogList.IsValueCreated;
            }
        }

        public void SetLazyProcessEventLogList(Lazy<List<EventLog>> lazyProcessEventLogList)
        {
            _lazyProcessEventLogList = lazyProcessEventLogList;
        }

        #endregion

        #region VerbosityEventLog Lazy Loading Properties and Methods

        private Lazy<List<EventLog>> _lazyVerbosityEventLogList;

        public List<EventLog> VerbosityEventLogList
        {
            get
            {
                return _lazyVerbosityEventLogList == null ? null : _lazyVerbosityEventLogList.Value;
            }
        }

        public bool VerbosityEventLogListIsLoaded
        {
            get
            {
                return _lazyVerbosityEventLogList == null ? false : _lazyVerbosityEventLogList.IsValueCreated;
            }
        }

        public void SetLazyVerbosityEventLogList(Lazy<List<EventLog>> lazyVerbosityEventLogList)
        {
            _lazyVerbosityEventLogList = lazyVerbosityEventLogList;
        }

        #endregion

        #region PriorityEventLog Lazy Loading Properties and Methods

        private Lazy<List<EventLog>> _lazyPriorityEventLogList;

        public List<EventLog> PriorityEventLogList
        {
            get
            {
                return _lazyPriorityEventLogList == null ? null : _lazyPriorityEventLogList.Value;
            }
        }

        public bool PriorityEventLogListIsLoaded
        {
            get
            {
                return _lazyPriorityEventLogList == null ? false : _lazyPriorityEventLogList.IsValueCreated;
            }
        }

        public void SetLazyPriorityEventLogList(Lazy<List<EventLog>> lazyPriorityEventLogList)
        {
            _lazyPriorityEventLogList = lazyPriorityEventLogList;
        }

        #endregion

        #region SeverityEventLog Lazy Loading Properties and Methods

        private Lazy<List<EventLog>> _lazySeverityEventLogList;

        public List<EventLog> SeverityEventLogList
        {
            get
            {
                return _lazySeverityEventLogList == null ? null : _lazySeverityEventLogList.Value;
            }
        }

        public bool SeverityEventLogListIsLoaded
        {
            get
            {
                return _lazySeverityEventLogList == null ? false : _lazySeverityEventLogList.IsValueCreated;
            }
        }

        public void SetLazySeverityEventLogList(Lazy<List<EventLog>> lazySeverityEventLogList)
        {
            _lazySeverityEventLogList = lazySeverityEventLogList;
        }

        #endregion

        //#region ZoneFreightProgramZoneBoxCost Lazy Loading Properties and Methods

        //private Lazy<List<FreightProgramZoneBoxCost>> _lazyZoneFreightProgramZoneBoxCostList;

        //public List<FreightProgramZoneBoxCost> ZoneFreightProgramZoneBoxCostList
        //{
        //    get
        //    {
        //        return _lazyZoneFreightProgramZoneBoxCostList == null ? null : _lazyZoneFreightProgramZoneBoxCostList.Value;
        //    }
        //}

        //public bool ZoneFreightProgramZoneBoxCostListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyZoneFreightProgramZoneBoxCostList == null ? false : _lazyZoneFreightProgramZoneBoxCostList.IsValueCreated;
        //    }
        //}

        //public void SetLazyZoneFreightProgramZoneBoxCostList(Lazy<List<FreightProgramZoneBoxCost>> lazyZoneFreightProgramZoneBoxCostList)
        //{
        //    _lazyZoneFreightProgramZoneBoxCostList = lazyZoneFreightProgramZoneBoxCostList;
        //}

        //#endregion

        //#region ZoneFreightSupplierStateZone Lazy Loading Properties and Methods

        //private Lazy<List<FreightSupplierStateZone>> _lazyZoneFreightSupplierStateZoneList;

        //public List<FreightSupplierStateZone> ZoneFreightSupplierStateZoneList
        //{
        //    get
        //    {
        //        return _lazyZoneFreightSupplierStateZoneList == null ? null : _lazyZoneFreightSupplierStateZoneList.Value;
        //    }
        //}

        //public bool ZoneFreightSupplierStateZoneListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyZoneFreightSupplierStateZoneList == null ? false : _lazyZoneFreightSupplierStateZoneList.IsValueCreated;
        //    }
        //}

        //public void SetLazyZoneFreightSupplierStateZoneList(Lazy<List<FreightSupplierStateZone>> lazyZoneFreightSupplierStateZoneList)
        //{
        //    _lazyZoneFreightSupplierStateZoneList = lazyZoneFreightSupplierStateZoneList;
        //}

        //#endregion

		#region GrowerTypeGrower Lazy Loading Properties and Methods

        private Lazy<List<Grower>> _lazyGrowerTypeGrowerList;

        public List<Grower> GrowerTypeGrowerList
        {
            get
            {
                return _lazyGrowerTypeGrowerList == null ? null : _lazyGrowerTypeGrowerList.Value;
            }
        }

        public bool GrowerTypeGrowerListIsLoaded
        {
            get
            {
                return _lazyGrowerTypeGrowerList == null ? false : _lazyGrowerTypeGrowerList.IsValueCreated;
            }
        }

        public void SetLazyGrowerTypeGrowerList(Lazy<List<Grower>> lazyGrowerTypeGrowerList)
        {
            _lazyGrowerTypeGrowerList = lazyGrowerTypeGrowerList;
        }

		#endregion

		#region DefaultPaymentTypeGrower Lazy Loading Properties and Methods

        private Lazy<List<Grower>> _lazyDefaultPaymentTypeGrowerList;

        public List<Grower> DefaultPaymentTypeGrowerList
        {
            get
            {
                return _lazyDefaultPaymentTypeGrowerList == null ? null : _lazyDefaultPaymentTypeGrowerList.Value;
            }
        }

        public bool DefaultPaymentTypeGrowerListIsLoaded
        {
            get
            {
                return _lazyDefaultPaymentTypeGrowerList == null ? false : _lazyDefaultPaymentTypeGrowerList.IsValueCreated;
            }
        }

        public void SetLazyDefaultPaymentTypeGrowerList(Lazy<List<Grower>> lazyDefaultPaymentTypeGrowerList)
        {
            _lazyDefaultPaymentTypeGrowerList = lazyDefaultPaymentTypeGrowerList;
        }

		#endregion

		#region RequestedLineOfCreditStatusGrower Lazy Loading Properties and Methods

        private Lazy<List<Grower>> _lazyRequestedLineOfCreditStatusGrowerList;

        public List<Grower> RequestedLineOfCreditStatusGrowerList
        {
            get
            {
                return _lazyRequestedLineOfCreditStatusGrowerList == null ? null : _lazyRequestedLineOfCreditStatusGrowerList.Value;
            }
        }

        public bool RequestedLineOfCreditStatusGrowerListIsLoaded
        {
            get
            {
                return _lazyRequestedLineOfCreditStatusGrowerList == null ? false : _lazyRequestedLineOfCreditStatusGrowerList.IsValueCreated;
            }
        }

        public void SetLazyRequestedLineOfCreditStatusGrowerList(Lazy<List<Grower>> lazyRequestedLineOfCreditStatusGrowerList)
        {
            _lazyRequestedLineOfCreditStatusGrowerList = lazyRequestedLineOfCreditStatusGrowerList;
        }

		#endregion

		#region GrowerSizeGrower Lazy Loading Properties and Methods

        private Lazy<List<Grower>> _lazyGrowerSizeGrowerList;

        public List<Grower> GrowerSizeGrowerList
        {
            get
            {
                return _lazyGrowerSizeGrowerList == null ? null : _lazyGrowerSizeGrowerList.Value;
            }
        }

        public bool GrowerSizeGrowerListIsLoaded
        {
            get
            {
                return _lazyGrowerSizeGrowerList == null ? false : _lazyGrowerSizeGrowerList.IsValueCreated;
            }
        }

        public void SetLazyGrowerSizeGrowerList(Lazy<List<Grower>> lazyGrowerSizeGrowerList)
        {
            _lazyGrowerSizeGrowerList = lazyGrowerSizeGrowerList;
        }

		#endregion

		#region OrderTypeGrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyOrderTypeGrowerOrderList;

        public List<GrowerOrder> OrderTypeGrowerOrderList
        {
            get
            {
                return _lazyOrderTypeGrowerOrderList == null ? null : _lazyOrderTypeGrowerOrderList.Value;
            }
        }

        public bool OrderTypeGrowerOrderListIsLoaded
        {
            get
            {
                return _lazyOrderTypeGrowerOrderList == null ? false : _lazyOrderTypeGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyOrderTypeGrowerOrderList(Lazy<List<GrowerOrder>> lazyOrderTypeGrowerOrderList)
        {
            _lazyOrderTypeGrowerOrderList = lazyOrderTypeGrowerOrderList;
        }

		#endregion

		#region PaymentTypeGrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyPaymentTypeGrowerOrderList;

        public List<GrowerOrder> PaymentTypeGrowerOrderList
        {
            get
            {
                return _lazyPaymentTypeGrowerOrderList == null ? null : _lazyPaymentTypeGrowerOrderList.Value;
            }
        }

        public bool PaymentTypeGrowerOrderListIsLoaded
        {
            get
            {
                return _lazyPaymentTypeGrowerOrderList == null ? false : _lazyPaymentTypeGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyPaymentTypeGrowerOrderList(Lazy<List<GrowerOrder>> lazyPaymentTypeGrowerOrderList)
        {
            _lazyPaymentTypeGrowerOrderList = lazyPaymentTypeGrowerOrderList;
        }

		#endregion

		#region GrowerOrderStatusGrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyGrowerOrderStatusGrowerOrderList;

        public List<GrowerOrder> GrowerOrderStatusGrowerOrderList
        {
            get
            {
                return _lazyGrowerOrderStatusGrowerOrderList == null ? null : _lazyGrowerOrderStatusGrowerOrderList.Value;
            }
        }

        public bool GrowerOrderStatusGrowerOrderListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderStatusGrowerOrderList == null ? false : _lazyGrowerOrderStatusGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderStatusGrowerOrderList(Lazy<List<GrowerOrder>> lazyGrowerOrderStatusGrowerOrderList)
        {
            _lazyGrowerOrderStatusGrowerOrderList = lazyGrowerOrderStatusGrowerOrderList;
        }

		#endregion

		#region FeeTypeGrowerOrderFee Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrderFee>> _lazyFeeTypeGrowerOrderFeeList;

        public List<GrowerOrderFee> FeeTypeGrowerOrderFeeList
        {
            get
            {
                return _lazyFeeTypeGrowerOrderFeeList == null ? null : _lazyFeeTypeGrowerOrderFeeList.Value;
            }
        }

        public bool FeeTypeGrowerOrderFeeListIsLoaded
        {
            get
            {
                return _lazyFeeTypeGrowerOrderFeeList == null ? false : _lazyFeeTypeGrowerOrderFeeList.IsValueCreated;
            }
        }

        public void SetLazyFeeTypeGrowerOrderFeeList(Lazy<List<GrowerOrderFee>> lazyFeeTypeGrowerOrderFeeList)
        {
            _lazyFeeTypeGrowerOrderFeeList = lazyFeeTypeGrowerOrderFeeList;
        }

		#endregion

		#region FeeStatXGrowerOrderFee Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrderFee>> _lazyFeeStatXGrowerOrderFeeList;

        public List<GrowerOrderFee> FeeStatXGrowerOrderFeeList
        {
            get
            {
                return _lazyFeeStatXGrowerOrderFeeList == null ? null : _lazyFeeStatXGrowerOrderFeeList.Value;
            }
        }

        public bool FeeStatXGrowerOrderFeeListIsLoaded
        {
            get
            {
                return _lazyFeeStatXGrowerOrderFeeList == null ? false : _lazyFeeStatXGrowerOrderFeeList.IsValueCreated;
            }
        }

        public void SetLazyFeeStatXGrowerOrderFeeList(Lazy<List<GrowerOrderFee>> lazyFeeStatXGrowerOrderFeeList)
        {
            _lazyFeeStatXGrowerOrderFeeList = lazyFeeStatXGrowerOrderFeeList;
        }

		#endregion

		#region LogTypeGrowerOrderHistory Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrderHistory>> _lazyLogTypeGrowerOrderHistoryList;

        public List<GrowerOrderHistory> LogTypeGrowerOrderHistoryList
        {
            get
            {
                return _lazyLogTypeGrowerOrderHistoryList == null ? null : _lazyLogTypeGrowerOrderHistoryList.Value;
            }
        }

        public bool LogTypeGrowerOrderHistoryListIsLoaded
        {
            get
            {
                return _lazyLogTypeGrowerOrderHistoryList == null ? false : _lazyLogTypeGrowerOrderHistoryList.IsValueCreated;
            }
        }

        public void SetLazyLogTypeGrowerOrderHistoryList(Lazy<List<GrowerOrderHistory>> lazyLogTypeGrowerOrderHistoryList)
        {
            _lazyLogTypeGrowerOrderHistoryList = lazyLogTypeGrowerOrderHistoryList;
        }

		#endregion

		#region FileExtensionImageFile Lazy Loading Properties and Methods

        private Lazy<List<ImageFile>> _lazyFileExtensionImageFileList;

        public List<ImageFile> FileExtensionImageFileList
        {
            get
            {
                return _lazyFileExtensionImageFileList == null ? null : _lazyFileExtensionImageFileList.Value;
            }
        }

        public bool FileExtensionImageFileListIsLoaded
        {
            get
            {
                return _lazyFileExtensionImageFileList == null ? false : _lazyFileExtensionImageFileList.IsValueCreated;
            }
        }

        public void SetLazyFileExtensionImageFileList(Lazy<List<ImageFile>> lazyFileExtensionImageFileList)
        {
            _lazyFileExtensionImageFileList = lazyFileExtensionImageFileList;
        }

		#endregion

		#region ImageFileTypeImageFile Lazy Loading Properties and Methods

        private Lazy<List<ImageFile>> _lazyImageFileTypeImageFileList;

        public List<ImageFile> ImageFileTypeImageFileList
        {
            get
            {
                return _lazyImageFileTypeImageFileList == null ? null : _lazyImageFileTypeImageFileList.Value;
            }
        }

        public bool ImageFileTypeImageFileListIsLoaded
        {
            get
            {
                return _lazyImageFileTypeImageFileList == null ? false : _lazyImageFileTypeImageFileList.IsValueCreated;
            }
        }

        public void SetLazyImageFileTypeImageFileList(Lazy<List<ImageFile>> lazyImageFileTypeImageFileList)
        {
            _lazyImageFileTypeImageFileList = lazyImageFileTypeImageFileList;
        }

		#endregion

		#region ImageSetTypeImageSet Lazy Loading Properties and Methods

        private Lazy<List<ImageSet>> _lazyImageSetTypeImageSetList;

        public List<ImageSet> ImageSetTypeImageSetList
        {
            get
            {
                return _lazyImageSetTypeImageSetList == null ? null : _lazyImageSetTypeImageSetList.Value;
            }
        }

        public bool ImageSetTypeImageSetListIsLoaded
        {
            get
            {
                return _lazyImageSetTypeImageSetList == null ? false : _lazyImageSetTypeImageSetList.IsValueCreated;
            }
        }

        public void SetLazyImageSetTypeImageSetList(Lazy<List<ImageSet>> lazyImageSetTypeImageSetList)
        {
            _lazyImageSetTypeImageSetList = lazyImageSetTypeImageSetList;
        }

		#endregion

        //#region EntryTypeLedger Lazy Loading Properties and Methods

        //private Lazy<List<Ledger>> _lazyEntryTypeLedgerList;

        //public List<Ledger> EntryTypeLedgerList
        //{
        //    get
        //    {
        //        return _lazyEntryTypeLedgerList == null ? null : _lazyEntryTypeLedgerList.Value;
        //    }
        //}

        //public bool EntryTypeLedgerListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyEntryTypeLedgerList == null ? false : _lazyEntryTypeLedgerList.IsValueCreated;
        //    }
        //}

        //public void SetLazyEntryTypeLedgerList(Lazy<List<Ledger>> lazyEntryTypeLedgerList)
        //{
        //    _lazyEntryTypeLedgerList = lazyEntryTypeLedgerList;
        //}

        //#endregion

        //#region GLAccountLedger Lazy Loading Properties and Methods

        //private Lazy<List<Ledger>> _lazyGLAccountLedgerList;

        //public List<Ledger> GLAccountLedgerList
        //{
        //    get
        //    {
        //        return _lazyGLAccountLedgerList == null ? null : _lazyGLAccountLedgerList.Value;
        //    }
        //}

        //public bool GLAccountLedgerListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyGLAccountLedgerList == null ? false : _lazyGLAccountLedgerList.IsValueCreated;
        //    }
        //}

        //public void SetLazyGLAccountLedgerList(Lazy<List<Ledger>> lazyGLAccountLedgerList)
        //{
        //    _lazyGLAccountLedgerList = lazyGLAccountLedgerList;
        //}

        //#endregion

		#region ParentLookup Lazy Loading Properties and Methods

        private Lazy<List<Lookup>> _lazyParentLookupList;

        public List<Lookup> ParentLookupList
        {
            get
            {
                return _lazyParentLookupList == null ? null : _lazyParentLookupList.Value;
            }
        }

        public bool ParentLookupListIsLoaded
        {
            get
            {
                return _lazyParentLookupList == null ? false : _lazyParentLookupList.IsValueCreated;
            }
        }

        public void SetLazyParentLookupList(Lazy<List<Lookup>> lazyParentLookupList)
        {
            _lazyParentLookupList = lazyParentLookupList;
        }

		#endregion

		#region CategoryLookup Lazy Loading Properties and Methods

        private Lazy<List<Lookup>> _lazyCategoryLookupList;

        public List<Lookup> CategoryLookupList
        {
            get
            {
                return _lazyCategoryLookupList == null ? null : _lazyCategoryLookupList.Value;
            }
        }

        public bool CategoryLookupListIsLoaded
        {
            get
            {
                return _lazyCategoryLookupList == null ? false : _lazyCategoryLookupList.IsValueCreated;
            }
        }

        public void SetLazyCategoryLookupList(Lazy<List<Lookup>> lazyCategoryLookupList)
        {
            _lazyCategoryLookupList = lazyCategoryLookupList;
        }

		#endregion

		#region OrderLineStatusOrderLine Lazy Loading Properties and Methods

        private Lazy<List<OrderLine>> _lazyOrderLineStatusOrderLineList;

        public List<OrderLine> OrderLineStatusOrderLineList
        {
            get
            {
                return _lazyOrderLineStatusOrderLineList == null ? null : _lazyOrderLineStatusOrderLineList.Value;
            }
        }

        public bool OrderLineStatusOrderLineListIsLoaded
        {
            get
            {
                return _lazyOrderLineStatusOrderLineList == null ? false : _lazyOrderLineStatusOrderLineList.IsValueCreated;
            }
        }

        public void SetLazyOrderLineStatusOrderLineList(Lazy<List<OrderLine>> lazyOrderLineStatusOrderLineList)
        {
            _lazyOrderLineStatusOrderLineList = lazyOrderLineStatusOrderLineList;
        }

		#endregion

		#region ChangeTypeOrderLine Lazy Loading Properties and Methods

        private Lazy<List<OrderLine>> _lazyChangeTypeOrderLineList;

        public List<OrderLine> ChangeTypeOrderLineList
        {
            get
            {
                return _lazyChangeTypeOrderLineList == null ? null : _lazyChangeTypeOrderLineList.Value;
            }
        }

        public bool ChangeTypeOrderLineListIsLoaded
        {
            get
            {
                return _lazyChangeTypeOrderLineList == null ? false : _lazyChangeTypeOrderLineList.IsValueCreated;
            }
        }

        public void SetLazyChangeTypeOrderLineList(Lazy<List<OrderLine>> lazyChangeTypeOrderLineList)
        {
            _lazyChangeTypeOrderLineList = lazyChangeTypeOrderLineList;
        }

		#endregion

		#region PersonTypePerson Lazy Loading Properties and Methods

        private Lazy<List<Person>> _lazyPersonTypePersonList;

        public List<Person> PersonTypePersonList
        {
            get
            {
                return _lazyPersonTypePersonList == null ? null : _lazyPersonTypePersonList.Value;
            }
        }

        public bool PersonTypePersonListIsLoaded
        {
            get
            {
                return _lazyPersonTypePersonList == null ? false : _lazyPersonTypePersonList.IsValueCreated;
            }
        }

        public void SetLazyPersonTypePersonList(Lazy<List<Person>> lazyPersonTypePersonList)
        {
            _lazyPersonTypePersonList = lazyPersonTypePersonList;
        }

		#endregion

		#region AvailabilityWeekCalcMethodProductFormCategory Lazy Loading Properties and Methods

        private Lazy<List<ProductFormCategory>> _lazyAvailabilityWeekCalcMethodProductFormCategoryList;

        public List<ProductFormCategory> AvailabilityWeekCalcMethodProductFormCategoryList
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodProductFormCategoryList == null ? null : _lazyAvailabilityWeekCalcMethodProductFormCategoryList.Value;
            }
        }

        public bool AvailabilityWeekCalcMethodProductFormCategoryListIsLoaded
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodProductFormCategoryList == null ? false : _lazyAvailabilityWeekCalcMethodProductFormCategoryList.IsValueCreated;
            }
        }

        public void SetLazyAvailabilityWeekCalcMethodProductFormCategoryList(Lazy<List<ProductFormCategory>> lazyAvailabilityWeekCalcMethodProductFormCategoryList)
        {
            _lazyAvailabilityWeekCalcMethodProductFormCategoryList = lazyAvailabilityWeekCalcMethodProductFormCategoryList;
        }

		#endregion

		#region AvailabilityWeekCalcMethodProgram Lazy Loading Properties and Methods

        private Lazy<List<Program>> _lazyAvailabilityWeekCalcMethodProgramList;

        public List<Program> AvailabilityWeekCalcMethodProgramList
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodProgramList == null ? null : _lazyAvailabilityWeekCalcMethodProgramList.Value;
            }
        }

        public bool AvailabilityWeekCalcMethodProgramListIsLoaded
        {
            get
            {
                return _lazyAvailabilityWeekCalcMethodProgramList == null ? false : _lazyAvailabilityWeekCalcMethodProgramList.IsValueCreated;
            }
        }

        public void SetLazyAvailabilityWeekCalcMethodProgramList(Lazy<List<Program>> lazyAvailabilityWeekCalcMethodProgramList)
        {
            _lazyAvailabilityWeekCalcMethodProgramList = lazyAvailabilityWeekCalcMethodProgramList;
        }

		#endregion

		#region AvailabilityTypeReportedAvailability Lazy Loading Properties and Methods

        private Lazy<List<ReportedAvailability>> _lazyAvailabilityTypeReportedAvailabilityList;

        public List<ReportedAvailability> AvailabilityTypeReportedAvailabilityList
        {
            get
            {
                return _lazyAvailabilityTypeReportedAvailabilityList == null ? null : _lazyAvailabilityTypeReportedAvailabilityList.Value;
            }
        }

        public bool AvailabilityTypeReportedAvailabilityListIsLoaded
        {
            get
            {
                return _lazyAvailabilityTypeReportedAvailabilityList == null ? false : _lazyAvailabilityTypeReportedAvailabilityList.IsValueCreated;
            }
        }

        public void SetLazyAvailabilityTypeReportedAvailabilityList(Lazy<List<ReportedAvailability>> lazyAvailabilityTypeReportedAvailabilityList)
        {
            _lazyAvailabilityTypeReportedAvailabilityList = lazyAvailabilityTypeReportedAvailabilityList;
        }

		#endregion

        //#region AvailabilityTypeReportedAvailabilityUpdate Lazy Loading Properties and Methods

        //private Lazy<List<ReportedAvailabilityUpdate>> _lazyAvailabilityTypeReportedAvailabilityUpdateList;

        //public List<ReportedAvailabilityUpdate> AvailabilityTypeReportedAvailabilityUpdateList
        //{
        //    get
        //    {
        //        return _lazyAvailabilityTypeReportedAvailabilityUpdateList == null ? null : _lazyAvailabilityTypeReportedAvailabilityUpdateList.Value;
        //    }
        //}

        //public bool AvailabilityTypeReportedAvailabilityUpdateListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyAvailabilityTypeReportedAvailabilityUpdateList == null ? false : _lazyAvailabilityTypeReportedAvailabilityUpdateList.IsValueCreated;
        //    }
        //}

        //public void SetLazyAvailabilityTypeReportedAvailabilityUpdateList(Lazy<List<ReportedAvailabilityUpdate>> lazyAvailabilityTypeReportedAvailabilityUpdateList)
        //{
        //    _lazyAvailabilityTypeReportedAvailabilityUpdateList = lazyAvailabilityTypeReportedAvailabilityUpdateList;
        //}

        //#endregion

		#region SupplierOrderStatusSupplierOrder Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrder>> _lazySupplierOrderStatusSupplierOrderList;

        public List<SupplierOrder> SupplierOrderStatusSupplierOrderList
        {
            get
            {
                return _lazySupplierOrderStatusSupplierOrderList == null ? null : _lazySupplierOrderStatusSupplierOrderList.Value;
            }
        }

        public bool SupplierOrderStatusSupplierOrderListIsLoaded
        {
            get
            {
                return _lazySupplierOrderStatusSupplierOrderList == null ? false : _lazySupplierOrderStatusSupplierOrderList.IsValueCreated;
            }
        }

        public void SetLazySupplierOrderStatusSupplierOrderList(Lazy<List<SupplierOrder>> lazySupplierOrderStatusSupplierOrderList)
        {
            _lazySupplierOrderStatusSupplierOrderList = lazySupplierOrderStatusSupplierOrderList;
        }

		#endregion

		#region TagRatioSupplierOrder Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrder>> _lazyTagRatioSupplierOrderList;

        public List<SupplierOrder> TagRatioSupplierOrderList
        {
            get
            {
                return _lazyTagRatioSupplierOrderList == null ? null : _lazyTagRatioSupplierOrderList.Value;
            }
        }

        public bool TagRatioSupplierOrderListIsLoaded
        {
            get
            {
                return _lazyTagRatioSupplierOrderList == null ? false : _lazyTagRatioSupplierOrderList.IsValueCreated;
            }
        }

        public void SetLazyTagRatioSupplierOrderList(Lazy<List<SupplierOrder>> lazyTagRatioSupplierOrderList)
        {
            _lazyTagRatioSupplierOrderList = lazyTagRatioSupplierOrderList;
        }

		#endregion

		#region ShipMethodSupplierOrder Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrder>> _lazyShipMethodSupplierOrderList;

        public List<SupplierOrder> ShipMethodSupplierOrderList
        {
            get
            {
                return _lazyShipMethodSupplierOrderList == null ? null : _lazyShipMethodSupplierOrderList.Value;
            }
        }

        public bool ShipMethodSupplierOrderListIsLoaded
        {
            get
            {
                return _lazyShipMethodSupplierOrderList == null ? false : _lazyShipMethodSupplierOrderList.IsValueCreated;
            }
        }

        public void SetLazyShipMethodSupplierOrderList(Lazy<List<SupplierOrder>> lazyShipMethodSupplierOrderList)
        {
            _lazyShipMethodSupplierOrderList = lazyShipMethodSupplierOrderList;
        }

		#endregion

		#region FeeTypeSupplierOrderFee Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrderFee>> _lazyFeeTypeSupplierOrderFeeList;

        public List<SupplierOrderFee> FeeTypeSupplierOrderFeeList
        {
            get
            {
                return _lazyFeeTypeSupplierOrderFeeList == null ? null : _lazyFeeTypeSupplierOrderFeeList.Value;
            }
        }

        public bool FeeTypeSupplierOrderFeeListIsLoaded
        {
            get
            {
                return _lazyFeeTypeSupplierOrderFeeList == null ? false : _lazyFeeTypeSupplierOrderFeeList.IsValueCreated;
            }
        }

        public void SetLazyFeeTypeSupplierOrderFeeList(Lazy<List<SupplierOrderFee>> lazyFeeTypeSupplierOrderFeeList)
        {
            _lazyFeeTypeSupplierOrderFeeList = lazyFeeTypeSupplierOrderFeeList;
        }

		#endregion

		#region FeeStatXSupplierOrderFee Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrderFee>> _lazyFeeStatXSupplierOrderFeeList;

        public List<SupplierOrderFee> FeeStatXSupplierOrderFeeList
        {
            get
            {
                return _lazyFeeStatXSupplierOrderFeeList == null ? null : _lazyFeeStatXSupplierOrderFeeList.Value;
            }
        }

        public bool FeeStatXSupplierOrderFeeListIsLoaded
        {
            get
            {
                return _lazyFeeStatXSupplierOrderFeeList == null ? false : _lazyFeeStatXSupplierOrderFeeList.IsValueCreated;
            }
        }

        public void SetLazyFeeStatXSupplierOrderFeeList(Lazy<List<SupplierOrderFee>> lazyFeeStatXSupplierOrderFeeList)
        {
            _lazyFeeStatXSupplierOrderFeeList = lazyFeeStatXSupplierOrderFeeList;
        }

		#endregion

        //#region PredicateTriple Lazy Loading Properties and Methods

        //private Lazy<List<Triple>> _lazyPredicateTripleList;

        //public List<Triple> PredicateTripleList
        //{
        //    get
        //    {
        //        return _lazyPredicateTripleList == null ? null : _lazyPredicateTripleList.Value;
        //    }
        //}

        //public bool PredicateTripleListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyPredicateTripleList == null ? false : _lazyPredicateTripleList.IsValueCreated;
        //    }
        //}

        //public void SetLazyPredicateTripleList(Lazy<List<Triple>> lazyPredicateTripleList)
        //{
        //    _lazyPredicateTripleList = lazyPredicateTripleList;
        //}

        //#endregion

		#region ColorVariety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyColorVarietyList;

        public List<Variety> ColorVarietyList
        {
            get
            {
                return _lazyColorVarietyList == null ? null : _lazyColorVarietyList.Value;
            }
        }

        public bool ColorVarietyListIsLoaded
        {
            get
            {
                return _lazyColorVarietyList == null ? false : _lazyColorVarietyList.IsValueCreated;
            }
        }

        public void SetLazyColorVarietyList(Lazy<List<Variety>> lazyColorVarietyList)
        {
            _lazyColorVarietyList = lazyColorVarietyList;
        }

		#endregion

		#region HabitVariety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyHabitVarietyList;

        public List<Variety> HabitVarietyList
        {
            get
            {
                return _lazyHabitVarietyList == null ? null : _lazyHabitVarietyList.Value;
            }
        }

        public bool HabitVarietyListIsLoaded
        {
            get
            {
                return _lazyHabitVarietyList == null ? false : _lazyHabitVarietyList.IsValueCreated;
            }
        }

        public void SetLazyHabitVarietyList(Lazy<List<Variety>> lazyHabitVarietyList)
        {
            _lazyHabitVarietyList = lazyHabitVarietyList;
        }

		#endregion

		#region VigorVariety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyVigorVarietyList;

        public List<Variety> VigorVarietyList
        {
            get
            {
                return _lazyVigorVarietyList == null ? null : _lazyVigorVarietyList.Value;
            }
        }

        public bool VigorVarietyListIsLoaded
        {
            get
            {
                return _lazyVigorVarietyList == null ? false : _lazyVigorVarietyList.IsValueCreated;
            }
        }

        public void SetLazyVigorVarietyList(Lazy<List<Variety>> lazyVigorVarietyList)
        {
            _lazyVigorVarietyList = lazyVigorVarietyList;
        }

		#endregion

		#region TimingVariety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyTimingVarietyList;

        public List<Variety> TimingVarietyList
        {
            get
            {
                return _lazyTimingVarietyList == null ? null : _lazyTimingVarietyList.Value;
            }
        }

        public bool TimingVarietyListIsLoaded
        {
            get
            {
                return _lazyTimingVarietyList == null ? false : _lazyTimingVarietyList.IsValueCreated;
            }
        }

        public void SetLazyTimingVarietyList(Lazy<List<Variety>> lazyTimingVarietyList)
        {
            _lazyTimingVarietyList = lazyTimingVarietyList;
        }

		#endregion

		#region ZoneVariety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyZoneVarietyList;

        public List<Variety> ZoneVarietyList
        {
            get
            {
                return _lazyZoneVarietyList == null ? null : _lazyZoneVarietyList.Value;
            }
        }

        public bool ZoneVarietyListIsLoaded
        {
            get
            {
                return _lazyZoneVarietyList == null ? false : _lazyZoneVarietyList.IsValueCreated;
            }
        }

        public void SetLazyZoneVarietyList(Lazy<List<Variety>> lazyZoneVarietyList)
        {
            _lazyZoneVarietyList = lazyZoneVarietyList;
        }

		#endregion

		#region BrandVariety Lazy Loading Properties and Methods

        private Lazy<List<Variety>> _lazyBrandVarietyList;

        public List<Variety> BrandVarietyList
        {
            get
            {
                return _lazyBrandVarietyList == null ? null : _lazyBrandVarietyList.Value;
            }
        }

        public bool BrandVarietyListIsLoaded
        {
            get
            {
                return _lazyBrandVarietyList == null ? false : _lazyBrandVarietyList.IsValueCreated;
            }
        }

        public void SetLazyBrandVarietyList(Lazy<List<Variety>> lazyBrandVarietyList)
        {
            _lazyBrandVarietyList = lazyBrandVarietyList;
        }

		#endregion

		#region VolumeLevelTypeVolumeLevel Lazy Loading Properties and Methods

        private Lazy<List<VolumeLevel>> _lazyVolumeLevelTypeVolumeLevelList;

        public List<VolumeLevel> VolumeLevelTypeVolumeLevelList
        {
            get
            {
                return _lazyVolumeLevelTypeVolumeLevelList == null ? null : _lazyVolumeLevelTypeVolumeLevelList.Value;
            }
        }

        public bool VolumeLevelTypeVolumeLevelListIsLoaded
        {
            get
            {
                return _lazyVolumeLevelTypeVolumeLevelList == null ? false : _lazyVolumeLevelTypeVolumeLevelList.IsValueCreated;
            }
        }

        public void SetLazyVolumeLevelTypeVolumeLevelList(Lazy<List<VolumeLevel>> lazyVolumeLevelTypeVolumeLevelList)
        {
            _lazyVolumeLevelTypeVolumeLevelList = lazyVolumeLevelTypeVolumeLevelList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "ParentLookupGuid={0}",this.ParentLookupGuid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "SortSequence={0}",this.SortSequence);
			result.Append(", ");
			result.AppendFormat( "Path={0}",this.Path);
			result.Append(", ");
			result.AppendFormat( "SequenceChildren={0}",this.SequenceChildren);
			result.Append(", ");
			result.AppendFormat( "CategoryLookupGuid={0}",this.CategoryLookupGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
