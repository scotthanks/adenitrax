using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class State : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			Code,
			Name,
			CountryGuid,
		}

		new public enum ForeignKeyFieldEnum
		{
			CountryGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Country),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public Guid CountryGuid { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "CountryGuid":
					value = GuidToString( CountryGuid);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "CountryGuid":
					CountryGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "CountryGuid":
						value = CountryGuid;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "CountryGuid":
						CountryGuid = (Guid)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Country Lazy Loading Properties and Methods

        private Lazy<Country> _lazyCountry;

        public Country Country
        {
            get
            {
                return _lazyCountry == null ? null : _lazyCountry.Value;
            }
            set
            {
                _lazyCountry = new Lazy<Country>(() => value);
            }
        }

        public bool CountryIsLoaded
        {
            get
            {
                return _lazyCountry == null ? false : _lazyCountry.IsValueCreated;
            }
        }

        public void SetLazyCountry(Lazy<Country> lazyCountry)
        {
            _lazyCountry = lazyCountry;
        }

		#endregion

		#region Address Lazy Loading Properties and Methods

        private Lazy<List<Address>> _lazyAddressList;

        public List<Address> AddressList
        {
            get
            {
                return _lazyAddressList == null ? null : _lazyAddressList.Value;
            }
        }

        public bool AddressListIsLoaded
        {
            get
            {
                return _lazyAddressList == null ? false : _lazyAddressList.IsValueCreated;
            }
        }

        public void SetLazyAddressList(Lazy<List<Address>> lazyAddressList)
        {
            _lazyAddressList = lazyAddressList;
        }

		#endregion

        //#region FreightSupplierStateZone Lazy Loading Properties and Methods

        //private Lazy<List<FreightSupplierStateZone>> _lazyFreightSupplierStateZoneList;

        //public List<FreightSupplierStateZone> FreightSupplierStateZoneList
        //{
        //    get
        //    {
        //        return _lazyFreightSupplierStateZoneList == null ? null : _lazyFreightSupplierStateZoneList.Value;
        //    }
        //}

        //public bool FreightSupplierStateZoneListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyFreightSupplierStateZoneList == null ? false : _lazyFreightSupplierStateZoneList.IsValueCreated;
        //    }
        //}

        //public void SetLazyFreightSupplierStateZoneList(Lazy<List<FreightSupplierStateZone>> lazyFreightSupplierStateZoneList)
        //{
        //    _lazyFreightSupplierStateZoneList = lazyFreightSupplierStateZoneList;
        //}

        //#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "CountryGuid={0}",this.CountryGuid);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return false; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return false; }
	}
}
