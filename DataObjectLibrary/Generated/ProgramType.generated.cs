using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class ProgramType : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Code,
			Name,
			TagMultiple,
			ImageSetGuid,
			DefaultPeakWeekNumber,
			DefaultSeasonEndWeekNumber,
		}

		new public enum ForeignKeyFieldEnum
		{
			ImageSetGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(ImageSet),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public int TagMultiple { get; set; }
		public Guid? ImageSetGuid { get; set; }
		public int? DefaultPeakWeekNumber { get; set; }
		public int? DefaultSeasonEndWeekNumber { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "TagMultiple":
					value = IntToString( TagMultiple);
					break;
				case "ImageSetGuid":
					value = GuidToString( ImageSetGuid);
					break;
				case "DefaultPeakWeekNumber":
					value = IntToString( DefaultPeakWeekNumber);
					break;
				case "DefaultSeasonEndWeekNumber":
					value = IntToString( DefaultSeasonEndWeekNumber);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "TagMultiple":
					TagMultiple = (int)IntFromString( value, isNullable: false);
					break;
				case "ImageSetGuid":
					ImageSetGuid = GuidFromString( value, isNullable: true);
					break;
				case "DefaultPeakWeekNumber":
					DefaultPeakWeekNumber = IntFromString( value, isNullable: true);
					break;
				case "DefaultSeasonEndWeekNumber":
					DefaultSeasonEndWeekNumber = IntFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "TagMultiple":
						value = TagMultiple;
						break;
					case "ImageSetGuid":
						value = ImageSetGuid;
						break;
					case "DefaultPeakWeekNumber":
						value = DefaultPeakWeekNumber;
						break;
					case "DefaultSeasonEndWeekNumber":
						value = DefaultSeasonEndWeekNumber;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "TagMultiple":
						TagMultiple = (int)value;
						break;
					case "ImageSetGuid":
						ImageSetGuid = (Guid?)value;
						break;
					case "DefaultPeakWeekNumber":
						DefaultPeakWeekNumber = (int?)value;
						break;
					case "DefaultSeasonEndWeekNumber":
						DefaultSeasonEndWeekNumber = (int?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region ImageSet Lazy Loading Properties and Methods

        private Lazy<ImageSet> _lazyImageSet;

        public ImageSet ImageSet
        {
            get
            {
                return _lazyImageSet == null ? null : _lazyImageSet.Value;
            }
            set
            {
                _lazyImageSet = new Lazy<ImageSet>(() => value);
            }
        }

        public bool ImageSetIsLoaded
        {
            get
            {
                return _lazyImageSet == null ? false : _lazyImageSet.IsValueCreated;
            }
        }

        public void SetLazyImageSet(Lazy<ImageSet> lazyImageSet)
        {
            _lazyImageSet = lazyImageSet;
        }

		#endregion

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyGrowerOrderList;

        public List<GrowerOrder> GrowerOrderList
        {
            get
            {
                return _lazyGrowerOrderList == null ? null : _lazyGrowerOrderList.Value;
            }
        }

        public bool GrowerOrderListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderList == null ? false : _lazyGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderList(Lazy<List<GrowerOrder>> lazyGrowerOrderList)
        {
            _lazyGrowerOrderList = lazyGrowerOrderList;
        }

		#endregion

		#region Program Lazy Loading Properties and Methods

        private Lazy<List<Program>> _lazyProgramList;

        public List<Program> ProgramList
        {
            get
            {
                return _lazyProgramList == null ? null : _lazyProgramList.Value;
            }
        }

        public bool ProgramListIsLoaded
        {
            get
            {
                return _lazyProgramList == null ? false : _lazyProgramList.IsValueCreated;
            }
        }

        public void SetLazyProgramList(Lazy<List<Program>> lazyProgramList)
        {
            _lazyProgramList = lazyProgramList;
        }

		#endregion

        //#region PromoCode Lazy Loading Properties and Methods

        //private Lazy<List<PromoCode>> _lazyPromoCodeList;

        //public List<PromoCode> PromoCodeList
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? null : _lazyPromoCodeList.Value;
        //    }
        //}

        //public bool PromoCodeListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyPromoCodeList == null ? false : _lazyPromoCodeList.IsValueCreated;
        //    }
        //}

        //public void SetLazyPromoCodeList(Lazy<List<PromoCode>> lazyPromoCodeList)
        //{
        //    _lazyPromoCodeList = lazyPromoCodeList;
        //}

        //#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "TagMultiple={0}",this.TagMultiple);
			result.Append(", ");
			result.AppendFormat( "ImageSetGuid={0}",this.ImageSetGuid);
			result.Append(", ");
			result.AppendFormat( "DefaultPeakWeekNumber={0}",this.DefaultPeakWeekNumber);
			result.Append(", ");
			result.AppendFormat( "DefaultSeasonEndWeekNumber={0}",this.DefaultSeasonEndWeekNumber);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
