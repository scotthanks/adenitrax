using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Grower : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Name,
			Code,
			IsActivated,
			AddressGuid,
			AllowSubstitutions,
			CreditLimit,
			CreditLimitTemporaryIncrease,
			CreditLimitTemporaryIncreaseExpiration,
			EMail,
			PhoneAreaCode,
			Phone,
			GrowerTypeLookupGuid,
			DefaultPaymentTypeLookupGuid,
			SalesTaxNumber,
			CreditAppOnFile,
			RequestedLineOfCredit,
			RequestedLineOfCreditStatusLookupGuid,
			GrowerSizeLookupGuid,
			AdditionalAccountingEmail,
			AdditionalOrderEmail,
            SellerGuid,
            OnlySpeciesIncluded,
            UseDummenPriceMethod,
		}

		new public enum ForeignKeyFieldEnum
		{
			AddressGuid,
			GrowerTypeLookupGuid,
			DefaultPaymentTypeLookupGuid,
			RequestedLineOfCreditStatusLookupGuid,
			GrowerSizeLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Address),
						typeof(Lookup),
						typeof(Lookup),
						typeof(Lookup),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public bool IsActivated { get; set; }
		public Guid AddressGuid { get; set; }
		public bool AllowSubstitutions { get; set; }
		public int CreditLimit { get; set; }
		public int CreditLimitTemporaryIncrease { get; set; }
		public DateTime? CreditLimitTemporaryIncreaseExpiration { get; set; }
		public string EMail { get; set; }
		public Decimal PhoneAreaCode { get; set; }
		public Decimal Phone { get; set; }
		public Guid GrowerTypeLookupGuid { get; set; }
		public Guid DefaultPaymentTypeLookupGuid { get; set; }
		public string SalesTaxNumber { get; set; }
		public bool CreditAppOnFile { get; set; }
		public int RequestedLineOfCredit { get; set; }
		public Guid? RequestedLineOfCreditStatusLookupGuid { get; set; }
		public Guid GrowerSizeLookupGuid { get; set; }
		public string AdditionalAccountingEmail { get; set; }
		public string AdditionalOrderEmail { get; set; }
        public Guid SellerGuid { get; set; }
        public bool OnlySpeciesIncluded { get; set; }
        public bool UseDummenPriceMethod { get; set; }
     
		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "IsActivated":
					value = BoolToString( IsActivated);
					break;
				case "AddressGuid":
					value = GuidToString( AddressGuid);
					break;
				case "AllowSubstitutions":
					value = BoolToString( AllowSubstitutions);
					break;
				case "CreditLimit":
					value = IntToString( CreditLimit);
					break;
				case "CreditLimitTemporaryIncrease":
					value = IntToString( CreditLimitTemporaryIncrease);
					break;
				case "CreditLimitTemporaryIncreaseExpiration":
					value = DateTimeToString( CreditLimitTemporaryIncreaseExpiration);
					break;
				case "EMail":
					value = StringToString( EMail);
					break;
				case "PhoneAreaCode":
					value = DecimalToString( PhoneAreaCode);
					break;
				case "Phone":
					value = DecimalToString( Phone);
					break;
				case "GrowerTypeLookupGuid":
					value = GuidToString( GrowerTypeLookupGuid);
					break;
				case "DefaultPaymentTypeLookupGuid":
					value = GuidToString( DefaultPaymentTypeLookupGuid);
					break;
				case "SalesTaxNumber":
					value = StringToString( SalesTaxNumber);
					break;
				case "CreditAppOnFile":
					value = BoolToString( CreditAppOnFile);
					break;
				case "RequestedLineOfCredit":
					value = IntToString( RequestedLineOfCredit);
					break;
				case "RequestedLineOfCreditStatusLookupGuid":
					value = GuidToString( RequestedLineOfCreditStatusLookupGuid);
					break;
				case "GrowerSizeLookupGuid":
					value = GuidToString( GrowerSizeLookupGuid);
					break;
				case "AdditionalAccountingEmail":
					value = StringToString( AdditionalAccountingEmail);
					break;
				case "AdditionalOrderEmail":
					value = StringToString( AdditionalOrderEmail);
					break;
                case "SellerGuid":
                    value = GuidToString(SellerGuid);
                    break;
                case "OnlySpeciesIncluded":
                    value = BoolToString(UseDummenPriceMethod);
                    break;
                case "UseDummenPriceMethod":
                    value = BoolToString(UseDummenPriceMethod);
                    break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "Code":
					Code = StringFromString( value, isNullable: true);
					break;
				case "IsActivated":
					IsActivated = (bool)BoolFromString( value, isNullable: false);
					break;
				case "AddressGuid":
					AddressGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "AllowSubstitutions":
					AllowSubstitutions = (bool)BoolFromString( value, isNullable: false);
					break;
				case "CreditLimit":
					CreditLimit = (int)IntFromString( value, isNullable: false);
					break;
				case "CreditLimitTemporaryIncrease":
					CreditLimitTemporaryIncrease = (int)IntFromString( value, isNullable: false);
					break;
				case "CreditLimitTemporaryIncreaseExpiration":
					CreditLimitTemporaryIncreaseExpiration = DateTimeFromString( value, isNullable: true);
					break;
				case "EMail":
					EMail = (string)StringFromString( value, isNullable: false);
					break;
				case "PhoneAreaCode":
					PhoneAreaCode = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "Phone":
					Phone = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "GrowerTypeLookupGuid":
					GrowerTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DefaultPaymentTypeLookupGuid":
					DefaultPaymentTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "SalesTaxNumber":
					SalesTaxNumber = (string)StringFromString( value, isNullable: false);
					break;
				case "CreditAppOnFile":
					CreditAppOnFile = (bool)BoolFromString( value, isNullable: false);
					break;
				case "RequestedLineOfCredit":
					RequestedLineOfCredit = (int)IntFromString( value, isNullable: false);
					break;
				case "RequestedLineOfCreditStatusLookupGuid":
					RequestedLineOfCreditStatusLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "GrowerSizeLookupGuid":
					GrowerSizeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "AdditionalAccountingEmail":
					AdditionalAccountingEmail = (string)StringFromString( value, isNullable: false);
					break;
				case "AdditionalOrderEmail":
					AdditionalOrderEmail = (string)StringFromString( value, isNullable: false);
					break;
                case "SellerGuid":
                    SellerGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "OnlySpeciesIncluded":
                    OnlySpeciesIncluded = (bool)BoolFromString(value, isNullable: false);
                    break;
                case "UseDummenPriceMethod":
                    UseDummenPriceMethod = (bool)BoolFromString(value, isNullable: false);
                    break;
                default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Name":
						value = Name;
						break;
					case "Code":
						value = Code;
						break;
					case "IsActivated":
						value = IsActivated;
						break;
					case "AddressGuid":
						value = AddressGuid;
						break;
					case "AllowSubstitutions":
						value = AllowSubstitutions;
						break;
					case "CreditLimit":
						value = CreditLimit;
						break;
					case "CreditLimitTemporaryIncrease":
						value = CreditLimitTemporaryIncrease;
						break;
					case "CreditLimitTemporaryIncreaseExpiration":
						value = CreditLimitTemporaryIncreaseExpiration;
						break;
					case "EMail":
						value = EMail;
						break;
					case "PhoneAreaCode":
						value = PhoneAreaCode;
						break;
					case "Phone":
						value = Phone;
						break;
					case "GrowerTypeLookupGuid":
						value = GrowerTypeLookupGuid;
						break;
					case "DefaultPaymentTypeLookupGuid":
						value = DefaultPaymentTypeLookupGuid;
						break;
					case "SalesTaxNumber":
						value = SalesTaxNumber;
						break;
					case "CreditAppOnFile":
						value = CreditAppOnFile;
						break;
					case "RequestedLineOfCredit":
						value = RequestedLineOfCredit;
						break;
					case "RequestedLineOfCreditStatusLookupGuid":
						value = RequestedLineOfCreditStatusLookupGuid;
						break;
					case "GrowerSizeLookupGuid":
						value = GrowerSizeLookupGuid;
						break;
					case "AdditionalAccountingEmail":
						value = AdditionalAccountingEmail;
						break;
					case "AdditionalOrderEmail":
						value = AdditionalOrderEmail;
						break;
                    case "SellerGuid":
                        value = SellerGuid;
                        break;
                    case "OnlySpeciesIncluded":
                        value = OnlySpeciesIncluded;
                        break;

                    case "UseDummenPriceMethod":
                        value = UseDummenPriceMethod;
                        break;

                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "IsActivated":
						IsActivated = (bool)value;
						break;
					case "AddressGuid":
						AddressGuid = (Guid)value;
						break;
					case "AllowSubstitutions":
						AllowSubstitutions = (bool)value;
						break;
					case "CreditLimit":
						CreditLimit = (int)value;
						break;
					case "CreditLimitTemporaryIncrease":
						CreditLimitTemporaryIncrease = (int)value;
						break;
					case "CreditLimitTemporaryIncreaseExpiration":
						CreditLimitTemporaryIncreaseExpiration = (DateTime?)value;
						break;
					case "EMail":
						EMail = (string)value;
						break;
					case "PhoneAreaCode":
						PhoneAreaCode = (Decimal)value;
						break;
					case "Phone":
						Phone = (Decimal)value;
						break;
					case "GrowerTypeLookupGuid":
						GrowerTypeLookupGuid = (Guid)value;
						break;
					case "DefaultPaymentTypeLookupGuid":
						DefaultPaymentTypeLookupGuid = (Guid)value;
						break;
					case "SalesTaxNumber":
						SalesTaxNumber = (string)value;
						break;
					case "CreditAppOnFile":
						CreditAppOnFile = (bool)value;
						break;
					case "RequestedLineOfCredit":
						RequestedLineOfCredit = (int)value;
						break;
					case "RequestedLineOfCreditStatusLookupGuid":
						RequestedLineOfCreditStatusLookupGuid = (Guid?)value;
						break;
					case "GrowerSizeLookupGuid":
						GrowerSizeLookupGuid = (Guid)value;
						break;
					case "AdditionalAccountingEmail":
						AdditionalAccountingEmail = (string)value;
						break;
                    case "AdditionalOrderEmail":
                        AdditionalOrderEmail = (string)value;
                        break;
                    case "SellerGuid":
                        SellerGuid = (Guid)value;
                        break;
                    case "OnlySpeciesIncluded":
                        OnlySpeciesIncluded = (bool)value;
                        break;
                    case "UseDummenPriceMethod":
                        UseDummenPriceMethod = (bool)value;
                        break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Address Lazy Loading Properties and Methods

        private Lazy<Address> _lazyAddress;

        public Address Address
        {
            get
            {
                return _lazyAddress == null ? null : _lazyAddress.Value;
            }
            set
            {
                _lazyAddress = new Lazy<Address>(() => value);
            }
        }

        public bool AddressIsLoaded
        {
            get
            {
                return _lazyAddress == null ? false : _lazyAddress.IsValueCreated;
            }
        }

        public void SetLazyAddress(Lazy<Address> lazyAddress)
        {
            _lazyAddress = lazyAddress;
        }

		#endregion

		#region GrowerTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyGrowerTypeLookup;

        public Lookup GrowerTypeLookup
        {
            get
            {
                return _lazyGrowerTypeLookup == null ? null : _lazyGrowerTypeLookup.Value;
            }
            set
            {
                _lazyGrowerTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool GrowerTypeLookupIsLoaded
        {
            get
            {
                return _lazyGrowerTypeLookup == null ? false : _lazyGrowerTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyGrowerTypeLookup(Lazy<Lookup> lazyGrowerTypeLookup)
        {
            _lazyGrowerTypeLookup = lazyGrowerTypeLookup;
        }

		#endregion

		#region DefaultPaymentTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyDefaultPaymentTypeLookup;

        public Lookup DefaultPaymentTypeLookup
        {
            get
            {
                return _lazyDefaultPaymentTypeLookup == null ? null : _lazyDefaultPaymentTypeLookup.Value;
            }
            set
            {
                _lazyDefaultPaymentTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool DefaultPaymentTypeLookupIsLoaded
        {
            get
            {
                return _lazyDefaultPaymentTypeLookup == null ? false : _lazyDefaultPaymentTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyDefaultPaymentTypeLookup(Lazy<Lookup> lazyDefaultPaymentTypeLookup)
        {
            _lazyDefaultPaymentTypeLookup = lazyDefaultPaymentTypeLookup;
        }

		#endregion

		#region RequestedLineOfCreditStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyRequestedLineOfCreditStatusLookup;

        public Lookup RequestedLineOfCreditStatusLookup
        {
            get
            {
                return _lazyRequestedLineOfCreditStatusLookup == null ? null : _lazyRequestedLineOfCreditStatusLookup.Value;
            }
            set
            {
                _lazyRequestedLineOfCreditStatusLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool RequestedLineOfCreditStatusLookupIsLoaded
        {
            get
            {
                return _lazyRequestedLineOfCreditStatusLookup == null ? false : _lazyRequestedLineOfCreditStatusLookup.IsValueCreated;
            }
        }

        public void SetLazyRequestedLineOfCreditStatusLookup(Lazy<Lookup> lazyRequestedLineOfCreditStatusLookup)
        {
            _lazyRequestedLineOfCreditStatusLookup = lazyRequestedLineOfCreditStatusLookup;
        }

		#endregion

		#region GrowerSizeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyGrowerSizeLookup;

        public Lookup GrowerSizeLookup
        {
            get
            {
                return _lazyGrowerSizeLookup == null ? null : _lazyGrowerSizeLookup.Value;
            }
            set
            {
                _lazyGrowerSizeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool GrowerSizeLookupIsLoaded
        {
            get
            {
                return _lazyGrowerSizeLookup == null ? false : _lazyGrowerSizeLookup.IsValueCreated;
            }
        }

        public void SetLazyGrowerSizeLookup(Lazy<Lookup> lazyGrowerSizeLookup)
        {
            _lazyGrowerSizeLookup = lazyGrowerSizeLookup;
        }

		#endregion

		#region GrowerAddress Lazy Loading Properties and Methods

        private Lazy<List<GrowerAddress>> _lazyGrowerAddressList;

        public List<GrowerAddress> GrowerAddressList
        {
            get
            {
                return _lazyGrowerAddressList == null ? null : _lazyGrowerAddressList.Value;
            }
        }

        public bool GrowerAddressListIsLoaded
        {
            get
            {
                return _lazyGrowerAddressList == null ? false : _lazyGrowerAddressList.IsValueCreated;
            }
        }

        public void SetLazyGrowerAddressList(Lazy<List<GrowerAddress>> lazyGrowerAddressList)
        {
            _lazyGrowerAddressList = lazyGrowerAddressList;
        }

		#endregion

		#region GrowerCreditCard Lazy Loading Properties and Methods

        private Lazy<List<GrowerCreditCard>> _lazyGrowerCreditCardList;

        public List<GrowerCreditCard> GrowerCreditCardList
        {
            get
            {
                return _lazyGrowerCreditCardList == null ? null : _lazyGrowerCreditCardList.Value;
            }
        }

        public bool GrowerCreditCardListIsLoaded
        {
            get
            {
                return _lazyGrowerCreditCardList == null ? false : _lazyGrowerCreditCardList.IsValueCreated;
            }
        }

        public void SetLazyGrowerCreditCardList(Lazy<List<GrowerCreditCard>> lazyGrowerCreditCardList)
        {
            _lazyGrowerCreditCardList = lazyGrowerCreditCardList;
        }

		#endregion

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyGrowerOrderList;

        public List<GrowerOrder> GrowerOrderList
        {
            get
            {
                return _lazyGrowerOrderList == null ? null : _lazyGrowerOrderList.Value;
            }
        }

        public bool GrowerOrderListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderList == null ? false : _lazyGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderList(Lazy<List<GrowerOrder>> lazyGrowerOrderList)
        {
            _lazyGrowerOrderList = lazyGrowerOrderList;
        }

		#endregion

		#region GrowerVolume Lazy Loading Properties and Methods

        private Lazy<List<GrowerVolume>> _lazyGrowerVolumeList;

        public List<GrowerVolume> GrowerVolumeList
        {
            get
            {
                return _lazyGrowerVolumeList == null ? null : _lazyGrowerVolumeList.Value;
            }
        }

        public bool GrowerVolumeListIsLoaded
        {
            get
            {
                return _lazyGrowerVolumeList == null ? false : _lazyGrowerVolumeList.IsValueCreated;
            }
        }

        public void SetLazyGrowerVolumeList(Lazy<List<GrowerVolume>> lazyGrowerVolumeList)
        {
            _lazyGrowerVolumeList = lazyGrowerVolumeList;
        }

		#endregion

        //#region Ledger Lazy Loading Properties and Methods

        //private Lazy<List<Ledger>> _lazyLedgerList;

        //public List<Ledger> LedgerList
        //{
        //    get
        //    {
        //        return _lazyLedgerList == null ? null : _lazyLedgerList.Value;
        //    }
        //}

        //public bool LedgerListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyLedgerList == null ? false : _lazyLedgerList.IsValueCreated;
        //    }
        //}

        //public void SetLazyLedgerList(Lazy<List<Ledger>> lazyLedgerList)
        //{
        //    _lazyLedgerList = lazyLedgerList;
        //}

        //#endregion

		#region Person Lazy Loading Properties and Methods

        private Lazy<List<Person>> _lazyPersonList;

        public List<Person> PersonList
        {
            get
            {
                return _lazyPersonList == null ? null : _lazyPersonList.Value;
            }
        }

        public bool PersonListIsLoaded
        {
            get
            {
                return _lazyPersonList == null ? false : _lazyPersonList.IsValueCreated;
            }
        }

        public void SetLazyPersonList(Lazy<List<Person>> lazyPersonList)
        {
            _lazyPersonList = lazyPersonList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "IsActivated={0}",this.IsActivated);
			result.Append(", ");
			result.AppendFormat( "AddressGuid={0}",this.AddressGuid);
			result.Append(", ");
			result.AppendFormat( "AllowSubstitutions={0}",this.AllowSubstitutions);
			result.Append(", ");
			result.AppendFormat( "CreditLimit={0}",this.CreditLimit);
			result.Append(", ");
			result.AppendFormat( "CreditLimitTemporaryIncrease={0}",this.CreditLimitTemporaryIncrease);
			result.Append(", ");
			result.AppendFormat( "CreditLimitTemporaryIncreaseExpiration={0}",this.CreditLimitTemporaryIncreaseExpiration);
			result.Append(", ");
			result.AppendFormat( "EMail={0}",this.EMail);
			result.Append(", ");
			result.AppendFormat( "PhoneAreaCode={0}",this.PhoneAreaCode);
			result.Append(", ");
			result.AppendFormat( "Phone={0}",this.Phone);
			result.Append(", ");
			result.AppendFormat( "GrowerTypeLookupGuid={0}",this.GrowerTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "DefaultPaymentTypeLookupGuid={0}",this.DefaultPaymentTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "SalesTaxNumber={0}",this.SalesTaxNumber);
			result.Append(", ");
			result.AppendFormat( "CreditAppOnFile={0}",this.CreditAppOnFile);
			result.Append(", ");
			result.AppendFormat( "RequestedLineOfCredit={0}",this.RequestedLineOfCredit);
			result.Append(", ");
			result.AppendFormat( "RequestedLineOfCreditStatusLookupGuid={0}",this.RequestedLineOfCreditStatusLookupGuid);
			result.Append(", ");
			result.AppendFormat( "GrowerSizeLookupGuid={0}",this.GrowerSizeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "AdditionalAccountingEmail={0}",this.AdditionalAccountingEmail);
            result.Append(", ");
            result.AppendFormat("AdditionalOrderEmail={0}", this.AdditionalOrderEmail);
            result.Append(", ");
            result.AppendFormat("UseDummenPriceMethod={0}", this.UseDummenPriceMethod);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
