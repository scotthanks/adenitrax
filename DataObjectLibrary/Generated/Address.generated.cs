using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
    public partial class Address : TableObjectBase
    {
        public enum ColumnEnum
        {
            Id,
            Guid,
            DateDeactivated,
            Name,
            StreetAddress1,
            StreetAddress2,
            City,
            StateGuid,
            ZipCode,
        }

        new public enum ForeignKeyFieldEnum
        {
            StateGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(State),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public string Name { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public Guid StateGuid { get; set; }
        public string ZipCode { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "Id":
                    value = LongToString(Id);
                    break;
                case "Guid":
                    value = GuidToString(Guid);
                    break;
                case "DateDeactivated":
                    value = DateTimeToString(DateDeactivated);
                    break;
                case "Name":
                    value = StringToString(Name);
                    break;
                case "StreetAddress1":
                    value = StringToString(StreetAddress1);
                    break;
                case "StreetAddress2":
                    value = StringToString(StreetAddress2);
                    break;
                case "City":
                    value = StringToString(City);
                    break;
                case "StateGuid":
                    value = GuidToString(StateGuid);
                    break;
                case "ZipCode":
                    value = StringToString(ZipCode);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "Id":
                    Id = (long)LongFromString(value, isNullable: false);
                    break;
                case "Guid":
                    Guid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "DateDeactivated":
                    DateDeactivated = DateTimeFromString(value, isNullable: true);
                    break;
                case "Name":
                    Name = (string)StringFromString(value, isNullable: false);
                    break;
                case "StreetAddress1":
                    StreetAddress1 = (string)StringFromString(value, isNullable: false);
                    break;
                case "StreetAddress2":
                    StreetAddress2 = (string)StringFromString(value, isNullable: false);
                    break;
                case "City":
                    City = (string)StringFromString(value, isNullable: false);
                    break;
                case "StateGuid":
                    StateGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ZipCode":
                    ZipCode = (string)StringFromString(value, isNullable: false);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "Id":
                        value = Id;
                        break;
                    case "Guid":
                        value = Guid;
                        break;
                    case "DateDeactivated":
                        value = DateDeactivated;
                        break;
                    case "Name":
                        value = Name;
                        break;
                    case "StreetAddress1":
                        value = StreetAddress1;
                        break;
                    case "StreetAddress2":
                        value = StreetAddress2;
                        break;
                    case "City":
                        value = City;
                        break;
                    case "StateGuid":
                        value = StateGuid;
                        break;
                    case "ZipCode":
                        value = ZipCode;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "Id":
                        Id = (long)value;
                        break;
                    case "Guid":
                        Guid = (Guid)value;
                        break;
                    case "DateDeactivated":
                        DateDeactivated = (DateTime?)value;
                        break;
                    case "Name":
                        Name = (string)value;
                        break;
                    case "StreetAddress1":
                        StreetAddress1 = (string)value;
                        break;
                    case "StreetAddress2":
                        StreetAddress2 = (string)value;
                        break;
                    case "City":
                        City = (string)value;
                        break;
                    case "StateGuid":
                        StateGuid = (Guid)value;
                        break;
                    case "ZipCode":
                        ZipCode = (string)value;
                        break;
                }
            }
        }

        #region Lazy Loading Logic

        #region State Lazy Loading Properties and Methods

        private Lazy<State> _lazyState;

        public State State
        {
            get
            {
                return _lazyState == null ? null : _lazyState.Value;
            }
            set
            {
                _lazyState = new Lazy<State>(() => value);
            }
        }

        public bool StateIsLoaded
        {
            get
            {
                return _lazyState == null ? false : _lazyState.IsValueCreated;
            }
        }

        public void SetLazyState(Lazy<State> lazyState)
        {
            _lazyState = lazyState;
        }

        #endregion

        #region Grower Lazy Loading Properties and Methods

        private Lazy<List<Grower>> _lazyGrowerList;

        public List<Grower> GrowerList
        {
            get
            {
                return _lazyGrowerList == null ? null : _lazyGrowerList.Value;
            }
        }

        public bool GrowerListIsLoaded
        {
            get
            {
                return _lazyGrowerList == null ? false : _lazyGrowerList.IsValueCreated;
            }
        }

        public void SetLazyGrowerList(Lazy<List<Grower>> lazyGrowerList)
        {
            _lazyGrowerList = lazyGrowerList;
        }

        #endregion

        #region GrowerAddress Lazy Loading Properties and Methods

        private Lazy<List<GrowerAddress>> _lazyGrowerAddressList;

        public List<GrowerAddress> GrowerAddressList
        {
            get
            {
                return _lazyGrowerAddressList == null ? null : _lazyGrowerAddressList.Value;
            }
        }

        public bool GrowerAddressListIsLoaded
        {
            get
            {
                return _lazyGrowerAddressList == null ? false : _lazyGrowerAddressList.IsValueCreated;
            }
        }

        public void SetLazyGrowerAddressList(Lazy<List<GrowerAddress>> lazyGrowerAddressList)
        {
            _lazyGrowerAddressList = lazyGrowerAddressList;
        }

        #endregion

        #region ShipToGrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyShipToGrowerOrderList;

        public List<GrowerOrder> ShipToGrowerOrderList
        {
            get
            {
                return _lazyShipToGrowerOrderList == null ? null : _lazyShipToGrowerOrderList.Value;
            }
        }

        public bool ShipToGrowerOrderListIsLoaded
        {
            get
            {
                return _lazyShipToGrowerOrderList == null ? false : _lazyShipToGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyShipToGrowerOrderList(Lazy<List<GrowerOrder>> lazyShipToGrowerOrderList)
        {
            _lazyShipToGrowerOrderList = lazyShipToGrowerOrderList;
        }

        #endregion

        #region Supplier Lazy Loading Properties and Methods

        private Lazy<List<Supplier>> _lazySupplierList;

        public List<Supplier> SupplierList
        {
            get
            {
                return _lazySupplierList == null ? null : _lazySupplierList.Value;
            }
        }

        public bool SupplierListIsLoaded
        {
            get
            {
                return _lazySupplierList == null ? false : _lazySupplierList.IsValueCreated;
            }
        }

        public void SetLazySupplierList(Lazy<List<Supplier>> lazySupplierList)
        {
            _lazySupplierList = lazySupplierList;
        }

        #endregion

        #endregion

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("Id={0}", this.Id);
            result.Append(", ");
            result.AppendFormat("Guid={0}", this.Guid);
            result.Append(", ");
            result.AppendFormat("DateDeactivated={0}", this.DateDeactivated);
            result.Append(", ");
            result.AppendFormat("Name={0}", this.Name);
            result.Append(", ");
            result.AppendFormat("StreetAddress1={0}", this.StreetAddress1);
            result.Append(", ");
            result.AppendFormat("StreetAddress2={0}", this.StreetAddress2);
            result.Append(", ");
            result.AppendFormat("City={0}", this.City);
            result.Append(", ");
            result.AppendFormat("StateGuid={0}", this.StateGuid);
            result.Append(", ");
            result.AppendFormat("ZipCode={0}", this.ZipCode);

            return result.ToString();
        }

        new protected static bool GetHasIdColumn() { return true; }
        new protected static bool GetHasGuidColumn() { return true; }
        new protected static bool GetHasDateDeactivatedColumn() { return true; }
        new protected static bool GetHasCodeColumn() { return false; }
        new protected static bool GetHasNameColumn() { return true; }
        new protected static bool GetDateDeactivatedIsNullable() { return true; }
    }
}
