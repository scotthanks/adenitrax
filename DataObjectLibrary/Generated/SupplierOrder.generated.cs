using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class SupplierOrder : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			GrowerOrderGuid,
			SupplierGuid,
			PoNo,
			SupplierOrderNo,
			TotalCost,
			SupplierOrderStatusLookupGuid,
			TagRatioLookupGuid,
			ShipMethodLookupGuid,
            TrackingNumber,
            ExpectedDeliveredDate,
			DateLastSenttoSupplier,
            TrackingComment,
		}

		new public enum ForeignKeyFieldEnum
		{
			GrowerOrderGuid,
			SupplierGuid,
			SupplierOrderStatusLookupGuid,
			TagRatioLookupGuid,
			ShipMethodLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(GrowerOrder),
						typeof(Supplier),
						typeof(Lookup),
						typeof(Lookup),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid GrowerOrderGuid { get; set; }
		public Guid SupplierGuid { get; set; }
		public string PoNo { get; set; }
		public string SupplierOrderNo { get; set; }
		public Decimal TotalCost { get; set; }
		public Guid SupplierOrderStatusLookupGuid { get; set; }
		public Guid? TagRatioLookupGuid { get; set; }
		public Guid? ShipMethodLookupGuid { get; set; }
        public string TrackingNumber { get; set; }
        public string TrackingComment { get; set; }
        public DateTime? ExpectedDeliveredDate { get; set; }
		public DateTime? DateLastSenttoSupplier { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "GrowerOrderGuid":
					value = GuidToString( GrowerOrderGuid);
					break;
				case "SupplierGuid":
					value = GuidToString( SupplierGuid);
					break;
				case "PoNo":
					value = StringToString( PoNo);
					break;
				case "SupplierOrderNo":
					value = StringToString( SupplierOrderNo);
					break;
				case "TotalCost":
					value = DecimalToString( TotalCost);
					break;
				case "SupplierOrderStatusLookupGuid":
					value = GuidToString( SupplierOrderStatusLookupGuid);
					break;
				case "TagRatioLookupGuid":
					value = GuidToString( TagRatioLookupGuid);
					break;
				case "ShipMethodLookupGuid":
					value = GuidToString( ShipMethodLookupGuid);
					break;
                case "TrackingNumber":
                    value = StringToString(TrackingNumber);
                    break;
                case "TrackingComment":
                    value = StringToString(TrackingComment);
                    break;
                case "ExpectedDeliveredDate":
					value = DateTimeToString( ExpectedDeliveredDate);
					break;
				case "DateLastSenttoSupplier":
					value = DateTimeToString( DateLastSenttoSupplier);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "GrowerOrderGuid":
					GrowerOrderGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "SupplierGuid":
					SupplierGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "PoNo":
					PoNo = (string)StringFromString( value, isNullable: false);
					break;
				case "SupplierOrderNo":
					SupplierOrderNo = (string)StringFromString( value, isNullable: false);
					break;
				case "TotalCost":
					TotalCost = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "SupplierOrderStatusLookupGuid":
					SupplierOrderStatusLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "TagRatioLookupGuid":
					TagRatioLookupGuid = GuidFromString( value, isNullable: true);
					break;
				case "ShipMethodLookupGuid":
					ShipMethodLookupGuid = GuidFromString( value, isNullable: true);
					break;
                case "TrackingNumber":
                    TrackingNumber = (string)StringFromString(value, isNullable: false);
                    break;
                case "TrackingComment":
                    TrackingComment = (string)StringFromString(value, isNullable: false);
                    break;
                case "ExpectedDeliveredDate":
					ExpectedDeliveredDate = DateTimeFromString( value, isNullable: true);
					break;
				case "DateLastSenttoSupplier":
					DateLastSenttoSupplier = DateTimeFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "GrowerOrderGuid":
						value = GrowerOrderGuid;
						break;
					case "SupplierGuid":
						value = SupplierGuid;
						break;
					case "PoNo":
						value = PoNo;
						break;
					case "SupplierOrderNo":
						value = SupplierOrderNo;
						break;
					case "TotalCost":
						value = TotalCost;
						break;
					case "SupplierOrderStatusLookupGuid":
						value = SupplierOrderStatusLookupGuid;
						break;
					case "TagRatioLookupGuid":
						value = TagRatioLookupGuid;
						break;
					case "ShipMethodLookupGuid":
						value = ShipMethodLookupGuid;
						break;
                    case "TrackingNumber":
                        value = TrackingNumber;
                        break;
                    case "TrackingComment":
                        value = TrackingComment;
                        break;
                    case "ExpectedDeliveredDate":
						value = ExpectedDeliveredDate;
						break;
					case "DateLastSenttoSupplier":
						value = DateLastSenttoSupplier;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "GrowerOrderGuid":
						GrowerOrderGuid = (Guid)value;
						break;
					case "SupplierGuid":
						SupplierGuid = (Guid)value;
						break;
					case "PoNo":
						PoNo = (string)value;
						break;
					case "SupplierOrderNo":
						SupplierOrderNo = (string)value;
						break;
					case "TotalCost":
						TotalCost = (Decimal)value;
						break;
					case "SupplierOrderStatusLookupGuid":
						SupplierOrderStatusLookupGuid = (Guid)value;
						break;
					case "TagRatioLookupGuid":
						TagRatioLookupGuid = (Guid?)value;
						break;
					case "ShipMethodLookupGuid":
						ShipMethodLookupGuid = (Guid?)value;
						break;
                    case "TrackingNumber":
                        TrackingNumber = (string)value;
                        break;
                    case "TrackingComment":
                        TrackingComment = (string)value;
                        break;
                    case "ExpectedDeliveredDate":
						ExpectedDeliveredDate = (DateTime?)value;
						break;
					case "DateLastSenttoSupplier":
						DateLastSenttoSupplier = (DateTime?)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<GrowerOrder> _lazyGrowerOrder;

        public GrowerOrder GrowerOrder
        {
            get
            {
                return _lazyGrowerOrder == null ? null : _lazyGrowerOrder.Value;
            }
            set
            {
                _lazyGrowerOrder = new Lazy<GrowerOrder>(() => value);
            }
        }

        public bool GrowerOrderIsLoaded
        {
            get
            {
                return _lazyGrowerOrder == null ? false : _lazyGrowerOrder.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrder(Lazy<GrowerOrder> lazyGrowerOrder)
        {
            _lazyGrowerOrder = lazyGrowerOrder;
        }

		#endregion

		#region Supplier Lazy Loading Properties and Methods

        private Lazy<Supplier> _lazySupplier;

        public Supplier Supplier
        {
            get
            {
                return _lazySupplier == null ? null : _lazySupplier.Value;
            }
            set
            {
                _lazySupplier = new Lazy<Supplier>(() => value);
            }
        }

        public bool SupplierIsLoaded
        {
            get
            {
                return _lazySupplier == null ? false : _lazySupplier.IsValueCreated;
            }
        }

        public void SetLazySupplier(Lazy<Supplier> lazySupplier)
        {
            _lazySupplier = lazySupplier;
        }

		#endregion

		#region SupplierOrderStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazySupplierOrderStatusLookup;

        public Lookup SupplierOrderStatusLookup
        {
            get
            {
                return _lazySupplierOrderStatusLookup == null ? null : _lazySupplierOrderStatusLookup.Value;
            }
            set
            {
                _lazySupplierOrderStatusLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool SupplierOrderStatusLookupIsLoaded
        {
            get
            {
                return _lazySupplierOrderStatusLookup == null ? false : _lazySupplierOrderStatusLookup.IsValueCreated;
            }
        }

        public void SetLazySupplierOrderStatusLookup(Lazy<Lookup> lazySupplierOrderStatusLookup)
        {
            _lazySupplierOrderStatusLookup = lazySupplierOrderStatusLookup;
        }

		#endregion

		#region TagRatioLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyTagRatioLookup;

        public Lookup TagRatioLookup
        {
            get
            {
                return _lazyTagRatioLookup == null ? null : _lazyTagRatioLookup.Value;
            }
            set
            {
                _lazyTagRatioLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool TagRatioLookupIsLoaded
        {
            get
            {
                return _lazyTagRatioLookup == null ? false : _lazyTagRatioLookup.IsValueCreated;
            }
        }

        public void SetLazyTagRatioLookup(Lazy<Lookup> lazyTagRatioLookup)
        {
            _lazyTagRatioLookup = lazyTagRatioLookup;
        }

		#endregion

		#region ShipMethodLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyShipMethodLookup;

        public Lookup ShipMethodLookup
        {
            get
            {
                return _lazyShipMethodLookup == null ? null : _lazyShipMethodLookup.Value;
            }
            set
            {
                _lazyShipMethodLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ShipMethodLookupIsLoaded
        {
            get
            {
                return _lazyShipMethodLookup == null ? false : _lazyShipMethodLookup.IsValueCreated;
            }
        }

        public void SetLazyShipMethodLookup(Lazy<Lookup> lazyShipMethodLookup)
        {
            _lazyShipMethodLookup = lazyShipMethodLookup;
        }

		#endregion

		#region OrderLine Lazy Loading Properties and Methods

        private Lazy<List<OrderLine>> _lazyOrderLineList;

        public List<OrderLine> OrderLineList
        {
            get
            {
                return _lazyOrderLineList == null ? null : _lazyOrderLineList.Value;
            }
        }

        public bool OrderLineListIsLoaded
        {
            get
            {
                return _lazyOrderLineList == null ? false : _lazyOrderLineList.IsValueCreated;
            }
        }

        public void SetLazyOrderLineList(Lazy<List<OrderLine>> lazyOrderLineList)
        {
            _lazyOrderLineList = lazyOrderLineList;
        }

		#endregion

		#region SupplierOrderFee Lazy Loading Properties and Methods

        private Lazy<List<SupplierOrderFee>> _lazySupplierOrderFeeList;

        public List<SupplierOrderFee> SupplierOrderFeeList
        {
            get
            {
                return _lazySupplierOrderFeeList == null ? null : _lazySupplierOrderFeeList.Value;
            }
        }

        public bool SupplierOrderFeeListIsLoaded
        {
            get
            {
                return _lazySupplierOrderFeeList == null ? false : _lazySupplierOrderFeeList.IsValueCreated;
            }
        }

        public void SetLazySupplierOrderFeeList(Lazy<List<SupplierOrderFee>> lazySupplierOrderFeeList)
        {
            _lazySupplierOrderFeeList = lazySupplierOrderFeeList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "GrowerOrderGuid={0}",this.GrowerOrderGuid);
			result.Append(", ");
			result.AppendFormat( "SupplierGuid={0}",this.SupplierGuid);
			result.Append(", ");
			result.AppendFormat( "PoNo={0}",this.PoNo);
			result.Append(", ");
			result.AppendFormat( "SupplierOrderNo={0}",this.SupplierOrderNo);
			result.Append(", ");
			result.AppendFormat( "TotalCost={0}",this.TotalCost);
			result.Append(", ");
			result.AppendFormat( "SupplierOrderStatusLookupGuid={0}",this.SupplierOrderStatusLookupGuid);
			result.Append(", ");
			result.AppendFormat( "TagRatioLookupGuid={0}",this.TagRatioLookupGuid);
			result.Append(", ");
			result.AppendFormat( "ShipMethodLookupGuid={0}",this.ShipMethodLookupGuid);
			result.Append(", ");
            result.AppendFormat("TrackingNumber={0}", this.TrackingNumber);
            result.Append(", ");
            result.AppendFormat("TrackingCommentr={0}", this.TrackingComment);
            result.Append(", ");
            result.AppendFormat("ExpectedDeliveredDate={0}", this.ExpectedDeliveredDate);
			result.Append(", ");
			result.AppendFormat( "DateLastSenttoSupplier={0}",this.DateLastSenttoSupplier);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
