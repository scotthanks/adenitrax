using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class ProgramSeason : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			ProgramGuid,
			StartDate,
			EndDate,
			EODDate,
			Code,
			Name,
			RollingWeeksEOD,
			OpenWeeksDesc,
			NAWeeksDesc,
			SpecialRulesByForm,
			SpecialRulesBySpecies,
			SpecialRulesByProduct,
			RunAvailabilityUpdate,
			InternalStatus,
		}

		new public enum ForeignKeyFieldEnum
		{
			ProgramGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Program),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid ProgramGuid { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public DateTime EODDate { get; set; }
		public string Name { get; set; }
		public int RollingWeeksEOD { get; set; }
		public string OpenWeeksDesc { get; set; }
		public string NAWeeksDesc { get; set; }
		public string SpecialRulesByForm { get; set; }
		public string SpecialRulesBySpecies { get; set; }
		public string SpecialRulesByProduct { get; set; }
		public bool RunAvailabilityUpdate { get; set; }
		public string InternalStatus { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "ProgramGuid":
					value = GuidToString( ProgramGuid);
					break;
				case "StartDate":
					value = DateTimeToString( StartDate);
					break;
				case "EndDate":
					value = DateTimeToString( EndDate);
					break;
				case "EODDate":
					value = DateTimeToString( EODDate);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "RollingWeeksEOD":
					value = IntToString( RollingWeeksEOD);
					break;
				case "OpenWeeksDesc":
					value = StringToString( OpenWeeksDesc);
					break;
				case "NAWeeksDesc":
					value = StringToString( NAWeeksDesc);
					break;
				case "SpecialRulesByForm":
					value = StringToString( SpecialRulesByForm);
					break;
				case "SpecialRulesBySpecies":
					value = StringToString( SpecialRulesBySpecies);
					break;
				case "SpecialRulesByProduct":
					value = StringToString( SpecialRulesByProduct);
					break;
				case "RunAvailabilityUpdate":
					value = BoolToString( RunAvailabilityUpdate);
					break;
				case "InternalStatus":
					value = StringToString( InternalStatus);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "ProgramGuid":
					ProgramGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "StartDate":
					StartDate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "EndDate":
					EndDate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "EODDate":
					EODDate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "RollingWeeksEOD":
					RollingWeeksEOD = (int)IntFromString( value, isNullable: false);
					break;
				case "OpenWeeksDesc":
					OpenWeeksDesc = (string)StringFromString( value, isNullable: false);
					break;
				case "NAWeeksDesc":
					NAWeeksDesc = (string)StringFromString( value, isNullable: false);
					break;
				case "SpecialRulesByForm":
					SpecialRulesByForm = (string)StringFromString( value, isNullable: false);
					break;
				case "SpecialRulesBySpecies":
					SpecialRulesBySpecies = (string)StringFromString( value, isNullable: false);
					break;
				case "SpecialRulesByProduct":
					SpecialRulesByProduct = (string)StringFromString( value, isNullable: false);
					break;
				case "RunAvailabilityUpdate":
					RunAvailabilityUpdate = (bool)BoolFromString( value, isNullable: false);
					break;
				case "InternalStatus":
					InternalStatus = (string)StringFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "ProgramGuid":
						value = ProgramGuid;
						break;
					case "StartDate":
						value = StartDate;
						break;
					case "EndDate":
						value = EndDate;
						break;
					case "EODDate":
						value = EODDate;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "RollingWeeksEOD":
						value = RollingWeeksEOD;
						break;
					case "OpenWeeksDesc":
						value = OpenWeeksDesc;
						break;
					case "NAWeeksDesc":
						value = NAWeeksDesc;
						break;
					case "SpecialRulesByForm":
						value = SpecialRulesByForm;
						break;
					case "SpecialRulesBySpecies":
						value = SpecialRulesBySpecies;
						break;
					case "SpecialRulesByProduct":
						value = SpecialRulesByProduct;
						break;
					case "RunAvailabilityUpdate":
						value = RunAvailabilityUpdate;
						break;
					case "InternalStatus":
						value = InternalStatus;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "ProgramGuid":
						ProgramGuid = (Guid)value;
						break;
					case "StartDate":
						StartDate = (DateTime)value;
						break;
					case "EndDate":
						EndDate = (DateTime)value;
						break;
					case "EODDate":
						EODDate = (DateTime)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "RollingWeeksEOD":
						RollingWeeksEOD = (int)value;
						break;
					case "OpenWeeksDesc":
						OpenWeeksDesc = (string)value;
						break;
					case "NAWeeksDesc":
						NAWeeksDesc = (string)value;
						break;
					case "SpecialRulesByForm":
						SpecialRulesByForm = (string)value;
						break;
					case "SpecialRulesBySpecies":
						SpecialRulesBySpecies = (string)value;
						break;
					case "SpecialRulesByProduct":
						SpecialRulesByProduct = (string)value;
						break;
					case "RunAvailabilityUpdate":
						RunAvailabilityUpdate = (bool)value;
						break;
					case "InternalStatus":
						InternalStatus = (string)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Program Lazy Loading Properties and Methods

        private Lazy<Program> _lazyProgram;

        public Program Program
        {
            get
            {
                return _lazyProgram == null ? null : _lazyProgram.Value;
            }
            set
            {
                _lazyProgram = new Lazy<Program>(() => value);
            }
        }

        public bool ProgramIsLoaded
        {
            get
            {
                return _lazyProgram == null ? false : _lazyProgram.IsValueCreated;
            }
        }

        public void SetLazyProgram(Lazy<Program> lazyProgram)
        {
            _lazyProgram = lazyProgram;
        }

		#endregion

		#region GrowerVolume Lazy Loading Properties and Methods

        private Lazy<List<GrowerVolume>> _lazyGrowerVolumeList;

        public List<GrowerVolume> GrowerVolumeList
        {
            get
            {
                return _lazyGrowerVolumeList == null ? null : _lazyGrowerVolumeList.Value;
            }
        }

        public bool GrowerVolumeListIsLoaded
        {
            get
            {
                return _lazyGrowerVolumeList == null ? false : _lazyGrowerVolumeList.IsValueCreated;
            }
        }

        public void SetLazyGrowerVolumeList(Lazy<List<GrowerVolume>> lazyGrowerVolumeList)
        {
            _lazyGrowerVolumeList = lazyGrowerVolumeList;
        }

		#endregion

        //#region ProductPriceGroupProgramSeason Lazy Loading Properties and Methods

        //private Lazy<List<ProductPriceGroupProgramSeason>> _lazyProductPriceGroupProgramSeasonList;

        //public List<ProductPriceGroupProgramSeason> ProductPriceGroupProgramSeasonList
        //{
        //    get
        //    {
        //        return _lazyProductPriceGroupProgramSeasonList == null ? null : _lazyProductPriceGroupProgramSeasonList.Value;
        //    }
        //}

        //public bool ProductPriceGroupProgramSeasonListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyProductPriceGroupProgramSeasonList == null ? false : _lazyProductPriceGroupProgramSeasonList.IsValueCreated;
        //    }
        //}

        //public void SetLazyProductPriceGroupProgramSeasonList(Lazy<List<ProductPriceGroupProgramSeason>> lazyProductPriceGroupProgramSeasonList)
        //{
        //    _lazyProductPriceGroupProgramSeasonList = lazyProductPriceGroupProgramSeasonList;
        //}

        //#endregion

		#region VolumeLevel Lazy Loading Properties and Methods

        private Lazy<List<VolumeLevel>> _lazyVolumeLevelList;

        public List<VolumeLevel> VolumeLevelList
        {
            get
            {
                return _lazyVolumeLevelList == null ? null : _lazyVolumeLevelList.Value;
            }
        }

        public bool VolumeLevelListIsLoaded
        {
            get
            {
                return _lazyVolumeLevelList == null ? false : _lazyVolumeLevelList.IsValueCreated;
            }
        }

        public void SetLazyVolumeLevelList(Lazy<List<VolumeLevel>> lazyVolumeLevelList)
        {
            _lazyVolumeLevelList = lazyVolumeLevelList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "ProgramGuid={0}",this.ProgramGuid);
			result.Append(", ");
			result.AppendFormat( "StartDate={0}",this.StartDate);
			result.Append(", ");
			result.AppendFormat( "EndDate={0}",this.EndDate);
			result.Append(", ");
			result.AppendFormat( "EODDate={0}",this.EODDate);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "RollingWeeksEOD={0}",this.RollingWeeksEOD);
			result.Append(", ");
			result.AppendFormat( "OpenWeeksDesc={0}",this.OpenWeeksDesc);
			result.Append(", ");
			result.AppendFormat( "NAWeeksDesc={0}",this.NAWeeksDesc);
			result.Append(", ");
			result.AppendFormat( "SpecialRulesByForm={0}",this.SpecialRulesByForm);
			result.Append(", ");
			result.AppendFormat( "SpecialRulesBySpecies={0}",this.SpecialRulesBySpecies);
			result.Append(", ");
			result.AppendFormat( "SpecialRulesByProduct={0}",this.SpecialRulesByProduct);
			result.Append(", ");
			result.AppendFormat( "RunAvailabilityUpdate={0}",this.RunAvailabilityUpdate);
			result.Append(", ");
			result.AppendFormat( "InternalStatus={0}",this.InternalStatus);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
