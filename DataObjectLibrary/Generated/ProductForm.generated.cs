using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class ProductForm : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			Code,
			Name,
			ProductFormCategoryGuid,
			SalesUnitQty,
			TagBundleQty,
			LineItemMinimumQty,
			SalesUnitsPerPackingUnit,
			SupplierGuid,
			IncludesDelivery,
			TagBundlesPerSalesUnit,
		}

		new public enum ForeignKeyFieldEnum
		{
			ProductFormCategoryGuid,
			SupplierGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(ProductFormCategory),
						typeof(Supplier),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public string Name { get; set; }
		public Guid ProductFormCategoryGuid { get; set; }
		public int SalesUnitQty { get; set; }
		public int TagBundleQty { get; set; }
		public int LineItemMinimumQty { get; set; }
		public int SalesUnitsPerPackingUnit { get; set; }
		public Guid? SupplierGuid { get; set; }
		public bool IncludesDelivery { get; set; }
		public int TagBundlesPerSalesUnit { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "Name":
					value = StringToString( Name);
					break;
				case "ProductFormCategoryGuid":
					value = GuidToString( ProductFormCategoryGuid);
					break;
				case "SalesUnitQty":
					value = IntToString( SalesUnitQty);
					break;
				case "TagBundleQty":
					value = IntToString( TagBundleQty);
					break;
				case "LineItemMinimumQty":
					value = IntToString( LineItemMinimumQty);
					break;
				case "SalesUnitsPerPackingUnit":
					value = IntToString( SalesUnitsPerPackingUnit);
					break;
				case "SupplierGuid":
					value = GuidToString( SupplierGuid);
					break;
				case "IncludesDelivery":
					value = BoolToString( IncludesDelivery);
					break;
				case "TagBundlesPerSalesUnit":
					value = IntToString( TagBundlesPerSalesUnit);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "Name":
					Name = (string)StringFromString( value, isNullable: false);
					break;
				case "ProductFormCategoryGuid":
					ProductFormCategoryGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "SalesUnitQty":
					SalesUnitQty = (int)IntFromString( value, isNullable: false);
					break;
				case "TagBundleQty":
					TagBundleQty = (int)IntFromString( value, isNullable: false);
					break;
				case "LineItemMinimumQty":
					LineItemMinimumQty = (int)IntFromString( value, isNullable: false);
					break;
				case "SalesUnitsPerPackingUnit":
					SalesUnitsPerPackingUnit = (int)IntFromString( value, isNullable: false);
					break;
				case "SupplierGuid":
					SupplierGuid = GuidFromString( value, isNullable: true);
					break;
				case "IncludesDelivery":
					IncludesDelivery = (bool)BoolFromString( value, isNullable: false);
					break;
				case "TagBundlesPerSalesUnit":
					TagBundlesPerSalesUnit = (int)IntFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "Code":
						value = Code;
						break;
					case "Name":
						value = Name;
						break;
					case "ProductFormCategoryGuid":
						value = ProductFormCategoryGuid;
						break;
					case "SalesUnitQty":
						value = SalesUnitQty;
						break;
					case "TagBundleQty":
						value = TagBundleQty;
						break;
					case "LineItemMinimumQty":
						value = LineItemMinimumQty;
						break;
					case "SalesUnitsPerPackingUnit":
						value = SalesUnitsPerPackingUnit;
						break;
					case "SupplierGuid":
						value = SupplierGuid;
						break;
					case "IncludesDelivery":
						value = IncludesDelivery;
						break;
					case "TagBundlesPerSalesUnit":
						value = TagBundlesPerSalesUnit;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "Name":
						Name = (string)value;
						break;
					case "ProductFormCategoryGuid":
						ProductFormCategoryGuid = (Guid)value;
						break;
					case "SalesUnitQty":
						SalesUnitQty = (int)value;
						break;
					case "TagBundleQty":
						TagBundleQty = (int)value;
						break;
					case "LineItemMinimumQty":
						LineItemMinimumQty = (int)value;
						break;
					case "SalesUnitsPerPackingUnit":
						SalesUnitsPerPackingUnit = (int)value;
						break;
					case "SupplierGuid":
						SupplierGuid = (Guid?)value;
						break;
					case "IncludesDelivery":
						IncludesDelivery = (bool)value;
						break;
					case "TagBundlesPerSalesUnit":
						TagBundlesPerSalesUnit = (int)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region ProductFormCategory Lazy Loading Properties and Methods

        private Lazy<ProductFormCategory> _lazyProductFormCategory;

        public ProductFormCategory ProductFormCategory
        {
            get
            {
                return _lazyProductFormCategory == null ? null : _lazyProductFormCategory.Value;
            }
            set
            {
                _lazyProductFormCategory = new Lazy<ProductFormCategory>(() => value);
            }
        }

        public bool ProductFormCategoryIsLoaded
        {
            get
            {
                return _lazyProductFormCategory == null ? false : _lazyProductFormCategory.IsValueCreated;
            }
        }

        public void SetLazyProductFormCategory(Lazy<ProductFormCategory> lazyProductFormCategory)
        {
            _lazyProductFormCategory = lazyProductFormCategory;
        }

		#endregion

		#region Supplier Lazy Loading Properties and Methods

        private Lazy<Supplier> _lazySupplier;

        public Supplier Supplier
        {
            get
            {
                return _lazySupplier == null ? null : _lazySupplier.Value;
            }
            set
            {
                _lazySupplier = new Lazy<Supplier>(() => value);
            }
        }

        public bool SupplierIsLoaded
        {
            get
            {
                return _lazySupplier == null ? false : _lazySupplier.IsValueCreated;
            }
        }

        public void SetLazySupplier(Lazy<Supplier> lazySupplier)
        {
            _lazySupplier = lazySupplier;
        }

		#endregion

		#region Product Lazy Loading Properties and Methods

        private Lazy<List<Product>> _lazyProductList;

        public List<Product> ProductList
        {
            get
            {
                return _lazyProductList == null ? null : _lazyProductList.Value;
            }
        }

        public bool ProductListIsLoaded
        {
            get
            {
                return _lazyProductList == null ? false : _lazyProductList.IsValueCreated;
            }
        }

        public void SetLazyProductList(Lazy<List<Product>> lazyProductList)
        {
            _lazyProductList = lazyProductList;
        }

		#endregion

        //#region FirstProductFormAvailabilityConversion Lazy Loading Properties and Methods

        //private Lazy<List<ProductFormAvailabilityConversion>> _lazyFirstProductFormAvailabilityConversionList;

        //public List<ProductFormAvailabilityConversion> FirstProductFormAvailabilityConversionList
        //{
        //    get
        //    {
        //        return _lazyFirstProductFormAvailabilityConversionList == null ? null : _lazyFirstProductFormAvailabilityConversionList.Value;
        //    }
        //}

        //public bool FirstProductFormAvailabilityConversionListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyFirstProductFormAvailabilityConversionList == null ? false : _lazyFirstProductFormAvailabilityConversionList.IsValueCreated;
        //    }
        //}

        //public void SetLazyFirstProductFormAvailabilityConversionList(Lazy<List<ProductFormAvailabilityConversion>> lazyFirstProductFormAvailabilityConversionList)
        //{
        //    _lazyFirstProductFormAvailabilityConversionList = lazyFirstProductFormAvailabilityConversionList;
        //}

        //#endregion

        //#region SecondProductFormAvailabilityConversion Lazy Loading Properties and Methods

        //private Lazy<List<ProductFormAvailabilityConversion>> _lazySecondProductFormAvailabilityConversionList;

        //public List<ProductFormAvailabilityConversion> SecondProductFormAvailabilityConversionList
        //{
        //    get
        //    {
        //        return _lazySecondProductFormAvailabilityConversionList == null ? null : _lazySecondProductFormAvailabilityConversionList.Value;
        //    }
        //}

        //public bool SecondProductFormAvailabilityConversionListIsLoaded
        //{
        //    get
        //    {
        //        return _lazySecondProductFormAvailabilityConversionList == null ? false : _lazySecondProductFormAvailabilityConversionList.IsValueCreated;
        //    }
        //}

        //public void SetLazySecondProductFormAvailabilityConversionList(Lazy<List<ProductFormAvailabilityConversion>> lazySecondProductFormAvailabilityConversionList)
        //{
        //    _lazySecondProductFormAvailabilityConversionList = lazySecondProductFormAvailabilityConversionList;
        //}

        //#endregion

        //#region ProductFormAvailabilityLinkedWeeks Lazy Loading Properties and Methods

        //private Lazy<List<ProductFormAvailabilityLinkedWeeks>> _lazyProductFormAvailabilityLinkedWeeksList;

        //public List<ProductFormAvailabilityLinkedWeeks> ProductFormAvailabilityLinkedWeeksList
        //{
        //    get
        //    {
        //        return _lazyProductFormAvailabilityLinkedWeeksList == null ? null : _lazyProductFormAvailabilityLinkedWeeksList.Value;
        //    }
        //}

        //public bool ProductFormAvailabilityLinkedWeeksListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyProductFormAvailabilityLinkedWeeksList == null ? false : _lazyProductFormAvailabilityLinkedWeeksList.IsValueCreated;
        //    }
        //}

        //public void SetLazyProductFormAvailabilityLinkedWeeksList(Lazy<List<ProductFormAvailabilityLinkedWeeks>> lazyProductFormAvailabilityLinkedWeeksList)
        //{
        //    _lazyProductFormAvailabilityLinkedWeeksList = lazyProductFormAvailabilityLinkedWeeksList;
        //}

        //#endregion

        //#region SupplierBoxProductForm Lazy Loading Properties and Methods

        //private Lazy<List<SupplierBoxProductForm>> _lazySupplierBoxProductFormList;

        //public List<SupplierBoxProductForm> SupplierBoxProductFormList
        //{
        //    get
        //    {
        //        return _lazySupplierBoxProductFormList == null ? null : _lazySupplierBoxProductFormList.Value;
        //    }
        //}

        //public bool SupplierBoxProductFormListIsLoaded
        //{
        //    get
        //    {
        //        return _lazySupplierBoxProductFormList == null ? false : _lazySupplierBoxProductFormList.IsValueCreated;
        //    }
        //}

        //public void SetLazySupplierBoxProductFormList(Lazy<List<SupplierBoxProductForm>> lazySupplierBoxProductFormList)
        //{
        //    _lazySupplierBoxProductFormList = lazySupplierBoxProductFormList;
        //}

        //#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "Name={0}",this.Name);
			result.Append(", ");
			result.AppendFormat( "ProductFormCategoryGuid={0}",this.ProductFormCategoryGuid);
			result.Append(", ");
			result.AppendFormat( "SalesUnitQty={0}",this.SalesUnitQty);
			result.Append(", ");
			result.AppendFormat( "TagBundleQty={0}",this.TagBundleQty);
			result.Append(", ");
			result.AppendFormat( "LineItemMinimumQty={0}",this.LineItemMinimumQty);
			result.Append(", ");
			result.AppendFormat( "SalesUnitsPerPackingUnit={0}",this.SalesUnitsPerPackingUnit);
			result.Append(", ");
			result.AppendFormat( "SupplierGuid={0}",this.SupplierGuid);
			result.Append(", ");
			result.AppendFormat( "IncludesDelivery={0}",this.IncludesDelivery);
			result.Append(", ");
			result.AppendFormat( "TagBundlesPerSalesUnit={0}",this.TagBundlesPerSalesUnit);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return true; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
