using System;
using System.Collections.Generic;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public static partial class TableObjects
	{
		public enum TypeEnum
		{
			Address,
            //Challenge,
            //ChallengeEvent,
            //Claim,
            //ClaimOrderLine,
            Config,
			Country,
            EventLog,
			GeneticOwner,
			Grower,
			GrowerAddress,
			GrowerCreditCard,
			GrowerOrder,
			GrowerOrderFee,
			GrowerOrderHistory,
            //GrowerOrderReminder,
			GrowerVolume,
			ImageFile,
			ImageSet,
			Lookup,
			OrderLine,
			Person,
			Price,
			PriceGroup,
			Product,
			ProductForm,
            //ProductFormAvailabilityConversion,
            ProductFormAvailabilityLinkedWeeks,
			ProductFormCategory,
            //ProductPriceGroupProgramSeason,
			Program,
            //ProgramCountry,
			ProgramSeason,
			ProgramType,
			ReportedAvailability,
			Series,
			ShipWeek,
			Species,
			State,
			Supplier,
			SupplierOrder,
			SupplierOrderFee,
            //Triple,
			Variety,
			VolumeLevel,
		}

		private static List<Type> _typeList;

		public static List<Type> TypeList
		{
			get
			{
				if (_typeList == null)
				{
					_typeList = new List<Type>
					{
						typeof( Address),
                        //typeof( Challenge),
                        //typeof( ChallengeEvent),
                        //typeof( Claim),
                        //typeof( ClaimOrderLine),
                        typeof( Config),
						typeof( Country),
                        typeof( EventLog),
						typeof( GeneticOwner),
						typeof( Grower),
						typeof( GrowerAddress),
						typeof( GrowerCreditCard),
						typeof( GrowerOrder),
						typeof( GrowerOrderFee),
						typeof( GrowerOrderHistory),
                        //typeof( GrowerOrderReminder),
						typeof( GrowerVolume),
						typeof( ImageFile),
						typeof( ImageSet),
						typeof( Lookup),
						typeof( OrderLine),
						typeof( Person),
						typeof( Price),
						typeof( PriceGroup),
						typeof( Product),
						typeof( ProductForm),
                        //typeof( ProductFormAvailabilityConversion),
                        typeof( ProductFormAvailabilityLinkedWeeks),
						typeof( ProductFormCategory),
                        //typeof( ProductPriceGroupProgramSeason),
						typeof( Program),
                        //typeof( ProgramCountry),
						typeof( ProgramSeason),
						typeof( ProgramType),
						typeof( ReportedAvailability),
						typeof( Series),
						typeof( ShipWeek),
						typeof( Species),
						typeof( State),
						typeof( Supplier),
						typeof( SupplierOrder),
						typeof( SupplierOrderFee),
                        //typeof( Triple),
						typeof( Variety),
						typeof( VolumeLevel),
					};
				}

				return _typeList;
			}
		}
	}
}
