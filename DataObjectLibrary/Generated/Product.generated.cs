using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class Product : TableObjectBase
	{
		public enum ColumnEnum
		{
			Guid,
			DateDeactivated,
			ProgramGuid,
			VarietyGuid,
			Code,
			SupplierIdentifier,
			ProductFormGuid,
			Id,
			SupplierDescription,
			ProductDescription,
			PriceGroupGuid,
			ProductionLeadTimeWeeks,
			IsOrganic,
			LastUpdate,
		}

		new public enum ForeignKeyFieldEnum
		{
			ProgramGuid,
			VarietyGuid,
			ProductFormGuid,
			PriceGroupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Program),
						typeof(Variety),
						typeof(ProductForm),
						typeof(PriceGroup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid ProgramGuid { get; set; }
		public Guid VarietyGuid { get; set; }
		public string SupplierIdentifier { get; set; }
		public Guid ProductFormGuid { get; set; }
		public string SupplierDescription { get; set; }
		public string ProductDescription { get; set; }
		public Guid PriceGroupGuid { get; set; }
		public int ProductionLeadTimeWeeks { get; set; }
		public bool IsOrganic { get; set; }
		public DateTime LastUpdate { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "ProgramGuid":
					value = GuidToString( ProgramGuid);
					break;
				case "VarietyGuid":
					value = GuidToString( VarietyGuid);
					break;
				case "Code":
					value = StringToString( Code);
					break;
				case "SupplierIdentifier":
					value = StringToString( SupplierIdentifier);
					break;
				case "ProductFormGuid":
					value = GuidToString( ProductFormGuid);
					break;
				case "Id":
					value = LongToString( Id);
					break;
				case "SupplierDescription":
					value = StringToString( SupplierDescription);
					break;
				case "ProductDescription":
					value = StringToString( ProductDescription);
					break;
				case "PriceGroupGuid":
					value = GuidToString( PriceGroupGuid);
					break;
				case "ProductionLeadTimeWeeks":
					value = IntToString( ProductionLeadTimeWeeks);
					break;
				case "IsOrganic":
					value = BoolToString( IsOrganic);
					break;
				case "LastUpdate":
					value = DateTimeToString( LastUpdate);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "ProgramGuid":
					ProgramGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "VarietyGuid":
					VarietyGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Code":
					Code = (string)StringFromString( value, isNullable: false);
					break;
				case "SupplierIdentifier":
					SupplierIdentifier = (string)StringFromString( value, isNullable: false);
					break;
				case "ProductFormGuid":
					ProductFormGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "SupplierDescription":
					SupplierDescription = (string)StringFromString( value, isNullable: false);
					break;
				case "ProductDescription":
					ProductDescription = (string)StringFromString( value, isNullable: false);
					break;
				case "PriceGroupGuid":
					PriceGroupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ProductionLeadTimeWeeks":
					ProductionLeadTimeWeeks = (int)IntFromString( value, isNullable: false);
					break;
				case "IsOrganic":
					IsOrganic = (bool)BoolFromString( value, isNullable: false);
					break;
				case "LastUpdate":
					LastUpdate = (DateTime)DateTimeFromString( value, isNullable: false);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "ProgramGuid":
						value = ProgramGuid;
						break;
					case "VarietyGuid":
						value = VarietyGuid;
						break;
					case "Code":
						value = Code;
						break;
					case "SupplierIdentifier":
						value = SupplierIdentifier;
						break;
					case "ProductFormGuid":
						value = ProductFormGuid;
						break;
					case "Id":
						value = Id;
						break;
					case "SupplierDescription":
						value = SupplierDescription;
						break;
					case "ProductDescription":
						value = ProductDescription;
						break;
					case "PriceGroupGuid":
						value = PriceGroupGuid;
						break;
					case "ProductionLeadTimeWeeks":
						value = ProductionLeadTimeWeeks;
						break;
					case "IsOrganic":
						value = IsOrganic;
						break;
					case "LastUpdate":
						value = LastUpdate;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "ProgramGuid":
						ProgramGuid = (Guid)value;
						break;
					case "VarietyGuid":
						VarietyGuid = (Guid)value;
						break;
					case "Code":
						Code = (string)value;
						break;
					case "SupplierIdentifier":
						SupplierIdentifier = (string)value;
						break;
					case "ProductFormGuid":
						ProductFormGuid = (Guid)value;
						break;
					case "Id":
						Id = (long)value;
						break;
					case "SupplierDescription":
						SupplierDescription = (string)value;
						break;
					case "ProductDescription":
						ProductDescription = (string)value;
						break;
					case "PriceGroupGuid":
						PriceGroupGuid = (Guid)value;
						break;
					case "ProductionLeadTimeWeeks":
						ProductionLeadTimeWeeks = (int)value;
						break;
					case "IsOrganic":
						IsOrganic = (bool)value;
						break;
					case "LastUpdate":
						LastUpdate = (DateTime)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Program Lazy Loading Properties and Methods

        private Lazy<Program> _lazyProgram;

        public Program Program
        {
            get
            {
                return _lazyProgram == null ? null : _lazyProgram.Value;
            }
            set
            {
                _lazyProgram = new Lazy<Program>(() => value);
            }
        }

        public bool ProgramIsLoaded
        {
            get
            {
                return _lazyProgram == null ? false : _lazyProgram.IsValueCreated;
            }
        }

        public void SetLazyProgram(Lazy<Program> lazyProgram)
        {
            _lazyProgram = lazyProgram;
        }

		#endregion

		#region Variety Lazy Loading Properties and Methods

        private Lazy<Variety> _lazyVariety;

        public Variety Variety
        {
            get
            {
                return _lazyVariety == null ? null : _lazyVariety.Value;
            }
            set
            {
                _lazyVariety = new Lazy<Variety>(() => value);
            }
        }

        public bool VarietyIsLoaded
        {
            get
            {
                return _lazyVariety == null ? false : _lazyVariety.IsValueCreated;
            }
        }

        public void SetLazyVariety(Lazy<Variety> lazyVariety)
        {
            _lazyVariety = lazyVariety;
        }

		#endregion

		#region ProductForm Lazy Loading Properties and Methods

        private Lazy<ProductForm> _lazyProductForm;

        public ProductForm ProductForm
        {
            get
            {
                return _lazyProductForm == null ? null : _lazyProductForm.Value;
            }
            set
            {
                _lazyProductForm = new Lazy<ProductForm>(() => value);
            }
        }

        public bool ProductFormIsLoaded
        {
            get
            {
                return _lazyProductForm == null ? false : _lazyProductForm.IsValueCreated;
            }
        }

        public void SetLazyProductForm(Lazy<ProductForm> lazyProductForm)
        {
            _lazyProductForm = lazyProductForm;
        }

		#endregion

		#region PriceGroup Lazy Loading Properties and Methods

        private Lazy<PriceGroup> _lazyPriceGroup;

        public PriceGroup PriceGroup
        {
            get
            {
                return _lazyPriceGroup == null ? null : _lazyPriceGroup.Value;
            }
            set
            {
                _lazyPriceGroup = new Lazy<PriceGroup>(() => value);
            }
        }

        public bool PriceGroupIsLoaded
        {
            get
            {
                return _lazyPriceGroup == null ? false : _lazyPriceGroup.IsValueCreated;
            }
        }

        public void SetLazyPriceGroup(Lazy<PriceGroup> lazyPriceGroup)
        {
            _lazyPriceGroup = lazyPriceGroup;
        }

		#endregion

		#region OrderLine Lazy Loading Properties and Methods

        private Lazy<List<OrderLine>> _lazyOrderLineList;

        public List<OrderLine> OrderLineList
        {
            get
            {
                return _lazyOrderLineList == null ? null : _lazyOrderLineList.Value;
            }
        }

        public bool OrderLineListIsLoaded
        {
            get
            {
                return _lazyOrderLineList == null ? false : _lazyOrderLineList.IsValueCreated;
            }
        }

        public void SetLazyOrderLineList(Lazy<List<OrderLine>> lazyOrderLineList)
        {
            _lazyOrderLineList = lazyOrderLineList;
        }

		#endregion

        //#region ProductPriceGroupProgramSeason Lazy Loading Properties and Methods

        //private Lazy<List<ProductPriceGroupProgramSeason>> _lazyProductPriceGroupProgramSeasonList;

        //public List<ProductPriceGroupProgramSeason> ProductPriceGroupProgramSeasonList
        //{
        //    get
        //    {
        //        return _lazyProductPriceGroupProgramSeasonList == null ? null : _lazyProductPriceGroupProgramSeasonList.Value;
        //    }
        //}

        //public bool ProductPriceGroupProgramSeasonListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyProductPriceGroupProgramSeasonList == null ? false : _lazyProductPriceGroupProgramSeasonList.IsValueCreated;
        //    }
        //}

        //public void SetLazyProductPriceGroupProgramSeasonList(Lazy<List<ProductPriceGroupProgramSeason>> lazyProductPriceGroupProgramSeasonList)
        //{
        //    _lazyProductPriceGroupProgramSeasonList = lazyProductPriceGroupProgramSeasonList;
        //}

        //#endregion

		#region ReportedAvailability Lazy Loading Properties and Methods

        private Lazy<List<ReportedAvailability>> _lazyReportedAvailabilityList;

        public List<ReportedAvailability> ReportedAvailabilityList
        {
            get
            {
                return _lazyReportedAvailabilityList == null ? null : _lazyReportedAvailabilityList.Value;
            }
        }

        public bool ReportedAvailabilityListIsLoaded
        {
            get
            {
                return _lazyReportedAvailabilityList == null ? false : _lazyReportedAvailabilityList.IsValueCreated;
            }
        }

        public void SetLazyReportedAvailabilityList(Lazy<List<ReportedAvailability>> lazyReportedAvailabilityList)
        {
            _lazyReportedAvailabilityList = lazyReportedAvailabilityList;
        }

		#endregion

		
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "ProgramGuid={0}",this.ProgramGuid);
			result.Append(", ");
			result.AppendFormat( "VarietyGuid={0}",this.VarietyGuid);
			result.Append(", ");
			result.AppendFormat( "Code={0}",this.Code);
			result.Append(", ");
			result.AppendFormat( "SupplierIdentifier={0}",this.SupplierIdentifier);
			result.Append(", ");
			result.AppendFormat( "ProductFormGuid={0}",this.ProductFormGuid);
			result.Append(", ");
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "SupplierDescription={0}",this.SupplierDescription);
			result.Append(", ");
			result.AppendFormat( "ProductDescription={0}",this.ProductDescription);
			result.Append(", ");
			result.AppendFormat( "PriceGroupGuid={0}",this.PriceGroupGuid);
			result.Append(", ");
			result.AppendFormat( "ProductionLeadTimeWeeks={0}",this.ProductionLeadTimeWeeks);
			result.Append(", ");
			result.AppendFormat( "IsOrganic={0}",this.IsOrganic);
			result.Append(", ");
			result.AppendFormat( "LastUpdate={0}",this.LastUpdate);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return true; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
