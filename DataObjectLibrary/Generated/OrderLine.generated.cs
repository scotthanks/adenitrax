using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class OrderLine : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			SupplierOrderGuid,
			ProductGuid,
			QtyOrdered,
			QtyShipped,
			ActualPriceUsedGuid,
			ActualPrice,
			OrderLineStatusLookupGuid,
			ChangeTypeLookupGuid,
			DateEntered,
			DateLastChanged,
			ActualCost,
			ActualCostUsedGuid,
            DateQtyLastChanged,
            LineNumber,
            LineComment,
            ShipWeekCode
		}

		new public enum ForeignKeyFieldEnum
		{
			SupplierOrderGuid,
			ProductGuid,
			OrderLineStatusLookupGuid,
			ChangeTypeLookupGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(SupplierOrder),
						typeof(Product),
						typeof(Lookup),
						typeof(Lookup),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid SupplierOrderGuid { get; set; }
		public Guid ProductGuid { get; set; }
		public int QtyOrdered { get; set; }
		public int QtyShipped { get; set; }
		public Guid? ActualPriceUsedGuid { get; set; }
		public Decimal ActualPrice { get; set; }
		public Guid OrderLineStatusLookupGuid { get; set; }
		public Guid ChangeTypeLookupGuid { get; set; }
		public DateTime? DateEntered { get; set; }
		public DateTime? DateLastChanged { get; set; }
		public Decimal? ActualCost { get; set; }
        public Guid? ActualCostUsedGuid { get; set; }
        public DateTime? DateQtyLastChanged { get; set; }
        public int LineNumber { get; set; }
        public string LineComment { get; set; }
        public string ShipWeekCode { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "SupplierOrderGuid":
					value = GuidToString( SupplierOrderGuid);
					break;
				case "ProductGuid":
					value = GuidToString( ProductGuid);
					break;
				case "QtyOrdered":
					value = IntToString( QtyOrdered);
					break;
				case "QtyShipped":
					value = IntToString( QtyShipped);
					break;
				case "ActualPriceUsedGuid":
					value = GuidToString( ActualPriceUsedGuid);
					break;
				case "ActualPrice":
					value = DecimalToString( ActualPrice);
					break;
				case "OrderLineStatusLookupGuid":
					value = GuidToString( OrderLineStatusLookupGuid);
					break;
				case "ChangeTypeLookupGuid":
					value = GuidToString( ChangeTypeLookupGuid);
					break;
				case "DateEntered":
					value = DateTimeToString( DateEntered);
					break;
				case "DateLastChanged":
					value = DateTimeToString( DateLastChanged);
					break;
				case "ActualCost":
					value = DecimalToString( ActualCost);
					break;
                case "ActualCostUsedGuid":
                    value = GuidToString(ActualCostUsedGuid);
                    break;
                case "DateQtyLastChanged":
                    value = DateTimeToString(DateQtyLastChanged);
                    break;
                case "LineNumber":
                    value = IntToString(LineNumber);
                    break;
                case "LineComment":
                    value = LineComment;
                    break;
                case "ShipWeekCode":
                    value = ShipWeekCode;
                    break;

            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "SupplierOrderGuid":
					SupplierOrderGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ProductGuid":
					ProductGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "QtyOrdered":
					QtyOrdered = (int)IntFromString( value, isNullable: false);
					break;
				case "QtyShipped":
					QtyShipped = (int)IntFromString( value, isNullable: false);
					break;
				case "ActualPriceUsedGuid":
					ActualPriceUsedGuid = GuidFromString( value, isNullable: true);
					break;
				case "ActualPrice":
					ActualPrice = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "OrderLineStatusLookupGuid":
					OrderLineStatusLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "ChangeTypeLookupGuid":
					ChangeTypeLookupGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateEntered":
					DateEntered = DateTimeFromString( value, isNullable: true);
					break;
				case "DateLastChanged":
					DateLastChanged = DateTimeFromString( value, isNullable: true);
					break;
				case "ActualCost":
					ActualCost = DecimalFromString( value, isNullable: true);
					break;
				case "ActualCostUsedGuid":
					ActualCostUsedGuid = GuidFromString( value, isNullable: true);
					break;
                case "DateQtyLastChanged":
                    DateQtyLastChanged = DateTimeFromString(value, isNullable: true);
                    break;
                case "LineNumber":
                    LineNumber = (int)IntFromString(value, isNullable: true);
                    break;
                case "LineComment":
                    LineComment = StringFromString(value, isNullable: true);
                    break;
                case "ShipWeekCode":
                    ShipWeekCode = StringFromString(value, isNullable: true);
                    break;
                default:
					valueSet = false;
					break;
                
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "SupplierOrderGuid":
						value = SupplierOrderGuid;
						break;
					case "ProductGuid":
						value = ProductGuid;
						break;
					case "QtyOrdered":
						value = QtyOrdered;
						break;
					case "QtyShipped":
						value = QtyShipped;
						break;
					case "ActualPriceUsedGuid":
						value = ActualPriceUsedGuid;
						break;
					case "ActualPrice":
						value = ActualPrice;
						break;
					case "OrderLineStatusLookupGuid":
						value = OrderLineStatusLookupGuid;
						break;
					case "ChangeTypeLookupGuid":
						value = ChangeTypeLookupGuid;
						break;
					case "DateEntered":
						value = DateEntered;
						break;
					case "DateLastChanged":
						value = DateLastChanged;
						break;
					case "ActualCost":
						value = ActualCost;
						break;
					case "ActualCostUsedGuid":
						value = ActualCostUsedGuid;
						break;
                    case "DateQtyLastChanged":
                        value = DateQtyLastChanged;
                        break;
                    case "LineNumber":
                        value = LineNumber;
                        break;
                    case "LineComment":
                        value = LineComment;
                        break;
                    case "ShipWeekCode":
                        value = ShipWeekCode;
                        break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "SupplierOrderGuid":
						SupplierOrderGuid = (Guid)value;
						break;
					case "ProductGuid":
						ProductGuid = (Guid)value;
						break;
					case "QtyOrdered":
						QtyOrdered = (int)value;
						break;
					case "QtyShipped":
						QtyShipped = (int)value;
						break;
					case "ActualPriceUsedGuid":
						ActualPriceUsedGuid = (Guid?)value;
						break;
					case "ActualPrice":
						ActualPrice = (Decimal)value;
						break;
					case "OrderLineStatusLookupGuid":
						OrderLineStatusLookupGuid = (Guid)value;
						break;
					case "ChangeTypeLookupGuid":
						ChangeTypeLookupGuid = (Guid)value;
						break;
					case "DateEntered":
						DateEntered = (DateTime?)value;
						break;
					case "DateLastChanged":
						DateLastChanged = (DateTime?)value;
						break;
					case "ActualCost":
						ActualCost = (Decimal?)value;
						break;
					case "ActualCostUsedGuid":
						ActualCostUsedGuid = (Guid?)value;
						break;
                    case "DateQtyLastChanged":
                        DateQtyLastChanged = (DateTime?)value;
                        break;
                    case "LineNumber":
                        LineNumber = (int)value;
                        break;
                    case "LineComment":
                        LineComment = (string)value;
                        break;
                    case "ShipWeekCode":
                        ShipWeekCode = (string)value;
                        break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region SupplierOrder Lazy Loading Properties and Methods

        private Lazy<SupplierOrder> _lazySupplierOrder;

        public SupplierOrder SupplierOrder
        {
            get
            {
                return _lazySupplierOrder == null ? null : _lazySupplierOrder.Value;
            }
            set
            {
                _lazySupplierOrder = new Lazy<SupplierOrder>(() => value);
            }
        }

        public bool SupplierOrderIsLoaded
        {
            get
            {
                return _lazySupplierOrder == null ? false : _lazySupplierOrder.IsValueCreated;
            }
        }

        public void SetLazySupplierOrder(Lazy<SupplierOrder> lazySupplierOrder)
        {
            _lazySupplierOrder = lazySupplierOrder;
        }

		#endregion

		#region Product Lazy Loading Properties and Methods

        private Lazy<Product> _lazyProduct;

        public Product Product
        {
            get
            {
                return _lazyProduct == null ? null : _lazyProduct.Value;
            }
            set
            {
                _lazyProduct = new Lazy<Product>(() => value);
            }
        }

        public bool ProductIsLoaded
        {
            get
            {
                return _lazyProduct == null ? false : _lazyProduct.IsValueCreated;
            }
        }

        public void SetLazyProduct(Lazy<Product> lazyProduct)
        {
            _lazyProduct = lazyProduct;
        }

		#endregion

		#region OrderLineStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyOrderLineStatusLookup;

        public Lookup OrderLineStatusLookup
        {
            get
            {
                return _lazyOrderLineStatusLookup == null ? null : _lazyOrderLineStatusLookup.Value;
            }
            set
            {
                _lazyOrderLineStatusLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool OrderLineStatusLookupIsLoaded
        {
            get
            {
                return _lazyOrderLineStatusLookup == null ? false : _lazyOrderLineStatusLookup.IsValueCreated;
            }
        }

        public void SetLazyOrderLineStatusLookup(Lazy<Lookup> lazyOrderLineStatusLookup)
        {
            _lazyOrderLineStatusLookup = lazyOrderLineStatusLookup;
        }

		#endregion

		#region ChangeTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyChangeTypeLookup;

        public Lookup ChangeTypeLookup
        {
            get
            {
                return _lazyChangeTypeLookup == null ? null : _lazyChangeTypeLookup.Value;
            }
            set
            {
                _lazyChangeTypeLookup = new Lazy<Lookup>(() => value);
            }
        }

        public bool ChangeTypeLookupIsLoaded
        {
            get
            {
                return _lazyChangeTypeLookup == null ? false : _lazyChangeTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyChangeTypeLookup(Lazy<Lookup> lazyChangeTypeLookup)
        {
            _lazyChangeTypeLookup = lazyChangeTypeLookup;
        }

		#endregion

        //#region ClaimOrderLine Lazy Loading Properties and Methods

        //private Lazy<List<ClaimOrderLine>> _lazyClaimOrderLineList;

        //public List<ClaimOrderLine> ClaimOrderLineList
        //{
        //    get
        //    {
        //        return _lazyClaimOrderLineList == null ? null : _lazyClaimOrderLineList.Value;
        //    }
        //}

        //public bool ClaimOrderLineListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyClaimOrderLineList == null ? false : _lazyClaimOrderLineList.IsValueCreated;
        //    }
        //}

        //public void SetLazyClaimOrderLineList(Lazy<List<ClaimOrderLine>> lazyClaimOrderLineList)
        //{
        //    _lazyClaimOrderLineList = lazyClaimOrderLineList;
        //}

        //#endregion

        //#region GrowerOrderReminder Lazy Loading Properties and Methods

        //private Lazy<List<GrowerOrderReminder>> _lazyGrowerOrderReminderList;

        //public List<GrowerOrderReminder> GrowerOrderReminderList
        //{
        //    get
        //    {
        //        return _lazyGrowerOrderReminderList == null ? null : _lazyGrowerOrderReminderList.Value;
        //    }
        //}

        //public bool GrowerOrderReminderListIsLoaded
        //{
        //    get
        //    {
        //        return _lazyGrowerOrderReminderList == null ? false : _lazyGrowerOrderReminderList.IsValueCreated;
        //    }
        //}

        //public void SetLazyGrowerOrderReminderList(Lazy<List<GrowerOrderReminder>> lazyGrowerOrderReminderList)
        //{
        //    _lazyGrowerOrderReminderList = lazyGrowerOrderReminderList;
        //}

        //#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "SupplierOrderGuid={0}",this.SupplierOrderGuid);
			result.Append(", ");
			result.AppendFormat( "ProductGuid={0}",this.ProductGuid);
			result.Append(", ");
			result.AppendFormat( "QtyOrdered={0}",this.QtyOrdered);
			result.Append(", ");
			result.AppendFormat( "QtyShipped={0}",this.QtyShipped);
			result.Append(", ");
			result.AppendFormat( "ActualPriceUsedGuid={0}",this.ActualPriceUsedGuid);
			result.Append(", ");
			result.AppendFormat( "ActualPrice={0}",this.ActualPrice);
			result.Append(", ");
			result.AppendFormat( "OrderLineStatusLookupGuid={0}",this.OrderLineStatusLookupGuid);
			result.Append(", ");
			result.AppendFormat( "ChangeTypeLookupGuid={0}",this.ChangeTypeLookupGuid);
			result.Append(", ");
			result.AppendFormat( "DateEntered={0}",this.DateEntered);
			result.Append(", ");
			result.AppendFormat( "DateLastChanged={0}",this.DateLastChanged);
			result.Append(", ");
			result.AppendFormat( "ActualCost={0}",this.ActualCost);
			result.Append(", ");
			result.AppendFormat( "ActualCostUsedGuid={0}",this.ActualCostUsedGuid);
            result.Append(", ");
            result.AppendFormat("DateQtyLastChanged={0}", this.DateQtyLastChanged);
            result.Append(", ");
            result.AppendFormat("LineNumber={0}", this.LineNumber);
            result.Append(", ");
            result.AppendFormat("LineComment={0}", this.LineComment);
            result.Append(", ");
            result.AppendFormat("ShipWeekCode={0}", this.ShipWeekCode);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
