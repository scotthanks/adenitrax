using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class GrowerAddress : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			DateDeactivated,
			GrowerGuid,
			AddressGuid,
			PhoneAreaCode,
			Phone,
			DefaultAddress,
			SpecialInstructions,
            SellerCustomerID,
		}

		new public enum ForeignKeyFieldEnum
		{
			GrowerGuid,
			AddressGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Grower),
						typeof(Address),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid GrowerGuid { get; set; }
		public Guid AddressGuid { get; set; }
		public Decimal PhoneAreaCode { get; set; }
		public Decimal Phone { get; set; }
		public bool DefaultAddress { get; set; }
        public string SpecialInstructions { get; set; }
        public string SellerCustomerID { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "GrowerGuid":
					value = GuidToString( GrowerGuid);
					break;
				case "AddressGuid":
					value = GuidToString( AddressGuid);
					break;
				case "PhoneAreaCode":
					value = DecimalToString( PhoneAreaCode);
					break;
				case "Phone":
					value = DecimalToString( Phone);
					break;
				case "DefaultAddress":
					value = BoolToString( DefaultAddress);
					break;
				case "SpecialInstructions":
					value = StringToString( SpecialInstructions);
					break;
                case "SellerCustomerID":
                    value = StringToString(SellerCustomerID);
                    break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "GrowerGuid":
					GrowerGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "AddressGuid":
					AddressGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "PhoneAreaCode":
					PhoneAreaCode = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "Phone":
					Phone = (Decimal)DecimalFromString( value, isNullable: false);
					break;
				case "DefaultAddress":
					DefaultAddress = (bool)BoolFromString( value, isNullable: false);
					break;
				case "SpecialInstructions":
					SpecialInstructions = (string)StringFromString( value, isNullable: false);
					break;
                case "SellerCustomerID":
                    SellerCustomerID = (string)StringFromString(value, isNullable: false);
                    break;
                default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "GrowerGuid":
						value = GrowerGuid;
						break;
					case "AddressGuid":
						value = AddressGuid;
						break;
					case "PhoneAreaCode":
						value = PhoneAreaCode;
						break;
					case "Phone":
						value = Phone;
						break;
					case "DefaultAddress":
						value = DefaultAddress;
						break;
					case "SpecialInstructions":
						value = SpecialInstructions;
						break;
                    case "SellerCustomerID":
                        value = SellerCustomerID;
                        break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "GrowerGuid":
						GrowerGuid = (Guid)value;
						break;
					case "AddressGuid":
						AddressGuid = (Guid)value;
						break;
					case "PhoneAreaCode":
						PhoneAreaCode = (Decimal)value;
						break;
					case "Phone":
						Phone = (Decimal)value;
						break;
					case "DefaultAddress":
						DefaultAddress = (bool)value;
						break;
					case "SpecialInstructions":
						SpecialInstructions = (string)value;
						break;
                    case "SellerCustomerID":
                        SellerCustomerID = (string)value;
                        break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
            set
            {
                _lazyGrower = new Lazy<Grower>(() => value);
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

		#endregion

		#region Address Lazy Loading Properties and Methods

        private Lazy<Address> _lazyAddress;

        public Address Address
        {
            get
            {
                return _lazyAddress == null ? null : _lazyAddress.Value;
            }
            set
            {
                _lazyAddress = new Lazy<Address>(() => value);
            }
        }

        public bool AddressIsLoaded
        {
            get
            {
                return _lazyAddress == null ? false : _lazyAddress.IsValueCreated;
            }
        }

        public void SetLazyAddress(Lazy<Address> lazyAddress)
        {
            _lazyAddress = lazyAddress;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "GrowerGuid={0}",this.GrowerGuid);
			result.Append(", ");
			result.AppendFormat( "AddressGuid={0}",this.AddressGuid);
			result.Append(", ");
			result.AppendFormat( "PhoneAreaCode={0}",this.PhoneAreaCode);
			result.Append(", ");
			result.AppendFormat( "Phone={0}",this.Phone);
			result.Append(", ");
			result.AppendFormat( "DefaultAddress={0}",this.DefaultAddress);
			result.Append(", ");
			result.AppendFormat( "SpecialInstructions={0}",this.SpecialInstructions);
            result.Append(", ");
            result.AppendFormat("SellerCustomerID={0}", this.SpecialInstructions);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
