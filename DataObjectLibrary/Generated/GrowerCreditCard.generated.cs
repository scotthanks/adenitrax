using System;
using System.Collections.Generic;
using System.Text;

// NOTE: Do not change this generated file. Your changes will be overwritten the next time code generation is done.

namespace DataObjectLibrary
{
	public partial class GrowerCreditCard : TableObjectBase
	{
		public enum ColumnEnum
		{
			Id,
			Guid,
			GrowerGuid,
			AuthorizeNetSerialNumber,
			CardDescription,
			DateDeactivated,
			DefaultCard,
			LastFourDigits,
			CardType,
		}

		new public enum ForeignKeyFieldEnum
		{
			GrowerGuid,
		}

		private static List<Type> _foreignKeyTableTypeList;

		public static List<Type> ForeignKeyTableTypeList
		{
			get
			{
				if (_foreignKeyTableTypeList == null)
				{
					_foreignKeyTableTypeList = new List<Type>
					{
						typeof(Grower),
					};
				}

				return _foreignKeyTableTypeList;
			}
		}

		public Guid GrowerGuid { get; set; }
		public string AuthorizeNetSerialNumber { get; set; }
		public string CardDescription { get; set; }
		public bool DefaultCard { get; set; }
		public string LastFourDigits { get; set; }
		public string CardType { get; set; }

		public override string GetFieldValueAsString( string fieldName)
		{
	        string value = null;
            switch (fieldName)
            {
				case "Id":
					value = LongToString( Id);
					break;
				case "Guid":
					value = GuidToString( Guid);
					break;
				case "GrowerGuid":
					value = GuidToString( GrowerGuid);
					break;
				case "AuthorizeNetSerialNumber":
					value = StringToString( AuthorizeNetSerialNumber);
					break;
				case "CardDescription":
					value = StringToString( CardDescription);
					break;
				case "DateDeactivated":
					value = DateTimeToString( DateDeactivated);
					break;
				case "DefaultCard":
					value = BoolToString( DefaultCard);
					break;
				case "LastFourDigits":
					value = StringToString( LastFourDigits);
					break;
				case "CardType":
					value = StringToString( CardType);
					break;
            }
	        return value;
		}

		public override bool ParseFieldFromString( string fieldName, string value)
		{
			bool valueSet = true;

            switch (fieldName)
            {
				case "Id":
					Id = (long)LongFromString( value, isNullable: false);
					break;
				case "Guid":
					Guid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "GrowerGuid":
					GrowerGuid = (Guid)GuidFromString( value, isNullable: false);
					break;
				case "AuthorizeNetSerialNumber":
					AuthorizeNetSerialNumber = StringFromString( value, isNullable: true);
					break;
				case "CardDescription":
					CardDescription = StringFromString( value, isNullable: true);
					break;
				case "DateDeactivated":
					DateDeactivated = DateTimeFromString( value, isNullable: true);
					break;
				case "DefaultCard":
					DefaultCard = (bool)BoolFromString( value, isNullable: false);
					break;
				case "LastFourDigits":
					LastFourDigits = StringFromString( value, isNullable: true);
					break;
				case "CardType":
					CardType = StringFromString( value, isNullable: true);
					break;
				default:
					valueSet = false;
					break;
            }

			return valueSet;
		}

	    public override object this[ string fieldName]
	    {
	        get
	        {
	            object value = null;
                switch (fieldName)
                {
					case "Id":
						value = Id;
						break;
					case "Guid":
						value = Guid;
						break;
					case "GrowerGuid":
						value = GrowerGuid;
						break;
					case "AuthorizeNetSerialNumber":
						value = AuthorizeNetSerialNumber;
						break;
					case "CardDescription":
						value = CardDescription;
						break;
					case "DateDeactivated":
						value = DateDeactivated;
						break;
					case "DefaultCard":
						value = DefaultCard;
						break;
					case "LastFourDigits":
						value = LastFourDigits;
						break;
					case "CardType":
						value = CardType;
						break;
                }
	            return value;
	        }
            set
            {
                switch (fieldName)
                {
					case "Id":
						Id = (long)value;
						break;
					case "Guid":
						Guid = (Guid)value;
						break;
					case "GrowerGuid":
						GrowerGuid = (Guid)value;
						break;
					case "AuthorizeNetSerialNumber":
						AuthorizeNetSerialNumber = (string)value;
						break;
					case "CardDescription":
						CardDescription = (string)value;
						break;
					case "DateDeactivated":
						DateDeactivated = (DateTime?)value;
						break;
					case "DefaultCard":
						DefaultCard = (bool)value;
						break;
					case "LastFourDigits":
						LastFourDigits = (string)value;
						break;
					case "CardType":
						CardType = (string)value;
						break;
                }
            }
	    }

		#region Lazy Loading Logic

		#region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
            set
            {
                _lazyGrower = new Lazy<Grower>(() => value);
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

		#endregion

		#region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<List<GrowerOrder>> _lazyGrowerOrderList;

        public List<GrowerOrder> GrowerOrderList
        {
            get
            {
                return _lazyGrowerOrderList == null ? null : _lazyGrowerOrderList.Value;
            }
        }

        public bool GrowerOrderListIsLoaded
        {
            get
            {
                return _lazyGrowerOrderList == null ? false : _lazyGrowerOrderList.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderList(Lazy<List<GrowerOrder>> lazyGrowerOrderList)
        {
            _lazyGrowerOrderList = lazyGrowerOrderList;
        }

		#endregion
		
		#endregion

		public override string ToString()
        {
            var result = new StringBuilder();
			result.AppendFormat( "Id={0}",this.Id);
			result.Append(", ");
			result.AppendFormat( "Guid={0}",this.Guid);
			result.Append(", ");
			result.AppendFormat( "GrowerGuid={0}",this.GrowerGuid);
			result.Append(", ");
			result.AppendFormat( "AuthorizeNetSerialNumber={0}",this.AuthorizeNetSerialNumber);
			result.Append(", ");
			result.AppendFormat( "CardDescription={0}",this.CardDescription);
			result.Append(", ");
			result.AppendFormat( "DateDeactivated={0}",this.DateDeactivated);
			result.Append(", ");
			result.AppendFormat( "DefaultCard={0}",this.DefaultCard);
			result.Append(", ");
			result.AppendFormat( "LastFourDigits={0}",this.LastFourDigits);
			result.Append(", ");
			result.AppendFormat( "CardType={0}",this.CardType);

            return result.ToString();
        }

		new protected static bool GetHasIdColumn() { return true; }
		new protected static bool GetHasGuidColumn() { return true; }
		new protected static bool GetHasDateDeactivatedColumn() { return true; }
		new protected static bool GetHasCodeColumn() { return false; }
		new protected static bool GetHasNameColumn() { return false; }
		new protected static bool GetDateDeactivatedIsNullable() { return true; }
	}
}
