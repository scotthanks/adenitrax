﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace PrimitiveObjectLibrary
{
    public class Person : PrimitiveObjectBase
    {
        public Person() { }

        public Person(Guid guid)
        {
        
            //ToDo: use guid to run Sproc to get reader with that Person and use LoadFromSqlDataReader to load that.
        }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
