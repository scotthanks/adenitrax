﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary;

namespace DataObjectLibrary
{
    partial class ImageFile
    {
        private const string EpsStorageUrlAppSettingName = "EpsStorageUrl";

        public enum  ImageContainerNameEnum
        {
            CatalogImages,
        }

        public string GetImageUrl( ImageContainerNameEnum imageContainerNameEnum)
        {
            return GetImageUrl(imageContainerNameEnum, this.Name.Trim(), FileExtensionLookup.Code);
        }

        public static string GetImageUrl(ImageContainerNameEnum imageContainerNameEnum, string imageName, string extension)
        {
            return string.Format("{0}/{1}.{2}", GetImagePath(ImageContainerNameEnum.CatalogImages), imageName, extension.ToLower());
        }

        private static string GetImagePath(ImageFile.ImageContainerNameEnum imageContainerNameEnum)
        {
            var imageContainerObject = ConfigurationManager.AppSettings[EpsStorageUrlAppSettingName];
            if (imageContainerObject == null)
            {
                throw new ApplicationException(string.Format("There is no AppSetting with a name of {0} in the config file.", EpsStorageUrlAppSettingName));
            }

            return string.Format("{0}/{1}", imageContainerObject.ToString(CultureInfo.InvariantCulture), imageContainerNameEnum.ToString().ToLower());
        }
    }
}
