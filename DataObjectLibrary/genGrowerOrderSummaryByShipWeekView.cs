﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DataObjectLibrary
{
    public partial class GrowerOrderSummaryByShipWeekView : DataObjectBase
    {
        public enum ColumnEnum
        {
            GrowerOrderGuid,
            GrowerGuid,
            OrderTypeLookupGuid,
            OrderTypeLookupCode,
            ProgramTypeGuid,
            ProgramTypeCode,
            ProductFormCategoryGuid,
            ProductFormCategoryCode,
            ShipWeekGuid,
            ShipWeekCode,
            SupplierOrderCount,
            SupplierOrderWithOrderLinesCount,
            ProductCount,
            ProductWithNonZeroOrderQtyCount,
            OrderLineCount,
            OrderLineWithNonZeroOrderQtyCount,
            QtyOrderedCount,
            GrowerOrderStatusLookupGuid,
            GrowerOrderStatusLookupCode,
            LowestSupplierOrderStatusLookupGuid,
            LowestSupplierOrderStatusLookupCode,
            LowestOrderLineStatusLookupGuid,
            LowestOrderLineStatusLookupCode,
            PersonGuid,
            CountOfPendingStatusLines,
            LowestOrderStatusLookupName,
            GrowerOrderStatusLookupName,
            InvoiceNo,
        }

        new public enum ForeignKeyFieldEnum
        {
            GrowerOrderGuid,
            GrowerGuid,
            OrderTypeLookupGuid,
            ProgramTypeGuid,
            ProductFormCategoryGuid,
            ShipWeekGuid,
            GrowerOrderStatusLookupGuid,
            LowestSupplierOrderStatusLookupGuid,
            LowestOrderLineStatusLookupGuid,
            PersonGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(GrowerOrder),
                        typeof(Grower),
                        typeof(Lookup),
                        typeof(ProgramType),
                        typeof(ProductFormCategory),
                        typeof(ShipWeek),
                        typeof(Lookup),
                        typeof(Lookup),
                        typeof(Lookup),
                        typeof(Person),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Guid? GrowerOrderGuid { get; set; }
        public Guid? GrowerGuid { get; set; }
        public Guid? OrderTypeLookupGuid { get; set; }
        public string OrderTypeLookupCode { get; set; }
        public Guid ProgramTypeGuid { get; set; }
        public string ProgramTypeCode { get; set; }
        public Guid ProductFormCategoryGuid { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public Guid? ShipWeekGuid { get; set; }
        public string ShipWeekCode { get; set; }
        public int SupplierOrderCount { get; set; }
        public int SupplierOrderWithOrderLinesCount { get; set; }
        public int ProductCount { get; set; }
        public int ProductWithNonZeroOrderQtyCount { get; set; }
        public int OrderLineCount { get; set; }
        public int? OrderLineWithNonZeroOrderQtyCount { get; set; }
        public int QtyOrderedCount { get; set; }
        public Guid? GrowerOrderStatusLookupGuid { get; set; }
        public string GrowerOrderStatusLookupCode { get; set; }
        public Guid LowestSupplierOrderStatusLookupGuid { get; set; }
        public string LowestSupplierOrderStatusLookupCode { get; set; }
        public Guid LowestOrderLineStatusLookupGuid { get; set; }
        public string LowestOrderLineStatusLookupCode { get; set; }
        public Guid? PersonGuid { get; set; }
        public int? CountOfPendingStatusLines { get; set; }
        public string LowestOrderStatusLookupName { get; set; }
        public string GrowerOrderStatusLookupName { get; set; }
        public string InvoiceNo { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "GrowerOrderGuid":
                    value = GuidToString(GrowerOrderGuid);
                    break;
                case "GrowerGuid":
                    value = GuidToString(GrowerGuid);
                    break;
                case "OrderTypeLookupGuid":
                    value = GuidToString(OrderTypeLookupGuid);
                    break;
                case "OrderTypeLookupCode":
                    value = StringToString(OrderTypeLookupCode);
                    break;
                case "ProgramTypeGuid":
                    value = GuidToString(ProgramTypeGuid);
                    break;
                case "ProgramTypeCode":
                    value = StringToString(ProgramTypeCode);
                    break;
                case "ProductFormCategoryGuid":
                    value = GuidToString(ProductFormCategoryGuid);
                    break;
                case "ProductFormCategoryCode":
                    value = StringToString(ProductFormCategoryCode);
                    break;
                case "ShipWeekGuid":
                    value = GuidToString(ShipWeekGuid);
                    break;
                case "ShipWeekCode":
                    value = StringToString(ShipWeekCode);
                    break;
                case "SupplierOrderCount":
                    value = IntToString(SupplierOrderCount);
                    break;
                case "SupplierOrderWithOrderLinesCount":
                    value = IntToString(SupplierOrderWithOrderLinesCount);
                    break;
                case "ProductCount":
                    value = IntToString(ProductCount);
                    break;
                case "ProductWithNonZeroOrderQtyCount":
                    value = IntToString(ProductWithNonZeroOrderQtyCount);
                    break;
                case "OrderLineCount":
                    value = IntToString(OrderLineCount);
                    break;
                case "OrderLineWithNonZeroOrderQtyCount":
                    value = IntToString(OrderLineWithNonZeroOrderQtyCount);
                    break;
                case "QtyOrderedCount":
                    value = IntToString(QtyOrderedCount);
                    break;
                case "GrowerOrderStatusLookupGuid":
                    value = GuidToString(GrowerOrderStatusLookupGuid);
                    break;
                case "GrowerOrderStatusLookupCode":
                    value = StringToString(GrowerOrderStatusLookupCode);
                    break;
                case "LowestSupplierOrderStatusLookupGuid":
                    value = GuidToString(LowestSupplierOrderStatusLookupGuid);
                    break;
                case "LowestSupplierOrderStatusLookupCode":
                    value = StringToString(LowestSupplierOrderStatusLookupCode);
                    break;
                case "LowestOrderLineStatusLookupGuid":
                    value = GuidToString(LowestOrderLineStatusLookupGuid);
                    break;
                case "LowestOrderLineStatusLookupCode":
                    value = StringToString(LowestOrderLineStatusLookupCode);
                    break;
                case "PersonGuid":
                    value = GuidToString(PersonGuid);
                    break;
                case "CountOfPendingStatusLines":
                    value = IntToString(CountOfPendingStatusLines);
                    break;
                case "LowestOrderStatusLookupName":
                    value = StringToString(LowestOrderStatusLookupName);
                    break;
                case "GrowerOrderStatusLookupName":
                    value = StringToString(GrowerOrderStatusLookupName);
                    break;
                case "InvoiceNo":
                    value = StringToString(InvoiceNo);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "GrowerOrderGuid":
                    GrowerOrderGuid = GuidFromString(value, isNullable: true);
                    break;
                case "GrowerGuid":
                    GrowerGuid = GuidFromString(value, isNullable: true);
                    break;
                case "OrderTypeLookupGuid":
                    OrderTypeLookupGuid = GuidFromString(value, isNullable: true);
                    break;
                case "OrderTypeLookupCode":
                    OrderTypeLookupCode = StringFromString(value, isNullable: true);
                    break;
                case "ProgramTypeGuid":
                    ProgramTypeGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProgramTypeCode":
                    ProgramTypeCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "ProductFormCategoryGuid":
                    ProductFormCategoryGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ProductFormCategoryCode":
                    ProductFormCategoryCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "ShipWeekGuid":
                    ShipWeekGuid = GuidFromString(value, isNullable: true);
                    break;
                case "ShipWeekCode":
                    ShipWeekCode = StringFromString(value, isNullable: true);
                    break;
                case "SupplierOrderCount":
                    SupplierOrderCount = (int)IntFromString(value, isNullable: false);
                    break;
                case "SupplierOrderWithOrderLinesCount":
                    SupplierOrderWithOrderLinesCount = (int)IntFromString(value, isNullable: false);
                    break;
                case "ProductCount":
                    ProductCount = (int)IntFromString(value, isNullable: false);
                    break;
                case "ProductWithNonZeroOrderQtyCount":
                    ProductWithNonZeroOrderQtyCount = (int)IntFromString(value, isNullable: false);
                    break;
                case "OrderLineCount":
                    OrderLineCount = (int)IntFromString(value, isNullable: false);
                    break;
                case "OrderLineWithNonZeroOrderQtyCount":
                    OrderLineWithNonZeroOrderQtyCount = IntFromString(value, isNullable: true);
                    break;
                case "QtyOrderedCount":
                    QtyOrderedCount = (int)IntFromString(value, isNullable: false);
                    break;
                case "GrowerOrderStatusLookupGuid":
                    GrowerOrderStatusLookupGuid = GuidFromString(value, isNullable: true);
                    break;
                case "GrowerOrderStatusLookupCode":
                    GrowerOrderStatusLookupCode = StringFromString(value, isNullable: true);
                    break;
                case "LowestSupplierOrderStatusLookupGuid":
                    LowestSupplierOrderStatusLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "LowestSupplierOrderStatusLookupCode":
                    LowestSupplierOrderStatusLookupCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "LowestOrderLineStatusLookupGuid":
                    LowestOrderLineStatusLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "LowestOrderLineStatusLookupCode":
                    LowestOrderLineStatusLookupCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "PersonGuid":
                    PersonGuid = GuidFromString(value, isNullable: true);
                    break;
                case "CountOfPendingStatusLines":
                    CountOfPendingStatusLines = IntFromString(value, isNullable: true);
                    break;
                case "LowestOrderStatusLookupName":
                    LowestOrderStatusLookupName = (string)StringFromString(value, isNullable: false);
                    break;
                case "GrowerOrderStatusLookupName":
                    GrowerOrderStatusLookupName = (string)StringFromString(value, isNullable: false);
                    break;
                case "InvoiceNo":
                    InvoiceNo = (string)StringFromString(value, isNullable: false);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "GrowerOrderGuid":
                        value = GrowerOrderGuid;
                        break;
                    case "GrowerGuid":
                        value = GrowerGuid;
                        break;
                    case "OrderTypeLookupGuid":
                        value = OrderTypeLookupGuid;
                        break;
                    case "OrderTypeLookupCode":
                        value = OrderTypeLookupCode;
                        break;
                    case "ProgramTypeGuid":
                        value = ProgramTypeGuid;
                        break;
                    case "ProgramTypeCode":
                        value = ProgramTypeCode;
                        break;
                    case "ProductFormCategoryGuid":
                        value = ProductFormCategoryGuid;
                        break;
                    case "ProductFormCategoryCode":
                        value = ProductFormCategoryCode;
                        break;
                    case "ShipWeekGuid":
                        value = ShipWeekGuid;
                        break;
                    case "ShipWeekCode":
                        value = ShipWeekCode;
                        break;
                    case "SupplierOrderCount":
                        value = SupplierOrderCount;
                        break;
                    case "SupplierOrderWithOrderLinesCount":
                        value = SupplierOrderWithOrderLinesCount;
                        break;
                    case "ProductCount":
                        value = ProductCount;
                        break;
                    case "ProductWithNonZeroOrderQtyCount":
                        value = ProductWithNonZeroOrderQtyCount;
                        break;
                    case "OrderLineCount":
                        value = OrderLineCount;
                        break;
                    case "OrderLineWithNonZeroOrderQtyCount":
                        value = OrderLineWithNonZeroOrderQtyCount;
                        break;
                    case "QtyOrderedCount":
                        value = QtyOrderedCount;
                        break;
                    case "GrowerOrderStatusLookupGuid":
                        value = GrowerOrderStatusLookupGuid;
                        break;
                    case "GrowerOrderStatusLookupCode":
                        value = GrowerOrderStatusLookupCode;
                        break;
                    case "LowestSupplierOrderStatusLookupGuid":
                        value = LowestSupplierOrderStatusLookupGuid;
                        break;
                    case "LowestSupplierOrderStatusLookupCode":
                        value = LowestSupplierOrderStatusLookupCode;
                        break;
                    case "LowestOrderLineStatusLookupGuid":
                        value = LowestOrderLineStatusLookupGuid;
                        break;
                    case "LowestOrderLineStatusLookupCode":
                        value = LowestOrderLineStatusLookupCode;
                        break;
                    case "PersonGuid":
                        value = PersonGuid;
                        break;
                    case "CountOfPendingStatusLines":
                        value = CountOfPendingStatusLines;
                        break;
                    case "LowestOrderStatusLookupName":
                        value = LowestOrderStatusLookupName;
                        break;
                    case "GrowerOrderStatusLookupName":
                        value = GrowerOrderStatusLookupName;
                        break;
                    case "InvoiceNo":
                        value = InvoiceNo;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "GrowerOrderGuid":
                        GrowerOrderGuid = (Guid?)value;
                        break;
                    case "GrowerGuid":
                        GrowerGuid = (Guid?)value;
                        break;
                    case "OrderTypeLookupGuid":
                        OrderTypeLookupGuid = (Guid?)value;
                        break;
                    case "OrderTypeLookupCode":
                        OrderTypeLookupCode = (string)value;
                        break;
                    case "ProgramTypeGuid":
                        ProgramTypeGuid = (Guid)value;
                        break;
                    case "ProgramTypeCode":
                        ProgramTypeCode = (string)value;
                        break;
                    case "ProductFormCategoryGuid":
                        ProductFormCategoryGuid = (Guid)value;
                        break;
                    case "ProductFormCategoryCode":
                        ProductFormCategoryCode = (string)value;
                        break;
                    case "ShipWeekGuid":
                        ShipWeekGuid = (Guid?)value;
                        break;
                    case "ShipWeekCode":
                        ShipWeekCode = (string)value;
                        break;
                    case "SupplierOrderCount":
                        SupplierOrderCount = (int)value;
                        break;
                    case "SupplierOrderWithOrderLinesCount":
                        SupplierOrderWithOrderLinesCount = (int)value;
                        break;
                    case "ProductCount":
                        ProductCount = (int)value;
                        break;
                    case "ProductWithNonZeroOrderQtyCount":
                        ProductWithNonZeroOrderQtyCount = (int)value;
                        break;
                    case "OrderLineCount":
                        OrderLineCount = (int)value;
                        break;
                    case "OrderLineWithNonZeroOrderQtyCount":
                        OrderLineWithNonZeroOrderQtyCount = (int?)value;
                        break;
                    case "QtyOrderedCount":
                        QtyOrderedCount = (int)value;
                        break;
                    case "GrowerOrderStatusLookupGuid":
                        GrowerOrderStatusLookupGuid = (Guid?)value;
                        break;
                    case "GrowerOrderStatusLookupCode":
                        GrowerOrderStatusLookupCode = (string)value;
                        break;
                    case "LowestSupplierOrderStatusLookupGuid":
                        LowestSupplierOrderStatusLookupGuid = (Guid)value;
                        break;
                    case "LowestSupplierOrderStatusLookupCode":
                        LowestSupplierOrderStatusLookupCode = (string)value;
                        break;
                    case "LowestOrderLineStatusLookupGuid":
                        LowestOrderLineStatusLookupGuid = (Guid)value;
                        break;
                    case "LowestOrderLineStatusLookupCode":
                        LowestOrderLineStatusLookupCode = (string)value;
                        break;
                    case "PersonGuid":
                        PersonGuid = (Guid?)value;
                        break;
                    case "CountOfPendingStatusLines":
                        CountOfPendingStatusLines = (int?)value;
                        break;
                    case "LowestOrderStatusLookupName":
                        LowestOrderStatusLookupName = (string)value;
                        break;
                    case "GrowerOrderStatusLookupName":
                        GrowerOrderStatusLookupName = (string)value;
                        break;
                    case "InvoiceNo":
                        InvoiceNo = (string)value;
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("GrowerOrderGuid={0}", this.GrowerOrderGuid);
            result.Append(", ");
            result.AppendFormat("GrowerGuid={0}", this.GrowerGuid);
            result.Append(", ");
            result.AppendFormat("OrderTypeLookupGuid={0}", this.OrderTypeLookupGuid);
            result.Append(", ");
            result.AppendFormat("OrderTypeLookupCode={0}", this.OrderTypeLookupCode);
            result.Append(", ");
            result.AppendFormat("ProgramTypeGuid={0}", this.ProgramTypeGuid);
            result.Append(", ");
            result.AppendFormat("ProgramTypeCode={0}", this.ProgramTypeCode);
            result.Append(", ");
            result.AppendFormat("ProductFormCategoryGuid={0}", this.ProductFormCategoryGuid);
            result.Append(", ");
            result.AppendFormat("ProductFormCategoryCode={0}", this.ProductFormCategoryCode);
            result.Append(", ");
            result.AppendFormat("ShipWeekGuid={0}", this.ShipWeekGuid);
            result.Append(", ");
            result.AppendFormat("ShipWeekCode={0}", this.ShipWeekCode);
            result.Append(", ");
            result.AppendFormat("SupplierOrderCount={0}", this.SupplierOrderCount);
            result.Append(", ");
            result.AppendFormat("SupplierOrderWithOrderLinesCount={0}", this.SupplierOrderWithOrderLinesCount);
            result.Append(", ");
            result.AppendFormat("ProductCount={0}", this.ProductCount);
            result.Append(", ");
            result.AppendFormat("ProductWithNonZeroOrderQtyCount={0}", this.ProductWithNonZeroOrderQtyCount);
            result.Append(", ");
            result.AppendFormat("OrderLineCount={0}", this.OrderLineCount);
            result.Append(", ");
            result.AppendFormat("OrderLineWithNonZeroOrderQtyCount={0}", this.OrderLineWithNonZeroOrderQtyCount);
            result.Append(", ");
            result.AppendFormat("QtyOrderedCount={0}", this.QtyOrderedCount);
            result.Append(", ");
            result.AppendFormat("GrowerOrderStatusLookupGuid={0}", this.GrowerOrderStatusLookupGuid);
            result.Append(", ");
            result.AppendFormat("GrowerOrderStatusLookupCode={0}", this.GrowerOrderStatusLookupCode);
            result.Append(", ");
            result.AppendFormat("LowestSupplierOrderStatusLookupGuid={0}", this.LowestSupplierOrderStatusLookupGuid);
            result.Append(", ");
            result.AppendFormat("LowestSupplierOrderStatusLookupCode={0}", this.LowestSupplierOrderStatusLookupCode);
            result.Append(", ");
            result.AppendFormat("LowestOrderLineStatusLookupGuid={0}", this.LowestOrderLineStatusLookupGuid);
            result.Append(", ");
            result.AppendFormat("LowestOrderLineStatusLookupCode={0}", this.LowestOrderLineStatusLookupCode);
            result.Append(", ");
            result.AppendFormat("PersonGuid={0}", this.PersonGuid);
            result.Append(", ");
            result.AppendFormat("CountOfPendingStatusLines={0}", this.CountOfPendingStatusLines);
            result.Append(", ");
            result.AppendFormat("LowestOrderStatusLookupName={0}", this.LowestOrderStatusLookupName);
            result.Append(", ");
            result.AppendFormat("GrowerOrderStatusLookupName={0}", this.GrowerOrderStatusLookupName);
            result.Append(", ");
            result.AppendFormat("InvoiceNo={0}", this.InvoiceNo);

            return result.ToString();
        }

        #region Lazy Loading Logic

        #region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<GrowerOrder> _lazyGrowerOrder;

        public GrowerOrder GrowerOrder
        {
            get
            {
                return _lazyGrowerOrder == null ? null : _lazyGrowerOrder.Value;
            }
        }

        public bool GrowerOrderIsLoaded
        {
            get
            {
                return _lazyGrowerOrder == null ? false : _lazyGrowerOrder.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrder(Lazy<GrowerOrder> lazyGrowerOrder)
        {
            _lazyGrowerOrder = lazyGrowerOrder;
        }

        #endregion

        #region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

        #endregion

        #region OrderTypeLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyOrderTypeLookup;

        public Lookup OrderTypeLookup
        {
            get
            {
                return _lazyOrderTypeLookup == null ? null : _lazyOrderTypeLookup.Value;
            }
        }

        public bool OrderTypeLookupIsLoaded
        {
            get
            {
                return _lazyOrderTypeLookup == null ? false : _lazyOrderTypeLookup.IsValueCreated;
            }
        }

        public void SetLazyOrderTypeLookup(Lazy<Lookup> lazyOrderTypeLookup)
        {
            _lazyOrderTypeLookup = lazyOrderTypeLookup;
        }

        #endregion

        #region ProgramType Lazy Loading Properties and Methods

        private Lazy<ProgramType> _lazyProgramType;

        public ProgramType ProgramType
        {
            get
            {
                return _lazyProgramType == null ? null : _lazyProgramType.Value;
            }
        }

        public bool ProgramTypeIsLoaded
        {
            get
            {
                return _lazyProgramType == null ? false : _lazyProgramType.IsValueCreated;
            }
        }

        public void SetLazyProgramType(Lazy<ProgramType> lazyProgramType)
        {
            _lazyProgramType = lazyProgramType;
        }

        #endregion

        #region ProductFormCategory Lazy Loading Properties and Methods

        private Lazy<ProductFormCategory> _lazyProductFormCategory;

        public ProductFormCategory ProductFormCategory
        {
            get
            {
                return _lazyProductFormCategory == null ? null : _lazyProductFormCategory.Value;
            }
        }

        public bool ProductFormCategoryIsLoaded
        {
            get
            {
                return _lazyProductFormCategory == null ? false : _lazyProductFormCategory.IsValueCreated;
            }
        }

        public void SetLazyProductFormCategory(Lazy<ProductFormCategory> lazyProductFormCategory)
        {
            _lazyProductFormCategory = lazyProductFormCategory;
        }

        #endregion

        #region ShipWeek Lazy Loading Properties and Methods

        private Lazy<ShipWeek> _lazyShipWeek;

        public ShipWeek ShipWeek
        {
            get
            {
                return _lazyShipWeek == null ? null : _lazyShipWeek.Value;
            }
        }

        public bool ShipWeekIsLoaded
        {
            get
            {
                return _lazyShipWeek == null ? false : _lazyShipWeek.IsValueCreated;
            }
        }

        public void SetLazyShipWeek(Lazy<ShipWeek> lazyShipWeek)
        {
            _lazyShipWeek = lazyShipWeek;
        }

        #endregion

        #region GrowerOrderStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyGrowerOrderStatusLookup;

        public Lookup GrowerOrderStatusLookup
        {
            get
            {
                return _lazyGrowerOrderStatusLookup == null ? null : _lazyGrowerOrderStatusLookup.Value;
            }
        }

        public bool GrowerOrderStatusLookupIsLoaded
        {
            get
            {
                return _lazyGrowerOrderStatusLookup == null ? false : _lazyGrowerOrderStatusLookup.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrderStatusLookup(Lazy<Lookup> lazyGrowerOrderStatusLookup)
        {
            _lazyGrowerOrderStatusLookup = lazyGrowerOrderStatusLookup;
        }

        #endregion

        #region LowestSupplierOrderStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyLowestSupplierOrderStatusLookup;

        public Lookup LowestSupplierOrderStatusLookup
        {
            get
            {
                return _lazyLowestSupplierOrderStatusLookup == null ? null : _lazyLowestSupplierOrderStatusLookup.Value;
            }
        }

        public bool LowestSupplierOrderStatusLookupIsLoaded
        {
            get
            {
                return _lazyLowestSupplierOrderStatusLookup == null ? false : _lazyLowestSupplierOrderStatusLookup.IsValueCreated;
            }
        }

        public void SetLazyLowestSupplierOrderStatusLookup(Lazy<Lookup> lazyLowestSupplierOrderStatusLookup)
        {
            _lazyLowestSupplierOrderStatusLookup = lazyLowestSupplierOrderStatusLookup;
        }

        #endregion

        #region LowestOrderLineStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyLowestOrderLineStatusLookup;

        public Lookup LowestOrderLineStatusLookup
        {
            get
            {
                return _lazyLowestOrderLineStatusLookup == null ? null : _lazyLowestOrderLineStatusLookup.Value;
            }
        }

        public bool LowestOrderLineStatusLookupIsLoaded
        {
            get
            {
                return _lazyLowestOrderLineStatusLookup == null ? false : _lazyLowestOrderLineStatusLookup.IsValueCreated;
            }
        }

        public void SetLazyLowestOrderLineStatusLookup(Lazy<Lookup> lazyLowestOrderLineStatusLookup)
        {
            _lazyLowestOrderLineStatusLookup = lazyLowestOrderLineStatusLookup;
        }

        #endregion

        #region Person Lazy Loading Properties and Methods

        private Lazy<Person> _lazyPerson;

        public Person Person
        {
            get
            {
                return _lazyPerson == null ? null : _lazyPerson.Value;
            }
        }

        public bool PersonIsLoaded
        {
            get
            {
                return _lazyPerson == null ? false : _lazyPerson.IsValueCreated;
            }
        }

        public void SetLazyPerson(Lazy<Person> lazyPerson)
        {
            _lazyPerson = lazyPerson;
        }

        #endregion

        #endregion
    }
}

