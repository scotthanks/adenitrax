﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DataObjectLibrary
{
    public partial class LookupView : DataObjectBase
    {
        public enum ColumnEnum
        {
            LookupGuid,
            LookupId,
            LookupCode,
            LookupName,
            SortSequence,
            Path,
            ParentLookupGuid,
            ParentLookupCode,
            ParentLookupName,
            ParentLookupPath,
            SortKey,
        }

        new public enum ForeignKeyFieldEnum
        {
            LookupGuid,
            ParentLookupGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        typeof(Lookup),
                        typeof(Lookup),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public Guid LookupGuid { get; set; }
        public long LookupId { get; set; }
        public string LookupCode { get; set; }
        public string LookupName { get; set; }
        public Double SortSequence { get; set; }
        public string Path { get; set; }
        public Guid? ParentLookupGuid { get; set; }
        public string ParentLookupCode { get; set; }
        public string ParentLookupName { get; set; }
        public string ParentLookupPath { get; set; }
        public string SortKey { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "LookupGuid":
                    value = GuidToString(LookupGuid);
                    break;
                case "LookupId":
                    value = LongToString(LookupId);
                    break;
                case "LookupCode":
                    value = StringToString(LookupCode);
                    break;
                case "LookupName":
                    value = StringToString(LookupName);
                    break;
                case "SortSequence":
                    value = DoubleToString(SortSequence);
                    break;
                case "Path":
                    value = StringToString(Path);
                    break;
                case "ParentLookupGuid":
                    value = GuidToString(ParentLookupGuid);
                    break;
                case "ParentLookupCode":
                    value = StringToString(ParentLookupCode);
                    break;
                case "ParentLookupName":
                    value = StringToString(ParentLookupName);
                    break;
                case "ParentLookupPath":
                    value = StringToString(ParentLookupPath);
                    break;
                case "SortKey":
                    value = StringToString(SortKey);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "LookupGuid":
                    LookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "LookupId":
                    LookupId = (long)LongFromString(value, isNullable: false);
                    break;
                case "LookupCode":
                    LookupCode = (string)StringFromString(value, isNullable: false);
                    break;
                case "LookupName":
                    LookupName = (string)StringFromString(value, isNullable: false);
                    break;
                case "SortSequence":
                    SortSequence = (Double)DoubleFromString(value, isNullable: false);
                    break;
                case "Path":
                    Path = StringFromString(value, isNullable: true);
                    break;
                case "ParentLookupGuid":
                    ParentLookupGuid = GuidFromString(value, isNullable: true);
                    break;
                case "ParentLookupCode":
                    ParentLookupCode = StringFromString(value, isNullable: true);
                    break;
                case "ParentLookupName":
                    ParentLookupName = StringFromString(value, isNullable: true);
                    break;
                case "ParentLookupPath":
                    ParentLookupPath = StringFromString(value, isNullable: true);
                    break;
                case "SortKey":
                    SortKey = StringFromString(value, isNullable: true);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "LookupGuid":
                        value = LookupGuid;
                        break;
                    case "LookupId":
                        value = LookupId;
                        break;
                    case "LookupCode":
                        value = LookupCode;
                        break;
                    case "LookupName":
                        value = LookupName;
                        break;
                    case "SortSequence":
                        value = SortSequence;
                        break;
                    case "Path":
                        value = Path;
                        break;
                    case "ParentLookupGuid":
                        value = ParentLookupGuid;
                        break;
                    case "ParentLookupCode":
                        value = ParentLookupCode;
                        break;
                    case "ParentLookupName":
                        value = ParentLookupName;
                        break;
                    case "ParentLookupPath":
                        value = ParentLookupPath;
                        break;
                    case "SortKey":
                        value = SortKey;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "LookupGuid":
                        LookupGuid = (Guid)value;
                        break;
                    case "LookupId":
                        LookupId = (long)value;
                        break;
                    case "LookupCode":
                        LookupCode = (string)value;
                        break;
                    case "LookupName":
                        LookupName = (string)value;
                        break;
                    case "SortSequence":
                        SortSequence = (Double)value;
                        break;
                    case "Path":
                        Path = (string)value;
                        break;
                    case "ParentLookupGuid":
                        ParentLookupGuid = (Guid?)value;
                        break;
                    case "ParentLookupCode":
                        ParentLookupCode = (string)value;
                        break;
                    case "ParentLookupName":
                        ParentLookupName = (string)value;
                        break;
                    case "ParentLookupPath":
                        ParentLookupPath = (string)value;
                        break;
                    case "SortKey":
                        SortKey = (string)value;
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("LookupGuid={0}", this.LookupGuid);
            result.Append(", ");
            result.AppendFormat("LookupId={0}", this.LookupId);
            result.Append(", ");
            result.AppendFormat("LookupCode={0}", this.LookupCode);
            result.Append(", ");
            result.AppendFormat("LookupName={0}", this.LookupName);
            result.Append(", ");
            result.AppendFormat("SortSequence={0}", this.SortSequence);
            result.Append(", ");
            result.AppendFormat("Path={0}", this.Path);
            result.Append(", ");
            result.AppendFormat("ParentLookupGuid={0}", this.ParentLookupGuid);
            result.Append(", ");
            result.AppendFormat("ParentLookupCode={0}", this.ParentLookupCode);
            result.Append(", ");
            result.AppendFormat("ParentLookupName={0}", this.ParentLookupName);
            result.Append(", ");
            result.AppendFormat("ParentLookupPath={0}", this.ParentLookupPath);
            result.Append(", ");
            result.AppendFormat("SortKey={0}", this.SortKey);

            return result.ToString();
        }

        #region Lazy Loading Logic

        #region Lookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyLookup;

        public Lookup Lookup
        {
            get
            {
                return _lazyLookup == null ? null : _lazyLookup.Value;
            }
        }

        public bool LookupIsLoaded
        {
            get
            {
                return _lazyLookup == null ? false : _lazyLookup.IsValueCreated;
            }
        }

        public void SetLazyLookup(Lazy<Lookup> lazyLookup)
        {
            _lazyLookup = lazyLookup;
        }

        #endregion

        #region ParentLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyParentLookup;

        public Lookup ParentLookup
        {
            get
            {
                return _lazyParentLookup == null ? null : _lazyParentLookup.Value;
            }
        }

        public bool ParentLookupIsLoaded
        {
            get
            {
                return _lazyParentLookup == null ? false : _lazyParentLookup.IsValueCreated;
            }
        }

        public void SetLazyParentLookup(Lazy<Lookup> lazyParentLookup)
        {
            _lazyParentLookup = lazyParentLookup;
        }

        #endregion

        #endregion
    }
}
