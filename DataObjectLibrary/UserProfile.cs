﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public class UserProfile
    {
        public enum ColumnEnum
        {
            UserId,
            UserName,
            EmailAddress,
            UserGuid,
            GrowerGuid
        }

        public enum RoleEnum
        {
            Administrator = 1,
            CustomerAdministrator = 2,
            CustomerOrderer = 3,
            CustomerReadOnly = 4,
            CustomerReadOnlyNoPrice = 5
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public Guid UserGuid { get; set; }
        public Guid GrowerGuid { get; set; }
    }
}
