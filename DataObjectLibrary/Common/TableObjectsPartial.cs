﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public partial class TableObjects
    {
        public static string[] TypeNameList
        {
            get { return Enum.GetNames(typeof (TableObjects.TypeEnum)); }
        }

        public static int GetIndexOf(Type type)
        {
            if (!TypeList.Contains(type))
            {
                throw new ApplicationException(string.Format( "The type {0} is not a DataObject.", type.Name));
            }

            return TypeList.IndexOf(type);
        }

        public static bool IsForeignKeyOf(Type type, string fieldName)
        {
            return TryGetForeignKeyTypeNameOf(type, fieldName) != null;
        }

        public static Type GetForeignKeyTypeOf(Type type, string fieldName)
        {
            string typeName = TryGetForeignKeyTypeNameOf(type, fieldName);

            if (typeName == null)
            {
                throw new ApplicationException(string.Format( "The field {0} is not a foreign key of {1}.", fieldName, type.GetType().Name));
            }

            return Type.GetType(typeName);
        }
        
        private static string TryGetForeignKeyTypeNameOf(Type type, string fieldName)
        {
            string foreignKeyTypeName = TryGetGuidForeignKeyTypeNameOf(type, fieldName);
            if (foreignKeyTypeName == null)
            {
                foreignKeyTypeName = TryGetCodeForeignKeyTypeNameOf(type, fieldName);
            }

            return foreignKeyTypeName;
        }

        private static string TryGetGuidForeignKeyTypeNameOf(Type type, string fieldName)
        {
            return TryGetForeignKeyTypeNameOf(type, fieldName, "Guid");
        }

        private static string TryGetCodeForeignKeyTypeNameOf(Type type, string fieldName)
        {
            return TryGetForeignKeyTypeNameOf(type, fieldName, "Code");
        }

        private static string TryGetForeignKeyTypeNameOf(Type type, string fieldName, string keyType)
        {
            int lowestIndex = int.MaxValue;
            string foreignKeyTypeName = null;

            foreach (string typeName in TypeNameList)
            {
                string foreignKeyEnding = typeName + keyType;

                if (fieldName.EndsWith(foreignKeyEnding))
                {
                    int index = fieldName.IndexOf(foreignKeyEnding, System.StringComparison.Ordinal);

                    if (index < lowestIndex)
                    {
                        lowestIndex = index;
                        foreignKeyTypeName = typeName;
                    }
                }
            }

            return foreignKeyTypeName;
        }

        private static bool IsPossibleKeyField(string fieldName, string keyType)
        {
            foreach (var className in TypeNameList)
            {
                if (fieldName.EndsWith(className + keyType))
                {
                    return true;
                }
            }

            return false;            
        }
    }
}
