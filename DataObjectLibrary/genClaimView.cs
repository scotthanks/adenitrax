﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DataObjectLibrary
{
    public partial class ClaimView : DataObjectBase
    {
        public enum ColumnEnum
        {
            ClaimId,
            ClaimGuid,
            GrowerOrderGuid,
            ClaimReasonLookupGuid,
            Comment,
            ClaimDate,
            CreditMemoLedgerGuid,
            ClaimStatusLookupGuid,
            ClaimNo,
            GrowerGuid,
            OrderNo,
            Description,
            CustomerPoNo,
            ShipWeekGuid,
            ShipWeekId,
            ShipWeekCode,
            ProgramTypeName,
            ProductFormCategoryName,
            TotalQtyClaimed,
            ClaimAmount,
        }

        new public enum ForeignKeyFieldEnum
        {
            ClaimGuid,
            GrowerOrderGuid,
            ClaimReasonLookupGuid,
            CreditMemoLedgerGuid,
            ClaimStatusLookupGuid,
            GrowerGuid,
            ShipWeekGuid,
        }

        private static List<Type> _foreignKeyTableTypeList;

        public static List<Type> ForeignKeyTableTypeList
        {
            get
            {
                if (_foreignKeyTableTypeList == null)
                {
                    _foreignKeyTableTypeList = new List<Type>
                    {
                        //typeof(Claim),
                        typeof(GrowerOrder),
                        typeof(Lookup),
                        //typeof(Ledger),
                        typeof(Lookup),
                        typeof(Grower),
                        typeof(ShipWeek),
                    };
                }

                return _foreignKeyTableTypeList;
            }
        }

        public long ClaimId { get; set; }
        public Guid ClaimGuid { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public Guid? ClaimReasonLookupGuid { get; set; }
        public string Comment { get; set; }
        public DateTime ClaimDate { get; set; }
        public Guid? CreditMemoLedgerGuid { get; set; }
        public Guid ClaimStatusLookupGuid { get; set; }
        public string ClaimNo { get; set; }
        public Guid GrowerGuid { get; set; }
        public string OrderNo { get; set; }
        public string Description { get; set; }
        public string CustomerPoNo { get; set; }
        public Guid ShipWeekGuid { get; set; }
        public long ShipWeekId { get; set; }
        public string ShipWeekCode { get; set; }
        public string ProgramTypeName { get; set; }
        public string ProductFormCategoryName { get; set; }
        public int? TotalQtyClaimed { get; set; }
        public Decimal? ClaimAmount { get; set; }

        public override string GetFieldValueAsString(string fieldName)
        {
            string value = null;
            switch (fieldName)
            {
                case "ClaimId":
                    value = LongToString(ClaimId);
                    break;
                case "ClaimGuid":
                    value = GuidToString(ClaimGuid);
                    break;
                case "GrowerOrderGuid":
                    value = GuidToString(GrowerOrderGuid);
                    break;
                case "ClaimReasonLookupGuid":
                    value = GuidToString(ClaimReasonLookupGuid);
                    break;
                case "Comment":
                    value = StringToString(Comment);
                    break;
                case "ClaimDate":
                    value = DateTimeToString(ClaimDate);
                    break;
                case "CreditMemoLedgerGuid":
                    value = GuidToString(CreditMemoLedgerGuid);
                    break;
                case "ClaimStatusLookupGuid":
                    value = GuidToString(ClaimStatusLookupGuid);
                    break;
                case "ClaimNo":
                    value = StringToString(ClaimNo);
                    break;
                case "GrowerGuid":
                    value = GuidToString(GrowerGuid);
                    break;
                case "OrderNo":
                    value = StringToString(OrderNo);
                    break;
                case "Description":
                    value = StringToString(Description);
                    break;
                case "CustomerPoNo":
                    value = StringToString(CustomerPoNo);
                    break;
                case "ShipWeekGuid":
                    value = GuidToString(ShipWeekGuid);
                    break;
                case "ShipWeekId":
                    value = LongToString(ShipWeekId);
                    break;
                case "ShipWeekCode":
                    value = StringToString(ShipWeekCode);
                    break;
                case "ProgramTypeName":
                    value = StringToString(ProgramTypeName);
                    break;
                case "ProductFormCategoryName":
                    value = StringToString(ProductFormCategoryName);
                    break;
                case "TotalQtyClaimed":
                    value = IntToString(TotalQtyClaimed);
                    break;
                case "ClaimAmount":
                    value = DecimalToString(ClaimAmount);
                    break;
            }
            return value;
        }

        public override bool ParseFieldFromString(string fieldName, string value)
        {
            bool valueSet = true;

            switch (fieldName)
            {
                case "ClaimId":
                    ClaimId = (long)LongFromString(value, isNullable: false);
                    break;
                case "ClaimGuid":
                    ClaimGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "GrowerOrderGuid":
                    GrowerOrderGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ClaimReasonLookupGuid":
                    ClaimReasonLookupGuid = GuidFromString(value, isNullable: true);
                    break;
                case "Comment":
                    Comment = (string)StringFromString(value, isNullable: false);
                    break;
                case "ClaimDate":
                    ClaimDate = (DateTime)DateTimeFromString(value, isNullable: false);
                    break;
                case "CreditMemoLedgerGuid":
                    CreditMemoLedgerGuid = GuidFromString(value, isNullable: true);
                    break;
                case "ClaimStatusLookupGuid":
                    ClaimStatusLookupGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ClaimNo":
                    ClaimNo = (string)StringFromString(value, isNullable: false);
                    break;
                case "GrowerGuid":
                    GrowerGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "OrderNo":
                    OrderNo = (string)StringFromString(value, isNullable: false);
                    break;
                case "Description":
                    Description = (string)StringFromString(value, isNullable: false);
                    break;
                case "CustomerPoNo":
                    CustomerPoNo = (string)StringFromString(value, isNullable: false);
                    break;
                case "ShipWeekGuid":
                    ShipWeekGuid = (Guid)GuidFromString(value, isNullable: false);
                    break;
                case "ShipWeekId":
                    ShipWeekId = (long)LongFromString(value, isNullable: false);
                    break;
                case "ShipWeekCode":
                    ShipWeekCode = StringFromString(value, isNullable: true);
                    break;
                case "ProgramTypeName":
                    ProgramTypeName = (string)StringFromString(value, isNullable: false);
                    break;
                case "ProductFormCategoryName":
                    ProductFormCategoryName = (string)StringFromString(value, isNullable: false);
                    break;
                case "TotalQtyClaimed":
                    TotalQtyClaimed = IntFromString(value, isNullable: true);
                    break;
                case "ClaimAmount":
                    ClaimAmount = DecimalFromString(value, isNullable: true);
                    break;
                default:
                    valueSet = false;
                    break;
            }

            return valueSet;
        }

        public override object this[string fieldName]
        {
            get
            {
                object value = null;
                switch (fieldName)
                {
                    case "ClaimId":
                        value = ClaimId;
                        break;
                    case "ClaimGuid":
                        value = ClaimGuid;
                        break;
                    case "GrowerOrderGuid":
                        value = GrowerOrderGuid;
                        break;
                    case "ClaimReasonLookupGuid":
                        value = ClaimReasonLookupGuid;
                        break;
                    case "Comment":
                        value = Comment;
                        break;
                    case "ClaimDate":
                        value = ClaimDate;
                        break;
                    case "CreditMemoLedgerGuid":
                        value = CreditMemoLedgerGuid;
                        break;
                    case "ClaimStatusLookupGuid":
                        value = ClaimStatusLookupGuid;
                        break;
                    case "ClaimNo":
                        value = ClaimNo;
                        break;
                    case "GrowerGuid":
                        value = GrowerGuid;
                        break;
                    case "OrderNo":
                        value = OrderNo;
                        break;
                    case "Description":
                        value = Description;
                        break;
                    case "CustomerPoNo":
                        value = CustomerPoNo;
                        break;
                    case "ShipWeekGuid":
                        value = ShipWeekGuid;
                        break;
                    case "ShipWeekId":
                        value = ShipWeekId;
                        break;
                    case "ShipWeekCode":
                        value = ShipWeekCode;
                        break;
                    case "ProgramTypeName":
                        value = ProgramTypeName;
                        break;
                    case "ProductFormCategoryName":
                        value = ProductFormCategoryName;
                        break;
                    case "TotalQtyClaimed":
                        value = TotalQtyClaimed;
                        break;
                    case "ClaimAmount":
                        value = ClaimAmount;
                        break;
                }
                return value;
            }
            set
            {
                switch (fieldName)
                {
                    case "ClaimId":
                        ClaimId = (long)value;
                        break;
                    case "ClaimGuid":
                        ClaimGuid = (Guid)value;
                        break;
                    case "GrowerOrderGuid":
                        GrowerOrderGuid = (Guid)value;
                        break;
                    case "ClaimReasonLookupGuid":
                        ClaimReasonLookupGuid = (Guid?)value;
                        break;
                    case "Comment":
                        Comment = (string)value;
                        break;
                    case "ClaimDate":
                        ClaimDate = (DateTime)value;
                        break;
                    case "CreditMemoLedgerGuid":
                        CreditMemoLedgerGuid = (Guid?)value;
                        break;
                    case "ClaimStatusLookupGuid":
                        ClaimStatusLookupGuid = (Guid)value;
                        break;
                    case "ClaimNo":
                        ClaimNo = (string)value;
                        break;
                    case "GrowerGuid":
                        GrowerGuid = (Guid)value;
                        break;
                    case "OrderNo":
                        OrderNo = (string)value;
                        break;
                    case "Description":
                        Description = (string)value;
                        break;
                    case "CustomerPoNo":
                        CustomerPoNo = (string)value;
                        break;
                    case "ShipWeekGuid":
                        ShipWeekGuid = (Guid)value;
                        break;
                    case "ShipWeekId":
                        ShipWeekId = (long)value;
                        break;
                    case "ShipWeekCode":
                        ShipWeekCode = (string)value;
                        break;
                    case "ProgramTypeName":
                        ProgramTypeName = (string)value;
                        break;
                    case "ProductFormCategoryName":
                        ProductFormCategoryName = (string)value;
                        break;
                    case "TotalQtyClaimed":
                        TotalQtyClaimed = (int?)value;
                        break;
                    case "ClaimAmount":
                        ClaimAmount = (Decimal?)value;
                        break;
                }
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("ClaimId={0}", this.ClaimId);
            result.Append(", ");
            result.AppendFormat("ClaimGuid={0}", this.ClaimGuid);
            result.Append(", ");
            result.AppendFormat("GrowerOrderGuid={0}", this.GrowerOrderGuid);
            result.Append(", ");
            result.AppendFormat("ClaimReasonLookupGuid={0}", this.ClaimReasonLookupGuid);
            result.Append(", ");
            result.AppendFormat("Comment={0}", this.Comment);
            result.Append(", ");
            result.AppendFormat("ClaimDate={0}", this.ClaimDate);
            result.Append(", ");
            result.AppendFormat("CreditMemoLedgerGuid={0}", this.CreditMemoLedgerGuid);
            result.Append(", ");
            result.AppendFormat("ClaimStatusLookupGuid={0}", this.ClaimStatusLookupGuid);
            result.Append(", ");
            result.AppendFormat("ClaimNo={0}", this.ClaimNo);
            result.Append(", ");
            result.AppendFormat("GrowerGuid={0}", this.GrowerGuid);
            result.Append(", ");
            result.AppendFormat("OrderNo={0}", this.OrderNo);
            result.Append(", ");
            result.AppendFormat("Description={0}", this.Description);
            result.Append(", ");
            result.AppendFormat("CustomerPoNo={0}", this.CustomerPoNo);
            result.Append(", ");
            result.AppendFormat("ShipWeekGuid={0}", this.ShipWeekGuid);
            result.Append(", ");
            result.AppendFormat("ShipWeekId={0}", this.ShipWeekId);
            result.Append(", ");
            result.AppendFormat("ShipWeekCode={0}", this.ShipWeekCode);
            result.Append(", ");
            result.AppendFormat("ProgramTypeName={0}", this.ProgramTypeName);
            result.Append(", ");
            result.AppendFormat("ProductFormCategoryName={0}", this.ProductFormCategoryName);
            result.Append(", ");
            result.AppendFormat("TotalQtyClaimed={0}", this.TotalQtyClaimed);
            result.Append(", ");
            result.AppendFormat("ClaimAmount={0}", this.ClaimAmount);

            return result.ToString();
        }

        #region Lazy Loading Logic

        //#region Claim Lazy Loading Properties and Methods

        //private Lazy<Claim> _lazyClaim;

        //public Claim Claim
        //{
        //    get
        //    {
        //        return _lazyClaim == null ? null : _lazyClaim.Value;
        //    }
        //}

        //public bool ClaimIsLoaded
        //{
        //    get
        //    {
        //        return _lazyClaim == null ? false : _lazyClaim.IsValueCreated;
        //    }
        //}

        //public void SetLazyClaim(Lazy<Claim> lazyClaim)
        //{
        //    _lazyClaim = lazyClaim;
        //}

        //#endregion

        #region GrowerOrder Lazy Loading Properties and Methods

        private Lazy<GrowerOrder> _lazyGrowerOrder;

        public GrowerOrder GrowerOrder
        {
            get
            {
                return _lazyGrowerOrder == null ? null : _lazyGrowerOrder.Value;
            }
        }

        public bool GrowerOrderIsLoaded
        {
            get
            {
                return _lazyGrowerOrder == null ? false : _lazyGrowerOrder.IsValueCreated;
            }
        }

        public void SetLazyGrowerOrder(Lazy<GrowerOrder> lazyGrowerOrder)
        {
            _lazyGrowerOrder = lazyGrowerOrder;
        }

        #endregion

        #region ClaimReasonLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyClaimReasonLookup;

        public Lookup ClaimReasonLookup
        {
            get
            {
                return _lazyClaimReasonLookup == null ? null : _lazyClaimReasonLookup.Value;
            }
        }

        public bool ClaimReasonLookupIsLoaded
        {
            get
            {
                return _lazyClaimReasonLookup == null ? false : _lazyClaimReasonLookup.IsValueCreated;
            }
        }

        public void SetLazyClaimReasonLookup(Lazy<Lookup> lazyClaimReasonLookup)
        {
            _lazyClaimReasonLookup = lazyClaimReasonLookup;
        }

        #endregion

        //#region CreditMemoLedger Lazy Loading Properties and Methods

        //private Lazy<Ledger> _lazyCreditMemoLedger;

        //public Ledger CreditMemoLedger
        //{
        //    get
        //    {
        //        return _lazyCreditMemoLedger == null ? null : _lazyCreditMemoLedger.Value;
        //    }
        //}

        //public bool CreditMemoLedgerIsLoaded
        //{
        //    get
        //    {
        //        return _lazyCreditMemoLedger == null ? false : _lazyCreditMemoLedger.IsValueCreated;
        //    }
        //}

        //public void SetLazyCreditMemoLedger(Lazy<Ledger> lazyCreditMemoLedger)
        //{
        //    _lazyCreditMemoLedger = lazyCreditMemoLedger;
        //}

        //#endregion

        #region ClaimStatusLookup Lazy Loading Properties and Methods

        private Lazy<Lookup> _lazyClaimStatusLookup;

        public Lookup ClaimStatusLookup
        {
            get
            {
                return _lazyClaimStatusLookup == null ? null : _lazyClaimStatusLookup.Value;
            }
        }

        public bool ClaimStatusLookupIsLoaded
        {
            get
            {
                return _lazyClaimStatusLookup == null ? false : _lazyClaimStatusLookup.IsValueCreated;
            }
        }

        public void SetLazyClaimStatusLookup(Lazy<Lookup> lazyClaimStatusLookup)
        {
            _lazyClaimStatusLookup = lazyClaimStatusLookup;
        }

        #endregion

        #region Grower Lazy Loading Properties and Methods

        private Lazy<Grower> _lazyGrower;

        public Grower Grower
        {
            get
            {
                return _lazyGrower == null ? null : _lazyGrower.Value;
            }
        }

        public bool GrowerIsLoaded
        {
            get
            {
                return _lazyGrower == null ? false : _lazyGrower.IsValueCreated;
            }
        }

        public void SetLazyGrower(Lazy<Grower> lazyGrower)
        {
            _lazyGrower = lazyGrower;
        }

        #endregion

        #region ShipWeek Lazy Loading Properties and Methods

        private Lazy<ShipWeek> _lazyShipWeek;

        public ShipWeek ShipWeek
        {
            get
            {
                return _lazyShipWeek == null ? null : _lazyShipWeek.Value;
            }
        }

        public bool ShipWeekIsLoaded
        {
            get
            {
                return _lazyShipWeek == null ? false : _lazyShipWeek.IsValueCreated;
            }
        }

        public void SetLazyShipWeek(Lazy<ShipWeek> lazyShipWeek)
        {
            _lazyShipWeek = lazyShipWeek;
        }

        #endregion

        #endregion
    }
}
