﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjectLibrary
{
    public class SearchResultData
    {
       
        public Guid Guid { get; set; }
        public string SearchString { get; set; }
        public int ResultCount { get; set; }
        public int Page { get; set; }
        public string RelatedSearches { get; set; }
        public List<SearchResultRow2> ResultRows { get; set; }
      
    }
}

