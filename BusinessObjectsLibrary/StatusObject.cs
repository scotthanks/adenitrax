﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class StatusObject
    {
        public StatusObject(Guid userGuid, bool isSecureRequest)
        {
            this.User = new UserObject(userGuid);
            this.IsSecureRequest = isSecureRequest;
        }
        public UserObject User { get; set; }

        public bool Success { get; set; }
        public bool IsSecureRequest { get; set; }

        public IEnumerable<ErrorObject> Errors { get; set; }
        public string StatusMessage { get; set; }


    }
}
