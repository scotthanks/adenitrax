﻿using System;


namespace BusinessObjectsLibrary
{
    public class ClinicPatientVisit
    {

        public Guid Guid { get; set; }
        public Guid ClinicPatientGuid { get; set; }
        public string ClinicPatientName { get; set; }

        public DateTime VisitDate { get; set; }
        public string Note { get; set; }
        public string ImageLink { get; set; }
        
        public decimal InitialWoundSizeCM { get; set; }
        public decimal WoundSizeCM { get; set; }
        public string ProductAppliedDescription { get; set; }
        public Guid ProductAppliedGuid { get; set; }
        public int ProductAppliedQty { get; set; }
        public decimal PercentReductionSinceStart { get; set; }
        public int DaysSinceStart { get; set; }







    }
}
