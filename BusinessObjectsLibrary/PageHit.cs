﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class PageHit
    {
        public PageHit()
        {
            
        }

        public string Environment { get; set; }
        public string UserName { get; set; }
        public string QueryString { get; set; }
        public string RawUrl { get; set; }
        public string UrlReferrer { get; set; } 
        public string UserAgent { get; set; }
        public string BrowserName { get; set; }
        public int BrowserVersion { get; set; }
        public string BrowserPlatform { get; set; }
        public string IPAddress { get; set; }
        public bool IsAuthenticated { get; set; }
        public bool IsCrawler { get; set; }
        public bool IsMobileDevice { get; set; }
        public bool AllowsCookies { get; set; }
        public bool IsLocal { get; set; }
        public string HttpMethod { get; set; }
        public string PartitionKey { get; set; }
        public string UserHostName { get; set; }
    }
}
