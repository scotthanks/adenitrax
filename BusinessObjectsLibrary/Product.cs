﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class Product
    {

        public Product()
        {
        }

        public Product(DataObjectLibrary.Product productTableRow)
        {
            Guid = productTableRow.Guid;
            ID = productTableRow.Id;
            Code = productTableRow.Code;
            SpeciesCode = productTableRow.Variety.Species.Code;
            SpeciesName = productTableRow.Variety.Species.Name;
            VarietyCode = productTableRow.Variety.Code;
            VarietyName = productTableRow.Variety.Name;
            SupplierCode = productTableRow.Program.Supplier.Code;
            SupplierName = productTableRow.Program.Supplier.Name;
            GeneticOwnerName = productTableRow.Variety.GeneticOwner.Name;

            ProductDescription = productTableRow.ProductDescriptionCalculated;
            SupplierProductIdentifier = productTableRow.SupplierIdentifier;
            SupplierProductDescription = productTableRow.SupplierDescription;
            ProductFormName = productTableRow.ProductForm.Name;

            ProductFormGuid = productTableRow.ProductForm.Guid;

            ProgramTypeCode = productTableRow.Program.ProgramType.Code;
            ProductFormCategoryCode = productTableRow.ProductForm.ProductFormCategory.Code;

            ProgramGuid = productTableRow.ProgramGuid;

            SalesUnitQty = productTableRow.ProductForm.SalesUnitQty;
            LineItemMinumumQty = productTableRow.ProductForm.LineItemMinimumQty;
            
            Price = 0;
            IncludesDelivery = productTableRow.ProductForm.IncludesDelivery;

            //TODO: Bury this logic deeper using ImageSet().
            ImageUrl = String.Format("https://epsstorage.blob.core.windows.net/publicimages/images/{0}.jpg", productTableRow.Code);
            ColorName = productTableRow.Variety.ColorLookup.Name;
            HabitName = productTableRow.Variety.HabitLookup.Name;
            VigorName = productTableRow.Variety.VigorLookup.Name;

            IsOrganic = productTableRow.IsOrganic;

            ProductionLeadTimeWeeks = productTableRow.ProductionLeadTimeWeeks;
        }

        public Guid Guid { get; set; }
        public long ID { get; set; }
        public string Code { get; set; }
        public string SpeciesCode { get; set; }
        public string SpeciesName { get; set; }
        public string VarietyCode { get; set; }
        public string VarietyName { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string GeneticOwnerName { get; set; }

        public Guid ProgramGuid { get; set; }
        public string ProgramTypeCode { get; set; }

        public string ProductDescription { get; set; }
        public string SupplierProductIdentifier { get; set; }
        public string SupplierProductDescription { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public string ProductFormName { get; set; }

        public Guid ProductFormGuid { get; set; }

        public int SalesUnitQty { get; set; }
        public int LineItemMinumumQty { get; set; }
        public decimal Price { get; set; }
        public bool IncludesDelivery { get; set; }

        public string ImageUrl { get; set; }
        public string ColorName { get; set; }
        public string HabitName { get; set; }
        public string VigorName { get; set; }

        public bool IsOrganic { get; set; }

        public bool VarietyWasSelectedByUser { get; set; }

        public int ProductionLeadTimeWeeks { get; set; }

        private List<AvailabilityShipWeek> _selectedAvailability; 
        public List<AvailabilityShipWeek> SelectedAvailability
        {
            get
            {
                if (_selectedAvailability == null)
                {
                    _selectedAvailability = new List<AvailabilityShipWeek>();
                }

                return _selectedAvailability;
            }
            set { _selectedAvailability = value; }
        }

        public OrderLine OrderLine;
        private List<OrderLine> _orderLines;
        public List<OrderLine> OrderLines
        {
            get
            {
                if (_orderLines == null)
                {
                    _orderLines = new List<OrderLine>();
                }

                return _orderLines;
            }
            set { _orderLines = value; }
        }
        public class ProductComparer : Comparer<Product>
        {
            override public int Compare(Product product1, Product product2)
            {
                if (product1 == null)
                    throw new ArgumentNullException("product1");

                if (product2 == null)
                    throw new ArgumentNullException("product2");

                int result = 0;

                result = -1 * product1.VarietyWasSelectedByUser.CompareTo(product2.VarietyWasSelectedByUser);

                if (result == 0)
                    result = String.Compare(product1.SpeciesName, product2.SpeciesName, StringComparison.Ordinal);

                if (result == 0)
                    result = String.Compare(product1.ProductDescription, product2.ProductDescription, StringComparison.Ordinal);

                return result;
            }
        }

        public bool HasAvailableQtyOfAtLeast(int quantity)
        {
            bool hasAvailability;

            if (SelectedAvailability.Count == 0)
            {
                throw new ApplicationException("Availability information has not been made available on this object.");                
            }
            else if (SelectedAvailability.Count > 1)
            {
                throw new ApplicationException("This method is only supported when a single week of availability data has been selected.");
            }
            else
            {
                var availability = SelectedAvailability[0];

                //TODO: Find a way to bring the Lookup values into here without hard-coding like this.
                switch (availability.AvailabilityType)
                {
                    case "OPEN":
                        hasAvailability = true;
                        break;
                    case "NA":
                        hasAvailability = false;
                        break;
                    case "AVAIL":
                        hasAvailability = availability.Quantity >= quantity;
                        break;
                    default:
                        throw new NotImplementedException(string.Format( "The availability type of \"{0}\" has not been implemented.", availability.AvailabilityType));
                        break;
                }
            }

            return hasAvailability;
        }
    }
}
