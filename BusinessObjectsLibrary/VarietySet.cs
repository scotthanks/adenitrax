﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class VarietySet
    {
        public VarietySet() { }

        public string SpeciesCode { get; set; }
        public string SpeciesName { get; set; }
        public IEnumerable<Variety> VarietiesCollection { get; set; }
    }
}
