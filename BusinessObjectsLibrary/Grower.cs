﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;
using Utility;

namespace BusinessObjectsLibrary
{
    public class Grower
    {
        public Grower()
        {            
        }

        public Grower(DataObjectLibrary.Grower growerDataRow)
            : this()
        {
            Guid = growerDataRow.Guid;
            GrowerName = growerDataRow.Name;
            GrowerCode = growerDataRow.Code;
            IsActivated = growerDataRow.IsActivated;
            Address = new Address( growerDataRow.Address);
            AllowSubstitutions = growerDataRow.AllowSubstitutions;
            CreditLimit = growerDataRow.CreditLimit;
            CreditLimitTemporaryIncrease = growerDataRow.CreditLimitTemporaryIncrease;
            CreditLimitTemporaryIncreaseExpiration = growerDataRow.CreditLimitTemporaryIncreaseExpiration;
            CreditAppOnFile = growerDataRow.CreditAppOnFile;
            RequestedLineOfCredit = growerDataRow.RequestedLineOfCredit;
            EMail = growerDataRow.EMail;
            AdditionalAccountingEmail = growerDataRow.AdditionalAccountingEmail;
            AdditionalOrderEmail = growerDataRow.AdditionalOrderEmail;
            PhoneNumber = new PhoneNumber(growerDataRow.PhoneAreaCode, growerDataRow.Phone);
            GrowerTypeLookupGuid = growerDataRow.GrowerTypeLookupGuid;
            DefaultPaymentType = new Lookup(growerDataRow.DefaultPaymentTypeLookup, growerDataRow.DefaultPaymentTypeLookup.ParentLookup.ChildLookupList);
            CreditAppStatus = new Lookup(growerDataRow.RequestedLineOfCreditStatusLookup, growerDataRow.RequestedLineOfCreditStatusLookup.ParentLookup.ChildLookupList);

        }

        public void LoadShipToAddresses(List<GrowerAddress> growerAddressList)
        {
            ShipToAddressList = new List<GrowerShipToAddress>();
            foreach (var growerAddress in growerAddressList)
            {
                ShipToAddressList.Add(new GrowerShipToAddress(growerAddress));
            }
        }

        public void LoadCreditCards(List<GrowerCreditCard> growerCreditCardList)
        {
            CreditCardList = new List<CreditCard>();
            foreach (var growerCreditCard in growerCreditCardList)
            {
                CreditCardList.Add(new CreditCard(growerCreditCard));
            }
        }

        public Guid Guid { get; set; }
        public string GrowerName { get; set; }
        public string GrowerCode { get; set; }
        public bool IsActivated { get; set; }
        public Address Address { get; set; }
        public bool AllowSubstitutions { get; set; }
        public int CreditLimit { get; set; }
        public int CreditLimitTemporaryIncrease { get; set; }
        public DateTime? CreditLimitTemporaryIncreaseExpiration { get; set; }
        public string EMail { get; set; }
        public string AdditionalAccountingEmail { get; set; }
        public string AdditionalOrderEmail { get; set; }
        public Utility.PhoneNumber PhoneNumber { get; set; }
        public Guid GrowerTypeLookupGuid { get; set; }
        public Lookup DefaultPaymentType { get; set; }
        public Lookup CreditAppStatus { get; set; }
        public bool CreditAppOnFile { get; set; }
        public int RequestedLineOfCredit { get; set; }

        public string SalesTaxNumber { get; set; }

        public List<GrowerShipToAddress> ShipToAddressList;
        public List<CreditCard> CreditCardList;
    }
}
