﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class ClaimSummary
    {
        public ClaimSummary(ClaimView summaryData)
        
        {
            ClaimGuid = summaryData.ClaimGuid;
            OrderNo = summaryData.OrderNo;
            ClaimNo = summaryData.ClaimNo;
            ClaimAmount = summaryData.ClaimAmount;
            ShipWeek = new ShipWeek(summaryData.ShipWeek);
            ProgramTypeName = summaryData.ProgramTypeName;
            ProductFormCategoryName = summaryData.ProductFormCategoryName;
            ClaimStatusName = summaryData.ClaimStatusLookup.Name;
            
        }

        public Guid ClaimGuid { get; set; }
        public string OrderNo { get; set; }
        public string ClaimNo { get; set; }
        public decimal? ClaimAmount { get; set; }
        public ShipWeek ShipWeek { get; set; }
        public string ProgramTypeName { get; set; }
        public string ProductFormCategoryName { get; set; }
        public string ClaimStatusName { get; set; }
       

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendFormat("Guid={0}", this.ClaimGuid);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("OrderNo={0}", this.OrderNo);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ClaimNo={0}", this.OrderNo);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ClaimAmount={0}", this.OrderNo);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ShipWeek={0}", this.ShipWeek.Code);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ProgramTypeName={0}", this.ProgramTypeName);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ProductFormCategoryName.{0}", this.ProductFormCategoryName);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ClaimStatusName.{0}", this.ClaimStatusName);
            stringBuilder.Append(",");
            

            return stringBuilder.ToString();
        }
    }
}
