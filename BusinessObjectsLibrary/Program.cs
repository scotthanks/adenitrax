﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class Program
    {

        public Program()
        {
        }

        public Program(DataObjectLibrary.Program program)
        {
            ProgramGuid = program.Guid;
            Code = program.Code;
            Name = program.Name;
            ProgramTypeGuid = program.ProgramTypeGuid;
            SupplierGuid = program.SupplierGuid;
              
        }


        public Guid ProgramGuid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Guid ProgramTypeGuid { get; set; }
        public Guid SupplierGuid { get; set; }
        public Guid ProgramFormCategoryGuid { get; set; }

    }
}
