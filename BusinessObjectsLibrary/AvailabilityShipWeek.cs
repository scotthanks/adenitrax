﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class AvailabilityShipWeek : IComparable<AvailabilityShipWeek>
    {
        public AvailabilityShipWeek()
        {   
        }

        public AvailabilityShipWeek(DataObjectLibrary.ReportedAvailability reportedAvailability)
        {
            ShipWeek = new ShipWeek(reportedAvailability.ShipWeek);

            int sales = 0;
            if (reportedAvailability.SalesSinceDateReported.HasValue)
            {
                sales = (int)reportedAvailability.SalesSinceDateReported;
            }

            Quantity = Math.Max(reportedAvailability.Qty - sales,0);
            AvailabilityType = reportedAvailability.AvailabilityTypeLookup.Code;
        }

        public ShipWeek ShipWeek { get; set; }
        public int Quantity { get; set; }
        public string AvailabilityType { get; set; }

        public int CompareTo(AvailabilityShipWeek other)
        {
            return ShipWeek.MondayDate.CompareTo(other.ShipWeek.MondayDate);
        }
    }
}
