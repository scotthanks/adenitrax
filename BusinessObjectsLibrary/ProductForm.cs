﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class ProductForm : ProductFormBase
    {
        public ProductForm(DataObjectLibrary.ProductForm dataObject)
        {
            Guid = dataObject.Guid;
            SupplierGuid = dataObject.SupplierGuid;
            ProductFormCategoryGuid = dataObject.ProductFormCategoryGuid;
            Code = dataObject.Code;
            Name = dataObject.Name;
            SalesUnitQty = dataObject.SalesUnitQty;
            TagBundleQty = dataObject.TagBundleQty;
            LineItemMinimumQty = dataObject.LineItemMinimumQty;
            IsActive = dataObject.IsActive;
        }
    }
}
