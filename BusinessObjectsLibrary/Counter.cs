﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public sealed class CSingleTone
    {
        private static CSingleTone instance;
        public int theCounter { get; set; }
        public static CSingleTone Instance
        {
            get
            {
                if (instance == null)
                    instance = new CSingleTone();
                return instance;
            }
        }
        private CSingleTone()
        {
        }
    }
}
