﻿using System;


namespace BusinessObjectsLibrary
{
    public class ClinicPatient
    {

        public Guid Guid { get; set; }

        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }

        public string WoundShortName { get; set; }
        public string WoundDescription { get; set; }
        public string DisplayName { get; set; }
        public string WoundCategory { get; set; }
        public Guid WoundCategoryGuid { get; set; }
        public string WoundStatus { get; set; }
        public Guid WoundStatusGuid { get; set; }

        public decimal InitialWoundSize { get; set; }

        public DateTime DateEntered { get; set; }
        public Guid ClinicPatientGuid { get; set; }
        public Guid PersonGuid { get; set; }




    }
}
