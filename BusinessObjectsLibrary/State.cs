﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class State
    {
        public State(DataObjectLibrary.State stateDataRow)
        {
            StateCode = stateDataRow.Code;
            StateName = stateDataRow.Name;
            Country = new Country(stateDataRow.Country);
        }

        public State(string stateCode)
        {
            StateCode = stateCode;
        }

        public string StateCode { get; set; }
        public string StateName { get; set; }
        public Country Country { get; set; }
    }
}
