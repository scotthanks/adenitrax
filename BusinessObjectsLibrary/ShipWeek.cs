﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class ShipWeek : IComparable<ShipWeek>
    {
        const string SHIP_WEEK_DELIMITER_CHAR = "|";
        public ShipWeek()
        {
        }

        public ShipWeek(DataObjectLibrary.ShipWeek shipWeek)
        {
            this.Id = shipWeek.Id;
            this.Guid = shipWeek.Guid;
            this.Week = shipWeek.Week;
            this.Year = shipWeek.Year;
            this.MondayDate = shipWeek.MondayDate;
            this.ShipWeekString = string.Format("{0:D2}{1}{2}", Week, SHIP_WEEK_DELIMITER_CHAR, Year);

        }

        public ShipWeek(string shipWeekCode)
            :this(GetWeekNumber(shipWeekCode), GetYearNumber(shipWeekCode))
        {

        }

        private static int GetWeekNumber(string shipWeekCode)
        {
            int week;
            int year;
            ParseWeekAndYear(shipWeekCode, out week, out year);
            return week;
        }

        private static int GetYearNumber(string shipWeekCode)
        {
            int week;
            int year;
            ParseWeekAndYear(shipWeekCode, out week, out year);
            return year;
        }

        private static void ParseWeekAndYear(string shipWeekCode, out int week, out int year)
        {
            var parts = shipWeekCode.Split(new char[] { '|' });
            int.TryParse(parts[0], out week);
            int.TryParse(parts[1], out year);            
        }

        public ShipWeek(int weekNumber, int year)
        {
            Week = weekNumber;
            Year = year;
            //TODO: Fix to actual work for years other than 2013!!!
            MondayDate = new DateTime(year - 1, 12, 31).AddDays(7 * (weekNumber - 1));            
        }

        public ShipWeek(int weekNumber)
            :this(weekNumber, 2013)
        {
        }

        public string Code
        {
            get { return string.Format("{0:0000}{1:00}", Year, Week); }
        }

        public long Id { get; set; }
        public Guid Guid { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public string ShipWeekString { get; set; }
        public DateTime MondayDate { get; set; }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }

        public int CompareTo(ShipWeek other)
        {
            return this.MondayDate.CompareTo(other.MondayDate);
        }
    }
}
