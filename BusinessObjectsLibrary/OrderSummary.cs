﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class OrderSummary
    {
       
        public OrderSummary(GrowerOrderSummaryByShipWeekView summaryData)
        {
            OrderGuid = summaryData.GrowerOrder.Guid;
            OrderNo = summaryData.GrowerOrder.OrderNo;
            ShipWeek = new ShipWeek(summaryData.ShipWeek);
            ProductFormCategoryCode = summaryData.GrowerOrder.ProductFormCategory.Code;
            ProductFormCategory = new BusinessObjectsLibrary.ProductFormCategory(summaryData.GrowerOrder.ProductFormCategory);
            ProgramTypeName = summaryData.GrowerOrder.ProgramType.Name;
            ProgramTypeCode = summaryData.GrowerOrder.ProgramType.Code;
            OrderQty = summaryData.QtyOrderedCount;

            CustomerPoNo = summaryData.GrowerOrder.CustomerPoNo;
            OrderDescription = summaryData.GrowerOrder.Description;
            PromotionalCode = summaryData.GrowerOrder.PromotionalCode;
            CustomerCode = summaryData.GrowerOrder.Code;
            GrowerName = summaryData.Grower.Name;

            PhoneNumber =
                Utility.PhoneNumber.FormatPhoneNumber(summaryData.Grower.PhoneAreaCode, summaryData.Grower.Phone);

            GrowerOrderStatusCode = summaryData.GrowerOrderStatusLookup.Code;
            GrowerOrderStatusName = summaryData.GrowerOrderStatusLookup.Name;
            LowestSupplierOrderStatusCode = summaryData.LowestSupplierOrderStatusLookup.Code;
            LowestOrderLineStatusCode = summaryData.LowestOrderLineStatusLookup.Code;

            PersonWhoPlacedOrder = new Person(summaryData.Person, summaryData.Grower);
            InvoiceNo = summaryData.InvoiceNo;
        }

        public Guid OrderGuid { get; set; }
        public string OrderNo { get; set; }
        public ShipWeek ShipWeek { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public BusinessObjectsLibrary.ProductFormCategory ProductFormCategory { get; set; }
        //TODO: Make this a ProgramType business class instead?
        public string ProgramTypeCode { get; set; }
        public string ProgramTypeName { get; set; }             //Database name for ProgramCategoryName
        public int OrderQty { get; set; }

        public string GrowerName { get; set; }
        public string CustomerPoNo { get; set; }
        public string OrderDescription { get; set; }
        public string PromotionalCode { get; set; }
        public string CustomerCode { get; set; }
        public string PhoneNumber { get; set; }

        public string GrowerOrderStatusCode { get; set; }
        public string GrowerOrderStatusName { get; set; }
        public string LowestSupplierOrderStatusCode { get; set; }
        public string LowestOrderLineStatusCode { get; set; }

        public Person PersonWhoPlacedOrder { get; set; }
        public string InvoiceNo { get; set; }


        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendFormat("Guid={0}", this.OrderGuid);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("OrderNo={0}", this.OrderNo);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ShipWeek={0}", this.ShipWeek.Code);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ProductFormCategory.Code={0}", this.ProductFormCategory.Code);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("ProgramTypeName={0}", this.ProgramTypeName);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("summaryData.QtyOrderedCount={0}", this.OrderQty);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("CustomerCode={0}", CustomerCode);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("OrderDescription={0}", OrderDescription);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("PhoneNumber={0}", PhoneNumber);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("GrowerOrderStatusCode={0}", GrowerOrderStatusCode);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("GrowerOrderStatusName={0}", GrowerOrderStatusName);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("LowestSupplierOrderStatusCode={0}", LowestSupplierOrderStatusCode);
            stringBuilder.Append(",");
            stringBuilder.AppendFormat("LowestOrderLineStatusCode={0}", LowestOrderLineStatusCode);

            return stringBuilder.ToString();
        }
    }
}
