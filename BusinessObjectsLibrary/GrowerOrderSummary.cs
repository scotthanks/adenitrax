﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class GrowerOrderSummary
    {
        public Guid GrowerOrderGuid { get; set; }
        public Guid ShipWeekGuid { get; set; }
        public string OrderNo { get; set; }
        public string CustomerPoNo { get; set; }
        public string ShipToCity { get; set; }
        public int QtyOrdered { get; set; }

    }
}
