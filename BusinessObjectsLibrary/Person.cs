﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace BusinessObjectsLibrary
{
    public class Person
    {
        public Person(DataObjectLibrary.Person personDataRow, DataObjectLibrary.Grower growerDataRow, bool isAuthorizedToPlaceOrders = false, bool isAuthorizedToSeePrices = false)
            : this(personDataRow, new Grower(growerDataRow), isAuthorizedToPlaceOrders, isAuthorizedToSeePrices)
        {            
        }

        public Person(DataObjectLibrary.Person personDataRow, Grower grower = null, bool isAuthorizedToPlaceOrders = false, bool isAuthorizedToSeePrices = false)
        {
            PersonGuid = personDataRow.Guid;
            FirstName = personDataRow.FirstName;
            LastName = personDataRow.LastName;
            UserGuid = personDataRow.UserGuid ?? Guid.Empty;
            Email = personDataRow.EMail;
            Phone = new PhoneNumber(personDataRow.PhoneAreaCode, personDataRow.Phone);

            PersonTypeLookupCode = personDataRow.PersonTypeLookup.Code;
            GrowerGuid = personDataRow.GrowerGuid.Value;

            IsAuthorizedToPlaceOrders = isAuthorizedToPlaceOrders;
            IsAuthorizedToSeePrices = isAuthorizedToSeePrices;

            Grower = grower;
        }

        public Guid PersonGuid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid UserGuid { get; set; }
        public string Email { get; set; }
        public Utility.PhoneNumber Phone { get; set; }

        public Guid GrowerGuid { get; set; }
        public string PersonTypeLookupCode { get; set; }
        public string UserRole { get; set; }

        public BusinessObjectsLibrary.Grower Grower { get; set; }

        public bool IsAuthorizedToPlaceOrders { get; set; }
        public bool IsAuthorizedToSeePrices { get; set; }
    }
}
