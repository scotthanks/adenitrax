﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class ProductFormAvailabilityLinkedWeeks
    {

        public ProductFormAvailabilityLinkedWeeks()
        {
        }

        public ProductFormAvailabilityLinkedWeeks(DataObjectLibrary.ProductFormAvailabilityLinkedWeeks productFormAvailabilityLinkedWeeksDataRow)
        {
            Guid = productFormAvailabilityLinkedWeeksDataRow.Guid;
            GroupingCode = productFormAvailabilityLinkedWeeksDataRow.GroupingCode;
            ProductFormGuid = productFormAvailabilityLinkedWeeksDataRow.ProductFormGuid;
            ShipWeekGuid = productFormAvailabilityLinkedWeeksDataRow.ShipWeekGuid;
        }

        public Guid Guid { get; set; }
        public int GroupingCode { get; set; }
        public Guid ProductFormGuid { get; set; }
        public Guid ShipWeekGuid { get; set; }
    }
}
