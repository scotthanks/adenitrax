﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary.Catalog
{
    public class SelectionsType
    {

        public IEnumerable<CategoryType> Categories { get; set; }
        public IEnumerable<ProductFormType> ProductForms { get; set; }


        public IEnumerable<SupplierType> Suppliers { get; set; }
        //A column has been added to SpeciesType to contain Supplier Codes to which it pertains
        public IEnumerable<SpeciesType> Species { get; set; }
        //This is a relationship table connecting each species to one or more suppliers
        public IEnumerable<SpeciesToSuppliersType> SpeciesToSuppliers { get; set; }
        public IEnumerable<VarietyType> Varieties { get; set; }
    }
}
