﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary.Catalog
{
    public class SpeciesType
    {
        public Guid Guid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        //SupplierCodes column is one of two ways this can be handled, the other is the relationship table that is in the SpeciesToSuppliersType
        public string[] SupplierCodes { get; set; }

    }
}
