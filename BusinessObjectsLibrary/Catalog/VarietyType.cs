﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary.Catalog
{
    public class VarietyType
    {
        //NOTE: Catalog v3 uses POCO

        public string Code { get; set; }
        public string Name { get; set; }
        public Guid SpeciesGuid { get; set; }
        public string SpeciesCode { get; set; }

        public string GeneticOwnerName { get; set; }    //revised 2/15/2013
        //public string GeneticOwnerCode { get; set; }    //added 2/15/2013

        public string ThumbnailImageUrl { get; set; }
      //  public string ImageUrl { get; set; }        //added 2/13/2013
      //  public string FUllImageUrl { get; set; }
      //  public string CultureLibraryUrl { get; set; }








        //public VarietyType()
        //{
        //    SelectedProducts = new List<Product>();
        //}

        //public VarietyType(DataObjectLibrary.Variety varietyTableRow, string thumbnailImageUrl, string fullImageUrl)
        //    : this()
        //{
        //    Code = varietyTableRow.Code;
        //    Name = varietyTableRow.Name;
        //    GeneticOwnerCode = varietyTableRow.GeneticOwner.Code;
        //    GeneticOwnerName = varietyTableRow.GeneticOwner.Name;

        //    ColorDescription = varietyTableRow.ColorDescription;
        //    Color = varietyTableRow.ColorLookup.Name;
        //    Habit = varietyTableRow.HabitLookup.Name;
        //    Vigor = varietyTableRow.VigorLookup.Name;

        //    DescriptionHtmlGuid = varietyTableRow.DescriptionHTMLGUID;

        //    //SupplierName = varietyTableRow.
        //    //SupplierCode = varietyTableRow.GeneticOwner.Name;
        //    //TODO: Bury this logic deeper using ImageSet.
        //    ImageUrl = thumbnailImageUrl;
        //    ThumbnailImageUrl = thumbnailImageUrl;
        //    FUllImageUrl = fullImageUrl;
        //    CultureLibraryURL = varietyTableRow.CultureLibraryLink;

        //    SpeciesName = varietyTableRow.Species.Name;

        //    SeriesGuid = varietyTableRow.SeriesGuid;
        //}

        ////These are not used in Catalogue2 Page
        ////public long Id { get; set; }
        ////public Guid Guid { get; set; }
        ////public Guid SpeciesGuid { get; set; }
        ////public Guid GeneticOwnerGuid { get; set; }

        ////These are the ones initially used
        //public string Code { get; set; }
        //public string Name { get; set; }
        //public string GeneticOwnerName { get; set; }    //revised 2/15/2013
        //public string GeneticOwnerCode { get; set; }    //added 2/15/2013
        //public string ColorDescription { get; set; }
        //public string Color { get; set; }
        //public string Habit { get; set; }
        //public string Vigor { get; set; }

        //public Guid? DescriptionHtmlGuid { get; set; }

        ////public string SupplierName { get; set; }        //added 2/18/2013
        ////public string SupplierCode { get; set; }        //added 2/18/2013
        //public string ImageUrl { get; set; }        //added 2/13/2013
        //public string ThumbnailImageUrl { get; set; }
        //public string FUllImageUrl { get; set; }
        //public string CultureLibraryURL { get; set; }

        //public string SpeciesName { get; set; }     //added 2/13/2013

        ////These may be needed for the call to VarietyDetail Page
        //public ShipWeek PreferredShipWeek { get; set; }
        //public ShipWeek StartingShipWeek { get; set; }
        //public ShipWeek EndingShipWeek { get; set; }

        //public List<Product> SelectedProducts { get; set; }

        //public Guid? SeriesGuid { get; set; }
    }
}
