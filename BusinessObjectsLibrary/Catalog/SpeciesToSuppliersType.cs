﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary.Catalog
{
    public class SpeciesToSuppliersType
    {
        public string SpeciesCode { get; set; }
        public string SupplierCode { get; set; }
    }
}
