﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary.Catalog
{
    public class VarietyDetailType
    {

        public string Code { get; set; }
        public string Name { get; set; }



        //Received Values
        //public string PlantCategoryCode { get; set; }
        //public string ProductFormCode { get; set; }
        //public string SupplierCodes { get; set; }
        public string VarietyCode { get; set; }
        //public string ShipWeekString { get; set; }

        //Retrieved Values
        public string PlantCategoryName { get; set; }
        public string ProductFormName { get; set; }
        public string SpeciesName { get; set; }
        public string VarietyName { get; set; }
        public string BreederName { get; set; }
        public string ColorDescription { get; set; }
        public string Color { get; set; }
        public string Habit { get; set; }
        public string Vigor { get; set; }
        public Guid? DescriptionHtmlGuid { get; set; }
        public string VarietyImageUrl { get; set; }
        public string CultureLibraryUrl { get; set; }
        public string CultureNotesLinkText { get; set; }


        //public Catalogue SelectedCategory { get; set; }
        //public ProductFormCategory SelectedProductFormCategory { get; set; }

        //public string Species { get; set; }

        //public int VarietyCount { get; set; }
        //public IEnumerable<Variety> VarietyCollection { get; set; }
        //public ShipWeek DefaultShipWeek { get; set; }
        //public ShipWeek SelectedShipWeek { get; set; }

        public Guid? SeriesDescriptionHtmlGuid { get; set; }


    }
}
