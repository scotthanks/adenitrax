﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObjectsLibrary
{
    public class SupplierOrder
    {
        public List<OrderLine> OrderLines;
        public Lookup ShipMethod;
        public Lookup ShipMethodDefault;
        public Lookup TagRatio;
        public Lookup TagRatioDefault;

        public Guid SupplierOrderGuid { get; set; }
        public string SupplierOrderNo { get; set; }
        public decimal Freight { get; set; }

        public Supplier Supplier { get; set; }

        public List<SupplierOrderFee> SupplierOrderFeeList { get; set; }

        public List<Lookup> TagRatioList { get; set; }
        public List<Lookup> ShipMethodList { get; set; }

        public Lookup SupplierOrderStatus;

        public List<GrowerOrderHistoryEvent> GrowerOrderHistoryEvents { get; set; }

        public List<SupplierOrderFee> SupplierOrderFees { get; set; }

        public string TrackingNumber { get; set; }
        public string TrackingComment { get; set; }
        public DateTime ExpectedDeliveryDate { get; set; }
        public DateTime LastSentToSupplierDate { get; set; }

        public SupplierOrder()
        {
            OrderLines = new List<OrderLine>();
            TagRatioList = new List<Lookup>();
            ShipMethodList = new List<Lookup>();
            SupplierOrderFeeList = new List<SupplierOrderFee>();
            GrowerOrderHistoryEvents = new List<GrowerOrderHistoryEvent>();
        }

        public SupplierOrder(DataObjectLibrary.SupplierOrder supplierOrderTableObject)
            : this()
        {
            TrackingNumber = supplierOrderTableObject.TrackingNumber;
            TrackingComment = supplierOrderTableObject.TrackingComment;
            if (supplierOrderTableObject.ExpectedDeliveredDate != null)
                ExpectedDeliveryDate = (DateTime)supplierOrderTableObject.ExpectedDeliveredDate;
            else
                ExpectedDeliveryDate = DateTime.MinValue;
            SupplierOrderGuid = supplierOrderTableObject.Guid;
            SupplierOrderNo = supplierOrderTableObject.SupplierOrderNo;
            Supplier = new Supplier(supplierOrderTableObject.Supplier);
            SupplierOrderStatus = new Lookup(supplierOrderTableObject.SupplierOrderStatusLookup);
            if (supplierOrderTableObject.DateLastSenttoSupplier != null)
            {
                LastSentToSupplierDate = (DateTime)supplierOrderTableObject.DateLastSenttoSupplier; 
            }
            
            TagRatio = new Lookup(supplierOrderTableObject.TagRatioLookup);
            ShipMethod = new Lookup(supplierOrderTableObject.ShipMethodLookup);

            foreach (var supplierOrderFeeTableRow in supplierOrderTableObject.SupplierOrderFeeList)
            {
                SupplierOrderFeeList.Add(new SupplierOrderFee(supplierOrderFeeTableRow));
            }
        }

        public string ToFormattedString(int tabLevel = 0)
        {
            var stringBuilder = new StringBuilder();

            string tabs = "";
            for (int i = 0; i < tabLevel; i++)
            {
                tabs += '\t';
            }

            stringBuilder.Append(tabs);
            stringBuilder.AppendLine("SUPPLIER ORDER");
            stringBuilder.Append(tabs);
            stringBuilder.Append("{");
            stringBuilder.AppendLine();
            stringBuilder.Append(Supplier.ToFormattedString(tabLevel + 1));
            stringBuilder.Append(tabs);
            stringBuilder.Append("}");
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Guid:{0}", SupplierOrderGuid);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("SupplierOrderNo:{0}", SupplierOrderNo);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Freight:${0:#,##0.00}", Freight);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("TagRatio:{0}", TagRatio.Code);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("ShipMethod:{0}", ShipMethod.Code);
            stringBuilder.AppendLine();

            foreach (OrderLine orderLine in OrderLines)
            {
                stringBuilder.Append(tabs);
                stringBuilder.AppendLine("{");
                stringBuilder.Append(orderLine.ToFormattedString(tabLevel + 1));
                stringBuilder.AppendLine();
                stringBuilder.Append(tabs);
                stringBuilder.AppendLine("}");
            }

            foreach (var supplierOrderFee in SupplierOrderFeeList)
            {
                stringBuilder.Append(tabs);
                stringBuilder.AppendLine("{");
                stringBuilder.Append(supplierOrderFee.ToFormattedString(tabLevel + 1));
                stringBuilder.AppendLine();
                stringBuilder.Append(tabs);
                stringBuilder.AppendLine("}");
            }

            return stringBuilder.ToString();
        }
    }
}