﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace BusinessObjectsLibrary
{
    [DataContract]
    public class ProductFormBase
    {
        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public Guid? SupplierGuid { get; set; }

        [DataMember]
        public Guid ProductFormCategoryGuid { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int SalesUnitQty { get; set; }

        [DataMember]
        public int TagBundleQty { get; set; }

        [DataMember]
        public int LineItemMinimumQty { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        public ProductFormCategory ProductFormCategory { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("Guid={0}", Guid);
            result.Append(", ");
            result.AppendFormat("ProductFormCategoryGuid={0}", ProductFormCategoryGuid);
            result.Append(", ");
            result.AppendFormat("SupplierGuid={0}", SupplierGuid);
            result.Append(", ");
            result.AppendFormat("Code={0}", Code);
            result.Append(", ");
            result.AppendFormat("Name={0}", Name);
            result.Append(", ");
            result.AppendFormat("SalesUnitQty={0}", SalesUnitQty);
            result.Append(", ");
            result.AppendFormat("TagBundleQty={0}", TagBundleQty);
            result.Append(", ");
            result.AppendFormat("LineItemMinimumQty={0}", LineItemMinimumQty);
            result.Append(", ");
            result.AppendFormat("IsActive={0}", IsActive);

            result.Append(", ");
            result.AppendFormat("ProductFormCategory={{{0}}}", ProductFormCategory);

            return result.ToString();
        }
    }
}