﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    
    public class SupplierOrderFee
    {
        public SupplierOrderFee(DataObjectLibrary.SupplierOrderFee supplierOrderFeeTableRow)
        {
            this.Guid = supplierOrderFeeTableRow.Guid;
            this.SupplierOrderGuid = supplierOrderFeeTableRow.SupplierOrderGuid;
            this.SupplierOrderFeeType = new Lookup(supplierOrderFeeTableRow.FeeTypeLookup);
            this.Amount = supplierOrderFeeTableRow.Amount;
            this.SupplierOrderFeeStatus = new Lookup(supplierOrderFeeTableRow.FeeStatXLookup);
            this.FeeDescription = supplierOrderFeeTableRow.FeeDescription;

        }

        public Guid Guid { get; set; }
        public Guid SupplierOrderGuid { get; set; }
        public Lookup SupplierOrderFeeType { get; set; }
        public Lookup SupplierOrderFeeStatus { get; set; }
        public decimal Amount { get; set; }
        public string FeeDescription { get; set; }

        internal string ToFormattedString(int tabLevel)
        {
            var stringBuilder = new StringBuilder();

            string tabs = "";
            for (int i = 0; i < tabLevel; i++)
            {
                tabs += '\t';
            }

            stringBuilder.Append(tabs);
            stringBuilder.AppendLine("SUPPLIER ORDER FEE");
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("SupplierOrderFeeTypeCode:{0}", this.SupplierOrderFeeType.Code);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("SupplierOrderFeeTypeDescription:{0}", this.SupplierOrderFeeType.Description);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Guid:{0}", Guid);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Amount:${0:#,##0.00}", Amount);
            stringBuilder.AppendLine();

            return stringBuilder.ToString();
        }
    }
}
