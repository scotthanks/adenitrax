﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class StatusTotal
    {
        public string Status { get; set; }
        public int Qty { get; set; }
    }
}
