﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class Supplier
    {
        public Supplier(DataObjectLibrary.Supplier supplierDataRow)
        {
            Guid = supplierDataRow.Guid;
            Code = supplierDataRow.Code;
            Name = supplierDataRow.Name;
            Email = supplierDataRow.EMail;

            var phone = new Utility.PhoneNumber(supplierDataRow.PhoneAreaCode, supplierDataRow.Phone);
            Phone = phone.FormattedPhoneNumber;
        }

        public Guid Guid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string ToFormattedString(int tabLevel = 0)
        {
            var stringBuilder = new StringBuilder();

            string tabs = "";
            for (int i = 0; i < tabLevel; i++)
            {
                tabs += '\t';
            }

            stringBuilder.Append(tabs);
            stringBuilder.AppendLine("SUPPLIER");
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Guid:{0}", Guid);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Code:{0}", Code);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Name:{0}", Name);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Email:{0}", Email);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Phone:{0}", Phone);
            stringBuilder.AppendLine();

            return stringBuilder.ToString();
        }
    }
}
