﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class Address
    {
        public Address()
        {
        }

        public Address(DataObjectLibrary.Address addressDataRow)
            : this()
        {
            AddressGuid = addressDataRow.Guid;
            AddressName = addressDataRow.Name;
            StreetAddress1 = addressDataRow.StreetAddress1;
            StreetAddress2 = addressDataRow.StreetAddress2;
            City = addressDataRow.City;
            State = new State(addressDataRow.State);
            ZipCode = addressDataRow.ZipCode;
        }

        public Guid AddressGuid { get; set; }
        public string AddressName { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public State State { get; set; }
        public string ZipCode { get; set; }
    }
}
