﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class OrderLine
    {
        public Guid Guid { get; set; }
        public Product Product { get; set; }

        public int QuantityOrdered { get; set; }
        public int QuantityShipped { get; set; }
        public bool? IsCarted { get; set; }

        public Guid? PriceUsedGuid { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public bool PriceHasBeenSet { get; set; }

        public Guid? CurrentProductPriceGuid { get; set; }
        public decimal CurrentProductPrice { get; set; }
        public decimal CurrentProductCost { get; set; }

        public int AvailableQty { get; set; }
        public string AvailabilityTypeCode { get; set; }

        public Lookup OrderLineStatus { get; set; }

        public List<GrowerOrderHistoryEvent> GrowerOrderHistoryEvents { get; set; }

        public bool IsLockedForAllChanges { get; set; }
        public bool IsLockedForReduction { get; set; }

        public int LineNumber { get; set; }
        public long OrderLineID { get; set; }
        public long ProductID { get; set; }
        public string LineComment { get; set; }

        public string DateEnteredString { get; set; }
        public string DateChangedString { get; set; }
        public string DateQtyChangedString { get; set; }

        public string ShipWeekCode { get; set; }
        private OrderLine()
        {
            GrowerOrderHistoryEvents = new List<GrowerOrderHistoryEvent>();
        }


        public OrderLine(DataObjectLibrary.OrderLine orderLineTableObject)
            : this()
        {
            Guid = orderLineTableObject.Guid;
            QuantityOrdered = orderLineTableObject.QtyOrdered;
            QuantityShipped = orderLineTableObject.QtyShipped;

            IsCarted = orderLineTableObject.OrderLineStatusLookup.Code != "PreCart";

            Product = new Product(orderLineTableObject.Product);

            OrderLineStatus = new Lookup(orderLineTableObject.OrderLineStatusLookup);

            if (orderLineTableObject.Product.ReportedAvailabilityListIsLoaded)
            {
                ReportedAvailability reportedAvailability =
                    orderLineTableObject.Product.ReportedAvailabilityList.First();
                if (reportedAvailability != null)
                {
                    int sales = 0;
                    if (reportedAvailability.SalesSinceDateReported.HasValue)
                    {
                        sales = (int)reportedAvailability.SalesSinceDateReported;
                    }

                    AvailableQty = Math.Max(reportedAvailability.Qty - sales,0);
                    AvailabilityTypeCode = reportedAvailability.AvailabilityTypeLookup.Code;
                }
            }

            PriceUsedGuid = orderLineTableObject.ActualPriceUsedGuid;
            Price = orderLineTableObject.ActualPrice;
            Cost = orderLineTableObject.ActualCost ?? 0;
            LineNumber = orderLineTableObject.LineNumber;
            LineComment = orderLineTableObject.LineComment;
            OrderLineID = orderLineTableObject.Id;
            ProductID = Product.ID;
            PriceHasBeenSet = !((orderLineTableObject.ActualPriceUsedGuid ?? Guid.Empty) == Guid.Empty);
        }

        public string ToFormattedString(int tabLevel = 0)
        {
            var stringBuilder = new StringBuilder();

            string tabs = "";
            for (int i = 0; i < tabLevel; i++)
            {
                tabs += '\t';
            }

            stringBuilder.Append(tabs);
            stringBuilder.AppendLine("ORDER LINE");
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Guid:{0}", Guid);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("QuantityOrdered:{0}", QuantityOrdered);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("IsCarted:{0}", IsCarted);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Price:${0:#,##0.00000}", Price);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Product:{0}", Product.Code);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("AvailableQty:{0}", AvailableQty);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("AvailabilityTypeCode:{0}", AvailabilityTypeCode);
            stringBuilder.AppendLine();

            return stringBuilder.ToString();
        }
    }
}