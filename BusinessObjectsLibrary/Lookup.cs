﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class Lookup : IComparable<Lookup>
    {
        public Lookup(DataObjectLibrary.Lookup lookupDataRow, IEnumerable<DataObjectLibrary.Lookup> siblingDataRowList = null, bool getParentPath = false)
        {
            Guid = lookupDataRow.Guid;
            Code = lookupDataRow.Code;
            Name = lookupDataRow.Name;
            DisplayName = lookupDataRow.Name;
            Description = lookupDataRow.Name;
            SortSequence = lookupDataRow.SortSequence;
            //TODO: Fix this so it doesn't cause duplicate Business objects.
            if (getParentPath)
            {
                ParentPath = lookupDataRow.ParentLookup != null ? lookupDataRow.ParentLookup.Path : "";                
            }

            if (siblingDataRowList != null)
            {
                SiblingList = new List<Lookup>();
                foreach (var siblingDataRow in siblingDataRowList)
                {
                    SiblingList.Add(new Lookup(siblingDataRow));
                }
            }
        }

        public Guid Guid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public double SortSequence { get; set; }
        public string ParentPath { get; set; }

        public List<Lookup> SiblingList { get; set; } 

        public int CompareTo(Lookup other)
        {
            int returnValue = 0;
            returnValue = this.SortSequence.CompareTo(other.SortSequence);

            if (returnValue == 0)
            {
                returnValue = System.String.Compare(this.DisplayName, other.DisplayName, System.StringComparison.Ordinal);
            }

            return returnValue;
        }
    }
}
