﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class MenuDataItem
    {
        public Guid ProductFormCategoryGuid { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public string ProductFormCategoryName { get; set; }
        public Guid ProgramTypeGuid { get; set; }
        public string ProgramTypeCode { get; set; }
        public string ProgramTypeName { get; set; }
			
	
    }
}
