﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class Species
    {
        public Species()
        {
            SelectedVarieties = new List<Variety>();
        }

        public Species(DataObjectLibrary.Species speciesTableRow)
            :this()
        {
            Code = speciesTableRow.Code;
            Name = speciesTableRow.Name;
        }

        public string Code { get; set; }
        public string Name { get; set; }

        public List<Variety> SelectedVarieties { get; set; }
    }
}
