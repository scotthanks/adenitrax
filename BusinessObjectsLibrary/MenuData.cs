﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    public class MenuData
    {
        public MenuData()
        {
            ProgramTypes = new List<ProgramType>();
            ActiveProgramTypes = new List<ProgramType>();
            ProductForms = new List<ProductFormCategory>();
        }

        public List<ProgramType> ProgramTypes { get; set; }
        public List<ProgramType> ActiveProgramTypes { get; set; }

        public List<ProductFormCategory> ProductForms { get; set; }
    }
}
