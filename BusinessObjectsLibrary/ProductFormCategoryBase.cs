﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BusinessObjectsLibrary
{
    [DataContract]
    public class ProductFormCategoryBase : IComparable<ProductFormCategoryBase>
    {
        [DataMember]
        public bool IsLoaded { get; set; }

        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public string Code { get; set; }    //nchar 10 in Database

        [DataMember]
        public string Name { get; set; }    //nvarchar 50 in Database

        [DataMember]
        public bool IsRooted { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string AvailabilityWeekCalculationMethod { get; set; }

        [DataMember]
        public IEnumerable<ProductForm> ProductFormList { get; set; }

        public int CompareTo(ProductFormCategoryBase other)
        {
            return System.String.Compare(this.Name, other.Name, System.StringComparison.Ordinal);
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("Guid={0}", this.Guid);
            result.Append(", ");
            result.AppendFormat("Code={0}", this.Code);
            result.Append(", ");
            result.AppendFormat("Name={0}", this.Name);
            result.Append(", ");
            result.AppendFormat("IsRooted={0}", this.IsRooted);
            result.Append(", ");
            result.AppendFormat("IsActive={0}", this.IsActive);

            return result.ToString();
        }
    }
}
