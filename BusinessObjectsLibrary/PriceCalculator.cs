﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class PriceCalculator
    {
        public Guid? PriceGuid { get; set; }

        public DateTime OrderDate { get; set; }
        public DateTime ShipDate { get; set; }

        public DateTime EodDate { get; set; }

        public decimal Cost { get; set; }
        public string PriceTypeCode { get; set; }

        public decimal RegularCost { get; set; }
        public decimal EodCost { get; set; }

        public decimal MarkupPercentage { get; set; }
        public decimal RegularMarkupPercentage { get; set; }
        public decimal EodMarkupPercentage { get; set; }

  

        public decimal Price
        {
            get { return Cost + (Cost * MarkupPercentage / 100); } 
        }

        public PriceCalculator(Price priceData, ProgramSeason programSeasonData, DateTime orderDate, DateTime shipDate, Guid? priceGuid)
        {
            if (priceData != null)
            {
                PriceGuid = priceGuid;

                OrderDate = orderDate;
                ShipDate = shipDate;

                if (programSeasonData.RollingWeeksEOD == 0)
                {
                    EodDate = programSeasonData.EODDate;
                }
                else
                {
                    EodDate = shipDate.Subtract(new TimeSpan(programSeasonData.RollingWeeksEOD * 7, 0, 0, 0));
                }

                if (orderDate <= EodDate)
                {
                    Cost = priceData.EODCost;
                    MarkupPercentage = (decimal)priceData.EODMUPercent;
                    PriceTypeCode = "EOD";
                }
                else
                {
                    Cost = priceData.RegularCost;
                    MarkupPercentage = (decimal)priceData.RegularMUPercent;
                    PriceTypeCode = "Regular";
                }

             
            }
        }
    }
}
