﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessObjectsLibrary
{
    public class CreditCard
    {
        public CreditCard()
        {    
        }

        public CreditCard(DataObjectLibrary.GrowerCreditCard creditCardDataRow)
        {
            Guid = creditCardDataRow.Guid;
            CardDescription = creditCardDataRow.CardDescription;
            CardTypeCode = "Visa";
            CardTypeImageUrl = "???";
            IsDefault = creditCardDataRow.DefaultCard;
            CardNumber = creditCardDataRow.LastFourDigits;
            ExpirationDate = "XXXX";
            PaymentProfileId = creditCardDataRow.AuthorizeNetSerialNumber;
        }

        public Guid Guid { get; set; }
        public string CardDescription { get; set; }
        public string CardTypeCode { get; set; }
        public string CardTypeImageUrl { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string PaymentProfileId { get; set; }

        public bool IsDefault { get; set; }
        public bool IsSelected { get; set; }
    }
}

