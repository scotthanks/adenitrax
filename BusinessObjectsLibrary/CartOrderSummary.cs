﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataObjectLibrary;

namespace BusinessObjectsLibrary
{
    public class CartOrderSummary
    {
        public CartOrderSummary()
        { 
        }
        

        public Guid OrderGuid { get; set; }
        public string ShipWeekString { get; set; }
        public string ProgramCategoryName { get; set; }
        public string ProductFormName { get; set; }
        public int OrderQty { get; set; }
        public long OrderID { get; set; }

  


    
    }
}
