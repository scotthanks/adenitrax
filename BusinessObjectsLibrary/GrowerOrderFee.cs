﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjectsLibrary
{
    
    public class GrowerOrderFee
    {
        public GrowerOrderFee(DataObjectLibrary.GrowerOrderFee growerOrderFeeTableRow)
        {
            this.Guid = growerOrderFeeTableRow.Guid;
            this.GrowerOrderGuid = growerOrderFeeTableRow.GrowerOrderGuid;
            this.GrowerOrderFeeType = new Lookup(growerOrderFeeTableRow.FeeTypeLookup);
            this.Amount = growerOrderFeeTableRow.Amount;
            this.GrowerOrderFeeStatus = new Lookup(growerOrderFeeTableRow.FeeStatXLookup);
            this.FeeDescription = growerOrderFeeTableRow.FeeDescription;

        }

        public Guid Guid { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public Lookup GrowerOrderFeeType { get; set; }
        public Lookup GrowerOrderFeeStatus { get; set; }
        public decimal Amount { get; set; }
        public string FeeDescription { get; set; }

        internal string ToFormattedString(int tabLevel)
        {
            var stringBuilder = new StringBuilder();

            string tabs = "";
            for (int i = 0; i < tabLevel; i++)
            {
                tabs += '\t';
            }

            stringBuilder.Append(tabs);
            stringBuilder.AppendLine("GROWER ORDER FEE");
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("GrowerOrderFeeTypeCode:{0}", this.GrowerOrderFeeType.Code);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("GrowerOrderFeeTypeDescription:{0}", this.GrowerOrderFeeType.Description);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Guid:{0}", Guid);
            stringBuilder.AppendLine();
            stringBuilder.Append(tabs);
            stringBuilder.AppendFormat("Amount:${0:#,##0.00}", Amount);
            stringBuilder.AppendLine();

            return stringBuilder.ToString();
        }
    }
}
