﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace DataAccessLibrary
{
    public abstract class ProcedureBase
    {
        private ProcedureParameterBase _parameters;

        public string ProcedureName { get; protected set; }

        public ProcedureParameterBase GetParameters()
        {
            if (_parameters == null)
            {
                _parameters = CreateParameters();
            }
            return _parameters;
        }

        public void SetParameters(ProcedureParameterBase parameters)
        {
            _parameters = parameters;
        }

        public abstract ProcedureParameterBase CreateParameters();

        public static void CheckForParameterOverflow(string parameterName, string value, int maxLength, string className)
        {
           

            if (value != null)
            {
                value = value.Trim();

                if (value.Length > maxLength)
                {
                    throw new ArgumentException
                        (
                            string.Format
                                (
                                    "The string value of \"{0}\" ({1}) is too long for the {2} parameter.", 
                                    value, 
                                    value.Length,
                                    parameterName
                                 )
                        );
                }
               
            }
        }
    }
}
