﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    public static class Utility
    {
        public static string GetParameterList(System.Data.SqlClient.SqlParameterCollection sqlParameterCollection)
        {
            var parameterListBuilder = new StringBuilder();

            foreach (var parameter in sqlParameterCollection)
            {
                var dbParameter = (System.Data.Common.DbParameter)parameter;
                string parameterValue = GetDbParameterValueString(dbParameter);
                parameterListBuilder.AppendFormat("{0}={1},", dbParameter.ParameterName, parameterValue);
            }

            string parameterList = parameterListBuilder.ToString();
            if (parameterListBuilder.Length >= 1)
                parameterList = parameterList.Substring(0, parameterListBuilder.Length - 1);

            return parameterList;
        }

        private static string GetDbParameterValueString(System.Data.Common.DbParameter dbParameter)
        {
            string valueString;

            if (dbParameter.Value == DBNull.Value)
                valueString = "(DBNull)";
            else if (dbParameter.Value == null)
                valueString = "(null)";
            else
            {
                switch (dbParameter.DbType)
                {
                    case DbType.String:
                    case DbType.StringFixedLength:
                        valueString = "\"" + dbParameter.Value + "\"";
                        break;
                    case DbType.Guid:
                        var guidValue = dbParameter.Value as Guid?;
                        valueString = guidValue == Guid.Empty ? "(empty)" : "\"" + guidValue.ToString() + "\"";
                        break;
                    default:
                        valueString = dbParameter.Value.ToString();
                        break;
                }
            }

            if (dbParameter.Direction != ParameterDirection.Input)
            {
                valueString += dbParameter.Direction.ToString();
            }

            return valueString;
        }
    }
}
