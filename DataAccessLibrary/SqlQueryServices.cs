﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataAccessLibrary
{
    public class SqlQueryServices
    {
        private const string DateDeactivated = "DateDeactivated";
        private const string IsActiveClause = DateDeactivated + " IS NULL";
        private const string AndIsActiveClause = " AND " + IsActiveClause;
        private const string IsDeactivatedClause = DateDeactivated + " IS NOT NULL";
        private const string AndIsDeactivatedClause = " AND " + IsDeactivatedClause;

        public SqlDataReader GetAllReader(string tableName, SqlConnection connection, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, GetAllQuery(tableName), transaction: transaction);
        }

        public SqlDataReader GetAllActiveReader(string tableName, SqlConnection connection, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, GetAllActiveQuery(tableName), transaction: transaction);
        }

        public SqlDataReader GetByGuidReader(string tableName, SqlConnection connection, Guid? guid, string guidFieldName = null, bool allowInactive = false, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, GetByGuidQuery(tableName, guid, guidFieldName, allowInactive), transaction: transaction);
        }

        public SqlDataReader GetByCodeReader(string tableName, SqlConnection connection, string code, bool excludeInactive, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, GetByCodeQuery(tableName, code, excludeInactive), transaction: transaction);
        }

        public SqlDataReader GetByFieldReader(string tableName, SqlConnection connection, string fieldName, string value, string operatorString, bool excludeInactive, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, GetByFieldQuery(tableName, fieldName, value, operatorString), transaction: transaction);
        }

        public SqlDataReader GetByFieldStartsWithReader(string tableName, SqlConnection connection, string fieldName, string value, bool excludeInactive, SqlTransaction transaction)
        {
            return ReaderFromSqlQuery(connection, GetByFieldStartsWithQuery(tableName, fieldName, value, excludeInactive), transaction: transaction);
        }

        public SqlDataReader GetByFieldLikeReader(string tableName, SqlConnection connection, string fieldName, string value, bool excludeInactive, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, GetByFieldLikeQuery(tableName, fieldName, value, excludeInactive), transaction: transaction);
        }

        public SqlDataReader UpdateFieldReader(string tableName, SqlConnection connection, string code, string fieldName, string newValue, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, UpdateFieldQuery(tableName, code, fieldName, newValue), transaction: transaction);
        }

        public SqlDataReader UpdateFieldReader(string tableName, SqlConnection connection, Guid guid, string fieldName, object newValue, SqlTransaction transaction = null)
        {
            return ReaderFromSqlQuery(connection, UpdateFieldQuery(tableName, guid, fieldName, newValue), transaction: transaction);
        }

        public long Execute(SqlCommand command)
        {
            long result;
            try
            {
                command.Connection.Open();
                result = Convert.ToInt64(command.ExecuteScalar());
            }
            catch (Exception e)
            {
                throw new DataException(string.Format( "Error execcuting command \"{0}\" ({1}).", command.CommandText, e.Message), e);
                result = long.MinValue;
            }
            finally
            {
                command.Connection.Close();                
            }

            return result;
        }

        public string GetAllQuery(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            return string.Format("SELECT * FROM {0}", tableName);
        }

        public string GetAllActiveQuery(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            return string.Format("SELECT * FROM {0} WHERE {1}", tableName, IsActiveClause);            
        }

        private string GetByGuidQuery(string tableName, Guid? guid, string guidFieldName = null, bool allowInactive = false)
        {
            string sql = "";
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            guidFieldName = string.IsNullOrEmpty(guidFieldName) ? "Guid" : guidFieldName.Trim();
           
            string isActiveClause = allowInactive ? "" : " AND " + IsActiveClause;
            if (tableName == "GrowerOrder")
            {
                sql = string.Format("SELECT *,'VA' as NavigationProgramTypeCode, 'RC' as NavigationProductFormCategoryCode FROM {0} WHERE {1} = '{2}'{3}", tableName, guidFieldName, guid == null ? "null" : guid.ToString(), isActiveClause);
            }
            else
            {
                sql = string.Format("SELECT * FROM {0} WHERE {1} = '{2}'{3}", tableName, guidFieldName, guid == null ? "null" : guid.ToString(), isActiveClause);

            }

            if (guid == null)
            {
                sql = sql.Replace(" = 'null'", " IS NULL");
            }

            return sql;
        }

        public string DeactivateByGuidQuery(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            string sql = string.Format("UPDATE {0} SET {1}=CURRENT_TIMESTAMP WHERE Guid=@Guid{2};SELECT * FROM {0} WHERE Guid=@Guid{3}", tableName, DateDeactivated, AndIsActiveClause, AndIsDeactivatedClause);

            return sql;
        }

        public string ActivateByGuidQuery(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            string sql = string.Format("UPDATE {0} SET {1}=NULL WHERE Guid=@Guid{2};SELECT * FROM {0} WHERE Guid=@Guid{3}", tableName, DateDeactivated, AndIsDeactivatedClause, AndIsActiveClause);

            return sql;
        }

        public string DeleteByGuidQuery(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            string sql = string.Format("DELETE {0} WHERE Guid=@Guid;SELECT * FROM {0} WHERE Guid=@Guid", tableName);

            return sql;
        }

        private string GetByCodeQuery(string tableName, string code, bool excludeInactive)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            if (string.IsNullOrEmpty(code))
            {
                throw new ArgumentNullException("code");
            }

            string sql = GetByFieldQuery(tableName, "Code", code, "=", excludeInactive);

            return sql;
        }

        private string GetByFieldStartsWithQuery(string tableName, string fieldName, string value, bool excludeInactive = true)
        {
            return GetByFieldLikeQuery(tableName, fieldName, value + '%', excludeInactive: excludeInactive);
        }

        private string GetByFieldLikeQuery(string tableName, string fieldName, string value, bool excludeInactive = true)
        {
            return GetByFieldQuery(tableName, fieldName, value, operatorString: "LIKE", excludeInactive: excludeInactive);
        }

        private string GetByFieldQuery(string tableName, string fieldName, string value, string operatorString, bool excludeInactive = true)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            if (value == null)
            {
                if (operatorString == "=")
                {
                    operatorString = "IS";
                }
                else if (operatorString == "!=")
                {
                    operatorString = "IS NOT";
                }

                if (operatorString != "IS" && operatorString != "IS NOT")
                {
                    throw new ArgumentException("When the value is null, the operator can only be \"IS\" or \"IS NOT\" or \"=\" or \"!=\", which will be converted to these).");
                }
            }

            string isActiveClause;
            if (excludeInactive)
            {
                isActiveClause = " AND " + IsActiveClause;
            }
            else
            {
                isActiveClause = "";                
            }

            string literalValue = (value == null) ? "NULL" : string.Format("'{0}'", value);
            string sql = string.Format("SELECT * FROM {0} WHERE {1} {2} {3}{4}", tableName, fieldName, operatorString, literalValue, isActiveClause);

            return sql;
        }

        private string UpdateFieldQuery(string tableName, string code, string fieldName, string newValue)
        {
            return UpdateFieldQuery(tableName, "Code", code, fieldName, newValue);
        }

        private string UpdateFieldQuery(string tableName, Guid guid, string fieldName, object newValue)
        {
            return UpdateFieldQuery(tableName, "Guid", guid, fieldName, newValue);
        }

        private string UpdateFieldQuery(string tableName, string rowIdentifierFieldName, object rowIdentifier, string fieldNameToUpdate, object newValue)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            if (string.IsNullOrEmpty(rowIdentifierFieldName))
            {
                throw new ArgumentNullException("rowIdentifierFieldName");
            }

            if (rowIdentifier == null || rowIdentifier is string && string.IsNullOrEmpty((string)rowIdentifier))
            {
                throw new ArgumentNullException("rowIdentifier");
            }

            string sql = string.Format("UPDATE {0} SET [{1}] = '{2}' WHERE [{3}] = '{4}'; SELECT * FROM [{0}] WHERE [{3}] = '{4}'", tableName, fieldNameToUpdate, newValue, rowIdentifierFieldName, rowIdentifier);

            return sql;
        } 

        public delegate void AddParametersDelegate(SqlCommand command);

        public SqlDataReader ReaderFromSqlQuery(SqlConnection connection, string sqlCommand, AddParametersDelegate addParametersDelegate = null, SqlTransaction transaction = null)
        {
            //ToDo: Make this primitive private and create a public class that returns reader as property along with details about the execution: elapsed time, errorMsg, rows, columns

            // If a transaction is provided, use its connection instead of the connection provided in the call.
            if (transaction != null && transaction.Connection != null)
            {
                connection = transaction.Connection;
            }

            var command = new SqlCommand(sqlCommand, connection);

            if (transaction != null)
            {
                command.Transaction = transaction;
            }

            if (addParametersDelegate != null)
            {
                addParametersDelegate(command);
            }

            return ReaderFromCommand(command);
        }

        private SqlDataReader ReaderFromCommand(SqlCommand command)
        {
            try
            {
                return command.ExecuteReader();
            }
            catch (Exception e)
            {
                string parameterList = Utility.GetParameterList(command.Parameters);

                if (parameterList.Length > 0)
                    parameterList = " with parameters of " + parameterList;
                else
                    parameterList = " with no parameters";

                throw new ApplicationException(string.Format("Exception occurred while trying to open a reader from the command \"{0}\" against the database{1}. Inner Exception Message:\"{2}\".", command.CommandText, parameterList, e.Message));
            }
        }

        public int ExecuteSqlCommand(SqlConnection connection, string sqlCommand, AddParametersDelegate addParametersDelegate = null)
        {
            //ToDo: Make this primitive private and create a public class that returns reader as property along with details about the execution: elapsed time, errorMsg, rows, columns

            var command = new SqlCommand(sqlCommand, connection);

            if (addParametersDelegate != null)
            {
                addParametersDelegate(command);
            }

            return ExecuteCommand(command);
        }

        public int ExecuteCommand(SqlCommand command)
        {
            try
            {
                return command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string parameterList = Utility.GetParameterList(command.Parameters);

                if (parameterList.Length > 0)
                    parameterList = " with parameters of " + parameterList;
                else
                    parameterList = " with no parameters";

                throw new ApplicationException(string.Format("Exception occurred while trying to execute the command \"{0}\" against the database{1}. Inner Exception Message:\"{2}\".", command.CommandText, parameterList, e.Message));
            }
        }

    }
}