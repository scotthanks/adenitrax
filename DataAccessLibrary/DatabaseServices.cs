﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;

namespace DataAccessLibrary
{
    public class DatabaseServices
    {
        public void Test()
        {
            Server myServer = new Server(@"tcp:ckdoythlme.database.windows.net,1433");
            myServer.ConnectionContext.DatabaseName = "ProXcontent";
            myServer.ConnectionContext.LoginSecure = false;
            myServer.ConnectionContext.Login = "prox_db_admin@ckdoythlme";
            myServer.ConnectionContext.Password = "A090a090";

            var database = myServer.Databases[1];
            System.Console.WriteLine("Database is " + database.Name);
            System.Console.WriteLine();

            System.Console.WriteLine("Tables:");
            foreach (Table table in database.Tables)
            {
                System.Console.WriteLine("\t" + table.Name);

                System.Console.WriteLine("\t\tColumns:");
                foreach (Column column in table.Columns)
                {
                    System.Console.WriteLine(string.Format("\t\t\t{0} ({1}({2}))", column.Name, column.DataType.ToString(), column.DataType.MaximumLength));
                }
            }

            System.Console.WriteLine("Procedures:");
            foreach (StoredProcedure storedProcedure in database.StoredProcedures)
            {
                if (!storedProcedure.IsSystemObject)
                {
                    System.Console.WriteLine("\t" + storedProcedure.Name);

                    System.Console.WriteLine("\t\tParameters:");
                    foreach (Parameter parameter in storedProcedure.Parameters)
                    {
                        System.Console.WriteLine("\t\t\t" + parameter.Name + " (" + parameter.DataType.ToString() + ")");
                    }
                }
            }
        }
    }
}
