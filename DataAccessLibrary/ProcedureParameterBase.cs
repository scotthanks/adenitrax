﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    public abstract class ProcedureParameterBase
    {
        public ProcedureParameterBase()
        {
        }

        public abstract void AddTo(SqlCommand command);

        public abstract void GetOutputValuesFrom(SqlCommand command);
    }
}
