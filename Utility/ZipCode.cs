﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Utility
{
    public class ZipCode
    {
        private const string UsaOrCanadaZipCodePattern = @"^\d{5}-\d{4}|\d{5}|[A-Z]\d[A-Z] \d[A-Z]\d$";

        public static Regex Regex
        {
            get { return new Regex(UsaOrCanadaZipCodePattern); }
            //fdfgddd
        }

        public static bool IsValidUsOrCanadaZipCode(string zipCode)
        {
            return Regex.IsMatch(zipCode);
        }
    }
}
